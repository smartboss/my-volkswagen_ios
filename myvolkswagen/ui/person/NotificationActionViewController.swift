//
//  NotificationActionViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/12/19.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
protocol NotificationActionSheetDelegate: class {
    func NotificationActionDeleteBtnPressed(indexPath: IndexPath)
    func NotificationActionNotiBtnPressed()
}

class NotificationActionViewController: BaseViewController {
    
    weak var delegate: NotificationActionSheetDelegate?
    var indexPath: IndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func deleteBtnPressed(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.NotificationActionDeleteBtnPressed(indexPath: self.indexPath)
        }
    }
    
    @IBAction func notiBtnPressed(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.NotificationActionNotiBtnPressed()
        }
    }
}
