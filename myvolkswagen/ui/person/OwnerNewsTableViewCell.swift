//
//  OwnerNewsTableViewCell.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/14.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

protocol OwnerNewsTableViewCellDelegate: class {
    func didPressEditBtn(ind: IndexPath!)
}
class OwnerNewsTableViewCell: UITableViewCell {

    @IBOutlet weak var desLb: BasicLabel!
    @IBOutlet weak var titleLb: BasicLabel!
    @IBOutlet weak var dateLb: BasicLabel!
    @IBOutlet weak var editBtn: UIButton!
    var indexPath: IndexPath!
    weak var delegate: OwnerNewsTableViewCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func editBtnPressed(_ sender: Any) {
        delegate?.didPressEditBtn(ind: self.indexPath)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
