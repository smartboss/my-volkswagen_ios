//
//  TabBarBtn.swift
//  myvolkswagen
//
//  Created by Shelley on 2022/9/22.
//  Copyright © 2022 volkswagen. All rights reserved.
//

import UIKit

class TabBarBtn: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var unselectedColor: UIColor = UIColor(red: 0.61, green: 0.61, blue: 0.61, alpha: 1.00)
    var selectedColor: UIColor = UIColor.black
    
    override open var isSelected: Bool {
        didSet {
            if (isSelected) { self.tintColor = selectedColor }
            else { self.tintColor = unselectedColor }
        }
    }
    
    func setColorByIndex(lv: Int) {
        if lv == 1 { // 藍卡會員
            unselectedColor = UIColor.white
            selectedColor = UIColor(red: 0.24, green: 0.52, blue: 0.84, alpha: 1.00)
        } else if lv == 2 { // 銀卡會員
            unselectedColor = UIColor.white
            selectedColor = UIColor(red: 0.07, green: 0.21, blue: 0.40, alpha: 1.00)
        } else if lv == 3 { // 金卡會員
            unselectedColor = UIColor.white
            selectedColor = UIColor(red: 1.00, green: 0.84, blue: 0.10, alpha: 1.00)
        }
        if (isSelected) { self.tintColor = selectedColor }
        else { self.tintColor = unselectedColor }
    }
}
