//
//  NotificationViewControllerV2.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/27.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

class NotificationViewControllerV2: NaviViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource, OwnerNewsTableViewCellDelegate, NotificationActionSheetDelegate {
    enum Section: Int {
        case latest = 0, before
        static var allCases: [Section] {
            var values: [Section] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }

    var notices: [Notice] = []
    var histories: [Notice] = []
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    var imageNewsList: [ImageNews] = []
    var tabSelectedIndex: Int = 1
    @IBOutlet weak var noticeV: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        naviBar.setTitle("訊息通知")
        naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
        
        imageNewsList = NewsManager.shared().getAllNews(listener: self)
        
        notices = NoticeManager.shared().getNotice(kind: 3, historical: false, fromRemote: true, listener: self)
        //histories = NoticeManager.shared().getNotice(kind: 3, historical: true, fromRemote: true, listener: self)
        
        if tabSelectedIndex == 0 {
            segment.selectedSegmentIndex = 0
            tableView.isHidden = false
            collectionView.isHidden = true
            noticeV.isHidden = false
        } else {
            segment.selectedSegmentIndex = 1
            tableView.isHidden = true
            collectionView.isHidden = false
            noticeV.isHidden = true
        }
        
        collectionView.register(UINib(nibName: "NotificationCell", bundle: nil), forCellWithReuseIdentifier: "NotificationCell")
        tableView.register(UINib(nibName: "OwnerNewsTableViewCell", bundle: nil), forCellReuseIdentifier: "OwnerNewsTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
        
        let gradientVieww = GradientBackgroundView()
        gradientVieww.frame = view.bounds
        gradientView.addSubview(gradientVieww)
        
        refreshUI()
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
        if segment.selectedSegmentIndex == 0 {
            Utility.sendEventLog(ServiceName: "pv_notice", UniqueId: "", EventName: "上一頁", InboxId: "", EventCategory: "button", PageName: "pv_notice", EventLabel: "上一頁", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        } else {
            Utility.sendEventLog(ServiceName: "pv_news", UniqueId: "", EventName: "上一頁", InboxId: "", EventCategory: "button", PageName: "pv_news", EventLabel: "上一頁", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if NoticeManager.shared().uiUpdateNeeded {
            NoticeManager.shared().uiUpdateNeeded = false
            
            //histories = NoticeManager.shared().getNotice(kind: 3, historical: true, fromRemote: true, listener: self)
            notices = NoticeManager.shared().getNotice(kind: 3, historical: false, fromRemote: true, listener: self)
            tableView.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NoticeManager.shared().getNoticeRingClose()
        
        if segment.selectedSegmentIndex == 0 {
            Utility.sendEventLog(ServiceName: "pv_notice", UniqueId: "", EventName: "pageview", InboxId: "", EventCategory: "pageview", PageName: "pv_notice", EventLabel: "", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        } else {
            Utility.sendEventLog(ServiceName: "pv_news", UniqueId: "", EventName: "pageview", InboxId: "", EventCategory: "pageview", PageName: "pv_news", EventLabel: "", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        }
    }
    
    @IBAction func segmennt(_ sender: Any) {
        if segment.selectedSegmentIndex == 0 {
            tabSelectedIndex = 0
            refreshUI()
            Utility.sendEventLog(ServiceName: "pv_news", UniqueId: "", EventName: "車主通知", InboxId: "", EventCategory: "button", PageName: "pv_news", EventLabel: "車主通知", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
            Utility.sendEventLog(ServiceName: "pv_notice", UniqueId: "", EventName: "pageview", InboxId: "", EventCategory: "pageview", PageName: "pv_notice", EventLabel: "", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        } else {
            tabSelectedIndex = 1
            refreshUI()
            Utility.sendEventLog(ServiceName: "pv_notice", UniqueId: "", EventName: "最新消息", InboxId: "", EventCategory: "button", PageName: "pv_notice", EventLabel: "最新消息", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
            Utility.sendEventLog(ServiceName: "pv_news", UniqueId: "", EventName: "pageview", InboxId: "", EventCategory: "pageview", PageName: "pv_news", EventLabel: "", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        }
    }
    
    private func refreshUI() {
//        histories = []
//        notices = []
        
        if tabSelectedIndex == 0 {
            tableView.isHidden = false
            collectionView.isHidden = true
            //NoticeManager.shared().getNotice(kind: 3, historical: true, fromRemote: true, listener: self)
            //NoticeManager.shared().getNotice(kind: 3, historical: false, fromRemote: true, listener: self)
            noticeV.isHidden = false
        } else {
            tableView.isHidden = true
            collectionView.isHidden = false
            noticeV.isHidden = true
        }
    }
    
    // MARK: NotificationActionViewControllerDelegate
    func NotificationActionDeleteBtnPressed(indexPath: IndexPath) {
        Utility.sendEventLog(ServiceName: "pv_notice", UniqueId: "", EventName: "刪除通知", InboxId: "", EventCategory: "button", PageName: "pv_notice", EventLabel: "刪除通知", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        let customAlert = VWCustomAlert()
        customAlert.alertTitle = "刪除訊息"
        customAlert.alertMessage = "確定刪除此訊息？"
        customAlert.alertTag = 1
        customAlert.isCancelButtonHidden = false
        customAlert.okButtonAction = {
            switch Section.init(rawValue: indexPath.section)! {
            case .latest:
                let noti = self.notices[indexPath.row]
                NoticeManager.shared().deleteNotice(notiId: noti.noticeID, listener: self)
            case .before:
                let noti = self.histories[indexPath.row]
                NoticeManager.shared().deleteNotice(notiId: noti.noticeID, listener: self)
            }
        }
        self.present(customAlert, animated: true, completion: nil)
    }
    
    override func didStartDeleteNotice() {
        showHud()
    }
    
    override func didFinishDeleteNotice(success: Bool) {
        dismissHud()
        if success {
            histories = []
            notices = []
            //histories = NoticeManager.shared().getNotice(kind: 3, historical: true, fromRemote: true, listener: self)
            notices = NoticeManager.shared().getNotice(kind: 3, historical: false, fromRemote: true, listener: self)
        }
    }
    
    func NotificationActionNotiBtnPressed() {
#if DEVELOPMENT
        let urll = URL(string: "myvolkswagendev://member/setting")!
        if Deeplinker.handleDeeplink(url: urll, ref: nil) {
            Deeplinker.checkDeepLink()
        }
#else
        let urll = URL(string: "myvolkswagen://member/setting")!
        if Deeplinker.handleDeeplink(url: urll, ref: nil) {
            Deeplinker.checkDeepLink()
        }
#endif
    }
    
    // MARK: OwnerNewsTableViewCellDelegate
    func didPressEditBtn(ind: IndexPath!) {
        let sb = UIStoryboard(name: "Car", bundle: nil)
        let bottomSheetVC = sb.instantiateViewController(withIdentifier: "NotificationActionViewController") as! NotificationActionViewController
        bottomSheetVC.delegate = self
        bottomSheetVC.indexPath = ind
        
        let options = SheetOptions(
            pullBarHeight: 8,
            presentingViewCornerRadius: 20,
            shouldExtendBackground: true,
            setIntrinsicHeightOnNavigationControllers: true,
            useFullScreenMode: true,
            shrinkPresentingViewController: true,
            useInlineMode: false,
            horizontalPadding: 0,
            maxWidth: nil
        )
        
        let sheetController = SheetViewController(
            controller: bottomSheetVC,
            sizes: [.fixed(283)])
        
        sheetController.cornerRadius = 20
        sheetController.minimumSpaceAbovePullBar = 0
        sheetController.treatPullBarAsClear = false
        sheetController.dismissOnOverlayTap = true
        sheetController.dismissOnPull = true
        
        sheetController.shouldDismiss = { _ in
            return true
        }
        sheetController.didDismiss = { _ in
        }
        self.present(sheetController, animated: true, completion: nil)
    }
    
    // MARK: notice protocol
    
    override func didStartGetNotice(historical: Bool) {
        Logger.d("didStartGetNotice historical: \(historical)")
//        showHud()
    }
    
    override func didFinishGetNotice(success: Bool, historical: Bool, notices: [Notice]?) {
        Logger.d("didFinishGetNotice historical: \(historical)")
//        dismissHud()
        
        if success {
            if historical {
                if let n = notices {
                    histories = n
                }
            } else {
                if let n = notices {
                    self.notices = n
                }
            }
            
            NoticeManager.shared().unreadCount = 0
        }
        self.tableView.reloadData()
    }
    
    override func didStartSetRead() {}
    
    override func didFinishSetRead(success: Bool, notice: Notice) {
        Logger.d("didFinishSetRead success: \(success)")
    }
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Section.init(rawValue: section)! {
        case .latest:
            if notices.count == 0 {
                return 0
            } else {
                return notices.count
            }
        case .before:
            if histories.count == 0 {
                return 0
            } else {
                return histories.count
            }
        }
    }
    
    private func getNotice(of position: IndexPath) -> Notice {
        switch Section.init(rawValue: position.section)! {
        case .latest:
            return notices[position.row]
        case .before:
            return histories[position.row]
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch Section.init(rawValue: indexPath.section)! {
        case .latest:
            let noti = self.notices[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "OwnerNewsTableViewCell", for: indexPath) as! OwnerNewsTableViewCell
            let timeDifference = Utility.timeDifferenceStringV2(from: noti.pDate ?? "")
            cell.dateLb.text = timeDifference
            cell.titleLb.text = noti.content
            cell.indexPath = indexPath
            cell.delegate = self
            if noti.isRead() {
                cell.backgroundColor = UIColor.clear
            } else {
                cell.backgroundColor = UIColor.MyTheme.secondaryColor000
            }
            if let dess = noti.des {
                let ts = noti.des?.truncateString(inputString: dess, maxLength: 40) ?? ""
                if ts.count == 40 {
                    cell.desLb.text = "\(ts)..."
                } else { cell.desLb.text = ts }
            } else {
                cell.desLb.text = " "
            }
            return cell
        case .before:
            let noti = self.histories[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "OwnerNewsTableViewCell", for: indexPath) as! OwnerNewsTableViewCell
            let timeDifference = Utility.timeDifferenceStringV2(from: noti.pDate ?? "")
            cell.dateLb.text = timeDifference
            cell.titleLb.text = noti.content
            cell.indexPath = indexPath
            cell.delegate = self
            if let dess = noti.des {
                let ts = noti.des?.truncateString(inputString: dess, maxLength: 40) ?? ""
                if ts.count == 40 {
                    cell.desLb.text = "\(ts)..."
                } else { cell.desLb.text = ts }
            } else {
                cell.desLb.text = " "
            }
             return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        Logger.d("You tapped cell number \(indexPath.row).")
        //tableView.deselectRow(at: indexPath, animated: true)
        
//        Logger.d("click notice at row \(String(describing: indexPath.row))")
        let notice: Notice = getNotice(of: indexPath)
        Logger.d("notice: \(notice)")
        
        Utility.sendEventLog(ServiceName: "pv_notice", UniqueId: "", EventName: "車主通知詳細_\(notice.noticeID)", InboxId: "", EventCategory: "button", PageName: "pv_notice", EventLabel: "車主通知詳細_\(notice.noticeID)", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        
        setReadIfNeeded(to: notice)
        
        switch NoticeKind.init(rawValue: notice.kind)! {
        case .follow, .suggestedFriend:
            UiUtility.goBrowse(profile: Profile.init(with: notice), controller: self)
        case .like, .officialActivity, .officialNews, .newPost, .comment, .postComment:
            let news = News.init(with: notice)
            NewsManager.shared().getNews(news: news, noticeKind: nil, listener: self)
        case .clubComment, .clubPostComment, .newClubPost:
            let clubPost = ClubPost.init(with: notice.newsID!)
            ClubManager.shared().getPost(post: clubPost, listener: self)
        case .redeem:
            UiUtility.goBrowseReward(controller: self)
        case .inBox:
            goInboxContent(id: notice.newsID!, noticeKind: nil)
        case .normalSurvey:
            goInboxContent(id: notice.newsID!, noticeKind: .normalSurvey)
        case .newCarsSurvey:
            goInboxContent(id: notice.newsID!, noticeKind: .newCarsSurvey)
        case .bodyAndPaint:
            goInboxContent(id: notice.newsID!, noticeKind: .bodyAndPaint)
        case .recall:
            UserManager.shared().showNoticeBoard = nil
            goInitialPage(success: true)
        case .drivingAssistant:
            DeeplinkNavigator.shared.proceedToDeeplink(.DrivingAssistant, param: nil)
        default:
            goInboxContent(id: notice.newsID!, noticeKind: nil)
        }
    }
    
    func goInboxContent(id: Int, noticeKind: NoticeKind?) {
        let sb = UIStoryboard(name: "Car", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "NotificationDetailViewController") as! NotificationDetailViewController
        vc.inboxId = id
        vc.noticeKind = noticeKind
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func setReadIfNeeded(to notice: Notice) {
        if !notice.isRead() {
            NoticeManager.shared().setRead(notice: notice, listener: self)
        }
    }
    
    // MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageNewsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let news = imageNewsList[indexPath.item]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        if let urll = URL(string: news.coverImage ?? "") {
            cell.image.kf.setImage(with: urll, placeholder: nil)
        } else {
            cell.image.image = nil
        }
        let originalDateString = news.publishedAt ?? ""
        if let formattedDateString = formatDateString(originalDateString) {
            cell.dateLb.text = formattedDateString
        } else {
            cell.dateLb.text = news.publishedAt ?? ""
        }
        cell.titleLb.text = news.title ?? ""
        cell.desLb.text = news.des ?? ""
        cell.layer.cornerRadius = CGFloat(8).getFitSize()
        return cell
    }
    
    func formatDateString(_ dateString: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = "yyyy/MM/dd"
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let ww = UiUtility.getScreenWidth() - 48
        return CGSize(width: ww, height: ww * (493/345))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 24, bottom: 24, right: 24)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let news = imageNewsList[indexPath.item]
        if news.id != nil {
            Utility.sendEventLog(ServiceName: "pv_news", UniqueId: "", EventName: "最新消息詳細_\(news.id!)", InboxId: "", EventCategory: "button", PageName: "pv_news", EventLabel: "最新消息詳細_\(news.id!)", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        }
        if news.isLink != nil && news.isLink == true {
            if news.link != nil {
                if let urll = URL(string: news.link!) {
                    UIApplication.shared.open(urll)
                }
            }
        } else {
            let sb = UIStoryboard(name: "Car", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "NotificationDetailViewController") as! NotificationDetailViewController
            vc.imageNews = news
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
