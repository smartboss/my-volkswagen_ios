//
//  PersonViewController.swift
//  myvolkswagen
//
//  Created by Apple on 2/1/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase
import WebKit

enum ContentKind: Int {
    case post = 1, collection, all, new, activity
    static var allCases: [ContentKind] {
        var values: [ContentKind] = []
        var index = 0
        while let element = self.init(rawValue: index) {
            values.append(element)
            index += 1
        }
        return values
    }
}

class PersonViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate, WKNavigationDelegate {
    
    enum Section: Int {
        case paddingProfileBackground = 0, tab, feed, empty
        static var allCases: [Section] {
            var values: [Section] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    var webview: WKWebView?
    var deeplink: DeeplinkType?
    @IBOutlet weak var webviewCloseView: UIView!
    @IBOutlet weak var naviTopCOns: NSLayoutConstraint!
    var contentKind: ContentKind = .post
    var profile: Profile! = nil
    var isMyId = false
    var relationFollowing: Relation?
    var isPersonRoot: Bool = false
    var redeemTaskCount: Int = 0
    var redeemTotalPoints: Int = 0
    
    var newsList: [News] = []
    
    var couldShowPostEmpty: Bool = false
    var couldShowCollectionEmpty: Bool = false
    var couldShowOfficialAllEmpty: Bool = false
    var couldShowOfficialMessageEmpty: Bool = false
    var couldShowOfficialActivityEmpty: Bool = false
    
    @IBOutlet weak var tableView: UITableView!
    var cellHeightsDictionary: [IndexPath: CGFloat]!
    @IBOutlet weak var constraintTableViewTop: NSLayoutConstraint!
    
    var cardHeightDifference: CGFloat = 0
    
    // person card
    @IBOutlet weak var containerCard: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var card: UIView!
    @IBOutlet weak var cardName: UILabel!
    @IBOutlet weak var cardCarIcon: UIImageView!
    @IBOutlet weak var cardCarModel: UILabel!
    @IBOutlet weak var cardCarIcon1: UIImageView!
    @IBOutlet weak var cardCarModel1: UILabel!
    @IBOutlet weak var cardCarIcon2: UIImageView!
    @IBOutlet weak var cardCarModel2: UILabel!
    @IBOutlet weak var cardSubtitle: UILabel!
    @IBOutlet weak var cardTitleFollowing: UILabel!
    @IBOutlet weak var cardTitleFollower: UILabel!
    @IBOutlet weak var cardValueFollowing: UILabel!
    @IBOutlet weak var cardValueFollower: UILabel!
    
    @IBOutlet weak var cardTitleReward: UILabel!
    @IBOutlet weak var cardTitlePoint: UILabel!
    @IBOutlet weak var cardValueReward: UILabel!
    @IBOutlet weak var cardValuePoint: UILabel!
    
    @IBOutlet weak var cardButton: UIButton!
    @IBOutlet weak var constraintContainerCardHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintCardNameTop: NSLayoutConstraint!
    @IBOutlet weak var constraintCardCarIconTop: NSLayoutConstraint!
    @IBOutlet weak var constraintCardModelTop: NSLayoutConstraint!
    @IBOutlet weak var constraintCardCarIcon1Top: NSLayoutConstraint!
    @IBOutlet weak var constraintCardModel1Top: NSLayoutConstraint!
    @IBOutlet weak var constraintCardCarIcon2Top: NSLayoutConstraint!
    @IBOutlet weak var constraintCardMode2lTop: NSLayoutConstraint!
    @IBOutlet weak var constraintCardSubtitleTop: NSLayoutConstraint!
    @IBOutlet weak var constraintCardTitleFollowingTop: NSLayoutConstraint!
    @IBOutlet weak var constraintCardValueFollowingTop: NSLayoutConstraint!
    @IBOutlet weak var constraintCardButtonBottom: NSLayoutConstraint!
    
    private let cardNameWidth = UiUtility.adaptiveSize(size: 180)
    private let cardModelWidth = UiUtility.adaptiveSize(size: 148)
    private let cardSubtitleWidth = UiUtility.adaptiveSize(size: 180)
    private var maxCardNameHeight: CGFloat = 0
    private var maxCardModelHeight: CGFloat = 0
    private var maxCardSubtitleHeight: CGFloat = 0
    
    
    // official card
    @IBOutlet weak var containerCardOfficial: UIView!
    @IBOutlet weak var officialImage: UIImageView!
    @IBOutlet weak var cardOfficial: UIView!
    @IBOutlet weak var cardOfficialName: UILabel!
    @IBOutlet weak var cardOfficialSubtitle: UILabel!
    @IBOutlet weak var constraintContainerCardOfficialHeight: NSLayoutConstraint!
    
    @IBOutlet weak var profileBackground: UIImageView!
    
    @IBOutlet weak var buttonAdd: UIButton!
    
    // qrcode
    @IBOutlet weak var containerQrcode: UIView!
    @IBOutlet weak var cardQrcode: UIView!
    @IBOutlet weak var imageQrcode: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UiUtility.colorBackgroundWhite
        
        if let profile = profile {
            naviBar.setLeftButton(UIImage(named: "backWhite"), highlighted: UIImage(named: "backWhite"))
            
            if profile.offical != nil && profile.offical! == Role.official.rawValue {
                // official
                contentKind = .all
                Analytics.logEvent("view_vw_profile", parameters: nil)
            } else {
                // all users excluding me
                contentKind = .post
                
                Analytics.logEvent("view_other_profile", parameters: nil)
            }
            
            buttonAdd.isHidden = true
        } else {
            // mine (tab 3)
            isPersonRoot = true
            naviBar.setLeftButton(UIImage(named: "vw"), highlighted: UIImage(named: "vw"))
            naviBar.setRightButton(UIImage(named: "notif_black"), highlighted: UIImage(named: "notif_black_unread"))
            imageViewNotification = naviBar.imageViewRight
            naviBar.setRightButton1(UIImage(named: "setting_black"), highlighted: UIImage(named: "setting_black"))
            //naviBar.setRightButton2(UIImage(named: "qrcode"), highlighted: UIImage(named: "qrcode"))
            
            buttonAdd.isHidden = false
        }
        
        
        
        naviBar.setBackgroundColor(UIColor.clear)
        
        
        
        cellHeightsDictionary = [:]
        tableView.rowHeight = UITableView.automaticDimension
        
        
        
        // card person
        userImage.clipsToBounds = true
        userImage.layer.cornerRadius = UiUtility.adaptiveSize(size: 29)
        userImage.layer.borderColor = UIColor.white.cgColor
        userImage.layer.borderWidth = UiUtility.adaptiveSize(size: 1)
        //        card.layer.cornerRadius = UiUtility.adaptiveSize(size: 13)
        //
        ////        card.layer.shadowColor = UiUtility.colorDarkGrey.cgColor
        //        card.layer.shadowColor = UIColor.black.cgColor
        //        card.layer.shadowOpacity = 0.7
        //        card.layer.shadowOffset = CGSize.zero
        //        card.layer.shadowRadius = UiUtility.adaptiveSize(size: 3.0)
        ////        card.layer.masksToBounds = false
        
        cardName.textColor = UiUtility.colorBackgroundWhite
        cardCarModel.textColor = UiUtility.colorBackgroundWhite
        cardCarModel1.textColor = UiUtility.colorBackgroundWhite
        cardCarModel2.textColor = UiUtility.colorBackgroundWhite
        cardSubtitle.textColor = UiUtility.colorBackgroundWhite
        cardTitleFollowing.textColor = UiUtility.colorBackgroundWhite
        cardTitleFollower.textColor = UiUtility.colorBackgroundWhite
        cardValueFollowing.textColor = UiUtility.colorBackgroundWhite
        cardValueFollower.textColor = UiUtility.colorBackgroundWhite
        cardTitleReward.textColor = UiUtility.colorBackgroundWhite
        cardTitlePoint.textColor = UiUtility.colorBackgroundWhite
        cardValueReward.textColor = UiUtility.colorBackgroundWhite
        cardValuePoint.textColor = UiUtility.colorBackgroundWhite
        
        cardName.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
        cardCarModel.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
        cardCarModel1.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
        cardCarModel2.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
        cardSubtitle.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
        cardTitleFollowing.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
        cardTitleFollower.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
        cardValueFollowing.font = UIFont(name: "VWHead-Bold", size: UiUtility.adaptiveSize(size: 16))
        cardValueFollower.font = UIFont(name: "VWHead-Bold", size: UiUtility.adaptiveSize(size: 16))
        cardTitleReward.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
        cardTitlePoint.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
        cardValueReward.font = UIFont(name: "VWHead-Bold", size: UiUtility.adaptiveSize(size: 16))
        cardValuePoint.font = UIFont(name: "VWHead-Bold", size: UiUtility.adaptiveSize(size: 16))
        
        cardTitleFollowing.text = "my_following".localized
        cardTitleFollower.text = "my_follower".localized
        cardTitleReward.text = "person_title_reward".localized
        cardTitlePoint.text = "person_title_point".localized
        
        maxCardNameHeight = "text_2_line_name".localized.height(withConstrainedWidth: cardNameWidth, font: cardName.font!)
        maxCardModelHeight = "text_2_line_model".localized.height(withConstrainedWidth: cardModelWidth, font: cardCarModel.font!)
        maxCardSubtitleHeight = "text_2_line_subtitle".localized.height(withConstrainedWidth: cardSubtitleWidth, font: cardSubtitle.font!)
        //        Logger.d("maxCardNameHeight: \(maxCardNameHeight)")
        //        Logger.d("maxCardModelHeight: \(maxCardModelHeight)")
        //        Logger.d("maxCardSubtitleHeight: \(maxCardSubtitleHeight)")
        
        //        constraintCardTitleFollowingTop.constant = UiUtility.adaptiveSize(size: 25)
        //        constraintCardValueFollowingTop.constant = UiUtility.adaptiveSize(size: 50)
        //        constraintCardButtonBottom.constant = UiUtility.adaptiveSize(size: 24)
        
        cardName.layer.masksToBounds = false
        cardName.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardName.layer.rasterizationScale = UIScreen.main.scale
        cardName.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardName.layer.shadowOpacity = 0.7
        
        cardCarModel.layer.masksToBounds = false
        cardCarModel.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardCarModel.layer.rasterizationScale = UIScreen.main.scale
        cardCarModel.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardCarModel.layer.shadowOpacity = 0.6
        
        cardCarModel1.layer.masksToBounds = false
        cardCarModel1.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardCarModel1.layer.rasterizationScale = UIScreen.main.scale
        cardCarModel1.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardCarModel1.layer.shadowOpacity = 0.6
        
        cardCarModel2.layer.masksToBounds = false
        cardCarModel2.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardCarModel2.layer.rasterizationScale = UIScreen.main.scale
        cardCarModel2.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardCarModel2.layer.shadowOpacity = 0.6
        
        cardSubtitle.layer.masksToBounds = false
        cardSubtitle.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardSubtitle.layer.rasterizationScale = UIScreen.main.scale
        cardSubtitle.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardSubtitle.layer.shadowOpacity = 0.6
        
        cardTitleFollowing.layer.masksToBounds = false
        cardTitleFollowing.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardTitleFollowing.layer.rasterizationScale = UIScreen.main.scale
        cardTitleFollowing.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardTitleFollowing.layer.shadowOpacity = 0.6
        
        cardTitleFollower.layer.masksToBounds = false
        cardTitleFollower.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardTitleFollower.layer.rasterizationScale = UIScreen.main.scale
        cardTitleFollower.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardTitleFollower.layer.shadowOpacity = 0.6
        
        cardValueFollowing.layer.masksToBounds = false
        cardValueFollowing.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardValueFollowing.layer.rasterizationScale = UIScreen.main.scale
        cardValueFollowing.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardValueFollowing.layer.shadowOpacity = 0.5
        
        cardValueFollower.layer.masksToBounds = false
        cardValueFollower.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardValueFollower.layer.rasterizationScale = UIScreen.main.scale
        cardValueFollower.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardValueFollower.layer.shadowOpacity = 0.5
        
        cardTitleReward.layer.masksToBounds = false
        cardTitleReward.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardTitleReward.layer.rasterizationScale = UIScreen.main.scale
        cardTitleReward.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardTitleReward.layer.shadowOpacity = 0.6
        
        cardTitlePoint.layer.masksToBounds = false
        cardTitlePoint.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardTitlePoint.layer.rasterizationScale = UIScreen.main.scale
        cardTitlePoint.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardTitlePoint.layer.shadowOpacity = 0.6
        
        cardValueReward.layer.masksToBounds = false
        cardValueReward.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardValueReward.layer.rasterizationScale = UIScreen.main.scale
        cardValueReward.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardValueReward.layer.shadowOpacity = 0.5
        
        cardValuePoint.layer.masksToBounds = false
        cardValuePoint.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardValuePoint.layer.rasterizationScale = UIScreen.main.scale
        cardValuePoint.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardValuePoint.layer.shadowOpacity = 0.5
        
        
        
        // init display
        cardName.text = ""
        cardCarModel.text = nil
        cardSubtitle.text = nil
        cardValueFollowing.text = nil
        cardValueFollower.text = nil
        cardValueReward.text = nil
        cardValuePoint.text = nil
        
        //        userImage.image = UIImage.init(named: "default_profile_photo")
        //        profileBackground.image = UIImage.init(named: "default_profile_background")
        
        
        
        // card official
        officialImage.clipsToBounds = true
        officialImage.layer.cornerRadius = UiUtility.adaptiveSize(size: 29)
        officialImage.layer.borderColor = UIColor.white.cgColor
        officialImage.layer.borderWidth = UiUtility.adaptiveSize(size: 1)
        //        cardOfficial.layer.cornerRadius = UiUtility.adaptiveSize(size: 13)
        
        //        cardOfficial.layer.shadowColor = UIColor.black.cgColor
        //        cardOfficial.layer.shadowOpacity = 0.7
        //        cardOfficial.layer.shadowOffset = CGSize.zero
        //        cardOfficial.layer.shadowRadius = UiUtility.adaptiveSize(size: 3.0)
        
        cardOfficialName.textColor = UiUtility.colorBackgroundWhite
        cardOfficialSubtitle.textColor = UiUtility.colorBackgroundWhite
        
        cardOfficialName.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
        cardOfficialSubtitle.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
        
        cardOfficialName.layer.masksToBounds = false
        cardOfficialName.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardOfficialName.layer.rasterizationScale = UIScreen.main.scale
        cardOfficialName.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardOfficialName.layer.shadowOpacity = 0.6
        
        cardOfficialSubtitle.layer.masksToBounds = false
        cardOfficialSubtitle.layer.shadowOffset = CGSize(width: 0, height: 0)
        cardOfficialSubtitle.layer.rasterizationScale = UIScreen.main.scale
        cardOfficialSubtitle.layer.shadowRadius = UiUtility.adaptiveSize(size: 4)
        cardOfficialSubtitle.layer.shadowOpacity = 0.5
        
        
        
        //        constraintTableViewTop.constant = UiUtility.adaptiveSize(size: 129)
        constraintTableViewTop.constant = UiUtility.adaptiveSize(size: 0)
        
        // init display
        cardName.text = ""
        cardCarModel.text = nil
        cardSubtitle.text = nil
        cardValueFollowing.text = nil
        cardValueFollower.text = nil
        cardValueReward.text = nil
        cardValuePoint.text = nil
        
        cardQrcode.layer.cornerRadius = UiUtility.adaptiveSize(size: 10)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfileAccountId), name: Notification.Name(rawValue: "updateProfileAccountId"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func updateProfileAccountId() {
        if isMyId {
            profile.accountId = UserManager.shared().currentUser.accountId
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if let profile = profile {
            // other's
            naviTopCOns.constant = 0
            if let aid = profile.accountId {
                isMyId = false
                if let myId: String = UserManager.shared().currentUser.accountId {
                    if myId == aid {
                        isMyId = true
                        setupSelfView()
                    }
                }
                
                UserManager.shared().getProfile(withId: aid, listener: self)
                if UserManager.shared().currentUser.accountId != nil {
                    UserManager.shared().getRedeemState(listener: self)
                }
            } else {
                Logger.d("empty account id in profile, should not happen")
            }
        } else {
            // get mine
            isMyId = true
            if let myId = UserManager.shared().currentUser.accountId {
                profile = UserManager.shared().getProfile(withId: myId, listener: self)
                //UserManager.shared().getRedeemState(listener: self)
                setupSelfView()
            }
        }
        updateUi()
        
        // update notice read/unread status locally
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            self.setNoticeImage(with: readStatus)
        }
    }
    
    func setupSelfView() {
        naviBar.setTitle("member_page".localized)
        naviBar.setBackgroundColor(UiUtility.colorBackgroundWhite)
        
        naviTopCOns.constant = UIApplication.shared.statusBarFrame.size.height
        
        if webview == nil {
            let configuration = WKWebViewConfiguration()
            configuration.ignoresViewportScaleLimits = true
            configuration.suppressesIncrementalRendering = true
            configuration.allowsInlineMediaPlayback = true
            configuration.allowsAirPlayForMediaPlayback = false
            configuration.allowsPictureInPictureMediaPlayback = true
            configuration.mediaTypesRequiringUserActionForPlayback = .all
            configuration.setURLSchemeHandler(CustomSchemeHandler(), forURLScheme: Config.redirectScheme)
            let preferences = WKPreferences()
            preferences.javaScriptEnabled = true
            configuration.preferences = preferences
            webview = WKWebView(frame: self.view.bounds, configuration: configuration)
            webview!.scrollView.bounces = false
            webview!.navigationDelegate = self
            self.view.addSubview(webview!)
            webview!.snp.makeConstraints { make in
                make.leading.trailing.bottom.equalToSuperview()
                make.top.equalTo(self.naviBar.snp.bottom)
            }
        }
        
//        if deeplink == .memberCoupon {
//            let url = URL(string: "\(Config.urlMemberCenterCouponList)?token=\(UserManager.shared().getUserToken()!)&AccountId=\(UserManager.shared().currentUser.accountId!)")!
//            let urlRequest = URLRequest(url: url)
//            webview!.load(urlRequest)
//            deeplink = nil
//        } else if deeplink == .memberLevel {
//            let url = URL(string: "\(Config.urlMemberCenterLevel)?token=\(UserManager.shared().getUserToken()!)&AccountId=\(UserManager.shared().currentUser.accountId!)")!
//            let urlRequest = URLRequest(url: url)
//            webview!.load(urlRequest)
//            deeplink = nil
//        } else {
//            let url = URL(string: "\(Config.urlMemberCenter)?token=\(UserManager.shared().getUserToken()!)&AccountId=\(UserManager.shared().currentUser.accountId!)")!
//            let urlRequest = URLRequest(url: url)
//            webview!.load(urlRequest)
//            deeplink = nil
//        }
        addFloatingBtnView()
    }
    
    func loadMemberCoupon() {
        let url = URL(string: "\(Config.urlMemberCenterCouponList)?token=\(UserManager.shared().getUserToken()!)&AccountId=\(UserManager.shared().currentUser.accountId!)")!
        let urlRequest = URLRequest(url: url)
        webview!.load(urlRequest)
    }
    
    func loadMemberLevel() {
        let url = URL(string: "\(Config.urlMemberCenterLevel)?token=\(UserManager.shared().getUserToken()!)&AccountId=\(UserManager.shared().currentUser.accountId!)")!
        let urlRequest = URLRequest(url: url)
        webview!.load(urlRequest)
    }
    
    func loadMember() {
        let url = URL(string: "\(Config.urlMemberCenter)?token=\(UserManager.shared().getUserToken()!)&AccountId=\(UserManager.shared().currentUser.accountId!)")!
        let urlRequest = URLRequest(url: url)
        webview!.load(urlRequest)
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        if isPersonRoot {
            self.tabControllerDelegate?.menuClicked()
        } else {
            back()
        }
    }
    
    override func rightButtonClicked(_ button: UIButton) {
        self.tabControllerDelegate?.notificationClicked()
    }
    
    override func rightButton1Clicked(_ button: UIButton) {
        Analytics.logEvent("setting", parameters: nil)
        self.tabControllerDelegate?.goSetting()
    }
    
    override func rightButton2Clicked(_ button: UIButton) {
        Analytics.logEvent("qr_code", parameters: nil)
        containerQrcode.isHidden = false
        UserManager.shared().getQrcode(listener: self)
    }
    
    override func updateUnreadCount(notification: NSNotification) {
        //        Logger.d("person updateUnreadCount")
        // update notice read/unread status locally
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            self.setNoticeImage(with: readStatus)
        }
    }
    
    
    
    // MARK: user protocol
    
    override func didStartFollow() {
        showHud()
    }
    
    override func didFinishFollow(success: Bool, accountId: String, set: Bool) {
        dismissHud()
        
        if success {
            for n in newsList {
                if let na = n.accountId {
                    if na == accountId {
                        if set {
                            n.isFollow = 1
                        } else {
                            n.isFollow = 0
                        }
                    }
                }
            }
            
            NewsManager.shared().listIsDirtyRemote = true
            self.relationFollowing = UserManager.shared().getFollowingRelation(to: self.profile, listener: self)
            updateUi()
        }
    }
    
    override func didStartGetRelation() {}
    override func didFinishGetRelation(success: Bool, isFollower: Bool, relations: [Relation]?) {
        // we now only care about following relation to specific user
        if success && !isFollower {
            if let relations = relations {
                if relations.count != 0 {
                    if let profile = self.profile {
                        self.relationFollowing = UserManager.shared().getFollowingRelation(to: profile, listener: nil)
                        updateUi()
                    }
                }
            }
        }
    }
    
    
    
    // MARK: news protocol
    
    override func didStartGetMemberContent() {
        showHud()
    }
    
    override func didFinishGetMemberContent(success: Bool, newsList: [News]?) {
        dismissHud()
        
        if success {
            contentIsDirtyRemote = false
            if let newsList = newsList {
                if contentKind == .post {
                    couldShowPostEmpty = true
                } else if contentKind == .collection {
                    couldShowCollectionEmpty = true
                } else if contentKind == .all {
                    couldShowOfficialAllEmpty = true
                } else if contentKind == .new {
                    couldShowOfficialMessageEmpty = true
                } else if contentKind == .activity {
                    couldShowOfficialActivityEmpty = true
                }
                self.newsList = newsList
                updateUi()
            }
        }
    }
    
    override func didStartLikeNews() {
        showHud()
    }
    
    override func didFinishLikeNews(success: Bool, news: News, set: Bool) {
        dismissHud()
        
        if success {
            var index = 0
            for n in newsList {
                if n.newsId! == news.newsId! {
                    if set {
                        n.likeStatus = 1
                        n.likeAmount = n.likeAmount + 1
                    } else {
                        n.likeStatus = 0
                        n.likeAmount = n.likeAmount - 1
                    }
                    updateUi()
                    break
                }
                index += 1
            }
        }
    }
    
    override func didStartTrackNews() {
        showHud()
    }
    
    override func didFinishTrackNews(success: Bool, news: News, set: Bool) {
        dismissHud()
        
        if success {
            if self.profile.isMine() && contentKind == .collection {
                var newArray: [News] = []
                for n in newsList {
                    if n.newsId! != news.newsId! {
                        newArray.append(n)
                    }
                }
                newsList = newArray
                updateUi()
            } else {
                for n in newsList {
                    if n.newsId! == news.newsId! {
                        if set {
                            n.bookmarkStatus = 1
                            n.bookmarkAmount = n.bookmarkAmount + 1
                        } else {
                            n.bookmarkStatus = 0
                            n.bookmarkAmount = n.bookmarkAmount - 1
                        }
                        updateUi()
                        break
                    }
                }
            }
        }
    }
    
    override func didStartReportNews() {
        showHud()
    }
    
    override func didFinishReportNews(success: Bool, news: News, type: Int) {
        dismissHud()
        
        if success {
            self.tabControllerDelegate?.goFinishReport()
        }
    }
    
    override func didStartDeletePost() {
        showHud()
    }
    
    override func didFinishDeletePost(success: Bool, news: News?) {
        dismissHud()
        
        if success {
            // TODO: you can do it better
            NewsManager.shared().getMemberContent(memberProfile: self.profile, contentKind: contentKind, listener: self)
        }
    }
    
    
    
    // MARK: profile protocol
    
    override func didStartGetProfile() {
        showHud()
    }
    
    override func didFinishGetProfile(success: Bool, profile: Profile?) {
        dismissHud()
        
        if success {
            if let p = profile {
                self.profile = p
                //                Logger.d("profile: \(p)")
                //                updateUi()
                
                // get content from remote
                NewsManager.shared().getMemberContent(memberProfile: self.profile, contentKind: contentKind, listener: self)
                
                if !p.isMine() {
                    if p.offical == nil || p.offical! != 2 {
                        // get relation (from remote maybe)
                        self.relationFollowing = UserManager.shared().getFollowingRelation(to: p, listener: self)
                    }
                }
                
                updateUi()
            }
        }
    }
    
    override func didStartGetRedeemState() {
        //showHud()
    }
    
    override func didFinishGetRedeemState(success: Bool, taskCount: Int?, totalPoints: Int?) {
        //dismissHud()
        
        if success {
            if  let tc = taskCount,
                let tp = totalPoints {
                redeemTaskCount = tc
                redeemTotalPoints = tp
                
                updateCardPersonRedeem()
            }
        }
    }
    
    
    
    // MARK: image picker controller delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.editedImage] as? UIImage else {
            Logger.d("No image found")
            return
        }
        
        // print out the image size as a test
        Logger.d("image size: \(image.size)")
        
        UiUtility.goEditPost(from: self, post: Post.init(with: [image]), contentDelegate: nil, photoSourceType: .none)
    }
    
    
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if profile == nil {
            return 0
        }
        
        switch Section.init(rawValue: section)! {
        case .paddingProfileBackground:
            return 1
        case .tab:
            return 1
        case .feed:
            return newsList.count
        case .empty:
            switch contentKind {
            case .post:
                return newsList.count == 0 && couldShowPostEmpty ? 1 : 0
            case .collection:
                return newsList.count == 0 && couldShowCollectionEmpty ? 1 : 0
            case .all:
                return newsList.count == 0 && couldShowOfficialAllEmpty ? 1 : 0
            case .new:
                return newsList.count == 0 && couldShowOfficialMessageEmpty ? 1 : 0
            case .activity:
                return newsList.count == 0 && couldShowOfficialActivityEmpty ? 1 : 0
                //            default:
                //                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Section.init(rawValue: indexPath.section)! {
        case .paddingProfileBackground:
            //            return UiUtility.adaptiveSize(size: 219)
            //            return UiUtility.adaptiveSize(size: 129)
            //            return UiUtility.adaptiveSize(size: 219 - 129)
            //            return UiUtility.adaptiveSize(size: 316)
            
            if profile.offical != nil && profile.offical! == Role.official.rawValue {
                // official
                return 44 + UiUtility.adaptiveSize(size: 237)
            } else {
                // all users including me
                return 44 + UiUtility.adaptiveSize(size: 252)
            }
        case .tab:
            if let profile = profile {
                let window = UIApplication.shared.keyWindow
                let topPadding = window?.safeAreaInsets.top ?? 0
                if profile.offical != nil && profile.offical! == Role.official.rawValue {
                    // official
                    //                    return UiUtility.adaptiveSize(size: 143 - 40 - 24 - 24 + 20) + topPadding
                    return UiUtility.adaptiveSize(size: 44)
                } else {
                    // all users including me
                    if profile.isMine() {
                        //                        return UiUtility.adaptiveSize(size: 143 - 40 - 24 + 20 + cardHeightDifference) + topPadding
                        return UiUtility.adaptiveSize(size: 44)
                    } else {
                        //                        return UiUtility.adaptiveSize(size: 143 - 40 - 24 - 40 + 20 + cardHeightDifference) + topPadding
                        return 0
                    }
                }
            } else {
                return 0
            }
        case .feed:
            let width = UiUtility.adaptiveSize(size: 375 - 20 - 20)
            let font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))!
            let maxContentHeight: CGFloat = "line_4_text".localized.height(withConstrainedWidth: width, font: font)
            
            var h: CGFloat = 0
            h += UiUtility.adaptiveSize(size: 70) // height before content
            
            let news = newsList[indexPath.row]
            if !Utility.isEmpty(news.content) {
                h += min(news.content!.height(withConstrainedWidth: width, font: font), maxContentHeight)
            }
            
            h += UiUtility.adaptiveSize(size: 36) // date
            h += UiUtility.adaptiveSize(size: 375) // image
            h += UiUtility.adaptiveSize(size: 50) // footer
            h += UiUtility.adaptiveSize(size: 6) // separator
            return h
        case .empty:
            return UiUtility.adaptiveSize(size: 186)
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightsDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let h:CGFloat = cellHeightsDictionary[indexPath] {
            return h
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        switch Section.init(rawValue: indexPath.section)! {
        case .paddingProfileBackground:
            cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath)
            cell.selectionStyle = .none
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(profileBackgroundClicked(_:)))
            cell.addGestureRecognizer(tapGesture)
        case .tab:
            if let profile = profile {
                if profile.offical != nil && profile.offical! == Role.official.rawValue {
                    // official
                    cell = tableView.dequeueReusableCell(withIdentifier: "OfficialTabCell", for: indexPath)
                    cell.backgroundColor = UiUtility.colorBackgroundWhite
                    cell.selectionStyle = .none
                    
                    let tab0: UIButton = cell.viewWithTag(1) as! UIButton
                    let tab1: UIButton = cell.viewWithTag(2) as! UIButton
                    let tab2: UIButton = cell.viewWithTag(3) as! UIButton
                    let background: UIImageView = cell.viewWithTag(4) as! UIImageView
                    
                    switch contentKind {
                    case .all:
                        background.image = UIImage.init(named: "tab_official_1_3")
                    case .new:
                        background.image = UIImage.init(named: "tab_official_2_3")
                    case .activity:
                        background.image = UIImage.init(named: "tab_official_3_3")
                    default:
                        background.image = UIImage.init(named: "tab_official_1_3")
                    }
                    
                    tab0.addTarget(self, action:#selector(officialTab0Clicked(_:)), for: .touchUpInside)
                    tab1.addTarget(self, action:#selector(officialTab1Clicked(_:)), for: .touchUpInside)
                    tab2.addTarget(self, action:#selector(officialTab2Clicked(_:)), for: .touchUpInside)
                } else {
                    // all users including me
                    if profile.isMine() {
                        cell = tableView.dequeueReusableCell(withIdentifier: "TabCell", for: indexPath)
                        cell.backgroundColor = UiUtility.colorBackgroundWhite
                        cell.selectionStyle = .none
                        
                        let tab0: UIButton = cell.viewWithTag(1) as! UIButton
                        let tab1: UIButton = cell.viewWithTag(2) as! UIButton
                        let background: UIImageView = cell.viewWithTag(3) as! UIImageView
                        
                        switch contentKind {
                        case .post:
                            background.image = UIImage.init(named: "my_vw_tab_0")
                        case .collection:
                            background.image = UIImage.init(named: "my_vw_tab_1")
                        default:
                            background.image = UIImage.init(named: "my_vw_tab_0")
                        }
                        
                        tab0.addTarget(self, action:#selector(tab0Clicked(_:)), for: .touchUpInside)
                        tab1.addTarget(self, action:#selector(tab1Clicked(_:)), for: .touchUpInside)
                    } else {
                        cell = tableView.dequeueReusableCell(withIdentifier: "NoTabCell", for: indexPath)
                        cell.backgroundColor = UiUtility.colorBackgroundWhite
                        cell.selectionStyle = .none
                    }
                }
            }
        case .feed:
            cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath)
            cell.backgroundColor = UiUtility.colorBackgroundWhite
            cell.selectionStyle = .none
            
            let feedItem: FeedItemView = cell.viewWithTag(1) as! FeedItemView
            for constraint in cell.contentView.constraints {
                if constraint.identifier == "marginBottom" {
                    constraint.constant = UiUtility.adaptiveSize(size: 6)
                }
            }
            
            let news = newsList[indexPath.row]
            feedItem.setupUi(with: news, isList: false, inPerson: true)
            
            feedItem.buttonUser.addTarget(self, action: #selector(userClicked(_:)), for: .touchUpInside)
            feedItem.buttonFollow.addTarget(self, action: #selector(followClicked(_:)), for: .touchUpInside)
            feedItem.buttonMore.addTarget(self, action: #selector(moreClicked(_:)), for: .touchUpInside)
            feedItem.buttonLike.addTarget(self, action: #selector(likeClicked(_:)), for: .touchUpInside)
            feedItem.buttonLikeList.addTarget(self, action: #selector(likersClicked(_:)), for: .touchUpInside)
            feedItem.buttonTrack.addTarget(self, action: #selector(trackClicked(_:)), for: .touchUpInside)
            
            //            feedItem.content.didTouch = { (touchResult) in
            //                switch touchResult.state {
            //                case .began:
            //                    Logger.d("began")
            //                //                    Logger.d("began, \(touchResult)")
            //                case .ended:
            //                    Logger.d("ended")
            //                    if let linkResult = touchResult.linkResult {
            //                        if linkResult.detectionType == .url {
            //                            Logger.d("text: \(linkResult.text)")
            //                        }
            //                    }
            //                //                    Logger.d("ended, \(touchResult)")
            //                default:
            //                    break
            //                }
        //            }
        case .empty:
            cell = tableView.dequeueReusableCell(withIdentifier: "EmptyMsgCell", for: indexPath)
            cell.backgroundColor = UiUtility.colorBackgroundWhite
            cell.selectionStyle = .none
            
            let msg: UILabel = cell.viewWithTag(1) as! UILabel
            msg.textColor = UIColor(rgb: 0x4A4A4A)
            msg.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
            switch contentKind {
            case .post:
                msg.text = "no_post_yet".localized
            case .collection:
                msg.text = "no_collection_yet".localized
            case .all:
                msg.text = "no_post_yet".localized
            case .new:
                msg.text = "no_new_message".localized
            case .activity:
                msg.text = "no_new_activity".localized
                //            default:
                //                msg.text = nil
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        Logger.d("Person You tapped cell number \(indexPath.row).")
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch Section.init(rawValue: indexPath.section)! {
        case .feed:
            if let tabControllerDelegate = self.tabControllerDelegate {
                tabControllerDelegate.goNewsDetail(news: newsList[indexPath.row])
            } else {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedDetailViewController") as! FeedDetailViewController
                vc.news = newsList[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }
        default:
            break
        }
    }
    
    
    
    // MARK: UserProtocol
    
    override func didFinishGetQrcode(success: Bool, qrcodeImage: UIImage?) {
        super.didFinishGetQrcode(success: success, qrcodeImage: qrcodeImage)
        
        if success && !containerQrcode.isHidden {
            imageQrcode.image = qrcodeImage
        }
    }
    
    
    
    // MARK: car protocol
    
    override func didStartGetCar() {
        showHud()
    }
    
    override func didFinishGetCar(success: Bool, cars: [Car]) {
        dismissHud()
        
        if success {
            if cars.count == 0 {
                self.tabControllerDelegate?.addPostClicked()
            } else {
                // add new post
                UiUtility.pickImage(from: self, delegate: self, allowsEditing: true)
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    private func blurEffect(bg: UIImageView) {
        let context = CIContext(options: nil)
        
        let currentFilter = CIFilter(name: "CIGaussianBlur")
        let beginImage = CIImage(image: bg.image!)
        currentFilter!.setValue(beginImage, forKey: kCIInputImageKey)
        currentFilter!.setValue(9, forKey: kCIInputRadiusKey)
        
        let cropFilter = CIFilter(name: "CICrop")
        cropFilter!.setValue(currentFilter!.outputImage, forKey: kCIInputImageKey)
        cropFilter!.setValue(CIVector(cgRect: beginImage!.extent), forKey: "inputRectangle")
        
        let output = cropFilter!.outputImage
        let cgimg = context.createCGImage(output!, from: output!.extent)
        let processedImage = UIImage(cgImage: cgimg!)
        bg.image = processedImage
    }
    
    
    private func updateCardPerson() {
        if let profile = self.profile {
            cardName.text = profile.nickname
            
            cardCarIcon.isHidden = true
            cardCarModel.isHidden = true
            cardCarIcon1.isHidden = true
            cardCarModel1.isHidden = true
            cardCarIcon2.isHidden = true
            cardCarModel2.isHidden = true
            
            cardTitleReward.isHidden = true
            cardTitlePoint.isHidden = true
            cardValueReward.isHidden = true
            cardValuePoint.isHidden = true
            
            if let ownModels = profile.ownModel {
                //                Logger.d("ownModels: \(ownModels)")
                if ownModels.count != 0 {
                    //                    let orderedOwnModels = ownModels.reversed()
                    if ownModels.count == 1 {
                        cardCarIcon.isHidden = false
                        cardCarModel.isHidden = false
                        
                        cardCarModel.text = ownModels[0]
                    } else if ownModels.count == 2 {
                        cardCarIcon.isHidden = false
                        cardCarModel.isHidden = false
                        //                        cardCarIcon1.isHidden = false
                        cardCarModel1.isHidden = false
                        
                        cardCarModel.text = ownModels[0]
                        cardCarModel1.text = ownModels[1]
                    } else if ownModels.count == 3 {
                        cardCarIcon.isHidden = false
                        cardCarModel.isHidden = false
                        //                        cardCarIcon1.isHidden = false
                        cardCarModel1.isHidden = false
                        //                        cardCarIcon2.isHidden = false
                        cardCarModel2.isHidden = false
                        
                        cardCarModel.text = ownModels[0]
                        cardCarModel1.text = ownModels[1]
                        cardCarModel2.text = ownModels[2]
                    }
                }
            }
            
            cardSubtitle.text = profile.brief
            
            cardValueFollowing.text = "\(profile.followAmount)"
            cardValueFollower.text = "\(profile.followerAmount)"
            
            if let coverURL = profile.coverURL {
                let url = URL(string: coverURL)
                //                profileBackground.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_background"))
                //                profileBackground.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_background"), options: nil, progressBlock: nil) { (image, error, cacheType, url) in
                //                    self.blurEffect(bg: self.profileBackground)
                //                }
                profileBackground.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cacheType, url) in
                    if let _ = error {
                        self.profileBackground.image = UIImage.init(named: "default_profile_background")
                    }
                    
                    self.blurEffect(bg: self.profileBackground)
                }
            } else {
                profileBackground.image = UIImage.init(named: "default_profile_background")
                blurEffect(bg: profileBackground)
            }
            
            
            
            
            if let stickerURL = profile.stickerURL {
                let url = URL(string: stickerURL)
                userImage.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_photo"))
            } else {
                userImage.image = UIImage.init(named: "default_profile_photo")
            }
            
            checkImage.isHidden = true
//            if let level = profile.level {
//                if level > 2 {
//                    checkImage.isHidden = false
//                    checkImage.image = UIImage.init(named: "check_golden_large")
//                } else if level > 1 {
//                    checkImage.isHidden = false
//                    checkImage.image = UIImage.init(named: "check_silver_large")
//                } else if level > 0 {
//                    checkImage.isHidden = false
//                    checkImage.image = UIImage.init(named: "check_large")
//                }
//            }
            
            if profile.isMine() {
                cardButton.setImage(UIImage.init(named: "edit_profile_transparent"), for: .normal)
                
                cardTitleReward.isHidden = false
                cardTitlePoint.isHidden = false
                cardValueReward.isHidden = false
                cardValuePoint.isHidden = false
                cardValueReward.text = "\(redeemTaskCount)"
                cardValuePoint.text = "\(redeemTotalPoints)"
            } else {
                if let relation = relationFollowing {
                    if relation.isFollow == 1 {
                        cardButton.setImage(UIImage.init(named: "following"), for: .normal)
                    } else {
                        cardButton.setImage(UIImage.init(named: "follow"), for: .normal)
                    }
                } else {
                    // unknown
                    cardButton.setImage(UIImage.init(named: "follow"), for: .normal)
                }
                
                
            }
            
            
            /*
             @IBOutlet weak var cardCarIcon: UIImageView!
             @IBOutlet weak var cardCarModel: UILabel!
             @IBOutlet weak var cardCarIcon1: UIImageView!
             @IBOutlet weak var cardCarModel1: UILabel!
             @IBOutlet weak var cardCarIcon2: UIImageView!
             @IBOutlet weak var cardCarModel2: UILabel!
             @IBOutlet weak var cardSubtitle: UILabel!
             @IBOutlet weak var cardTitleFollowing: UILabel!
             @IBOutlet weak var cardTitleFollower: UILabel!
             @IBOutlet weak var cardValueFollowing: UILabel!
             @IBOutlet weak var cardValueFollower: UILabel!
             @IBOutlet weak var cardButton: UIButton!
             @IBOutlet weak var constraintContainerCardHeight: NSLayoutConstraint!
             @IBOutlet weak var constraintCardNameTop: NSLayoutConstraint!
             @IBOutlet weak var constraintCardCarIconTop: NSLayoutConstraint!
             @IBOutlet weak var constraintCardModelTop: NSLayoutConstraint!
             @IBOutlet weak var constraintCardCarIcon1Top: NSLayoutConstraint!
             @IBOutlet weak var constraintCardModel1Top: NSLayoutConstraint!
             @IBOutlet weak var constraintCardCarIcon2Top: NSLayoutConstraint!
             @IBOutlet weak var constraintCardMode2lTop: NSLayoutConstraint!
             @IBOutlet weak var constraintCardSubtitleTop: NSLayoutConstraint!
             */
            let offsetStart: CGFloat = 54
            var offset = offsetStart
            let spacingNameModel: CGFloat = 6
            
            // card name
            //            constraintCardNameTop.constant = UiUtility.adaptiveSize(size: offset)
            if let cnt = cardName.text {
                let offsetName: CGFloat = min(cnt.height(withConstrainedWidth: cardNameWidth, font: cardName.font), maxCardNameHeight)
                offset = offset + offsetName
            }
            
            // card cars
            let diffHeightModelIconName: CGFloat = 3
            let spacingModelModel: CGFloat = 0
            //            let offsetStartCars: CGFloat = offsetStart + offsetName + spacingNameModel//orig: 82
            offset = offset + spacingNameModel
            //            // card car 0
            //            if !cardCarIcon.isHidden {
            //                constraintCardCarIconTop.constant = UiUtility.adaptiveSize(size: offset)
            //                constraintCardModelTop.constant = UiUtility.adaptiveSize(size: offset + diffHeightModelIconName)
            //
            //                let offsetModel: CGFloat = min(cardCarModel.text!.height(withConstrainedWidth: cardModelWidth, font: cardCarModel.font), maxCardModelHeight)
            //                offset = offset + diffHeightModelIconName + offsetModel
            //            }
            //            // card car 1
            //            if !cardCarIcon1.isHidden {
            //                offset = offset + spacingModelModel
            //                constraintCardCarIcon1Top.constant = UiUtility.adaptiveSize(size: offset)
            //                constraintCardModel1Top.constant = UiUtility.adaptiveSize(size: offset + diffHeightModelIconName)
            //
            //                let offsetModel1: CGFloat = min(cardCarModel1.text!.height(withConstrainedWidth: cardModelWidth, font: cardCarModel.font), maxCardModelHeight)
            //                offset = offset + diffHeightModelIconName + offsetModel1
            //            }
            //            // card car 2
            //            if !cardCarIcon2.isHidden {
            //                offset = offset + spacingModelModel
            //                constraintCardCarIcon2Top.constant = UiUtility.adaptiveSize(size: offset)
            //                constraintCardMode2lTop.constant = UiUtility.adaptiveSize(size: offset + diffHeightModelIconName)
            //
            //                let offsetModel2: CGFloat = min(cardCarModel2.text!.height(withConstrainedWidth: cardModelWidth, font: cardCarModel.font), maxCardModelHeight)
            //                offset = offset + diffHeightModelIconName + offsetModel2
            //            }
            
            // card subtitle
            let spacingModelSubtitle: CGFloat = 10
            offset = offset + spacingModelSubtitle
            //            constraintCardSubtitleTop.constant = UiUtility.adaptiveSize(size: offset)
            var offsetSubtitle: CGFloat = 0
            if let _ = cardSubtitle.text {
                offsetSubtitle = min(cardSubtitle.text!.height(withConstrainedWidth: cardSubtitleWidth, font: cardSubtitle.font), maxCardSubtitleHeight)
            }
            
            // card container height
            //            constraintContainerCardHeight.constant = UiUtility.adaptiveSize(size: 204) // orig
            let containerHeight: CGFloat = max(204, 47 + offset + offsetSubtitle + 12)
            cardHeightDifference = containerHeight - 204
            //            constraintContainerCardHeight.constant = UiUtility.adaptiveSize(size: containerHeight)
        }
        
        
        
        
    }
    
    private func updateCardPersonRedeem() {
        cardValueReward.text = "\(redeemTaskCount)"
        cardValuePoint.text = "\(redeemTotalPoints)"
    }
    
    private func updateCardOfficial() {
        if let profile = self.profile {
            cardOfficialName.text = profile.nickname
            cardOfficialSubtitle.text = profile.brief
            
            if let coverURL = profile.coverURL {
                let url = URL(string: coverURL)
                //                profileBackground.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_background"))
                //                profileBackground.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_background"), options: nil, progressBlock: nil) { (image, error, cacheType, url) in
                //                    self.blurEffect(bg: self.profileBackground)
                //                }
                profileBackground.kf.setImage(with: url, placeholder: nil, options: nil, progressBlock: nil) { (image, error, cacheType, url) in
                    if let _ = error {
                        self.profileBackground.image = UIImage.init(named: "default_profile_background")
                    }
                    
                    self.blurEffect(bg: self.profileBackground)
                }
            } else {
                profileBackground.image = UIImage.init(named: "default_profile_background")
                blurEffect(bg: profileBackground)
            }
            
            if let stickerURL = profile.stickerURL {
                let url = URL(string: stickerURL)
                officialImage.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_photo"))
            } else {
                officialImage.image = UIImage.init(named: "default_profile_photo")
            }
        }
        
        // card container height
        //        constraintContainerCardOfficialHeight.constant = UiUtility.adaptiveSize(size: 180)
    }
    
    private func updateUi() {
        if let profile = profile {
            if profile.offical != nil && profile.offical! == Role.official.rawValue {
                // official
                updateCardOfficial()
                containerCard.isHidden = true
                containerCardOfficial.isHidden = false
                //                containerCardOfficial.removeFromSuperview()
                //                tableView.backgroundView = containerCardOfficial
            } else {
                // all users including me
                updateCardPerson()
                containerCard.isHidden = false
                containerCardOfficial.isHidden = true
                //                containerCard.removeFromSuperview()
                //                tableView.backgroundView = containerCard
            }
            
            tableView.reloadData()
        }
    }
    
    @objc private func tab0Clicked(_ sender: Any) {
        contentKind = .post
        
        //        let indexPath = IndexPath(item: 0, section: Section.tab.rawValue)
        //        tableView.reloadRows(at: [indexPath], with: .none)
        
        NewsManager.shared().getMemberContent(memberProfile: self.profile, contentKind: contentKind, listener: self)
    }
    
    @objc private func tab1Clicked(_ sender: Any) {
        contentKind = .collection
        
        //        let indexPath = IndexPath(item: 0, section: Section.tab.rawValue)
        //        tableView.reloadRows(at: [indexPath], with: .none)
        
        NewsManager.shared().getMemberContent(memberProfile: self.profile, contentKind: contentKind, listener: self)
    }
    
    @objc private func profileBackgroundClicked(_ sender: UITapGestureRecognizer) {
        if let profile = profile {
            if profile.offical != nil && profile.offical! == Role.official.rawValue {
                // official
                let buttonPosition: CGPoint = sender.location(in: containerCardOfficial)
                let v = containerCardOfficial.hitTest(buttonPosition, with: nil)
                if let v = v as? UIButton {
                    v.sendActions(for: .touchUpInside)
                }
            } else {
                // all users including me
                let buttonPosition: CGPoint = sender.location(in: containerCard)
                let v = containerCard.hitTest(buttonPosition, with: nil)
                if let v = v as? UIButton {
                    v.sendActions(for: .touchUpInside)
                }
            }
        }
        
    }
    
    @objc private func officialTab0Clicked(_ sender: Any) {
        contentKind = .all
        
        //        let indexPath = IndexPath(item: 0, section: Section.tab.rawValue)
        //        tableView.reloadRows(at: [indexPath], with: .none)
        
        Analytics.logEvent("view_vw_profile", parameters: nil)
        NewsManager.shared().getMemberContent(memberProfile: self.profile, contentKind: contentKind, listener: self)
    }
    
    @objc private func officialTab1Clicked(_ sender: Any) {
        contentKind = .new
        
        //        let indexPath = IndexPath(item: 0, section: Section.tab.rawValue)
        //        tableView.reloadRows(at: [indexPath], with: .none)
        
        Analytics.logEvent("view_vw_news", parameters: nil)
        NewsManager.shared().getMemberContent(memberProfile: self.profile, contentKind: contentKind, listener: self)
    }
    
    @objc private func officialTab2Clicked(_ sender: Any) {
        contentKind = .activity
        
        //        let indexPath = IndexPath(item: 0, section: Section.tab.rawValue)
        //        tableView.reloadRows(at: [indexPath], with: .none)
        
        Analytics.logEvent("view_vw_event", parameters: nil)
        NewsManager.shared().getMemberContent(memberProfile: self.profile, contentKind: contentKind, listener: self)
    }
    
    @IBAction func AddClicked() {
        Analytics.logEvent("add_post", parameters: ["screen_name": "screen_my" as NSObject])
        CarManager.shared().getCars(remotely: true, listener: self)
    }
    
    @IBAction func closeQrcodeClicked() {
        containerQrcode.isHidden = true
    }
    
    @IBAction func carButtonClicked() {
        if profile.isMine() {
            // edit
            self.tabControllerDelegate?.goEditProfile()
        } else {
            // follow/unfollow
            
            if let relation = relationFollowing {
                if relation.isFollow == 1 {
                    Logger.d("unfollow")
                    UserManager.shared().follow(accountId: profile.accountId!, set: false, listener: self)
                } else {
                    Logger.d("follow")
                    UserManager.shared().follow(accountId: profile.accountId!, set: true, listener: self)
                }
            } else {
                // unknown
                Logger.d("follow")
                UserManager.shared().follow(accountId: profile.accountId!, set: true, listener: self)
            }
        }
    }
    
    @IBAction func profilePhotoClicked() {
        if profile.isMine() {
            if let tabControllerDelegate = self.tabControllerDelegate {
                tabControllerDelegate.goBrowseMemberLevel()
            } else {
                UiUtility.goBrowseMemberLevel(controller: self)
            }
        }
    }
    
    @IBAction func officialFbClicked(_ sender: Any) {
        if let link = URL(string: Config.urlFacebook) {
            UIApplication.shared.open(link)
        }
    }
    
    @IBAction func officialInsClicked(_ sender: Any) {
        if let link = URL(string: Config.urlInstagram) {
            UIApplication.shared.open(link)
        }
    }
    
    @IBAction func officialYoutubeClicked(_ sender: Any) {
        if let link = URL(string: Config.urlYoutube) {
            UIApplication.shared.open(link)
        }
    }
    
    
    
    @objc private func userClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            let news: News = newsList[indexPath.row]
            if let nAccountId = news.accountId, let pAccountId = profile.accountId {
                if nAccountId == pAccountId {
                    //                    Logger.d("same profile, skip")
                    return
                }
                UiUtility.goBrowse(profile: Profile.init(with: news), controller: self)
            }
        }
    }
    
    @objc private func followClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            //            Logger.d("click follow at row \(String(describing: indexPath.row))")
            let news: News = newsList[indexPath.row]
            if let accountId = news.accountId {
                UserManager.shared().follow(accountId: accountId, set: true, listener: self)
            }
        }
    }
    
    @objc private func moreClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            //            Logger.d("click like at row \(String(describing: indexPath.row))")
            let news: News = newsList[indexPath.row]
            if news.offical != nil && news.offical! == Role.official.rawValue {
                // official
                UiUtility.moreMenu(from: self, type: 0, news: news, contentDelegate: self)
            } else if let newsAccoundId = news.accountId {
                if newsAccoundId == UserManager.shared().currentUser.accountId! {
                    // mine
                    UiUtility.moreMenu(from: self, type: 1, news: news, contentDelegate: self)
                } else {
                    // others
                    UiUtility.moreMenu(from: self, type: 2, news: news, contentDelegate: self)
                }
            }
        }
    }
    
    @objc private func likeClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            //            Logger.d("click like at row \(String(describing: indexPath.row))")
            let news: News = newsList[indexPath.row]
            NewsManager.shared().like(news: news, set: (news.likeStatus == 0), listener: self)
        }
    }
    
    @objc private func likersClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            //            Logger.d("click track at row \(String(describing: indexPath.row))")
            let news: News = newsList[indexPath.row]
            self.tabControllerDelegate?.goBrowseLikers(news: news)
        }
    }
    
    @objc private func trackClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            //            Logger.d("click track at row \(String(describing: indexPath.row))")
            let news: News = newsList[indexPath.row]
            NewsManager.shared().track(news: news, set: (news.bookmarkStatus == 0), listener: self)
        }
    }
    
    @IBAction private func goBrowseFollower(_ sender: UIButton) {
        Analytics.logEvent("view_following_list", parameters: nil)
        if let tabControllerDelegate = self.tabControllerDelegate {
            tabControllerDelegate.goBrowseFollow(of: profile, isFollower: true)
        } else {
            UiUtility.goBrowseFollow(of: profile, isFollower: true, controller: self)
        }
    }
    
    @IBAction private func goBrowseFollowing(_ sender: UIButton) {
        Analytics.logEvent("view_follow_list", parameters: nil)
        if let tabControllerDelegate = self.tabControllerDelegate {
            tabControllerDelegate.goBrowseFollow(of: profile, isFollower: false)
        } else {
            UiUtility.goBrowseFollow(of: profile, isFollower: false, controller: self)
        }
    }
    
    @IBAction func goBrowseReward(_ sender: Any) {
        if let tabControllerDelegate = self.tabControllerDelegate {
            tabControllerDelegate.goBrowseReward()
        } else {
            UiUtility.goBrowseReward(controller: self)
        }
    }
    
    @IBAction func goBrowsePoint(_ sender: Any) {
        if let tabControllerDelegate = self.tabControllerDelegate {
            tabControllerDelegate.goBrowsePoint()
        } else {
            UiUtility.goBrowsePoint(controller: self)
        }
    }
    
    // MARK: - webview
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let urll = navigationAction.request.url!.absoluteString.removingPercentEncoding!
        print("url: \(urll)")
        
        if urll == "about:blank" {
            //decisionHandler(.cancel)
            decisionHandler(.allow)
            return
        }
        
        webview!.snp.removeConstraints()
        if urll.contains("member.html") {
            showFloatingBtn(show: true)
            self.webviewCloseView.isHidden = true
            webview!.snp.makeConstraints { make in
                make.leading.trailing.bottom.equalToSuperview()
                make.top.equalTo(self.naviBar.snp.bottom)
            }
        } else {
            showFloatingBtn(show: false)
            self.webviewCloseView.isHidden = false
            webview!.snp.makeConstraints { make in
                make.leading.trailing.bottom.equalToSuperview()
                make.top.equalTo(self.webviewCloseView.snp.bottom)
            }
        }
        
        if navigationAction.request.url?.scheme == "tel" {
            UIApplication.shared.open(navigationAction.request.url!)
            decisionHandler(.cancel)
        } else {
            if navigationAction.targetFrame == nil {
                webView.load(navigationAction.request)
            }
            decisionHandler(.allow)
        }
    }
    
    @IBAction func resetBtnPressed(_ sender: Any) {
        let url = URL(string: "\(Config.urlMemberCenter)?token=\(UserManager.shared().getUserToken()!)&AccountId=\(UserManager.shared().currentUser.accountId!)")!
        let urlRequest = URLRequest(url: url)
        webview!.load(urlRequest)
    }
}
