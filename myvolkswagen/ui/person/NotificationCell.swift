//
//  NotificationCell.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/27.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

class NotificationCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var desLb: UILabel!
    @IBOutlet weak var dateLb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        image.layer.cornerRadius = CGFloat(10).getFitSize()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowRadius = CGFloat(10).getFitSize()
        self.contentView.layer.cornerRadius = CGFloat(10).getFitSize()
        self.contentView.layer.masksToBounds = true
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.clipsToBounds = false
    }

}
