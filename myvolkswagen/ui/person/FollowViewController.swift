//
//  FollowViewController.swift
//  myvolkswagen
//
//  Created by Apple on 3/6/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class FollowViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    enum Mode: Int {
        case follow = 0, search
    }
    
    enum Section: Int {
        case paddingTab = 0, paddingHistory, content, empty
        static var allCases: [Section] {
            var values: [Section] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    var of: Profile!
    var mode: Mode = .follow
    
    // mode follow
    var isFollower: Bool = false
    @IBOutlet weak var containerTab: UIView!
    @IBOutlet weak var imageViewTab: UIImageView!
    var followers: [Relation] = []
    var followings: [Relation] = []
    
    // mode history
    @IBOutlet weak var containerHistory: UIView!
    @IBOutlet weak var labelHistory: UILabel!
    @IBOutlet weak var buttonClearHistory: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var histories: [Relation] = []
    var results: [Relation] = []
    
    var afterFirstQueryFollowing = false
    var afterFirstQueryFollower = false
    var emptySearchResult = false
    var hideLeftBtn: Bool = false
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()

        view.backgroundColor = UiUtility.colorBackgroundWhite
        containerTab.backgroundColor = UiUtility.colorBackgroundWhite
        containerHistory.backgroundColor = UiUtility.colorBackgroundWhite
        tableView.backgroundColor = UiUtility.colorBackgroundWhite
        
        if !hideLeftBtn {
            naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        }
        naviBar.containerSearch.isHidden = false
        naviBar.textFieldSearch.inputAccessoryView = toolbar
        naviBar.textFieldSearch.delegate = self
        
        labelHistory.textColor = UiUtility.colorDarkGrey
        labelHistory.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
        labelHistory.text = "recent_search_result".localized
        
        buttonClearHistory.setTitleColor(UiUtility.colorBrownGrey, for: .normal)
        buttonClearHistory.titleLabel?.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
        buttonClearHistory.setTitle("clear_all".localized, for: .normal)
        buttonClearHistory.contentEdgeInsets = UIEdgeInsets(top: 0.01, left: 0, bottom: 0.01, right: 0)
        
        collectionView.backgroundColor = UiUtility.colorBackgroundWhite
        collectionView.delaysContentTouches = false
        
        if isFollower {
            followers = UserManager.shared().getRelation(of: of, isFollower: self.isFollower, fromRemote: true, listener: self)
        } else {
            followings = UserManager.shared().getRelation(of: of, isFollower: self.isFollower, fromRemote: true, listener: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateUi()
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    override func cancelButtonClicked(_ button: UIButton) {
        naviBar.textFieldSearch.text = nil
        naviBar.textFieldSearch.resignFirstResponder()
        
        if mode == .search {
            results = []
            mode = .follow
            if isFollower {
                self.followers = UserManager.shared().getRelation(of: of, isFollower: true, fromRemote: false, listener: self)
            } else {
                self.followings = UserManager.shared().getRelation(of: of, isFollower: false, fromRemote: false, listener: self)
            }
            updateUi()
        }
    }
    
    
    
    // MARK: text field delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if mode != .search {
            mode = .search
            emptySearchResult = false
            updateUi()
            
            if self.historicalIds.count == 0 {
                loadHistoryFromLocal()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text {
            let trimmedText = text.trimmingCharacters(in: .whitespacesAndNewlines)
            if trimmedText.count != 0 {
                UserManager.shared().search(keyword: trimmedText, listener: self)
            }
        }
        textField.resignFirstResponder()
        
        return true
    }
    
    
    
    // MARK: user protocol
    
    override func didStartFollow() {
        showHud()
    }
    
    override func didFinishFollow(success: Bool, accountId: String, set: Bool) {
        dismissHud()
        
        if success {
            if mode == .follow {
                if isFollower {
                    self.followers = UserManager.shared().getRelation(of: of, isFollower: true, fromRemote: false, listener: self)
                } else {
                    self.followings = UserManager.shared().getRelation(of: of, isFollower: false, fromRemote: false, listener: self)
                }
            } else {
                for relation in results {
                    if let aid = relation.accountId {
                        if aid == accountId {
                            if set {
                                relation.isFollow = 1
                            } else {
                                relation.isFollow = 0
                            }
                            break
                        }
                    }
                }
            }
            
            updateUi()
        }
    }
    
    override func didStartGetRelation() {
        showHud()
    }
    
    override func didFinishGetRelation(success: Bool, isFollower: Bool, relations: [Relation]?) {
        dismissHud()
        
        if success {
            if let relations = relations {
                if isFollower {
                    afterFirstQueryFollower = true
                    self.followers = relations
                } else {
                    afterFirstQueryFollowing = true
                    self.followings = relations
                }
                
                updateUi()
            }
        }
    }
    
    override func didStartSearch() {
        showHud()
    }
    override func didFinishSearch(success: Bool, relations: [Relation]?) {
        dismissHud()
        
        if success {
            if let relations = relations {
                emptySearchResult = true
                self.results = relations
                updateUi()
            }
        }
    }
    
    override func didStartGetRecentlyBrosedMember() {
        showHud()
    }
    override func didFinishGetRecentlyBrosedMember(success: Bool, relations: [Relation]?) {
        dismissHud()
        
        if success {
            if let relations = relations {
                self.histories = relations
                updateUi()
            }
        }
    }
    
    
    
    // MARK: collection view delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return histories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        Logger.d("indexPath: \(indexPath)")
        return CGSize(width: UiUtility.adaptiveSize(size: 146), height: UiUtility.adaptiveSize(size: 137))
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HistoryCell", for: indexPath)
//        cell.backgroundColor = UiUtility.colorBackgroundWhite
        
        cell.contentView.layer.cornerRadius = UiUtility.adaptiveSize(size: 2)
        cell.contentView.layer.borderWidth = 1.0
        cell.contentView.layer.borderColor = UIColor.clear.cgColor
        cell.contentView.layer.masksToBounds = true;
        
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width:0,height: 2.0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 1.0
        cell.layer.masksToBounds = false;
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath

        
        
        
        let card: UIView = cell.viewWithTag(99)!
        card.layer.cornerRadius = UiUtility.adaptiveSize(size: 3)
        
        let button: UIButton = cell.viewWithTag(1) as! UIButton
        let imageView: UIImageView = cell.viewWithTag(2) as! UIImageView
        let nickname: UILabel = cell.viewWithTag(3) as! UILabel
        let carModel: UILabel = cell.viewWithTag(4) as! UILabel
        let imageCheck: UIImageView = cell.viewWithTag(5) as! UIImageView
        
        let history = histories[indexPath.row]
        
        button.addTarget(self, action: #selector(deleteHistoryClicked(_:)), for: .touchUpInside)
        
        imageView.backgroundColor = UiUtility.colorBackgroundWhite
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = UiUtility.adaptiveSize(size: 30)
        if let stickerURL = history.stickerURL {
            let url = URL(string: stickerURL)
            imageView.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_photo"))
        }
        
        nickname.textColor = UIColor.black
        nickname.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
        nickname.text = history.nickname
        
        carModel.textColor = UiUtility.colorBrownGrey
        carModel.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
        carModel.text = nil
        if let cm = history.ownModel {
            if cm.count != 0 {
                carModel.text = cm[0]
            }
        }
        
        // image check
        imageCheck.isHidden = true
//        if let level = history.level {
//            if level > 2 {
//                imageCheck.isHidden = false
//                imageCheck.image = UIImage.init(named: "check_golden_large")
//            } else if level > 1 {
//                imageCheck.isHidden = false
//                imageCheck.image = UIImage.init(named: "check_silver_large")
//            } else if level > 0 {
//                imageCheck.isHidden = false
//                imageCheck.image = UIImage.init(named: "check_large")
//            }
//        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let history = histories[indexPath.row]
        addHistory(with: history)
        
        UiUtility.goBrowse(profile: Profile.init(with: history), controller: self)
    }
    
    
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Section.init(rawValue: section)! {
        case .paddingTab:
            if mode == .follow {
                return 1
            } else {
                return 0
            }
        case .paddingHistory:
            if mode == .follow {
                return 0
            } else {
                if histories.count == 0 {
                    return 0
                } else {
                    return 1
                }
            }
        case .content:
            if mode == .follow {
                if isFollower {
                    return followers.count
                } else {
                    return followings.count
                }
            } else {
                return results.count
            }
        case .empty:
            if getOperatingData().count == 0 {
                return 1
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Section.init(rawValue: indexPath.section)! {
        case .paddingTab:
            return UiUtility.adaptiveSize(size: 40)
        case .paddingHistory:
            return UiUtility.adaptiveSize(size: 177)
        case .content:
            return UiUtility.adaptiveSize(size: 80)
        case .empty:
            return UiUtility.adaptiveSize(size: 152)
        }
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        switch Section.init(rawValue: indexPath.section)! {
        case .paddingTab, .paddingHistory:
            cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath)
            cell.selectionStyle = .none
        case .content:
            cell = tableView.dequeueReusableCell(withIdentifier: "LikerCell", for: indexPath)
            
            let photo: UIImageView = cell.viewWithTag(1) as! UIImageView
            let name: UILabel = cell.viewWithTag(2) as! UILabel
            let button: UIButton = cell.viewWithTag(3) as! UIButton
            let imageCheck: UIImageView = cell.viewWithTag(4) as! UIImageView
            
            let user = getItem(at: indexPath)
            
            photo.backgroundColor = UiUtility.colorBackgroundWhite
            photo.layer.masksToBounds = true
            photo.layer.cornerRadius = UiUtility.adaptiveSize(size: 30)
            if let stickerURL = user.stickerURL {
                let url = URL(string: stickerURL)
                photo.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_photo"))
            }
            
            // image check
            imageCheck.isHidden = true
//            if let level = user.level {
//                if level > 2 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_golden_large")
//                } else if level > 1 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_silver_large")
//                } else if level > 0 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_large")
//                }
//            }
            
            name.textColor = UIColor.black
            name.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            name.text = user.nickname
            
            button.isHidden = user.isMe() || user.isOfficial()
            button.setImage(user.isFollowedByMe() ? UIImage.init(named: "following_small") : UIImage.init(named: "follow_small"), for: .normal)
            button.addTarget(self, action: #selector(buttonFollowClicked(_:)), for: .touchUpInside)
        case .empty:
            cell = tableView.dequeueReusableCell(withIdentifier: "EmptyMsgCell", for: indexPath)
            cell.selectionStyle = .none
            cell.backgroundColor = UiUtility.colorBackgroundWhite
            
            let msg: UILabel = cell.viewWithTag(1) as! UILabel
            msg.textColor = UIColor(rgb: 0x4A4A4A)
            msg.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
            msg.isHidden = true
            if mode == .follow {
                if isFollower {
                    if afterFirstQueryFollower {
                        msg.isHidden = false
                        msg.text = "no_follower_yet".localized
                    }
                } else {
                    if afterFirstQueryFollowing {
                        msg.isHidden = false
                        msg.text = "no_follower_yet".localized
                    }
                }
            } else {
                if emptySearchResult {
                    msg.isHidden = false
                    msg.text = "no_search_result".localized
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == Section.content.rawValue {
            let relation = getItem(at: indexPath)
            
            if mode == .search {
                addHistory(with: relation)
            }
            
            UiUtility.goBrowse(profile: Profile.init(with: relation), controller: self)
        }
    }
    
    
    
    // MARK: histories
    let keyHistoryId = "key_history_id"
    let separatorHistoryId = ","
    let maxHistoryCount: Int = 10
    var historicalIds: [String] = []
    private func loadHistoryFromLocal() {
        let defaults = UserDefaults.standard
        if let ids = defaults.string(forKey: keyHistoryId) {
            if ids.count != 0 {
                historicalIds = ids.components(separatedBy: separatorHistoryId)
                Logger.d("load local historicalIds: \(historicalIds)")
                UserManager.shared().getRecentlyBrowsed(memberIds: historicalIds, listener: self)
            }
        } else {
            Logger.d("no historicalIds in local")
        }
    }
    
    private func saveHistoryToLocal() {
        var str: String = ""
        for hid in historicalIds {
            if str.count == 0 {
                str = hid
            } else {
                str = "\(str)\(separatorHistoryId)\(hid)"
            }
        }
        
        let defaults = UserDefaults.standard
        if str.count == 0 {
            defaults.removeObject(forKey: keyHistoryId)
        } else {
            defaults.set(str, forKey: keyHistoryId)
        }
    }
    
    private func clearAllHistory() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: keyHistoryId)
        
        historicalIds = []
        histories = []
        updateUi()
    }
    
    private func addHistory(with relation: Relation) {
        if let addedId = relation.accountId {
//            for id in historicalIds {
//                if id == addedId {
//                    Logger.d("already exist in history")
//                    return
//                }
//            }
            
            var newData: [String] = [addedId]
            for id in historicalIds {
                if addedId != id {
                    newData.append(id)
                }
            }
            if newData.count > maxHistoryCount {
                newData.removeLast()
            }
            
            historicalIds = newData
            saveHistoryToLocal()
            
            var newHistory: [Relation] = [relation]
            for r in histories {
                if addedId != r.accountId {
                    newHistory.append(r)
                }
            }
            if newHistory.count > maxHistoryCount {
                newHistory.removeLast()
            }
            histories = newHistory
            updateUi()
        }
    }
    
    private func removeHistory(with relation: Relation) {
        if let aid = relation.accountId {
            var newData: [String] = []
            for id in historicalIds {
                if id != aid {
                    newData.append(id)
                }
            }
            historicalIds = newData
            saveHistoryToLocal()
            
            var newHistory: [Relation] = []
            for r in histories {
                if r.accountId != relation.accountId {
                    newHistory.append(r)
                }
            }
            histories = newHistory
            updateUi()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func getItem(at position: IndexPath) -> Relation {
        // no boundry check
        return getOperatingData()[position.row]
    }
    
    private func getOperatingData() -> [Relation] {
        if mode == .follow {
            if isFollower {
                return followers
            } else {
                return followings
            }
        } else {
            return results
        }
    }
    
    private func updateUi() {
        if mode == .follow {
            // navi
            naviBar.setupSearchArea(orig: true)
            
            // tab
            containerTab.isHidden = false
            imageViewTab.isHighlighted = isFollower
//            if isFollower {
//
//            } else {
//
//            }
            
            // history
            containerHistory.isHidden = true
            
            // table view
            tableView.reloadData()
            
            // empty view
            // TODO
        } else {
            // navi
            naviBar.setupSearchArea(orig: false)
            
            // tab
            containerTab.isHidden = true
            
            // history
            containerHistory.isHidden = (histories.count == 0)
            if histories.count != 0 {
                collectionView.reloadData()
            }
            
            // table view
            tableView.reloadData()
            
            // empty view
            // TODO
        }
    }

    @IBAction private func tab0Clicked(_ sender: UIButton) {
        if isFollower {
            isFollower = false
            followings = UserManager.shared().getRelation(of: of, isFollower: self.isFollower, fromRemote: false, listener: self)
            updateUi()
        }
    }
    
    func switchToTab0() {
            isFollower = false
            followings = UserManager.shared().getRelation(of: of, isFollower: self.isFollower, fromRemote: false, listener: self)
            updateUi()
    }
    
    @IBAction private func tab1Clicked(_ sender: UIButton) {
        if !isFollower {
            isFollower = true
            followers = UserManager.shared().getRelation(of: of, isFollower: self.isFollower, fromRemote: false, listener: self)
            updateUi()
        }
    }
    
    @IBAction private func clearHistoryClicked(_ sender: UIButton) {
        clearAllHistory()
    }
    
    @objc private func buttonFollowClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            let user: Relation = getItem(at: indexPath)
            if let aid = user.accountId {
                UserManager.shared().follow(accountId: aid, set: !user.isFollowedByMe(), listener: self)
            }
        }
    }
    
    @objc private func deleteHistoryClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.collectionView)
        if let indexPath = self.collectionView.indexPathForItem(at: buttonPosition) {
            let history: Relation = histories[indexPath.row]
            
            removeHistory(with: history)
        }
    }
}
