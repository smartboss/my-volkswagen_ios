//
//  NotificationDetailViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/27.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
import Kingfisher

class NotificationDetailViewController: NaviViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var cardBg: UIView!
    @IBOutlet weak var dateLb: BasicLabel!
    @IBOutlet weak var titleLb: BasicLabel!
    @IBOutlet weak var contentLb: ContextLabel!
    @IBOutlet weak var scrollview: UIScrollView!
    var imageNews: ImageNews?
    var currentIndex = 0
    var inboxId: Int?
    var noticeKind: NoticeKind?
    var news: News!
    @IBOutlet weak var noticeV: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
        
        let gradientVieww = GradientBackgroundView()
        gradientVieww.frame = view.bounds
        gradientView.addSubview(gradientVieww)
        
        if inboxId != nil {
            naviBar.setTitle("車主通知".localized)
            NoticeManager.shared().getInboxContent(inBoxId: self.inboxId!, listener: self)
            cardBg.backgroundColor = .clear
            noticeV.isHidden = true
        } else if imageNews != nil {
            naviBar.setTitle("最新消息".localized)
            setupImageNewsViews()
            noticeV.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if inboxId != nil {
            Utility.sendEventLog(ServiceName: "pv_notice_detail", UniqueId: "", EventName: "pageview", InboxId: "", EventCategory: "pageview", PageName: "pv_notice_detail", EventLabel: "", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        } else if imageNews != nil {
            Utility.sendEventLog(ServiceName: "pv_news_detail", UniqueId: "", EventName: "pageview", InboxId: "", EventCategory: "pageview", PageName: "pv_news_detail", EventLabel: "", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        }
    }
    override func leftButtonClicked(_ button: UIButton) {
        back()
        if inboxId != nil {
            Utility.sendEventLog(ServiceName: "pv_notice_detail", UniqueId: "", EventName: "上一頁", InboxId: "", EventCategory: "button", PageName: "pv_notice_detail", EventLabel: "上一頁", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        } else if imageNews != nil {
            Utility.sendEventLog(ServiceName: "pv_news_detail", UniqueId: "", EventName: "上一頁", InboxId: "", EventCategory: "button", PageName: "pv_news_detail", EventLabel: "上一頁", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        }
    }
    
    func setupImageNewsViews() {
        if imageNews!.gallery!.count > 0 {
            if imageNews!.gallery![0].contains("empty.") {
                pageControl.isHidden = true
                imageCollectionView.removeFromSuperview()
            } else {
                pageControl.numberOfPages = imageNews!.gallery!.count
                pageControl.isHidden = false
            }
        } else {
            pageControl.isHidden = true
            imageCollectionView.removeFromSuperview()
        }
        let originalDateString = imageNews!.publishedAt ?? ""
        if let formattedDateString = formatDateString(originalDateString) {
            print(formattedDateString)
            dateLb.text = formattedDateString
        } else {
            dateLb.text = imageNews!.publishedAt
        }
        titleLb.text = imageNews!.title
        contentLb.text = imageNews!.content
        
        contentLb.foregroundColor = { (linkResult) in
            switch linkResult.detectionType {
            case .url:
                return UIColor(rgb: 0x007AFF)
            default:
                return UiUtility.colorDarkGrey
            }
        }
        
        contentLb.underlineStyle = { (linkResult) in
            switch linkResult.detectionType {
            default:
                return NSUnderlineStyle.single
            }
        }
        
        contentLb.didTouch = { (touchResult) in
            switch touchResult.state {
            case .began:
                Logger.d("began")
            case .ended:
                Logger.d("ended")
                self.setupLinkResult(touchResult: touchResult)
            //                    Logger.d("ended, \(touchResult)")
            default:
                break
            }
        }
    }
    
    func setupLinkResult(touchResult: TouchResult) {
        if let linkResult = touchResult.linkResult {
            if linkResult.detectionType == .url {
                Logger.d("text: \(linkResult.text)")
                
                var text = linkResult.text
//                        if !text.starts(with: "http://") && !text.starts(with: "https://") {
//                            text = "http://\(text)"
//                        }
                if let link = URL(string: text) {
                    if let nk = self.noticeKind {
                        if nk == .normalSurvey {
                            let ttext = "\(text)?aka_account_id=\(UserManager.shared().currentUser.accountId!)"
                            UiUtility.goBrowseWebview(url: URL(string: ttext)!, notiKind: .normalSurvey, controller: self)
                        } else if nk == .newCarsSurvey {
                            let ttext = "\(text)?AccountId=\(UserManager.shared().currentUser.accountId!)"
                            UiUtility.goBrowseWebview(url: URL(string: ttext)!, notiKind: .newCarsSurvey, controller: self)
                        } else {
                            let deeplinkType = DeeplinkParser.shared.parseDeepLink(link, ref: nil)
                                let urll = URL(string: text)!
                                UIApplication.shared.open(urll)
                        }
                    } else {
                        if link.pathComponents.contains("memberCouponList.html") {
                            let ttext = "\(text)?token=\(UserManager.shared().getUserToken()!)&AccountId=\(UserManager.shared().currentUser.accountId!)"
                            UiUtility.goBrowseWebview(url: URL(string: ttext)!, notiKind: .memberPointList, controller: self)
                        } else if link.host!.contains("service-cam.volkswagen-service.com") {
                            UiUtility.goBrowseWebview(url: link, notiKind: .serviceCam, controller: self)
                        } else {
                            let urll = URL(string: text)!
                            UIApplication.shared.open(urll)
                        }
                    }
                }
            }
            if linkResult.detectionType == .phoneNumber {
                if let url = URL(string: "telprompt://\(linkResult.text)") {
                    UIApplication.shared.open(url)
                }
            }
        }
    }
    
    func appendCouponParameters(to urlString: String) -> String? {
        guard var urlComponents = URLComponents(string: urlString) else { return nil }

        // 初始化查詢項目，如果已存在則繼承
        var queryItems: [URLQueryItem] = urlComponents.queryItems ?? []

        // 添加新的查詢項目
        let userToken = UserManager.shared().getUserToken() ?? ""
        var getvin = ""
        for cc in CarManager.shared().cars {
            if cc.licensePlateNumber == CarManager.shared().getSelectedCarNumber()! {
                getvin = cc.vin ?? ""
            }
        }
        let accId = UserManager.shared().currentUser.accountId ?? ""
        let licNum = CarManager.shared().getSelectedCarNumber() ?? ""
        
        let newQueryItems = [
            URLQueryItem(name: "token", value: userToken),
            URLQueryItem(name: "VIN", value: getvin),
            URLQueryItem(name: "LicensePlateNumber", value: licNum),
            URLQueryItem(name: "AccountId", value: accId)
        ]

        // 將新的查詢項目添加到查詢項目陣列中
        queryItems.append(contentsOf: newQueryItems)

        // 更新 URL 組件的查詢項目
        urlComponents.queryItems = queryItems

        // 返回更新後的 URL 字串
        return urlComponents.string
    }
    
    func formatDateString(_ dateString: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = "yyyy/MM/dd"
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
    func setupInboxViews() {
        if news!.photoList!.count > 0 {
            if news!.photoList![0].contains("empty.") {
                pageControl.isHidden = true
                imageCollectionView.removeFromSuperview()
            } else {
                pageControl.numberOfPages = news!.photoList!.count
                pageControl.isHidden = false
                cardBg.backgroundColor = .white
                self.scrollview.backgroundColor = .white
            }
        } else {
            pageControl.isHidden = true
            imageCollectionView.removeFromSuperview()
        }
        if let spaceIndex = news!.pDate!.firstIndex(of: " ") {
            let truncatedDate = news!.pDate![..<spaceIndex]
            print(truncatedDate)
            dateLb.text = truncatedDate.description
        } else {
            dateLb.text = news!.pDate
        }
        titleLb.text = news!.title
        contentLb.text = news!.content
        
        contentLb.foregroundColor = { (linkResult) in
            switch linkResult.detectionType {
            case .url:
                return UIColor(rgb: 0x007AFF)
            default:
                return UiUtility.colorDarkGrey
            }
        }
        
        contentLb.underlineStyle = { (linkResult) in
            switch linkResult.detectionType {
            default:
                return NSUnderlineStyle.single
            }
        }
        
        contentLb.didTouch = { (touchResult) in
            switch touchResult.state {
            case .began:
                Logger.d("began")
            case .ended:
                Logger.d("ended")
                self.setupLinkResult(touchResult: touchResult)
            //                    Logger.d("ended, \(touchResult)")
            default:
                break
            }
        }
    }
    
    // MARK: notice protocol
    override func didStartGetInboxContent() {
        showHud()
    }
    
    override func didFinishGetInboxContent(success: Bool, inboxContent: News?) {
        dismissHud()
        if success {
//            Logger.d("get news: \(news)")
            if inboxContent?.newsId == nil {
                let alertController = UIAlertController(title: nil, message: "post_has_deleted".localized, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "got_it".localized, style: .default) { (action:UIAlertAction) in
                    Logger.d("do nothing")
                }
                
                alertController.addAction(action1)
                present(alertController, animated: true, completion: nil)
            } else {
                self.news = inboxContent
                let n = Notice(with: "", idd: self.news.noticeId!)
                NoticeManager.shared().setRead(notice: n, listener: self)
                self.setupInboxViews()
                self.imageCollectionView.reloadData()
            }
        } else {
            Logger.d("failed to get news")
        }
    }
    
    // MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.imageNews != nil {
            return imageNews!.gallery?.count ?? 0
        } else if self.news != nil {
            return news!.photoList?.count ?? 0
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageNewsImageCell", for: indexPath)
        let imgv = cell.viewWithTag(1) as! UIImageView
        if self.imageNews != nil {
            let imgnewsimg = imageNews!.gallery![indexPath.item]
            if let urll = URL(string: imgnewsimg) {
                imgv.kf.setImage(with: urll, placeholder: nil)
            } else {
                imgv.image = nil
            }
        } else {
            let imgnewsimg = news!.photoList![indexPath.item]
            if let urll = URL(string: imgnewsimg) {
                imgv.kf.setImage(with: urll, placeholder: nil)
            } else {
                imgv.image = nil
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    // MARK: ScrollView
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // 算當前頁數
        if self.imageNews != nil {
            let currentIndex = Int(floor(scrollView.contentOffset.x/UiUtility.getScreenWidth()))
            pageControl.currentPage = currentIndex
        } else {
            let currentIndex = Int(floor(scrollView.contentOffset.x/UiUtility.getScreenWidth()))
            pageControl.currentPage = currentIndex
        }
    }
}

//class SmallPageControl: UIPageControl {
//    
//    override var numberOfPages: Int {
//        didSet {
//            updateDots()
//        }
//    }
//    
//    override var currentPage: Int {
//        didSet {
//            updateDots()
//        }
//    }
//    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        updateDots()
//    }
//    
//    private let dotSize: CGFloat = 3.0
//    private let dotSpacing: CGFloat = 5.0
//    
//    private func updateDots() {
//        for i in 0..<subviews.count {
//            let dot = subviews[i]
//            dot.layer.cornerRadius = dotSize / 2
//            dot.frame.size = CGSize(width: dotSize, height: dotSize)
//            
//            if i == currentPage {
//                dot.backgroundColor = UIColor.blue // 当前页的颜色
//            } else {
//                dot.backgroundColor = UIColor.gray // 其他页的颜色
//            }
//            
//            // 调整点之间的间距
//            if i < subviews.count - 1 {
//                let nextDot = subviews[i + 1]
//                let distance = dotSpacing + dotSize
//                let offsetX = dot.frame.origin.x + dotSize + distance - nextDot.frame.origin.x
//                nextDot.frame.origin.x += offsetX
//            }
//        }
//    }
//}
