//
//  MemberLevelViewController.swift
//  myvolkswagen
//
//  Created by CHUNG CHING JIANG on 2019/8/27.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class MemberLevelViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var naviBarHeight: NSLayoutConstraint!
    var hideNavBar: Bool = false
    
    enum Row: Int {
        case card = 0, level, faq
        static var allCases: [Row] {
            var values: [Row] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        naviBar.setTitle("member_level_title".localized)
        
        if hideNavBar {
            naviBarHeight.constant = 0
            naviBar.alpha = 0
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    // MARK: table view delegate
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 0
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Row.allCases.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Row.init(rawValue: indexPath.row)! {
        case .card:
            return UiUtility.adaptiveSize(size: 300)
        case .level, .faq:
            return UiUtility.adaptiveSize(size: 100)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        switch Row.init(rawValue: indexPath.row)! {
        case .card:
            cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath)
            
            let ivCard: UIImageView = cell.viewWithTag(1) as! UIImageView
            let lbTitleLevel: UILabel = cell.viewWithTag(2) as! UILabel
            let lbTitleNumber: UILabel = cell.viewWithTag(3) as! UILabel
            let lbValueLevel: UILabel = cell.viewWithTag(4) as! UILabel
            let lbValueNumber: UILabel = cell.viewWithTag(5) as! UILabel
            
//            switch UserManager.shared().currentUser.level {
//            case 1:
//                ivCard.image = UIImage.init(named: "member_card_l1")
//            case 2:
//                ivCard.image = UIImage.init(named: "member_card_l2")
//            case 3...:
//                ivCard.image = UIImage.init(named: "member_card_l3")
//            default:
//                break;
//            }
            
            lbTitleLevel.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
            lbTitleLevel.textColor = UIColor(rgb: 0x9B9B9B)
            lbTitleLevel.text = "member_level".localized
            
            lbTitleNumber.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
            lbTitleNumber.textColor = UIColor(rgb: 0x9B9B9B)
            lbTitleNumber.text = "member_level_number".localized
            
            lbValueLevel.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            lbValueLevel.textColor = .black
            lbValueLevel.text = UserManager.shared().currentUser.levelName
            
            lbValueNumber.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            lbValueNumber.textColor = .black
            lbValueNumber.text = UserManager.shared().currentUser.accountId
            
            
        case .level:
            cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath)
            
            let ivIcon: UIImageView = cell.viewWithTag(1) as! UIImageView
            let lbTitle: UILabel = cell.viewWithTag(2) as! UILabel
            let lbDesc: UILabel = cell.viewWithTag(3) as! UILabel
            let vUnderline: UIView = cell.viewWithTag(4)!
            
            ivIcon.image = UIImage.init(named: "memberLevel")
            
            lbTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
            lbTitle.textColor = .black
            lbTitle.text = "member_level_explanation".localized
            
            lbDesc.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
            lbDesc.textColor = UIColor(rgb: 0x333333)
            lbDesc.text = "member_level_desc".localized
            
            vUnderline.isHidden = false
        case .faq:
            cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath)
            
            let ivIcon: UIImageView = cell.viewWithTag(1) as! UIImageView
            let lbTitle: UILabel = cell.viewWithTag(2) as! UILabel
            let lbDesc: UILabel = cell.viewWithTag(3) as! UILabel
            let vUnderline: UIView = cell.viewWithTag(4)!
            
            ivIcon.image = UIImage.init(named: "memberFaq")
            
            lbTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
            lbTitle.textColor = .black
            lbTitle.text = "member_level_faq".localized
            
            lbDesc.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
            lbDesc.textColor = UIColor(rgb: 0x333333)
            lbDesc.text = "member_level_faq_desc".localized
            
            vUnderline.isHidden = true
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        Logger.d("You tapped cell number \(indexPath.row).")
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch Row.init(rawValue: indexPath.row)! {
        case .level:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.index = WebsiteIndex.memberExplanation
            self.navigationController?.pushViewController(vc, animated: true)
        case .faq:
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.index = WebsiteIndex.memberFaq
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            break
        }
    }

}
