//
//  DrawerNavigationController.swift
//  myvolkswagen
//
//  Created by Apple on 2/12/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Firebase

protocol DrawerRootViewControllerDelegate: class {
    func drawerRootViewControllerDidTapMenuButton(_ rootViewController: DrawerNavigationController)
}

class DrawerNavigationController: UINavigationController {
    
    weak var drawerDelegate: DrawerRootViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UiUtility.colorBackgroundWhite
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DrawerNavigationController {
    
    @objc func handleMenuButton() {
        Analytics.logEvent("view_vw", parameters: nil)
        drawerDelegate?.drawerRootViewControllerDidTapMenuButton(self)
    }
    
    @objc func handleNotificationButton() {
        Analytics.logEvent("view_notification", parameters: nil)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NotificationViewController")
        self.pushViewController(vc, animated: true)
    }
}
