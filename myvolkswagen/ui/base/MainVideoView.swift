//
//  MainVideoView.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/29.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
import AVFoundation

class MainVideoView: UIView {
    private var playerLayer: AVPlayerLayer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupPlayer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupPlayer()
    }
    
    private func setupPlayer() {
        // 設置影片文件的URL
        if let videoURL = Bundle.main.url(forResource: "main_video", withExtension: "mp4") {
            let player = AVPlayer(url: videoURL)
            
            // 設置音量為靜音
            player.volume = 0.0
            
            // 設置循環播放
            player.actionAtItemEnd = .none
            NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd(_:)), name: .AVPlayerItemDidPlayToEndTime, object: player.currentItem)
            
            // 創建AVPlayerLayer，用於顯示影片
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.videoGravity = .resizeAspectFill
            playerLayer.frame = bounds
            
            // 將AVPlayerLayer添加到視圖中
            layer.addSublayer(playerLayer)
            
            // 開始播放影片
            player.play()
            
            // 將playerLayer存儲在私有變數中，以便稍後訪問（例如，停止播放）
            self.playerLayer = playerLayer
        }
    }
    
    // 停止播放影片
    func stop() {
        playerLayer?.player?.pause()
    }
    
    @objc private func playerItemDidReachEnd(_ notification: Notification) {
        if let playerItem = notification.object as? AVPlayerItem {
            playerItem.seek(to: .zero, completionHandler: nil)
        }
    }
}
