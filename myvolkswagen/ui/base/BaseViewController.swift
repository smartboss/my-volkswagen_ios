//
//  BaseViewController.swift
//  myvolkswagen
//
//  Created by Apple on 1/24/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol ContentDelegate: class {
    func setDirty(remote: Bool)
}

protocol ClubSortDelegate: class {
    func setSort(type: ClubSort)
    //    func setSort(type: ClubJoinedSort)
}

class BaseViewController: UIViewController, UserProtocol, CarProtocol, ProfileProtocol, NewsProtocol, NoticeProtocol, ContentDelegate, ClubProtocol, ClubSortDelegate {
    
    var offsetY: CGFloat = -1
    var distance: CGFloat = 277
    var editingTextField: UITextField?
    
    var hasTextField: Bool = false
    var toolbar: UIToolbar!
    @IBOutlet weak var viewInputShield: UIView?
    var keyboardShowing: Bool = false
    
    var contentIsDirtyLocal: Bool = false
    var contentIsDirtyRemote: Bool = false
    var mybtnV: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if hasTextField {
            //init toolbar
            toolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
            //create left side empty space so that done button set on right side
            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "done".localized, style: .done, target: self, action: #selector(doneButtonAction(_:)))
            toolbar.setItems([flexSpace, doneBtn], animated: false)
            toolbar.sizeToFit()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if hasTextField {
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
            //            NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
            //            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardDidHideNotification, object: nil)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUnreadCount), name: NSNotification.Name(rawValue: NoticeManager.nameUnreadCountNotification), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if hasTextField {
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        }
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NoticeManager.nameUnreadCountNotification), object: nil)
        
        //NotificationCenter.default.removeObserver(self)
    }
    
    func getDeeplinkParams() -> String {
        let userToken = UserManager.shared().getUserToken() ?? ""
        var getvin = ""
        for cc in CarManager.shared().cars {
            if cc.licensePlateNumber == CarManager.shared().getSelectedCarNumber()! {
                getvin = cc.vin ?? ""
            }
        }
        let accId = UserManager.shared().currentUser.accountId ?? ""
        let licNum = CarManager.shared().getSelectedCarNumber() ?? ""
        let paramString = "token=\(userToken)&Vin=\(getvin)&AccountId=\(accId)&LicensePlateNumber=\(licNum)"
        return paramString
    }
    
    // MARK: content delegate
    
    func setDirty(remote: Bool) {
        if remote {
            contentIsDirtyRemote = true
            contentIsDirtyLocal = true
        } else {
            contentIsDirtyLocal = true
        }
    }
    
    
    
    // MARK: club sort delegate
    
    func setSort(type: ClubSort) {
        
    }
    
    
    
    // MARK: done button
    
    @objc func doneButtonAction(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }
    
    
    
    // MARK: keyboard
    
    @objc func keyboardWillShow(notification: NSNotification) {
        //        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
        //            if self.view.frame.origin.y == 0 {
        //                Logger.d("keyboardWillShow, \(keyboardSize.height)")
        //                self.view.frame.origin.y -= keyboardSize.height
        //            }
        //        }
        
        viewInputShield?.isHidden = false
        keyboardShowing = true
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        //        if self.view.frame.origin.y != 0 {
        //            Logger.d("keyboardWillHide")
        //            self.view.frame.origin.y = 0
        //        }
        viewInputShield?.isHidden = true
        keyboardShowing = false
    }
    
    
    
    // MARK: club protocol
    
    func didStartGetJoinedClubCount() {
        showHud()
    }
    
    func didFinishGetJoinedClubCount(success: Bool, count: Int?) {
        dismissHud()
    }
    
    func didStartGetGuestData() {
        showHud()
    }
    
    func didFinishGetGuestData(success: Bool, guestClubData: GuestClubData?) {
        dismissHud()
    }
    
    func didStartGetGuestClubs() {
        showHud()
    }
    
    func didFinishGetGuestClubs(success: Bool, guestClubs: [Club]?) {
        dismissHud()
    }
    
    func didStartGetClubRelation() {
        showHud()
    }
    
    func didFinishGetClubRelation(success: Bool, kind: ClubRelationKind, relations: [ClubRelation]) {
        dismissHud()
    }
    
    func didStartAppendClub() {
        showHud()
    }
    
    func didFinishAppendClub(success: Bool) {
        dismissHud()
    }
    
    func didStartGetClubData() {
        showHud()
    }
    
    func didFinishGetClubData(success: Bool, clubData: ClubData?) {
        dismissHud()
    }
    
    func didStartSearchClub() {
        showHud()
    }
    
    func didFinishSearchClub(success: Bool, result: [Club]) {
        dismissHud()
    }
    
    func didStartUpdateClub() {
        showHud()
    }
    
    func didFinishUpdateClub(success: Bool) {
        dismissHud()
    }
    
    func didStartGetClubMembers() {
        showHud()
    }
    
    func didFinishGetClubMembers(success: Bool, relations: [ClubRelation]) {
        dismissHud()
    }
    
    func didStartDiscardMember() {
        showHud()
    }
    
    func didFinishDiscardMember(success: Bool) {
        dismissHud()
    }
    
    func didStartLeaveClub() {
        showHud()
    }
    
    func didFinishLeaveClub(success: Bool) {
        dismissHud()
    }
    
    func didStartMemberAdd() {
        showHud()
    }
    
    func didFinishMemberAdd(success: Bool) {
        dismissHud()
    }
    
    func didStartGetMyClubList() {
        showHud()
    }
    
    func didFinishGetMyClubList(success: Bool, result: [Club]) {
        dismissHud()
    }
    
    func didStartGetSinglePage() {
        //        showHud()
    }
    
    func didFinishGetSinglePage(success: Bool, posts: [ClubPost]) {
        //        dismissHud()
    }
    
    func didStartAppendNews() {
        showHud()
    }
    
    func didFinishAppendNews(success: Bool) {
        dismissHud()
    }
    
    func didStartLikeClubPost() {
        showHud()
    }
    
    func didFinishLikeClubPost(success: Bool, post: ClubPost, set: Bool) {
        dismissHud()
    }
    
    func didStartTrackClubPost() {
        showHud()
    }
    
    func didFinishTrackClubPost(success: Bool, post: ClubPost, set: Bool) {
        dismissHud()
    }
    
    func didStartGetClubMembersOfBehavior() {
        showHud()
    }
    
    func didFinishGetClubMembersOfBehavior(success: Bool, members: [Relation]?) {
        dismissHud()
    }
    
    func didStartSetTop() {
        showHud()
    }
    
    func didFinishSetTop(success: Bool, post: ClubPost, set: Bool) {
        dismissHud()
    }
    
    func didStartDeleteClubPost() {
        showHud()
    }
    
    func didFinishDeleteClubPost(success: Bool, post: ClubPost?) {
        dismissHud()
    }
    
    func didStartEditClubPost() {
        showHud()
    }
    
    func didFinishEditClubPost(success: Bool, newsId: Int?) {
        dismissHud()
    }
    
    func didStartGetClubPost() {
        showHud()
    }
    
    func didFinishGetClubPost(success: Bool, post: ClubPost?) {
        dismissHud()
    }
    
    func didStartCommentClubPost() {
        showHud()
    }
    
    func didFinishCommentClubPost(success: Bool, post: ClubPost) {
        dismissHud()
    }
    
    func didStartDeleteClubPostComment() {
        showHud()
    }
    
    func didFinishDeleteClubPostComment(success: Bool, comment: Comment?) {
        dismissHud()
    }
    
    func didStartGetExtUrl() {
        showHud()
    }
    
    func didFinishGetExtUrl(success: Bool, type: String, url: String?) {
        dismissHud()
    }
    
    
    
    // MARK: user protocol
    
    func didStartLogin() {
        showHud()
    }
    
    func didFinishLogin(success: Bool) {
        goInitialPage(success: success)
    }
    
    func didStartPhoneLogin() {
        showHud()
    }
    
    func didFinishPhoneLogin(success: Bool, code: Int?, msg: String?) {
        goInitialPage(success: success)
    }
    
    func goInitialPage(success: Bool) {
        dismissHud()
        
        if success {
            if UserManager.shared().currentUser.showProfile ?? false {
                // show profile editor
                let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewControllerV2") as! EditProfileViewControllerV2
                vc.type = .login
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.updateRootVC(showAlert: false)
            }
        } else {
            showApiFailureAlert(message: "msg_failed_to_login".localized)
        }
    }
    
    func goInitialPageWithDeeplink(deeplink: URL) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.updateRootVC(showAlert: false)
        if Deeplinker.handleDeeplink(url: deeplink, ref: nil) {
            print("goInitialPageWithDeeplink start deeplink(1): \(deeplink.absoluteString)")
            Deeplinker.checkDeepLink()
        }
    }
    
    func didStartLogout() {
        showHud()
    }
    
    func didFinishLogout(success: Bool) {
        dismissHud()
        
        if !success {
            showApiFailureAlert(message: "msg_failed_to_logout".localized)
        }
    }
    
    func didStartBindFb() {
        showHud()
    }
    
    func didFinishBindFb(success: Bool, isBind: Bool) {
        dismissHud()
        
        if !success {
            if isBind {
                showApiFailureAlert(message: "msg_failed_to_bind_fb".localized)
            } else {
                showApiFailureAlert(message: "msg_failed_to_unbind_fb".localized)
            }
        }
    }
    
    func didStartBindLine() {
        showHud()
    }
    
    func didFinishBindLine(success: Bool, isBind: Bool) {
        dismissHud()
        
        if !success {
            if isBind {
                showApiFailureAlert(message: "msg_failed_to_bind_fb".localized)
            } else {
                showApiFailureAlert(message: "msg_failed_to_unbind_fb".localized)
            }
        }
    }
    
    func didStartUnbindLine() {
        showHud()
    }
    
    func didFinishUnbindLine(success: Bool, isBind: Bool) {
        dismissHud()
        
        if !success {
            if isBind {
                showApiFailureAlert(message: "msg_failed_to_bind_fb".localized)
            } else {
                showApiFailureAlert(message: "msg_failed_to_unbind_fb".localized)
            }
        }
    }
    
    func didStartBindVWId() {
        showHud()
    }
    
    func didFinishBindVWId(success: Bool, isBind: Bool) {}
    
    func didStartGetQrcode() {
        showHud()
    }
    
    func didFinishGetQrcode(success: Bool, qrcodeImage: UIImage?) {
        dismissHud()
    }
    
    func didStartFollow() {}
    func didFinishFollow(success: Bool, accountId: String, set: Bool) {}
    
    func didStartGetRelation() {}
    func didFinishGetRelation(success: Bool, isFollower: Bool, relations: [Relation]?) {}
    
    func didStartUpdatePushToken() {}
    func didFinishUpdatePushToken(success: Bool) {}
    
    func didStartSearch() {}
    func didFinishSearch(success: Bool, relations: [Relation]?) {}
    
    func didStartGetRecentlyBrosedMember() {}
    func didFinishGetRecentlyBrosedMember(success: Bool, relations: [Relation]?) {}
    
    func didStartGetMemberLevelInfo() {}
    func didFinishGetMemberLevelInfo(success: Bool, level: Int?, levelName: String?, levelIntro: String?) {}
    
    func didStartGetMemberLevelInfoWebview() {}
    func didFinishGetMemberLevelInfoWebview(success: Bool, level: Int?, levelName: String?) {}
    
    func didStartGetRedeemState() {}
    func didFinishGetRedeemState(success: Bool, taskCount: Int?, totalPoints: Int?) {}
    
    func didStartGetRedeemRecord() {}
    func didFinishGetRedeemRecord(success: Bool, record: [Redeem]?) {}
    
    func didStartRedeem() {}
    func didFinishRedeem(success: Bool) {}
    
    func didStartGetRedeemList() {}
    func didFinishGetRedeemList(success: Bool, list: [Redeem]?, totalPoints: Int?) {}
    
    func didStartRedeemReceive() {}
    func didFinishRedeemReceive(success: Bool, code: String?, redeem: Redeem?) {}
    
    func didStartRedeemCopy() {}
    func didFinishRedeemCopy(success: Bool, redeem: Redeem?) {}
    
    func didStartGetUserMaintenancePlant() {}
    func didFinishGetUserMaintenancePlant(success: Bool, maintenancePlant: MaintainancePlant?) {}
    
    func didStartRecallCheck() {}
    func didFinishRecallCheck(success: Bool, showNoticeBoard: Bool, recellUrl: String?) {}
    
    func didStartLoginCruisys() {}
    func didFinishLoginCruisys(success: Bool) {}
    
    // MARK: profile protocol
    
    func didStartGetProfile() {}
    func didFinishGetProfile(success: Bool, profile: Profile?) {}
    
    func didStartUpdateProfile() {}
    func didFinishUpdateProfile(success: Bool, profile: Profile?, msg: String?) {}
    
    
    // MARK: news protocol
    
    func didStartGetNewsCarModel() {}
    func didFinishGetNewsCarModel(success: Bool, carModels: [Model]?) {}
    
    func didStartGetNewsList(loadMore: Bool) {}
    func didFinishGetNewsList(success: Bool, loadMore: Bool, newsList: [News]?, newsKind: NewsKind) {}
    
    func didStartLikeNews() {}
    func didFinishLikeNews(success: Bool, news: News, set: Bool) {}
    
    func didStartTrackNews() {}
    func didFinishTrackNews(success: Bool, news: News, set: Bool) {}
    
    func didStartReportNews() {}
    func didFinishReportNews(success: Bool, news: News, type: Int) {}
    
    func didStartCommentNews() {}
    func didFinishCommentNews(success: Bool, news: News) {}
    
    func didStartGetNews() {}
    func didFinishGetNews(success: Bool, news: News?, noticeKind: NoticeKind?) {}
    
    func didStartGetImageNews() {}
    func didFinishGetImageNews(success: Bool, news: [ImageNews]?) {}
    
    func didStartAddPost() {}
    func didFinishAddPost(success: Bool, newsId: String?) {}
    
    func didStartEditPost() {}
    func didFinishEditPost(success: Bool, newsId: Int?) {}
    
    func didStartDeletePost() {}
    func didFinishDeletePost(success: Bool, news: News?) {}
    
    func didStartDeleteComment() {}
    func didFinishDeleteComment(success: Bool, comment: Comment?) {}
    
    func didStartGetMemberContent() {}
    func didFinishGetMemberContent(success: Bool, newsList: [News]?) {}
    
    func didStartGetMembersOfBehavior() {}
    func didFinishGetMembersOfBehavior(success: Bool, members: [Relation]?) {}
    
    
    
    // MARK: notice protocol
    
    func didStartGetNotice(historical: Bool) {}
    func didFinishGetNotice(success: Bool, historical: Bool, notices: [Notice]?) {}
    
    func didStartSetRead() {}
    func didFinishSetRead(success: Bool, notice: Notice) {}
    
    func didStartGetUnreadCount() {}
    func didFinishGetUnreadCount(success: Bool, unreadCount: Int?) {}
    
    func didStartGetInboxContent() {}
    func didFinishGetInboxContent(success: Bool, inboxContent: News?) {}
    
    func didStartDeleteNotice() {}
    func didFinishDeleteNotice(success: Bool) {}
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    // MARK: HUD
    
    var hud: MBProgressHUD?
    
    func showHud(uiEnabled: Bool) {
        DispatchQueue.main.async {
            
            // 创建一个 MBProgressHUD
            //self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            ZKProgressHUD.showGif(gifUrl: Bundle.main.url(forResource: "VW-APP_Loading", withExtension: "gif"), gifSize: CGFloat(50).getFitSize(), status: "Loading...")
            ZKProgressHUD.setBackgroundColor(UIColor.clear)
        }
    }
    
    func showHud() {
        DispatchQueue.main.async {
            ZKProgressHUD.showGif(gifUrl: Bundle.main.url(forResource: "VW-APP_Loading", withExtension: "gif"), gifSize: CGFloat(50).getFitSize(), status: "Loading...")
            ZKProgressHUD.setBackgroundColor(UIColor.clear)
        }
    }
    
    func dismissHud() {
        DispatchQueue.main.async {
            //self.hud?.hide(animated: true)
            ZKProgressHUD.hide()
        }
    }
    
    func showBGHud() {
        DispatchQueue.main.async {
            // 创建一个 MBProgressHUD
            //self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            ZKProgressHUD.showGif(gifUrl: Bundle.main.url(forResource: "VW-APP_Loading", withExtension: "gif"), gifSize: CGFloat(50).getFitSize(), status: "Loading...")
            ZKProgressHUD.setBackgroundColor(UIColor.clear)
        }
    }
    
    func showAssiBGHud() {
        DispatchQueue.main.async {
            // 创建一个 MBProgressHUD
            //self.hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            ZKProgressHUD.showGif(gifUrl: Bundle.main.url(forResource: "VW-APP_Loading", withExtension: "gif"), gifSize: CGFloat(50).getFitSize(), status: "Loading...")
            ZKProgressHUD.setBackgroundColor(UIColor.clear)
        }
    }
    
    
    // MARK: car protocol
    
    func didStartGetCar() {}
    func didFinishGetCar(success: Bool, cars: [Car]) {}
    
    func didStartGetCarPAT() {}
    func didFinishGetCarPAT(success: Bool, pat: Pat?, msg: String?) {}
    
    func didStartSaveCarPAT() {}
    func didFinishSaveCarPAT(success: Bool, msg: String?) {}
    
    func didStartRemoveCar() {}
    func didFinishRemoveCar(success: Bool, car: Car) {}
    
    func didStartCheckCarExist() {}
    func didFinishCheckCarExist(result: Result?) {}
    
    func didStartGetCarModel() {}
    func didFinishGetCarModel(success: Bool, models: [Model]?) {}
    
    func didStartAddCar() {}
    func didFinishAddCar(success: Bool) {}
    
    func didStartGetCarMaintenancePlant() {}
    func didFinishGetCarMaintenancePlant(success: Bool, maintenancePlants: [MaintainancePlant]?) {}
    
    func didStartGetCarBtns() {}
    func didFinishGetCarBtns(success: Bool, btns: [CarBtn]?) {}
    
    func didStartGetDongleBinding() {}
    func didFinishGetDongleBinding(state: CarDoungleState?, dongleData: DongleData?) {}
    
    func didStartPostDongleBinding() {}
    func didFinishPostDongleBinding(state: CarDoungleState?, dongleData: DongleData?, msg: String?) {}
    
    func didStartDeleteDongleBinding() {}
    func didFinishDeleteDongleBinding(success: Bool, car: Car?) {}
    
    func didStartGetDongleSummary() {}
    func didFinishGetDongleSummary(data: DongleSummary?) {}
    
    func didStartGetDongleNotification() {}
    func didFinishGetDongleNotification(success: Bool, notificationList: [Notificationn]?, notificationDialogList: [Notificationn]?) {}
    
    func didStartGetCarWarranty() {}
    func didFinishGetCarWarranty(success: Bool, date: String?) {}
    
    func didStartGetCarTrips() {}
    func didFinishGetCarTrips(success: Bool, trips: [CarTrip]?) {}
    
    func didStartGetCarMaintanceInfo() {}
    func didFinishGetCarMaintanceInfo(success: Bool, info: [MaintainanceInfo]?) {}
    
    // MARK: alert
    
    func showApiFailureAlert(message: String) {
        showGeneralWarning(title: nil, message: message)
    }
    
    func showGeneralWarning(title: String?, message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "fine".localized, style: .default) { (action:UIAlertAction) in
            Logger.d("do nothing")
        }
        
        alertController.addAction(action1)
        present(alertController, animated: true, completion: nil)
    }
    
    func showVersionUpdate(message: String?, force: Bool, url: String?) {
        let alertController = UIAlertController(title: "title_new_app_version".localized, message: message, preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "update".localized, style: .default) { (action:UIAlertAction) in
            Logger.d("update, url: \(String(describing: url))")
            if let url = url {
                if let link = URL(string: url) {
                    UIApplication.shared.open(link)
                }
            }
        }
        alertController.addAction(action1)
        
        if !force {
            let action2 = UIAlertAction(title: "cancel".localized, style: .cancel) { (action:UIAlertAction) in
                Logger.d("You've pressed cancel");
            }
            alertController.addAction(action2)
        }
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    
    func updateFromKeyboardChangeToFrame(_ keyboardFrame: CGRect) -> CGFloat {
        let constant = view.bounds.height - max(0, view.convert(keyboardFrame, from: nil).origin.y)
        return constant
    }
    
    
    var imageViewNotification: UIImageView?
    func setNoticeImage(with status: ReadStatus) {
        imageViewNotification?.isHighlighted = (status == .unread)
    }
    
    
    
    @objc func updateUnreadCount(notification: NSNotification) {}
    
    // MARK: Floating Btn
    func addFloatingBtnView() {
        if mybtnV != nil {
            return
        }
        mybtnV = UIButton()
        self.view.addSubview(mybtnV!)
        let bottomPadding = UIApplication.shared.keyWindow!.safeAreaInsets.bottom
        mybtnV!.frame = CGRect(
            x: UiUtility.getScreenWidth() - CGFloat(48).getFitSize() - CGFloat(17).getFitSize(),
            y: UiUtility.getScreenHeight() - CGFloat(48 + 40 + 49 + bottomPadding).getFitSize(),
            width: CGFloat(48).getFitSize(),
            height: CGFloat(48).getFitSize()
        )
        mybtnV!.setImage(UIImage(named: "Floating_Button_AST"), for: .normal)
        mybtnV!.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapHandler)))
    }
    
    @objc func tapHandler() {
        UiUtility.goReservation()
    }
    
    func showFloatingBtn(show: Bool) {
        if mybtnV == nil {
            return
        }
        UIView.animate(withDuration: 0.2) {
            if show {
                self.mybtnV!.alpha = 1
            } else {
                self.mybtnV!.alpha = 0
            }
        }
    }
}
