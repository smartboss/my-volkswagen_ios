//
//  NaviViewController.swift
//  myvolkswagen
//
//  Created by Apple on 2/1/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class NaviViewController: BaseViewController, VWNaviBarDelegate {
    
    @IBOutlet weak var upperSafeArea: UIView?
    @IBOutlet weak var naviBar: VWNaviBar!
    
    weak var tabControllerDelegate: TabControllerDelegate?
    weak var tabControllerDelegateV2: TabControllerDelegateV2?

    override func viewDidLoad() {
        super.viewDidLoad()

        naviBar.delegate = self

        upperSafeArea?.backgroundColor = UiUtility.colorBackgroundWhite
    }
    
    // MARK: VWNaviBar delegate
    
    func leftButtonClicked(_ button: UIButton) {
        
    }
    
    func rightButtonClicked(_ button: UIButton) {
        
    }
    
    func rightButton1Clicked(_ button: UIButton) {
        
    }
    
    func rightButton2Clicked(_ button: UIButton) {
        
    }
    
    func rightButtonNotiClicked(_ button: UIButton) {
        
    }
    
    func doneButtonClicked(_ button: UIButton) {
        
    }
    
    func editButtonClicked(_ button: UIButton) {
        
    }
    
    func publishButtonClicked(_ button: UIButton) {
        
    }
    
    func cancelButtonClicked(_ button: UIButton) {
        
    }
    
    func centerButtonClicked() {
        
    }
    
    func leftCancelButtonClicked(_ button: UIButton) {
        
    }
    
    func createButtonClicked(_ button: UIButton) {
        
    }
    
    func segmentDidChangeIndex(index: Int) {
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    func back() {
        navigationController?.popViewController(animated: true)
    }
}
