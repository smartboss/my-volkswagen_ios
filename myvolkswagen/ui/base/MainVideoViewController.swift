//
//  MainVideoViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/29.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

class MainVideoViewController: NaviViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func startBtnPressed(_ sender: Any) {
        let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "OnboardingViewController") as! OnboardingViewController
//        let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "OTPLoginViewController") as! OTPLoginViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
