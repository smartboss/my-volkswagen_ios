//
//  AssistantFilterViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/19.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

protocol AssistantFilterDelegate: class {
    func didSelectAssistantFilter(start: String!, end: String!)
}

class AssistantFilterViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var startbg: UIView!
    @IBOutlet weak var endbg: UIView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var startTxtField: UITextField!
    @IBOutlet weak var endTxtField: UITextField!
    @IBOutlet weak var dateRangeLb: BasicLabel!
    @IBOutlet weak var customDateView: UIView!
    weak var deleagte: AssistantFilterDelegate?
    var selectedSegmentIndex: Int = 0
    var selectedDateStart: String = ""
    var selectedDateEnd: String = ""
    var datePicker1: UIDatePicker!
    var datePicker2: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let (today, fourteenDaysAgo) = calculateDates(dateCount: 14)
        selectedDateStart = fourteenDaysAgo
        selectedDateEnd = today
        reloadDateString()
        
        // 初始化日期選擇器
        datePicker1 = createDatePicker(for: startTxtField)
        datePicker2 = createDatePicker(for: endTxtField)
        
        startbg.layer.cornerRadius = CGFloat(10).getFitSize()
        endbg.layer.cornerRadius = CGFloat(10).getFitSize()
        startbg.layer.borderColor = UIColor.MyTheme.primaryColor000.cgColor
        endbg.layer.borderColor = UIColor.MyTheme.primaryColor000.cgColor
        startbg.layer.borderWidth = 1
        endbg.layer.borderWidth = 1
    }
    
    func reloadDateString() {
        if self.segment.selectedSegmentIndex == 0 {
            let (today, fourteenDaysAgo) = calculateDates(dateCount: 14)
            selectedDateStart = fourteenDaysAgo
            selectedDateEnd = today
            customDateView.isHidden = true
            dateRangeLb.text = "\(selectedDateStart) ~ \(selectedDateEnd)"
        } else if self.segment.selectedSegmentIndex == 1 {
            let (today, fourteenDaysAgo) = calculateDates(dateCount: 30)
            selectedDateStart = fourteenDaysAgo
            selectedDateEnd = today
            customDateView.isHidden = true
            dateRangeLb.text = "\(selectedDateStart) ~ \(selectedDateEnd)"
        } else {
            customDateView.isHidden = false
        }
    }
    
    func getFormattedDate(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY/MM/dd"
        return dateFormatter.string(from: date)
    }
    
    func calculateDates(dateCount: Int) -> (String, String) {
        let calendar = Calendar.current
        let currentDate = Date()
        
        if let dateDaysAgo = calendar.date(byAdding: .day, value: -dateCount, to: currentDate) {
            let currentDateString = getFormattedDate(currentDate)
            let dateDaysAgoString = getFormattedDate(dateDaysAgo)
            
            return (currentDateString, dateDaysAgoString)
        } else {
            return ("", "")
        }
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func switchChanged(_ sender: Any) {
        reloadDateString()
    }
    
    @IBAction func sendBtnPressed(_ sender: Any) {
        if self.segment.selectedSegmentIndex == 0 ||
            self.segment.selectedSegmentIndex == 1 {
            if self.selectedDateStart != "" &&
                self.selectedDateEnd != "" {
                deleagte?.didSelectAssistantFilter(start: self.selectedDateStart, end: self.selectedDateEnd)
                dismiss(animated: true)
            }
        } else {
            if self.startTxtField.text != nil &&
                self.endTxtField.text != nil &&
                self.startTxtField.text != "" &&
                self.endTxtField.text != "" {
                deleagte?.didSelectAssistantFilter(start: self.startTxtField.text, end: self.endTxtField.text)
                dismiss(animated: true)
            } else {
                let customAlert = VWCustomAlert()
                customAlert.alertTitle = "提醒"
                customAlert.alertMessage = "請選擇日期"
                customAlert.alertTag = 1
                customAlert.isCancelButtonHidden = true
                self.present(customAlert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func resetBtnPressed(_ sender: Any) {
        self.startTxtField.text = nil
        self.endTxtField.text = nil
    }
    
    // MARK: - picker
    func createDatePicker(for textField: UITextField) -> UIDatePicker {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        textField.inputView = datePicker
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        datePicker.maximumDate = Date()

        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        // 創建一個 toolbar 包含 "完成" 按鈕
        let toolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.isTranslucent = true
        toolbar.sizeToFit()
        
        if textField == startTxtField {
            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(title: "完成", style: .done, target: self, action: #selector(doneButtonTapped1))
            toolbar.setItems([flexSpace, doneButton], animated: false)
        } else {
            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(title: "完成", style: .done, target: self, action: #selector(doneButtonTapped2))
            toolbar.setItems([flexSpace, doneButton], animated: false)
        }
        
        textField.inputAccessoryView = toolbar
        
        // 設置 text field 代理
        textField.delegate = self
        
        return datePicker
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY/MM/dd"
        
        if sender == datePicker1 {
            startTxtField.text = dateFormatter.string(from: sender.date)
            // 更新picker2的最小日期為picker1的日期
            datePicker2.minimumDate = datePicker1.date
        } else if sender == datePicker2 {
            endTxtField.text = dateFormatter.string(from: sender.date)
        }
    }
    
    @objc func doneButtonTapped1() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY/MM/dd"
        startTxtField.text = dateFormatter.string(from: self.datePicker1.date)
        startTxtField.resignFirstResponder()
    }
    
    @objc func doneButtonTapped2() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY/MM/dd"
        endTxtField.text = dateFormatter.string(from: self.datePicker2.date)
        endTxtField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == startTxtField {
            datePicker2.minimumDate = datePicker1.date
        } else if textField == endTxtField {
        }
    }
}
