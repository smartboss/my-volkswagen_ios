//
//  AssistantRecordCell.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/18.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

class AssistantRecordCell: UITableViewCell {

    @IBOutlet var cellView: UIView!
    @IBOutlet var detailView: UIView!
    @IBOutlet weak var cellBGView: UIView!
    @IBOutlet weak var headerBGView: UIView!
    @IBOutlet weak var milesLb: BasicLabel!
    @IBOutlet weak var timeLb: BasicLabel!
    @IBOutlet weak var dropDownImg: UIImageView!
    @IBOutlet weak var startTimeLb: BasicLabel!
    @IBOutlet weak var startAddress: BasicLabel!
    @IBOutlet weak var endTimeLb: BasicLabel!
    @IBOutlet weak var endAddress: BasicLabel!
    @IBOutlet weak var maxSpeedLb: BasicLabel!
    @IBOutlet weak var drivingTimeLb: BasicLabel!
    @IBOutlet weak var speedbg: UIView!
    @IBOutlet weak var drivingtimebg: UIView!
    @IBOutlet var detailCons: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
        
    }
    
    func setUI(with index: Int) {
//        cellView.setUI(with: Constants.fruits[index])
//        detailView.setUI(with: Constants.fruits[index],
//                         image: UIImage(named: Constants.fruits[index]) ?? UIImage())
    }
    
    func commonInit() {
        selectionStyle = .none
        //detailView.isHidden = true
        speedbg.layer.borderColor = UIColor.MyTheme.primaryColor000.cgColor
        speedbg.layer.borderWidth = 1
        drivingtimebg.layer.borderColor = UIColor.MyTheme.primaryColor000.cgColor
        drivingtimebg.layer.borderWidth = 1
    }
    
}

extension AssistantRecordCell {
    var isDetailViewHidden: Bool {
        return detailView.isHidden
    }

    func showDetailView() {
        //detailView.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            self.dropDownImg.transform = CGAffineTransformMakeRotation(Double.pi)
            self.layoutIfNeeded()
        }) { (completed) in
        }
//        UIView.animate(
//            withDuration: 0.3,
//            delay: 0.0,
//            options: [.curveEaseOut],
//            animations: {
//                self.detailView.isHidden = false
//        })
//        UIView.animate(
//            withDuration: 0.3,
//            delay: 0.0,
//            options: [.curveEaseOut],
//            animations: {
//                self.detailView.alpha = 1
//        })
    }

    func hideDetailView() {
        //detailView.isHidden = true
        UIView.animate(withDuration: 0.3, animations: {
            self.dropDownImg.transform = CGAffineTransform.identity
            self.layoutIfNeeded()
        }) { (completed) in
        }
//        UIView.animate(
//            withDuration: 0.3,
//            delay: 0.0,
//            options: [.curveEaseOut],
//            animations: {
//                self.detailView.alpha = 0.0
//        })
//        UIView.animate(
//            withDuration: 0.3,
//            delay: 0.0,
//            options: [.curveEaseOut],
//            animations: {
//                self.detailView.isHidden = true
//        })
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//        if isDetailViewHidden, selected {
//            showDetailView()
//        } else {
//            hideDetailView()
//        }
//    }
}
