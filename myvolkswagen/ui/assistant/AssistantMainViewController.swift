//
//  AssistantMainViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/18.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
//import GoogleMaps
import WebKit

class AssistantMainViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource, MyGarageDelegate, AssistantFilterDelegate, WKNavigationDelegate, WKUIDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var carNumberPicker: UIView!
    @IBOutlet weak var carNumberLb: UILabel!
    @IBOutlet weak var wkwebview: WKWebView!
    @IBOutlet weak var loadingView: UIView!
    var dongleSummary: DongleSummary?
    var selectedIndexpath: IndexPath?
    var cars: [Car] = []
    var selectedCar: Car?
    var carTrips: [CarTrip]?
    var profile: Profile?
    @IBOutlet weak var topUserNameLb: UILabel!
    var dongleState: CarDoungleState?
    @IBOutlet weak var bindAssistantView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        naviBar.setTitle("".localized)
        naviBar.setRightNotiButton()
        
        let gradientVieww = GradientBackgroundView()
        gradientVieww.frame = view.bounds
        gradientView.addSubview(gradientVieww)
    
        
        self.tableview.register(UINib(nibName: "AssistantInfoCell", bundle: nil), forCellReuseIdentifier: "AssistantInfoCell")

        carNumberPicker.layer.cornerRadius = CGFloat(8).getFitSize()
        carNumberPicker.layer.borderColor = UIColor.MyTheme.greyColor300.cgColor
        carNumberPicker.layer.borderWidth = 1
        
        self.fetchData()
        
        if let myId = UserManager.shared().currentUser.accountId {
            profile = UserManager.shared().getProfile(withId: myId, listener: self)
            self.topUserNameLb.text = "Hi, \(profile?.nickname ?? "")"
        }
        
        wkwebview.navigationDelegate = self
        wkwebview.uiDelegate = self
        wkwebview.allowsBackForwardNavigationGestures = true
        wkwebview.scrollView.maximumZoomScale = 1.0
        wkwebview.scrollView.minimumZoomScale = 1.0
        wkwebview.scrollView.bouncesZoom = false
        wkwebview.scrollView.pinchGestureRecognizer?.isEnabled = false
        wkwebview.scrollView.delegate = self
        
//        NotificationCenter.default.addObserver(self, selector: #selector(reloadWebview), name: NSNotification.Name(rawValue: CarManager.nameCarAssistantReloadNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadCars), name: NSNotification.Name(rawValue: CarManager.nameCarReloadNotification), object: nil)
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
            scrollView.pinchGestureRecognizer?.isEnabled = false
        }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.loadingView.isHidden = false
        if self.carNumberLb.text != CarManager.shared().getSelectedCarNumber() {
            self.carNumberLb.text = nil
            self.fetchData()
        } else {
            self.fetchData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            if readStatus == .read {
                naviBar.btnNoti.setImage(UIImage(named: "noti_btn_read"), for: .normal)
            } else {
                naviBar.btnNoti.setImage(UIImage(named: "noti_btn_unread"), for: .normal)
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(updateUnreadCount), name: NSNotification.Name(rawValue: NoticeManager.nameUnreadCountNotification), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NoticeManager.nameUnreadCountNotification), object: nil)
    }
    
    deinit {
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CarManager.nameCarAssistantReloadNotification), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CarManager.nameCarReloadNotification), object: nil)
    }
    
    override func rightButtonNotiClicked(_ button: UIButton) {
        let sb = UIStoryboard(name: "Car", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "NotificationViewControllerV2") as! NotificationViewControllerV2
        vc.tabSelectedIndex = 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func fetchData() {
        print("assi: fetch data")
        if CarManager.shared().cars.count == 0 {
            carNumberPicker.isHidden = true
            topUserNameLb.isHidden = false
            self.carTrips = []
            self.tableview.reloadData()
            self.loadingView.isHidden = true
            self.bindAssistantView.isHidden = true
            return
        }
        
        carNumberPicker.isHidden = false
        topUserNameLb.isHidden = true
        self.cars = CarManager.shared().cars
        setupSelectedCar()
        if let ci = CarManager.shared().getSelectedCarNumber() {
            UserManager.shared().loginCruisys(listener: self, accountId: UserManager.shared().currentUser.accountId!, plateNum: ci)
        } else {
            self.loadingView.isHidden = true
        }
    }
    
    @objc func reloadCars() {
        print("assi: reloadCars")
        self.cars = CarManager.shared().getCars(remotely: true, listener: self)
    }
    
    @objc func reloadWebview() {
        print("assi: reloadWebview")
        self.cars = CarManager.shared().cars
        if self.cars.count == 0 {
            self.loadingView.isHidden = true
        } else {
            setupSelectedCar()
            UserManager.shared().loginCruisys(listener: self, accountId: UserManager.shared().currentUser.accountId!, plateNum: CarManager.shared().getSelectedCarNumber()!)
        }
    }
    
    func setupWebview() {
        print("assi: setupWebview")
        if CarManager.shared().assistantRedirectUrl != nil && CarManager.shared().assistantRedirectUrl != "" {
            let reUrl = "\(CarManager.shared().assistantRedirectUrl!)&\(getDeeplinkParams())"
            if let urll = URL(string: reUrl) {
                print("assistantRedirectUrl: \(urll)")
                self.wkwebview.load(URLRequest(url: urll))
                self.wkwebview.isHidden = false
                self.wkwebview.alpha = 0
                UIView.animate(withDuration: 0.3) {
                    self.wkwebview.alpha = 100
                } completion: { success in
                    self.loadingView.isHidden = true
                }
                return
            }
        }
        UIView.animate(withDuration: 0.3) {
            self.wkwebview.alpha = 0
        } completion: { success in
            self.wkwebview.isHidden = true
        }
    }
    
    func setupSelectedCar() {
        print("assi: setupSelectedCar")
        if let ci = CarManager.shared().getSelectedCarNumber() {
            self.carNumberPicker.isHidden = false
            for car in self.cars {
                if car.licensePlateNumber == ci {
                    self.selectedCar = car
                    // 選過車
                }
            }
            if self.selectedCar == nil {
                if self.cars.count > 0 { // 選過的車不見了 設定第一台車為預設
                    self.selectedCar = CarManager.shared().cars[0]
                    CarManager.shared().saveSelectedCarNumer(with: CarManager.shared().cars[0].licensePlateNumber!)
                }
            }
            self.carNumberLb.text = self.selectedCar?.licensePlateNumber
        } else {
            // 沒選過車
            if self.cars.count > 0 {
                self.carNumberPicker.isHidden = false
                self.selectedCar = self.cars[0]
                CarManager.shared().saveSelectedCarNumer(with: self.cars[0].licensePlateNumber!)
                
                self.carNumberLb.text = self.selectedCar?.licensePlateNumber
            } else {
                // 沒車
                self.carNumberPicker.isHidden = true
            }
        }
    }
    
    @IBAction func carPickerBtnPressed(_ sender: Any) {
        // 車庫
        let vc = UIStoryboard(name: "Car", bundle: nil).instantiateViewController(withIdentifier: "MyGarageViewController") as! MyGarageViewController
        vc.cars = cars
        vc.delegate = self
        tabControllerDelegateV2?.goViewController(view: vc)
    }
    
    @IBAction func bindAssistantBtnPressed(_ sender: Any) {
        let sb = UIStoryboard(name: "Login", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "BindAssistantViewController") as! BindAssistantViewController
        vc.carNumber = CarManager.shared().getSelectedCarNumber()!
        vc.type = .profile
        self.navigationController?.present(vc, animated: true)
    }
    
    
    // MARK: tableview
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(
                    withIdentifier: "AssistantInfoCell") as? AssistantInfoCell
            else { return UITableViewCell() }
            if CarManager.shared().cars.count == 0 {
                cell.showAddCarView(show: true)
            } else if dongleState != .USER_MAPPED {
                cell.showBindAssiView(show: true)
            } else {
                cell.showAddCarView(show: false)
                cell.totalMiles.text = "\(self.dongleSummary?.odometer?.value ?? 0)"
                cell.fuelLb.text = "\(self.dongleSummary?.fuelLevel?.value ?? 0)"
                cell.coolerTemp.text = "\(self.dongleSummary?.coolantTemperature?.value ?? 0)"
                cell.intakeTemp.text = "\(self.dongleSummary?.intakeAirTemperature?.value ?? 0)"
                cell.batteryLb.text = "\(self.dongleSummary?.controlModuleVoltage?.value ?? 0)"
            }
            return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.cars.count == 0 {
            let sb = UIStoryboard(name: "Login", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "AddCarNavi") as! UINavigationController
            self.navigationController?.present(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func headerViewTapped() {
        let sb = UIStoryboard(name: "Assistant", bundle: nil)
        let bottomSheetVC = sb.instantiateViewController(withIdentifier: "AssistantFilterViewController") as! AssistantFilterViewController
        bottomSheetVC.deleagte = self
        
        let options = SheetOptions(
            pullBarHeight: 8,
            presentingViewCornerRadius: 20,
            shouldExtendBackground: true,
            setIntrinsicHeightOnNavigationControllers: true,
            useFullScreenMode: true,
            shrinkPresentingViewController: true,
            useInlineMode: false,
            horizontalPadding: 0,
            maxWidth: nil
        )
        
        let sheetController = SheetViewController(
            controller: bottomSheetVC,
            sizes: [.fixed(350)])
        
        sheetController.cornerRadius = 20
        sheetController.minimumSpaceAbovePullBar = 0
        sheetController.treatPullBarAsClear = false
        sheetController.dismissOnOverlayTap = true
        sheetController.dismissOnPull = true
        
        sheetController.shouldDismiss = { _ in
            return true
        }
        sheetController.didDismiss = { _ in
        }
        self.present(sheetController, animated: true, completion: nil)
    }
    
    // MARK: profile protocol
    
    override func didStartGetProfile() {
        print("assi: didStartGetProfile")
    }
    
    override func didFinishGetProfile(success: Bool, profile: Profile?) {
        print("assi: didFinishGetProfile")
        dismissHud()
        if success {
            if let p = profile {
                self.profile = p
                self.topUserNameLb.text = "Hi, \(p.nickname ?? "")"
            }
        }
    }
    
    // MARK: MyGarageDelegate
    func didSelectAssistantFilter(start: String!, end: String!) {
        let sb = UIStoryboard(name: "Assistant", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "AssistantFilterResultViewController") as! AssistantFilterResultViewController
        vc.startDate = start
        vc.endDate = end
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: MyGarageDelegate
    func didSelectCarFromGarage() {
        self.cars = CarManager.shared().cars
        setupSelectedCar()
        UserManager.shared().loginCruisys(listener: self, accountId: UserManager.shared().currentUser.accountId!, plateNum: CarManager.shared().getSelectedCarNumber()!)
    }
    
    // MARK: user protocol
    override func didStartLoginCruisys() {
        print("assi: didStartLoginCruisys")
        showAssiBGHud()
    }
    
    override func didFinishLoginCruisys(success: Bool) {
        print("assi: didFinishLoginCruisys")
        dismissHud()
        if success {
            CarManager.shared().getDongleBindingInfo(listener: self, plateNo: CarManager.shared().getSelectedCarNumber()!)
        } else {
            showApiFailureAlert(message: "發生錯誤，請稍後再試")
        }
    }
    
    // MARK: car protocol
    
    override func didStartGetCar() {
        print("assi: didStartGetCar")
        showAssiBGHud()
    }
    
    override func didFinishGetCar(success: Bool, cars: [Car]) {
        print("assi: didFinishGetCar")
        dismissHud()
        
        if success {
            fetchData()
        }
    }
    
    override func didStartGetDongleBinding() {
        print("assi: didStartGetDongleBinding")
        showBGHud()
    }
    
    override func didFinishGetDongleBinding(state: CarDoungleState?, dongleData: DongleData?) {
        print("assi: didFinishGetDongleBinding")
        dismissHud()
        self.dongleState = state
        if dongleData?.redirectUrl != nil && dongleData?.redirectUrl != "" {
            CarManager.shared().assistantRedirectUrl = dongleData?.redirectUrl!
        }
        if state == .VIN_MAPPED {
            self.bindAssistantView.isHidden = false
        } else {
            self.bindAssistantView.isHidden = true
        }
        if state == .USER_MAPPED {
            if CarManager.shared().assistantRedirectUrl != nil && CarManager.shared().assistantRedirectUrl != "" {
                self.setupWebview()
            } else {
                CarManager.shared().getDongleSummary(listener: self, plateNo: CarManager.shared().getSelectedCarNumber()!)
                self.loadingView.isHidden = true
            }
        } else {
            // show empty view
            dismissHud()
            self.wkwebview.isHidden = true
            self.tableview.reloadData()
            self.loadingView.isHidden = true
        }
    }
    
    override func didStartGetDongleSummary() {
        print("assi: didStartGetDongleSummary")
    }
    
    override func didFinishGetDongleSummary(data: DongleSummary?) {
        print("assi: didFinishGetDongleSummary")
        self.dismissHud()
        self.dongleSummary = data
        self.tableview.reloadData()
    }
    
    override func didStartGetCarTrips() {
        print("assi: didStartGetCarTrips")
    }
    
    override func didFinishGetCarTrips(success: Bool, trips: [CarTrip]?) {
        print("assi: didFinishGetCarTrips")
        self.dismissHud()
        carTrips = trips
        self.tableview.reloadData()
    }
    
    // MARK: WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    }
    
    func webView(_ webView: WKWebView,
                 didFail navigation: WKNavigation!,
                 withError error: Error) {
    }
    
    func webView(_ webView: WKWebView,
                 didFailProvisionalNavigation navigation: WKNavigation!,
                 withError error: Error) {
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: ((WKNavigationActionPolicy) -> Void)) {
 
        let urll = navigationAction.request.url!
        if Deeplinker.handleDeeplink(url: urll, ref: nil) {
            print("start deeplink: \(urll)")
            Deeplinker.checkDeepLink()
        }

        decisionHandler(.allow)
    }
    
    // MARK: - JS
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {
        
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (action) in
            completionHandler()
        }))
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (Bool) -> Void) {
        
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (action) in
            completionHandler(true)
        }))
        
        alertController.addAction(UIAlertAction(title: "取消", style: .default, handler: { (action) in
            completionHandler(false)
        }))
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (String?) -> Void) {
        
        let alertController = UIAlertController(title: nil, message: prompt, preferredStyle: .actionSheet)
        
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        
        alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "取消", style: .default, handler: { (action) in
            completionHandler(nil)
        }))
        
        present(alertController, animated: true, completion: nil)
    }
}
