//
//  AssistantFilterResultViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/19.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

class AssistantFilterResultViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var resultLb: UILabel!
    var selectedIndexpath: IndexPath?
    var carTrips: [CarTrip]?
    var startDate: String!
    var endDate: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        naviBar.setTitle("行駛紀錄篩選")
        
        let gradientVieww = GradientBackgroundView()
        gradientVieww.frame = view.bounds
        gradientView.addSubview(gradientVieww)
        
        naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
        
        self.tableview.estimatedRowHeight = 100
        self.tableview.register(UINib(nibName: "AssistantRecordCell", bundle: nil),
                                forCellReuseIdentifier: "AssistantRecordCell")
        
        CarManager.shared().getCarTrip(listener: self, plateNo: CarManager.shared().getSelectedCarNumber()!, startDate: startDate, endDate: endDate, page: "1", perPage: "10")
        
        resultLb.text = "篩選結果 \(startDate!) ~ \(endDate!)"
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    // MARK: Car protocol
    override func didStartGetCarTrips() {
        showHud()
    }
    
    override func didFinishGetCarTrips(success: Bool, trips: [CarTrip]?) {
        dismissHud()
        if trips == nil || trips?.count == 0 {
            self.emptyView.isHidden = false
        } else {
            carTrips = trips
            self.tableview.reloadData()
        }
    }
    
    // MARK: tableview
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let ct = carTrips {
            return ct.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: "AssistantRecordCell") as? AssistantRecordCell
        else { return UITableViewCell() }
        
        let ctt = carTrips![indexPath.row]
        
        if selectedIndexpath?.row == indexPath.row {
            cell.detailView.isHidden = false
            cell.detailCons.isActive = true
            cell.dropDownImg.transform = CGAffineTransformMakeRotation(Double.pi)
            cell.headerBGView.layer.cornerRadius = 0
            cell.cellBGView.layer.cornerRadius = CGFloat(12).getFitSize()
        } else {
            cell.detailView.isHidden = true
            cell.detailCons.isActive = false
            cell.dropDownImg.transform = CGAffineTransform.identity
            cell.headerBGView.layer.cornerRadius = CGFloat(12).getFitSize()
            cell.cellBGView.layer.cornerRadius = 0
        }
        cell.timeLb.text = ctt.startTime
        cell.startTimeLb.text = ctt.startTime?.extractTime()
        cell.endTimeLb.text = ctt.endTime?.extractTime()
        cell.startAddress.text = ctt.startAddress
        cell.endAddress.text = ctt.endAddress
        cell.milesLb.text = ctt.distance?.metersToKilometers()
        cell.maxSpeedLb.text = "\(ctt.maxSpeed ?? 0)"
        cell.drivingTimeLb.text = "行駛時間 \(ctt.drivingTime ?? 0) 分鐘"
        
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let previousRow = self.selectedIndexpath
        let tripp = carTrips![indexPath.row]
        // 1 ----
//        let geocoder = GMSGeocoder()
//        var addressResult: GMSAddress?
//        let coordinate = CLLocationCoordinate2D(latitude: tripp.startLocation!.latitude!, longitude: tripp.startLocation!.longitude!)
//        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
//            if let address = response?.firstResult() {
//                addressResult = address
//                if let address = addressResult {
//                    let lines = address.lines
//                    if let addressString = lines?.joined(separator: "\n") {
//                        self.carTrips![indexPath.row].startAddress = addressString
//                        if previousRow != nil {
//                            self.tableview.reloadRows(at: [indexPath, previousRow!], with: .automatic)
//                        } else {
//                            self.tableview.reloadRows(at: [indexPath], with: .automatic)
//                        }
//                    }
//                } else {
//                }
//            }
//
//        }
        
        // 2 ----
//        var addressResult2: GMSAddress?
//        let coordinate2 = CLLocationCoordinate2D(latitude: tripp.endLocation!.latitude!, longitude: tripp.endLocation!.longitude!)
//        geocoder.reverseGeocodeCoordinate(coordinate2) { response, error in
//            if let address = response?.firstResult() {
//                addressResult2 = address
//                if let address = addressResult2 {
//                    let lines = address.lines
//                    if let addressString = lines?.joined(separator: "\n") {
//                        self.carTrips![indexPath.row].endAddress = addressString
//                        if previousRow != nil {
//                            self.tableview.reloadRows(at: [indexPath, previousRow!], with: .automatic)
//                        } else {
//                            self.tableview.reloadRows(at: [indexPath], with: .automatic)
//                        }
//                    }
//                } else {
//                }
//            }
//        }
        
        if indexPath.row == self.selectedIndexpath?.row {
            self.selectedIndexpath = nil
        } else {
            self.selectedIndexpath = indexPath
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
