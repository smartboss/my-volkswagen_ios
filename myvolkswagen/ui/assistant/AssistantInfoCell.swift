//
//  AssistantInfoCell.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/19.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
import SnapKit

class AssistantInfoCell: UITableViewCell {

    @IBOutlet weak var totalMiles: BasicLabel!
    @IBOutlet weak var fuelLb: BasicLabel!
    @IBOutlet weak var coolerTemp: BasicLabel!
    @IBOutlet weak var intakeTemp: BasicLabel!
    @IBOutlet weak var batteryLb: BasicLabel!
    @IBOutlet weak var topLb: BasicLabel!
    @IBOutlet var bgview: UIView!
    @IBOutlet var addCarView: UIView!
    @IBOutlet weak var infoTitle: BasicLabel!
    @IBOutlet weak var infoDes: BasicLabel!
    @IBOutlet weak var addCarBtn: BasicSizingButton!
    @IBOutlet weak var notReadyLb: BasicLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bgview.layer.cornerRadius = CGFloat(12).getFitSize()
        bgview.removeFromSuperview()
        addCarView.removeFromSuperview()
        notReadyLb.isHidden = true
    }

    func showAddCarView(show: Bool) {
        if show {
            infoTitle.text = "My Volkswagen App"
            infoDes.text = "新增愛車即時掌握車輛情報，輕鬆線上預約回廠。"
            bgview.removeFromSuperview()
            self.contentView.addSubview(addCarView)
            addCarView.snp.makeConstraints { make in
                make.bottom.equalToSuperview()
                make.top.equalTo(self.topLb.snp.bottom).offset(24)
                make.leading.equalToSuperview().offset(24)
                make.trailing.equalToSuperview().offset(-24)
            }
            addCarBtn.isHidden = false
            notReadyLb.isHidden = true
        } else {
            addCarView.removeFromSuperview()
            self.contentView.addSubview(bgview)
            bgview.snp.makeConstraints { make in
                make.bottom.equalToSuperview()
                make.leading.equalToSuperview().offset(24)
                make.trailing.equalToSuperview().offset(-24)
                make.top.equalTo(self.topLb.snp.bottom).offset(24)
            }
            notReadyLb.isHidden = true
        }
    }
    
    func showBindAssiView(show: Bool) {
        if show {
            infoTitle.text = "您尚未綁定 Volkswagen 行車助理"
            infoDes.text = "透過 Volkswagen 行車助理，讓您隨時掌握愛車行駛總里程、剩餘油耗、冷卻液溫度、進氣溫度、電量等詳細記錄。"
            bgview.removeFromSuperview()
            addCarBtn.isHidden = true
            self.contentView.addSubview(addCarView)
            addCarView.snp.makeConstraints { make in
                make.bottom.equalToSuperview()
                make.top.equalTo(self.topLb.snp.bottom).offset(24)
                make.leading.equalToSuperview().offset(24)
                make.trailing.equalToSuperview().offset(-24)
            }
            notReadyLb.isHidden = false
        } else {
            addCarView.removeFromSuperview()
            self.contentView.addSubview(bgview)
            bgview.snp.makeConstraints { make in
                make.bottom.equalToSuperview()
                make.leading.equalToSuperview().offset(24)
                make.trailing.equalToSuperview().offset(-24)
                make.top.equalTo(self.topLb.snp.bottom).offset(24)
            }
            notReadyLb.isHidden = true
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
