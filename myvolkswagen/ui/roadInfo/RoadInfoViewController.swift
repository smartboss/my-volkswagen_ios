//
//  RoadInfoViewController.swift
//  myvolkswagen
//
//  Created by Apple on 7/4/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Kingfisher
import CoreLocation

class RoadInfoViewController: NaviViewController, RoadProtocol, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var pathSelectedIndex: Int = 0
    
    @IBOutlet weak var labelTitle0: UILabel!
    @IBOutlet weak var labelTitle1: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var locationManager: CLLocationManager = CLLocationManager()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UiUtility.colorBackgroundWhite
        
        naviBar.setTitle("road_info_title".localized)
        naviBar.setLeftButton(UIImage(named: "back"), highlighted: UIImage(named: "back"))
        naviBar.setRightButton(UIImage(named: "refresh"), highlighted: UIImage(named: "refresh"))
        naviBar.setRightButton1(UIImage(named: "target"), highlighted: UIImage(named: "target"))
        
        labelTitle0.isHidden = true
        labelTitle0.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
        labelTitle0.textColor = .black
        
        labelTitle1.isHidden = true
        labelTitle1.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
        labelTitle1.textColor = .black
        
        
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        RoadManager.shared().registerListener(listener: self)
        RoadManager.shared().collectDataIfNeededV2(init: true)
        
        if RoadManager.shared().dataReady() {
            refreshUI(scrollToTop: false)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        RoadManager.shared().unregisterListener(listener: self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    // MARK: road protocol
    
    func didStartSyncRoadInfo() {
//        Logger.d("didStartSyncRoadInfo")
        self.showHud()
    }
    
    func didFinishSyncRoadInfo(success: Bool) {
//        Logger.d("didFinishSyncRoadInfo")
        self.dismissHud()
        
        if RoadManager.shared().dataReady() {
            refreshUI(scrollToTop: false)
        }
    }
    
    
    
    // MARK: VWNaviBar delegate
    
    override func leftButtonClicked(_ button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func rightButtonClicked(_ button: UIButton) {
        RoadManager.shared().collectDataIfNeededV2(init: true)
    }
    
    override func rightButton1Clicked(_ button: UIButton) {
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        showHud()
    }
    
    
    
    // MARK: collection view delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        Logger.d("RoadManager.shared().getPaths().count: \(RoadManager.shared().getPaths().count)")
        return RoadManager.shared().getPaths().count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: UiUtility.adaptiveSize(size: 22), bottom: 0, right: UiUtility.adaptiveSize(size: 22))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return UiUtility.adaptiveSize(size: 14)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UiUtility.adaptiveSize(size: 54), height: collectionView.frame.height)
    }
    
    //    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    //        Logger.d("indexPath: \(indexPath)")
    ////        self.pageControl.currentPage = indexPath.section
    //    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PathCell", for: indexPath)
        
//        cell.backgroundColor = .green
        let imageView: UIImageView = cell.viewWithTag(1) as! UIImageView
        let vUnderline: UIView = cell.viewWithTag(2)!
        
        let path = RoadManager.shared().getPaths()[indexPath.row]
        
        let url = URL(string: path.icon)
        imageView.kf.setImage(with: url)
        
        vUnderline.isHidden = (pathSelectedIndex != indexPath.row)
        vUnderline.layer.cornerRadius = UiUtility.adaptiveSize(size: 2)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        pathSelectedIndex = indexPath.row
        refreshUI(scrollToTop: true)
    }
    
    
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let paths = RoadManager.shared().getPaths()
        if paths.count == 0 {
            return 0
        } else {
            let path = paths[pathSelectedIndex]
            return path.points.count * 2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let path: Path = RoadManager.shared().getPaths()[pathSelectedIndex]
        if indexPath.row == path.points.count * 2 - 1 {
            return UiUtility.adaptiveSize(size: 56)
        } else if indexPath.row % 2 == 0 {
            return UiUtility.adaptiveSize(size: 36)
        } else {
            return UiUtility.adaptiveSize(size: 58)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        let path: Path = RoadManager.shared().getPaths()[pathSelectedIndex]
        if indexPath.row == path.points.count * 2 - 1 {
            cell = tableView.dequeueReusableCell(withIdentifier: "TimestampCell", for: indexPath)
            let label: UILabel = cell.viewWithTag(1) as! UILabel
            
            label.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 12))
            let ts = RoadManager.shared().getLastUpdate()
            label.text = "\("road_info_last_updated".localized)\(dateText(date: ts))"
        } else if indexPath.row % 2 == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "PointCell", for: indexPath)
            
            let vContainer: UIView = cell.viewWithTag(1)!
            let labelPointName: UILabel = cell.viewWithTag(2) as! UILabel
            let labelKM: UILabel = cell.viewWithTag(3) as! UILabel
            let vIntersection: UIView = cell.viewWithTag(4)!
            let btnPathIcon: UIButton = cell.viewWithTag(5) as! UIButton
            
            vContainer.layer.cornerRadius = UiUtility.adaptiveSize(size: 18)
            
            let point = path.points[indexPath.row / 2]
            
            labelPointName.font = UIFont(name: "PingFangTC-Semibold", size: UiUtility.adaptiveSize(size: 22))
            labelPointName.textColor = UiUtility.colorBackgroundWhite
            labelPointName.text = point.name
            
            labelKM.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
            labelKM.textColor = UiUtility.colorBackgroundWhite
            labelKM.text = RoadManager.shared().getKMInfo(from: point.id)
            
            vIntersection.isHidden = true
            btnPathIcon.isHidden = true
            if let intersections = point.intersections {
                if intersections.count > 0 {
                    let pointId = intersections[0]
                    if let path = RoadManager.shared().getPath(of: pointId) {
                        let url = URL(string: path.icon)
                        btnPathIcon.kf.setImage(with: url, for: .normal)
                        vIntersection.isHidden = false
                        btnPathIcon.isHidden = false
                        
                        btnPathIcon.addTarget(self, action: #selector(goIntersection(_:)), for: .touchUpInside)
                    }
                }
            }
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "RouteCell", for: indexPath)
            
            let btnCameraL: UIButton = cell.viewWithTag(1) as! UIButton
            let btnCameraR: UIButton = cell.viewWithTag(2) as! UIButton
            let ivSpeedL: UIImageView = cell.viewWithTag(3) as! UIImageView
            let ivSpeedR: UIImageView = cell.viewWithTag(4) as! UIImageView
            let lbL: UILabel = cell.viewWithTag(5) as! UILabel
            let lbR: UILabel = cell.viewWithTag(6) as! UILabel
            
            lbL.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
            lbL.textColor = .white
            lbR.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
            lbR.textColor = .white
            
            let previousPoint = path.points[(indexPath.row - 1) / 2]
            let nextPoint = path.points[(indexPath.row + 1) / 2]
            
            btnCameraL.isHidden = true
            btnCameraR.isHidden = true
            ivSpeedL.image = UIImage.init(named: "rectangle17Copy16")
            ivSpeedL.isHidden = false
            ivSpeedR.image = UIImage.init(named: "rectangle17Copy15")
            ivSpeedR.isHidden = false
            lbL.isHidden = true
            lbR.isHidden = true
            
            let routeL: RoadDynamicInfo? = RoadManager.shared().getRouteBy(startPointId: previousPoint.id, endPointId: nextPoint.id)
            if let routeL = routeL {
                lbL.text = "\(routeL.value)"
                
                if routeL.level == 1 {
                    ivSpeedL.image = UIImage.init(named: "rectangle17Copy11")
                    ivSpeedL.isHidden = false
                    lbL.isHidden = false
                } else if routeL.level == 2 {
                    ivSpeedL.image = UIImage.init(named: "rectangle17Copy20")
                    ivSpeedL.isHidden = false
                    lbL.isHidden = false
                } else if routeL.level == 3 {
                    ivSpeedL.image = UIImage.init(named: "rectangle17Copy12")
                    ivSpeedL.isHidden = false
                    lbL.isHidden = false
                } else if routeL.level == 4 {
                    ivSpeedL.image = UIImage.init(named: "rectangle17Copy13")
                    ivSpeedL.isHidden = false
                    lbL.isHidden = false
                } else if routeL.level == 5 {
                    ivSpeedL.image = UIImage.init(named: "rectangle17Copy14")
                    ivSpeedL.isHidden = false
                    lbL.isHidden = false
                }
                
                if let cCTVs: [CCTVDynamicInfo] = RoadManager.shared().getCCTVsBy(startPointId: previousPoint.id, endPointId: nextPoint.id) {
                    if cCTVs.count == 0 {
                        Logger.d("cctv count should not be 0")
                    } else {
                        btnCameraL.isHidden = false
                        btnCameraL.addTarget(self, action: #selector(goCameraL(_:)), for: .touchUpInside)
                    }
                }
            }
            
            let routeR: RoadDynamicInfo? = RoadManager.shared().getRouteBy(startPointId: nextPoint.id, endPointId: previousPoint.id)
            if let routeR = routeR {
                lbR.text = "\(routeR.value)"
                
                if routeR.level == 1 {
                    ivSpeedR.image = UIImage.init(named: "rectangle17Copy7")
                    ivSpeedR.isHidden = false
                    lbR.isHidden = false
                } else if routeR.level == 2 {
                    ivSpeedR.image = UIImage.init(named: "rectangle17Copy19")
                    ivSpeedR.isHidden = false
                    lbR.isHidden = false
                } else if routeR.level == 3 {
                    ivSpeedR.image = UIImage.init(named: "rectangle17Copy8")
                    ivSpeedR.isHidden = false
                    lbR.isHidden = false
                } else if routeR.level == 4 {
                    ivSpeedR.image = UIImage.init(named: "rectangle17Copy9")
                    ivSpeedR.isHidden = false
                    lbR.isHidden = false
                } else if routeR.level == 5 {
                    ivSpeedR.image = UIImage.init(named: "rectangle17Copy10")
                    ivSpeedR.isHidden = false
                    lbR.isHidden = false
                }
                
                if let cCTVs: [CCTVDynamicInfo] = RoadManager.shared().getCCTVsBy(startPointId: nextPoint.id, endPointId: previousPoint.id) {
                    if cCTVs.count == 0 {
                        Logger.d("cctv count should not be 0")
                    } else {
                        btnCameraR.isHidden = false
                        btnCameraR.addTarget(self, action: #selector(goCameraR(_:)), for: .touchUpInside)
                    }
                }
            }
        }
        
        

        
        cell.selectionStyle = .none
        return cell
    }
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        Logger.d("You tapped cell number \(indexPath.row).")
    ////        tableView.deselectRow(at: indexPath, animated: true)
    //    }
    
    
    
    // MARK: location manager delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let latestLocation: CLLocation = locations[locations.count - 1]
        Logger.d("latestLocation: \(latestLocation)")
        
        locationManager.stopUpdatingLocation()
        
        let path = RoadManager.shared().getPaths()[pathSelectedIndex]
        let points: [Point] = path.points
        var locations: [CLLocation] = []
        for point in points {
            let location: CLLocation = CLLocation.init(latitude: point.lat, longitude: point.lon)
            locations.append(location)
        }
        
        var closestLocation: CLLocation?
        var smallestDistance: CLLocationDistance?
        var closestPoint: Point?
        var closestIndex: Int = 0
        
        var index = 0
        for location in locations {
            let distance = latestLocation.distance(from: location)
            if smallestDistance == nil || distance < smallestDistance! {
                closestLocation = location
                smallestDistance = distance
                
                closestPoint = points[index]
                closestIndex = index
            }
            
            index = index + 1
        }
        
        Logger.d("closestLocation: \(String(describing: closestLocation))")
        Logger.d("smallestDistance: \(String(describing: smallestDistance))")
        Logger.d("closest point: \(String(describing: closestPoint))")
        
        dismissHud()
        
        tableView.scrollToRow(at: IndexPath.init(row: closestIndex * 2, section: 0), at: .middle, animated: true)
//        latitude.text = String(format: "%.4f", latestLocation.coordinate.latitude)
//        longitude.text = String(format: "%.4f", latestLocation.coordinate.longitude)
//        hAccuracy.text = String(format: "%.4f", latestLocation.horizontalAccuracy)
//        altitude.text = String(format: "%.4f", latestLocation.altitude)
//        vAccuracy.text = String(format: "%.4f", latestLocation.verticalAccuracy)
//
//        if startLocation == nil {
//            startLocation = latestLocation
//        }
//
//        let distanceBetween: CLLocationDistance = latestLocation.distance(from: startLocation)
//
//        distance.text = String(format: "%.2f", distanceBetween)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Logger.d("error: \(error.localizedDescription)")
        dismissHud()
    }

    
    
    // MARK: private methods
    
    private func refreshUI(scrollToTop: Bool) {
        Logger.d("refreshUI")
        collectionView.reloadData()
        
        let path = RoadManager.shared().getPaths()[pathSelectedIndex]
        labelTitle0.isHidden = false
        labelTitle1.isHidden = false
        labelTitle0.text = path.direction == 0 ? "to_south".localized : "to_east".localized
        labelTitle1.text = path.direction == 0 ? "to_north".localized : "to_west".localized
        
        UIView.animate(withDuration: 0, animations: {
            self.tableView.reloadData()
        }, completion:{ _ in
            if scrollToTop {
                self.tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: false)
            }
        })
    }
    
    private func dateText(date: Date?) -> String {
        let dateFormatter = DateFormatter()
        //        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm"
        
        if let date = date {
            return dateFormatter.string(from: date)
        }
        
        return ""
    }
    
    @objc private func goCameraR(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            goCamera(isLeft: false, indexPath: indexPath)
        }
    }
    
    @objc private func goCameraL(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            goCamera(isLeft: true, indexPath: indexPath)
        }
    }
    
    private func goCamera(isLeft: Bool, indexPath: IndexPath) {
        let paths = RoadManager.shared().getPaths()
        let path: Path = paths[pathSelectedIndex]
        
        let previousPoint = path.points[(indexPath.row - 1) / 2]
        let nextPoint = path.points[(indexPath.row + 1) / 2]
        
        let route: RoadDynamicInfo? = isLeft ? RoadManager.shared().getRouteBy(startPointId: previousPoint.id, endPointId: nextPoint.id) : RoadManager.shared().getRouteBy(startPointId: nextPoint.id, endPointId: previousPoint.id)
        if let _ = route {
            if let cCTVs: [CCTVDynamicInfo] = isLeft ? RoadManager.shared().getCCTVsBy(startPointId: previousPoint.id, endPointId: nextPoint.id) : RoadManager.shared().getCCTVsBy(startPointId: nextPoint.id, endPointId: previousPoint.id) {
                if cCTVs.count == 0 {
                    Logger.d("cctv count should not be 0")
                } else {
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RoadCameraViewController") as! RoadCameraViewController
                    vc.point = previousPoint
                    vc.cameras = cCTVs
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    @objc private func goIntersection(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            let paths = RoadManager.shared().getPaths()
            let path: Path = paths[pathSelectedIndex]
            let point = path.points[indexPath.row / 2]
            if let intersections = point.intersections {
                if intersections.count > 0 {
                    let pointId = intersections[0]
                    if let path = RoadManager.shared().getPath(of: pointId) {
//                        Logger.d("go to path: \(path)")
                        
                        var idx: Int = 0
                        for p in paths {
                            if p.id == path.id {
//                                Logger.d("idx: \(idx)")
                                collectionView.scrollToItem(at: IndexPath.init(row: idx, section: 0), at: .centeredHorizontally, animated: true)
                                pathSelectedIndex = idx
                                refreshUI(scrollToTop: true)
                                break
                            }
                            idx = idx + 1
                        }
                    }
                }
            }
        }
    }
}
