//
//  RoadCameraViewController.swift
//  myvolkswagen
//
//  Created by CHUNG CHING JIANG on 2019/7/12.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import WebKit

class RoadCameraViewController: NaviViewController, WKNavigationDelegate {
    
    var point: Point!
    var cameras: [CCTVDynamicInfo]!
    
    @IBOutlet weak var imageViewError: UIImageView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var labelError: UILabel!
    var webView: WKWebView!
    
    @IBOutlet weak var labelInfo: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UiUtility.colorBackgroundWhite
        
        naviBar.setTitle(point.name)
        naviBar.setLeftButton(UIImage(named: "back"), highlighted: UIImage(named: "back"))
        
        labelError.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
        labelError.textColor = UiUtility.colorDarkGrey
        labelError.text = "road_camera_error".localized
        
        labelInfo.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
        labelInfo.textColor = .black
        let cctv = cameras[0]
        let splittedArray = cctv.cctvid.split(separator: "-")
        Logger.d(splittedArray)
        if splittedArray.count == 5 {
            var roadName = splittedArray[1].replacingOccurrences(of: "N", with: "road_camera_name_n".localized)
            roadName = roadName.replacingOccurrences(of: "T", with: "road_camera_name_t".localized)
            
            var direction = splittedArray[2].replacingOccurrences(of: "N", with: "to_north".localized)
            direction = direction.replacingOccurrences(of: "S", with: "to_south".localized)
            direction = direction.replacingOccurrences(of: "E", with: "to_east".localized)
            direction = direction.replacingOccurrences(of: "W", with: "to_west".localized)
            
            let info = "\(roadName)\("road_camera_comma".localized)\(direction)\("road_camera_comma".localized)\(splittedArray[3])K"
            labelInfo.text = info
        } else {
            labelInfo.text = cctv.cctvid
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        let cctv = cameras[0]
        if cctv.status != 0 {
//            webView.isHidden = true
            imageViewError.isHidden = false
            labelError.isHidden = false
        } else {
            imageViewError.isHidden = true
            labelError.isHidden = true
            
            let configuration = WKWebViewConfiguration()
            configuration.setURLSchemeHandler(CustomSchemeHandler(), forURLScheme: Config.redirectScheme)
            let preferences = WKPreferences()
            preferences.javaScriptEnabled = true
            configuration.preferences = preferences
            webView = WKWebView(frame: self.viewContainer.bounds, configuration: configuration)
            self.viewContainer.addSubview(webView)
            
            webView.navigationDelegate = self
            webView.allowsBackForwardNavigationGestures = true
            
            Logger.d("cctv.url: \(cctv.url)")
            let url = URL(string: cctv.url)!
//            let replaced = cctv.url.replacingOccurrences(of: "http:", with: "https:")
//            Logger.d("replaced cctv.url: \(replaced)")
//            let url = URL(string: replaced)!
            self.webView.load(URLRequest(url: url))
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    // MARK: VWNaviBar delegate
    
    override func leftButtonClicked(_ button: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    // MARK: WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        //        Logger.d("didCommit")
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        //        Logger.d("didStartProvisionalNavigation")
        //        showHud()
    }
    
    func webView(_ webView: WKWebView,
                 didFail navigation: WKNavigation!,
                 withError error: Error) {
        Logger.d("didFail")
    }
    
    func webView(_ webView: WKWebView,
                 didFailProvisionalNavigation navigation: WKNavigation!,
                 withError error: Error) {
        Logger.d("didFailProvisionalNavigation, error: \(error)")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        //        Logger.d("didFinish")
        //        dismissHud()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: ((WKNavigationActionPolicy) -> Void)) {
        decisionHandler(.allow)
    }
    
//    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
//        let cred = URLCredential(trust: challenge.protectionSpace.serverTrust!)
//        completionHandler(.useCredential, cred)
//    }

}
