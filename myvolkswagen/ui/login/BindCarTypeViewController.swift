//
//  BindCardTypeViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/13.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
import Kingfisher

class BindCarTypeViewController: NaviViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var progressImg: UIImageView!
    @IBOutlet weak var profileNaviBar: UIView!
    @IBOutlet weak var gestureLineView: UIView!
    @IBOutlet weak var carImgView: UIImageView!
    @IBOutlet weak var carLb: BasicLabel!
    @IBOutlet weak var carNumLb: BasicLabel!
    @IBOutlet weak var carModelTxtField: VWTextField!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var viewPickerContainer: UIView!
    @IBOutlet weak var buttonPickerDone: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var skipBtn: VWButtonWhite!
    var type: EditProfileType2 = .profile
    var carNumber: String!
    
    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()

        naviBar.setTitle("新增愛車".localized)
        naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
        
        setupUI()
        updateAdd2Ui()
    }
    
    func setupUI() {
        if type == .login {
            profileNaviBar.isHidden = true
            let gradientVieww = GradientBackgroundView()
            gradientVieww.frame = view.bounds
            gradientView.addSubview(gradientVieww)
        } else {
            gestureLineView.layer.cornerRadius = gestureLineView.frame.height / 2
            gradientView.backgroundColor = UIColor.white
            skipBtn.removeFromSuperview()
            progressImg.removeFromSuperview()
        }
    }
    
    private func updateAdd2Ui() {
        let addingCar = CarManager.shared().getAddingCar()!
        carLb.text = addingCar.typeName
        carNumLb.text = "\(addingCar.platePrefix ?? "")-\(addingCar.plateSuffix ?? "")"
        if let mo = addingCar.models {
            if let urll = URL(string: mo[addingCar.selectedModelIndex].image ?? "") {
                carImgView.kf.setImage(with: urll)
            }
        }
        
        carModelTxtField.text = addingCar.models![addingCar.selectedModelIndex].name
        //pickerView.reloadComponent(0)
        pickerView.reloadAllComponents()
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        CarManager.shared().addCar(listener: self)
    }
    
    @IBAction func skipBtnPressed(_ sender: Any) {
        CarManager.shared().deleteAddingCar()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.updateRootVC(showAlert: false)
    }
    
    // MARK: - Profile Navi Bar
    @IBAction func profileNaviBackBtnPressed(_ sender: Any) {
        back()
    }
    
    @IBAction func profileNaviCloseBtnPressed(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    // MARK: Add Car
    override func didStartAddCar() {
        showHud()
    }
    
    override func didFinishAddCar(success: Bool) {
        dismissHud()
        
        let addingCar = CarManager.shared().getAddingCar()!
        if success {
            if type == .login {
                self.carNumber = "\(addingCar.platePrefix ?? "")-\(addingCar.plateSuffix ?? "")"
                UserManager.shared().loginCruisys(listener: self, accountId: UserManager.shared().currentUser.accountId!, plateNum: carNumber)
            } else {
                self.dismiss(animated: true)
            }
        }
        CarManager.shared().deleteAddingCar()
    }
    
    override func didStartLoginCruisys() {
        showHud()
    }
    
    override func didFinishLoginCruisys(success: Bool) {
        dismissHud()
        print("--- didFinishLoginCruisys ---")
        if success {
            CarManager.shared().getDongleBindingInfo(listener: self, plateNo: self.carNumber)
        } else {
            showApiFailureAlert(message: "發生錯誤，請稍後再試")
        }
    }
    
    // MARK: DongleBinding
    override func didStartGetDongleBinding() {
        showHud()
    }
    
    override func didFinishGetDongleBinding(state: CarDoungleState?, dongleData: DongleData?) {
        dismissHud()
//        if state == nil && dongleData == nil {
//            return
//        }
        
        if state == nil {
            let sb = UIStoryboard(name: "Login", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "BindAssistantViewController") as! BindAssistantViewController
            vc.carNumber = self.carNumber
            vc.dongleData = dongleData
            vc.statee = state
            vc.type = .login
            self.navigationController?.pushViewController(vc, animated: true)
            return
        }
        
        let dongleData = dongleData
        let statee = state
        if state == .VIN_MAPPED { // 未綁定行車助理
            let sb = UIStoryboard(name: "Login", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "BindAssistantViewController") as! BindAssistantViewController
            vc.carNumber = self.carNumber
            vc.dongleData = dongleData
            vc.statee = state
            vc.type = .login
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let sb = UIStoryboard(name: "Login", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "BindAssistantViewController") as! BindAssistantViewController
            vc.carNumber = self.carNumber
            vc.dongleData = dongleData
            vc.statee = state
            vc.type = .login
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: picker view delegate
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if let addingCar = CarManager.shared().getAddingCar() {
            if let models = addingCar.models {
                return models.count
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let addingCar = CarManager.shared().getAddingCar()!
        return addingCar.models![row].name
    }
    
    @IBAction func selectCarTypeClicked(_ sender: UIButton) {
        viewPickerContainer.isHidden = false
    }
    
    @IBAction func selectedCarTypeClicked(_ sender: UIButton) {
        viewPickerContainer.isHidden = true
        
        let index: Int = pickerView.selectedRow(inComponent: 0)
        //        Logger.d("index: \(index)")
        CarManager.shared().appendAddingCarModel(index: index)
        updateAdd2Ui()
    }
}
