//
//  OTPLoginViewController.swift
//  myvolkswagen
//
//  Created by Macintosh on 2021/1/27.
//  Copyright © 2021 volkswagen. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import SwiftyJSON
import CommonCrypto
import FirebaseInstallations
import FirebaseCore

protocol OTPLoginDataDelegate: class {
    func setLoginDataWith(code: String, token: String)
}

class OTPLoginViewController: NaviViewController, UITextFieldDelegate, LoginDataDelegate, OTPPhoneVerifyInfoDismissDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    

    // login data
    var code: String? = nil
    var token: String? = nil
    @IBOutlet weak var phoneTextField: VWTextField!
    @IBOutlet weak var phonePrefixView: UIView!
    @IBOutlet weak var nextBtn: VWButton!
    @IBOutlet weak var darkBGView: UIView!
    @IBOutlet weak var countryCodeLb: BasicLabel!
    @IBOutlet weak var pickerToolbar: UIToolbar!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var gradientView: UIView!
    let countries: [String] = ["美國","加拿大","俄羅斯","埃及","南非","希臘","荷蘭", "比利時","西班牙","匈牙利", "義大利","羅馬尼亞", "瑞士", "奧地利", "英國", "丹麥", "瑞典", "挪威", "波蘭", "德國", "祕魯", "墨西哥", "阿根廷", "巴西", "智利", "哥倫比亞", "馬來西亞", "澳洲", "印尼", "菲律賓", "紐西蘭", "新加坡", "泰國", "葉門", "日本", "南韓", "越南", "中國大陸", "土耳其", "印度", "巴基斯坦", "斯里蘭卡", "伊朗", "摩洛哥", "阿爾及利亞", "奈及利亞", "肯亞", "坦薩尼亞", "葡萄牙", "盧森堡", "愛爾蘭", "冰島", "馬爾他", "芬蘭", "法國", "摩洛哥", "瓜地馬拉", "薩爾瓦多", "宏都拉斯", "尼加拉瓜", "哥斯大黎加", "巴拿馬", "厄瓜多爾", "烏拉圭", "巴布亞 新幾內亞", "所羅門群島", "斐濟", "帛琉", "香港", "澳門", "台灣", "馬爾地夫", "伊拉克", "科威特", "沙烏地阿拉伯", "以色列", "尼泊爾", "關島", "多明尼亞"]
    let countryCode: [String] = ["+1","+1","+7","+20","+27","+30","+31", "+32","+34","+36","+39","+40", "+41", "+43", "+44", "+45", "+46", "+47", "+48", "+49", "+51", "+52", "+54", "+55", "+56", "+57", "+60", "+61", "+62", "+63", "+64", "+65", "+66", "+66", "+81", "+82", "+84", "+86", "+90", "+91", "+92", "+94", "+98", "+212", "+213", "+234", "+254", "+255", "+351", "+352", "+353", "+354", "+356", "+358", "+358", "+377", "+502", "+503", "+504", "+505", "+506", "+507", "+593", "+598", "+675", "+677", "+679", "+680", "+852", "+853", "+886", "+960", "+964", "+965", "+966", "+972", "+977", "+1671", "+1809"]
    @IBOutlet var wrongPhoneNumLb: BasicLabel!
    
    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()
        
        phoneTextField.inputAccessoryView = toolbar
        phoneTextField.delegate = self
        nextBtn.isEnabled = false
        
        wrongPhoneNumLb.removeFromSuperview()
        
        phonePrefixView.layer.cornerRadius = CGFloat(6).getFitSize()
        phonePrefixView.layer.borderColor = UIColor.MyTheme.greyColor400.cgColor
        phonePrefixView.layer.borderWidth = 1
        countryCodeLb.text = "+886"
        self.pickerToolbar.alpha = 0
        self.pickerView.alpha = 0
        self.pickerToolbar.isHidden = true
        self.pickerView.isHidden = true
        
        naviBar.setTitle("".localized)
        naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
        
        let gradientVieww = GradientBackgroundView()
        gradientVieww.frame = view.bounds
        gradientView.addSubview(gradientVieww)
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let code = self.code, let token = self.token {
            Logger.d("code: \(code), token: \(token)")
            
            Installations.installations().installationID { (installationID, error) in
                if let error = error {
                    Logger.d("Error fetching remote instance ID: \(error)")
                    return
                }

                guard let installationID = installationID else {
                    print("设备的安装 ID 为空")
                    return
                }

                print("设备的安装 ID：\(installationID)")
                Logger.d("Remote instance ID token: \(installationID)")
                UserManager.shared().signIn(token: token, code: code, deviceId: installationID, listener: self)
            }
            
//            InstanceID.instanceID().instanceID { (result, error) in
//                if let error = error {
//                    Logger.d("Error fetching remote instance ID: \(error)")
//                } else if let result = result {
//                    Logger.d("Remote instance ID token: \(result.token)")
//                    Logger.d("Remote instance ID instanceID: \(result.instanceID)")
//                    UserManager.shared().signIn(token: token, code: code, deviceId: result.instanceID, listener: self)
//                    //                    self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
//                }
//            }
        }
    }

    // MARK: LoginDataDelegate
    func setLoginDataWith(code: String, token: String) {
        self.code = code
        self.token = token
    }
    
    @IBAction private func nextClicked(_ sender: Any) {
        if let mobile = phoneTextField.text {
            if mobile != "" {
                sendPhoneNum(phoneNum: mobile)
                return
            }
        }
        
        let alertController = UIAlertController(title: "提醒", message: "warning_format_mobile".localized, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "confirm".localized, style: .default, handler: nil)
        
        alertController.addAction(action1)
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func infoClicked(_ sender: Any) {
        let sb = UIStoryboard(name: "Login", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "OTPPhoneVerifyInfoViewController") as! OTPPhoneVerifyInfoViewController
        vc.delegate = self
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true) {
            self.darkBGView.isHidden = false
            UIView.animate(withDuration: 0.3, animations: {
                self.darkBGView.alpha = 1
            }) { (success) in
            }
        }
    }
    
    @IBAction func countryCodeClicked(_ sender: Any) {
        showPickerView(show: true)
    }
    
    @IBAction func useVWIDClicked(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let vc: LoginViewController = sb.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            vc.loginDataDelegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func sendPhoneNum(phoneNum: String) {
        showHud()
//        var phoneNumToSend = phoneNum
//        if countryCodeLb.text! == "+886" {
//            if phoneNumToSend.hasPrefix("09") {
//                phoneNumToSend = String(phoneNumToSend.dropFirst())
//            }
//        }
        
        let parameters: Parameters = [
            "mobile_prefix": countryCodeLb.text!,
            "mobile": phoneNum,
            "hash_mobile": "\(countryCodeLb.text!)\(phoneNum)VolkswagenApp".sha256().uppercased()
        ]
            Alamofire.request(Config.urlMemberServer + "APP_Phone_Send_Code", method: .post, parameters: parameters)
                .validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                    
                    self.dismissHud()
                    if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                        let arr: [String] = authorization.components(separatedBy: " ")
                        
                        // "Bearer xxxxx"
                        if arr.count == 2 {
                            UserManager.shared().saveUserToken(with: arr[1])
                        }
                    }
                    
                    response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                        let responseJson = JSON.init(rawValue: response.result.value as Any)!
                        
                        let result: Result = Result(with: responseJson)
                        Logger.d("result: \(result)")
                        if result.isSuccess() {
                            self.performSegue(withIdentifier: "phoneVerifySeg", sender: self)
                        } else {
                            Logger.d("errorCode: \(String(describing: result.errorCode))")
                            Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                            if let msg = result.errorMsg {
                                if let ms = self.convertToDictionary(text: msg) {
                                    if let de = ms["detail"] as? String {
                                        self.showErrorMsg(show: true, txt: de)
                                        //self.showGeneralWarning(title: ms["msg"] as? String, message: de)
                                    } else {
                                        self.showErrorMsg(show: true, txt: ms["msg"] as! String)
                                        //self.showApiFailureAlert(message: ms["msg"] as! String)
                                    }
                                } else {
                                    self.showApiFailureAlert(message: msg)
                                }
                            }
                        }
                    })
                    
                    response.result.ifFailure({
                        Logger.d("error: \(response.error as Any)")
                        self.showApiFailureAlert(message: "msg_failed_to_login".localized)
                    })
                    
            }
    }
    
    func didDismiss() {
        hideBGView()
    }
    
    func hideBGView() {
        UIView.animate(withDuration: 0.3, animations: {
            self.darkBGView.alpha = 0
        }) { (success) in
            self.darkBGView.isHidden = true
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    // MARK: - Textfield
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        let newText = (currentText as NSString).replacingCharacters(in: range, with: string)
        if newText.isEmpty {
            nextBtn.isEnabled = false
        } else if newText.count == 10 {
            nextBtn.isEnabled = true
        }
        
        return true
    }
    
    // MARK: - Error msg
    func showErrorMsg(show: Bool, txt: String) {
        wrongPhoneNumLb.removeFromSuperview()
        
        if show {
            self.phoneTextField.setErrorBorder(error: true)
            self.view.addSubview(wrongPhoneNumLb)
            wrongPhoneNumLb.text = txt
            wrongPhoneNumLb.snp.makeConstraints { make in
                make.leading.equalTo(self.phoneTextField.snp.leading)
                make.top.equalTo(self.phoneTextField.snp.bottom).offset(4)
                make.bottom.equalTo(self.nextBtn.snp.top).offset(-16)
            }
        }
    }
    
    // MARK: - Textfiled
    func textFieldDidBeginEditing(_ textField: UITextField) {
        showErrorMsg(show: false, txt: "")
        phoneTextField.textColor = UIColor(rgb: 0x4a4a4a)
    }
    
    // MARK: - PickerView
    func showPickerView(show: Bool) {
        if show {
            pickerToolbar.isHidden = false
            pickerView.isHidden = false
            UIView.animate(withDuration: 0.3) {
                self.pickerToolbar.alpha = 1
                self.pickerView.alpha = 1
            }
        } else {
            UIView.animate(withDuration: 0.3) {
                self.pickerToolbar.alpha = 0
                self.pickerView.alpha = 0
            } completion: { (succ) in
                self.pickerToolbar.isHidden = true
                self.pickerView.isHidden = true
            }

        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let cc = countries[row]
        let ccCode = countryCode[row]
        return "\(cc) \(ccCode)"
    }
    
    @IBAction func pickerViewCancel(_ sender: Any) {
        guard let codeIndex = countryCode.index(of: countryCodeLb.text!) else { return }
        pickerView.selectRow(codeIndex, inComponent: 0, animated: false)
        showPickerView(show: false)
    }
    
    @IBAction func pickerViewDone(_ sender: Any) {
        let selectedRow = pickerView.selectedRow(inComponent: 0)
        let ccCode = countryCode[selectedRow]
        countryCodeLb.text = ccCode
        showPickerView(show: false)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "phoneVerifySeg" {
            let dvc = segue.destination as! OTPPhoneVerifyViewController
            dvc.mobile = phoneTextField.text
            dvc.mobilePrefix = countryCodeLb.text!
        }
    }
    

}

extension Data{
    public func sha256() -> String{
        return hexStringFromData(input: digest(input: self as NSData))
    }
    
    private func digest(input : NSData) -> NSData {
        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
        var hash = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }
    
    private  func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
        }
        
        return hexString
    }
}

public extension String {
    func sha256() -> String{
        if let stringData = self.data(using: String.Encoding.utf8) {
            return stringData.sha256()
        }
        return ""
    }
}
