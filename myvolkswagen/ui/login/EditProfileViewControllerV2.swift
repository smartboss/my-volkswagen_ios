//
//  EditProfileViewController.swift
//  myvolkswagen
//
//  Created by Apple on 1/31/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Kingfisher

enum EditProfileType2: Int {
    case login = 0, profile
}

class EditProfileViewControllerV2: NaviViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate, KeyboardHandler {
    
    enum EditImageType: Int {
        case background = 0, photo
    }
    
    enum Row: Int {
        case header = 0, nickname, email, mobile, address, birthday, gender , dealerarea, dealerplant
        
        static var allCases: [Row] {
            var values: [Row] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    var profile: Profile?
    var editingProfile: Profile?
    
    var type: EditProfileType2 = .login
    @IBOutlet weak var tableView: UITableView!
    
    var inputNickname: PInput!
    var inputEmail: PInput!
    var inputMobile: PInput!
    var inputAddress: PInput!
    
    var isEmailValid: Bool = true
    var isMobileValid: Bool = true
    
    @IBOutlet weak var viewBirthdayPickerContainer: UIView!
    @IBOutlet weak var buttonBirthdayPickerDone: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var viewGenderPickerContainer: UIView!
    @IBOutlet weak var buttonGenderPickerDone: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var viewAreaPickerContainer: UIView!
    @IBOutlet weak var areaPickerView: UIPickerView!
    @IBOutlet weak var buttonAreaPickerDone: UIButton!
    @IBOutlet weak var viewPlantPickerContainer: UIView!
    @IBOutlet weak var plantPickerView: UIPickerView!
    @IBOutlet weak var buttonPlantPickerDone: UIButton!
    @IBOutlet weak var gradientView: UIView!
    let genders: [Gender] = [.male, .female, .notSpecified]
    var maintenancePlant: [MaintainancePlant]?
    
    var contentScrollView: UIScrollView {
        tableView
      }
    
    override func viewDidLoad() {
        self.hasTextField = true
        super.viewDidLoad()
        
        switch type {
        case .login:
            naviBar.setTitle("會員資料".localized)
            naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
        case .profile:
            naviBar.setTitle("會員資料".localized)
            naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
        }
        
        datePicker.backgroundColor = UiUtility.colorBackgroundWhite
        buttonBirthdayPickerDone.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        buttonBirthdayPickerDone.setTitle("done".localized, for: .normal)
        buttonGenderPickerDone.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        buttonGenderPickerDone.setTitle("done".localized, for: .normal)
        buttonAreaPickerDone.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        buttonAreaPickerDone.setTitle("done".localized, for: .normal)
        buttonPlantPickerDone.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        buttonPlantPickerDone.setTitle("done".localized, for: .normal)
        
        profile = UserManager.shared().getProfile(withId: UserManager.shared().currentUser.accountId!, listener: self)
        if profile == nil {
            profile = Profile.init()
        }
        editingProfile = Profile.init(with: profile!)
        
        maintenancePlant = CarManager.shared().getMaintenancePlant(listener: self)
        
        let gradientVieww = GradientBackgroundView()
        gradientVieww.frame = view.bounds
        gradientView.addSubview(gradientVieww)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObservingKeyboard()
      }

      override func viewWillDisappear(_ animated: Bool) {
        removeObservingKeyboard()
      }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if offsetY == -1 {
            offsetY = self.tableView.frame.origin.y
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        self.view.endEditing(true)
        
        back()
    }
    
    override func doneButtonClicked(_ button: UIButton) {
        doneClicked(button)
    }
    
    
    
    // MARK: picker view delegate
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.pickerView {
            return genders.count
        } else if pickerView == areaPickerView {
            guard let mp = maintenancePlant else { return 0 }
            if let profile = self.profile {
                if profile.dealerAreaId == "" {
                    // user沒選過,增加請選擇選項
                    return mp.count + 1
                }
                
                return mp.count
            }
            return 0
        } else {
            if let eprofile = self.editingProfile {
                if eprofile.dealerAreaId == "" {
                    return 0
                }
                
                guard let mp = maintenancePlant else { return 0 }
                if let profile = self.profile {
                    if profile.dealerAreaId == "" {
                        // user沒選過,增加請選擇選項
                        for (index, mpArea) in mp.enumerated() {
                            if String(mpArea.areaId!) == eprofile.dealerAreaId {
                                return mp[index].plants!.count + 1
                            }
                        }
                    }
                    
                    for (index, mpArea) in mp.enumerated() {
                        if String(mpArea.areaId!) == eprofile.dealerAreaId {
                            return mp[index].plants!.count
                        }
                    }
                }
            }
            return 0
        }
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.pickerView {
            return genders[row].displayText()
        } else if pickerView == areaPickerView {
            if let profile = self.profile {
                if profile.dealerAreaId == "" {
                    // user沒選過,增加請選擇選項
                    if row == 0 {
                        return "請選擇"
                    } else {
                        return maintenancePlant![row - 1].areaName
                    }
                }
            }
            return maintenancePlant![row].areaName
        } else {
            if let eprofile = self.editingProfile {
                if eprofile.dealerAreaId == "" {
                    return ""
                }
                
                guard let mp = maintenancePlant else { return "" }
                if let profile = self.profile {
                    if profile.dealerAreaId == "" {
                        // user沒選過,增加請選擇選項
                        if row == 0 {
                            return "請選擇"
                        } else {
                            for (index, mpArea) in mp.enumerated() {
                                if String(mpArea.areaId!) == eprofile.dealerAreaId {
                                    return mp[index].plants![row - 1].plantName
                                }
                            }
                        }
                    }
                    
                    for (index, mpArea) in mp.enumerated() {
                        if String(mpArea.areaId!) == eprofile.dealerAreaId {
                            return mp[index].plants![row].plantName
                        }
                    }
                }
            }
            return ""
        }
    }
    
    
    // MARK: profile protocol
    
    override func didStartGetProfile() {
        showHud()
    }
    
    override func didFinishGetProfile(success: Bool, profile: Profile?) {
        dismissHud()
        
        if success {
            if let p = profile {
                self.profile = p
                editingProfile = Profile.init(with: p)
                tableView.reloadData()
                self.areaPickerView.reloadAllComponents()
                self.plantPickerView.reloadAllComponents()
            }
        }
        
        if type == .login {
        }
    }
    
    override func didStartUpdateProfile() {
        showHud()
    }
    
    override func didFinishUpdateProfile(success: Bool, profile: Profile?, msg: String?) {
        dismissHud()
        
        if success {
            switch type {
            case .login:
                let sb = UIStoryboard(name: "Login", bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "BindCarViewController") as! BindCarViewController
                vc.name = self.inputNickname.textField.text
                vc.type = .login
                self.navigationController?.pushViewController(vc, animated: true)
                UserManager.shared().currentUser.showProfile = false
//                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                appDelegate.updateRootVC(showAlert: false)
            case .profile:
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            showGeneralWarning(title: nil, message: msg)
        }
    }
    
    
    
    // MARK: text field delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        editingTextField = textField
        
        if textField == inputNickname.textField {
            if textField.text?.count == 0 {
                inputNickname.buttonClear.isHidden = true
            } else {
                inputNickname.buttonClear.isHidden = false
            }
        } else if textField == inputEmail.textField {
            if textField.text?.count == 0 {
                inputEmail.buttonClear.isHidden = true
            } else {
                inputEmail.buttonClear.isHidden = false
            }
        } else if textField == inputMobile.textField {
            if textField.text?.count == 0 {
                inputMobile.buttonClear.isHidden = true
            } else {
                inputMobile.buttonClear.isHidden = false
            }
        } else if textField == inputAddress.textField {
            if textField.text?.count == 0 {
                inputAddress.buttonClear.isHidden = true
            } else {
                inputAddress.buttonClear.isHidden = false
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == inputNickname.textField {
            Logger.d("update nickname value: \(String(describing: textField.text))")
            editingProfile?.nickname = textField.text
        } else if textField == inputEmail.textField {
            Logger.d("update email value: \(String(describing: textField.text))")
            editingProfile?.email = textField.text
        } else if textField == inputMobile.textField {
            Logger.d("update cellphone value: \(String(describing: textField.text))")
            editingProfile?.cellphone = textField.text
        } else if textField == inputAddress.textField {
            Logger.d("update name value: \(String(describing: textField.text))")
            editingProfile?.address = textField.text
        }
        Logger.d("editing profile: \(String(describing: editingProfile))")
        
        if textField == inputNickname.textField {
            inputNickname.buttonClear.isHidden = true
        } else if textField == inputEmail.textField {
            inputEmail.buttonClear.isHidden = true
        } else if textField == inputMobile.textField {
            inputMobile.buttonClear.isHidden = true
        } else if textField == inputAddress.textField {
            inputAddress.buttonClear.isHidden = true
        }
    }
    
    // MARK: car protocal
    override func didStartGetCarMaintenancePlant() {
        //showHud()
    }
    
    override func didFinishGetCarMaintenancePlant(success: Bool, maintenancePlants: [MaintainancePlant]?) {
        // dismissHud()
        self.maintenancePlant = maintenancePlants
        self.areaPickerView.reloadAllComponents()
        self.plantPickerView.reloadAllComponents()
    }
    
    // MARK: keyboard
    
    @objc override func keyboardWillShow(notification: NSNotification) {
        keyboardShowing = true
    }
    
    @objc override func keyboardWillHide(notification: NSNotification) {
        keyboardShowing = false
    }
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Row.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Row.init(rawValue: indexPath.section)! {
        case .header:
            return UiUtility.adaptiveSize(size: 82)
        case .birthday, .gender, .email, .mobile, .address, .dealerarea, .dealerplant:
            return UiUtility.adaptiveSize(size: 42)
        case .nickname:
            return UiUtility.adaptiveSize(size: 60)
            //        case .done:
            //            switch type {
            //            case .login:
            //                return UiUtility.adaptiveSize(size: 143)
            //            case .profile:
            //                return 0
            //            }
        
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TitleHeader")!
        let hea = cell.viewWithTag(1) as? BasicLabel
        
        switch Row.init(rawValue: section)! {
        case .nickname:
            hea?.text = "\("nickname".localized)*"
        case .birthday:
            hea?.text = "birthday".localized
        case .gender:
            hea?.text = "gender".localized
        case .email:
            hea?.text = "\("email".localized)*"
        case .mobile:
            hea?.text = "mobile".localized
        case .address:
            hea?.text = "address".localized
        case .dealerarea:
            hea?.text = "my_dealer".localized
        default:
            return cell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch Row.init(rawValue: section)! {
        case .header:
            return 0.1
        case .dealerplant:
            return 8
        case .nickname, .birthday, .gender, .email, .mobile, .address, .dealerarea:
            return UiUtility.adaptiveSize(size: 50)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }

    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        switch Row.init(rawValue: indexPath.section)! {
        case .header:
            cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath)
            cell.selectionStyle = .none
        case .nickname:
            cell = tableView.dequeueReusableCell(withIdentifier: "InputCell", for: indexPath)
            cell.selectionStyle = .none
            
            inputNickname = cell.viewWithTag(1) as? PInput
            inputNickname.setupWith(hint: "hint_nickname".localized)
            inputNickname.textField.inputAccessoryView = toolbar
            inputNickname.textField.delegate = self
            inputNickname.textField.keyboardType = .default
            inputNickname.textField.addTarget(self, action:#selector(textFieldDidChange(_:)), for: .editingChanged)
            inputNickname.textField.isUserInteractionEnabled = true
            inputNickname.dropdownPic.isHidden = true
            inputNickname.warnWithBlack("將會在首頁顯示，稍後也可以至會員資料頁面修改")
            
            if let profile = self.editingProfile {
                inputNickname.textField.text = profile.nickname
                
                if let nickname = profile.nickname {
                    if nickname.count > 20 {
                        inputNickname.warnWith("warning_man_20".localized)
                    }
                }
            }
        case .birthday:
            cell = tableView.dequeueReusableCell(withIdentifier: "PickerCell", for: indexPath)
            cell.selectionStyle = .none
            
            let pInput: PInput = cell.viewWithTag(1) as! PInput
            pInput.setupWith(hint: "hint_birthday".localized)
            pInput.dropdownPic.isHidden = false
            pInput.dropdownPic.image = UIImage(named: "date")?.withRenderingMode(.alwaysTemplate)
            pInput.dropdownPic.tintColor = UIColor.MyTheme.greyColor400
            
            let button: UIButton = cell.viewWithTag(2) as! UIButton
            button.removeTarget(nil, action: nil, for: .allEvents)
            button.addTarget(self, action:#selector(selectBirthdayClicked(_:)), for: .touchUpInside)
            
            if let profile = self.editingProfile {
                pInput.textField.text = profile.getBirthdayText()
            }
        case .gender:
            cell = tableView.dequeueReusableCell(withIdentifier: "PickerCell", for: indexPath)
            cell.selectionStyle = .none
            
            let pInput: PInput = cell.viewWithTag(1) as! PInput
            pInput.setupWith(hint: "hint_gender".localized)
            pInput.dropdownPic.isHidden = false
            pInput.dropdownPic.image = UIImage(named: "down")
            
            let button: UIButton = cell.viewWithTag(2) as! UIButton
            button.addTarget(self, action:#selector(selectGenderClicked(_:)), for: .touchUpInside)
            
            if let profile = self.editingProfile {
                pInput.textField.text = profile.getGenderText()
            }
        case .email:
            cell = tableView.dequeueReusableCell(withIdentifier: "InputCell", for: indexPath)
            cell.selectionStyle = .none
            
            inputEmail = cell.viewWithTag(1) as? PInput
            inputEmail.setupWith(hint: "hint_email".localized)
            inputEmail.textField.inputAccessoryView = toolbar
            inputEmail.textField.keyboardType = .emailAddress
            inputEmail.textField.delegate = self
            inputEmail.textField.addTarget(self, action:#selector(textFieldDidChange(_:)), for: .editingChanged)
            inputEmail.textField.isUserInteractionEnabled = false
            inputEmail.dropdownPic.isHidden = true
            
            if let profile = self.editingProfile {
                inputEmail.textField.text = profile.email
                if profile.OTPFlag == 1 {
                    inputEmail.textField.isUserInteractionEnabled = true
                }
            }
            
//            if isEmailValid {
//                inputEmail.unwarn()
//            } else {
//                inputEmail.warnWith("warning_format_email".localized)
//            }
        case .mobile:
            cell = tableView.dequeueReusableCell(withIdentifier: "InputCell", for: indexPath)
            cell.selectionStyle = .none
            
            inputMobile = cell.viewWithTag(1) as? PInput
            inputMobile.setupWith(hint: "請輸入您的手機號碼")
            inputMobile.textField.inputAccessoryView = toolbar
            inputMobile.textField.keyboardType = .phonePad
            inputMobile.textField.delegate = self
            inputMobile.textField.addTarget(self, action:#selector(textFieldDidChange(_:)), for: .editingChanged)
            inputMobile.textField.isUserInteractionEnabled = false
            inputMobile.dropdownPic.isHidden = true
            
            if let profile = self.editingProfile {
                inputMobile.textField.text = profile.cellphone
                if profile.OTPFlag == 2 {
                    inputMobile.textField.isUserInteractionEnabled = true
                }
            }
            
//            if isMobileValid {
//                inputMobile.unwarn()
//            } else {
//                inputMobile.warnWith("warning_format_mobile".localized)
//            }
        case .address:
            cell = tableView.dequeueReusableCell(withIdentifier: "InputCell", for: indexPath)
            cell.selectionStyle = .none
            
            inputAddress = cell.viewWithTag(1) as? PInput
            inputAddress.setupWith(hint: "hint_address".localized)
            inputAddress.textField.inputAccessoryView = toolbar
            inputAddress.textField.delegate = self
            inputAddress.textField.keyboardType = .default
            inputAddress.textField.addTarget(self, action:#selector(textFieldDidChange(_:)), for: .editingChanged)
            inputAddress.textField.isUserInteractionEnabled = true
            inputAddress.dropdownPic.isHidden = true
            
            if let profile = self.editingProfile {
                inputAddress.textField.text = profile.address
            }
        case .dealerarea:
            cell = tableView.dequeueReusableCell(withIdentifier: "PickerCell", for: indexPath)
            cell.selectionStyle = .none
            
            let pInput: PInput = cell.viewWithTag(1) as! PInput
            pInput.setupWith(hint: "請選擇地區")
            pInput.dropdownPic.isHidden = false
            pInput.dropdownPic.image = UIImage(named: "down")
            
            let button: UIButton = cell.viewWithTag(2) as! UIButton
            button.removeTarget(nil, action: nil, for: .allEvents)
            button.addTarget(self, action:#selector(selectAreaClicked(_:)), for: .touchUpInside)
            
            if let profile = self.editingProfile {
                pInput.textField.text = profile.dealerAreaName
            }
        case .dealerplant:
            cell = tableView.dequeueReusableCell(withIdentifier: "PickerCell", for: indexPath)
            cell.selectionStyle = .none
            
            let pInput: PInput = cell.viewWithTag(1) as! PInput
            pInput.setupWith(hint: "請選擇服務中心")
            pInput.dropdownPic.isHidden = false
            pInput.dropdownPic.image = UIImage(named: "down")
            
            let button: UIButton = cell.viewWithTag(2) as! UIButton
            button.removeTarget(nil, action: nil, for: .allEvents)
            button.addTarget(self, action:#selector(selectPlantClicked(_:)), for: .touchUpInside)
            
            if let profile = self.editingProfile {
                pInput.textField.text = profile.dealerPlantName
            }
        }
        
        return cell
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        
    }
    
    private func checkField() -> Bool {
        if let nickname = editingProfile?.nickname {
            isEmailValid = false
            if let email = editingProfile?.email {
                if !Utility.isEmpty(email) && Utility.isValidEmail(testStr: email) {
                    isEmailValid = true
                }
            }
            
            if editingProfile?.OTPFlag == 1 {
                isMobileValid = false
                if let mobile = editingProfile?.cellphone {
                    if mobile != "" {
                        isMobileValid = true
                    }
                }
            }
            
            if nickname.count > 20 {
                return false
            }
            
            if let brief = editingProfile?.brief {
                if brief.count > 30 {
                    return false
                }
            }
            
            if !isEmailValid {
                showGeneralWarning(title: "請輸入正確的電子信箱", message: "")
            }
            
            if editingProfile?.OTPFlag == 1 {
                if !isMobileValid {
                    showGeneralWarning(title: "請輸入正確的手機號碼", message: "")
                }
            }
            
            if editingProfile?.OTPFlag == 1 {
                return isEmailValid && isMobileValid
            } else {
                return isEmailValid
            }
        } else {
            showGeneralWarning(title: "請輸入您的顯示名稱", message: "")
            return false
        }
    }
    
    
    @IBAction private func doneClicked(_ sender: Any) {
        self.view.endEditing(true)
        
        // check fields
        if checkField() {
            // update profile
            UserManager.shared().updateProfile(profile: editingProfile!, listener: self)
        } else {
            tableView.reloadData()
        }
    }
    
    @objc private func textFieldDidChange(_ sender: UITextField) {
        if sender == inputNickname.textField {
            editingProfile?.nickname = inputNickname.textField.text
            if let text = sender.text {
                if text.count >= 21 {
                    inputNickname.warnWith("warning_man_20".localized)
                } else/* if text.count <= 20*/ {
                    inputNickname.unwarn()
                }
            } else {
                //inputNickname.unwarn()
            }
            
            if sender.text?.count == 0 {
                inputNickname.buttonClear.isHidden = true
            } else {
                inputNickname.buttonClear.isHidden = false
            }
        } else if sender == inputEmail.textField {
            editingProfile?.email = inputEmail.textField.text
            if sender.text?.count == 0 {
                inputEmail.buttonClear.isHidden = true
            } else {
                inputEmail.buttonClear.isHidden = false
            }
        } else if sender == inputMobile.textField {
            editingProfile?.cellphone = inputMobile.textField.text
            if sender.text?.count == 0 {
                inputMobile.buttonClear.isHidden = true
            } else {
                inputMobile.buttonClear.isHidden = false
            }
        } else if sender == inputAddress.textField {
            editingProfile?.address = inputAddress.textField.text
            if sender.text?.count == 0 {
                inputAddress.buttonClear.isHidden = true
            } else {
                inputAddress.buttonClear.isHidden = false
            }
        }
    }
    
    @objc private func selectBirthdayClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if let profile = editingProfile, let date = profile.getBirthDate() {
            datePicker.date = date
        }
        
        viewGenderPickerContainer.isHidden = true
        viewBirthdayPickerContainer.isHidden = false
        datePicker.isHidden = false
        viewPlantPickerContainer.isHidden = true
        viewAreaPickerContainer.isHidden = true
    }
    
    @IBAction func selectedBirthdayClicked(_ sender: UIButton) {
        viewBirthdayPickerContainer.isHidden = true
        
        Logger.d("datePicker.date: \(datePicker.date.description(with: .current))")
        editingProfile?.setBirthday(to: datePicker.date)
        tableView.reloadData()
    }
    
    @objc private func selectGenderClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        
        var targetGender: Gender = .notSpecified
        if let profile = editingProfile, let sex = profile.sex {
            if let gender: Gender = Gender(rawValue: sex) {
                targetGender = gender
            }
        }
        
        var index: Int = 0
        for g in genders {
            if g == targetGender {
                break
            }
            index += 1
        }
        
        pickerView.selectRow(index, inComponent: 0, animated: true)
        
        viewBirthdayPickerContainer.isHidden = true
        viewGenderPickerContainer.isHidden = false
        viewPlantPickerContainer.isHidden = true
        viewAreaPickerContainer.isHidden = true
    }
    
    @IBAction func selectedGenderClicked(_ sender: UIButton) {
        viewGenderPickerContainer.isHidden = true
        
        editingProfile?.setGender(to: genders[pickerView.selectedRow(inComponent: 0)])
        tableView.reloadData()
    }
    
    @objc private func selectAreaClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        
        viewBirthdayPickerContainer.isHidden = true
        viewGenderPickerContainer.isHidden = true
        viewAreaPickerContainer.isHidden = false
        viewPlantPickerContainer.isHidden = true
    }
    
    @IBAction func selectedAreaClicked(_ sender: UIButton) {
        viewAreaPickerContainer.isHidden = true
        
        let index = areaPickerView.selectedRow(inComponent: 0)
        
        if let profile = self.profile {
            if profile.dealerAreaId == "" {
                // user沒選過,index0為請選擇選項
                if index == 0 {
                    // 請選擇
                    editingProfile?.dealerAreaId = nil
                    editingProfile?.dealerAreaName = nil
                } else {
                    editingProfile?.dealerAreaId = String(maintenancePlant![index - 1].areaId ?? 0)
                    editingProfile?.dealerAreaName = maintenancePlant![index - 1].areaName
                }
                editingProfile?.dealerPlantId = nil
                editingProfile?.dealerPlantName = nil
                tableView.reloadData()
                plantPickerView.selectRow(0, inComponent: 0, animated: false)
                plantPickerView.reloadAllComponents()
                return
            }
        }
        
        editingProfile?.dealerAreaId = String(maintenancePlant![index].areaId ?? 0)
        editingProfile?.dealerAreaName = maintenancePlant![index].areaName
        editingProfile?.dealerPlantId = nil
        editingProfile?.dealerPlantName = nil
        tableView.reloadData()
        plantPickerView.selectRow(0, inComponent: 0, animated: false)
        plantPickerView.reloadAllComponents()
    }
    
    @objc private func selectPlantClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if let edip = editingProfile {
            if edip.dealerAreaId == nil || edip.dealerAreaId == "" {
                showApiFailureAlert(message: "請先選擇服務中心地區")
                return
            }
        }
        viewBirthdayPickerContainer.isHidden = true
        viewGenderPickerContainer.isHidden = true
        viewAreaPickerContainer.isHidden = true
        viewPlantPickerContainer.isHidden = false
    }
    
    @IBAction func selectedPlantClicked(_ sender: UIButton) {
        viewPlantPickerContainer.isHidden = true
        
        let aIndex = areaPickerView.selectedRow(inComponent: 0)
        let pIndex = plantPickerView.selectedRow(inComponent: 0)
        
        if let profile = self.profile {
            if profile.dealerAreaId == "" {
                // user沒選過,index0為請選擇選項
                if pIndex == 0 {
                    // 請選擇
                    editingProfile?.dealerPlantId = nil
                    editingProfile?.dealerPlantName = nil
                } else {
                    editingProfile?.dealerPlantId = String(maintenancePlant![aIndex - 1].plants![pIndex - 1].plantId ?? 0)
                    editingProfile?.dealerPlantName = maintenancePlant![aIndex - 1].plants![pIndex - 1].plantName
                }
                tableView.reloadData()
                return
            }
        }
        editingProfile?.dealerPlantId = String(maintenancePlant![aIndex].plants![pIndex].plantId ?? 0)
        editingProfile?.dealerPlantName = maintenancePlant![aIndex].plants![pIndex].plantName
        tableView.reloadData()
    }
}
