//
//  OTPPhoneVerifyInfoViewController.swift
//  myvolkswagen
//
//  Created by Macintosh on 2021/1/31.
//  Copyright © 2021 volkswagen. All rights reserved.
//

import UIKit

protocol OTPPhoneVerifyInfoDismissDelegate: class {
    func didDismiss()
}

class OTPPhoneVerifyInfoViewController: BaseViewController {

    var viewTranslation = CGPoint(x: 0, y: 0)
    weak var delegate: OTPPhoneVerifyInfoDismissDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
    }
    
    @objc func handleDismiss(sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .changed:
            viewTranslation = sender.translation(in: view)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.view.transform = CGAffineTransform(translationX: 0, y: self.viewTranslation.y)
            })
        case .ended:
            if viewTranslation.y < 200 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.view.transform = .identity
                })
            } else {
                delegate?.didDismiss()
                dismiss(animated: true, completion: nil)
            }
        default:
            break
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
