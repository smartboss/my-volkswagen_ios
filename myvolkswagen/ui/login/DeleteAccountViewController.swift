//
//  DeleteAccountViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2022/11/22.
//  Copyright © 2022 volkswagen. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DeleteAccountViewController: NaviViewController {

    @IBOutlet weak var cancelBtn: BasicSizingButton!
    @IBOutlet weak var okBtn: BasicSizingButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        naviBar.setTitle("delete_account".localized)
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        
        cancelBtn.layer.cornerRadius = cancelBtn.frame.size.height/2
        cancelBtn.layer.borderColor = UIColor(red: 0.00, green: 0.15, blue: 0.29, alpha: 1.00).cgColor
        cancelBtn.layer.borderWidth = 1
        okBtn.layer.cornerRadius = okBtn.frame.size.height/2
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    @IBAction func didPressCancelBtn(_ sender: Any) {
        back()
    }
    
    @IBAction func didPressOkBtn(_ sender: Any) {
        
        let currentUser: User = UserManager.shared().currentUser
    
        if !Utility.isEmpty(currentUser.lineUid) {
            UserManager.shared().bindLine(set: false, lineUid: currentUser.lineUid!, listener: self)
            return
        }
        deleteAccount()
    }
    
    override func didStartBindLine() {
        showBGHud()
    }
    
    override func didFinishBindLine(success: Bool, isBind: Bool) {
        deleteAccount()
    }

    func deleteAccount() {
        Alamofire.request(Config.urlMemberServer + "APP_Del_Account?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        UserManager.shared().signOut(listener: self, showAlert: true)
                    } else {
                        
                    }
                })
                
                response.result.ifFailure({
                    
                })
            }
    }
}
