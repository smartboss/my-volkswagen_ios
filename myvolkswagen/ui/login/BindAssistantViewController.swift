//
//  BindAssistantViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/20.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
import AVFoundation

class BindAssistantViewController: NaviViewController, UITextFieldDelegate, KeyboardHandler, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    var captureSession: AVCaptureSession!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    //var previewLayer: AVCaptureVideoPreviewLayer!
    
    @IBOutlet weak var scrollview: UIScrollView!
    var name: String?
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var skipBtn: VWButtonWhite!
    @IBOutlet weak var imeiTxtfield: VWTextField!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var progressImg: UIImageView!
    @IBOutlet weak var progressImg2: UIImageView!
    @IBOutlet weak var profileNaviBar: UIView!
    @IBOutlet weak var gestureLineView: UIView!
    var carNumber: String!
    var dongleData: DongleData?
    var statee: CarDoungleState?
    var type: EditProfileType2 = .profile
    
    var contentScrollView: UIScrollView {
        scrollview
    }
    
    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()
        
        imeiTxtfield.inputAccessoryView = toolbar
        
        naviBar.setTitle("新增愛車".localized)
        naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
        
        setupUI()
        print("--- BindAssistantViewController ---")
        print("account id : \(UserManager.shared().currentUser.accountId!)")
        print("carNumber : \(carNumber)")
        if type == .login {
            self.didFinishGetDongleBinding(state: self.statee, dongleData: self.dongleData)
        } else {
            UserManager.shared().loginCruisys(listener: self, accountId: UserManager.shared().currentUser.accountId!, plateNum: carNumber)
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        CarManager.shared().deleteAddingCar()
        back()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObservingKeyboard()
        
        if self.dongleData != nil {
            if statee == .VIN_MAPPED { // 未綁定行車助理
                self.scrollview.isHidden = false
                self.emptyView.isHidden = true
            } else if statee == .USER_MAPPED {
                let sb = UIStoryboard(name: "Login", bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "BindLineIDViewController") as! BindLineIDViewController
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.emptyView.isHidden = false
                self.scrollview.isHidden = true
                if self.skipBtn != nil {
                    self.skipBtn.removeFromSuperview()
                }
            }
        } else {
            self.emptyView.isHidden = false
            self.scrollview.isHidden = true
            if self.skipBtn != nil {
                self.skipBtn.removeFromSuperview()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeObservingKeyboard()
    }
    
    func setupUI() {
        if type == .login {
            profileNaviBar.isHidden = true
            let gradientVieww = GradientBackgroundView()
            gradientVieww.frame = view.bounds
            gradientView.addSubview(gradientVieww)
        } else {
            gestureLineView.layer.cornerRadius = gestureLineView.frame.height / 2
            gradientView.backgroundColor = UIColor.white
            skipBtn.removeFromSuperview()
            progressImg.removeFromSuperview()
            progressImg2.removeFromSuperview()
        }
    }
    
    override func didStartLoginCruisys() {
        showHud()
    }
    
    override func didFinishLoginCruisys(success: Bool) {
        dismissHud()
        print("--- didFinishLoginCruisys ---")
        if success {
            CarManager.shared().getDongleBindingInfo(listener: self, plateNo: self.carNumber)
        } else {
            showApiFailureAlert(message: "發生錯誤，請稍後再試")
        }
    }
    
    // MARK: - Profile Navi Bar
    @IBAction func profileNaviBackBtnPressed(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func profileNaviCloseBtnPressed(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    // MARK: DongleBinding
    override func didStartGetDongleBinding() {
        showHud()
    }
    
    override func didFinishGetDongleBinding(state: CarDoungleState?, dongleData: DongleData?) {
        dismissHud()
        if state == nil && dongleData == nil {
            self.emptyView.isHidden = false
            self.scrollview.isHidden = true
            if self.skipBtn != nil {
                self.skipBtn.removeFromSuperview()
            }
            return
        }
        
        if state == nil {
            self.emptyView.isHidden = false
            self.scrollview.isHidden = true
            if self.skipBtn != nil {
                self.skipBtn.removeFromSuperview()
            }
            return
        }
        
        self.dongleData = dongleData
        self.statee = state
        if state == .VIN_MAPPED { // 未綁定行車助理
            self.scrollview.isHidden = false
            self.emptyView.isHidden = true
        } else if statee == .USER_MAPPED {
            let sb = UIStoryboard(name: "Login", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "BindLineIDViewController") as! BindLineIDViewController
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            self.emptyView.isHidden = false
            self.scrollview.isHidden = true
            if self.skipBtn != nil {
                self.skipBtn.removeFromSuperview()
            }
        }
    }
    
    override func didStartPostDongleBinding() {
        showHud()
    }
    
    override func didFinishPostDongleBinding(state: CarDoungleState?, dongleData: DongleData?, msg: String?) {
        dismissHud()
        
        if msg != nil {
            showApiFailureAlert(message: msg!)
        }
        
        if state == .USER_MAPPED || state == .ACTIVATED {
            if type == .login {
                let sb = UIStoryboard(name: "Login", bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "BindLineIDViewController") as! BindLineIDViewController
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.dismiss(animated: true)
            }
        }
    }
    
    // MARK: Scan
    @IBAction func scanBtnPressed(_ sender: Any) {
        // 初始化 AVCaptureSession
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if captureSession.canAddInput(videoInput) {
            captureSession.addInput(videoInput)
        } else {
            return
        }
        
        let videoOutput = AVCaptureVideoDataOutput()
        videoOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "videoQueue"))
        if captureSession.canAddOutput(videoOutput) {
            captureSession.addOutput(videoOutput)
        } else {
            return
        }
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer.frame = view.layer.bounds
        videoPreviewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(videoPreviewLayer)
    
        let closeButton = UIButton(frame: CGRect(x: 20, y: 20, width: 40, height: 40))
        closeButton.setTitle("X", for: .normal)
        closeButton.setTitleColor(.white, for: .normal)
        closeButton.backgroundColor = .black.withAlphaComponent(0.6)
        closeButton.layer.cornerRadius = 20
        closeButton.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        self.view.addSubview(closeButton)
            
        DispatchQueue.global(qos: .userInitiated).async {
            self.captureSession.startRunning()
        }
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    @objc func closeAction() {
        captureSession?.stopRunning()
        videoPreviewLayer?.removeFromSuperlayer()
        self.view.subviews.last?.removeFromSuperview()
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }
        
        let ciImage = CIImage(cvImageBuffer: pixelBuffer)
        let context = CIContext()
        let detector = CIDetector(ofType: CIDetectorTypeQRCode, context: context, options: [CIDetectorAccuracy: CIDetectorAccuracyHigh])
        
        if let features = detector?.features(in: ciImage) {
            for feature in features as! [CIQRCodeFeature] {
                DispatchQueue.main.async {
                    self.imeiTxtfield.text = "\(feature.messageString ?? "")"
                    print("imei scan: \(feature.messageString ?? "")")
                    self.captureSession.stopRunning()
                    self.videoPreviewLayer.removeFromSuperlayer()
                }
            }
        }
    }
    
    // MARK: Btns
    @IBAction func nextBtnPressed(_ sender: Any) {
        self.view.endEditing(true)
        
        if dongleData == nil { // 無行車助理
            if type == .login {
                let sb = UIStoryboard(name: "Login", bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "BindLineIDViewController") as! BindLineIDViewController
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.dismiss(animated: true)
            }
            return
        }
        
        if self.imeiTxtfield.text != nil &&
            self.imeiTxtfield.text != "" {
            self.dongleData?.imei = self.imeiTxtfield.text!
            if self.dongleData != nil &&
                self.dongleData!.imei != nil {
                CarManager.shared().postDongleBinding(listener: self, dongleData: self.dongleData!, plateNo: self.carNumber)
            }
        }
    }
    
    @IBAction func skipBtnPressed(_ sender: Any) {
        let sb = UIStoryboard(name: "Login", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "BindLineIDViewController") as! BindLineIDViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
