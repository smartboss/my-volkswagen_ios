//
//  BindCarViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/13.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
import SnapKit

class BindCarViewController: NaviViewController, UITextFieldDelegate, KeyboardHandler {
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var progressImg: UIImageView!
    @IBOutlet weak var noticeLb: UILabel!
    @IBOutlet weak var profileNaviBar: UIView!
    @IBOutlet weak var gestureLineView: UIView!
    @IBOutlet weak var nameTxtField: VWTextField!
    @IBOutlet weak var numPrefixTxtField: VWTextField!
    @IBOutlet weak var numSurfixTxtField: VWTextField!
    @IBOutlet weak var vinTitleLb: BasicLabel!
    @IBOutlet weak var vinTextField: VWTextField!
    @IBOutlet var plateWarning: BasicLabel!
    @IBOutlet var vinWarning: BasicLabel!
    var name: String?
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var skipBtn: VWButtonWhite!
    var type: EditProfileType2 = .profile
    var profile: Profile?
    @IBOutlet weak var nextBtn: VWButton!
    
    var contentScrollView: UIScrollView {
        scrollview
    }
    
    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()
        
        nameTxtField.inputAccessoryView = toolbar
        numPrefixTxtField.inputAccessoryView = toolbar
        numSurfixTxtField.inputAccessoryView = toolbar
        vinTextField.inputAccessoryView = toolbar
        
        nameTxtField.delegate = self
        numPrefixTxtField.delegate = self
        numSurfixTxtField.delegate = self
        vinTextField.delegate = self
        
        naviBar.setTitle("新增愛車".localized)
        naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
        
        CarManager.shared().startAddingCar()
        
        setupUI()
        self.checkTextFields()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObservingKeyboard()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeObservingKeyboard()
    }
    
    func setupUI() {
        if CarManager.shared().cars.count > 0 {
            let mycar = CarManager.shared().cars[0]
            if mycar.name != nil && mycar.name != "" {
                self.nameTxtField.text = mycar.name!
                self.nameTxtField.isEnabled = false
            }
        } else if let myId = UserManager.shared().currentUser.accountId {
            profile = UserManager.shared().getProfile(withId: myId, listener: self)
            self.nameTxtField.text = "\(profile?.nickname ?? "")"
        }
        plateWarning.removeFromSuperview()
        vinWarning.removeFromSuperview()
        if type == .login {
            profileNaviBar.isHidden = true
            let gradientVieww = GradientBackgroundView()
            gradientVieww.frame = view.bounds
            gradientView.addSubview(gradientVieww)
        } else {
            gestureLineView.layer.cornerRadius = gestureLineView.frame.height / 2
            gradientView.backgroundColor = UIColor.white
            skipBtn.removeFromSuperview()
            progressImg.removeFromSuperview()
            noticeLb.removeFromSuperview()
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        CarManager.shared().deleteAddingCar()
        back()
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        self.view.endEditing(true)
        
        CarManager.shared().appendAddingCarInfo(name: self.nameTxtField.text, platePrefix: self.numPrefixTxtField.text, plateSuffix: self.numSurfixTxtField.text, vinLast5: self.vinTextField.text)
        
        let addingCar: AddingCar = CarManager.shared().getAddingCar()!
        if Utility.isEmpty(addingCar.name) || Utility.isEmpty(addingCar.platePrefix) || Utility.isEmpty(addingCar.plateSuffix) || Utility.isEmpty(addingCar.vinLast5) {
            //            let alertController = UIAlertController(title: nil, message: "car_data_lack".localized, preferredStyle: .alert)
            //            let action1 = UIAlertAction(title: "fine".localized, style: .default) { (action:UIAlertAction) in
            //                Logger.d("do nothing")
            //            }
            //
            //            alertController.addAction(action1)
            //            present(alertController, animated: true, completion: nil)
            let customAlert = VWCustomAlert()
            customAlert.alertTitle = ""
            customAlert.alertMessage = "car_data_lack".localized
            customAlert.alertTag = 1
            customAlert.isCancelButtonHidden = true
            customAlert.modalPresentationStyle = .overFullScreen
            self.present(customAlert, animated: true, completion: nil)
        } else {
            CarManager.shared().checkExist(listener: self)
        }
    }
    
    @IBAction func skipBtnPressed(_ sender: Any) {
        CarManager.shared().deleteAddingCar()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.updateRootVC(showAlert: false)
    }
    
    @IBAction func vinInfoBtnPressed(_ sender: Any) {
        let sb = UIStoryboard(name: "Login", bundle: nil)
        let bottomSheetVC = sb.instantiateViewController(withIdentifier: "VinInfoViewController") as! VinInfoViewController
        
        let h = UiUtility.getScreenWidth() * (369/393)
        let sheetController = SheetViewController(
            controller: bottomSheetVC,
            sizes: [.fixed(CGFloat(48 + 16 + h))])
        
        sheetController.cornerRadius = 20
        sheetController.minimumSpaceAbovePullBar = 0
        sheetController.treatPullBarAsClear = false
        sheetController.dismissOnOverlayTap = true
        sheetController.dismissOnPull = true
        
        sheetController.shouldDismiss = { _ in
            return true
        }
        sheetController.didDismiss = { _ in
        }
        self.present(sheetController, animated: true, completion: nil)
    }
    
    // MARK: - Textfiled
    func textFieldDidBeginEditing(_ textField: UITextField) {
        showErrorMsg(show: false)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == numPrefixTxtField || textField == numSurfixTxtField {
            // check chinese
            let isChinese = string.range(of: "\\p{Han}", options: .regularExpression) != nil
            if isChinese {
                return false
            }
            
            // allow english & digit
            let allowedCharacters = CharacterSet.alphanumerics
            let isAllowed = string.rangeOfCharacter(from: allowedCharacters) != nil || string.isEmpty
            
            // uppercase
            if isAllowed && !string.isEmpty {
                textField.text = (textField.text! as NSString).replacingCharacters(in: range, with: string.uppercased())
                return false
            }
            DispatchQueue.main.async {
                self.checkTextFields()
            }
            return isAllowed
        }
        DispatchQueue.main.async {
            self.checkTextFields()
        }
        return true
    }
    
    func checkTextFields() {
        let areAllTextFieldsFilled = nameTxtField.hasText && numPrefixTxtField.hasText &&
        numSurfixTxtField.hasText && vinTextField.hasText
        
        nextBtn.isEnabled = areAllTextFieldsFilled
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        checkTextFields()
    }
    
    // MARK: - Profile Navi Bar
    @IBAction func profileNaviBackBtnPressed(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func profileNaviCloseBtnPressed(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    // MARK: car protocol
    
    override func didStartCheckCarExist() {
        showHud()
    }
    
    override func didFinishCheckCarExist(result: Result?) {
        dismissHud()
        
        if let result = result {
            if result.isSuccess() {
                CarManager.shared().getModel(listener: self)
            } else {
                var msg = ""
                if let code: Int = result.errorCode {
                    switch code {
                    case 3004:
                        msg = "err_msg_3004".localized
                    case 3010:
                        msg = "err_msg_3010".localized
                    case 3001:
                        msg = "err_msg_3001".localized
                    case 3003:
                        msg = "err_msg_3003".localized
                    case 3005:
                        msg = "err_msg_3005".localized
                    case 3009:
                        msg = "err_msg_3009".localized
                    default:
                        msg = "發生錯誤，請稍後再試"
                    }
                }
                //                let alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
                //                let action1 = UIAlertAction(title: "錯誤", style: .default) { (action:UIAlertAction) in
                //                    Logger.d("do nothing")
                //                }
                //
                //                alertController.addAction(action1)
                //                present(alertController, animated: true, completion: nil)
                self.plateWarning.text = msg
                showErrorMsg(show: true)
            }
        }
    }
    
    // MARK: - Error msg
    func showErrorMsg(show: Bool) {
        self.plateWarning.removeFromSuperview()
        self.numPrefixTxtField.setErrorBorder(error: false)
        self.numSurfixTxtField.setErrorBorder(error: false)
        
        if show {
            self.numPrefixTxtField.setErrorBorder(error: true)
            self.numSurfixTxtField.setErrorBorder(error: true)
            self.view.addSubview(self.plateWarning)
            self.plateWarning.snp.makeConstraints { make in
                make.leading.equalTo(self.numPrefixTxtField.snp.leading)
                make.top.equalTo(self.numPrefixTxtField.snp.bottom).offset(4)
                make.bottom.equalTo(self.vinTitleLb.snp.top).offset(-16)
            }
        }
    }
    
    override func didStartGetCarModel() {
        showHud()
    }
    
    override func didFinishGetCarModel(success: Bool, models: [Model]?) {
        dismissHud()
        
        if success {
            let sb = UIStoryboard(name: "Login", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "BindCarTypeViewController") as! BindCarTypeViewController
            vc.type = self.type
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            // TODO: error handling
        }
    }
}
