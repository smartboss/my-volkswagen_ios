//
//  OTPPhoneVerifyViewController.swift
//  myvolkswagen
//
//  Created by Macintosh on 2021/1/29.
//  Copyright © 2021 volkswagen. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import SwiftyJSON
import CommonCrypto
import FirebaseInstallations
import SnapKit

class OTPPhoneVerifyViewController: NaviViewController, UITextFieldDelegate {
    
    @IBOutlet weak var verifyCodeTextfield: VWTextField!
    @IBOutlet weak var loginBtn: VWButton!
    @IBOutlet weak var contentLb: BasicLabel!
    @IBOutlet var wrongCodeLb: BasicLabel!
    @IBOutlet weak var sendAgainBtn: UIButton!
    @IBOutlet weak var gradientView: UIView!
    var mobile: String!
    var mobilePrefix: String!
    var warningColor = UIColor(rgb: 0xea2b40)
    var timer: Timer?
    var totalTime = 60
    
    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()
        
        naviBar.setTitle("手機驗證".localized)
        naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
        
        verifyCodeTextfield.inputAccessoryView = toolbar
        verifyCodeTextfield.delegate = self
        
        loginBtn.isEnabled = false
        
        wrongCodeLb.removeFromSuperview()
        
        contentLb.text = "已發送六位數驗證碼至您的手機 \(mobilePrefix!) \(mobile!) 請儘速完成驗證。"
        sendAgainBtn.layer.cornerRadius = CGFloat(5).getFitSize()
        enableSendAgain(enable: false)
        
        let gradientVieww = GradientBackgroundView()
        gradientVieww.frame = view.bounds
        gradientView.addSubview(gradientVieww)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startOtpTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let timer = self.timer {
            timer.invalidate()
            self.timer = nil
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    // MARK: - Timer
    private func startOtpTimer() {
        enableSendAgain(enable: false)
        self.totalTime = 60
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        print(self.totalTime)
        UIView.performWithoutAnimation {
            self.sendAgainBtn.setTitle("重新發送驗證碼(\(self.timeFormatted(self.totalTime))s)", for: .normal)
            self.sendAgainBtn.layoutIfNeeded()
        }
        if totalTime != 0 {
            totalTime -= 1
        } else {
            if let timer = self.timer {
                timer.invalidate()
                self.timer = nil
                enableSendAgain(enable: true)
            }
        }
    }
    
    func enableSendAgain(enable: Bool) {
        if enable {
            sendAgainBtn.setTitle("重新發送驗證碼", for: .normal)
            sendAgainBtn.layer.borderColor = UIColor.MyTheme.secondaryColor300.cgColor
            sendAgainBtn.layer.borderWidth = 1
            sendAgainBtn.setTitleColor(UIColor.MyTheme.secondaryColor300, for: .normal)
            sendAgainBtn.isEnabled = true
        } else {
            sendAgainBtn.layer.borderColor = UIColor.MyTheme.greyColor400.cgColor
            sendAgainBtn.layer.borderWidth = 1
            sendAgainBtn.setTitleColor(UIColor.MyTheme.greyColor400, for: .disabled)
            sendAgainBtn.isEnabled = false
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds// % 60
        return String(format: "%d", seconds)
    }
    
    // MARK: - Btns
    @IBAction func loginBtnPressed(_ sender: Any) {
        Analytics.logEvent("login", parameters: nil)
        
        verifyCodeTextfield.resignFirstResponder()
        if let vco = verifyCodeTextfield.text {
            if !Utility.isEmpty(vco) {
                getId(code: vco)
            }
        }
    }
    
    @IBAction func sendAgainBtnPressed(_ sender: Any) {
        if timer != nil {
            return
        }
        
        verifyCodeTextfield.resignFirstResponder()
        startOtpTimer()
        sendPhoneNum(phoneNum: mobile)
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        verifyCodeTextfield.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBackBtnPressed(_ sender: Any) {
        verifyCodeTextfield.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Data
    func sendPhoneNum(phoneNum: String) {
//        var phoneNumToSend = phoneNum
//        if mobilePrefix == "+886" {
//            if phoneNumToSend.hasPrefix("09") {
//                phoneNumToSend = String(phoneNumToSend.dropFirst())
//            }
//        }
        
        let parameters: Parameters = [
            "mobile_prefix": mobilePrefix!,
            "mobile": phoneNum,
            "hash_mobile": "\(mobilePrefix!)\(phoneNum)VolkswagenApp".sha256().uppercased()
        ]
            Alamofire.request(Config.urlMemberServer + "APP_Phone_Send_Code", method: .post, parameters: parameters)
                .validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"]).responseJSON { response in

                    if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                        let arr: [String] = authorization.components(separatedBy: " ")
                        
                        // "Bearer xxxxx"
                        if arr.count == 2 {
                            UserManager.shared().saveUserToken(with: arr[1])
                        }
                    }
                    
                    response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                        let responseJson = JSON.init(rawValue: response.result.value as Any)!
                        
                        let result: Result = Result(with: responseJson)
                        Logger.d("result: \(result)")
                        if result.isSuccess() {
                            self.loginBtn.isEnabled = true
                        } else {
                            Logger.d("errorCode: \(String(describing: result.errorCode))")
                            Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                            if let msg = result.errorMsg {
                                if let ms = self.convertToDictionary(text: msg) {
                                    if let de = ms["detail"] as? String {
                                        self.showGeneralWarning(title: ms["msg"] as? String, message: de)
                                        
                                    } else {
                                        self.showApiFailureAlert(message: ms["msg"] as! String)
                                    }
                                } else {
                                    self.showApiFailureAlert(message: msg)
                                }
                            }
                        }
                    })
                    
                    response.result.ifFailure({
                        Logger.d("error: \(response.error as Any)")
                        self.showApiFailureAlert(message: "msg_failed_to_login".localized)
                    })
                    
            }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func getId(code: String) {
        
        Installations.installations().installationID { (installationID, error) in
            if let error = error {
                Logger.d("Error fetching remote instance ID: \(error)")
                return
            }

            guard let installationID = installationID else {
                print("设备的安装 ID 为空")
                return
            }

            print("设备的安装 ID：\(installationID)")
            Logger.d("Remote instance ID token: \(installationID)")
            self.sendVerifyCodeAndSignIn(code: code, udid: installationID)
        }
        
//        InstanceID.instanceID().instanceID { (result, error) in
//            if let error = error {
//                Logger.d("Error fetching remote instance ID: \(error)")
//            } else if let result = result {
//                Logger.d("Remote instance ID token: \(result.token)")
//                Logger.d("Remote instance ID instanceID: \(result.instanceID)")
//                self.sendVerifyCodeAndSignIn(code: code, udid: result.instanceID)
//            }
//        }
    }
    
    func sendVerifyCodeAndSignIn(code: String, udid: String) {
        UserManager.shared().phoneSignIn(code: code, deviceId: udid, mobile: mobile, mobilePrefix: mobilePrefix, listener: self)
    }
    
    override func didFinishLogin(success: Bool) {
        if !success {
            showErrorMsg(show: false)
            verifyCodeTextfield.textColor = warningColor
        }
        
        goInitialPage(success: success)
    }
    
    override func didFinishPhoneLogin(success: Bool, code: Int?, msg: String?) {
        dismissHud()
        
        if !success {
            if let cc = code {
                if cc == 2008 {
                    self.wrongCodeLb.text = "驗證碼錯誤"
                    self.showErrorMsg(show: true)
                    
                    verifyCodeTextfield.textColor = warningColor
                }
            }
            if let mm = msg {
                if let ms = self.convertToDictionary(text: mm) {
                    if let de = ms["detail"] as? String {
                        self.wrongCodeLb.text = de
                        self.showErrorMsg(show: true)
                        //self.showGeneralWarning(title: ms["msg"] as? String, message: de)
                    } else {
                        self.wrongCodeLb.text = ms["msg"] as? String
                        self.showErrorMsg(show: true)
                        //self.showApiFailureAlert(message: ms["msg"] as! String)
                    }
                } else {
                    self.wrongCodeLb.text = mm
                    self.showErrorMsg(show: true)
                    //self.showApiFailureAlert(message: mm)
                }
            }
            return
        }
        
        goInitialPage(success: success)
    }
    
    // MARK: - Error msg
    func showErrorMsg(show: Bool) {
        wrongCodeLb.removeFromSuperview()
        
        if show {
            self.verifyCodeTextfield.setErrorBorder(error: true)
            self.view.addSubview(wrongCodeLb)
            wrongCodeLb.snp.makeConstraints { make in
                make.leading.equalTo(self.verifyCodeTextfield.snp.leading)
                make.top.equalTo(self.verifyCodeTextfield.snp.bottom).offset(4)
                make.bottom.equalTo(self.sendAgainBtn.snp.top).offset(-16)
            }
        }
    }
    
    // MARK: - Textfiled
    func textFieldDidBeginEditing(_ textField: UITextField) {
        showErrorMsg(show: false)
        verifyCodeTextfield.textColor = UIColor(rgb: 0x4a4a4a)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        let newText = (currentText as NSString).replacingCharacters(in: range, with: string)
        if newText.count >= 6 {
            loginBtn.isEnabled = true
        } else {
            loginBtn.isEnabled = false
        }
        
        return true
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
