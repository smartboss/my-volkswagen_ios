//
//  BindLineIDViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/27.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

class BindLineIDViewController: NaviViewController {

    @IBOutlet weak var skipBtn: VWButtonWhite!
    @IBOutlet weak var gradientView: UIView!
    var showSuccMsg = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        naviBar.setTitle("連結帳號")
        naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
        
        let gradientVieww = GradientBackgroundView()
        gradientVieww.frame = view.bounds
        gradientView.addSubview(gradientVieww)
        
        if showSuccMsg {
            showSuccessMsg()
        }
    }
    
    func showSuccessMsg() {
        let customAlert = VWCustomAlert()
        customAlert.alertTitle = "完成通知"
        customAlert.alertMessage = "您已完成連結 LINE ID"
        customAlert.alertTag = 1
        customAlert.isCancelButtonHidden = true
        customAlert.okButtonAction = {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.updateRootVC(showAlert: false)
        }
        self.present(customAlert, animated: true, completion: nil)
    }
    
    // MARK: Btns
    @IBAction func nextBtnPressed(_ sender: Any) {
        self.view.endEditing(true)
        
//        canStartDplk = true
//        DeeplinkNavigator.shared.canStartDeeplink()
        
        if let ust = UserManager.shared().getUserToken() {
            let url = URL(string: Config.urlLineLiff + "\(ust)&source=new")!
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func skipBtnPressed(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.updateRootVC(showAlert: false)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
