//
//  OnboardingViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/11/12.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

class OnboardingViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIView!
    @IBOutlet weak var skipView: UIView!
    @IBOutlet weak var beginBtn: UIButton!
    lazy var pageControll: VWPageControl = {
        let pc                   = VWPageControl(numberOfPages: 4, currentPage: 0, isCircular: true)
        pc.currentIndicatorColor = .black
        return pc
    }()
    var cellId = "onboardingCell"
    let pagecount = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.edgesForExtendedLayout = .all
        
        let long: CGFloat = 16
        let short: CGFloat = (CGFloat((pagecount - 1)) * CGFloat(8).getFitSize())
        let spacing: CGFloat = CGFloat((pagecount - 1)) * 5
        let mywidth = long + short + spacing
        // 長 + 短 + spacing
        pageControll = VWPageControl(numberOfPages: pagecount, currentPage: 0, isCircular: true)
        pageControll.backgroundColor = UIColor.clear

        // 添加到视图
        pageControl.addSubview(pageControll)
        pageControll.snp.makeConstraints { make in
            make.width.equalTo(mywidth)
            make.height.equalTo(CGFloat(4).getFitSize())
            make.centerX.centerY.equalToSuperview()
        }
        beginBtn.layer.cornerRadius = CGFloat(5).getFitSize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.collectionView.alpha = 1
    }
    
    //MARK: ScrollView Delegate
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        guard let visible = collectionView.visibleCells.first else { return }
        guard let index = collectionView.indexPath(for: visible)?.row else { return }
        pageControll.currentpage = index

    }
    
    @IBAction func skipBtnPressed(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.collectionView.alpha = 0
        } completion: { succ in
//            UserManager.shared().saveUserInstruction()
//            let token = UserManager.shared().getUserToken()
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            appDelegate.updateRootVC(showAlert: false)
            let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "OTPLoginViewController") as! OTPLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func beginBtnPressed(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.collectionView.alpha = 0
        } completion: { succ in
//            UserManager.shared().saveUserInstruction()
//            let token = UserManager.shared().getUserToken()
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            appDelegate.updateRootVC(showAlert: false)
            let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "OTPLoginViewController") as! OTPLoginViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if Int(scrollView.contentOffset.x) / Int(scrollView.frame.width) == 4 {
            beginBtn.isHidden = false
        } else {
            beginBtn.isHidden = true
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        let imgc = cell.viewWithTag(1) as! UIImageView
        //imgc.image = UIImage(named: "ins_\(indexPath.item + 1)")
        imgc.image = UIImage(named: "Onboarding Screens\(indexPath.item + 1)")
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UiUtility.getScreenWidth(), height: UiUtility.getScreenHeight())
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 4 {
            UIView.animate(withDuration: 0.3) {
                self.collectionView.alpha = 0
            } completion: { succ in
                let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "OTPLoginViewController") as! OTPLoginViewController
                self.navigationController?.pushViewController(vc, animated: true)
//                let token = UserManager.shared().getUserToken()
//                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                appDelegate.updateRootVC(showAlert: false)
            }
        } else {
            let rect = self.collectionView.layoutAttributesForItem(at: IndexPath(row: (indexPath.item + 1), section: 0))?.frame
             self.collectionView.scrollRectToVisible(rect!, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
