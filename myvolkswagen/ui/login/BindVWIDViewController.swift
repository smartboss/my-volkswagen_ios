//
//  BindVWIDViewController.swift
//  myvolkswagen
//
//  Created by Macintosh on 2021/1/30.
//  Copyright © 2021 volkswagen. All rights reserved.
//

import UIKit
import Firebase

class BindVWIDViewController: NaviViewController, LoginDataDelegate {

    @IBOutlet weak var vwEmailView: UIView!
    @IBOutlet weak var bindVWIDView: UIView!
    @IBOutlet weak var emailLb: BasicLabel!
    var pressType: String?
    // login data
    var code: String? = nil
    var token: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        naviBar.setTitle("Volkswagen ID")
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        refresh()
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    func refresh() {
        if UserManager.shared().getOTPFlag() == 1 {
            vwEmailView.isHidden = true
            bindVWIDView.isHidden = false
        } else {
            vwEmailView.isHidden = false
            bindVWIDView.isHidden = true
            emailLb.text = UserManager.shared().currentUser.vwEmail
        }
    }
    
    @IBAction func showVWIDBtnPressed(_ sender: Any) {
        pressType = "show"
        showLoginVC()
    }
    
    @IBAction func bindVWIDBtnPressed(_ sender: Any) {
        pressType = "bind"
        showLoginVC()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let code = self.code, let token = self.token {
            Logger.d("code: \(code), token: \(token)")
            
            Installations.installations().installationID { (installationID, error) in
                if let error = error {
                    Logger.d("Error fetching remote instance ID: \(error)")
                    return
                }
                
                guard let installationID = installationID else {
                    print("设备的安装 ID 为空")
                    return
                }
                
                print("设备的安装 ID：\(installationID)")
                Logger.d("Remote instance ID token: \(installationID)")
                if self.pressType == "show" {
                    UserManager.shared().signIn(token: token, code: code, deviceId: installationID, listener: self)
                } else if self.pressType == "bind" {
                    UserManager.shared().bindVWId(token: token, code: code, deviceId: installationID, listener: self)
                }
            }
        }
            
//            InstanceID.instanceID().instanceID { (result, error) in
//                if let error = error {
//                    Logger.d("Error fetching remote instance ID: \(error)")
//                } else if let result = result {
//                    Logger.d("Remote instance ID token: \(result.token)")
//                    Logger.d("Remote instance ID instanceID: \(result.instanceID)")
//                    
//                    if self.pressType == "show" {
//                        UserManager.shared().signIn(token: token, code: code, deviceId: result.instanceID, listener: self)
//                    } else if self.pressType == "bind" {
//                        UserManager.shared().bindVWId(token: token, code: code, deviceId: result.instanceID, listener: self)
//                    }
//                    
//                    self.pressType = nil
//                    //                    self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
//                }
//            }
//        }
    }
    
    override func didFinishBindVWId(success: Bool, isBind: Bool) {
        dismissHud()
        UserManager.shared().setOTPFlag(flag: 0)
        refresh()
        self.code = nil
        self.token = nil
    }
    
    override func didFinishLogin(success: Bool) {
        dismissHud()
        self.code = nil
        self.token = nil
    }
    
    // MARK: LoginDataDelegate
    func setLoginDataWith(code: String, token: String) {
        self.code = code
        self.token = token
    }
    
    func showLoginVC() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let vc: LoginViewController = sb.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            vc.loginDataDelegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
