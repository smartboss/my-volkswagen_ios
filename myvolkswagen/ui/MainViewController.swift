//
//  MainViewController.swift
//  myvolkswagen
//
//  Created by Apple on 1/24/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Firebase

protocol TabControllerDelegate: class {
    func menuClicked()
    func notificationClicked()
    func addPostClicked()
    func goSetting()
    func goEditProfile()
    func goNewsDetail(news: News)
    func goBrowseLikers(news: News)
    func goBrowseFollow(of profile: Profile, isFollower: Bool)
    func goBrowseReward()
    func goBrowseMemberLevel()
    func goBrowsePoint()
    func goFinishReport()
    func goEditPost(post: Post?, contentDelegate: ContentDelegate?, photoSourceType: PhotoSourceType)
    func goEditPhotos(post: Post?, contentDelegate: ContentDelegate?)
    func addCar()
    func goRoadInfo()
    func goCarPage(of tab: Int)
    func showOrderDetail(view: UIView)
    func goPatView(cars: [Car])
    func goViewController(view: UIViewController)
    
    func goCreateClub(clubCarModels: [CarModel2])
    func goClub(_ club: Club)
    func sortClub()
    func goYourClub()
    func goExploreClub()
    func goBrowseClubPostLikers(post: ClubPost)
    func goEditClubPost(clubId: Int, post: Post?, clubUpdateDelegate: ClubUpdateDelegate?)
    func goClubPostDetail(post: ClubPost, clubUpdateDelegate: ClubUpdateDelegate?)
    func getTabbarIndex() -> Int
}

class MainViewController: BaseViewController, TabControllerDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var feedViewController: UINavigationController!
    var carViewController: UINavigationController!
    var actionViewController: UINavigationController!
    var chatViewController: UINavigationController!
    var personViewController: UINavigationController!
    
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet var tabButtons: [UIButton]!
    @IBOutlet weak var tab0Btn: TabBarBtn!
    @IBOutlet weak var tab1Btn: TabBarBtn!
    @IBOutlet weak var tab2Btn: TabBarBtn!
    @IBOutlet weak var tab3Btn: TabBarBtn!
    @IBOutlet weak var tab4Btn: TabBarBtn!
    @IBOutlet weak var tabBarBg: UIView!
    var viewControllers: [UIViewController]!
    var selectedIndex: Int = 0
    var firstSelection: Bool = true
    
    
    // add car
    @IBOutlet weak var viewShieldAdd: UIView!
    @IBOutlet weak var containerAdd: UIView!
    var addMinCenterY: CGFloat = 0
    var addMaxCenterY: CGFloat = 0
    @IBOutlet weak var viewAdd0: UIView!
    @IBOutlet weak var add0titleOwnerName: UILabel!
    @IBOutlet weak var add0valueOwnerName: UnderlineInput!
    @IBOutlet weak var add0labelOwnerName: UILabel!
    @IBOutlet weak var add0titlePlate: UILabel!
    @IBOutlet weak var add0valuePlate0: UnderlineInput!
    @IBOutlet weak var add0valuePlate1: UnderlineInput!
    @IBOutlet weak var add0titleVin: UILabel!
    @IBOutlet weak var add0buttonVin: UIButton!
    @IBOutlet weak var add0valueVin: UnderlineInput!
    @IBOutlet weak var add0warning: UILabel!
    @IBOutlet weak var viewAdd1: UIView!
    @IBOutlet weak var add1tableView: UITableView!
    @IBOutlet weak var viewAdd2: UIView!
    @IBOutlet weak var add2titleModel: UILabel!
    @IBOutlet weak var add2valueModel: UILabel!
    @IBOutlet weak var add2titleType: UILabel!
    @IBOutlet weak var add2containerType: UIView!
    @IBOutlet weak var add2ButtonType: UIButton!
    @IBOutlet weak var viewAdd3: UIView!
    @IBOutlet weak var add3title: UILabel!
    
    @IBOutlet weak var viewPickerContainer: UIView!
    @IBOutlet weak var buttonPickerDone: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    ////

    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()
        
        upperView.backgroundColor = UiUtility.colorBackgroundWhite
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        feedViewController = storyboard.instantiateViewController(withIdentifier: "FeedNavi") as? UINavigationController
        (feedViewController.viewControllers[0] as! NaviViewController).tabControllerDelegate = self
        carViewController = storyboard.instantiateViewController(withIdentifier: "CarNavi") as? UINavigationController
        (carViewController.viewControllers[0] as! NaviViewController).tabControllerDelegate = self
        actionViewController = storyboard.instantiateViewController(withIdentifier: "ActionNavi") as? UINavigationController
        (actionViewController.viewControllers[0] as! NaviViewController).tabControllerDelegate = self
        chatViewController = storyboard.instantiateViewController(withIdentifier: "ChatNavi") as? UINavigationController
        (chatViewController.viewControllers[0] as! NaviViewController).tabControllerDelegate = self
        personViewController = storyboard.instantiateViewController(withIdentifier: "PersonNavi") as? UINavigationController
        (personViewController.viewControllers[0] as! NaviViewController).tabControllerDelegate = self
        viewControllers = [feedViewController, carViewController, actionViewController, chatViewController, personViewController]
        
        tabButtons[selectedIndex].isSelected = true
//        tabPressed(tabButtons[selectedIndex])
        goTab(of: selectedIndex, subTab: nil, deeplink: false)
        
        
        
        // add car
        let font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        setAddVisibility(false, animated: false, isInit: true)
        var panAddGesture = UIPanGestureRecognizer()
        panAddGesture = UIPanGestureRecognizer(target: self, action: #selector(draggedAddView(_:)))
        //        containerCard.isUserInteractionEnabled = true
        containerAdd.addGestureRecognizer(panAddGesture)
        add0titleOwnerName.textColor = UIColor.black
        add0titleOwnerName.font = font
        add0titleOwnerName.text = "car_owner_name".localized
        add0valueOwnerName.setHint("hint_car_owner_name".localized)
        add0valueOwnerName.textField.inputAccessoryView = toolbar
        add0valueOwnerName.textField.delegate = self
        add0labelOwnerName.textColor = UIColor.black
        add0labelOwnerName.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))!
        add0labelOwnerName.text = "David Lee".localized
        add0titlePlate.textColor = UIColor.black
        add0titlePlate.font = font
        add0titlePlate.text = "car_plate_number".localized
        add0valuePlate0.setHint("abc")
        add0valuePlate0.textField.inputAccessoryView = toolbar
        add0valuePlate0.textField.autocapitalizationType = .allCharacters
        add0valuePlate0.textField.delegate = self
        add0valuePlate1.setHint("1234")
        add0valuePlate1.textField.inputAccessoryView = toolbar
        add0valuePlate1.textField.autocapitalizationType = .allCharacters
        add0valuePlate1.textField.delegate = self
        add0titleVin.textColor = UIColor.black
        add0titleVin.font = font
        add0titleVin.text = "car_vin_last_5_number".localized
        add0buttonVin.setTitleColor(UiUtility.colorPinkyRed, for: .normal)
        add0buttonVin.titleLabel?.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))!
        add0buttonVin.setTitle("where_is_vin".localized, for: .normal)
        add0valueVin.setHint("12345")
        add0valueVin.textField.inputAccessoryView = toolbar
        add0valueVin.textField.autocapitalizationType = .allCharacters
        add0valueVin.textField.delegate = self
        add0warning.textColor = UiUtility.colorPinkyRed
        add0warning.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 12))!
        add0warning.isHidden = true
        
        add2titleModel.textColor = UIColor.black
        add2titleModel.font = font
        add2titleModel.text = "car_model_title".localized
        add2valueModel.textColor = UiUtility.colorDarkGrey
        add2valueModel.font = UIFont(name: "VWHead-Bold", size: UiUtility.adaptiveSize(size: 22))
        add2valueModel.text = "The Tiguan Allspace"
        add2titleType.textColor = UIColor.black
        add2titleType.font = font
        add2titleType.text = "car_type_title".localized
        add2containerType.layer.cornerRadius = UiUtility.adaptiveSize(size: 3)
        add2containerType.backgroundColor = UiUtility.colorBrownGrey.withAlphaComponent(0.3)
        add2ButtonType.setTitle("Allspace 330 TSI Comfortline", for: .normal)
        add2ButtonType.setTitleColor(UIColor(rgb: 0x4A4A4A), for: .normal)
        add2ButtonType.titleLabel?.font = UIFont(name: "VWHead", size: UiUtility.adaptiveSize(size: 15))
        buttonPickerDone.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        buttonPickerDone.setTitle("done".localized, for: .normal)
        add3title.textColor = UIColor.black
        add3title.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))!
        add3title.text = "car_add_success".localized
        
        UserManager.shared().getMemberLevelInfoWebview(listener: self)
        canStartDplk = true
        DeeplinkNavigator.shared.canStartDeeplink()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if offsetY == -1 {
            offsetY = self.view.frame.origin.y
        }
    }
    
    
    
    // MARK: club sort delegate
    
    override func setSort(type: ClubSort) {
        (feedViewController.viewControllers[0] as! NaviViewController).setSort(type: type)
    }
    

    
    @IBAction private func tabPressed(_ sender: UIButton) {
        self.goTab(of: sender.tag, subTab: nil, deeplink: false)
        
//        if !firstSelection && selectedIndex == sender.tag {
////            Logger.d("same tab")
//
//            if selectedIndex == 0 || selectedIndex == 4 {
//                let currentVC = viewControllers[selectedIndex]
//                if let navi = currentVC as? UINavigationController {
//                    if navi.viewControllers.count > 1 {
//                        navi.popToRootViewController(animated: true)
//                    }
//                }
//            }
//
//            return
//        }
//        let previousIndex = selectedIndex
//        selectedIndex = sender.tag
//
//        tabButtons[previousIndex].isSelected = false
//        let previousVC = viewControllers[previousIndex]
//        if previousIndex == 3 {
//            // clear chat messages
//            (chatViewController.viewControllers[0] as! ChatViewController).initUiState()
//        }
//        previousVC.willMove(toParent: nil)
//        previousVC.view.removeFromSuperview()
//        previousVC.removeFromParent()
//
//        sender.isSelected = true
//        let vc = viewControllers[selectedIndex]
//
//        switch selectedIndex {
//        case 1:
//            Analytics.logEvent("view_car", parameters: nil)
//        case 2:
//            Analytics.logEvent("view_tool", parameters: nil)
//        case 4:
//            Analytics.logEvent("view_my", parameters: nil)
//        default:
//            break
//        }
//
//
//        addChild(vc)
//        vc.view.frame = contentView.bounds
//        contentView.addSubview(vc.view)
//        vc.didMove(toParent: self)
//
//        firstSelection = false
    }
    
    private func goTab(of index: Int, subTab: Int?, deeplink: Bool) {
        if !firstSelection && selectedIndex == index && !deeplink {
            Logger.d("same tab")
            
            if selectedIndex == 0 || selectedIndex == 4 {
                let currentVC = viewControllers[selectedIndex]
                if let navi = currentVC as? UINavigationController {
                    if navi.viewControllers.count > 1 {
                        navi.popToRootViewController(animated: true)
                    }
                }
            }
            
            return
        }
        let previousIndex = selectedIndex
        selectedIndex = index
        
        tabButtons[previousIndex].isSelected = false
        let previousVC = viewControllers[previousIndex]
        if previousIndex == 3 {
            // clear chat messages
            (chatViewController.viewControllers[0] as! ChatViewController).initUiState()
        }
        previousVC.willMove(toParent: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParent()
        
        tabButtons[selectedIndex].isSelected = true
        let vc = viewControllers[selectedIndex]
        
        switch selectedIndex {
        case 1:
            Analytics.logEvent("view_car", parameters: nil)
            (carViewController.viewControllers[0] as! CarViewController).targetTab = subTab
            if let webv = (carViewController.viewControllers[0] as! CarViewController).webview {
                let request = URLRequest(url: URL(string: "\(Config.urlMyCar)?token=\(UserManager.shared().getUserToken()!)&AccountId=\(UserManager.shared().currentUser.accountId!)")!)
                webv.load(request)
            }
        case 2:
            Analytics.logEvent("view_tool", parameters: nil)
        case 4:
            Analytics.logEvent("view_my", parameters: nil)
//            let vc = (personViewController.viewControllers[0] as! PersonViewController)
//            if let _ = vc.webview {
//                if subTab == 0 {
//                    vc.loadMemberCoupon()
//                    vc.deeplink = .memberCoupon
//                } else if subTab == 1 {
//                    vc.loadMemberLevel()
//                    vc.deeplink = .memberLevel
//                } else {
//                    vc.loadMember()
//                    vc.deeplink = .member
//                }
//            } else {
//                if subTab == 0 {
//                    vc.deeplink = .memberCoupon
//                } else if subTab == 1 {
//                    vc.deeplink = .memberLevel
//                } else {
//                    vc.deeplink = .member
//                }
//            }
        default:
            break
        }
        
        
        addChild(vc)
        vc.view.frame = contentView.bounds
        contentView.addSubview(vc.view)
        vc.didMove(toParent: self)
        
        firstSelection = false
    }
    
    private func addCarDirectly() {
//        let sender: UIButton = tabButtons[1]
//
//        let previousIndex = selectedIndex
//        selectedIndex = sender.tag
//
//        tabButtons[previousIndex].isSelected = false
//        let previousVC = viewControllers[previousIndex]
//        previousVC.willMove(toParent: nil)
//        previousVC.view.removeFromSuperview()
//        previousVC.removeFromParent()
//
//        sender.isSelected = true
//        let vc = viewControllers[selectedIndex]
//
//        if let naviController: UINavigationController = vc as? UINavigationController {
//            let vcCar: CarViewController = naviController.viewControllers[0] as! CarViewController
//            vcCar.addNow = true
//        }
//
//        addChild(vc)
//        vc.view.frame = contentView.bounds
//        contentView.addSubview(vc.view)
//        vc.didMove(toParent: self)
        
        
        
        // prepare adding data
        CarManager.shared().startAddingCar()
        
        if addMinCenterY == 0 && addMaxCenterY == 0 {
            addMinCenterY = containerAdd.center.y
            //            Logger.d("card height: \(containerAdd.frame.height)")
            //            Logger.d("self.view height: \(view.frame.height)")
            
            addMaxCenterY = view.frame.height + (containerAdd.frame.height / 2)
            
            //            Logger.d("addMinCenterY: \(addMinCenterY)")
            //            Logger.d("addMaxCenterY: \(addMaxCenterY)")
        }
        setAddVisibility(true, animated: true, isInit: true)
    }
    
    private func showNoCarWarning() {
        let alertController = UIAlertController(title: "no_car_yet".localized, message: "msg_no_car".localized, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "next_time".localized, style: .cancel) { (action:UIAlertAction) in
            
        }
        
        let action2 = UIAlertAction(title: "add_now".localized, style: .default) { (action:UIAlertAction) in
            self.addCarDirectly()
        }
        
        alertController.addAction(action2)
        alertController.addAction(action1)
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    
    // MARK: car protocol
    
    override func didStartCheckCarExist() {
        showHud()
    }
    
    override func didFinishCheckCarExist(result: Result?) {
        dismissHud()
        
        if let result = result {
            if result.isSuccess() {
                CarManager.shared().getModel(listener: self)
            } else {
                if let code: Int = result.errorCode {
                    switch code {
                    case 3004:
                        add0warning.text = "err_msg_3004".localized
                        add0warning.isHidden = false
                    case 3010:
                        add0warning.text = "err_msg_3010".localized
                        add0warning.isHidden = false
                    case 3001:
                        add0warning.text = "err_msg_3001".localized
                        add0warning.isHidden = false
                    case 3003:
                        add0warning.text = "err_msg_3003".localized
                        add0warning.isHidden = false
                    case 3005:
                        add0warning.text = "err_msg_3005".localized
                        add0warning.isHidden = false
                    default:
                        add0warning.isHidden = true
                    }
                }
            }
        }
    }
    
    override func didStartGetCarModel() {
        showHud()
    }
    
    override func didFinishGetCarModel(success: Bool, models: [Model]?) {
        dismissHud()
        
        if success {
            viewAdd0.isHidden = true
            viewAdd1.isHidden = true
            viewAdd2.isHidden = false
            viewAdd3.isHidden = true
            
            updateAdd2Ui()
        }
    }
    
    override func didStartAddCar() {
        showHud()
    }
    
    override func didFinishAddCar(success: Bool) {
        dismissHud()
        
        if success {
            viewAdd0.isHidden = true
            viewAdd1.isHidden = true
            viewAdd2.isHidden = true
            viewAdd3.isHidden = false
        }
    }
    
    // MARK: User
    override func didStartGetMemberLevelInfoWebview() {
        showHud()
    }
    
    override func didFinishGetMemberLevelInfoWebview(success: Bool, level: Int?, levelName: String?) {
        dismissHud()
        
        if let lv = level {
            tabBarBg.backgroundColor = UIColor.white
            if lv == 1 { // 藍卡會員
                tabBarBg.backgroundColor = UIColor(rgb: 0x23406F)
            } else if lv == 2 { // 銀卡會員
                tabBarBg.backgroundColor = UIColor(rgb: 0xA2A5AC)
            } else if lv == 3 { // 金卡會員
                tabBarBg.backgroundColor = UIColor(rgb: 0xCCBB73)
            }
            
            let tab0ImgUnselected = UIImage(named: "tab_feed_normal")!.withRenderingMode(.alwaysTemplate)
            let tab0ImgSelected = UIImage(named: "tab_feed_highlighted")!.withRenderingMode(.alwaysTemplate)
            tab0Btn.setImage(tab0ImgUnselected, for: .normal)
            tab0Btn.setImage(tab0ImgSelected, for: .selected)
            tab0Btn.setColorByIndex(lv: lv)
            
            let tab1ImgUnselected = UIImage(named: "tab_car_normal")!.withRenderingMode(.alwaysTemplate)
            let tab1ImgSelected = UIImage(named: "tab_car_highlighted")!.withRenderingMode(.alwaysTemplate)
            tab1Btn.setImage(tab1ImgUnselected, for: .normal)
            tab1Btn.setImage(tab1ImgSelected, for: .selected)
            tab1Btn.setColorByIndex(lv: lv)
            
            let tab2ImgUnselected = UIImage(named: "tab_actions_normal")!.withRenderingMode(.alwaysTemplate)
            let tab2ImgSelected = UIImage(named: "tab_actions_highlighted")!.withRenderingMode(.alwaysTemplate)
            tab2Btn.setImage(tab2ImgUnselected, for: .normal)
            tab2Btn.setImage(tab2ImgSelected, for: .selected)
            tab2Btn.setColorByIndex(lv: lv)
            
            let tab3ImgUnselected = UIImage(named: "Charge")!.withRenderingMode(.alwaysTemplate)
            let tab3ImgSelected = UIImage(named: "Charge")!.withRenderingMode(.alwaysTemplate)
            tab3Btn.setImage(tab3ImgUnselected, for: .normal)
            tab3Btn.setImage(tab3ImgSelected, for: .selected)
            tab3Btn.setColorByIndex(lv: lv)
            
            let tab4ImgUnselected = UIImage(named: "tab_person_normal")!.withRenderingMode(.alwaysTemplate)
            let tab4ImgSelected = UIImage(named: "tab_person_highlighted")!.withRenderingMode(.alwaysTemplate)
            tab4Btn.setImage(tab4ImgUnselected, for: .normal)
            tab4Btn.setImage(tab4ImgSelected, for: .selected)
            tab4Btn.setColorByIndex(lv: lv)
        }
    }
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0, 1:
            return UiUtility.adaptiveSize(size: 141)
        case 2:
            return UiUtility.adaptiveSize(size: 306)
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        switch indexPath.row {
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath)
            cell.selectionStyle = .none
            
            let title: UILabel = cell.viewWithTag(1) as! UILabel
            let value: UILabel = cell.viewWithTag(2) as! UILabel
            
            title.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            title.textColor = UIColor.black
            title.text = "car_vin_intro_0".localized
            
            value.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
            value.textColor = UIColor(rgb: 0x4A4A4A)
            value.text = "car_vin_intro_1".localized
        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath)
            cell.selectionStyle = .none
            
            let title: UILabel = cell.viewWithTag(1) as! UILabel
            let value: UILabel = cell.viewWithTag(2) as! UILabel
            
            title.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            title.textColor = UIColor.black
            title.text = "car_vin_intro_2".localized
            
            value.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
            value.textColor = UIColor(rgb: 0x4A4A4A)
            value.text = "car_vin_intro_3".localized
        case 2:
            cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath)
            cell.selectionStyle = .none
        default:
            Logger.d("do nothing")
        }
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        Logger.d("You tapped cell number \(indexPath.row).")
//    }
    
    
    
    // MARK: picker view delegate
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if let addingCar = CarManager.shared().getAddingCar() {
            if let models = addingCar.models {
                return models.count
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let addingCar = CarManager.shared().getAddingCar()!
        return addingCar.models![row].name
    }
    
    
    
    // MARK: text field delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        editingTextField = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == add0valueVin.textField {
            let maxLength = 5
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        return true
    }
    
    
    
    // MARK: keyboard
    
    @objc override func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if let editingTextField = editingTextField {
                //                                let p = self.view.convert(editingTextField.center, to: self.view)
                let p = editingTextField.convert(editingTextField.center, to: self.view)
                //                                Logger.d("p.y: \(p.y)")
                //                                Logger.d("screen height: \(UiUtility.getScreenHeight())")
                //                                Logger.d("view.bounds.height: \(view.bounds.height)")
                
                let d = updateFromKeyboardChangeToFrame(keyboardSize)
                
                //                                Logger.d("view.bounds.height - p.y: \(view.bounds.height - p.y)")
                if view.bounds.height - p.y - 20 < d {
                    if self.view.frame.origin.y == offsetY {
                        //                        Logger.d("keyboardWillShow xx, \(d)")
                        self.view.frame.origin.y -= d
                    }
                }
            }
        }
        
        //        super.keyboardWillShow(notification: notification)
        keyboardShowing = true
    }
    
    @objc override func keyboardWillHide(notification: NSNotification) {
        
        if self.view.frame.origin.y != offsetY {
            self.view.frame.origin.y = offsetY
        }
        
        super.keyboardWillHide(notification: notification)
        keyboardShowing = false
    }
    
    
    
    // MARK: add car
    
    @objc func draggedAddView(_ sender:UIPanGestureRecognizer) {
        if keyboardShowing {
            return
        }
        //        self.view.bringSubviewToFront(containerCard)
        let translation = sender.translation(in: self.view)
        
        containerAdd.center = CGPoint(x: containerAdd.center.x/* + translation.x*/, y: min(addMaxCenterY, max(containerAdd.center.y + translation.y, addMinCenterY)))
        sender.setTranslation(CGPoint.zero, in: self.view)
        
        
        
        //        if sender.state == UIGestureRecognizer.State.began {
        //            Logger.d("began")
        //        } else if sender.state == UIGestureRecognizer.State.changed {
        //            Logger.d("changed")
        //        } else
        if sender.state == UIGestureRecognizer.State.ended {
            //            Logger.d("ended")
            let velocity = sender.velocity(in: view)
            if velocity.y > 0 {
                UIView.animate(withDuration: 0.3, animations: {
                    self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMaxCenterY)
                }) { (finished) in
                    self.viewShieldAdd.isHidden = true
                    self.containerAdd.isHidden = true
                    
                    self.afterClosingAddCar()
                }
            } else if velocity.y < 0 {
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMinCenterY)
                    self.viewShieldAdd.isHidden = false
                    self.containerAdd.isHidden = false
                })
            } else {
                if self.containerAdd.center.y - self.addMinCenterY > self.addMaxCenterY - self.containerAdd.center.y {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMaxCenterY)
                    }) { (finished) in
                        self.viewShieldAdd.isHidden = true
                        self.containerAdd.isHidden = true
                        
                        self.afterClosingAddCar()
                    }
                } else {
                    UIView.animate(withDuration: 0.3, animations: { () -> Void in
                        self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMinCenterY)
                        self.viewShieldAdd.isHidden = false
                        self.containerAdd.isHidden = false
                    })
                }
            }
        }
    }
    
    private func setAddVisibility(_ set: Bool, animated: Bool, isInit: Bool) {
        if animated {
            if set {
                self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMaxCenterY)
                self.containerAdd.isHidden = !set
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMinCenterY)
                    self.viewShieldAdd.isHidden = !set
                    
                    if isInit {
                        self.initAdd0Ui(isFirstCar: CarManager.shared().isAddingFirstCar())
                    }
                })
            } else {
                self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMinCenterY)
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMaxCenterY)
                }) { (finished) in
                    self.viewShieldAdd.isHidden = !set
                    self.containerAdd.isHidden = !set
                    
                    self.afterClosingAddCar()
                }
            }
        } else {
            self.viewShieldAdd.isHidden = !set
            self.containerAdd.isHidden = !set
        }
    }
    
    private func afterClosingAddCar() {
        if !self.viewAdd0.isHidden {
            CarManager.shared().appendAddingCarInfo(name: self.add0valueOwnerName.textField.text, platePrefix: self.add0valuePlate0.textField.text, plateSuffix: self.add0valuePlate1.textField.text, vinLast5: self.add0valueVin.textField.text)
        } else if !self.viewAdd2.isHidden {
            
        }
        
        if !viewAdd3.isHidden {
            CarManager.shared().deleteAddingCar()
            if selectedIndex == 1 {
                let vc = viewControllers[selectedIndex]
                if let naviController: UINavigationController = vc as? UINavigationController {
                    let vcCar: CarViewController = naviController.viewControllers[0] as! CarViewController
                    CarManager.shared().getCars(remotely: true, listener: vcCar)
                }
            }
            
        } else {
            showAddingCarWarning()
        }
    }
    
    private func showAddingCarWarning() {
        let alertController = UIAlertController(title: nil, message: "car_add_unfinish".localized, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "leave".localized, style: .default) { (action:UIAlertAction) in
            CarManager.shared().deleteAddingCar()
        }
        
        let action2 = UIAlertAction(title: "continue".localized, style: .cancel) { (action:UIAlertAction) in
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMinCenterY)
                self.viewShieldAdd.isHidden = false
                self.containerAdd.isHidden = false
            })
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        present(alertController, animated: true, completion: nil)
    }
    
    
    
    // MARK: step 0 of adding car
    
    @IBAction func goAddCar1Clicked(_ sender: UIButton) {
        viewAdd0.isHidden = true
        viewAdd1.isHidden = false
        viewAdd2.isHidden = true
        viewAdd3.isHidden = true
    }
    
    @IBAction func addCar0NextClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        
        CarManager.shared().appendAddingCarInfo(name: self.add0valueOwnerName.textField.text, platePrefix: self.add0valuePlate0.textField.text, plateSuffix: self.add0valuePlate1.textField.text, vinLast5: self.add0valueVin.textField.text)
        
        let addingCar: AddingCar = CarManager.shared().getAddingCar()!
        if Utility.isEmpty(addingCar.name) || Utility.isEmpty(addingCar.platePrefix) || Utility.isEmpty(addingCar.plateSuffix) || Utility.isEmpty(addingCar.vinLast5) {
            let alertController = UIAlertController(title: nil, message: "car_data_lack".localized, preferredStyle: .alert)
            let action1 = UIAlertAction(title: "fine".localized, style: .default) { (action:UIAlertAction) in
                Logger.d("do nothing")
            }
            
            alertController.addAction(action1)
            present(alertController, animated: true, completion: nil)
        } else {
            CarManager.shared().checkExist(listener: self)
        }
    }
    
    private func initAdd0Ui(isFirstCar: Bool) {
        viewAdd0.isHidden = false
        viewAdd1.isHidden = true
        viewAdd2.isHidden = true
        viewAdd3.isHidden = true
        
        add0valueOwnerName.isHidden = !isFirstCar
        add0labelOwnerName.isHidden = isFirstCar
        add0warning.isHidden = true
        
        let addingCar: AddingCar = CarManager.shared().getAddingCar()!
        if isFirstCar {
            add0valueOwnerName.textField.text = addingCar.name
        } else {
            add0labelOwnerName.text = addingCar.name
        }
        add0valuePlate0.textField.text = addingCar.platePrefix
        add0valuePlate1.textField.text = addingCar.plateSuffix
        add0valueVin.textField.text = addingCar.vinLast5
    }
    
    
    
    // MARK: step 1 of adding car
    
    @IBAction func backAddCar0Clicked(_ sender: UIButton) {
        viewAdd0.isHidden = false
        viewAdd1.isHidden = true
        viewAdd2.isHidden = true
        viewAdd3.isHidden = true
    }
    
    // MARK: step 2 of adding car
    
    private func updateAdd2Ui() {
        let addingCar = CarManager.shared().getAddingCar()!
        add2valueModel.text = addingCar.typeName
        
        add2ButtonType.setTitle(addingCar.models![addingCar.selectedModelIndex].name, for: .normal)
        pickerView.reloadComponent(0)
    }
    
    @IBAction func selectCarTypeClicked(_ sender: UIButton) {
        viewPickerContainer.isHidden = false
    }
    
    @IBAction func selectedCarTypeClicked(_ sender: UIButton) {
        viewPickerContainer.isHidden = true
        
        let index: Int = pickerView.selectedRow(inComponent: 0)
        //        Logger.d("index: \(index)")
        CarManager.shared().appendAddingCarModel(index: index)
        updateAdd2Ui()
    }
    
    @IBAction func confirmAddCar2Clicked(_ sender: UIButton) {
        CarManager.shared().addCar(listener: self)
    }
    
    // MARK: step 3 of adding car
    
    @IBAction func doneAddCarClicked(_ sender: UIButton) {
        self.setAddVisibility(false, animated: true, isInit: false)
    }
}

extension MainViewController {
    func menuClicked() {
        (self.navigationController as! DrawerNavigationController).handleMenuButton()
    }
    
    func notificationClicked() {
        (self.navigationController as! DrawerNavigationController).handleNotificationButton()
    }
    
    func addPostClicked() {
        showNoCarWarning()
    }
    
    func goSetting() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goEditProfile() {
        let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewControllerV2") as! EditProfileViewControllerV2
        vc.type = .profile
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goNewsDetail(news: News) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedDetailViewController") as! FeedDetailViewController
        vc.news = news
//        Logger.d("news: \(news.newsId)")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goBrowseLikers(news: News) {
        UiUtility.goBrowseLikers(of: news, controller: self)
    }
    
    func goBrowseReward() {
        UiUtility.goBrowseReward(controller: self)
    }
    
    func goBrowsePoint() {
        UiUtility.goBrowsePoint(controller: self)
    }
    
    func goBrowseMemberLevel() {
        UiUtility.goBrowseMemberLevel(controller: self)
    }
    
    func goBrowseFollow(of profile: Profile, isFollower: Bool) {
        UiUtility.goBrowseFollow(of: profile, isFollower: isFollower, controller: self)
    }
    
    func goFinishReport() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReportViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goEditPost(post: Post?, contentDelegate: ContentDelegate?, photoSourceType: PhotoSourceType) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditPostViewController") as! EditPostViewController
        vc.editingPost = post
        vc.contentDelegate = contentDelegate
        vc.photoSourceType = photoSourceType
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goEditPhotos(post: Post?, contentDelegate: ContentDelegate?) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditPhotosViewController") as! EditPhotosViewController
        vc.editingPost = post
        vc.contentDelegate = contentDelegate
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goDeeplink(deeplink: DeeplinkType) {
//        if deeplink == .Car {
//            goTab(of: 1, subTab: nil, deeplink: true)
//        } else if deeplink == .CarMaintain {
//            goTab(of: 1, subTab: 1, deeplink: true)
//        } else if deeplink == .memberCoupon {
//            goTab(of: 4, subTab: 0, deeplink: true)
//        } else if deeplink == .Reservation {
//            UiUtility.goReservation()
//        } else if deeplink == .memberLevel {
//            goTab(of: 4, subTab: 1, deeplink: true)
//        } else if deeplink == .member {
//            goTab(of: 4, subTab: nil, deeplink: true)
//        }
    }
    
    func addCar() {
        addCarDirectly()
    }
    
    func goRoadInfo() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RoadInfoViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goCarPage(of tab: Int) {
        self.goTab(of: 1, subTab: tab, deeplink: false)
    }
    
    func showOrderDetail(view: UIView) {
        self.navigationController?.view.addSubview(view)
        NSLayoutConstraint.activate([
            view.leadingAnchor.constraint(equalTo: self.navigationController!.view.leadingAnchor, constant: 0),
            view.topAnchor.constraint(equalTo: self.navigationController!.view.topAnchor,constant: 0),
            view.trailingAnchor.constraint(equalTo: self.navigationController!.view.trailingAnchor,constant: 0),
            view.bottomAnchor.constraint(equalTo: self.navigationController!.view.bottomAnchor,constant: 0)
        ])
        view.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
            view.alpha = 1
        }) { (success) in
        }
    }
    
    func goPatView(cars: [Car]) {
        let vc = UIStoryboard(name: "Car", bundle: nil).instantiateViewController(withIdentifier: "PatVC") as! PATViewController
        vc.cars = cars
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goViewController(view: UIViewController) {
        view.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    func goCreateClub(clubCarModels: [CarModel2]) {
        let navi = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateClubNavi") as! UINavigationController
        (navi.viewControllers[0] as! CreateClubViewController).clubCarModels = clubCarModels
        self.present(navi, animated: true, completion: nil)
    }
    
    func goClub(_ club: Club) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ClubViewController") as! ClubViewController
        vc.club = club
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func sortClub() {
        UiUtility.sortClub(controller: self, hasClub: false)
    }
    
    func goYourClub() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "YourClubViewController")// as! YourClubViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goExploreClub() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ExploreClubViewController")// as! YourClubViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goBrowseClubPostLikers(post: ClubPost) {
        UiUtility.goBrowseClubPostLikers(of: post, controller: self)
    }
    
    func goEditClubPost(clubId: Int, post: Post?, clubUpdateDelegate: ClubUpdateDelegate?) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditPostViewController") as! EditPostViewController
        vc.editingPost = post
        vc.contentDelegate = nil
        vc.mode = .club
        vc.clubId = clubId
        vc.clubUpdateDelegate = clubUpdateDelegate
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goClubPostDetail(post: ClubPost, clubUpdateDelegate: ClubUpdateDelegate?) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedDetailViewController") as! FeedDetailViewController
        vc.clubPost = post
        vc.clubUpdateDelegate = clubUpdateDelegate
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getTabbarIndex() -> Int {
        return self.selectedIndex
    }
}

