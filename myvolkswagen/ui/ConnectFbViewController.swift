//
//  ConnectFbViewController.swift
//  myvolkswagen
//
//  Created by Apple on 1/30/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Firebase

class ConnectFbViewController: BaseViewController {
    
    @IBOutlet weak var connect: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var later: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        connect.text = "connect_fb_account".localized
        connect.textColor = UIColor.black
        connect.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 22))
        
        subtitle.text = "connect_fb_subtitle".localized
        subtitle.textColor = UIColor(rgb: 0x4A4A4A)
        subtitle.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
        
        later.setTitle("connect_fb_later".localized, for: .normal)
        later.setTitleColor(UIColor.black, for: .normal)
        later.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
        
        // facebook should not be logged in
    }
    
    override func didFinishBindFb(success: Bool, isBind: Bool) {
        super.didFinishBindFb(success: success, isBind: isBind)
        
        if success {
            Analytics.logEvent("connect_fb", parameters: ["screen_name": "screen_login" as NSObject])
            next()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    @IBAction private func connectClicked(_ sender: Any) {
    }
    
    @IBAction private func laterClicked(_ sender: Any) {
        Analytics.logEvent("login_skip_fb", parameters: nil)
        next()
    }
    
    private func next() {
        let currentUser: User = UserManager.shared().currentUser
    
        if Utility.isEmpty(currentUser.lineUid) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConnectLineViewController") as! ConnectLineViewController
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            if UserManager.shared().currentUser.showProfile ?? false {
                // show profile editor
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
                vc.type = .login
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.updateRootVC(showAlert: false)
            }
        }
    }
}
