//
//  ContactUsViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/5.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
import SafariServices

class ContactUsViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func onlineBtnPressed(_ sender: Any) {
        self.dismiss(animated: true)
        UIApplication.shared.open(URL(string: Config.urlOnlineService)!)
        Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "專屬線上顧問", InboxId: "", EventCategory: "button", PageName: "pv_home", EventLabel: "專屬線上顧問", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
    }
    
    @IBAction func customerServiceBtnPressed(_ sender: Any) {
        Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "客服信箱", InboxId: "", EventCategory: "button", PageName: "pv_home", EventLabel: "客服信箱", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        self.dismiss(animated: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let mController = appDelegate.window?.rootViewController as? UINavigationController {
            if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
                vc.index = .loadUrl
                vc.loadUrl = URL(string: Config.urlContactUs)
                vc.mytitle = "客服信箱"
                mainVC.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @IBAction func phoneBtnPressed(_ sender: Any) {
        Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "撥打客服電話", InboxId: "", EventCategory: "button", PageName: "pv_home", EventLabel: "撥打客服電話", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        if let url = URL(string: "tel://0800828818") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}
