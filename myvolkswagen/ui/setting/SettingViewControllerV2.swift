//
//  SettingViewControllerV2.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/5.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SettingViewControllerV2: NaviViewController {

    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var notiSwitch: UISwitch!
    @IBOutlet weak var bg1: UIView!
    @IBOutlet weak var bg2: UIView!
    @IBOutlet weak var bg3: UIView!
    @IBOutlet weak var version: BasicLabel!
    var dongleFinished = 0
    var cars: [Car] = []
    let group = DispatchGroup()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        notiSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        naviBar.setTitle("設定")
        naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
        
        let gradientVieww = GradientBackgroundView()
        gradientVieww.frame = view.bounds
        gradientView.addSubview(gradientVieww)
        
        bg1.layer.cornerRadius = CGFloat(8).getFitSize()
        bg2.layer.cornerRadius = CGFloat(8).getFitSize()
        bg3.layer.cornerRadius = CGFloat(8).getFitSize()
        
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: .appDidBecomeActive, object: nil)
        
        if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            version.text = "版本 \(appVersion)"
        } else {
            print("Unable to retrieve app version")
        }
        
        cars = CarManager.shared().cars
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    @objc func appDidBecomeActive() {
        checkAuth()
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: .appDidBecomeActive, object: nil)
    }
    
    @IBAction func switchPressed(_ sender: Any) {
        DispatchQueue.main.async {
        let alertController = UIAlertController (title: "是否前往設定？", message: "欲開啟通知請進入 My Volkswagen>通知>允許通知", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "前往設定", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
            let cancelAction = UIAlertAction(title: "取消", style: .cancel) { success in
                self.checkAuth()
            }
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkAuth()
    }
    
    func checkAuth() {
        let current = UNUserNotificationCenter.current()
        current.getNotificationSettings() { (settings) in
            switch settings.authorizationStatus {
            case .authorized:
                DispatchQueue.main.async {
                    self.notiSwitch.setOn(true, animated: true)
                }
                break
            case .denied:
                DispatchQueue.main.async {
                    self.notiSwitch.setOn(false, animated: true)
                }
                break
            case .notDetermined:
                // 註冊一波
                print("notDetermined")
            case .provisional:
                break
            case .ephemeral:
                break
            @unknown default:
                break
            }
        }
    }
    
    @IBAction func privacyBtnPressed(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.loadUrl
        vc.loadUrl = URL(string: "\(Config.urlMemberPrivacy)?\(getDeeplinkParams())")
        vc.mytitle = "隱私權聲明"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func serviceBtnPressed(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.loadUrl
        vc.loadUrl = URL(string: "\(Config.urlMemberTerms)?\(getDeeplinkParams())")
        vc.mytitle = "服務條款"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func deleteAccountBtnPressed(_ sender: Any) {
        let customAlert = VWCustomAlert()
        customAlert.alertTitle = "刪除帳號"
        customAlert.alertMessage = "提醒您，若刪除帳號後您儲存在App上的所有數據都會永久刪除，無法重新回復原有紀錄。"
        customAlert.alertTag = 1
        customAlert.isCancelButtonHidden = false
        customAlert.okButtonAction = {
            self.deleteAccount()
        }
        self.present(customAlert, animated: true, completion: nil)
    }
    
    @IBAction func logoutBtnPressed(_ sender: Any) {
        let customAlert = VWCustomAlert()
        customAlert.alertTitle = "登出"
        customAlert.alertMessage = "登出後您隨時可以重新登入。"
        customAlert.alertTag = 1
        customAlert.isCancelButtonHidden = false
        customAlert.okButtonAction = {
            UserManager.shared().signOut(listener: self, showAlert: false)
        }
        self.present(customAlert, animated: true, completion: nil)
    }
    
    func deleteAccount() {
        showBGHud()
        dongleFinished = self.cars.count
        for cc in self.cars {
            CarManager.shared().getDongleBindingInfo(listener: self, plateNo: cc.licensePlateNumber!)
        }
        if self.cars.count == 0 {
            self.callDeleteAccountApi()
        }
    }
    
    override func didStartGetDongleBinding() {
    }
    
    override func didFinishGetDongleBinding(state: CarDoungleState?, dongleData: DongleData?) {
        for cc in self.cars {
            if cc.vin == dongleData?.vin {
                cc.dongleData = dongleData
            }
        }
        dongleFinished = dongleFinished - 1
        if dongleFinished == 0 { // finished
            dismissHud()
            var assToUnbindCount = 0
            if self.cars.count > 0 {
                for cc in self.cars {
                    if cc.dongleData?.state == "USER_MAPPED" {
                        assToUnbindCount = assToUnbindCount + 1
                        CarManager.shared().removeDongleBinding(listener: self, plateNo: cc.licensePlateNumber!, car: cc)
                    }
                    
                }
            }
            
            if assToUnbindCount > 0 {
                group.notify(queue: DispatchQueue.main) {
                    self.callDeleteAccountApi()
                }
            } else {
                callDeleteAccountApi()
            }
        }
    }
    
    func callDeleteAccountApi() {
        UserManager.shared().bindLine(set: false, lineUid: "", listener: self)
    }
    
    override func didFinishUnbindLine(success: Bool, isBind: Bool) {
        Alamofire.request(Config.urlMemberServer + "APP_Del_Account?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result

                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")

                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }

                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!

                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        UserManager.shared().signOut(listener: self, showAlert: true)
                    } else {

                    }
                })

                response.result.ifFailure({

                })
            }
    }
    
    override func didStartDeleteDongleBinding() {
        group.enter()
    }
    
    override func didFinishDeleteDongleBinding(success: Bool, car: Car?) {
        group.leave()
    }
}
