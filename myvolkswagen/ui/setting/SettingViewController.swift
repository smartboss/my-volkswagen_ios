//
//  SettingViewController.swift
//  myvolkswagen
//
//  Created by Apple on 1/31/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Firebase

class SettingViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource {
    
    enum Section: Int {
        case account = 0, id, about, deleteAccount, logout
        static var allCases: [Section] {
            var values: [Section] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    enum AccountRow: Int {
        case header = 0, editProfile, manageCar, member, padding
        
        static var allCases: [AccountRow] {
            var values: [AccountRow] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    enum IdRow: Int {
        case header = 0, connectFacebook, connectLine, padding
        
        static var allCases: [IdRow] {
            var values: [IdRow] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    enum AboutRow: Int {
        case privacy = 0, tos, version, padding
        
        static var allCases: [AboutRow] {
            var values: [AboutRow] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    enum DeleteAccountRow: Int {
        case deleteAccount = 0
        
        static var allCases: [DeleteAccountRow] {
            var values: [DeleteAccountRow] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    enum LogoutRow: Int {
        case logout = 0
        
        static var allCases: [LogoutRow] {
            var values: [LogoutRow] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        naviBar.setTitle("setting".localized)
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        
        tableView.backgroundColor = UiUtility.colorBackgroundWhite
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    override func didFinishBindFb(success: Bool, isBind: Bool) {
        super.didFinishBindFb(success: success, isBind: isBind)
        
        if success {
            tableView.beginUpdates()
            tableView.reloadRows(at: [NSIndexPath.init(row: IdRow.connectFacebook.rawValue, section: Section.id.rawValue) as IndexPath], with: UITableView.RowAnimation.fade)
            tableView.endUpdates()
            
            if !isBind {
            } else {
                Analytics.logEvent("connect_fb", parameters: ["screen_name": "screen_setting" as NSObject])
            }
        }
    }
    
    override func didFinishBindLine(success: Bool, isBind: Bool) {
        super.didFinishBindLine(success: success, isBind: isBind)
        
        if success {
            tableView.beginUpdates()
            tableView.reloadRows(at: [NSIndexPath.init(row: IdRow.connectLine.rawValue, section: Section.id.rawValue) as IndexPath], with: UITableView.RowAnimation.fade)
            tableView.endUpdates()
        }
    }
    
    override func didFinishUnbindLine(success: Bool, isBind: Bool) {
        super.didFinishUnbindLine(success: success, isBind: isBind)
        
        if success {
            tableView.beginUpdates()
            tableView.reloadRows(at: [NSIndexPath.init(row: IdRow.connectLine.rawValue, section: Section.id.rawValue) as IndexPath], with: UITableView.RowAnimation.fade)
            tableView.endUpdates()
        }
    }
    
    
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Section.init(rawValue: section)! {
        case .account:
            return AccountRow.allCases.count
        case .id:
            return IdRow.allCases.count
        case .about:
            return AboutRow.allCases.count
        case .deleteAccount:
            return DeleteAccountRow.allCases.count
        case .logout:
            return LogoutRow.allCases.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Section.init(rawValue: indexPath.section)! {
            case .account:
                switch AccountRow.init(rawValue: indexPath.row)! {
                    case .header:
                        return UiUtility.adaptiveSize(size: 46)
                    case .editProfile, .manageCar:
                        return UiUtility.adaptiveSize(size: 36)
                    case .member:
                        if UserManager.shared().getOTPFlag() == 1 {
                            return UiUtility.adaptiveSize(size: 68)
                        }
                        return UiUtility.adaptiveSize(size: 36)
                    case .padding:
                        return UiUtility.adaptiveSize(size: 32)
                }
            case .id:
                switch IdRow.init(rawValue: indexPath.row)! {
                    case .header:
                        return UiUtility.adaptiveSize(size: 46)
                    case .connectFacebook:
                        return UiUtility.adaptiveSize(size: 36)
                    case .connectLine:
                        return UiUtility.adaptiveSize(size: 36)
                    case .padding:
                        return UiUtility.adaptiveSize(size: 114)
                }
            case .about:
                switch AboutRow.init(rawValue: indexPath.row)! {
                case .privacy:
                    return UiUtility.adaptiveSize(size: 36)
                case .tos:
                    return UiUtility.adaptiveSize(size: 36)
                case .version:
                    return UiUtility.adaptiveSize(size: 36)
                case .padding:
                    return UiUtility.adaptiveSize(size: 29)
            }
            case .deleteAccount:
                switch DeleteAccountRow.init(rawValue: indexPath.row)! {
                case .deleteAccount:
                    return UiUtility.adaptiveSize(size: 36)
                }
            case .logout:
                switch LogoutRow.init(rawValue: indexPath.row)! {
                case .logout:
                    return UiUtility.adaptiveSize(size: 36)
            }
        }
        
        return 0
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        switch Section.init(rawValue: indexPath.section)! {
        case .account:
            switch AccountRow.init(rawValue: indexPath.row)! {
            case .header:
                cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath)
                cell.selectionStyle = .none
                
                let labelTitle: UILabel = cell.viewWithTag(1) as! UILabel
                labelTitle.text = "account".localized
                labelTitle.textColor = UiUtility.colorBrownGrey
                labelTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
            case .editProfile:
                cell = tableView.dequeueReusableCell(withIdentifier: "ItemArrowCell", for: indexPath)
                let labelTitle: UILabel = cell.viewWithTag(1) as! UILabel
                labelTitle.text = "edit_profile".localized
                labelTitle.textColor = UIColor.black
                labelTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            case .manageCar:
                cell = tableView.dequeueReusableCell(withIdentifier: "ItemArrowCell", for: indexPath)
                let labelTitle: UILabel = cell.viewWithTag(1) as! UILabel
                labelTitle.text = "edit_car".localized
                labelTitle.textColor = UIColor.black
                labelTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            case .member:
                if UserManager.shared().getOTPFlag() == 1 {
                    cell = tableView.dequeueReusableCell(withIdentifier: "ItemArrowInfoCell", for: indexPath)
                    let labelTitle: UILabel = cell.viewWithTag(1) as! UILabel
                    labelTitle.text = "vw_id_bind".localized
                    labelTitle.textColor = UIColor.black
                    labelTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
                    let labelTitle2: UILabel = cell.viewWithTag(2) as! UILabel
                    labelTitle2.text = "vw_id_bind_info".localized
                    labelTitle2.textColor = UIColor.black
                    labelTitle2.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "ItemArrowCell", for: indexPath)
                    let labelTitle: UILabel = cell.viewWithTag(1) as! UILabel
                    labelTitle.text = "vw_id_bind".localized
                    labelTitle.textColor = UIColor.black
                    labelTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
                }
            case .padding:
                cell = tableView.dequeueReusableCell(withIdentifier: "PaddingCell", for: indexPath)
                cell.selectionStyle = .none
            }
        case .id:
            switch IdRow.init(rawValue: indexPath.row)! {
            case .header:
                cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath)
                cell.selectionStyle = .none
                
                let labelTitle: UILabel = cell.viewWithTag(1) as! UILabel
                labelTitle.text = "id_binding".localized
                labelTitle.textColor = UiUtility.colorBrownGrey
                labelTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
            case .connectFacebook:
                cell = tableView.dequeueReusableCell(withIdentifier: "ItemCheckboxCell", for: indexPath)
                let labelTitle: UILabel = cell.viewWithTag(1) as! UILabel
                labelTitle.text = "bind_fb".localized
                labelTitle.textColor = UIColor.black
                labelTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
                
                let checkbox: UIImageView = cell.viewWithTag(2) as! UIImageView
                let currentUser: User = UserManager.shared().currentUser
//                Logger.d("current user: \(currentUser)")
                
                if !Utility.isEmpty(currentUser.fbId) {
                    checkbox.image = UIImage.init(named: "fbBinding")
                } else {
                    checkbox.image = UIImage.init(named: "fbBindDefault")
                }
            case .connectLine:
                cell = tableView.dequeueReusableCell(withIdentifier: "ItemCheckboxCell2", for: indexPath)
                let labelTitle: UILabel = cell.viewWithTag(1) as! UILabel
                labelTitle.text = "bind_line".localized
                labelTitle.textColor = UIColor.black
                labelTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
                
                let checkbox: UIImageView = cell.viewWithTag(2) as! UIImageView
                let currentUser: User = UserManager.shared().currentUser
//                Logger.d("current user: \(currentUser)")
                
                if !Utility.isEmpty(currentUser.lineUid) {
                    checkbox.image = UIImage.init(named: "fbBinding")
                } else {
                    checkbox.image = UIImage.init(named: "fbBindDefault")
                }
            case .padding:
                cell = tableView.dequeueReusableCell(withIdentifier: "PaddingCell", for: indexPath)
                cell.selectionStyle = .none
            }
        case .about:
            switch AboutRow.init(rawValue: indexPath.row)! {
            case .privacy:
                cell = tableView.dequeueReusableCell(withIdentifier: "ItemArrowCell", for: indexPath)
                let labelTitle: UILabel = cell.viewWithTag(1) as! UILabel
                labelTitle.text = "privacy_statement".localized
                labelTitle.textColor = UIColor.black
                labelTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            case .tos:
                cell = tableView.dequeueReusableCell(withIdentifier: "ItemArrowCell", for: indexPath)
                let labelTitle: UILabel = cell.viewWithTag(1) as! UILabel
                labelTitle.text = "term_of_service".localized
                labelTitle.textColor = UIColor.black
                labelTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            case .version:
                cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath)
                cell.selectionStyle = .none
                let labelTitle: UILabel = cell.viewWithTag(1) as! UILabel
                labelTitle.text = "\("version".localized) \(Utility.versionText())"
                labelTitle.textColor = UIColor.black
                labelTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            case .padding:
                cell = tableView.dequeueReusableCell(withIdentifier: "PaddingCell", for: indexPath)
                cell.selectionStyle = .none
            }
        case .deleteAccount:
            switch DeleteAccountRow.init(rawValue: indexPath.row)! {
            case .deleteAccount:
                cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath)
                let labelTitle: UILabel = cell.viewWithTag(1) as! UILabel
                labelTitle.text = "delete_account".localized
                labelTitle.textColor = UIColor.black
                labelTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            }
        case .logout:
            switch LogoutRow.init(rawValue: indexPath.row)! {
            case .logout:
                cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath)
                let labelTitle: UILabel = cell.viewWithTag(1) as! UILabel
                labelTitle.text = "logout".localized
                labelTitle.textColor = UIColor.black
                labelTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            }
        }
        
        cell.backgroundColor = UiUtility.colorBackgroundWhite
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        Logger.d("You tapped cell number \(indexPath.row).")
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch Section.init(rawValue: indexPath.section)! {
        case .account:
            switch AccountRow.init(rawValue: indexPath.row)! {
            case .editProfile:
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
                vc.type = .profile
                self.navigationController?.pushViewController(vc, animated: true)
            case .manageCar:
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditCarViewController") as! EditCarViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case .member:
                let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "BindVWIDViewController") as! BindVWIDViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
//                if Utility.isEmpty(UserManager.shared().currentUser.vwEmail) {
//
//                } else {
//                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VWIDViewController") as! VWIDViewController
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }
            default:
                break
            }
            break
        case .id:
            switch IdRow.init(rawValue: indexPath.row)! {
            case .connectFacebook:
                
                let currentUser: User = UserManager.shared().currentUser
                //                Logger.d("current user: \(currentUser)")
                
                if Utility.isEmpty(currentUser.fbId) {
//                    Logger.d("bind fb")
                } else {
//                    Logger.d("unbind fb")
                    showUnbindWarning()
                }
            case .connectLine:
                let currentUser: User = UserManager.shared().currentUser
                
                if Utility.isEmpty(currentUser.lineUid) {
                } else {
                    showLineUnbindWarning()
                }
            default:
                break
            }
        case .about:
            switch AboutRow.init(rawValue: indexPath.row)! {
            case .privacy:
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
                vc.index = WebsiteIndex.privacy
                self.navigationController?.pushViewController(vc, animated: true)
            case .tos:
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
                vc.index = WebsiteIndex.tos
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                break
            }
            break
        case .deleteAccount:
            switch DeleteAccountRow.init(rawValue: indexPath.row)! {
            case .deleteAccount:
                let sb = UIStoryboard(name: "Login", bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "DeleteAccountViewController") as! DeleteAccountViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case .logout:
            switch LogoutRow.init(rawValue: indexPath.row)! {
            case .logout:
                let alertController = UIAlertController(title: nil, message: "warning_logout".localized, preferredStyle: .alert)
                
                let action2 = UIAlertAction(title: "forget_it".localized, style: .cancel) { (action:UIAlertAction) in
                    Logger.d("You've pressed cancel");
                }
                
                let action3 = UIAlertAction(title: "logout".localized, style: .destructive) { (action:UIAlertAction) in
                    UserManager.shared().signOut(listener: self, showAlert: false)
                }
                
                alertController.addAction(action2)
                alertController.addAction(action3)
                present(alertController, animated: true, completion: nil)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func showUnbindWarning() {
        let alertController = UIAlertController(title: "unbind_fb_alert_title".localized, message: "unbind_fb_alert_message".localized, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "unbind".localized, style: .default) { (action:UIAlertAction) in
            let currentUser: User = UserManager.shared().currentUser
            UserManager.shared().bindFb(set: false, fbId: currentUser.fbId!, listener: self)
        }
        
        let action2 = UIAlertAction(title: "forget_it".localized, style: .cancel) { (action:UIAlertAction) in
            Logger.d("You've pressed cancel");
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        present(alertController, animated: true, completion: nil)
    }
    
    func showLineUnbindWarning() {
        let alertController = UIAlertController(title: "unbind_line_alert_title".localized, message: "unbind_line_alert_message".localized, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "unbind".localized, style: .default) { (action:UIAlertAction) in
            let currentUser: User = UserManager.shared().currentUser
            UserManager.shared().bindLine(set: false, lineUid: currentUser.lineUid!, listener: self)
        }
        
        let action2 = UIAlertAction(title: "forget_it".localized, style: .cancel) { (action:UIAlertAction) in
            Logger.d("You've pressed cancel");
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        present(alertController, animated: true, completion: nil)
    }
}
