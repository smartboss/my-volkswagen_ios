//
//  VWIDViewController.swift
//  myvolkswagen
//
//  Created by Apple on 3/5/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class VWIDViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var buttonChangePassword: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var levelName: String?
    var levelIntro: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UiUtility.colorBackgroundWhite
        tableView.backgroundColor = UiUtility.colorBackgroundWhite
        
        naviBar.setTitle("vw_id_and_credit".localized)
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        
        labelEmail.textColor = UIColor(rgb: 0x4A4A4A)
        labelEmail.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
        labelEmail.text = "\("vw_email_title".localized) \(UserManager.shared().currentUser.vwEmail)"
        
        buttonChangePassword.setTitleColor(UIColor.black, for: .normal)
        buttonChangePassword.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        buttonChangePassword.setTitle("vw_change_password".localized, for: .normal)
        
        UserManager.shared().getMemberLevelInfo(listener: self)
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    
    
    // MARK: table view delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = levelName, let _ = levelIntro {
            return 2
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UiUtility.adaptiveSize(size: 130)
        } else {
            let width = UiUtility.adaptiveSize(size: 375 - 40 - 27)
            let font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))!
            let minContentHeight: CGFloat = UiUtility.adaptiveSize(size: 0)
            
            var h: CGFloat = UiUtility.adaptiveSize(size: 30)
            
            if !Utility.isEmpty(levelIntro) {
                h += max(levelIntro!.height(withConstrainedWidth: width, font: font), minContentHeight)
            }
            
            return h
        }
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        if indexPath.row == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "Row0Cell", for: indexPath)
            cell.selectionStyle = .none
            
            let title: UILabel = cell.viewWithTag(1) as! UILabel
            let value: UILabel = cell.viewWithTag(2) as! UILabel
            
            title.textColor = UIColor(rgb: 0x4A4A4A)
            title.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
            title.text = "member_level".localized
            value.textColor = UIColor(rgb: 0x4A4A4A)
            value.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
//            value.text = UserManager.shared().currentUser.levelName
            value.text = levelName
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "Row1Cell", for: indexPath)
            cell.selectionStyle = .none
            
            for constraint in cell.contentView.constraints {
                if constraint.identifier == "value_top" {
                    constraint.constant = UiUtility.adaptiveSize(size: 30)
                }
            }
            
            let title: UILabel = cell.viewWithTag(1) as! UILabel
            let value: UILabel = cell.viewWithTag(2) as! UILabel
            
            title.textColor = UIColor(rgb: 0x4A4A4A)
            title.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
            title.text = "vw_rights".localized
            value.textColor = UIColor(rgb: 0x4A4A4A)
            value.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
            value.text = levelIntro
        }
        
        
        cell.backgroundColor = UiUtility.colorBackgroundWhite
        
        return cell
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction private func idAndPasswordClicked(_ sender: UIButton) {
        if let link = URL(string: Config.urlChangePassword) {
            UIApplication.shared.open(link)
        }
    }
    
    
    
    // MARK: user protocol
    
    override func didStartGetMemberLevelInfo() {
        showHud()
    }
    override func didFinishGetMemberLevelInfo(success: Bool, level: Int?, levelName: String?, levelIntro: String?) {
        dismissHud()
        
        if success {
            self.levelName = levelName
            self.levelIntro = levelIntro
            
            tableView.reloadData()
        }
    }
}
