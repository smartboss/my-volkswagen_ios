//
//  PInput.swift
//  myvolkswagen
//
//  Created by Apple on 2/22/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class PInput: UIView {
    
    weak var contentView: UIView!
    
    var colorWarning: UIColor!
    var colorInput: UIColor!
    var colorUnderline: UIColor!
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var buttonClear: UIButton!
    @IBOutlet weak var warning: UILabel!
    @IBOutlet weak var dropdownPic: UIImageView!
    @IBOutlet weak var dropDownBtn: UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    private func commonInit() {
        
        colorWarning = UiUtility.colorPinkyRed
        colorInput = UIColor(red: 0.29, green: 0.33, blue: 0.41, alpha: 1)
        colorUnderline = UIColor.black.withAlphaComponent(0.1)
        
        contentView = (Bundle.main.loadNibNamed("PInput", owner: self, options: nil)?.first as! UIView)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        textField.textColor = colorInput
        textField.font = UIFont(name: "PingFangTC-Light", size: UiUtility.adaptiveSize(size: 16))
        
        warning.textColor = colorWarning
        warning.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 12))
        
        buttonClear.addTarget(self, action:#selector(clearClicked(_:)), for: .touchUpInside)
    }
    
    func setupWith(hint: String) {
        textField.textColor = colorInput
        textField.text = nil
        textField.placeholder = hint
        buttonClear.isHidden = true
        warning.isHidden = true
    }
    
    func warnWith(_ w: String) {
        textField.textColor = colorWarning
        warning.text = w
        warning.isHidden = false
    }
    
    func warnWithBlack(_ w: String) {
        warning.text = w
        warning.textColor = UIColor(red: 0.31, green: 0.35, blue: 0.41, alpha: 1)
        warning.isHidden = false
    }
    
    func unwarn() {
        textField.textColor = colorInput
        warning.isHidden = true
    }
    
    @objc private func clearClicked(_ sender: UIButton) {
        textField.text = ""
        buttonClear.isHidden = true
        unwarn()
        textField.sendActions(for: .editingChanged)
    }
    
    @IBAction func dropDownClicked(_ sender: Any) {
        
    }
}
