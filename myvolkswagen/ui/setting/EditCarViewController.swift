//
//  EditCarViewController.swift
//  myvolkswagen
//
//  Created by Apple on 2/25/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase

class EditCarViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    enum Mode: Int {
        case normal = 0, edit
    }
    
    enum Section: Int {
        case cars = 0, add
        static var allCases: [Section] {
            var values: [Section] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    var mode: Mode = .normal
    var cars: [Car] = []
    
    @IBOutlet weak var viewEmpty: UIView!
    @IBOutlet weak var labelEmpty: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    
    
    ///
    // add car
    @IBOutlet weak var viewShieldAdd: UIView!
    @IBOutlet weak var containerAdd: UIView!
    var addMinCenterY: CGFloat = 0
    var addMaxCenterY: CGFloat = 0
    @IBOutlet weak var viewAdd0: UIView!
    @IBOutlet weak var add0titleOwnerName: UILabel!
    @IBOutlet weak var add0valueOwnerName: UnderlineInput!
    @IBOutlet weak var add0labelOwnerName: UILabel!
    @IBOutlet weak var add0titlePlate: UILabel!
    @IBOutlet weak var add0valuePlate0: UnderlineInput!
    @IBOutlet weak var add0valuePlate1: UnderlineInput!
    @IBOutlet weak var add0titleVin: UILabel!
    @IBOutlet weak var add0buttonVin: UIButton!
    @IBOutlet weak var add0valueVin: UnderlineInput!
    @IBOutlet weak var add0warning: UILabel!
    @IBOutlet weak var viewAdd1: UIView!
    @IBOutlet weak var add1tableView: UITableView!
    @IBOutlet weak var viewAdd2: UIView!
    @IBOutlet weak var add2titleModel: UILabel!
    @IBOutlet weak var add2valueModel: UILabel!
    @IBOutlet weak var add2titleType: UILabel!
    @IBOutlet weak var add2containerType: UIView!
    @IBOutlet weak var add2ButtonType: UIButton!
    @IBOutlet weak var viewAdd3: UIView!
    @IBOutlet weak var add3title: UILabel!
    
    @IBOutlet weak var viewPickerContainer: UIView!
    @IBOutlet weak var buttonPickerDone: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    ///

    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()

        view.backgroundColor = UiUtility.colorBackgroundWhite
        naviBar.setTitle("edit_my_car".localized)
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        
        viewEmpty.isHidden = true
        viewEmpty.backgroundColor = UiUtility.colorBackgroundWhite
        labelEmpty.textColor = UIColor(rgb: 0x4A4A4A)
        labelEmpty.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
        labelEmpty.text = "car_empty_msg".localized
        
        tableView.backgroundColor = UiUtility.colorBackgroundWhite
        
        cars = CarManager.shared().getCars(remotely: true, listener: self)
        if !cars.isEmpty {
            reloadUi()
        }
        
        
        
        // add car
        let font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        
        setAddVisibility(false, animated: false, isInit: true)
        var panAddGesture = UIPanGestureRecognizer()
        panAddGesture = UIPanGestureRecognizer(target: self, action: #selector(draggedAddView(_:)))
        //        containerCard.isUserInteractionEnabled = true
        containerAdd.addGestureRecognizer(panAddGesture)
        add0titleOwnerName.textColor = UIColor.black
        add0titleOwnerName.font = font
        add0titleOwnerName.text = "car_owner_name".localized
        add0valueOwnerName.setHint("hint_car_owner_name".localized)
        add0valueOwnerName.textField.inputAccessoryView = toolbar
        add0valueOwnerName.textField.delegate = self
        add0labelOwnerName.textColor = UIColor.black
        add0labelOwnerName.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))!
        add0labelOwnerName.text = "David Lee".localized
        add0titlePlate.textColor = UIColor.black
        add0titlePlate.font = font
        add0titlePlate.text = "car_plate_number".localized
        add0valuePlate0.setHint("abc")
        add0valuePlate0.textField.inputAccessoryView = toolbar
        add0valuePlate0.textField.autocapitalizationType = .allCharacters
        add0valuePlate0.textField.delegate = self
        add0valuePlate1.setHint("1234")
        add0valuePlate1.textField.inputAccessoryView = toolbar
        add0valuePlate1.textField.autocapitalizationType = .allCharacters
        add0valuePlate1.textField.delegate = self
        add0titleVin.textColor = UIColor.black
        add0titleVin.font = font
        add0titleVin.text = "car_vin_last_5_number".localized
        add0buttonVin.setTitleColor(UiUtility.colorPinkyRed, for: .normal)
        add0buttonVin.titleLabel?.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))!
        add0buttonVin.setTitle("where_is_vin".localized, for: .normal)
        add0valueVin.setHint("12345")
        add0valueVin.textField.inputAccessoryView = toolbar
        add0valueVin.textField.autocapitalizationType = .allCharacters
        add0valueVin.textField.delegate = self
        add0warning.textColor = UiUtility.colorPinkyRed
        add0warning.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 12))!
        add0warning.isHidden = true
        
        add2titleModel.textColor = UIColor.black
        add2titleModel.font = font
        add2titleModel.text = "car_model_title".localized
        add2valueModel.textColor = UiUtility.colorDarkGrey
        add2valueModel.font = UIFont(name: "VWHead-Bold", size: UiUtility.adaptiveSize(size: 22))
        add2valueModel.text = "The Tiguan Allspace"
        add2titleType.textColor = UIColor.black
        add2titleType.font = font
        add2titleType.text = "car_type_title".localized
        add2containerType.layer.cornerRadius = UiUtility.adaptiveSize(size: 3)
        add2containerType.backgroundColor = UiUtility.colorBrownGrey.withAlphaComponent(0.3)
        add2ButtonType.setTitle("Allspace 330 TSI Comfortline", for: .normal)
        add2ButtonType.setTitleColor(UIColor(rgb: 0x4A4A4A), for: .normal)
        add2ButtonType.titleLabel?.font = UIFont(name: "VWHead", size: UiUtility.adaptiveSize(size: 15))
        buttonPickerDone.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        buttonPickerDone.setTitle("done".localized, for: .normal)
        add3title.textColor = UIColor.black
        add3title.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))!
        add3title.text = "car_add_success".localized
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if offsetY == -1 {
            offsetY = self.view.frame.origin.y
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    override func doneButtonClicked(_ button: UIButton) {
        mode = .normal
        reloadUi()
    }
    
    override func editButtonClicked(_ button: UIButton) {
        mode = .edit
        reloadUi()
    }
    
    
    
    // MARK: picker view delegate
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if let addingCar = CarManager.shared().getAddingCar() {
            if let models = addingCar.models {
                return models.count
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let addingCar = CarManager.shared().getAddingCar()!
        return addingCar.models![row].name
    }
    
    
    
    // MARK: car protocol
    
    override func didStartGetCar() {
        showHud()
    }
    
    override func didFinishGetCar(success: Bool, cars: [Car]) {
        dismissHud()
        
        self.cars = cars
        reloadUi()
    }
    
    override func didStartRemoveCar() {
        showHud()
    }
    
    override func didFinishRemoveCar(success: Bool, car: Car) {
        dismissHud()
        
        if success {
//            mode = .normal
            cars = CarManager.shared().getCars(remotely: false, listener: nil)
            reloadUi()
        }
    }
    
    override func didStartCheckCarExist() {
        showHud()
    }
    
    override func didFinishCheckCarExist(result: Result?) {
        dismissHud()
        
        if let result = result {
            if result.isSuccess() {
                CarManager.shared().getModel(listener: self)
            } else {
                if let code: Int = result.errorCode {
                    switch code {
                    case 3004:
                        add0warning.text = "err_msg_3004".localized
                        add0warning.isHidden = false
                    case 3010:
                        add0warning.text = "err_msg_3010".localized
                        add0warning.isHidden = false
                    case 3001:
                        add0warning.text = "err_msg_3001".localized
                        add0warning.isHidden = false
                    case 3003:
                        add0warning.text = "err_msg_3003".localized
                        add0warning.isHidden = false
                    case 3005:
                        add0warning.text = "err_msg_3005".localized
                        add0warning.isHidden = false
                    default:
                        add0warning.isHidden = true
                    }
                }
            }
        }
    }
    
    override func didStartGetCarModel() {
        showHud()
    }
    
    override func didFinishGetCarModel(success: Bool, models: [Model]?) {
        dismissHud()
        
        if success {
            viewAdd0.isHidden = true
            viewAdd1.isHidden = true
            viewAdd2.isHidden = false
            viewAdd3.isHidden = true
            
            updateAdd2Ui()
        }
    }
    
    override func didStartAddCar() {
        showHud()
    }
    
    override func didFinishAddCar(success: Bool) {
        dismissHud()
        
        if success {
            viewAdd0.isHidden = true
            viewAdd1.isHidden = true
            viewAdd2.isHidden = true
            viewAdd3.isHidden = false
        }
    }
    
    
    
    // MARK: text field delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        editingTextField = textField
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == add0valueVin.textField {
            let maxLength = 5
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        return true
    }
    
    
    
    // MARK: keyboard
    
    @objc override func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if let editingTextField = editingTextField {
                //                                let p = self.view.convert(editingTextField.center, to: self.view)
                let p = editingTextField.convert(editingTextField.center, to: self.view)
                //                                Logger.d("p.y: \(p.y)")
                //                                Logger.d("screen height: \(UiUtility.getScreenHeight())")
                //                                Logger.d("view.bounds.height: \(view.bounds.height)")
                
                let d = updateFromKeyboardChangeToFrame(keyboardSize)
                
                //                                Logger.d("view.bounds.height - p.y: \(view.bounds.height - p.y)")
                if view.bounds.height - p.y - 20 < d {
                    if self.view.frame.origin.y == offsetY {
                        //                        Logger.d("keyboardWillShow xx, \(d)")
                        self.view.frame.origin.y -= d
                    }
                }
            }
        }
        
        //        super.keyboardWillShow(notification: notification)
        keyboardShowing = true
    }
    
    @objc override func keyboardWillHide(notification: NSNotification) {
        
        if self.view.frame.origin.y != offsetY {
            self.view.frame.origin.y = offsetY
        }
        
        super.keyboardWillHide(notification: notification)
        keyboardShowing = false
    }
    
    
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.tableView {
            return Section.allCases.count
        } else if tableView == add1tableView {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            switch mode {
            case .normal:
                switch Section.init(rawValue: section)! {
                case .cars:
                    return cars.count
                case .add:
                    return 0
                }
                
            case .edit:
                switch Section.init(rawValue: section)! {
                case .cars:
                    return cars.count
                case .add:
                    return 1
                }
            }
        } else if tableView == add1tableView {
            return 3
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tableView {
            switch Section.init(rawValue: indexPath.section)! {
            case .cars:
                return UiUtility.adaptiveSize(size: 130)
            case .add:
                return UiUtility.adaptiveSize(size: 51)
            }
        } else if tableView == add1tableView {
            switch indexPath.row {
            case 0, 1:
                return UiUtility.adaptiveSize(size: 141)
            case 2:
                return UiUtility.adaptiveSize(size: 306)
            default:
                return 0
            }
        }
        
        return 0
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        if tableView == self.tableView {
            switch Section.init(rawValue: indexPath.section)! {
            case .cars:
                cell = tableView.dequeueReusableCell(withIdentifier: "CarCell", for: indexPath)
                cell.selectionStyle = .none
                
                let imageView: UIImageView = cell.viewWithTag(1) as! UIImageView
                let labelType: UILabel = cell.viewWithTag(2) as! UILabel
                let labelPlate: UILabel = cell.viewWithTag(3) as! UILabel
                let button: UIButton = cell.viewWithTag(4) as! UIButton
                
                let car: Car = cars[indexPath.row]
                
                if let path = car.carModelImage {
                    let url = URL(string: path)
                    imageView.kf.setImage(with: url)
                }
                
                labelType.text = car.carTypeName
                labelType.textColor = UIColor.black
                labelType.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
                labelPlate.text = car.licensePlateNumber
                labelPlate.textColor = UIColor(rgb: 0x4A4A4A)
                labelPlate.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
                button.isHidden = (mode == .normal)
                button.addTarget(self, action:#selector(removeClicked(_:)), for: .touchUpInside)
            case .add:
                cell = tableView.dequeueReusableCell(withIdentifier: "AddCell", for: indexPath)
                cell.selectionStyle = .none
                
                let button: UIButton = cell.viewWithTag(1) as! UIButton
                button.setTitleColor(UiUtility.colorPinkyRed, for: .normal)
                button.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
                button.setTitle("add_car".localized, for: .normal)
                button.addTarget(self, action:#selector(addClicked(_:)), for: .touchUpInside)
            }
            
            cell.backgroundColor = UiUtility.colorBackgroundWhite
        } else if tableView == add1tableView {
            switch indexPath.row {
            case 0:
                cell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath)
                cell.selectionStyle = .none
                
                let title: UILabel = cell.viewWithTag(1) as! UILabel
                let value: UILabel = cell.viewWithTag(2) as! UILabel
                
                title.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
                title.textColor = UIColor.black
                title.text = "car_vin_intro_0".localized
                
                value.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
                value.textColor = UIColor(rgb: 0x4A4A4A)
                value.text = "car_vin_intro_1".localized
            case 1:
                cell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath)
                cell.selectionStyle = .none
                
                let title: UILabel = cell.viewWithTag(1) as! UILabel
                let value: UILabel = cell.viewWithTag(2) as! UILabel
                
                title.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
                title.textColor = UIColor.black
                title.text = "car_vin_intro_2".localized
                
                value.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
                value.textColor = UIColor(rgb: 0x4A4A4A)
                value.text = "car_vin_intro_3".localized
            case 2:
                cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath)
                cell.selectionStyle = .none
            default:
                Logger.d("do nothing")
            }
        }
        
        return cell
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func draggedAddView(_ sender:UIPanGestureRecognizer) {
        if keyboardShowing {
            return
        }
        //        self.view.bringSubviewToFront(containerCard)
        let translation = sender.translation(in: self.view)
        
        containerAdd.center = CGPoint(x: containerAdd.center.x/* + translation.x*/, y: min(addMaxCenterY, max(containerAdd.center.y + translation.y, addMinCenterY)))
        sender.setTranslation(CGPoint.zero, in: self.view)
        
        
        
        //        if sender.state == UIGestureRecognizer.State.began {
        //            Logger.d("began")
        //        } else if sender.state == UIGestureRecognizer.State.changed {
        //            Logger.d("changed")
        //        } else
        if sender.state == UIGestureRecognizer.State.ended {
            //            Logger.d("ended")
            let velocity = sender.velocity(in: view)
            if velocity.y > 0 {
                UIView.animate(withDuration: 0.3, animations: {
                    self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMaxCenterY)
                }) { (finished) in
                    self.viewShieldAdd.isHidden = true
                    self.containerAdd.isHidden = true
                    
                    self.afterClosingAddCar()
                }
            } else if velocity.y < 0 {
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMinCenterY)
                    self.viewShieldAdd.isHidden = false
                    self.containerAdd.isHidden = false
                })
            } else {
                if self.containerAdd.center.y - self.addMinCenterY > self.addMaxCenterY - self.containerAdd.center.y {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMaxCenterY)
                    }) { (finished) in
                        self.viewShieldAdd.isHidden = true
                        self.containerAdd.isHidden = true
                        
                        self.afterClosingAddCar()
                    }
                } else {
                    UIView.animate(withDuration: 0.3, animations: { () -> Void in
                        self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMinCenterY)
                        self.viewShieldAdd.isHidden = false
                        self.containerAdd.isHidden = false
                    })
                }
            }
        }
    }

    @IBAction private func addClicked(_ sender: Any) {
        Analytics.logEvent("add_car", parameters: ["screen_name": "screen_setting" as NSObject])
        if cars.count >= Config.maxCarCount {
            let alertController = UIAlertController(title: nil, message: "car_reach_max".localized, preferredStyle: .alert)
            
            let action2 = UIAlertAction(title: "close".localized, style: .cancel) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(action2)
            present(alertController, animated: true, completion: nil)
        } else {
            // prepare adding data
            CarManager.shared().startAddingCar()
            
            if addMinCenterY == 0 && addMaxCenterY == 0 {
                addMinCenterY = containerAdd.center.y
                //            Logger.d("card height: \(containerAdd.frame.height)")
                //            Logger.d("self.view height: \(view.frame.height)")
                
                addMaxCenterY = view.frame.height + (containerAdd.frame.height / 2)
                
                //            Logger.d("addMinCenterY: \(addMinCenterY)")
                //            Logger.d("addMaxCenterY: \(addMaxCenterY)")
            }
            setAddVisibility(true, animated: true, isInit: true)
        }
    }
    
    @objc private func removeClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            Logger.d("click at row \(String(describing: indexPath.row))")
            let car: Car = cars[indexPath.row]
            showRemoveWarning(car: car)
        }
    }
    
    private func reloadUi() {
//        if cars.isEmpty {
//            viewEmpty.isHidden = false
//            naviBar.setEditVisibility(false)
//            naviBar.setDoneVisibility(false)
//        } else {
//            viewEmpty.isHidden = true
//            switch mode {
//            case .normal:
//                naviBar.setEditVisibility(true)
//                naviBar.setDoneVisibility(false)
//                naviBar.setLeftVisibility(true)
//            case .edit:
//                naviBar.setDoneVisibility(true)
//                naviBar.setEditVisibility(false)
//                naviBar.setLeftVisibility(false)
//            }
//
//            tableView.reloadData()
//        }
        
        viewEmpty.isHidden = true
        switch mode {
        case .normal:
            if cars.isEmpty {
                viewEmpty.isHidden = false
                naviBar.setEditVisibility(false)
                naviBar.setDoneVisibility(false)
                naviBar.setLeftVisibility(true)
                return
            } else {
                naviBar.setEditVisibility(true)
                naviBar.setDoneVisibility(false)
                naviBar.setLeftVisibility(true)
            }
        case .edit:
            naviBar.setDoneVisibility(true)
            naviBar.setEditVisibility(false)
            naviBar.setLeftVisibility(false)
        }
        
        tableView.reloadData()
    }
    
    
    
    private func setAddVisibility(_ set: Bool, animated: Bool, isInit: Bool) {
        //        Logger.d("setAddVisibility, \(set)")
        if animated {
            if set {
                self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMaxCenterY)
                self.containerAdd.isHidden = !set
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMinCenterY)
                    self.viewShieldAdd.isHidden = !set
                    
                    if isInit {
                        self.initAdd0Ui(isFirstCar: CarManager.shared().isAddingFirstCar())
                    }
                })
            } else {
                self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMinCenterY)
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMaxCenterY)
                }) { (finished) in
                    self.viewShieldAdd.isHidden = !set
                    self.containerAdd.isHidden = !set
                    
                    self.afterClosingAddCar()
                }
            }
        } else {
            self.viewShieldAdd.isHidden = !set
            self.containerAdd.isHidden = !set
        }
    }
    
    private func afterClosingAddCar() {
        if !self.viewAdd0.isHidden {
            CarManager.shared().appendAddingCarInfo(name: self.add0valueOwnerName.textField.text, platePrefix: self.add0valuePlate0.textField.text, plateSuffix: self.add0valuePlate1.textField.text, vinLast5: self.add0valueVin.textField.text)
        } else if !self.viewAdd2.isHidden {
            
        }
        
        if !viewAdd3.isHidden {
            CarManager.shared().deleteAddingCar()
            CarManager.shared().getCars(remotely: true, listener: self)
        } else {
            showAddingCarWarning()
        }
    }
    
    private func showAddingCarWarning() {
        let alertController = UIAlertController(title: nil, message: "car_add_unfinish".localized, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "leave".localized, style: .default) { (action:UIAlertAction) in
            CarManager.shared().deleteAddingCar()
        }
        
        let action2 = UIAlertAction(title: "continue".localized, style: .cancel) { (action:UIAlertAction) in
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.containerAdd.center = CGPoint(x: self.containerAdd.center.x, y: self.addMinCenterY)
                self.viewShieldAdd.isHidden = false
                self.containerAdd.isHidden = false
            })
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        present(alertController, animated: true, completion: nil)
    }
    
    func showRemoveWarning(car: Car) {
        let alertController = UIAlertController(title: "warning_remove_car".localized, message: nil, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "remove".localized, style: .default) { (action:UIAlertAction) in
            CarManager.shared().remove(car: car, listener: self)
        }
        
        let action2 = UIAlertAction(title: "forget_it".localized, style: .cancel) { (action:UIAlertAction) in
            Logger.d("You've pressed cancel");
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        present(alertController, animated: true, completion: nil)
    }
    
    
    
    // MARK: step 0 of adding car
    
    @IBAction func goAddCar1Clicked(_ sender: UIButton) {
        viewAdd0.isHidden = true
        viewAdd1.isHidden = false
        viewAdd2.isHidden = true
        viewAdd3.isHidden = true
    }
    
    @IBAction func addCar0NextClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        
        CarManager.shared().appendAddingCarInfo(name: self.add0valueOwnerName.textField.text, platePrefix: self.add0valuePlate0.textField.text, plateSuffix: self.add0valuePlate1.textField.text, vinLast5: self.add0valueVin.textField.text)
        
        let addingCar: AddingCar = CarManager.shared().getAddingCar()!
        if Utility.isEmpty(addingCar.name) || Utility.isEmpty(addingCar.platePrefix) || Utility.isEmpty(addingCar.plateSuffix) || Utility.isEmpty(addingCar.vinLast5) {
            let alertController = UIAlertController(title: nil, message: "car_data_lack".localized, preferredStyle: .alert)
            let action1 = UIAlertAction(title: "fine".localized, style: .default) { (action:UIAlertAction) in
                Logger.d("do nothing")
            }
            
            alertController.addAction(action1)
            present(alertController, animated: true, completion: nil)
        } else {
            CarManager.shared().checkExist(listener: self)
        }
    }
    
    private func initAdd0Ui(isFirstCar: Bool) {
        viewAdd0.isHidden = false
        viewAdd1.isHidden = true
        viewAdd2.isHidden = true
        viewAdd3.isHidden = true
        
        add0valueOwnerName.isHidden = !isFirstCar
        add0labelOwnerName.isHidden = isFirstCar
        add0warning.isHidden = true
        
        let addingCar: AddingCar = CarManager.shared().getAddingCar()!
        if isFirstCar {
            add0valueOwnerName.textField.text = addingCar.name
        } else {
            add0labelOwnerName.text = addingCar.name
        }
        add0valuePlate0.textField.text = addingCar.platePrefix
        add0valuePlate1.textField.text = addingCar.plateSuffix
        add0valueVin.textField.text = addingCar.vinLast5
    }
    
    
    
    // MARK: step 1 of adding car
    
    @IBAction func backAddCar0Clicked(_ sender: UIButton) {
        viewAdd0.isHidden = false
        viewAdd1.isHidden = true
        viewAdd2.isHidden = true
        viewAdd3.isHidden = true
    }
    
    // MARK: step 2 of adding car
    
    private func updateAdd2Ui() {
        let addingCar = CarManager.shared().getAddingCar()!
        add2valueModel.text = addingCar.typeName
        
        add2ButtonType.setTitle(addingCar.models![addingCar.selectedModelIndex].name, for: .normal)
        pickerView.reloadComponent(0)
    }
    
    @IBAction func selectCarTypeClicked(_ sender: UIButton) {
        viewPickerContainer.isHidden = false
    }
    
    @IBAction func selectedCarTypeClicked(_ sender: UIButton) {
        viewPickerContainer.isHidden = true
        
        let index: Int = pickerView.selectedRow(inComponent: 0)
        //        Logger.d("index: \(index)")
        CarManager.shared().appendAddingCarModel(index: index)
        updateAdd2Ui()
    }
    
    @IBAction func confirmAddCar2Clicked(_ sender: UIButton) {
        CarManager.shared().addCar(listener: self)
    }
    
    // MARK: step 3 of adding car
    
    @IBAction func doneAddCarClicked(_ sender: UIButton) {
        self.setAddVisibility(false, animated: true, isInit: false)
    }
}
