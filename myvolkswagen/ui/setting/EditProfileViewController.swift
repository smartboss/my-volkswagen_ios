//
//  EditProfileViewController.swift
//  myvolkswagen
//
//  Created by Apple on 1/31/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Kingfisher

enum EditProfileType: Int {
    case login = 0, profile
}

class EditProfileViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate, DealerMaintanenceInfoDismissDelegate {
    
    enum EditImageType: Int {
        case background = 0, photo
    }
    
    enum Row: Int {
        case header = 0, nickname, intro, privacy, birthday, gender, email, mobile, dealerheader, dealerarea, dealerplant, done
        
        static var allCases: [Row] {
            var values: [Row] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    var profile: Profile?
    var editingProfile: Profile?
    
    var editingBackground: UIImage?
    var editingPhoto: UIImage?
    
    var editImageType: EditImageType = .background
    
    var type: EditProfileType = .login
    @IBOutlet weak var tableView: UITableView!
    
    var inputNickname: PInput!
    var inputIntro: PInput!
    var inputEmail: PInput!
    var inputMobile: PInput!
    
    var isEmailValid: Bool = true
    var isMobileValid: Bool = true
    
    @IBOutlet weak var viewBirthdayPickerContainer: UIView!
    @IBOutlet weak var buttonBirthdayPickerDone: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var viewGenderPickerContainer: UIView!
    @IBOutlet weak var buttonGenderPickerDone: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var viewAreaPickerContainer: UIView!
    @IBOutlet weak var areaPickerView: UIPickerView!
    @IBOutlet weak var buttonAreaPickerDone: UIButton!
    @IBOutlet weak var viewPlantPickerContainer: UIView!
    @IBOutlet weak var plantPickerView: UIPickerView!
    @IBOutlet weak var buttonPlantPickerDone: UIButton!
    let genders: [Gender] = [.male, .female, .notSpecified]
    var maintenancePlant: [MaintainancePlant]?

    override func viewDidLoad() {
        self.hasTextField = true
        super.viewDidLoad()
        
        view.backgroundColor = UiUtility.colorBackgroundWhite
        tableView.backgroundColor = UiUtility.colorBackgroundWhite

        switch type {
        case .login:
            Logger.d("do nothing")
        case .profile:
            naviBar.setTitle("profile".localized)
            naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
            naviBar.setDoneVisibility(true)
        }
        
        datePicker.backgroundColor = UiUtility.colorBackgroundWhite
        buttonBirthdayPickerDone.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        buttonBirthdayPickerDone.setTitle("done".localized, for: .normal)
        buttonGenderPickerDone.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        buttonGenderPickerDone.setTitle("done".localized, for: .normal)
        buttonAreaPickerDone.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        buttonAreaPickerDone.setTitle("done".localized, for: .normal)
        buttonPlantPickerDone.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        buttonPlantPickerDone.setTitle("done".localized, for: .normal)
        
        profile = UserManager.shared().getProfile(withId: UserManager.shared().currentUser.accountId!, listener: self)
        if profile == nil {
            profile = Profile.init()
        }
        editingProfile = Profile.init(with: profile!)
        
        maintenancePlant = CarManager.shared().getMaintenancePlant(listener: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if offsetY == -1 {
            offsetY = self.tableView.frame.origin.y
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        self.view.endEditing(true)

        if let p = profile, let editingP = editingProfile {
            if editingP.isEdited(from: p) {
                let alertController = UIAlertController(title: nil, message: "warning_not_finish".localized, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "leave".localized, style: .default) { (action:UIAlertAction) in
                    self.back()
                }

                let action2 = UIAlertAction(title: "continue_edit".localized, style: .cancel) { (action:UIAlertAction) in
                    // do nothing
                }

                alertController.addAction(action1)
                alertController.addAction(action2)
                present(alertController, animated: true, completion: nil)
                
                return
            }
        }
        
        back()
    }
    
    override func doneButtonClicked(_ button: UIButton) {
        doneClicked(button)
    }
    
    
    
    // MARK: picker view delegate
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.pickerView {
            return genders.count
        } else if pickerView == areaPickerView {
            guard let mp = maintenancePlant else { return 0 }
            if let profile = self.profile {
                if profile.dealerAreaId == "" {
                    // user沒選過,增加請選擇選項
                    return mp.count + 1
                }
                
                return mp.count
            }
            return 0
        } else {
            if let eprofile = self.editingProfile {
                if eprofile.dealerAreaId == "" {
                    return 0
                }
                
                guard let mp = maintenancePlant else { return 0 }
                if let profile = self.profile {
                    if profile.dealerAreaId == "" {
                        // user沒選過,增加請選擇選項
                        for (index, mpArea) in mp.enumerated() {
                            if String(mpArea.areaId!) == eprofile.dealerAreaId {
                                return mp[index].plants!.count + 1
                            }
                        }
                    }
                    
                    for (index, mpArea) in mp.enumerated() {
                        if String(mpArea.areaId!) == eprofile.dealerAreaId {
                            return mp[index].plants!.count
                        }
                    }
                }
            }
            return 0
        }
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.pickerView {
            return genders[row].displayText()
        } else if pickerView == areaPickerView {
            if let profile = self.profile {
                if profile.dealerAreaId == "" {
                    // user沒選過,增加請選擇選項
                    if row == 0 {
                        return "請選擇"
                    } else {
                        return maintenancePlant![row - 1].areaName
                    }
                }
            }
            return maintenancePlant![row].areaName
        } else {
            if let eprofile = self.editingProfile {
                if eprofile.dealerAreaId == "" {
                    return ""
                }
                
                guard let mp = maintenancePlant else { return "" }
                if let profile = self.profile {
                    if profile.dealerAreaId == "" {
                        // user沒選過,增加請選擇選項
                        if row == 0 {
                            return "請選擇"
                        } else {
                            for (index, mpArea) in mp.enumerated() {
                                if String(mpArea.areaId!) == eprofile.dealerAreaId {
                                    return mp[index].plants![row - 1].plantName
                                }
                            }
                        }
                    }
                    
                    for (index, mpArea) in mp.enumerated() {
                        if String(mpArea.areaId!) == eprofile.dealerAreaId {
                            return mp[index].plants![row].plantName
                        }
                    }
                }
            }
            return ""
        }
    }
    
    
    
    // MARK: image picker controller delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.editedImage] as? UIImage else {
            Logger.d("No image found")
            return
        }
        
        // print out the image size as a test
        Logger.d("image size: \(image.size)")
        
        let imageData: Data = image.pngData()!
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
//        Logger.d("strBase64: \(strBase64)")
        
        switch editImageType {
        case .background:
            editingBackground = image
            editingProfile?.coverStream = "data:image/png;base64,\(strBase64)"
        case .photo:
            editingPhoto = image
//            editingProfile?.stickerStream = strBase64
            editingProfile?.stickerStream = "data:image/png;base64,\(strBase64)"
        }
        
        tableView.reloadData()
    }
    
    
    
    // MARK: profile protocol
    
    override func didStartGetProfile() {
        showHud()
    }
    
    override func didFinishGetProfile(success: Bool, profile: Profile?) {
        dismissHud()
        
        if success {
            if let p = profile {
                self.profile = p
                editingProfile = Profile.init(with: p)
                tableView.reloadData()
                self.areaPickerView.reloadAllComponents()
                self.plantPickerView.reloadAllComponents()
            }
        }
        
        if type == .login {
        }
    }
    
//    struct MyProfileRequest: GraphRequestProtocol {
//        struct Response: GraphResponseProtocol {
//
//            var name: String?
//            var id: String?
//            var gender: String?
//            var email: String?
//            var profilePictureUrl: String?
//
//            init(rawResponse: Any?) {
//                // Decode JSON from rawResponse into other properties here.
//                guard let response = rawResponse as? Dictionary<String, Any> else {
//                    return
//                }
//
//                if let name = response["name"] as? String {
//                    self.name = name
//                }
//
//                if let id = response["id"] as? String {
//                    self.id = id
//                }
//
//                if let gender = response["gender"] as? String {
//                    self.gender = gender
//                }
//
//                if let email = response["email"] as? String {
//                    self.email = email
//                }
//
//                if let picture = response["picture"] as? Dictionary<String, Any> {
//
//                    if let data = picture["data"] as? Dictionary<String, Any> {
//                        if let url = data["url"] as? String {
//                            self.profilePictureUrl = url
//                        }
//                    }
//                }
//            }
//        }
//
//        var graphPath = "/me"
//        var parameters: [String : Any]? = ["fields": "id, name, email, picture"]
//        var accessToken = AccessToken.current
//        var httpMethod: GraphRequestHTTPMethod = .GET
//        var apiVersion: GraphAPIVersion = .defaultVersion
//    }
    
    override func didStartUpdateProfile() {
        showHud()
    }
    
    override func didFinishUpdateProfile(success: Bool, profile: Profile?, msg: String?) {
        dismissHud()
        
        if success {
            switch type {
            case .login:
                UserManager.shared().currentUser.showProfile = false
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.updateRootVC(showAlert: false)
            case .profile:
                self.navigationController?.popViewController(animated: true)
            }
        } else {
            showGeneralWarning(title: nil, message: msg)
        }
    }
    
    
    
    // MARK: text field delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        editingTextField = textField
        
        if textField == inputNickname.textField {
            if textField.text?.count == 0 {
                inputNickname.buttonClear.isHidden = true
            } else {
                inputNickname.buttonClear.isHidden = false
            }
        } else if textField == inputIntro.textField {
            if textField.text?.count == 0 {
                inputIntro.buttonClear.isHidden = true
            } else {
                inputIntro.buttonClear.isHidden = false
            }
        } else if textField == inputEmail.textField {
            if textField.text?.count == 0 {
                inputEmail.buttonClear.isHidden = true
            } else {
                inputEmail.buttonClear.isHidden = false
            }
        } else if textField == inputMobile.textField {
            if textField.text?.count == 0 {
                inputMobile.buttonClear.isHidden = true
            } else {
                inputMobile.buttonClear.isHidden = false
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let fieldPosition: CGPoint = textField.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: fieldPosition) {
//            Logger.d("text field at row \(String(describing: indexPath.row))")
            
            switch Row.init(rawValue: indexPath.row)! {
            case .nickname:
                Logger.d("update nickname value: \(String(describing: textField.text))")
                editingProfile?.nickname = textField.text
            case .intro:
                Logger.d("update brief value: \(String(describing: textField.text))")
                editingProfile?.brief = textField.text
            case .email:
                Logger.d("update email value: \(String(describing: textField.text))")
                editingProfile?.email = textField.text
            case .mobile:
                Logger.d("update cellphone value: \(String(describing: textField.text))")
                editingProfile?.cellphone = textField.text
            default:
                Logger.d("do nothing")
            }
        }
        Logger.d("editing profile: \(String(describing: editingProfile))")
        
        if textField == inputNickname.textField {
            inputNickname.buttonClear.isHidden = true
        } else if textField == inputIntro.textField {
            inputIntro.buttonClear.isHidden = true
        } else if textField == inputEmail.textField {
            inputEmail.buttonClear.isHidden = true
        } else if textField == inputMobile.textField {
            inputMobile.buttonClear.isHidden = true
        }
    }
    
    // MARK: car protocal
    override func didStartGetCarMaintenancePlant() {
        //showHud()
    }
    
    override func didFinishGetCarMaintenancePlant(success: Bool, maintenancePlants: [MaintainancePlant]?) {
       // dismissHud()
        self.maintenancePlant = maintenancePlants
        self.areaPickerView.reloadAllComponents()
        self.plantPickerView.reloadAllComponents()
    }
    
    // MARK: keyboard
    
    @objc override func keyboardWillShow(notification: NSNotification) {
        //        let keyboardSize = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if let editingTextField = editingTextField {
//                let p = self.view.convert(editingTextField.center, to: self.view)
                let p = editingTextField.convert(editingTextField.center, to: self.view)
//                Logger.d("p.y: \(p.y)")
//                Logger.d("screen height: \(UiUtility.getScreenHeight())")
//                Logger.d("view.bounds.height: \(view.bounds.height)")
                
                if editingTextField == inputNickname.textField {
                    // customize an offset for nickname field
                    if self.tableView.frame.origin.y == offsetY {
//                        Logger.d("keyboardWillShow xx, \(d)")
                        self.tableView.frame.origin.y -= 30
                    }
                } else {
                    let d = updateFromKeyboardChangeToFrame(keyboardSize)
                    if view.bounds.height - p.y - 20 < d {
                        if self.tableView.frame.origin.y == offsetY {
//                            Logger.d("keyboardWillShow xx, \(d)")
                            self.tableView.frame.origin.y -= d
                        }
                    }
                }
            }
        }
        
//        super.keyboardWillShow(notification: notification)
        keyboardShowing = true
    }
    
    @objc override func keyboardWillHide(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
//            Logger.d("keyboardWillHide xx, \(keyboardSize.height)")
//        }
        
        if self.tableView.frame.origin.y != offsetY {
            self.tableView.frame.origin.y = offsetY
        }
        
//        super.keyboardWillHide(notification: notification)
        keyboardShowing = false
    }
    
    
    
    
    // MARK: table view delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Row.allCases.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Row.init(rawValue: indexPath.row)! {
        case .header:
            return UiUtility.adaptiveSize(size: 261)
        case .nickname, .intro, .birthday, .gender, .email, .mobile, .dealerarea, .dealerplant:
            return UiUtility.adaptiveSize(size: 59)
        case .privacy, .dealerheader:
            return UiUtility.adaptiveSize(size: 81)
        case .done:
            switch type {
            case .login:
                return UiUtility.adaptiveSize(size: 143)
            case .profile:
                return 0
            }
            
        }
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        switch Row.init(rawValue: indexPath.row)! {
        case .header:
            cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath)
            cell.selectionStyle = .none
            
            let imageBackground: UIImageView = cell.viewWithTag(1) as! UIImageView
            let imagePhoto: UIImageView = cell.viewWithTag(2) as! UIImageView
            
            imagePhoto.clipsToBounds = true
            imagePhoto.layer.borderWidth = UiUtility.adaptiveSize(size: 2)
            imagePhoto.layer.borderColor = UIColor.white.cgColor
            imagePhoto.layer.cornerRadius = UiUtility.adaptiveSize(size: 47)
            imagePhoto.backgroundColor = UiUtility.colorBackgroundWhite
            
            let buttonEditBackground: UIButton = cell.viewWithTag(3) as! UIButton
            let buttonEditPhoto: UIButton = cell.viewWithTag(4) as! UIButton
            buttonEditBackground.addTarget(self, action: #selector(editBackgroundClicked(_:)), for: .touchUpInside)
            buttonEditPhoto.addTarget(self, action: #selector(editPhotoClicked(_:)), for: .touchUpInside)
            
            let labelId: UILabel = cell.viewWithTag(5) as! UILabel
            let labelLevel: UILabel = cell.viewWithTag(6) as! UILabel
            labelId.isHidden = true
            labelLevel.isHidden = true
            
            if let profile = self.editingProfile {
                if let editingBackground = self.editingBackground {
                    imageBackground.image = editingBackground
                } else {
                    if let coverURL = profile.coverURL {
                        let url = URL(string: coverURL)
                        imageBackground.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_background"))
                    }
                }
                
                if let editingPhoto = self.editingPhoto {
                    imagePhoto.image = editingPhoto
                } else {
                    if let stickerURL = profile.stickerURL {
                        let url = URL(string: stickerURL)
                        imagePhoto.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_photo"))
                    }
                }
                
                if type == .profile {
                    labelId.textColor = UiUtility.colorDarkGrey
                    labelId.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
                    labelId.text = "\("id".localized) \(UserManager.shared().currentUser.accountId!)"
                    
                    labelLevel.textColor = UiUtility.colorDarkGrey
                    labelLevel.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
                    labelLevel.text = "\("member_level".localized) \(UserManager.shared().currentUser.levelName)"
                    
                    labelId.isHidden = false
                    labelLevel.isHidden = false
                }
            }
            
        case .nickname:
            cell = tableView.dequeueReusableCell(withIdentifier: "InputCell", for: indexPath)
            cell.selectionStyle = .none
            
            inputNickname = cell.viewWithTag(1) as? PInput
            inputNickname.setupWith(hint: "hint_nickname".localized)
            inputNickname.textField.inputAccessoryView = toolbar
            inputNickname.textField.delegate = self
            inputNickname.textField.keyboardType = .default
            inputNickname.textField.addTarget(self, action:#selector(textFieldDidChange(_:)), for: .editingChanged)
            inputNickname.textField.isUserInteractionEnabled = true
            inputNickname.dropdownPic.isHidden = true
            
            if let profile = self.editingProfile {
                inputNickname.textField.text = profile.nickname
                
                if let nickname = profile.nickname {
                    if nickname.count > 20 {
                        inputNickname.warnWith("warning_man_20".localized)
                    }
                }
            }
        case .intro:
            cell = tableView.dequeueReusableCell(withIdentifier: "InputCell", for: indexPath)
            cell.selectionStyle = .none
            
            inputIntro = cell.viewWithTag(1) as? PInput
            inputIntro.setupWith(hint: "hint_intro".localized)
            inputIntro.textField.inputAccessoryView = toolbar
            inputIntro.textField.delegate = self
            inputIntro.textField.keyboardType = .default
            inputIntro.textField.addTarget(self, action:#selector(textFieldDidChange(_:)), for: .editingChanged)
            inputIntro.textField.isUserInteractionEnabled = true
            inputIntro.dropdownPic.isHidden = true
            
            if let profile = self.editingProfile {
                inputIntro.textField.text = profile.brief
                
                if let intro = profile.brief {
                    if intro.count > 30 {
                        inputIntro.warnWith("warning_man_30".localized)
                    }
                }
            }
        case .privacy:
            cell = tableView.dequeueReusableCell(withIdentifier: "PrivateCell", for: indexPath)
            cell.selectionStyle = .none
            
            let title: UILabel = cell.viewWithTag(1) as! UILabel
            let subtitle: UILabel = cell.viewWithTag(2) as! UILabel
            let questionImg: UIImageView = cell.viewWithTag(3) as! UIImageView
            
            title.textColor = UiUtility.colorBrownGrey
            title.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
            title.text = "private_info".localized
            subtitle.textColor = UiUtility.colorBrownGrey
            subtitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
            subtitle.text = "seen_by_you_only".localized
            questionImg.isHidden = true
        case .birthday:
            cell = tableView.dequeueReusableCell(withIdentifier: "PickerCell", for: indexPath)
            cell.selectionStyle = .none
            
            let pInput: PInput = cell.viewWithTag(1) as! PInput
            pInput.setupWith(hint: "1990/12/02")
            pInput.dropdownPic.isHidden = true
            
            let button: UIButton = cell.viewWithTag(2) as! UIButton
            button.addTarget(self, action:#selector(selectBirthdayClicked(_:)), for: .touchUpInside)
            
            if let profile = self.editingProfile {
                pInput.textField.text = profile.getBirthdayText()
            }
        case .gender:
            cell = tableView.dequeueReusableCell(withIdentifier: "PickerCell", for: indexPath)
            cell.selectionStyle = .none
            
            let pInput: PInput = cell.viewWithTag(1) as! PInput
            pInput.setupWith(hint: "not_specified".localized)
            pInput.dropdownPic.isHidden = true
            
            let button: UIButton = cell.viewWithTag(2) as! UIButton
            button.addTarget(self, action:#selector(selectGenderClicked(_:)), for: .touchUpInside)
            
            if let profile = self.editingProfile {
                pInput.textField.text = profile.getGenderText()
            }
        case .email:
            cell = tableView.dequeueReusableCell(withIdentifier: "InputCell", for: indexPath)
            cell.selectionStyle = .none
            
            inputEmail = cell.viewWithTag(1) as? PInput
            inputEmail.setupWith(hint: "abc123@gmail.com")
            inputEmail.textField.inputAccessoryView = toolbar
            inputEmail.textField.keyboardType = .emailAddress
            inputEmail.textField.delegate = self
            inputEmail.textField.addTarget(self, action:#selector(textFieldDidChange(_:)), for: .editingChanged)
            inputEmail.textField.isUserInteractionEnabled = false
            inputEmail.dropdownPic.isHidden = true
            
            if let profile = self.editingProfile {
                inputEmail.textField.text = profile.email
                if profile.OTPFlag == 1 {
                    inputEmail.textField.isUserInteractionEnabled = true
                }
            }
            
            if isEmailValid {
                inputEmail.unwarn()
            } else {
                inputEmail.warnWith("warning_format_email".localized)
            }
        case .mobile:
            cell = tableView.dequeueReusableCell(withIdentifier: "InputCell", for: indexPath)
            cell.selectionStyle = .none
            
            inputMobile = cell.viewWithTag(1) as? PInput
            inputMobile.setupWith(hint: "0912-123-123")
            inputMobile.textField.inputAccessoryView = toolbar
            inputMobile.textField.keyboardType = .phonePad
            inputMobile.textField.delegate = self
            inputMobile.textField.addTarget(self, action:#selector(textFieldDidChange(_:)), for: .editingChanged)
            inputMobile.textField.isUserInteractionEnabled = false
            inputMobile.dropdownPic.isHidden = true
            
            if let profile = self.editingProfile {
                inputMobile.textField.text = profile.cellphone
                if profile.OTPFlag == 2 {
                    inputMobile.textField.isUserInteractionEnabled = true
                }
            }
            
            if isMobileValid {
                inputMobile.unwarn()
            } else {
                inputMobile.warnWith("warning_format_mobile".localized)
            }
        case .dealerheader:
            cell = tableView.dequeueReusableCell(withIdentifier: "PrivateCell", for: indexPath)
            cell.selectionStyle = .none
            
            let title: UILabel = cell.viewWithTag(1) as! UILabel
            let subtitle: UILabel = cell.viewWithTag(2) as! UILabel
            let questionImg: UIImageView = cell.viewWithTag(3) as! UIImageView
            
            title.textColor = UiUtility.colorBrownGrey
            title.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
            title.text = "my_dealer".localized
            subtitle.textColor = UiUtility.colorBrownGrey
            subtitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
            subtitle.text = ""
            questionImg.isHidden = false
        case .dealerarea:
            cell = tableView.dequeueReusableCell(withIdentifier: "PickerCell", for: indexPath)
            cell.selectionStyle = .none
            
            let pInput: PInput = cell.viewWithTag(1) as! PInput
            pInput.setupWith(hint: "請選擇")
            pInput.dropdownPic.isHidden = false
            
            let button: UIButton = cell.viewWithTag(2) as! UIButton
            button.addTarget(self, action:#selector(selectAreaClicked(_:)), for: .touchUpInside)
            
            if let profile = self.editingProfile {
                pInput.textField.text = profile.dealerAreaName
            }
        case .dealerplant:
            cell = tableView.dequeueReusableCell(withIdentifier: "PickerCell", for: indexPath)
            cell.selectionStyle = .none
            
            let pInput: PInput = cell.viewWithTag(1) as! PInput
            pInput.setupWith(hint: "請選擇")
            pInput.dropdownPic.isHidden = false
            
            let button: UIButton = cell.viewWithTag(2) as! UIButton
            button.addTarget(self, action:#selector(selectPlantClicked(_:)), for: .touchUpInside)
            
            if let profile = self.editingProfile {
                pInput.textField.text = profile.dealerPlantName
            }
        case .done:
            cell = tableView.dequeueReusableCell(withIdentifier: "DoneCell", for: indexPath)
            cell.selectionStyle = .none
            
            let buttonDone: UIButton = cell.viewWithTag(1) as! UIButton
            buttonDone.addTarget(self, action:#selector(doneClicked(_:)), for: .touchUpInside)
        }
        
        cell.backgroundColor = UiUtility.colorBackgroundWhite
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch Row.init(rawValue: indexPath.row)! {
        case .dealerheader:
            let sb = UIStoryboard(name: "Login", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "DealerMaintanenceInfoViewController") as! DealerMaintanenceInfoViewController
            vc.delegate = self
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true) {
                let v = UIView()
                v.translatesAutoresizingMaskIntoConstraints = false
                v.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                v.tag = 399
                self.view.addSubview(v)
                v.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
                v.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
                v.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
                v.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
                v.alpha = 0
                UIView.animate(withDuration: 0.3, animations: {
                    v.alpha = 1
                }) { (success) in
                }
            }
        default:
            return
        }
    }
    
    func didDismiss() {
        self.view.viewWithTag(399)?.removeFromSuperview()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func checkField() -> Bool {
        if let nickname = editingProfile?.nickname {
            isEmailValid = true
            if let email = editingProfile?.email {
                if !Utility.isEmpty(email) && !Utility.isValidEmail(testStr: email) {
                    isEmailValid = false
                }
            }
            
            isMobileValid = true
            if let mobile = editingProfile?.cellphone {
                if mobile == "" {
                    isMobileValid = false
                }
            }
            
            if nickname.count > 20 {
                return false
            }
            
            if let brief = editingProfile?.brief {
                if brief.count > 30 {
                    return false
                }
            }
            
            return isEmailValid && isMobileValid
        } else {
            showGeneralWarning(title: "請輸入您的暱稱", message: "")
            return false
        }
    }

    
    @IBAction private func doneClicked(_ sender: Any) {
        self.view.endEditing(true)
        
        // check fields
        if checkField() {
            // update profile
            UserManager.shared().updateProfile(profile: editingProfile!, listener: self)
        } else {
            tableView.reloadData()
        }
    }
    
    @objc private func textFieldDidChange(_ sender: UITextField) {
        if sender == inputNickname.textField {
            editingProfile?.nickname = inputNickname.textField.text
            if let text = sender.text {
                if text.count >= 21 {
                    inputNickname.warnWith("warning_man_20".localized)
                } else/* if text.count <= 20*/ {
                    inputNickname.unwarn()
                }
            } else {
                inputNickname.unwarn()
            }
            
            if sender.text?.count == 0 {
                inputNickname.buttonClear.isHidden = true
            } else {
                inputNickname.buttonClear.isHidden = false
            }
        } else if sender == inputIntro.textField {
            editingProfile?.brief = inputIntro.textField.text
            if let text = sender.text {
                if text.count >= 31 {
                    inputIntro.warnWith("warning_man_30".localized)
                } else/* if text.count <= 30*/ {
                    inputIntro.unwarn()
                }
            } else {
                inputIntro.unwarn()
            }
            
            if sender.text?.count == 0 {
                inputIntro.buttonClear.isHidden = true
            } else {
                inputIntro.buttonClear.isHidden = false
            }
        } else if sender == inputEmail.textField {
            editingProfile?.email = inputEmail.textField.text
            if sender.text?.count == 0 {
                inputEmail.buttonClear.isHidden = true
            } else {
                inputEmail.buttonClear.isHidden = false
            }
        } else if sender == inputMobile.textField {
            editingProfile?.cellphone = inputMobile.textField.text
            if sender.text?.count == 0 {
                inputMobile.buttonClear.isHidden = true
            } else {
                inputMobile.buttonClear.isHidden = false
            }
        }
    }
    
    @objc private func selectBirthdayClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if let profile = editingProfile, let date = profile.getBirthDate() {
            datePicker.date = date
        }
        
        viewGenderPickerContainer.isHidden = true
        viewBirthdayPickerContainer.isHidden = false
        datePicker.isHidden = false
        viewPlantPickerContainer.isHidden = true
        viewAreaPickerContainer.isHidden = true
    }
    
    @IBAction func selectedBirthdayClicked(_ sender: UIButton) {
        viewBirthdayPickerContainer.isHidden = true
        
        Logger.d("datePicker.date: \(datePicker.date.description(with: .current))")
        editingProfile?.setBirthday(to: datePicker.date)
        tableView.reloadData()
    }
    
    @objc private func selectGenderClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        
        var targetGender: Gender = .notSpecified
        if let profile = editingProfile, let sex = profile.sex {
            if let gender: Gender = Gender(rawValue: sex) {
                targetGender = gender
            }
        }
        
        var index: Int = 0
        for g in genders {
            if g == targetGender {
                break
            }
            index += 1
        }
        
        pickerView.selectRow(index, inComponent: 0, animated: true)
        
        viewBirthdayPickerContainer.isHidden = true
        viewGenderPickerContainer.isHidden = false
        viewPlantPickerContainer.isHidden = true
        viewAreaPickerContainer.isHidden = true
    }
    
    @IBAction func selectedGenderClicked(_ sender: UIButton) {
        viewGenderPickerContainer.isHidden = true
        
        editingProfile?.setGender(to: genders[pickerView.selectedRow(inComponent: 0)])
        tableView.reloadData()
    }
    
    @objc private func selectAreaClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        
        viewBirthdayPickerContainer.isHidden = true
        viewGenderPickerContainer.isHidden = true
        viewAreaPickerContainer.isHidden = false
        viewPlantPickerContainer.isHidden = true
    }
    
    @IBAction func selectedAreaClicked(_ sender: UIButton) {
        viewAreaPickerContainer.isHidden = true
        
        let index = areaPickerView.selectedRow(inComponent: 0)
        
        if let profile = self.profile {
            if profile.dealerAreaId == "" {
                // user沒選過,index0為請選擇選項
                if index == 0 {
                    // 請選擇
                    editingProfile?.dealerAreaId = nil
                    editingProfile?.dealerAreaName = nil
                } else {
                    editingProfile?.dealerAreaId = String(maintenancePlant![index - 1].areaId ?? 0)
                    editingProfile?.dealerAreaName = maintenancePlant![index - 1].areaName
                }
                editingProfile?.dealerPlantId = nil
                editingProfile?.dealerPlantName = nil
                tableView.reloadData()
                plantPickerView.selectRow(0, inComponent: 0, animated: false)
                plantPickerView.reloadAllComponents()
                return
            }
        }
        
        editingProfile?.dealerAreaId = String(maintenancePlant![index].areaId ?? 0)
        editingProfile?.dealerAreaName = maintenancePlant![index].areaName
        editingProfile?.dealerPlantId = nil
        editingProfile?.dealerPlantName = nil
        tableView.reloadData()
        plantPickerView.selectRow(0, inComponent: 0, animated: false)
        plantPickerView.reloadAllComponents()
    }
    
    @objc private func selectPlantClicked(_ sender: UIButton) {
        self.view.endEditing(true)
    
        if let edip = editingProfile {
            if edip.dealerAreaId == nil {
                showApiFailureAlert(message: "請先選擇服務中心地區")
                return
            }
        }
        viewBirthdayPickerContainer.isHidden = true
        viewGenderPickerContainer.isHidden = true
        viewAreaPickerContainer.isHidden = true
        viewPlantPickerContainer.isHidden = false
    }
    
    @IBAction func selectedPlantClicked(_ sender: UIButton) {
        viewPlantPickerContainer.isHidden = true
        
        let aIndex = areaPickerView.selectedRow(inComponent: 0)
        let pIndex = plantPickerView.selectedRow(inComponent: 0)
        
        if let profile = self.profile {
            if profile.dealerAreaId == "" {
                // user沒選過,index0為請選擇選項
                if pIndex == 0 {
                    // 請選擇
                    editingProfile?.dealerPlantId = nil
                    editingProfile?.dealerPlantName = nil
                } else {
                    editingProfile?.dealerPlantId = String(maintenancePlant![aIndex - 1].plants![pIndex - 1].plantId ?? 0)
                    editingProfile?.dealerPlantName = maintenancePlant![aIndex - 1].plants![pIndex - 1].plantName
                }
                tableView.reloadData()
                return
            }
        }
        editingProfile?.dealerPlantId = String(maintenancePlant![aIndex].plants![pIndex].plantId ?? 0)
        editingProfile?.dealerPlantName = maintenancePlant![aIndex].plants![pIndex].plantName
        tableView.reloadData()
    }
    
    @objc private func editBackgroundClicked(_ sender: UIButton) {
        editImageType = .background
        UiUtility.pickImage(from: self, delegate: self, allowsEditing: true)
    }
    
    @objc private func editPhotoClicked(_ sender: UIButton) {
        editImageType = .photo
        UiUtility.pickImage(from: self, delegate: self, allowsEditing: true)
    }
}
