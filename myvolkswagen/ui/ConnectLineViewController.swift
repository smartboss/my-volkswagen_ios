//
//  ConnectLineViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/2/16.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

class ConnectLineViewController: BaseViewController {

    @IBOutlet weak var connect: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var later: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        connect.text = "connect_line_account".localized
        connect.textColor = UIColor.black
        connect.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 22))
        
        subtitle.text = "connect_line_subtitle".localized
        subtitle.textColor = UIColor(rgb: 0x4A4A4A)
        subtitle.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
        
        later.setTitle("connect_line_later".localized, for: .normal)
        later.setTitleColor(UIColor.black, for: .normal)
        later.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
    }
    
    override func didFinishBindLine(success: Bool, isBind: Bool) {
        if success {
            next()
        }
    }
    
    @IBAction private func connectClicked(_ sender: Any) {
    }
    
    @IBAction private func laterClicked(_ sender: Any) {
        next()
    }

    private func next() {
        if UserManager.shared().currentUser.showProfile ?? false {
            // show profile editor
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            vc.type = .login
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.updateRootVC(showAlert: false)
        }
    }
}
