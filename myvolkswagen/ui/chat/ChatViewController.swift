//
//  ChatViewController.swift
//  myvolkswagen
//
//  Created by CHUNG CHING JIANG on 2019/8/28.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class ChatViewController: NaviViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ChatProtocol {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var buttonLeaveMessage: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    private var cellHeightsDictionary: [IndexPath: CGFloat]!
    
    @IBOutlet weak var collectionViewKeyword: UICollectionView!
    //    private var greeting: String?
    private var menuButtons: [ChatButton]?
    private var chatGrade: ChatGrade?
    private var grade: Int = 0
    private var greetingFetched: Bool = false
    private var menuButtonsFetched: Bool = false
    private var chatGradeFetched: Bool = false
    
    private var chatMessages: [ChatMessage]?
    private var chatHistory: [ChatHistory]?
    private var chatHistoryMsg: [ChatMessage]?
    private var beforeTableViewContentHeight: CGFloat = 0
    private var beforeTableViewOffset: CGFloat = 0
    
    private var timer: Timer?
    
    
    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()
        
        view.backgroundColor = UiUtility.colorBackgroundWhite
        naviBar.setTitle("chat_title".localized)
        naviBar.setLeftButton(UIImage(named: "vw"), highlighted: UIImage(named: "vw"))
        naviBar.setRightButton(UIImage(named: "notif_black"), highlighted: UIImage(named: "notif_black_unread"))
        imageViewNotification = naviBar.imageViewRight
        
        tableView.backgroundColor = UiUtility.colorBackgroundWhite
        cellHeightsDictionary = [:]
        
        collectionViewKeyword.backgroundColor = UiUtility.colorBackgroundWhite
        collectionViewKeyword.delaysContentTouches = false
        collectionViewKeyword.delegate = self
        
        textField.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 16))
        textField.placeholder = "hint_input".localized
        textField.layer.cornerRadius = UiUtility.adaptiveSize(size: 15)
        textField.layer.borderWidth = UiUtility.adaptiveSize(size: 1)
        textField.layer.borderColor = UiUtility.colorBrownGrey.cgColor
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 30))
        textField.leftView = paddingView
        textField.leftViewMode = .always
        textField.inputAccessoryView = toolbar
        textField.delegate = self
        
        buttonLeaveMessage.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
        buttonLeaveMessage.setTitleColor(.black, for: .normal)
        buttonLeaveMessage.setTitle("leave_message".localized, for: .normal)
        
        chatMessages = []
        
        addFloatingBtnView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        // update notice read/unread status locally
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            self.setNoticeImage(with: readStatus)
        }
        
        if chatMessages?.count == 0 {
            fetchInitData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if offsetY == -1 {
            offsetY = self.view.frame.origin.y
        }
        
        if let timer = timer, timer.isValid {
            timer.invalidate()
        }
        timer = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        self.tabControllerDelegate?.menuClicked()
    }
    
    override func rightButtonClicked(_ button: UIButton) {
        self.tabControllerDelegate?.notificationClicked()
    }
    
    override func updateUnreadCount(notification: NSNotification) {
        //        Logger.d("update notice read/unread status locally")
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            self.setNoticeImage(with: readStatus)
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    // MARK: chat protocol
    
    func didStartGetGreeting() {
        showHud()
    }
    
    func didFinishGetGreeting(success: Bool, greeting: String?) {
        dismissHud()
        
        if success {
            if let greeting = greeting {
                //                Logger.d("greeting: \(greeting)")
                self.generateTextMessage(with: greeting, isIncoming: true)
            }
            
            self.updateTimer(initial: true)
        }
        
        greetingFetched = true
        fetchInitData()
    }
    
    func didStartGetMenuButtons() {
        showHud()
    }
    
    func didFinishGetMenuButtons(success: Bool, content: String?, buttons: [ChatButton]?) {
        dismissHud()
        
        if success {
            menuButtons = buttons
            if menuButtons != nil {
                //                Logger.d("menuButtons: \(menuButtons!)")
                collectionViewKeyword.reloadData()
            }
        }
        
        menuButtonsFetched = true
        fetchInitData()
    }
    
    func didStartGetButtons() {
        //showHud()
    }
    
    func didFinishGetButtons(success: Bool, buttons: [ChatButton]?) {
        //dismissHud()
        
        if success {
            menuButtons = buttons
            if menuButtons != nil {
                Logger.d("menuButtons: \(menuButtons!)")
                collectionViewKeyword.reloadData()
            }
        }
    }
    
    func didStartGetGrading() {
        showHud()
    }
    
    func didFinishGetGrading(success: Bool, chatGrade: ChatGrade?) {
        dismissHud()
        
        if success {
            self.chatGrade = chatGrade
            if self.chatGrade != nil {
                //                Logger.d("chatGrade: \(chatGrade!)")
            }
        }
        
        chatGradeFetched = true
        fetchInitData()
    }
    
    func didStartGenerateAnswer() {
        showHud()
    }
    
    func didFinishGenerateAnswer(success: Bool, answers: [ChatAnswer]?, buttons: [ChatButton]?, exception: String?, question: String?) {
        dismissHud()
        
        if success {
            var answer: ChatAnswer?
            if let exception = exception {
                self.generateTextMessage(with: exception, isIncoming: true)
                if let answers = answers, answers.count != 0 {
                    answer = answers[0]
                    
                    if let buttons = buttons, buttons.count != 0 {
                        menuButtons = buttons
                        collectionViewKeyword.reloadData()
                    }
                    ChatManager.shared().writeTalkRecord(with: ChatRecord.init(with: question, answer: exception, score: nil), listener: self)
                }
            } else {
                if let answers = answers, answers.count != 0 {
                    answer = answers[0]
                    if let text = answer!.answer {
                        self.generateTextMessage(with: text, isIncoming: true)
                    }
                    
                    if let buttons = buttons, buttons.count != 0 {
                        menuButtons = buttons
                        collectionViewKeyword.reloadData()
                    }
                }
                if let answer = answer {
                    ChatManager.shared().writeTalkRecord(with: ChatRecord.init(with: question, answer: answer.answer, score: answer.score), listener: self)
                }
            }
        }
    }
    
    func didStartWriteTalkRecords() {
        showHud()
    }
    
    func didFinishWriteTalkRecords(success: Bool) {
        dismissHud()
    }
    
    func didStartGetTalkHistroy() {
        //showHud()
    }
    
    func didFinishGetTalkHistroy(success: Bool, history: [ChatHistory]?, exception: String?) {
        chatHistory = history
        if let cha = chatHistory {
            chatHistoryMsg = []
            for ch in cha {
                if ch.category == "button" {
                    if let type = ch.data!.type {
                        switch type {
                        case ChatButton.ButtonType.text.rawValue:
                            var messages: [ChatMessage] = []
                            messages.append(ChatMessage.init(with: .outgoing, text: ch.data!.buttonName))
                            if let content = ch.data!.content {
                                messages.append(ChatMessage.init(with: .incoming, text: content))
                            }
                            if messages.count != 0 {
                                self.chatHistoryMsg!.insert(contentsOf: messages, at: 0)
                            }
                        case ChatButton.ButtonType.url.rawValue:
                            //                Logger.d("url")
                            if let content = ch.data!.content {
                                var text = content
                                if !text.starts(with: "http://") && !text.starts(with: "https://") {
                                    text = "http://\(text)"
                                }
                                var messages: [ChatMessage] = []
                                messages.append(ChatMessage.init(with: .outgoing, text: ch.data!.buttonName))
                                messages.append(ChatMessage.init(with: .incoming, text: text))
                                self.chatHistoryMsg!.insert(contentsOf: messages, at: 0)
                            }
                            
                        case ChatButton.ButtonType.image.rawValue:
                            //                Logger.d("image")
                            var messages: [ChatMessage] = []
                            messages.append(ChatMessage.init(with: .outgoing, text: ch.data!.buttonName))
                            messages.append(ChatMessage.init(with: .incoming, text: nil, button: ch.data!))
                            self.chatHistoryMsg!.insert(contentsOf: messages, at: 0)
                        case ChatButton.ButtonType.deeplink.rawValue:
                            //                Logger.d("deeplink")
                            if let deepLink = ch.data!.content {
                                var messages: [ChatMessage] = []
                                messages.append(ChatMessage.init(with: .outgoing, text: ch.data!.buttonName))
                                messages.append(ChatMessage.init(with: .incoming, text: deepLink))
                                self.chatHistoryMsg!.insert(contentsOf: messages, at: 0)
                            }
                        case ChatButton.ButtonType.generic.rawValue:
                            //                Logger.d("generic")
                            var messages: [ChatMessage] = []
                            messages.append(ChatMessage.init(with: .outgoing, text: ch.data!.buttonName))
                            messages.append(ChatMessage.init(with: .incoming, text: nil, button: ch.data!))
                            self.chatHistoryMsg!.insert(contentsOf: messages, at: 0)
                        default:
                            //                Logger.d("default")
                            break
                        }
                    }
                } else {
                    if ch.answer != nil {
                        let chm = ChatMessage.init(with: .incoming, text: ch.answer)
                        self.chatHistoryMsg!.insert(chm, at: 0)
                    }
                    if ch.question != nil {
                        let chm = ChatMessage.init(with: .outgoing, text: ch.question)
                        self.chatHistoryMsg!.insert(chm, at: 0)
                    }
                }
            }
        }
        dismissHud()
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.tableView.layer.layoutIfNeeded()
            //            let insertCellHeight = self.beforeTableViewOffset + (self.tableView.contentSize.height - self.beforeTableViewContentHeight)
            //            let newOffSet = CGPoint(x: 0, y: insertCellHeight)
            //            self.tableView.contentOffset = newOffSet
            //            let indexPath = IndexPath(row: 0, section: 1)
            //            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
            
            if (self.tableView.contentSize.height > self.tableView.frame.size.height)
            {
                let offset = CGPoint(x: 0, y: self.tableView.contentSize.height - self.tableView.frame.size.height)
                self.tableView.setContentOffset(offset, animated: false)
            }
        }
    }
    
    
    
    // MARK: text field delegate
    
    //    func textFieldDidEndEditing(_ textField: UITextField) {
    //        editingContent = textField.text
    //    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.send(textField.text)
        return true
    }
    
    
    
    // MARK: keyboard
    
    @objc override func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let d = updateFromKeyboardChangeToFrame(keyboardSize)
            self.view.frame.origin.y -= d
        }
        
        keyboardShowing = true
    }
    
    @objc override func keyboardWillHide(notification: NSNotification) {
        
        if self.view.frame.origin.y != offsetY {
            self.view.frame.origin.y = offsetY
        }
        
        keyboardShowing = false
    }
    
    
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return chatHistoryMsg?.count ?? 0
        }
        //        Logger.d("c: \(chatMessages?.count ?? 0)")
        return chatMessages?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var chatMessage: ChatMessage!
        if indexPath.section == 0 {
            chatMessage = chatHistoryMsg![indexPath.row]
        } else {
            chatMessage = chatMessages![indexPath.row]
        }
        
        if chatMessage.direction == .grade {
            var height = UiUtility.adaptiveSize(size: 8) // title top
            
            let width = UiUtility.adaptiveSize(size: 250 - 20)
            let fontTitle = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
            if let title = chatGrade!.title {
                let maxTitleHeight: CGFloat = title.height(withConstrainedWidth: width, font: fontTitle)
                height += maxTitleHeight
            }
            
            if let subtitle = chatGrade!.content {
                height += UiUtility.adaptiveSize(size: 4) // title - subtitle spacing
                let fontSubtitle = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))!
                let maxSubtitleHeight: CGFloat = subtitle.height(withConstrainedWidth: width, font: fontSubtitle)
                height += maxSubtitleHeight
            }
            
            height += UiUtility.adaptiveSize(size: 70) // stars container
            height += UiUtility.adaptiveSize(size: 10 + 10) // padding top and bottom
            
            return height
        } else if chatMessage.direction == .keep {
            var height = UiUtility.adaptiveSize(size: 8) // title top
            
            let width = UiUtility.adaptiveSize(size: 250 - 20)
            let fontTitle = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))!
            let title = "chat_keep_chatbox".localized
            let maxTitleHeight: CGFloat = title.height(withConstrainedWidth: width, font: fontTitle)
            height += maxTitleHeight
            height += UiUtility.adaptiveSize(size: 20) // title bottom
            height += UiUtility.adaptiveSize(size: 78 + 16) // stars yes n no container
            height += UiUtility.adaptiveSize(size: 10 + 10) // padding top and bottom
            
            return height
        } else if chatMessage.direction == .incoming {
            if chatMessage.text != nil {
                let width = UiUtility.adaptiveSize(size: 250 - 20)
                let font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))!
                let maxContentHeight: CGFloat = chatMessage.text!.height(withConstrainedWidth: width, font: font)
                return maxContentHeight + UiUtility.adaptiveSize(size: 20 + 12)
            } else if let button = chatMessage.button {
                if let type = button.type {
                    switch type {
                    case ChatButton.ButtonType.text.rawValue:
                        return 0
                    case ChatButton.ButtonType.url.rawValue:
                        return 0
                    case ChatButton.ButtonType.image.rawValue:
                        return UiUtility.adaptiveSize(size: 150)
                    case ChatButton.ButtonType.deeplink.rawValue:
                        return 0
                    case ChatButton.ButtonType.generic.rawValue:
                        if let elementts = button.genericMenu?.elements {
                            if elementts.count > 0 {
                                let element = elementts[0]
                                var height = UiUtility.adaptiveSize(size: 130) // photo
                                height += UiUtility.adaptiveSize(size: 6) // title top
                                
                                let width = UiUtility.adaptiveSize(size: 250 - 20)
                                let fontTitle = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))!
                                let maxTitleHeight: CGFloat = element.title.height(withConstrainedWidth: width, font: fontTitle)
                                height += maxTitleHeight
                                
                                if let subtitle = element.subtitle {
                                    height += UiUtility.adaptiveSize(size: 6) // title - subtitle spacing
                                    let fontSubtitle = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))!
                                    let maxSubtitleHeight: CGFloat = subtitle.height(withConstrainedWidth: width, font: fontSubtitle)
                                    height += maxSubtitleHeight
                                }
                                
                                height += UiUtility.adaptiveSize(size: 9.5) // separator top
                                height += 1 // separator
                                
                                height += UiUtility.adaptiveSize(size: 10) // button0 top
                                height += UiUtility.adaptiveSize(size: 34) // button0
                                
                                if element.actions!.count > 1 {
                                    height += UiUtility.adaptiveSize(size: 10) // button1 top
                                    height += UiUtility.adaptiveSize(size: 34) // button1
                                }
                                
                                if element.actions!.count > 2 {
                                    height += UiUtility.adaptiveSize(size: 10) // button2 top
                                    height += UiUtility.adaptiveSize(size: 34) // button2
                                }
                                
                                height += UiUtility.adaptiveSize(size: 20) // buttons bottom
                                height += UiUtility.adaptiveSize(size: 10 + 10) // padding top and bottom
                                
                                return height
                            } else {
                                return 0
                            }
                        } else {
                            return 0
                        }
                        
                    default:
                        return 0
                    }
                } else {
                    return 0
                }
            } else {
                return 0
            }
        } else {
            let width = UiUtility.adaptiveSize(size: 255 - 20)
            let font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))!
            let maxContentHeight: CGFloat = chatMessage.text!.height(withConstrainedWidth: width, font: font)
            return maxContentHeight + UiUtility.adaptiveSize(size: 20 + 12)
        }
    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        //        cellHeightsDictionary[indexPath] = cell.frame.size.height
    //
    //        if indexPath.section == Section.feed.rawValue {
    //            if newsList.count != 0 {
    //                let lastElement = newsList.count - 1
    //                if indexPath.row == lastElement {
    //                    if !NewsManager.shared().isListEndReached() {
    //                        // handle your logic here to get more items, add it to dataSource and reload tableview
    //                        Logger.d("should try load more")
    //                        NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: true, listener: self)
    //                    }
    //                }
    //            }
    //        }
    //    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightsDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let h:CGFloat = cellHeightsDictionary[indexPath] {
            return h
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        var message: ChatMessage!
        if indexPath.section == 0 {
            message = chatHistoryMsg![indexPath.row]
        } else {
            message = chatMessages![indexPath.row]
        }
        
        if message.direction == .grade {
            cell = tableView.dequeueReusableCell(withIdentifier: "GradeCell", for: indexPath)
            
            for constraint in cell.contentView.constraints {
                if constraint.identifier == "padding_top" {
                    constraint.constant = UiUtility.adaptiveSize(size: 10)
                } else if constraint.identifier == "padding_bottom" {
                    constraint.constant = UiUtility.adaptiveSize(size: 10)
                }
            }
            
            let vContainer: UIView = cell.viewWithTag(1)!
            let lbTitle: ContextLabel = cell.viewWithTag(2) as! ContextLabel
            let lbSubtitle: ContextLabel = cell.viewWithTag(3) as! ContextLabel
            let btnStar0: UIButton = cell.viewWithTag(4) as! UIButton
            let btnStar1: UIButton = cell.viewWithTag(5) as! UIButton
            let btnStar2: UIButton = cell.viewWithTag(6) as! UIButton
            let btnStar3: UIButton = cell.viewWithTag(7) as! UIButton
            let btnStar4: UIButton = cell.viewWithTag(8) as! UIButton
            
            btnStar0.addTarget(self, action: #selector(starClicked(_:)), for: .touchUpInside)
            btnStar1.addTarget(self, action: #selector(starClicked(_:)), for: .touchUpInside)
            btnStar2.addTarget(self, action: #selector(starClicked(_:)), for: .touchUpInside)
            btnStar3.addTarget(self, action: #selector(starClicked(_:)), for: .touchUpInside)
            btnStar4.addTarget(self, action: #selector(starClicked(_:)), for: .touchUpInside)
            
            for constraint in vContainer.constraints {
                if constraint.identifier == "title_padding_left" {
                    constraint.constant = UiUtility.adaptiveSize(size: 10)
                } else if constraint.identifier == "title_padding_top" {
                    constraint.constant = UiUtility.adaptiveSize(size: 8)
                } else if constraint.identifier == "title_padding_right" {
                    constraint.constant = UiUtility.adaptiveSize(size: 10)
                } else if constraint.identifier == "subtitle_padding_top" {
                    if chatGrade!.content == nil {
                        constraint.constant = 0
                    } else {
                        constraint.constant = UiUtility.adaptiveSize(size: 4)
                    }
                }
            }
            
            vContainer.layer.cornerRadius = UiUtility.adaptiveSize(size: 10)
            vContainer.backgroundColor = UiUtility.colorBackgroundIncomingMessage
            
            lbTitle.textColor = .black
            lbTitle.font = UIFont(name: "PingFangTC-Medium", size: 16)
            lbTitle.foregroundColor = { (linkResult) in
                switch linkResult.detectionType {
                case .url:
                    return UIColor(rgb: 0x007AFF)
                default:
                    return .black
                }
            }
            
            lbTitle.underlineStyle = { (linkResult) in
                switch linkResult.detectionType {
                default:
                    return NSUnderlineStyle.single
                }
            }
            
            lbTitle.text = chatGrade?.title
            
            lbTitle.didTouch = { (touchResult) in
                switch touchResult.state {
                case .began:
                    break
                case .ended:
                    if let linkResult = touchResult.linkResult {
                        if linkResult.detectionType == .url {
                            var text = linkResult.text
                            if !text.starts(with: "http://") && !text.starts(with: "https://") {
                                text = "http://\(text)"
                            }
                            if let link = URL(string: text) {
                                UIApplication.shared.open(link)
                            }
                        } else if linkResult.detectionType == .phoneNumber {
                            if let url = URL(string: "telprompt://\(linkResult.text)") {
                                UIApplication.shared.open(url)
                            }
                        }
                    }
                default:
                    break
                }
            }
            
            lbSubtitle.textColor = UiUtility.colorDarkGrey
            lbSubtitle.font = UIFont(name: "PingFangTC-Regular", size: 15)
            lbSubtitle.foregroundColor = { (linkResult) in
                switch linkResult.detectionType {
                case .url:
                    return UIColor(rgb: 0x007AFF)
                default:
                    return UiUtility.colorDarkGrey
                }
            }
            
            lbSubtitle.underlineStyle = { (linkResult) in
                switch linkResult.detectionType {
                default:
                    return NSUnderlineStyle.single
                }
            }
            
            lbSubtitle.text = chatGrade?.content
            
            lbSubtitle.didTouch = { (touchResult) in
                switch touchResult.state {
                case .began:
                    break
                case .ended:
                    if let linkResult = touchResult.linkResult {
                        if linkResult.detectionType == .url {
                            var text = linkResult.text
                            if !text.starts(with: "http://") && !text.starts(with: "https://") {
                                text = "http://\(text)"
                            }
                            if let link = URL(string: text) {
                                UIApplication.shared.open(link)
                            }
                        } else if linkResult.detectionType == .phoneNumber {
                            if let url = URL(string: "telprompt://\(linkResult.text)") {
                                UIApplication.shared.open(url)
                            }
                        }
                    }
                default:
                    break
                }
            }
            
            switch grade {
            case 0:
                btnStar0.isHighlighted = false
                btnStar1.isHighlighted = false
                btnStar2.isHighlighted = false
                btnStar3.isHighlighted = false
                btnStar4.isHighlighted = false
            case 1:
                btnStar0.isHighlighted = true
                btnStar1.isHighlighted = false
                btnStar2.isHighlighted = false
                btnStar3.isHighlighted = false
                btnStar4.isHighlighted = false
            case 2:
                btnStar0.isHighlighted = true
                btnStar1.isHighlighted = true
                btnStar2.isHighlighted = false
                btnStar3.isHighlighted = false
                btnStar4.isHighlighted = false
            case 3:
                btnStar0.isHighlighted = true
                btnStar1.isHighlighted = true
                btnStar2.isHighlighted = true
                btnStar3.isHighlighted = false
                btnStar4.isHighlighted = false
            case 4:
                btnStar0.isHighlighted = true
                btnStar1.isHighlighted = true
                btnStar2.isHighlighted = true
                btnStar3.isHighlighted = true
                btnStar4.isHighlighted = false
            case 5:
                btnStar0.isHighlighted = true
                btnStar1.isHighlighted = true
                btnStar2.isHighlighted = true
                btnStar3.isHighlighted = true
                btnStar4.isHighlighted = true
                
            default:
                break
            }
            
        } else if message.direction == .keep {
            cell = tableView.dequeueReusableCell(withIdentifier: "KeepCell", for: indexPath)
            
            for constraint in cell.contentView.constraints {
                if constraint.identifier == "padding_top" {
                    constraint.constant = UiUtility.adaptiveSize(size: 10)
                } else if constraint.identifier == "padding_bottom" {
                    constraint.constant = UiUtility.adaptiveSize(size: 10)
                }
            }
            
            let vContainer: UIView = cell.viewWithTag(1)!
            let lbTitle: ContextLabel = cell.viewWithTag(2) as! ContextLabel
            let btnYes: UIButton = cell.viewWithTag(3) as! UIButton
            let btnNo: UIButton = cell.viewWithTag(4) as! UIButton
            
            btnYes.addTarget(self, action: #selector(yesBtnClicked(_:)), for: .touchUpInside)
            btnNo.addTarget(self, action: #selector(noBtnClicked(_:)), for: .touchUpInside)
            
            for constraint in vContainer.constraints {
                if constraint.identifier == "title_padding_left" {
                    constraint.constant = UiUtility.adaptiveSize(size: 10)
                } else if constraint.identifier == "title_padding_top" {
                    constraint.constant = UiUtility.adaptiveSize(size: 8)
                } else if constraint.identifier == "title_padding_right" {
                    constraint.constant = UiUtility.adaptiveSize(size: 10)
                } else if constraint.identifier == "subtitle_padding_top" {
                    if chatGrade!.content == nil {
                        constraint.constant = 0
                    } else {
                        constraint.constant = UiUtility.adaptiveSize(size: 4)
                    }
                }
            }
            
            vContainer.layer.cornerRadius = UiUtility.adaptiveSize(size: 10)
            vContainer.backgroundColor = UiUtility.colorBackgroundIncomingMessage
            
            lbTitle.textColor = UiUtility.colorDarkGrey
            lbTitle.font = UIFont(name: "PingFangTC-Regular", size: 15)
            lbTitle.text = "chat_keep_chatbox".localized
            
        } else if message.direction == .incoming {
            if message.text != nil {
                cell = tableView.dequeueReusableCell(withIdentifier: "IncomingTextCell", for: indexPath)
                
                for constraint in cell.contentView.constraints {
                    if constraint.identifier == "padding_top" {
                        //                        Logger.d("padding_top")
                        constraint.constant = UiUtility.adaptiveSize(size: 10)
                    } else if constraint.identifier == "padding_bottom" {
                        //                        Logger.d("padding_bottom")
                        constraint.constant = UiUtility.adaptiveSize(size: 10)
                    }
                }
                
                let vContainer: UIView = cell.viewWithTag(1)!
                let lbText: ContextLabel = cell.viewWithTag(2) as! ContextLabel
                
                for constraint in lbText.constraints {
                    if constraint.identifier == "text_width" {
                        constraint.constant = UiUtility.adaptiveSize(size: 230)
                    }
                }
                
                for constraint in vContainer.constraints {
                    if constraint.identifier == "text_padding_left" {
                        //                        Logger.d("text_padding_left")
                        constraint.constant = UiUtility.adaptiveSize(size: 10)
                    } else if constraint.identifier == "text_padding_top" {
                        //                        Logger.d("text_padding_top")
                        constraint.constant = UiUtility.adaptiveSize(size: 6)
                    } else if constraint.identifier == "text_padding_right" {
                        //                        Logger.d("text_padding_right")
                        constraint.constant = UiUtility.adaptiveSize(size: 10)
                    } else if constraint.identifier == "text_padding_bottom" {
                        //                        Logger.d("text_padding_bottom")
                        constraint.constant = UiUtility.adaptiveSize(size: 6)
                    }
                }
                
                vContainer.layer.cornerRadius = UiUtility.adaptiveSize(size: 10)
                vContainer.backgroundColor = UiUtility.colorBackgroundIncomingMessage
                
                lbText.textColor = UiUtility.colorDarkGrey
                lbText.font = UIFont(name: "PingFangTC-Regular", size: 15)
                lbText.foregroundColor = { (linkResult) in
                    switch linkResult.detectionType {
                    case .url:
                        return UIColor(rgb: 0x007AFF)
                    default:
                        return UiUtility.colorDarkGrey
                    }
                }
                
                lbText.underlineStyle = { (linkResult) in
                    switch linkResult.detectionType {
                    default:
                        return []
                    }
                }
                
                lbText.setContextLabelDataWithText(message.text)
                //lbText.text = message.text
                
                lbText.underlineStyle = { (linkResult) in
                    switch linkResult.detectionType {
                    case .url, .phoneNumber:
                        return NSUnderlineStyle.single
                    default:
                        return []
                    }
                }
                
                lbText.setContextLabelDataWithText(message.text)
                
                lbText.didTouch = { (touchResult) in
                    switch touchResult.state {
                    case .began:
                        //                        Logger.d("began")
                        break
                    case .ended:
                        //                        Logger.d("ended")
                        if let linkResult = touchResult.linkResult {
                            if linkResult.detectionType == .url {
                                //                                Logger.d("text: \(linkResult.text)")
                                
                                var text = linkResult.text
                                if !text.starts(with: "http://") && !text.starts(with: "https://") {
                                    text = "http://\(text)"
                                }
                                if let link = URL(string: text) {
                                    UIApplication.shared.open(link)
                                }
                            } else if linkResult.detectionType == .phoneNumber {
                                if let url = URL(string: "telprompt://\(linkResult.text)") {
                                    UIApplication.shared.open(url)
                                }
                            }
                        }
                    //                    Logger.d("ended, \(touchResult)")
                    default:
                        break
                    }
                }
            } else if let button = message.button {
                if let type = button.type {
                    switch type {
                    //                    case ChatButton.ButtonType.text.rawValue:
                    //                        Logger.d("text")
                    //                    case ChatButton.ButtonType.url.rawValue:
                    //                        Logger.d("url")
                    case ChatButton.ButtonType.image.rawValue:
                        //                        Logger.d("image")
                        cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath)
                        
                        for constraint in cell.contentView.constraints {
                            if constraint.identifier == "padding_top" {
                                constraint.constant = UiUtility.adaptiveSize(size: 10)
                            } else if constraint.identifier == "padding_bottom" {
                                constraint.constant = UiUtility.adaptiveSize(size: 10)
                            }
                        }
                        
                        let vContainer: UIView = cell.viewWithTag(1)!
                        let ivPhoto: UIImageView = cell.viewWithTag(2) as! UIImageView
                        
                        vContainer.layer.cornerRadius = UiUtility.adaptiveSize(size: 10)
                        vContainer.backgroundColor = UiUtility.colorBackgroundIncomingMessage
                        
                        if let content = button.content {
                            let url = URL(string: content)
                            ivPhoto.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_background"))
                        } else {
                            ivPhoto.image = UIImage.init(named: "default_profile_background")
                        }
                        
                    //                    case ChatButton.ButtonType.deeplink.rawValue:
                    //                        Logger.d("deeplink")
                    case ChatButton.ButtonType.generic.rawValue:
                        //                        Logger.d("generic")
                        cell = tableView.dequeueReusableCell(withIdentifier: "GenericCell", for: indexPath)
                        
                        //                        Logger.d("genericMenu: \(String(describing: button.genericMenu))")
                        
                        if let elementts = button.genericMenu?.elements {
                            if elementts.count > 0 {
                                let element = elementts[0]
                                
                                //                            Logger.d("element: \(element)")
                                
                                for constraint in cell.contentView.constraints {
                                    if constraint.identifier == "padding_top" {
                                        constraint.constant = UiUtility.adaptiveSize(size: 10)
                                    } else if constraint.identifier == "padding_bottom" {
                                        constraint.constant = UiUtility.adaptiveSize(size: 10)
                                    }
                                }
                                
                                let vContainer: UIView = cell.viewWithTag(1)!
                                let ivPhoto: UIImageView = cell.viewWithTag(2) as! UIImageView
                                let lbTitle: ContextLabel = cell.viewWithTag(3) as! ContextLabel
                                let lbSubtitle: ContextLabel = cell.viewWithTag(4) as! ContextLabel
                                let btnAction0: UIButton = cell.viewWithTag(5) as! UIButton
                                let btnAction1: UIButton = cell.viewWithTag(6) as! UIButton
                                let btnAction2: UIButton = cell.viewWithTag(7) as! UIButton
                                
                                btnAction0.addTarget(self, action: #selector(actionClicked(_:)), for: .touchUpInside)
                                btnAction1.addTarget(self, action: #selector(actionClicked(_:)), for: .touchUpInside)
                                btnAction2.addTarget(self, action: #selector(actionClicked(_:)), for: .touchUpInside)
                                
                                for constraint in vContainer.constraints {
                                    if constraint.identifier == "title_padding_left" {
                                        constraint.constant = UiUtility.adaptiveSize(size: 10)
                                    } else if constraint.identifier == "title_padding_top" {
                                        constraint.constant = UiUtility.adaptiveSize(size: 6)
                                    } else if constraint.identifier == "title_padding_right" {
                                        constraint.constant = UiUtility.adaptiveSize(size: 10)
                                    } else if constraint.identifier == "subtitle_padding_top" {
                                        if element.subtitle == nil {
                                            constraint.constant = 0
                                        } else {
                                            constraint.constant = UiUtility.adaptiveSize(size: 6)
                                        }
                                    } else if constraint.identifier == "subtitle_padding_bottom" {
                                        constraint.constant = UiUtility.adaptiveSize(size: 9.5)
                                    } else if constraint.identifier == "action0_top" {
                                        constraint.constant = UiUtility.adaptiveSize(size: 10)
                                    } else if constraint.identifier == "action1_top" {
                                        constraint.constant = UiUtility.adaptiveSize(size: 10)
                                    } else if constraint.identifier == "action2_top" {
                                        constraint.constant = UiUtility.adaptiveSize(size: 10)
                                    }
                                }
                                
                                vContainer.layer.cornerRadius = UiUtility.adaptiveSize(size: 10)
                                vContainer.backgroundColor = UiUtility.colorBackgroundIncomingMessage
                                
                                let url = URL(string: element.image_url)
                                ivPhoto.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_background"))
                                
                                lbTitle.textColor = .black
                                lbTitle.font = UIFont(name: "PingFangTC-Medium", size: 14)
                                lbTitle.foregroundColor = { (linkResult) in
                                    switch linkResult.detectionType {
                                    case .url:
                                        return UIColor(rgb: 0x007AFF)
                                    default:
                                        return .black
                                    }
                                }
                                
                                lbTitle.underlineStyle = { (linkResult) in
                                    switch linkResult.detectionType {
                                    default:
                                        return NSUnderlineStyle.single
                                    }
                                }
                                
                                lbTitle.text = element.title
                                
                                lbTitle.didTouch = { (touchResult) in
                                    switch touchResult.state {
                                    case .began:
                                        break
                                    case .ended:
                                        if let linkResult = touchResult.linkResult {
                                            if linkResult.detectionType == .url {
                                                var text = linkResult.text
                                                if !text.starts(with: "http://") && !text.starts(with: "https://") {
                                                    text = "http://\(text)"
                                                }
                                                if let link = URL(string: text) {
                                                    UIApplication.shared.open(link)
                                                }
                                            } else if linkResult.detectionType == .phoneNumber {
                                                if let url = URL(string: "telprompt://\(linkResult.text)") {
                                                    UIApplication.shared.open(url)
                                                }
                                            }
                                        }
                                    default:
                                        break
                                    }
                                }
                                
                                lbSubtitle.textColor = UiUtility.colorDarkGrey
                                lbSubtitle.font = UIFont(name: "PingFangTC-Regular", size: 12)
                                lbSubtitle.foregroundColor = { (linkResult) in
                                    switch linkResult.detectionType {
                                    case .url:
                                        return UIColor(rgb: 0x007AFF)
                                    default:
                                        return UiUtility.colorDarkGrey
                                    }
                                }
                                
                                lbSubtitle.underlineStyle = { (linkResult) in
                                    switch linkResult.detectionType {
                                    default:
                                        return NSUnderlineStyle.single
                                    }
                                }
                                
                                lbSubtitle.text = element.subtitle
                                
                                lbSubtitle.didTouch = { (touchResult) in
                                    switch touchResult.state {
                                    case .began:
                                        break
                                    case .ended:
                                        if let linkResult = touchResult.linkResult {
                                            if linkResult.detectionType == .url {
                                                var text = linkResult.text
                                                if !text.starts(with: "http://") && !text.starts(with: "https://") {
                                                    text = "http://\(text)"
                                                }
                                                if let link = URL(string: text) {
                                                    UIApplication.shared.open(link)
                                                }
                                            }  else if linkResult.detectionType == .phoneNumber {
                                                if let url = URL(string: "telprompt://\(linkResult.text)") {
                                                    UIApplication.shared.open(url)
                                                }
                                            }
                                        }
                                    default:
                                        break
                                    }
                                }
                                
                                let chatAction: ChatAction = element.actions![0]
                                btnAction0.layer.cornerRadius = UiUtility.adaptiveSize(size: 10)
                                btnAction0.layer.borderWidth = UiUtility.adaptiveSize(size: 1)
                                btnAction0.layer.borderColor = UiUtility.colorBrownGrey.cgColor
                                btnAction0.setTitle(chatAction.title, for: .normal)
                                btnAction0.setTitleColor(.black, for: .normal)
                                
                                btnAction1.isHidden = true
                                btnAction2.isHidden = true
                                if element.actions!.count == 2 {
                                    let chatAction1: ChatAction = element.actions![1]
                                    btnAction1.layer.cornerRadius = UiUtility.adaptiveSize(size: 10)
                                    btnAction1.layer.borderWidth = UiUtility.adaptiveSize(size: 1)
                                    btnAction1.layer.borderColor = UiUtility.colorBrownGrey.cgColor
                                    btnAction1.setTitle(chatAction1.title, for: .normal)
                                    btnAction1.setTitleColor(.black, for: .normal)
                                    btnAction1.isHidden = false
                                } else if element.actions!.count == 3 {
                                    let chatAction1: ChatAction = element.actions![1]
                                    btnAction1.layer.cornerRadius = UiUtility.adaptiveSize(size: 10)
                                    btnAction1.layer.borderWidth = UiUtility.adaptiveSize(size: 1)
                                    btnAction1.layer.borderColor = UiUtility.colorBrownGrey.cgColor
                                    btnAction1.setTitle(chatAction1.title, for: .normal)
                                    btnAction1.setTitleColor(.black, for: .normal)
                                    btnAction1.isHidden = false
                                    
                                    let chatAction2: ChatAction = element.actions![2]
                                    btnAction2.layer.cornerRadius = UiUtility.adaptiveSize(size: 10)
                                    btnAction2.layer.borderWidth = UiUtility.adaptiveSize(size: 1)
                                    btnAction2.layer.borderColor = UiUtility.colorBrownGrey.cgColor
                                    btnAction2.setTitle(chatAction2.title, for: .normal)
                                    btnAction2.setTitleColor(.black, for: .normal)
                                    btnAction1.isHidden = false
                                    btnAction2.isHidden = false
                                }
                            }
                        }
                        
                        
                    default:
                        //                        Logger.d("default")
                        break
                    }
                }
            }
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "OutgoingCell", for: indexPath)
            
            for constraint in cell.contentView.constraints {
                if constraint.identifier == "padding_top" {
                    constraint.constant = UiUtility.adaptiveSize(size: 10)
                } else if constraint.identifier == "padding_bottom" {
                    constraint.constant = UiUtility.adaptiveSize(size: 10)
                }
            }
            
            let vContainer: UIView = cell.viewWithTag(1)!
            let lbText: ContextLabel = cell.viewWithTag(2) as! ContextLabel
            
            for constraint in vContainer.constraints {
                if constraint.identifier == "text_padding_left" {
                    constraint.constant = UiUtility.adaptiveSize(size: 10)
                } else if constraint.identifier == "text_padding_top" {
                    constraint.constant = UiUtility.adaptiveSize(size: 6)
                } else if constraint.identifier == "text_padding_right" {
                    constraint.constant = UiUtility.adaptiveSize(size: 10)
                } else if constraint.identifier == "text_padding_bottom" {
                    constraint.constant = UiUtility.adaptiveSize(size: 6)
                }
            }
            
            for constraint in lbText.constraints {
                if constraint.identifier == "text_width" {
                    constraint.constant = UiUtility.adaptiveSize(size: 235)
                }
            }
            
            vContainer.layer.cornerRadius = UiUtility.adaptiveSize(size: 10)
            vContainer.backgroundColor = UiUtility.colorBackgroundOutgoingMessage
            
            lbText.textColor = .white
            lbText.font = UIFont(name: "PingFangTC-Regular", size: 15)
            
            lbText.setContextLabelDataWithText(message.text)
            //lbText.text = message.text
            
            lbText.underlineStyle = { (linkResult) in
                switch linkResult.detectionType {
                case .url, .phoneNumber:
                    return NSUnderlineStyle.single
                default:
                    return []
                }
            }
            
            lbText.setContextLabelDataWithText(message.text)
            
            lbText.foregroundColor = { (linkResult) in
                switch linkResult.detectionType {
                case .url:
                    return UIColor(rgb: 0x007AFF)
                case .phoneNumber:
                    return UIColor.white
                default:
                    return UiUtility.colorDarkGrey
                }
            }
            
            lbText.didTouch = { (touchResult) in
                switch touchResult.state {
                case .began:
                    break
                case .ended:
                    if let linkResult = touchResult.linkResult {
                        if linkResult.detectionType == .url {
                            var text = linkResult.text
                            if !text.starts(with: "http://") && !text.starts(with: "https://") {
                                text = "http://\(text)"
                            }
                            if let link = URL(string: text) {
                                UIApplication.shared.open(link)
                            }
                        }  else if linkResult.detectionType == .phoneNumber {
                            if let url = URL(string: "telprompt://\(linkResult.text)") {
                                UIApplication.shared.open(url)
                            }
                        }
                    }
                default:
                    break
                }
            }
        }
        
        cell.backgroundColor = UiUtility.colorBackgroundWhite
        cell.selectionStyle = .none
        
        return cell
    }
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //        tableView.deselectRow(at: indexPath, animated: true)
    //    }
    
    
    
    // MARK: collection view delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (menuButtons?.count ?? 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    //        return UiUtility.adaptiveSize(size: 1000)
    //    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return UiUtility.adaptiveSize(size: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat = UiUtility.adaptiveSize(size: 30)
        
        return CGSize(width: menuButtons![indexPath.row].buttonName!.width(withConstrainedHeight: height, font: UIFont(name: "PingFangTC-Regular", size: 15)!) + UiUtility.adaptiveSize(size: 30), height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KeywordCell", for: indexPath)
        cell.clipsToBounds = false
        
        let viewContainer: UIView = cell.viewWithTag(2)!
        let lbName: UILabel = cell.viewWithTag(3) as! UILabel
        
        let chatButton = menuButtons![indexPath.row]
        
        viewContainer.layer.cornerRadius = UiUtility.adaptiveSize(size: 15)
        viewContainer.layer.shadowColor = UIColor.black.cgColor
        viewContainer.layer.shadowOpacity = 0.3
        viewContainer.layer.shadowOffset = CGSize(width: 0, height: 0)
        viewContainer.layer.shadowRadius = UiUtility.adaptiveSize(size: 5.0)
        
        lbName.font = UIFont(name: "PingFangTC-Regular", size: 15)
        lbName.textColor = UIColor(rgb: 0x515151)
        lbName.text = chatButton.buttonName
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.updateTimer(initial: false)
        
        let chatButton = menuButtons![indexPath.row]
        
        // TEST
        //        chatButton.type = "deeplink"
        //        chatButton.content = "http://tw.yahoo.com"
        //        chatButton.content = "volkswagen://mycar/car"
        //        chatButton.content = "volkswagen://mycar/maintenance"
        //        chatButton.content = "volkswagen://profile/my/membercard/rewardpoints"
        //        chatButton.content = "volkswagen://profile/my/membercard"
        //        chatButton.content = "volkswagen://post/336"
        // END OF TEST
        //        Logger.d("tap chatButton: \(chatButton)")
        
        
        if let type = chatButton.type {
            switch type {
            case ChatButton.ButtonType.text.rawValue:
                //                Logger.d("text")
                var messages: [ChatMessage] = []
                messages.append(ChatMessage.init(with: .outgoing, text: chatButton.buttonName))
                //                chatMessages?.append(ChatMessage.init(with: .incoming, text: chatButton.content))
                //                tableView.reloadData()
                //                tableView.scrollToBottom()
                if let content = chatButton.content {
                    messages.append(ChatMessage.init(with: .incoming, text: content))
                }
                if messages.count != 0 {
                    self.generateMessages(with: messages)
                }
            case ChatButton.ButtonType.url.rawValue:
                //                Logger.d("url")
                if let content = chatButton.content {
                    var text = content
                    if !text.starts(with: "http://") && !text.starts(with: "https://") {
                        text = "http://\(text)"
                    }
                    if let link = URL(string: text) {
                        UIApplication.shared.open(link)
                    }
                }
                
            case ChatButton.ButtonType.image.rawValue:
                //                Logger.d("image")
                var messages: [ChatMessage] = []
                messages.append(ChatMessage.init(with: .outgoing, text: chatButton.buttonName))
                messages.append(ChatMessage.init(with: .incoming, text: nil, button: chatButton))
                self.generateMessages(with: messages)
            case ChatButton.ButtonType.deeplink.rawValue:
                //                Logger.d("deeplink")
                if let deepLink = chatButton.content {
                    self.handleDeepLink(deepLink: deepLink)
                }
            case ChatButton.ButtonType.generic.rawValue:
                //                Logger.d("generic")
                var messages: [ChatMessage] = []
                messages.append(ChatMessage.init(with: .outgoing, text: chatButton.buttonName))
                messages.append(ChatMessage.init(with: .incoming, text: nil, button: chatButton))
                self.generateMessages(with: messages)
            default:
                //                Logger.d("default")
                break
            }
            
            if let id: Int = chatButton.id {
                ChatManager.shared().writeTalkRecord(with: ChatRecord.init(with: ChatRecord.categoryButton, id: id, buttonName: chatButton.buttonName, title: nil, button: chatButton), listener: self)
            }
        }
    }
    
    //    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    //        if scrollView.contentOffset.y < 0 {
    //            beforeTableViewContentHeight = tableView.contentSize.height
    //            beforeTableViewOffset = tableView.contentOffset.y
    //            ChatManager.shared().getTalkHistory(start: 0, offset: 100000, listener: self)
    //        }
    //    }
    
    
    
    // MARK: private methods
    
    private func handleDeepLink(deepLink: String) {
        if deepLink.caseInsensitiveCompare("volkswagen://mycar/car") == ComparisonResult.orderedSame {
            self.tabControllerDelegate?.goCarPage(of: 0)
        } else if deepLink.caseInsensitiveCompare("volkswagen://mycar/maintenance") == ComparisonResult.orderedSame {
            self.tabControllerDelegate?.goCarPage(of: 1)
        } else if deepLink.caseInsensitiveCompare("volkswagen://profile/my/membercard/rewardpoints") == ComparisonResult.orderedSame {
            
        } else if deepLink.caseInsensitiveCompare("volkswagen://profile/my/membercard") == ComparisonResult.orderedSame {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MemberLevelViewController")
            self.navigationController?.pushViewController(vc, animated: true)
        } else if deepLink.starts(with: "volkswagen://post/") {
            let splittedArray = deepLink.split(separator: "/")
            if splittedArray.count > 1 {
                let newsId: String = String(splittedArray[splittedArray.count - 1])
                if let i = Int(newsId) {
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedDetailViewController") as! FeedDetailViewController
                    vc.news = News.init(with: i)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    private func fetchInitData() {
        if !greetingFetched {
            ChatManager.shared().getGreeting(listener: self)
        } else if !menuButtonsFetched {
            ChatManager.shared().getMenuButtons(listener: self)
        } else if !chatGradeFetched {
            ChatManager.shared().getGrading(listener: self)
        }
        
        //        beforeTableViewContentHeight = tableView.contentSize.height
        //        beforeTableViewOffset = tableView.contentOffset.y
        ChatManager.shared().getTalkHistory(start: 0, offset: 100000, listener: self)
    }
    
    @IBAction private func leaveMessage(_ sender: Any) {
        self.send(textField.text)
    }
    
    private func send(_ text: String?) {
        self.updateTimer(initial: false)
        if let text = text {
            let text = text.trimmingCharacters(in: .whitespaces)
            if text.count != 0 {
                //                Logger.d("textField.text: \(text)")
                self.generateTextMessage(with: text, isIncoming: false)
                
                textField.text = nil
                textField.resignFirstResponder()
                
                ChatManager.shared().generateAnswer(with: text, listener: self)
            }
        }
    }
    
    @objc private func actionClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            var message: ChatMessage!
            if indexPath.section == 0 {
                message = chatHistoryMsg![indexPath.row]
            } else {
                message = chatMessages![indexPath.row]
            }
            //let message: ChatMessage = chatMessages![indexPath.row]
            if let button = message.button, let element: ChatElement = button.genericMenu?.elements?[0] {
                switch sender.tag {
                case 5:
                    let action: ChatAction = element.actions![0]
                    self.handleAction(action, button: button)
                case 6:
                    let action: ChatAction = element.actions![1]
                    self.handleAction(action, button: button)
                case 7:
                    let action: ChatAction = element.actions![2]
                    self.handleAction(action, button: button)
                default:
                    break
                }
            }
        }
    }
    
    private func handleAction(_ action: ChatAction, button: ChatButton) {
        if let type = action.type {
            self.updateTimer(initial: false)
            
            if type == "text" {
                //                Logger.d("text action")
                if let data = action.data {
                    if data.hasPrefix("*") && data.hasSuffix("*") {
                        self.generateTextMessage(with: "chat_replace_msg".localized, isIncoming: true)
                        ChatManager.shared().getButtons(with: data, listener: self)
                        ChatManager.shared().writeTalkRecord(with: ChatRecord.init(with: action.title, answer: "chat_replace_msg".localized, score: nil), listener: self)
                    } else {
                        self.generateTextMessage(with: data, isIncoming: true)
                        ChatManager.shared().writeTalkRecord(with: ChatRecord.init(with: action.title, answer: data, score: nil), listener: self)
                    }
                }
                return
            } else if type == "url" {
                //                Logger.d("url action")
                if let data = action.data {
                    var text = data
                    if !text.starts(with: "http://") && !text.starts(with: "https://") {
                        text = "http://\(text)"
                    }
                    ChatManager.shared().writeTalkRecord(with: ChatRecord.init(with: action.title, answer: text, score: nil), listener: self)
                    if let link = URL(string: text) {
                        UIApplication.shared.open(link)
                    }
                }
                return
            }
            
            if let id: Int = button.id {
                ChatManager.shared().writeTalkRecord(with: ChatRecord.init(with: ChatRecord.categoryButton, id: id, buttonName: button.buttonName, title: action.title, button: button), listener: self)
            }
        }
    }
    
    private func generateTextMessage(with text: String, isIncoming: Bool) {
        if isIncoming {
            chatMessages?.append(ChatMessage.init(with: .incoming, text: text, button: nil))
        } else {
            chatMessages?.append(ChatMessage.init(with: .outgoing, text: text))
        }
        tableView.reloadData()
        tableView.scrollToBottom()
    }
    
    private func generateMessages(with messages: [ChatMessage]) {
        chatMessages?.append(contentsOf: messages)
        tableView.reloadData()
        tableView.scrollToBottom()
    }
    
    @objc private func timerFired() {
        //        Logger.d("timerFired")
        if let timer = timer, timer.isValid {
            timer.invalidate()
        }
        
        if chatGrade != nil {
            //chatMessages?.append(ChatMessage.init(with: .grade))
            chatMessages?.append(ChatMessage.init(with: .keep))
            tableView.reloadData()
            tableView.scrollToBottom()
        }
    }
    
    private func updateTimer(initial: Bool) {
        //        Logger.d("updateTimer, initial: \(initial)")
        if initial {
            if timer == nil {
                //                Logger.d("updateTimer, initial")
                timer = Timer.scheduledTimer(timeInterval: Config.gradingSeconds, target: self, selector: #selector(timerFired), userInfo: nil, repeats: false)
            }
        } else {
            if let timer = timer {
                if timer.isValid {
                    timer.invalidate()
                    //                    Logger.d("updateTimer, restart")
                    self.timer = Timer.scheduledTimer(timeInterval: Config.gradingSeconds, target: self, selector: #selector(timerFired), userInfo: nil, repeats: false)
                }
                //            } else {
                //                Logger.d("timer is already fired, ignore")
            }
        }
    }
    
    @objc private func starClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            switch sender.tag {
            case 4:
                grade = 1
            case 5:
                grade = 2
            case 6:
                grade = 3
            case 7:
                grade = 4
            case 8:
                grade = 5
                
            default:
                break
            }
            
            if grade != 0 {
                if let id: Int = chatGrade!.buttons![grade - 1].id {
                    ChatManager.shared().writeTalkRecord(with: ChatRecord.init(with: ChatRecord.categoryGrading, id: id, buttonName: nil, title: nil, button: chatGrade!.buttons![grade - 1]), listener: self)
                }
            }
            
            tableView.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    @objc private func yesBtnClicked(_ sender: UIButton) {
        timer?.invalidate()
        timer = nil
        updateTimer(initial: true)
        if let cm = chatMessages {
            for (index, chat) in cm.enumerated() {
                if chat.direction == .keep {
                    chatMessages!.remove(at: index)
                    break
                }
            }
        }
        tableView.reloadData()
        tableView.scrollToBottom()
    }
    
    @objc private func noBtnClicked(_ sender: UIButton) {
        if chatGrade != nil {
            chatMessages?.append(ChatMessage.init(with: .grade))
            tableView.reloadData()
            tableView.scrollToBottom()
        }
    }
    
    func initUiState() {
        greetingFetched = false
        menuButtonsFetched = false
        chatGradeFetched = false
        chatMessages = []
        menuButtons = nil
        grade = 0
        chatGrade = nil
        
        if let timer = timer, timer.isValid {
            timer.invalidate()
        }
        timer = nil
        
        tableView.reloadData()
        collectionViewKeyword.reloadData()
    }
}
