//
//  NotificationViewController.swift
//  myvolkswagen
//
//  Created by Apple on 2/20/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class NotificationViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource {

    enum Section: Int {
        case latest = 0, before
        static var allCases: [Section] {
            var values: [Section] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    var notices: [Notice] = []
    var histories: [Notice] = []
    
    
    @IBOutlet weak var viewNavi: UIView!
    @IBOutlet weak var title0: UILabel!
    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var underline0: UIView!
    @IBOutlet weak var underline1: UIView!
    var tabSelectedIndex: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        naviBar.setTitle("notification".localized)
        
        viewNavi.backgroundColor = UiUtility.colorBackgroundWhite
        
        title0.textColor = UIColor.black
        title0.font = UIFont(name: "PingFangTC-Medium", size: 18)
        title0.text = "notification".localized
        
        title1.textColor = UIColor.black
        title1.font = UIFont(name: "PingFangTC-Medium", size: 18)
        title1.text = "news".localized
        
        underline0.backgroundColor = UiUtility.colorPinkyRed
        underline1.backgroundColor = UiUtility.colorPinkyRed
        
        underline0.layer.cornerRadius = 2
        underline1.layer.cornerRadius = 2
        
        tableView.backgroundColor = UiUtility.colorBackgroundWhite
        
        refreshUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if NoticeManager.shared().uiUpdateNeeded {
            NoticeManager.shared().uiUpdateNeeded = false
            
            //histories = NoticeManager.shared().getNotice(kind: tabSelectedIndex + 1, historical: true, fromRemote: false, listener: self)
            notices = NoticeManager.shared().getNotice(kind: tabSelectedIndex + 1, historical: false, fromRemote: false, listener: self)
            tableView.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NoticeManager.shared().getNoticeRingClose()
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//
//        NoticeManager.shared().getNoticeEventCount()
//    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    @IBAction func b(_ sender: UIButton) {
        back()
    }
    
    @IBAction func tab0(_ sender: UIButton) {
        if tabSelectedIndex != 0 {
            tabSelectedIndex = 0
            
            refreshUI()
        }
    }
    
    @IBAction func tab1(_ sender: UIButton) {
        if tabSelectedIndex != 1 {
            tabSelectedIndex = 1
            
            refreshUI()
        }
    }
    
    private func refreshUI() {
        histories = []
        notices = []
        
        underline0.isHidden = (tabSelectedIndex == 0 ? false : true)
        underline1.isHidden = (tabSelectedIndex == 1 ? false : true)
        
        if tabSelectedIndex == 0 {
//            histories = NoticeManager.shared().getNotice(kind: tabSelectedIndex + 1, historical: true, fromRemote: true, listener: self)
//            notices = NoticeManager.shared().getNotice(kind: tabSelectedIndex + 1, historical: false, fromRemote: true, listener: self)
            //NoticeManager.shared().getNotice(kind: tabSelectedIndex + 1, historical: true, fromRemote: true, listener: self)
            NoticeManager.shared().getNotice(kind: tabSelectedIndex + 1, historical: false, fromRemote: true, listener: self)
        } else {
            histories = []
            //notices = NoticeManager.shared().getNotice(kind: 3, historical: false, fromRemote: true, listener: self)
            NoticeManager.shared().getNotice(kind: 3, historical: false, fromRemote: true, listener: self)
        }
    }
    
    
    
    // MARK: notice protocol
    
    override func didStartGetNotice(historical: Bool) {
        Logger.d("didStartGetNotice historical: \(historical)")
//        showHud()
    }
    
    override func didFinishGetNotice(success: Bool, historical: Bool, notices: [Notice]?) {
        Logger.d("didFinishGetNotice historical: \(historical)")
//        dismissHud()
        
        if success {
            if historical {
                if let n = notices {
                    histories = n
                }
            } else {
                if let n = notices {
                    self.notices = n
                }
            }
            
            NoticeManager.shared().unreadCount = 0
        }
        self.tableView.reloadData()
    }
    
    override func didStartSetRead() {}
    
    override func didFinishSetRead(success: Bool, notice: Notice) {
        Logger.d("didFinishSetRead success: \(success)")
    }
    
    
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Section.init(rawValue: section)! {
        case .latest:
            if notices.count == 0 {
                return 0
            } else {
                return notices.count + 1
            }
        case .before:
            if histories.count == 0 {
                return 0
            } else {
                return histories.count + 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UiUtility.adaptiveSize(size: 24)
        } else {
            let width = UiUtility.adaptiveSize(size: 375 - 70 - 20)
            let font = UIFont(name: "PingFangTC-Regular", size: 14)!
            let maxContentHeight: CGFloat = "line_2_notice_text".localized.height(withConstrainedWidth: width, font: font)
            
            var h: CGFloat = 0
            h += UiUtility.adaptiveSize(size: 16) // height before content
            let notice = getNotice(of: indexPath)
            
            if !Utility.isEmpty(notice.content) {
                h += min(notice.content!.height(withConstrainedWidth: width, font: font), maxContentHeight)
            }
            
            h += UiUtility.adaptiveSize(size: 36) // date
            //            Logger.d("row \(indexPath.row) height \(h)")
            return max(h, UiUtility.adaptiveSize(size: 40 + 16 * 2))
        }
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        //        cellHeightsDictionary[indexPath] = cell.frame.size.height
//
//        if indexPath.section == Section.feed.rawValue {
//            if newsList.count != 0 {
//                let lastElement = newsList.count - 1
//                if indexPath.row == lastElement {
//                    if !NewsManager.shared().isListEndReached() {
//                        // handle your logic here to get more items, add it to dataSource and reload tableview
//                        Logger.d("should try load more")
//                        NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: true, listener: self)
//                    }
//                }
//            }
//        }
//    }
    
//    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        cellHeightsDictionary[indexPath] = cell.frame.size.height
//    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        if let h:CGFloat = cellHeightsDictionary[indexPath] {
//            return h
//        } else {
//            return UITableView.automaticDimension
//        }
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        switch Section.init(rawValue: indexPath.section)! {
        case .latest:
            if indexPath.row == 0 {
                cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath)
                cell.selectionStyle = .none
                cell.backgroundColor = UiUtility.colorBackgroundWhite
                
                let label: UILabel = cell.viewWithTag(1) as! UILabel
                label.textColor = UiUtility.colorDarkGrey
                label.font = UIFont(name: "PingFangTC-Regular", size: 12)
                label.text = "notice_latest".localized
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "NoticeCell", for: indexPath)
                decorate(cell, position: indexPath)
            }
            
        case .before:
            if indexPath.row == 0 {
                cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath)
                cell.selectionStyle = .none
                cell.backgroundColor = UiUtility.colorBackgroundWhite
                
                let label: UILabel = cell.viewWithTag(1) as! UILabel
                label.textColor = UiUtility.colorDarkGrey
                label.font = UIFont(name: "PingFangTC-Regular", size: 12)
                label.text = "notice_before".localized
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "NoticeCell", for: indexPath)
                decorate(cell, position: indexPath)
            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        Logger.d("You tapped cell number \(indexPath.row).")
        tableView.deselectRow(at: indexPath, animated: true)
        
//        Logger.d("click notice at row \(String(describing: indexPath.row))")
        let notice: Notice = getNotice(of: indexPath)
        Logger.d("notice: \(notice)")
        
        setReadIfNeeded(to: notice)
        
        switch NoticeKind.init(rawValue: notice.kind)! {
        case .follow, .suggestedFriend:
            UiUtility.goBrowse(profile: Profile.init(with: notice), controller: self)
        case .like, .officialActivity, .officialNews, .newPost, .comment, .postComment:
            let news = News.init(with: notice)
            NewsManager.shared().getNews(news: news, noticeKind: nil, listener: self)
        case .clubComment, .clubPostComment, .newClubPost:
            let clubPost = ClubPost.init(with: notice.newsID!)
            ClubManager.shared().getPost(post: clubPost, listener: self)
        case .redeem:
            UiUtility.goBrowseReward(controller: self)
        case .inBox:
            goInboxContent(id: notice.newsID!, noticeKind: nil)
        case .normalSurvey:
            goInboxContent(id: notice.newsID!, noticeKind: .normalSurvey)
        case .newCarsSurvey:
            goInboxContent(id: notice.newsID!, noticeKind: .newCarsSurvey)
        case .bodyAndPaint:
            goInboxContent(id: notice.newsID!, noticeKind: .bodyAndPaint)
        case .recall:
            UserManager.shared().showNoticeBoard = nil
            goInitialPage(success: true)
        case .drivingAssistant:
            DeeplinkNavigator.shared.proceedToDeeplink(.DrivingAssistant, param: nil)
        }
    }
    
    func goInboxContent(id: Int, noticeKind: NoticeKind?) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedDetailViewController") as! FeedDetailViewController
        vc.noticeKind = noticeKind
        vc.inboxId = id
        vc.isFull = true
        vc.isInboxContent = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: news protocol
    
    override func didStartGetNews() {
        showHud()
    }
    
    override func didFinishGetNews(success: Bool, news: News?, noticeKind: NoticeKind?) {
        dismissHud()
        if success {
//            Logger.d("get news: \(news)")
            if news?.newsId == nil {
                let alertController = UIAlertController(title: nil, message: "post_has_deleted".localized, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "got_it".localized, style: .default) { (action:UIAlertAction) in
                    Logger.d("do nothing")
                }
                
                alertController.addAction(action1)
                present(alertController, animated: true, completion: nil)
            } else {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedDetailViewController") as! FeedDetailViewController
                vc.noticeKind = noticeKind
                vc.news = news
                vc.isFull = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            Logger.d("failed to get news")
        }
    }
    
    
    
    // MARK: club protocol
    
    override func didFinishGetClubPost(success:Bool, post: ClubPost?) {
        dismissHud()
        if success {
            Logger.d("get club post: \(post)")
            if post?.newsId == nil {
                let alertController = UIAlertController(title: nil, message: "post_has_deleted".localized, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "got_it".localized, style: .default) { (action:UIAlertAction) in
                    Logger.d("do nothing")
                }
                
                alertController.addAction(action1)
                present(alertController, animated: true, completion: nil)
            } else {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedDetailViewController") as! FeedDetailViewController
                vc.clubPost = post
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            Logger.d("failed to get club post")
        }
    }
    
    
    
    private func getNotice(of position: IndexPath) -> Notice {
        switch Section.init(rawValue: position.section)! {
        case .latest:
            return notices[position.row - 1]
        case .before:
            return histories[position.row - 1]
        }
    }
    
    private func decorate(_ cell: UITableViewCell, position: IndexPath) {
        for constraint in cell.contentView.constraints {
            if constraint.identifier == "avatarTop" {
                constraint.constant = UiUtility.adaptiveSize(size: 16)
            }
        }
        
        for constraint in cell.contentView.constraints {
            if constraint.identifier == "dateBottom" {
                constraint.constant = UiUtility.adaptiveSize(size: 10)
            }
        }
        
        let imageViewAvatar: UIImageView = cell.viewWithTag(1) as! UIImageView
        let imageViewCheck: UIImageView = cell.viewWithTag(2) as! UIImageView
        let labelContent: UILabel = cell.viewWithTag(3) as! UILabel
        let labelDate: UILabel = cell.viewWithTag(4) as! UILabel
        
        imageViewAvatar.backgroundColor = UiUtility.colorBackgroundWhite
        imageViewAvatar.layer.masksToBounds = true
        imageViewAvatar.layer.cornerRadius = UiUtility.adaptiveSize(size: 20)
        imageViewAvatar.layer.borderColor = UIColor.white.cgColor
        imageViewAvatar.layer.borderWidth = UiUtility.adaptiveSize(size: 1)
        
        let notice = getNotice(of: position)
        if notice.isRead() {
            cell.backgroundColor = UIColor.white
        } else {
            cell.backgroundColor = UiUtility.colorBrownGrey.withAlphaComponent(0.2)
        }
        
        if let stickerURL = notice.stickerURL {
            let url = URL(string: stickerURL)
            imageViewAvatar.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_photo"))
        }
        
        imageViewCheck.isHidden = true
//        if let level = notice.level {
//            if level > 2 {
//                imageViewCheck.isHidden = false
//                imageViewCheck.image = UIImage.init(named: "check_golden_large")
//            } else if level > 1 {
//                imageViewCheck.isHidden = false
//                imageViewCheck.image = UIImage.init(named: "check_silver_large")
//            } else if level > 0 {
//                imageViewCheck.isHidden = false
//                imageViewCheck.image = UIImage.init(named: "check_large")
//            }
//        }
        
        labelContent.textColor = UIColor(rgb: 0x4A4A4A)
        labelContent.font = UIFont(name: "PingFangTC-Regular", size: 14)
        labelContent.text = notice.content
        
        if tabSelectedIndex == 1 {
            if let nn = notice.nickname {
                if let nc = notice.content {
                    let muStr = NSMutableAttributedString().bold(nn).normal("\(nc)")
                    labelContent.attributedText = muStr
                } else {
                    let muStr = NSMutableAttributedString().bold(nn)
                    labelContent.attributedText = muStr
                }
            } else {
                labelContent.text = notice.content
            }
        }
        
        labelDate.textColor = UiUtility.colorBrownGrey
        labelDate.font = UIFont(name: "PingFangTC-Medium", size: 10)
        labelDate.text = notice.getDateText()
        
        let button: UIButton = cell.viewWithTag(5) as! UIButton
        button.addTarget(self, action: #selector(avatarClicked(_:)), for: .touchUpInside)
    }
    
    @objc private func avatarClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
//            Logger.d("click avatar at row \(String(describing: indexPath.row))")
            let notice: Notice = getNotice(of: indexPath)
//            Logger.d("notice: \(notice)")
            setReadIfNeeded(to: notice)
            UiUtility.goBrowse(profile: Profile.init(with: notice), controller: self)
        }
    }
    
    private func setReadIfNeeded(to notice: Notice) {
        if !notice.isRead() {
            NoticeManager.shared().setRead(notice: notice, listener: self)
        }
    }
}
