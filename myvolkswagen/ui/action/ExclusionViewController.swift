//
//  ExclusionViewController.swift
//  myvolkswagen
//
//  Created by Apple on 3/6/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import TTTAttributedLabel

class ExclusionViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource, TTTAttributedLabelDelegate {
    
    enum Section: Int {
        case padding = 0, content
        static var allCases: [Section] {
            var values: [Section] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UiUtility.colorBackgroundWhite
        tableView.backgroundColor = UiUtility.colorBackgroundWhite
        
        naviBar.setTitle("exclusion_problem".localized)
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return 8
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UiUtility.adaptiveSize(size: 25)
        } else {
            switch indexPath.row {
            case 0, 1, 2, 3, 4, 7:
                let width = UiUtility.adaptiveSize(size: 375 - 73 - 50)
                let fontTitle = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
                let fontValue = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))!
//                let minContentHeight: CGFloat = UiUtility.adaptiveSize(size: 150)
                
                let title: String = "exclusion_row_\(indexPath.row)_title".localized
                let value: String = "exclusion_row_\(indexPath.row)_value".localized
                
                var h: CGFloat = 0
                h += title.height(withConstrainedWidth: width, font: fontTitle)
                h += value.height(withConstrainedWidth: width, font: fontValue)
                h += UiUtility.adaptiveSize(size: 20)
                
                return h
            case 5:
                let width = UiUtility.adaptiveSize(size: 375 - 73 - 50)
                let fontTitle = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
                let fontValue = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))!
                //                let minContentHeight: CGFloat = UiUtility.adaptiveSize(size: 150)
                
                let title: String = "exclusion_row_\(indexPath.row)_title".localized
                let value0: String = "exclusion_row_\(indexPath.row)_value_0".localized
                let value1: String = "exclusion_row_\(indexPath.row)_value_1".localized
                
                var h: CGFloat = 0
                h += title.height(withConstrainedWidth: width, font: fontTitle)
                h += value0.height(withConstrainedWidth: width, font: fontTitle)
                h += value1.height(withConstrainedWidth: width, font: fontValue)
                h += UiUtility.adaptiveSize(size: 20)
                
                return h
            case 6:
                let width = UiUtility.adaptiveSize(size: 375 - 73 - 50)
                let fontTitle = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
                let fontValue = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))!
                //                let minContentHeight: CGFloat = UiUtility.adaptiveSize(size: 150)
                
                let title: String = "exclusion_row_\(indexPath.row)_title".localized
                let value0: String = "exclusion_row_\(indexPath.row)_value_0".localized
                let value1: String = "exclusion_row_\(indexPath.row)_value_1".localized
                let value2: String = "exclusion_row_\(indexPath.row)_value_2".localized
                
                var h: CGFloat = 0
                h += title.height(withConstrainedWidth: width, font: fontTitle)
                h += "\(value0)\(value1)\(value2)".height(withConstrainedWidth: width, font: fontValue)
                h += UiUtility.adaptiveSize(size: 20)
                
                return h
            default:
                return 0
            }
        }
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath)
            cell.selectionStyle = .none
        } else {
            switch indexPath.row {
            case 0, 1, 2, 3, 4, 7:
                cell = tableView.dequeueReusableCell(withIdentifier: "Row0Cell", for: indexPath)
                cell.selectionStyle = .none
                
                let index: UILabel = cell.viewWithTag(1) as! UILabel
                let title: UILabel = cell.viewWithTag(2) as! UILabel
                let value: UILabel = cell.viewWithTag(3) as! UILabel
                
                index.textColor = UIColor.black
                index.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
                index.text = "\(indexPath.row + 1)"
                title.textColor = UIColor.black
                title.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
                title.text = "exclusion_row_\(indexPath.row)_title".localized
                value.textColor = UIColor(rgb: 0x4A4A4A)
                value.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
                value.text = "exclusion_row_\(indexPath.row)_value".localized
            case 5:
                cell = tableView.dequeueReusableCell(withIdentifier: "Row1Cell", for: indexPath)
                cell.selectionStyle = .none
                
                let index: UILabel = cell.viewWithTag(1) as! UILabel
                let title: UILabel = cell.viewWithTag(2) as! UILabel
                let value: UILabel = cell.viewWithTag(3) as! UILabel
                let button: UIButton = cell.viewWithTag(4) as! UIButton
                
                index.textColor = UIColor.black
                index.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
                index.text = "\(indexPath.row + 1)"
                title.textColor = UIColor.black
                title.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
                title.text = "exclusion_row_\(indexPath.row)_title".localized
                value.textColor = UIColor(rgb: 0x4A4A4A)
                value.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
                value.text = "exclusion_row_\(indexPath.row)_value_1".localized
                
                button.setTitleColor(UiUtility.colorPinkyRed, for: .normal)
                button.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
                button.setTitle("exclusion_row_\(indexPath.row)_value_0".localized, for: .normal)
                button.contentEdgeInsets = UIEdgeInsets(top: 0.01, left: 0, bottom: 0.01, right: 0)
                button.addTarget(self, action: #selector(callPolice(_:)), for: .touchUpInside)
            case 6:
                cell = tableView.dequeueReusableCell(withIdentifier: "Row0Cell", for: indexPath)
                cell.selectionStyle = .none
                
                let index: UILabel = cell.viewWithTag(1) as! UILabel
                let title: UILabel = cell.viewWithTag(2) as! UILabel
                let value: TTTAttributedLabel = cell.viewWithTag(3) as! TTTAttributedLabel
                
                index.textColor = UIColor.black
                index.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
                index.text = "\(indexPath.row + 1)"
                title.textColor = UIColor.black
                title.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
                title.text = "exclusion_row_\(indexPath.row)_title".localized
                value.textColor = UIColor(rgb: 0x4A4A4A)
                value.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
                
                let value0: String = "exclusion_row_\(indexPath.row)_value_0".localized
                let value1: String = "exclusion_row_\(indexPath.row)_value_1".localized
                let value2: String = "exclusion_row_\(indexPath.row)_value_2".localized
                
                let attributedString = NSMutableAttributedString(string: "\(value0)\(value1)\(value2)", attributes: [
//                    NSAttributedString.Key.paragraphStyle: paragraphStyle,
                    NSAttributedString.Key.foregroundColor: UIColor(rgb: 0x4A4A4A).cgColor,
                    NSAttributedString.Key.font: UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))!
                    ])
                attributedString.addAttribute(.link, value: "https://www.hackingwithswift.com", range: NSRange(location: value0.count, length: value1.count))
                attributedString.addAttribute(.foregroundColor, value: UiUtility.colorPinkyRed, range: NSRange(location: value0.count, length: value1.count))
                attributedString.addAttribute(.font, value: UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!, range: NSRange(location: value0.count, length: value1.count))
                
//                value.text = "\(value0)\(value1)\(value2)"
                let ppLinkAttributes: [String: Any] = [
                    NSAttributedString.Key.foregroundColor.rawValue: UiUtility.colorPinkyRed.cgColor,
                    NSAttributedString.Key.underlineStyle.rawValue: false,
                    ]
                value.linkAttributes = ppLinkAttributes
                value.attributedText = attributedString
                
                let urlPP = URL(string: "action://PP")!
                value.addLink(to: urlPP, with: NSRange(location: value0.count, length: value1.count))
                value.delegate = self
            default:
                break
            }
        }
        
        cell.backgroundColor = UiUtility.colorBackgroundWhite
        
        return cell
    }
    
    
    
    // MARK: TTTAttributedLabelDelegate
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        if url.absoluteString == "action://PP" {
            UiUtility.callService(controller: self)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    @objc private func callPolice(_ sender: UIButton) {
        let alertController = UIAlertController(title: "call_police".localized, message: "police_phone_number".localized, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "confirm".localized, style: .default) { (action:UIAlertAction) in
            Utility.call(number: Config.numberPolice)
        }
        
        let action2 = UIAlertAction(title: "forget_it".localized, style: .cancel) { (action:UIAlertAction) in
            Logger.d("You've pressed cancel");
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        present(alertController, animated: true, completion: nil)
    }
}
