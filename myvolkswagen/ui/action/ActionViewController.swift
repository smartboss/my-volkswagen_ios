//
//  ActionViewController.swift
//  myvolkswagen
//
//  Created by Apple on 2/1/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Firebase

class ActionViewController: NaviViewController {
    
    @IBOutlet weak var tabImage: UIImageView!
    @IBOutlet weak var tab0: UIView!
    @IBOutlet weak var tab1: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UiUtility.colorBackgroundWhite
        tab0.backgroundColor = UiUtility.colorBackgroundWhite
        tab1.backgroundColor = UiUtility.colorBackgroundWhite
        
        tab1.isHidden = true
        
        naviBar.setTitle("action".localized)
        naviBar.setLeftButton(UIImage(named: "vw"), highlighted: UIImage(named: "vw"))
        naviBar.setRightButton(UIImage(named: "notif_black"), highlighted: UIImage(named: "notif_black_unread"))
        imageViewNotification = naviBar.imageViewRight
        
        addFloatingBtnView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        // update notice read/unread status locally
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            self.setNoticeImage(with: readStatus)
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        self.tabControllerDelegate?.menuClicked()
    }
    
    override func rightButtonClicked(_ button: UIButton) {
        self.tabControllerDelegate?.notificationClicked()
    }
    
    override func updateUnreadCount(notification: NSNotification) {
//        Logger.d("update notice read/unread status locally")
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            self.setNoticeImage(with: readStatus)
        }
    }

    
    
    
    @IBAction private func tab0Clicked(_ sender: Any) {
        tabImage.isHighlighted = false
        
        tab0.isHidden = false
        tab1.isHidden = true
    }
    
    @IBAction private func tab1Clicked(_ sender: Any) {
        tabImage.isHighlighted = true
        
        tab0.isHidden = true
        tab1.isHidden = false
    }
    
    @IBAction private func parkingLotClicked(_ sender: Any) {
        Analytics.logEvent("tool_parking", parameters: nil)
        let queryParams: [String: String] = [
            "api": "1",
            "query": "parking_lot".localized,
            "center": "0,0,12z"
        ]
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.google.com"
        urlComponents.path = "/maps/search/"
        urlComponents.setQueryItems(with: queryParams)
//        Logger.d(urlComponents.url?.absoluteString)
        UIApplication.shared.open(urlComponents.url!)
    }
    
    @IBAction private func realTimeRoadInfoClicked(_ sender: Any) {
        tabControllerDelegate?.goRoadInfo()
    }
    
    @IBAction private func gasolineStationClicked(_ sender: Any) {
        Analytics.logEvent("tool_gas", parameters: nil)
        let queryParams: [String: String] = [
            "api": "1",
            "query": "gasoline_station".localized,
            "center": "0,0,12z"
        ]
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.google.com"
        urlComponents.path = "/maps/search/"
        urlComponents.setQueryItems(with: queryParams)
//        Logger.d(urlComponents.url?.absoluteString)
        UIApplication.shared.open(urlComponents.url!)
    }
    
    @IBAction private func nearbyServiceCenterClicked(_ sender: Any) {
        Analytics.logEvent("tool_vw_center", parameters: nil)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.nearbyServiceCenter
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction private func rescueLineClicked(_ sender: Any) {
        Analytics.logEvent("tool_help_call", parameters: nil)
        UiUtility.callService(controller: self)
    }
    
    @IBAction private func troubleshootingClicked(_ sender: Any) {
        Analytics.logEvent("tool_sop", parameters: nil)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ExclusionViewController") as! ExclusionViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction private func etagClicked(_ sender: Any) {
        Analytics.logEvent("tool_etag", parameters: nil)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.eTag
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction private func supervisionStationClicked(_ sender: Any) {
        Analytics.logEvent("tool_bill", parameters: nil)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.supervisionStation
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension URLComponents {
    
    mutating func setQueryItems(with parameters: [String: String]) {
        self.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
    }
}
