//
//  UiUtility.swift
//  badminton
//
//  Created by Apple on 9/24/18.
//  Copyright © 2018 sofasogood. All rights reserved.
//

import UIKit
import PhotosUI
import Kingfisher
import SafariServices
import Alamofire
import SwiftyJSON

enum Visibility: Int {
    case PUBLIC = 1, PRIVATE = 2
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int, a: CGFloat = 1.0) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: a)
    }
    
    convenience init(rgb: Int, a: CGFloat = 1.0) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF,
            a: a
        )
    }
}

class UiUtility: NSObject {
    // MARK: - colors
    
    static let colorBackgroundWhite = UIColor(rgb: 0xF7F7F7) // 247, 247, 247
    static let colorDarkGrey = UIColor(rgb: 0x333333) // 51, 51, 51
    static let colorBrownGrey = UIColor(rgb: 0x9B9B9B) // 155, 155, 155
    static let colorPinkyRed = UIColor(rgb: 0xEA2B40) // 234, 43, 64
    
    static let colorBackgroundIncomingMessage = UIColor(rgb: 0xEEEEEE) // 238, 238, 238
    static let colorBackgroundOutgoingMessage = UIColor(rgb: 0x3E84D7) // 62, 132, 215
    
    static let colorDark4A = UIColor(rgb: 0x4A4A4A) // 74, 74, 74
    
    
    

    // MARK: - Image View
    
    static func makeCircular(image: UIImageView, radius: CGFloat = -1.0) {
        image.layer.masksToBounds = false
        if (radius < 0) {
            image.layer.cornerRadius = image.frame.height / 2.0
        } else {
            image.layer.cornerRadius = radius
        }
        image.clipsToBounds = true
    }
    
    
    
    // MARK: - device related
    
    static func getScreenWidth() -> CGFloat {
        return UIScreen.main.bounds.width
    }
    
    static func getScreenHeight() -> CGFloat {
        return UIScreen.main.bounds.height
    }
    
    static func adaptiveSize(size: CGFloat) -> CGFloat {
        return size * self.getScreenWidth() / 375.0
    }
    
    
    
    // MARK: qrcode
    static func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    
    
    // MARK: image
    
    static func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {

        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize.init(width: newWidth, height: newHeight))
        image.draw(in: CGRect.init(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
    
    
    
    // MARK: alert
    
    static func callService(controller: UIViewController) {
        let alertController = UIAlertController(title: "title_call_rescue".localized, message: "msg_call_rescue".localized, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "confirm".localized, style: .default) { (action:UIAlertAction) in
            Utility.call(number: Config.numberRescue)
        }
        
        let action2 = UIAlertAction(title: "forget_it".localized, style: .cancel) { (action:UIAlertAction) in
            Logger.d("You've pressed cancel");
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    static func sortClub(controller: BaseViewController, hasClub: Bool) {
        let alertController = UIAlertController(title: "club_sort_title".localized, message: nil, preferredStyle: .actionSheet)
        
        let actionBrowse = UIAlertAction(title: "club_sort_browse".localized, style: .default) { (action:UIAlertAction) in
            
        }
        
        let actionLatestPost = UIAlertAction(title: "club_sort_latest_post".localized, style: .default) { (action:UIAlertAction) in
            controller.setSort(type: .latestPost)
        }
        
        let actionLatestJoin = UIAlertAction(title: "club_sort_latest_join".localized, style: .default) { (action:UIAlertAction) in
            
        }
        
        let actionMember = UIAlertAction(title: "club_sort_member".localized, style: .default) { (action:UIAlertAction) in
            controller.setSort(type: .member)
        }
        
        let actionLatestClub = UIAlertAction(title: "club_sort_latest_club".localized, style: .default) { (action:UIAlertAction) in
            controller.setSort(type: .latestClub)
        }
        
        let actionCancel = UIAlertAction(title: "cancel".localized, style: .cancel) { (action:UIAlertAction) in
            
        }
        
        if hasClub {
            alertController.addAction(actionBrowse)
            alertController.addAction(actionLatestPost)
            alertController.addAction(actionLatestJoin)
        } else {
            alertController.addAction(actionLatestClub)
            alertController.addAction(actionLatestPost)
        }
        
        alertController.addAction(actionMember)
        alertController.addAction(actionCancel)
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    
    
    // MARK: pick image
    
    static func pickImage<T: UINavigationControllerDelegate & UIImagePickerControllerDelegate>(from controller: UIViewController, delegate: T, allowsEditing: Bool) {
        
        let alert = UIAlertController(title: "choose_image".localized, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "camera".localized, style: .default, handler: { _ in
            DispatchQueue.main.async {
            presentCamera(from: controller, delegate: delegate)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "gallery".localized, style: .default, handler: { _ in
            DispatchQueue.main.async {
            presentImagePicker(from: controller, delegate: delegate, allowsEditing: allowsEditing)
            }
        }))
        
        alert.addAction(UIAlertAction.init(title: "cancel".localized, style: .cancel, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
    
    
    // MARK: club post sheet
    
    static func clubMoreMenu(from controller: NaviViewController, post: ClubPost, clubProtocol: ClubProtocol?, clubUpdateDelegate: ClubUpdateDelegate?) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        Logger.d("post: \(post)")
//        var isMember: Bool = false
        var isAdmin: Bool = false
        
        if let joinedClubs = ClubManager.shared().getClubData(remotely: false, listener: nil)?.clubs {
            for club in joinedClubs {
                if club.id == post.clubId {
                    isAdmin = club.isManager()
//                    isMember = club.isMember()
                }
            }
        }
        
        let actionShare = UIAlertAction(title: "share_to".localized, style: .default, handler: { _ in
            if let photoList = post.photoList {
                if let url = URL(string: photoList[0]) {
                    KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                        if let img = image {
                            Utility.shareAll(text: post.content, image: img, controller: controller)
                        }
                    })
                }
            }
        })
        
        let actionPin = UIAlertAction(title: "club_post_more_menu_pin".localized, style: .default, handler: { _ in
            ClubManager.shared().setTop(post: post, set: true, listener: clubProtocol)
        })
        
        let actionUnpin = UIAlertAction(title: "club_post_more_menu_unpin".localized, style: .default, handler: { _ in
            ClubManager.shared().setTop(post: post, set: false, listener: clubProtocol)
        })
        
        let actionEdit = UIAlertAction(title: "edit_post".localized, style: .default, handler: { _ in
            goEditClubPost(from: controller, post: Post.init(with: post), clubId: post.clubId, clubUpdateDelegate: clubUpdateDelegate)
        })
        
        let actionDelete = UIAlertAction(title: "delete_post".localized, style: .destructive, handler: { _ in
            let alertController = UIAlertController(title: nil, message: "warning_delete_post".localized, preferredStyle: .alert)

            let action2 = UIAlertAction(title: "forget_it".localized, style: .cancel) { (action:UIAlertAction) in
                Logger.d("You've pressed cancel");
            }

            let action3 = UIAlertAction(title: "delete".localized, style: .destructive) { (action:UIAlertAction) in
                ClubManager.shared().delete(post: post, listener: clubProtocol)
            }

            alertController.addAction(action2)
            alertController.addAction(action3)
            controller.present(alertController, animated: true, completion: nil)
        })
        
        if isAdmin {
            alert.addAction(post.isTopping() ? actionUnpin : actionPin)
        }
        alert.addAction(actionShare)
        if post.isMine() {
            alert.addAction(actionEdit)
            alert.addAction(actionDelete)
        }
        
        alert.addAction(UIAlertAction.init(title: "cancel".localized, style: .cancel, handler: nil))
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    
    
    // MARK: news feed sheet
    
    static func moreMenu(from controller: NaviViewController, type: Int, news: News, contentDelegate: ContentDelegate) {
        // type 0: official
        // type 1: mine
        // type 2: other's
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "share_to".localized, style: .default, handler: { _ in
            if let photoList = news.photoList {
                if let url = URL(string: photoList[0]) {
                    KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                        if let img = image {
                            Utility.shareAll(text: news.content, image: img, controller: controller)
                        }
                    })
                }
            }
        }))
        
        if type == 0 {
            
        } else if type == 1 {
            alert.addAction(UIAlertAction(title: "edit_post".localized, style: .default, handler: { _ in
                goEditPost(from: controller, post: Post.init(with: news), contentDelegate: contentDelegate, photoSourceType: .none)
            }))
            
            alert.addAction(UIAlertAction(title: "delete_post".localized, style: .destructive, handler: { _ in
                let alertController = UIAlertController(title: nil, message: "warning_delete_post".localized, preferredStyle: .alert)
                
                let action2 = UIAlertAction(title: "forget_it".localized, style: .cancel) { (action:UIAlertAction) in
                    Logger.d("You've pressed cancel");
                }
        
                let action3 = UIAlertAction(title: "delete".localized, style: .destructive) { (action:UIAlertAction) in
                    NewsManager.shared().delete(news: news, listener: controller)
                }
                
                alertController.addAction(action2)
                alertController.addAction(action3)
                controller.present(alertController, animated: true, completion: nil)
            }))
        } else if type == 2 {
            alert.addAction(UIAlertAction(title: "report_post".localized, style: .destructive, handler: { _ in
                reportMenu(from: controller, news: news)
            }))
        }
        
        alert.addAction(UIAlertAction.init(title: "cancel".localized, style: .cancel, handler: nil))
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    static func reportMenu(from controller: BaseViewController, news: News) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "garbage_post".localized, style: .destructive, handler: { _ in
            NewsManager.shared().report(news: news, type: 1, listener: controller)
        }))
        
        alert.addAction(UIAlertAction(title: "unproper_post".localized, style: .destructive, handler: { _ in
            NewsManager.shared().report(news: news, type: 2, listener: controller)
        }))
        
        alert.addAction(UIAlertAction.init(title: "cancel".localized, style: .cancel, handler: nil))
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    
    
    // MARK: request permissions
    
    static func presentImagePicker<T: UINavigationControllerDelegate & UIImagePickerControllerDelegate>(from controller: UIViewController, delegate: T, allowsEditing: Bool) {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            Logger.d("Access is granted by user")
            
            let picker = UIImagePickerController()
            picker.allowsEditing = allowsEditing
            picker.delegate = delegate
            controller.present(picker, animated: true)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (newStatus) in Logger.d("status is \(newStatus)")
                if newStatus == PHAuthorizationStatus.authorized {
                    Logger.d("success")
                    DispatchQueue.main.async {
                        let picker = UIImagePickerController()
                        picker.allowsEditing = allowsEditing
                        picker.delegate = delegate
                        controller.present(picker, animated: true)
                    }
                }
            })
        case .restricted:
            Logger.d("User do not have access to photo album.")
        case .denied:
            Logger.d("User has denied the permission.")
            UiUtility.showPhotoLibraryDeniedWarning(controller)
        case .limited:
            Logger.d("User do not have access to photo album.")
        }
    }
    
    static func presentCamera<T: UINavigationControllerDelegate & UIImagePickerControllerDelegate>(from controller: UIViewController, delegate: T) {
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
        switch cameraAuthorizationStatus {
        case .authorized:
            Logger.d("The user has previously granted access to the camera.")
            
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.allowsEditing = true
            picker.delegate = delegate
            controller.present(picker, animated: true)
        case .notDetermined:
            Logger.d("The user has not yet been asked for camera access.")
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    Logger.d("camera granted")
                    DispatchQueue.main.async {
                        let picker = UIImagePickerController()
                        picker.sourceType = .camera
                        picker.allowsEditing = true
                        picker.delegate = delegate
                        controller.present(picker, animated: true)
                    }
                }
            }
        case .restricted:
            Logger.d("The user can't grant access due to restrictions.")
        case .denied:
            Logger.d("The user has previously denied access.")
            UiUtility.showCameraDeniedWarning(controller)
        }
    }
    
    
    
    static func goEditPost(from controller: NaviViewController, post: Post?, contentDelegate: ContentDelegate?, photoSourceType: PhotoSourceType) {
        if let tabControllerDelegate = controller.tabControllerDelegate {
            tabControllerDelegate.goEditPost(post: post, contentDelegate: contentDelegate, photoSourceType: photoSourceType)
        } else {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditPostViewController") as! EditPostViewController
            vc.editingPost = post
            vc.contentDelegate = contentDelegate
            vc.photoSourceType = photoSourceType
            controller.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    static func goEditPhotos(from controller: NaviViewController, post: Post?, contentDelegate: ContentDelegate?) {
        if let tabControllerDelegate = controller.tabControllerDelegate {
            tabControllerDelegate.goEditPhotos(post: post, contentDelegate: contentDelegate)
        } else {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditPhotosViewController") as! EditPhotosViewController
            vc.editingPost = post
            vc.contentDelegate = contentDelegate
            controller.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    static func goEditClubPost(from controller: NaviViewController, post: Post?, clubId: Int, clubUpdateDelegate: ClubUpdateDelegate?) {
        if let tabControllerDelegate = controller.tabControllerDelegate {
            tabControllerDelegate.goEditClubPost(clubId: clubId, post: post, clubUpdateDelegate: clubUpdateDelegate)
        } else {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditPostViewController") as! EditPostViewController
            vc.editingPost = post
            vc.contentDelegate = nil
            vc.mode = .club
            vc.clubId = clubId
            vc.clubUpdateDelegate = clubUpdateDelegate
            controller.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    static func goBrowse(profile: Profile, controller: NaviViewController) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PersonViewController") as! PersonViewController
        vc.tabControllerDelegate = controller.tabControllerDelegate
        vc.profile = profile
        controller.navigationController?.pushViewController(vc, animated: true)
    }
    
    static func goBrowseLikers(of news: News, controller: UIViewController) {
        if news.likeAmount != 0 {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LikeViewController") as! LikeViewController
            vc.news = news
            controller.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    static func goBrowseFollow(of profile: Profile, isFollower: Bool, controller: UIViewController) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FollowViewController") as! FollowViewController
        vc.isFollower = isFollower
        vc.of = profile
        controller.navigationController?.pushViewController(vc, animated: true)
    }
    
    static func goBrowseClubPostLikers(of post: ClubPost, controller: UIViewController) {
        if post.likeAmount != 0 {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LikeViewController") as! LikeViewController
            vc.post = post
            controller.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    static func goBrowseReward(controller: UIViewController) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let mController = appDelegate.window?.rootViewController as? UINavigationController {
            if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                mainVC.goDeeplink(deeplink: .MemberCoupon, param: "type=2")
            }
        }
    }
    
    static func goBrowsePoint(controller: UIViewController) {
        let vc = UIStoryboard(name: "Redeem", bundle: nil).instantiateViewController(withIdentifier: "redeemVC") as! RedeemViewController
        vc.selectedIndex = 2
        controller.navigationController?.pushViewController(vc, animated: true)
    }
    
    static func goBrowseMemberLevel(controller: UIViewController) {
        let vc = UIStoryboard(name: "Redeem", bundle: nil).instantiateViewController(withIdentifier: "redeemVC") as! RedeemViewController
        vc.selectedIndex = 0
        controller.navigationController?.pushViewController(vc, animated: true)
    }
    
    static func goBrowseWebview(url: URL, notiKind: WebsiteIndex, controller: UIViewController) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        if notiKind == .normalSurvey {
            vc.surveyUrl = url
        } else if notiKind == .newCarsSurvey {
            vc.surveyUrl = url
        } else if notiKind == .BnP {
            vc.loadUrl = url
        } else if notiKind == .memberPointList {
            vc.loadUrl = url
        } else if notiKind == .serviceCam {
            vc.loadUrl = url
        }
        vc.index = notiKind
        controller.navigationController?.pushViewController(vc, animated: true)
    }
    
    static func goReservation() {
        let mycars = CarManager.shared().getCars(remotely: true, listener: nil)
        if mycars.count == 0 {
            self.getCars()
            return
        }
        let accId = UserManager.shared().currentUser.accountId!
        let licNum = mycars[0].licensePlateNumber!
        let astString = "AccountId=\(accId)&PlateNum=\(licNum)"
        let astStringEncode = astString.toBase64URL()
        let astUrlEncode = URL(string: "\(Config.urlMaintenanceReservation)?\(astStringEncode)")!
        if #available(iOS 14.5, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.index = .loadUrl
            vc.loadUrl = astUrlEncode
            vc.mytitle = "預約保養"
            if let drawerController = UIApplication.shared.keyWindow?.rootViewController as? DrawerController {
                let naviViewController: UINavigationController = drawerController.rootViewController as UINavigationController
                naviViewController.pushViewController(vc, animated: true)
            } else if let naviViewController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                naviViewController.pushViewController(vc, animated: true)
            }
        } else if #available(iOS 13.0, *) {
            let url = astUrlEncode
            let safari = SFSafariViewController(url: url)
            if let drawerController = UIApplication.shared.keyWindow?.rootViewController as? DrawerController {
                let naviViewController: UINavigationController = drawerController.rootViewController as UINavigationController
                //            let viewController: BaseViewController = naviViewController.viewControllers.last as! BaseViewController
                naviViewController.pushViewController(safari, animated: true)
            } else if let naviViewController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                naviViewController.pushViewController(safari, animated: true)
            }
        } else {
            UIApplication.shared.open(astUrlEncode)
        }
    }
    
    static func getCars() {
        Alamofire.request(Config.urlMemberServer + "APP_Get_MyCar_Data?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                    if result.isSuccess() {
                        if let data = responseJson["datas"].array {
                            CarManager.shared().cars = []
                            for datum in data {
                                let car: Car = Car(with: datum)
//                                    Logger.d("load car from server response: \(car)")
                                CarManager.shared().cars.append(car)
                            }
//                                Logger.d("get \(self.cars.count) cars")
                        }
                        
                        self.didFinishGetCar(success: true, cars: CarManager.shared().cars)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        self.didFinishGetCar(success: false, cars: CarManager.shared().cars)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    self.didFinishGetCar(success: false, cars: CarManager.shared().cars)
                })
                
        }
    }
    
    static func didFinishGetCar(success: Bool, cars: [Car]) {
        if cars.count == 0 {
#if DEVELOPMENT
            if Deeplinker.handleDeeplink(url: URL(string: "myvolkswagendev://car")!, ref: nil) {
                Deeplinker.checkDeepLink()
            }
#else
            if Deeplinker.handleDeeplink(url: URL(string: "myvolkswagen://car")!, ref: nil) {
                Deeplinker.checkDeepLink()
            }
#endif
            return
        }
        let accId = UserManager.shared().currentUser.accountId!
        let licNum = cars[0].licensePlateNumber!
        let astString = "AccountId=\(accId)&PlateNum=\(licNum)"
        let astStringEncode = astString.toBase64URL()
        let astUrlEncode = URL(string: "\(Config.urlMaintenanceReservation)?\(astStringEncode)")!
        if #available(iOS 14.5, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.index = .loadUrl
            vc.loadUrl = astUrlEncode
            vc.mytitle = "預約保養"
            if let drawerController = UIApplication.shared.keyWindow?.rootViewController as? DrawerController {
                let naviViewController: UINavigationController = drawerController.rootViewController as UINavigationController
                naviViewController.pushViewController(vc, animated: true)
            } else if let naviViewController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                naviViewController.pushViewController(vc, animated: true)
            }
        } else if #available(iOS 13.0, *) {
            let url = astUrlEncode
            let safari = SFSafariViewController(url: url)
            if let drawerController = UIApplication.shared.keyWindow?.rootViewController as? DrawerController {
                let naviViewController: UINavigationController = drawerController.rootViewController as UINavigationController
                //            let viewController: BaseViewController = naviViewController.viewControllers.last as! BaseViewController
                naviViewController.pushViewController(safari, animated: true)
            } else if let naviViewController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                naviViewController.pushViewController(safari, animated: true)
            }
        } else {
            UIApplication.shared.open(astUrlEncode)
        }
    }
    
    // MARK: text format
//    static func dateText(date: Date?) -> String? {
//        let dateFormatter = DateFormatter()
////        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
//
//        if let date = date {
//            return dateFormatter.string(from: date)
//        }
//
//        return nil
//    }
//
//    static func getCurrencyText(_ c: Double) -> String {
////        let myNumber = NSNumber(value:c)
//
//        let formatter = NumberFormatter()
//        formatter.numberStyle = .currency
////        formatter.locale = Locale(identifier: "en_US")
//        formatter.locale = Locale.current
//        formatter.minimumFractionDigits = 0
//        formatter.maximumFractionDigits = 2
//
//        return formatter.string(from: NSNumber.init(value: c))!
//    }
    
    static func numberStringWithComma(_ number: Int) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        if let str = numberFormatter.string(from: NSNumber(value: number)) {
            return str
        } else {
            return ""
        }
    }
    
    
    
    // MARK: warning
    static func showPhotoLibraryDeniedWarning(_ controller: UIViewController) {
        let alertController = UIAlertController(title: "warning".localized, message: "photo_access_denied_warning".localized, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "got_it".localized, style: .default) { (action:UIAlertAction) in
            Logger.d("do nothing")
        }
        
        alertController.addAction(action1)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    static func showCameraDeniedWarning(_ controller: UIViewController) {
//        let alertController = UIAlertController(title: "warning".localized, message: "camera_access_denied_warning".localized, preferredStyle: .alert)
//        let action1 = UIAlertAction(title: "got_it".localized, style: .default) { (action:UIAlertAction) in
//            Logger.d("do nothing")
//        }
//
//        alertController.addAction(action1)
//        controller.present(alertController, animated: true, completion: nil)
        
        let alertController = UIAlertController(title: "title_camera_permission".localized, message: "msg_camera_permission".localized, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "setting".localized, style: .default) { (action:UIAlertAction) in
            if let url = URL.init(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        
        let action2 = UIAlertAction(title: "forget_it".localized, style: .cancel) { (action:UIAlertAction) in
            Logger.d("do nothing")
        }
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        controller.present(alertController, animated: true, completion: nil)
    }
    
    
    
    static func formatDateText(from serverDateText: String?) -> String? {
        if let pDate = serverDateText {
            let formatter = DateFormatter()
            //            formatter.dateFormat = "yyyy/MM/dd'T'HH:mm:ss"
            formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
            
            // convert your string to date
            if let newsDate: Date = formatter.date(from: pDate) {
                let timeInteval = -newsDate.timeIntervalSinceNow
                //                Logger.d("timeInteval: \(timeInteval)")
                
                if timeInteval >= Utility.secondsInWeek {
                    let d = timeInteval / Utility.secondsInWeek
                    //                    Logger.d("date: \(pDate), \(d), \(String(format: "%.0f", d.rounded(.towardZero)))\("week_before".localized)")
                    //                    return "\(pDate), \(d), \(String(format: "%.0f", d.rounded(.towardZero)))\("week_before".localized)"
                    return "\(String(format: "%.0f", d.rounded(.towardZero)))\("week_before".localized)"
                } else if timeInteval >= Utility.secondsInDay {
                    let d = timeInteval / Utility.secondsInDay
                    //                    Logger.d("date: \(pDate), \(d), \(String(format: "%.0f", d.rounded()))\("day_before".localized)")
                    //                    return "\(pDate), \(d), \(String(format: "%.0f", d.rounded()))\("day_before".localized)"
                    return "\(String(format: "%.0f", d.rounded()))\("day_before".localized)"
                } else if timeInteval >= Utility.secondsInHour {
                    let d = timeInteval / Utility.secondsInHour
                    //                    Logger.d("date: \(pDate), \(d), \(String(format: "%.0f", d.rounded()))\("hour_before".localized)")
                    //                    return "\(pDate), \(d), \(String(format: "%.0f", d.rounded()))\("hour_before".localized)"
                    return "\(String(format: "%.0f", d.rounded()))\("hour_before".localized)"
                } else {
                    let d = timeInteval / Utility.secondsInMinute
                    //                    Logger.d("date: \(pDate), \(d), \(String(format: "%.0f", d.rounded()))\("minute_before".localized)")
                    //                    return "\(pDate), \(d), \(String(format: "%.0f", d.rounded()))\("minute_before".localized)"
                    return "\(String(format: "%.0f", d.rounded()))\("minute_before".localized)"
                }
            }
        }
        
        return nil
    }
    
    
    
    // MARK: car model
    
    static func getModelSelectedColor(of index: Int) -> UIColor {
        var colorIndex: Int = index % 4
        
        switch colorIndex {
        case 0:
            return UIColor(rgb: 0x66BC63)
        case 1:
            return UIColor(rgb: 0xD75778)
        case 2:
            return UIColor(rgb: 0x4C85D1)
        case 3:
            return UIColor(rgb: 0x8863CA)
        default:
            return .white
        }
    }
}


/// Extend UITextView and implemented UITextViewDelegate to listen for changes
extension UITextView: UITextViewDelegate {
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.characters.count > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor = UIColor.black.withAlphaComponent(0.5)
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.characters.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
    
    public func updatePlaceholderVisibility() {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.characters.count > 0
        }
    }
    
}



extension UITableView {
    
    func scrollToBottom() {
        if self.numberOfSections != 0 && self.numberOfRows(inSection: self.numberOfSections - 1) != 0 {
            DispatchQueue.main.async {
                let indexPath = IndexPath(
                    row: self.numberOfRows(inSection: self.numberOfSections - 1) - 1,
                    section: self.numberOfSections - 1)
                self.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
//    func scrollToTop() {
//
//        DispatchQueue.main.async {
//            let indexPath = IndexPath(row: 0, section: 0)
//            self.scrollToRow(at: indexPath, at: .top, animated: false)
//        }
//    }
}

class BasicConstraint: NSLayoutConstraint {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateFontSize()
    }
    
    func updateFontSize() {
        self.constant = self.constant.getFitSize()
    }
}

extension CGFloat {
    func getFitSize() -> CGFloat {
        return self * UiUtility.getScreenWidth() / 375.0
    }
}

class BasicLabel: UILabel {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateFontSize()
    }
    
    func updateFontSize() {
        self.font = self.font.withSize(self.font.pointSize.getFitSize())
    }
}

class CircleView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.height / 2
    }
}

class BasicSizingButton: UIButton {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateFontSize()
        self.clipsToBounds = true
    }
    
    func updateFontSize() {
        self.titleLabel?.font = self.titleLabel?.font.withSize(self.titleLabel!.font!.pointSize.getFitSize())
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        self.layer.cornerRadius = self.frame.size.height / 2
//    }
}


extension NSMutableAttributedString {
    var fontSize:CGFloat { return 14 }
    var boldFont:UIFont { return UIFont(name: "PingFangTC-Bold", size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize) }
    var normalFont:UIFont { return UIFont(name: "PingFangTC-Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)}
    
    func bold(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : boldFont
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func normal(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font : normalFont,
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    /* Other styling methods */
    func orangeHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.orange
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func blackHighlight(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .foregroundColor : UIColor.white,
            .backgroundColor : UIColor.black
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
    
    func underlined(_ value:String) -> NSMutableAttributedString {
        
        let attributes:[NSAttributedString.Key : Any] = [
            .font :  normalFont,
            .underlineStyle : NSUnderlineStyle.single.rawValue
            
        ]
        
        self.append(NSAttributedString(string: value, attributes:attributes))
        return self
    }
}

extension String {
    func htmlAttributedString() -> NSAttributedString? {
        guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil) else { return nil }
        return html
    }
}

extension UIView {
    func addRoundedCorners(topLeft: CGFloat, topRight: CGFloat, bottomLeft: CGFloat, bottomRight: CGFloat) {
        let maskPath = UIBezierPath(
            roundedRect: bounds,
            byRoundingCorners: [.topLeft, .topRight, .bottomLeft, .bottomRight],
            cornerRadii: CGSize(width: topLeft, height: topLeft)
        )
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        layer.mask = maskLayer
    }
}
