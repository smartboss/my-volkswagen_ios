//
//  MainViewControllerV2.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/25.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
import SafariServices
import WebKit

protocol TabControllerDelegateV2: class {
    func addCar()
    func goViewController(view: UIViewController)
    func goGarageVC(cars: [Car])
}

class MainViewControllerV2: BaseViewController, TabControllerDelegateV2, WKNavigationDelegate, WKUIDelegate {
    
    var carViewController: UINavigationController!
    var assistantMainViewController: AssistantMainViewController!
    var settingViewController: SettingViewControllerV2!
    var maintainenceViewController: MaintenanceWebViewController!
    var eshopViewController: WebViewViewController!
    var memberViewController: WebViewViewController!
    var webView: WKWebView!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet var tabButtons: [UIButton]!
    @IBOutlet var tabButtonsView: [UIView]!
    @IBOutlet weak var tab0Btn: UIButton!
    @IBOutlet weak var tab0BtnView: UIView!
    @IBOutlet weak var tab1Btn: UIButton!
    @IBOutlet weak var tab1BtnView: UIView!
    @IBOutlet weak var tab2Btn: UIButton!
    @IBOutlet weak var tab2BtnView: UIView!
    @IBOutlet weak var tab3Btn: UIButton!
    @IBOutlet weak var tab3BtnView: UIView!
    @IBOutlet weak var tab4Btn: UIButton!
    @IBOutlet weak var tab4BtnView: UIView!
    var viewControllers: [UIViewController]!
    var selectedIndex: Int = 0
    var firstSelection: Bool = true
    
    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()
        
        let storyboard = UIStoryboard(name: "Car", bundle: nil)
        carViewController = storyboard.instantiateViewController(withIdentifier: "CarNNavi") as? UINavigationController
        (carViewController.viewControllers[0] as!
         NaviViewController).tabControllerDelegateV2 = self
        let storyboard3 = UIStoryboard(name: "Assistant", bundle: nil)
        assistantMainViewController = storyboard3.instantiateViewController(withIdentifier: "AssistantMainViewController") as? AssistantMainViewController
        assistantMainViewController.tabControllerDelegateV2 = self
        maintainenceViewController = storyboard.instantiateViewController(withIdentifier: "MaintenanceWebViewController") as? MaintenanceWebViewController
        maintainenceViewController.tabControllerDelegateV2 = self
        let storyboard4 = UIStoryboard(name: "Main", bundle: nil)
//        eshopViewController = storyboard4.instantiateViewController(withIdentifier: "WebViewViewController") as? WebViewViewController
//        eshopViewController.tabControllerDelegateV2 = self
//        eshopViewController.index = .eshop
        memberViewController = storyboard4.instantiateViewController(withIdentifier: "WebViewViewController") as? WebViewViewController
        memberViewController.tabControllerDelegateV2 = self
        memberViewController.index = .memberCenterV2
        
        viewControllers = [carViewController, maintainenceViewController, assistantMainViewController, UIViewController(), memberViewController]
        
        tabButtons[selectedIndex].isSelected = true
        setupButtons()
//        tabPressed(tabButtons[selectedIndex])
        goTab(of: selectedIndex, subTab: nil, deeplink: false, param: nil)
    }
    
    func setupButtons() {
        let img1 = tab0BtnView.viewWithTag(1) as! UIImageView
        img1.image = UIImage(named: "homee")?.withRenderingMode(.alwaysTemplate)
        let img2 = tab1BtnView.viewWithTag(1) as! UIImageView
        img2.image = UIImage(named: "wrench")?.withRenderingMode(.alwaysTemplate)
        let img3 = tab2BtnView.viewWithTag(1) as! UIImageView
        img3.image = UIImage(named: "diary")?.withRenderingMode(.alwaysTemplate)
        let img4 = tab3BtnView.viewWithTag(1) as! UIImageView
        img4.image = UIImage(named: "Charge")?.withRenderingMode(.alwaysTemplate)
        let img5 = tab4BtnView.viewWithTag(1) as! UIImageView
        img5.image = UIImage(named: "user")?.withRenderingMode(.alwaysTemplate)
        for (i, _) in tabButtonsView.enumerated() {
            if i == selectedIndex {
                setUpTabbarImageAndWordColor(view: tabButtonsView[selectedIndex], isSelected: true)
            } else {
                setUpTabbarImageAndWordColor(view: tabButtonsView[i], isSelected: false)
            }
        }
        
    }
    
    @IBAction private func tabPressed(_ sender: UIButton) {
        self.goTab(of: sender.tag, subTab: nil, deeplink: false, param: nil)
        
        switch selectedIndex {
        case 0:
            Utility.sendEventLog(ServiceName: "footer_menu", UniqueId: "", EventName: "首頁", InboxId: "", EventCategory: "button", PageName: "footer_menu", EventLabel: "首頁", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CarManager.nameCarReloadNotification), object: nil, userInfo: nil)
        case 1:
            Utility.sendEventLog(ServiceName: "footer_menu", UniqueId: "", EventName: "保養維修", InboxId: "", EventCategory: "button", PageName: "footer_menu", EventLabel: "保養維修", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        case 2:
            print("tab 2 connectivity")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CarManager.nameCarAssistantReloadNotification), object: nil, userInfo: nil)
            Utility.sendEventLog(ServiceName: "footer_menu", UniqueId: "", EventName: "行車助理", InboxId: "", EventCategory: "button", PageName: "footer_menu", EventLabel: "行車助理", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        case 3:
            print("tab 3 openhub")
            Utility.sendEventLog(ServiceName: "footer_menu", UniqueId: "", EventName: "充電地圖", InboxId: "", EventCategory: "button", PageName: "footer_menu", EventLabel: "充電地圖", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        case 4:
            Utility.sendEventLog(ServiceName: "footer_menu", UniqueId: "", EventName: "福斯人", InboxId: "", EventCategory: "button", PageName: "footer_menu", EventLabel: "福斯人", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        default:
            break
        }
    }
    
    func showOpenhubWebview(url: String) {
        dismissHud()
        view.backgroundColor = UIColor.white
        
        // Set the status bar style
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.backgroundColor = .white
        
        // Create a view to cover the status bar area
        let statusBarCoverView = UIView()
        statusBarCoverView.backgroundColor = UIColor.MyTheme.greyColor100
        view.addSubview(statusBarCoverView)
        
        // Set constraints for the status bar cover view
        statusBarCoverView.translatesAutoresizingMaskIntoConstraints = false
        statusBarCoverView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        statusBarCoverView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        statusBarCoverView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        statusBarCoverView.heightAnchor.constraint(equalToConstant: UIApplication.shared.statusBarFrame.height).isActive = true
        
        let configuration = WKWebViewConfiguration()
        configuration.allowsInlineMediaPlayback = true
        configuration.websiteDataStore = WKWebsiteDataStore.default()
        configuration.mediaTypesRequiringUserActionForPlayback = []
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        configuration.preferences = preferences
        
        webView = WKWebView(frame: .zero, configuration: configuration)
        webView.navigationDelegate = self
        webView.backgroundColor = UIColor.MyTheme.greyColor100
        webView.scrollView.bounces = false
        webView.uiDelegate = self
        
        view.addSubview(webView)
        
        // Set constraints to make the web view fill the screen
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        webView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        // Load a URL
        if let url = URL(string: url) {
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
    
    private func goTab(of index: Int, subTab: Int?, deeplink: Bool, param: String?) {
        if !firstSelection && selectedIndex == index && !deeplink {
            Logger.d("same tab")
            
            if selectedIndex == 0 || selectedIndex == 4 {
                let currentVC = viewControllers[selectedIndex]
                if let navi = currentVC as? UINavigationController {
                    if navi.viewControllers.count > 1 {
                        navi.popToRootViewController(animated: true)
                    }
                }
            }
            
            return
        }
        
        if index == 3 {
            showHud()
            UserManager.shared().getProfileToCheckCellphone(param: param)
            print("tab 3 openhub")
            Utility.sendEventLog(ServiceName: "footer_menu", UniqueId: "", EventName: "充電地圖", InboxId: "", EventCategory: "button", PageName: "footer_menu", EventLabel: "充電地圖", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
            return
        }
        
        let previousIndex = selectedIndex
        selectedIndex = index
        
        tabButtons[previousIndex].isSelected = false
        setUpTabbarImageAndWordColor(view: tabButtonsView[previousIndex], isSelected: false)
        let previousVC = viewControllers[previousIndex]
        previousVC.willMove(toParent: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParent()
        
        tabButtons[selectedIndex].isSelected = true
        setUpTabbarImageAndWordColor(view: tabButtonsView[selectedIndex], isSelected: true)
        let vc = viewControllers[selectedIndex]
        
        switch selectedIndex {
        case 0:
            (carViewController.viewControllers[0] as!
             CarMainViewController).dpLinkIndex = subTab
//        case 1:
//            maintainenceViewController.dpLinkIndex = subTab
//            maintainenceViewController.goDeeplink(param: param)
        //case 2:
//        case 4:
//            if subTab == 1 {
//                memberViewController.dpLinkType = .MemberSetting
//            } else if subTab == 2 {
//                memberViewController.dpLinkType = .MemberProfile
//            } else if subTab == 3 {
//                memberViewController.dpLinkType = .MemberBenefits
//            } else if subTab == 4 {
//                memberViewController.dpLinkType = .MemberPrivacy
//            } else if subTab == 5 {
//                memberViewController.dpLinkType = .MemberTerms
//            } else if subTab == 6 {
//                memberViewController.dpLinkType = .MemberCoupon
//            }
//            memberViewController.goDeeplink(param: param)
        default:
            break
        }
        
        addChild(vc)
        vc.view.frame = contentView.bounds
        contentView.addSubview(vc.view)
        vc.didMove(toParent: self)
        
        firstSelection = false
    }
    
    func goDeeplink(deeplink: DeeplinkType, param: String?) {
        if deeplink == .AddCar {
            goTab(of: 0, subTab: 1, deeplink: true, param: param)
        } else if deeplink == .Maintenance {
            goTab(of: 1, subTab: nil, deeplink: true, param: param)
        } else if deeplink == .MaintenanceRecalls {
            goRecallWebview(param: param)
        } else if deeplink == .MaintenancePat {
            goPat(param: param)
        } else if deeplink == .MaintenanceBodyAndPaint {
            goBnPWebview(param: param)
        } else if deeplink == .MaintenanceAdditionalQuotation {
            goAdditionQ(param: param)
        } else if deeplink == .MaintenanceReservation {
            goReservation(param: param)
        } else if deeplink == .MaintenanceRecords {
            goRecord(param: param)
        } else if deeplink == .MaintenanceWarranty {
            goWarranty(param: param)
        } else if deeplink == .DrivingAssistant {
            goTab(of: 2, subTab: nil, deeplink: true, param: param)
        } else if deeplink == .Member {
            goTab(of: 4, subTab: nil, deeplink: true, param: param)
        } else if deeplink == .MemberSetting {
            goMemberSetting(param: param)
        } else if deeplink == .MemberProfile {
            goMemberProfile(param: param)
        } else if deeplink == .MemberBenefits {
            goMemberBenefits(param: param)
        } else if deeplink == .MemberPrivacy {
            goMemberPrivacy(param: param)
        } else if deeplink == .MemberTerms {
            goMemberTerms(param: param)
        } else if deeplink == .MemberCoupon {
            goMemberCoupon(param: param)
        }
    }
    
    // MARK: Maintenence Deeplink
    func goReservation(param: String?) {
        let mycars = CarManager.shared().getCars(remotely: false, listener: nil)
        if mycars.count == 0 {
            return
        }
        var astUrlEncode = URL(string: Config.urlMaintenanceReservation)!
        if let paramm = param {
            if paramm.contains("utm") {
                astUrlEncode = URL(string: "\(Config.urlMaintenanceReservation)?\(paramm)&\(getDeeplinkParams())")!
            } else {
                astUrlEncode = URL(string: "\(Config.urlMaintenanceReservation)?\(paramm)")!
            }
        } else {
            astUrlEncode = URL(string: "\(Config.urlMaintenanceReservation)?\(getDeeplinkParams())")!
        }
        if #available(iOS 14.5, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.index = .loadUrl
            vc.loadUrl = astUrlEncode
            vc.mytitle = "預約保養"
            self.goViewController(view: vc)
        } else if #available(iOS 13.0, *) {
            let url = astUrlEncode
            let safari = SFSafariViewController(url: url)
            //self.tabControllerDelegateV2?.goViewController(view: safari)
            self.navigationController?.pushViewController(safari, animated: true)
        } else {
            UIApplication.shared.open(astUrlEncode)
        }
    }
    
    func goAdditionQ(param: String?) {
        var urll = URL(string: "\(Config.urlMaintenanceAdditional)")
        if let paramm = param {
            if paramm.contains("utm") {
                urll = URL(string: "\(Config.urlMaintenanceAdditional)?\(paramm)&\(getDeeplinkParams())")!
            } else {
                urll = URL(string: "\(Config.urlMaintenanceAdditional)?\(paramm)")!
            }
        } else {
            urll = URL(string: "\(Config.urlMaintenanceAdditional)?\(getDeeplinkParams())")
        }
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.loadUrl
        vc.mytitle = "加修報價"
        vc.loadUrl = urll
        self.goViewController(view: vc)
    }
    
    func goRecord(param: String?) {
        var urll = URL(string: "\(Config.urlMaintenanceRecord)")
        if let paramm = param {
            if paramm.contains("utm") {
                urll = URL(string: "\(Config.urlMaintenanceRecord)?\(paramm)&\(getDeeplinkParams())")!
            } else {
                urll = URL(string: "\(Config.urlMaintenanceRecord)?\(paramm)")!
            }
        } else {
            urll = URL(string: "\(Config.urlMaintenanceRecord)?\(getDeeplinkParams())")!
        }
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.loadUrl
        vc.mytitle = "保養維修紀錄"
        vc.loadUrl = urll
        self.goViewController(view: vc)
    }
    
    func goWarranty(param: String?) {
        var urll = URL(string: "\(Config.urlMaintenanceWarranty)")
        if let paramm = param {
            if paramm.contains("utm") {
                urll = URL(string: "\(Config.urlMaintenanceWarranty)?\(paramm)&\(getDeeplinkParams())")!
            } else {
                urll = URL(string: "\(Config.urlMaintenanceWarranty)?\(paramm)")!
            }
        } else {
            urll = URL(string: "\(Config.urlMaintenanceWarranty)?\(getDeeplinkParams())")!
        }
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.loadUrl
        vc.mytitle = "愛車保固"
        vc.loadUrl = urll
        self.goViewController(view: vc)
    }
    
    func goBnPWebview(param: String?) {
        var bnpUrlEncode = URL(string: "\(Config.urlMaintenanceBodyAndPaint)")!
        if let paramm = param {
            if paramm.contains("utm") {
                bnpUrlEncode = URL(string: "\(Config.urlMaintenanceBodyAndPaint)?\(paramm)&\(getDeeplinkParams())")!
            } else {
                bnpUrlEncode = URL(string: "\(Config.urlMaintenanceBodyAndPaint)?\(paramm)")!
            }
        } else {
            bnpUrlEncode = URL(string: "\(Config.urlMaintenanceBodyAndPaint)?\(getDeeplinkParams())")!
        }
        if #available(iOS 14.5, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.index = .BnP
            vc.loadUrl = bnpUrlEncode
            vc.mytitle = "鈑噴估價"
            self.goViewController(view: vc)
        } else if #available(iOS 13.0, *) {
            let url = bnpUrlEncode
            let safari = SFSafariViewController(url: url)
            self.goViewController(view: safari)
        } else {
            UIApplication.shared.open(bnpUrlEncode)
        }
    }
    
    func goRecallWebview(param: String?) {
        var urll = URL(string: "\(Config.urlMaintenanceRecall)")!
        if let paramm = param {
            if paramm.contains("utm") {
                urll = URL(string: "\(Config.urlMaintenanceRecall)?\(paramm)&\(getDeeplinkParams())")!
            } else {
                urll = URL(string: "\(Config.urlMaintenanceRecall)?\(paramm)")!
            }
        } else {
            urll = URL(string: "\(Config.urlMaintenanceRecall)?\(getDeeplinkParams())")!
        }
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = .recall
        vc.loadUrl = urll
        vc.mytitle = "召回活動"
        self.goViewController(view: vc)
    }
    
    func goPat(param: String?) {
        var urll = URL(string: "\(Config.urlMaintenancePat)")!
        if let paramm = param {
            if paramm.contains("utm") {
                urll = URL(string: "\(Config.urlMaintenancePat)?\(paramm)&\(getDeeplinkParams())")!
            } else {
                urll = URL(string: "\(Config.urlMaintenancePat)?\(paramm)")!
            }
        } else {
            urll = URL(string: "\(Config.urlMaintenancePat)?\(getDeeplinkParams())")!
        }
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = .loadUrl
        vc.loadUrl = urll
        vc.mytitle = "專屬保修建議"
        self.goViewController(view: vc)
    }
    
    // member deeplink
    func goMemberSetting(param: String?) {
        let storyboard2 = UIStoryboard(name: "Person", bundle: nil)
        let vcc = storyboard2.instantiateViewController(withIdentifier: "SettingViewControllerV2") as! SettingViewControllerV2
        self.goViewController(view: vcc)
    }
    
    func goMemberProfile(param: String?) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.loadUrl
        if let pp = param {
            vc.loadUrl = URL(string: "\(Config.urlMemberUserEdit)?\(pp)")
        } else {
            vc.loadUrl = URL(string: "\(Config.urlMemberUserEdit)?\(getDeeplinkParams())")
        }
        vc.mytitle = "會員資料"
        self.goViewController(view: vc)
    }
    
    func goMemberBenefits(param: String?) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.loadUrl
        if let pp = param {
            vc.loadUrl = URL(string: "\(Config.urlMemberBenefit)?\(pp)")
        } else {
            vc.loadUrl = URL(string: "\(Config.urlMemberBenefit)?\(getDeeplinkParams())")
        }
        vc.mytitle = "會員升等/續會資格"
        self.goViewController(view: vc)
    }
    
    func goMemberPrivacy(param: String?) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.loadUrl
        if let pp = param {
            vc.loadUrl = URL(string: "\(Config.urlMemberPrivacy)?\(pp)")
        } else {
            vc.loadUrl = URL(string: "\(Config.urlMemberPrivacy)?\(getDeeplinkParams())")
        }
        vc.mytitle = "隱私權聲明"
        self.goViewController(view: vc)
    }
    
    func goMemberTerms(param: String?) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.loadUrl
        if let pp = param {
            vc.loadUrl = URL(string: "\(Config.urlMemberTerms)?\(pp)")
        } else {
            vc.loadUrl = URL(string: "\(Config.urlMemberTerms)?\(getDeeplinkParams())")
        }
        vc.mytitle = "服務條款"
        self.goViewController(view: vc)
    }
    
    func goMemberCoupon(param: String?) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.loadUrl
        if let pp = param {
            if pp.contains("utm") {
                vc.loadUrl = URL(string: "\(Config.urlMemberCenterCouponList)?\(pp)&\(getDeeplinkParams())")
            } else {
                vc.loadUrl = URL(string: "\(Config.urlMemberCenterCouponList)?\(pp)&\(getDeeplinkParams())")
            }
        } else {
            vc.loadUrl = URL(string: "\(Config.urlMemberCenterCouponList)?\(getDeeplinkParams())")
        }
        vc.mytitle = "優惠獎勵"
        self.goViewController(view: vc)
    }
    
    func setUpTabbarImageAndWordColor(view: UIView, isSelected: Bool) {
        let img = view.viewWithTag(1) as! UIImageView
        let text = view.viewWithTag(2) as! BasicLabel
        if isSelected {
            img.tintColor = UIColor.MyTheme.secondaryColor400
            text.textColor = UIColor.MyTheme.secondaryColor400
        } else {
            img.tintColor = UIColor.white
            text.textColor = UIColor.white
        }
    }
    
    // MARK: - TabControllerDelegateV2
    func goViewController(view: UIViewController) {
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    func goGarageVC(cars: [Car]) {
        let vc = UIStoryboard(name: "Car", bundle: nil).instantiateViewController(withIdentifier: "MyGarageViewController") as! MyGarageViewController
        vc.cars = cars
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func addCar() {
        addCarDirectly()
    }
    
    private func addCarDirectly() {
        let sb = UIStoryboard(name: "Login", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "AddCarNavi") as! UINavigationController
        self.navigationController?.present(vc, animated: true)
    }
    
    // MARK: webview
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: ((WKNavigationActionPolicy) -> Void)) {
        let urll = navigationAction.request.url!
        if Deeplinker.handleDeeplink(url: urll, ref: nil) {
            print("start deeplink: \(urll)")
            Deeplinker.checkDeepLink()
            decisionHandler(.cancel)
            return
        }
        
        
        if navigationAction.request.url?.scheme == "tel" {
            UIApplication.shared.open(navigationAction.request.url!)
            decisionHandler(.cancel)
            return
        }
         
        if navigationAction.targetFrame == nil {
            webView.load(navigationAction.request)
        }
        decisionHandler(.allow)
        return
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Start navigating to: \(webView.url?.absoluteString ?? "")")
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished navigating to: \(webView.url?.absoluteString ?? "")")
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("Failed to navigate: \(error.localizedDescription)")
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if let urll = navigationAction.request.url {
            if urll.absoluteString.contains("www.google.com") {
                UIApplication.shared.open(urll)
            }
        }
        return nil
    }
}
