//
//  EditPhotosViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2020/6/14.
//  Copyright © 2020 volkswagen. All rights reserved.
//

import UIKit

class EditPhotosViewController: UIViewController/*NaviViewController*/, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ClubUpdateDelegate {

    enum Mode: Int {
        case feed = 0, club
    }
    
    var clubId: Int?
    var mode: Mode = .feed
    
    @IBOutlet weak var cropViews: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var countLabelTopConstraint: NSLayoutConstraint!
    weak var contentDelegate: ContentDelegate? // for feed
    weak var clubUpdateDelegate: ClubUpdateDelegate?
    var selectedIndexPath = IndexPath(item: 0, section: 0)
    
    // editing post
    var editingPost: Post!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
//        naviBar.setTitle("edit_post_content".localized)
//        naviBar.setPublishVisibility(false)
        
        if let editpImg = editingPost.images {
            for (index, img) in editpImg.enumerated() {
                let cropView = CropView(frame: self.cropViews.bounds)
                cropViews.addSubview(cropView)
                cropView.backgroundColor = .black
                cropView.tag = index + 1
                cropView.image = img
            }
        }
        
        titleLable.font = UIFont(name: "VWHead-Bold", size: UiUtility.adaptiveSize(size: 17))
        nextBtn.titleLabel!.font = UIFont(name: "VWHead-Bold", size: UiUtility.adaptiveSize(size: 17))
        
        deleteBtn.setTitle("刪除此照片", for: .normal)
        deleteBtn.titleLabel!.font = UIFont(name: "VWHead-Regular", size: UiUtility.adaptiveSize(size: 15))
        
        countLabel.font = UIFont(name: "VWHead-Regular", size: UiUtility.adaptiveSize(size: 15))
    }
    
    deinit {
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let cropV = cropViews.viewWithTag(1) as? CropView {
            deleteBtn.isHidden = false
            countLabel.isHidden = false
            let fr = cropV.convert(cropV.cropRect, to: self.view)
            countLabelTopConstraint.constant = fr.origin.y + UiUtility.getScreenWidth() + UiUtility.adaptiveSize(size: 16)
        } else {
            deleteBtn.isHidden = true
            countLabel.isHidden = true
        }
    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        var cropImgs: [UIImage] = []
        var cropImgsSize: Double = 0
        if let editpImg = editingPost.images {
            for (index, _) in editpImg.enumerated() {
                let cropV = cropViews.viewWithTag(index + 1) as! CropView
                if let ci = cropV.croppedImage {
                    cropImgs.append(ci)
                    let imgData = NSData(data: ci.jpegData(compressionQuality: 0.3)!)
                    var imageSize: Int = imgData.count
                    //print("size......\((Double(imageSize) / 1000.0))")
                    cropImgsSize = cropImgsSize + (Double(imageSize) / 1000.0)
                }
            }
        }
        
        if cropImgsSize > 10000 {
            let alertController = UIAlertController(title: nil, message: "photo_limited".localized, preferredStyle: .alert)
            let action1 = UIAlertAction(title: "fine".localized, style: .default) { (action:UIAlertAction) in
                Logger.d("do nothing")
            }
            
            alertController.addAction(action1)
            present(alertController, animated: true, completion: nil)
        }
        
        if mode == .club {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditPostViewController") as! EditPostViewController
            vc.editingPost = Post.init(with: cropImgs)
            vc.contentDelegate = nil
            vc.mode = .club
            vc.clubId = clubId
            vc.clubUpdateDelegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            //UiUtility.goEditPost(from: self, post: Post.init(with: cropImgs), contentDelegate: nil, photoSourceType: .album)
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditPostViewController") as! EditPostViewController
            vc.editingPost = Post.init(with: cropImgs)
            vc.contentDelegate = contentDelegate
            vc.photoSourceType = .album
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteBtnPressed(_ sender: Any) {
        editingPost.images?.remove(at: selectedIndexPath.item)
        
        if editingPost.images?.count == 0 {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        for theView in cropViews.subviews {
            if theView.tag == selectedIndexPath.item + 1 {
                theView.removeFromSuperview()
            } else if theView.tag > selectedIndexPath.item + 1 {
                theView.tag = theView.tag - 1
            }
        }
        
        if  selectedIndexPath.item == 0 ||
            editingPost.images?.count == 1 {
            selectedIndexPath = IndexPath(item: 0, section: 0)
        } else {
            selectedIndexPath = IndexPath(item: selectedIndexPath.item - 1, section: 0)
        }
        
        collectionView.reloadData()
        
        for theView in cropViews.subviews {
            if theView.tag == selectedIndexPath.item + 1 {
                cropViews.bringSubviewToFront(theView)
            }
        }
    }
    
    func didFinishUpdateClub() {
        clubUpdateDelegate?.didFinishUpdateClub()
    }
    
    
    // MARK: collection view delegate
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let editpImg = editingPost.images {
            countLabel.text = "\(selectedIndexPath.item + 1)/\(editpImg.count)"
            if editpImg.count == 10 {
                return 10
            }
            return editpImg.count + 1
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EditPhotoImgCell", for: indexPath)
        let img: UIImageView = cell.viewWithTag(1) as! UIImageView
        let addImg: UIImageView = cell.viewWithTag(2) as! UIImageView
        addImg.image = UIImage(named: "add")?.withRenderingMode(.alwaysTemplate)
        img.layer.borderColor = UIColor.clear.cgColor
        img.layer.borderWidth = 4
        
        if let editpImg = editingPost.images {
            if indexPath.item == editpImg.count {
                img.isHidden = true
                return cell
            }
            img.isHidden = false
            img.image = editpImg[indexPath.item]
            if selectedIndexPath == indexPath {
                img.layer.borderColor = UIColor.white.cgColor
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let h = UiUtility.getScreenHeight() * 0.12 - 10
        return CGSize(width: h, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == editingPost.images!.count {
            UiUtility.presentImagePicker(from: self, delegate: self, allowsEditing: false)
            return
        }

        selectedIndexPath = indexPath
        collectionView.reloadData()
        
        for theView in cropViews.subviews {
            if theView.tag == indexPath.item + 1 {
                cropViews.bringSubviewToFront(theView)
            }
        }
    }

    // MARK: Image
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            picker.dismiss(animated: true)
            
            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
                Logger.d("No image found")
                return
            }
        
        let thumb = image.resized(toWidth: 1080.0)!
        
        selectedIndexPath = IndexPath(item: editingPost.images!.count, section: 0)
        editingPost.images?.append(thumb)
//        let imgData = NSData(data: image.jpegData(compressionQuality: 0.5)!)
//        var imageSize: Int = imgData.count
//        print("actual size of image in KB: %f ", Double(imageSize) / 1000.0)
        collectionView.reloadData()
        
        let cropView = CropView(frame: self.cropViews.bounds)
        cropViews.addSubview(cropView)
        cropView.backgroundColor = .black
        cropView.tag = editingPost.images!.count
        cropView.image = thumb

    }
}
