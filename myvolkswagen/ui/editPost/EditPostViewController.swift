//
//  EditPostViewController.swift
//  myvolkswagen
//
//  Created by Apple on 3/1/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Firebase

class EditPostViewController: NaviViewController, UITextViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    enum Mode: Int {
        case feed = 0, club
    }
    
    weak var contentDelegate: ContentDelegate? // for feed
    weak var clubUpdateDelegate: ClubUpdateDelegate?
    
    // editing post
    var editingPost: Post!
    var clubId: Int?
    
    var carModels: [CarModel2] = []
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var multiImgView: UIImageView!
    
    var mode: Mode = .feed
    var photoSourceType: PhotoSourceType = .album

    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()
        
        if editingPost == nil {
            editingPost = Post.init()
        }
        
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        naviBar.setTitle("edit_post_content".localized)
        naviBar.setPublishVisibility(true)
        
        textView.font = UIFont(name: "PingFangTC-Regular", size: 16)
        textView.placeholder = "hint_post_content".localized
        textView.textColor = UIColor.black
        textView.inputAccessoryView = toolbar
        
        multiImgView.isHidden = true
        if let editPostImg = editingPost.images {
            if editPostImg.count > 1 {
                multiImgView.isHidden = false
            }
        } else if let photoList = editingPost.photoList {
            if photoList.count > 1 {
                multiImgView.isHidden = false
            }
        }
        
        if mode == .feed {
            separator.backgroundColor = UiUtility.colorBrownGrey
            
            label.textColor = UIColor(rgb: 0x4A4A4A)
            label.font = UIFont(name: "PingFangTC-Regular", size: 14)
            label.text = "set_car_type".localized
            
            collectionView.clipsToBounds = false
        } else {
            separator.isHidden = true
            label.isHidden = true
            collectionView.isHidden = true
        }
        
        
        NewsManager.shared().getCarModel(remotely: true, listener: self)
    }
    
    deinit {
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if offsetY == -1 {
            offsetY = self.view.frame.origin.y
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        self.view.endEditing(true)
        
        let alertController = UIAlertController(title: nil, message: "warning_stop_edit".localized, preferredStyle: .alert)
        
        let action2 = UIAlertAction(title: "continue_edit".localized, style: .cancel) { (action:UIAlertAction) in
            Logger.d("You've pressed cancel");
        }
        
        let action3 = UIAlertAction(title: "give_up".localized, style: .destructive) { (action:UIAlertAction) in
            self.back()
        }
        
        alertController.addAction(action2)
        alertController.addAction(action3)
        present(alertController, animated: true, completion: nil)
    }
    
    override func publishButtonClicked(_ button: UIButton) {
        self.view.endEditing(true)
        
        if mode == .feed {
            if let models = getModelIdArrayText() {
                if let newsId = editingPost.newsId {
                    NewsManager.shared().edit(newsId: newsId, content: textView.text, models: models, modelList: getModelList(), listener: self)
                } else {
                    if let image: [UIImage] = editingPost?.images {
                        var base64Imgs: [String] = []
                        for img in image {
                            let bi = Utility.getBase64Image(image: img)
                            
                            base64Imgs.append(bi)
                        }
                        NewsManager.shared().add(imageStreams: base64Imgs, content: textView.text, models: models, listener: self)
                    }
                }
            } else {
                let alertController = UIAlertController(title: "at_least_1_type".localized, message: nil, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "got_it".localized, style: .default) { (action:UIAlertAction) in
                    Logger.d("do nothing")
                }
                
                alertController.addAction(action1)
                present(alertController, animated: true, completion: nil)
            }
        } else {
            if let newsId = editingPost.newsId {
                ClubManager.shared().edit(newsId: newsId, content: textView.text, listener: self)
            } else {
                if let images: [UIImage] = editingPost?.images {
                    var resizedImages: [UIImage]?
                    for img in images {
                        if let resizedImage = UiUtility.resizeImage(image: img, newWidth: 1000) {
                            resizedImages?.append(resizedImage)
                        }
                    }
                    if let ri = resizedImages {
                        if ri.count == images.count {
                            ClubManager.shared().appendNews(clubId: clubId!, content: textView.text, photos: ri, listener: self)
                            return
                        }
                    }
                    
                    ClubManager.shared().appendNews(clubId: clubId!, content: textView.text, photos: images, listener: self)
                }
            }
        }
    }
    
    
    
    // MARK: club protocol
    
    override func didFinishAppendNews(success: Bool) {
        super.didFinishAppendNews(success: success)
        
        if success {
            clubUpdateDelegate?.didFinishUpdateClub()
            
            if photoSourceType == .album {
                let controller = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 3]
                self.navigationController?.popToViewController(controller!, animated: true)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    override func didFinishEditClubPost(success: Bool, newsId: Int?) {
        super.didFinishEditClubPost(success: success, newsId: newsId)
        
        if success {
            if let clubUpdateDelegate = clubUpdateDelegate {
                clubUpdateDelegate.didFinishUpdateClub()
            } else {
                ClubManager.shared().cleanCache()
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    
    // MARK: news protocol
    
    override func didStartGetNewsCarModel() {
        showHud()
    }
    
    override func didFinishGetNewsCarModel(success: Bool, carModels: [Model]?) {
        dismissHud()
        
        if success {
//            Logger.d("get carModels: \(String(describing: carModels))")
            
            if let carModels = carModels {
                self.carModels = []
                for model in carModels {
                    let carModel = CarModel2.init(model: model)
                    self.carModels.append(carModel)
                }
            }
            
            setupUi()
        }
    }
    
    override func didStartAddPost() {
        showHud()
    }
    
    override func didFinishAddPost(success: Bool, newsId: String?) {
        dismissHud()
        
        if success {
            Analytics.logEvent("add_post_complete", parameters: nil)
            if photoSourceType == .album {
                let controller = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 3]
                self.navigationController?.popToViewController(controller!, animated: true)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    override func didStartEditPost() {
        showHud()
    }
    
    override func didFinishEditPost(success: Bool, newsId: Int?) {
        dismissHud()
        
        if success {
            self.navigationController?.popViewController(animated: true)
            contentDelegate?.setDirty(remote: true)
        }
    }
    
    
    
    // MARK: keyboard
    
    @objc override func keyboardWillShow(notification: NSNotification) {
        let d = UiUtility.adaptiveSize(size: 80/*magic*/)
        if self.view.frame.origin.y == offsetY {
            self.view.frame.origin.y -= d
        }
        keyboardShowing = true
    }
    
    @objc override func keyboardWillHide(notification: NSNotification) {
        
        if self.view.frame.origin.y != offsetY {
            self.view.frame.origin.y = offsetY
        }
        
        keyboardShowing = false
    }
    
    
    
    
    // MARK: collection view delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if mode == .club {
            return 0
        }
        return carModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat = 34
        return CGSize(width: carModels[indexPath.row].model.name!.width(withConstrainedHeight: height, font: UIFont(name: "PingFangTC-Medium", size: 16)!) + 32, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KindCell", for: indexPath)
        
        cell.clipsToBounds = false
        
//        let button: UIButton = cell.viewWithTag(1) as! UIButton
        let viewContainer: UIView = cell.viewWithTag(2)!
        let lbName: UILabel = cell.viewWithTag(3) as! UILabel
        
        let carModel = carModels[indexPath.row]
        
        viewContainer.layer.cornerRadius = 17
        viewContainer.layer.shadowColor = UIColor.black.cgColor
        viewContainer.layer.shadowOpacity = 0.3
        viewContainer.layer.shadowOffset = CGSize(width: 0, height: 1)
        viewContainer.layer.shadowRadius = UiUtility.adaptiveSize(size: 1.0)
        
        lbName.font = UIFont(name: "PingFangTC-Medium", size: 16)
        lbName.text = carModel.model.name
        
        if carModel.selected {
            lbName.textColor = .white
            viewContainer.backgroundColor = UiUtility.getModelSelectedColor(of: indexPath.row)
        } else {
            lbName.textColor = UiUtility.colorBrownGrey
            viewContainer.backgroundColor = .white
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let carModel: CarModel2 = carModels[indexPath.row]
        carModel.selected = !carModel.selected
        if carModel.selected {
            if editingPost.newsId == nil {
                let tag: String = "tag_\(carModel.model.name!.lowercased())"
//                Logger.d("tag: \(tag)")
                Analytics.logEvent("add_post_tag", parameters: ["tag_name": tag as NSObject])
            }
        }
        collectionView.reloadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

//    @objc private func modelClicked(_ sender: UIButton) {
//        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.collectionView)
//        if let indexPath = self.collectionView.indexPathForItem(at: buttonPosition) {
//            let carModel: CarModel = carModels[indexPath.row]
//            carModel.selected = !carModel.selected
//            if carModel.selected {
//                if editingPost.newsId == nil {
//                    let tag: String = "tag_\(carModel.kind.getText().lowercased())"
////                    Logger.d("tag: \(tag)")
//                    Analytics.logEvent("add_post_tag", parameters: ["tag_name": tag as NSObject])
//                }
//            }
//            collectionView.reloadData()
//        }
//    }
    
    private func getModelList() -> [Model] {
        var array: [Model] = []
        for carModel in carModels {
            if carModel.selected {
                array.append(carModel.model)
            }
        }
        
        return array
    }
    
    private func getModelIdArrayText() -> String? {
        var text: String? = nil
        for carModel in carModels {
            if carModel.selected {
                if let t = text {
                    text = "\(t),\(carModel.model.id!)"
                } else {
                    text = "\(carModel.model.id!)"
                }
            }
        }
        
        return text
    }
    
    private func setupUi() {
        if editingPost.newsId == nil {
            photo.image = editingPost!.images![0]
        } else {
            if let imageList = editingPost.photoList {
                if imageList.count > 0 {
                    let url = URL(string: imageList[0])
                    photo.kf.setImage(with: url)
                }
            }
        }
        
        if let content = editingPost.content {
            if content.count != 0 {
                textView.text = content
                textView.updatePlaceholderVisibility()
            }
        }
        
        Logger.d("editingPost.models: \(editingPost.models)")
        for model in editingPost.models {
            for carModel in carModels {
                if carModel.model.id == model.id {
                    carModel.selected = true
                }
            }
        }
        
        collectionView.reloadData()
    }
}
