//
//  WebViewViewController.swift
//  myvolkswagen
//
//  Created by Apple on 2/18/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import WebKit
import AVFAudio
import AVFoundation
import Firebase
import SnapKit

enum WebsiteIndex: Int {
    case reservation = 0, visualizer360, reseller, official, nearbyServiceCenter, eTag, supervisionStation, privacy, tos, memberExplanation, memberFaq, linePointsInfo, cpoCar, normalSurvey, newCarsSurvey, loadUrl, BnP, recall, memberCenter, memberPointList, serviceCam, eshop, memberCenterV2
    static var allCases: [WebsiteIndex] {
        var values: [WebsiteIndex] = []
        var index = 0
        while let element = self.init(rawValue: index) {
            values.append(element)
            index += 1
        }
        return values
    }
}

class WebViewViewController: NaviViewController, WKNavigationDelegate, WKUIDelegate, UIScrollViewDelegate {
    
    var index: WebsiteIndex!
    @IBOutlet var navCons: NSLayoutConstraint!
    @IBOutlet weak var container: UIView!
    var webView: WKWebView!
//    var state: String!
    var surveyUrl: URL?
    var loadUrl: URL?
    var patId: String?
    var uniqueId: String?
    var mytitle: String?
    var dpLinkType: DeeplinkType?
    @IBOutlet weak var gradientView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        naviBar.setTitle("".localized)
        if index == .eshop {
        } else {
            naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
        }
        
        let gradientVieww = GradientBackgroundView()
        gradientVieww.frame = view.bounds
        gradientView.addSubview(gradientVieww)
        
        switch index! {
        case .reservation:
            naviBar.setTitle("trial_dirve".localized)
        case .visualizer360:
            naviBar.setTitle("online_preview".localized)
        case .reseller:
            naviBar.setTitle("sale".localized)
        case .official:
            naviBar.setTitle("vw_official_website".localized)
        case .nearbyServiceCenter:
            naviBar.setTitle("nearby_service_center".localized)
        case .eTag:
            naviBar.setTitle("etag".localized)
        case .supervisionStation:
            naviBar.setTitle("supervision_station".localized)
        case .privacy:
            naviBar.setTitle("privacy_statement".localized)
        case .tos:
            naviBar.setTitle("term_of_service".localized)
        case .memberExplanation:
            naviBar.setTitle("member_explanation".localized)
        case .memberFaq:
            naviBar.setTitle("member_level_faq".localized)
        case .linePointsInfo:
            naviBar.setTitle("redeem_points_info".localized)
        case .cpoCar:
            naviBar.setTitle("cpo_car".localized)
        case .normalSurvey, .newCarsSurvey:
            naviBar.setTitle("survey".localized)
        case .loadUrl:
            if mytitle != nil {
                naviBar.setTitle(mytitle!)
            } else {
                naviBar.setTitle("")
            }
        case .BnP:
            naviBar.setTitle("body_and_paint".localized)
        case .recall:
            naviBar.setTitle("recall".localized)
        case .memberCenter:
            naviBar.setTitle("member_page".localized)
        case .memberPointList:
            naviBar.setTitle("member_page".localized)
        case .serviceCam:
            naviBar.setTitle("service_cam".localized)
        case .eshop:
            naviBar.setTitle("線上商店")
            navCons.isActive = true
        case .memberCenterV2:
            naviBar.setTitle("")
            navCons.isActive = false
        }
        
        /*
        To summarize, the requirements for it to work are:
        1. Use iOS 14.5+.
        2. Use the following two attributes: <video autoplay playsinline>.
        3. Request camera permission with AVCaptureDevice.authorizationStatus(for:) **before** creating the WKWebView (and thus have a NSCameraUsageDescription in your Info.plist)
        4. Set allowsInlineMediaPlayback on the WKWebViewConfiguration.
         */
                                            
//        let contentController = WKUserContentController();
//        contentController.add(self, name: "callbackHandler")
//
//        let script = try! String(contentsOf: Bundle.main.url(forResource: "WebRTC", withExtension: "js")!, encoding: String.Encoding.utf8)
//        contentController.addUserScript(WKUserScript(source: script, injectionTime: WKUserScriptInjectionTime.atDocumentStart, forMainFrameOnly: true))
        
        let configuration = WKWebViewConfiguration()
        configuration.ignoresViewportScaleLimits = true
        configuration.suppressesIncrementalRendering = true
        configuration.allowsInlineMediaPlayback = true
        configuration.allowsAirPlayForMediaPlayback = false
        configuration.allowsPictureInPictureMediaPlayback = true
        configuration.mediaTypesRequiringUserActionForPlayback = .all
        configuration.setURLSchemeHandler(CustomSchemeHandler(), forURLScheme: Config.redirectScheme)
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        configuration.preferences = preferences
        webView = WKWebView(frame: self.container.bounds, configuration: configuration)
        self.container.addSubview(webView)
        webView.snp.makeConstraints { make in
            make.leading.trailing.bottom.top.equalToSuperview()
        }
        
        webView.navigationDelegate = self
        webView.uiDelegate = self
        webView.allowsBackForwardNavigationGestures = true
        webView.scrollView.maximumZoomScale = 1.0
        webView.scrollView.minimumZoomScale = 1.0
        webView.scrollView.bouncesZoom = false
        webView.scrollView.pinchGestureRecognizer?.isEnabled = false
        webView.scrollView.delegate = self
        
        switch self.index! {
        case .reservation:
            let url = URL(string: Config.urlReservation)!
            self.webView.load(URLRequest(url: url))
        case .visualizer360:
            let url = URL(string: Config.urlVisualizer360)!
            self.webView.load(URLRequest(url: url))
        case .reseller:
            let url = URL(string: Config.urlDealer)!
            self.webView.load(URLRequest(url: url))
        case .official:
            let url = URL(string: Config.urlOfficial)!
            self.webView.load(URLRequest(url: url))
        case .nearbyServiceCenter:
            let url = URL(string: Config.urlNearbyServiceCenter)!
            self.webView.load(URLRequest(url: url))
        case .eTag:
            let dataStore = WKWebsiteDataStore.default()
            dataStore.fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
//                Logger.d("records: \(records)")
                dataStore.removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), for: records.filter { $0.displayName.contains("fetc") }, completionHandler: {
                    let url = URL(string: Config.urlETag)!
                    self.webView.load(URLRequest(url: url))
                })
            }
        case .supervisionStation:
            let url = URL(string: Config.urlSupervisionStation)!
            self.webView.load(URLRequest(url: url))
        case .privacy:
            let url = Bundle.main.url(forResource: "privacy", withExtension: "html"/*, subdirectory: "website"*/)!
            webView.loadFileURL(url, allowingReadAccessTo: url)
            let request = URLRequest(url: url)
            webView.load(request)
        case .tos:
            let url = Bundle.main.url(forResource: "tos", withExtension: "html"/*, subdirectory: "website"*/)!
            webView.loadFileURL(url, allowingReadAccessTo: url)
            let request = URLRequest(url: url)
            webView.load(request)
        case .memberExplanation:
            let url = URL(string: Config.urlMemberExplanation)!
            self.webView.load(URLRequest(url: url))
        case .memberFaq:
            let url = URL(string: Config.urlMemberFaq)!
            self.webView.load(URLRequest(url: url))
        case .linePointsInfo:
            let url = URL(string: Config.urlLinePointsInfo)!
            self.webView.load(URLRequest(url: url))
        case .cpoCar:
            let url = URL(string: "\(Config.urlCpoCar)?token=\(UserManager.shared().getUserToken()!)")!
            self.webView.load(URLRequest(url: url))
        case .normalSurvey, .newCarsSurvey:
            if let ur = surveyUrl {
                self.webView.load(URLRequest(url: ur))
            }
        case .loadUrl:
            self.webView.load(URLRequest(url: loadUrl!))
        case .BnP:
            self.webView.load(URLRequest(url: loadUrl!))
        case .recall:
            print("load url: \(loadUrl?.absoluteString ?? "")")
            self.webView.load(URLRequest(url: loadUrl!))
        case .memberCenter:
            let url = URL(string: "\(Config.urlMemberCenter)?token=\(UserManager.shared().getUserToken()!)")!
            self.webView.load(URLRequest(url: url))
        case .memberPointList:
            self.webView.load(URLRequest(url: loadUrl!))
        case .serviceCam:
            self.webView.load(URLRequest(url: loadUrl!))
        case .eshop:
            let url = URL(string: "\(Config.urlEShop)")!
            self.webView.load(URLRequest(url: url))
        case .memberCenterV2:
            self.loadMemberCenterV2()
        }
    }
    
    func loadMemberCenterV2() {
        var getvin = ""
        for cc in CarManager.shared().cars {
            if cc.licensePlateNumber == CarManager.shared().getSelectedCarNumber()! {
                getvin = cc.vin ?? ""
            }
        }
        
        Installations.installations().installationID { (installationID, error) in
            if let error = error {
                Logger.d("Error fetching remote instance ID: \(error)")
                return
            }

            guard let installationID = installationID else {
                print("设备的安装 ID 为空")
                if let carn = CarManager.shared().getSelectedCarNumber() {
                    let url = URL(string: "\(Config.urlMemberCenterV2)?VIN=\(getvin)&LicensePlateNumber=\(carn)&AccountId=\(UserManager.shared().currentUser.accountId!)&token=\(UserManager.shared().getUserToken()!)")!
                    self.webView.load(URLRequest(url: url))
                } else {
                    let url = URL(string: "\(Config.urlMemberCenterV2)?VIN=\(getvin)&AccountId=\(UserManager.shared().currentUser.accountId!)&token=\(UserManager.shared().getUserToken()!)")!
                    self.webView.load(URLRequest(url: url))
                }
                return
            }

            print("设备的安装 ID：\(installationID)")
            Logger.d("Remote instance ID token: \(installationID)")
            if let carn = CarManager.shared().getSelectedCarNumber() {
                let url = URL(string: "\(Config.urlMemberCenterV2)?VIN=\(getvin)&LicensePlateNumber=\(carn)&AccountId=\(UserManager.shared().currentUser.accountId!)&token=\(UserManager.shared().getUserToken()!)&device_id=\(installationID)")!
                self.webView.load(URLRequest(url: url))
            } else {
                let url = URL(string: "\(Config.urlMemberCenterV2)?VIN=\(getvin)&AccountId=\(UserManager.shared().currentUser.accountId!)&token=\(UserManager.shared().getUserToken()!)&device_id=\(installationID)")!
                self.webView.load(URLRequest(url: url))
            }
        }
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
            scrollView.pinchGestureRecognizer?.isEnabled = false
        }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        AppUtility.lockOrientation(.landscapeLeft)
        // Or to rotate and lock        
        if index! == .visualizer360 {
            AppUtility.lockOrientation(.landscapeLeft, andRotateTo: .landscapeLeft)
        } else if index == .memberCenterV2 {
            self.loadMemberCenterV2()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        if index! == .visualizer360 {
            AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        }
    }
    
    override func viewDidLayoutSubviews() {
    }
    
    override func leftButtonClicked(_ button: UIButton) {
//        dismissHud()
        switch self.index! {
        case .eshop, .memberCenterV2:
            if self.webView.canGoBack {
                self.webView.goBack()
            }
        default:
            print("do nothing")
        }
        back()
        if let pp = self.patId {
            //Utility.sendEventLog(ServiceName: "PAT", UniqueId: self.uniqueId!, EventName: "pat_reserve_other_workshop_back_btn", InboxId: pp)
        }
    }
    
    
    
    // MARK: WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
//        Logger.d("didCommit")
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//        Logger.d("didStartProvisionalNavigation")
//        showHud()
        if webView.canGoBack {
            navCons.isActive = true
        }
    }
    
    func webView(_ webView: WKWebView,
                 didFail navigation: WKNavigation!,
                 withError error: Error) {
        Logger.d("didFail")
    }
    
    func webView(_ webView: WKWebView,
                 didFailProvisionalNavigation navigation: WKNavigation!,
                 withError error: Error) {
        Logger.d("didFailProvisionalNavigation, error: \(error)")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Logger.d("didFinish")
        if index == .memberCenter {
            if webView.canGoBack {
                navCons.isActive = true
            } else {
                navCons.isActive = false
            }
        } else if index == .eshop {
            if webView.canGoBack {
                naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
            } else {
                naviBar.setLeftButton(nil, highlighted: nil)
            }
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: ((WKNavigationActionPolicy) -> Void)) {
        
//        let urll = navigationAction.request.url!.absoluteString.removingPercentEncoding!
//        let urllArr = urll.components(separatedBy: "#")
//        if urllArr.count > 1 {
//            //let one: String = urllArr[0]
//            let two: String = urllArr[1]
//            let txt = "cpo_car".localized
//            if naviBar.title.text == txt {
//            } else {
//                naviBar.setTitle(two)
//            }
//        }
        
        let urll = navigationAction.request.url!
        if Deeplinker.handleDeeplink(url: urll, ref: nil) {
            print("start deeplink: \(urll)")
            Deeplinker.checkDeepLink()
            decisionHandler(.cancel)
            return
        }
        
        switch self.index! {
        case .privacy, .tos:
            if let url = navigationAction.request.url {
                Logger.d(url.absoluteString) // It will give the selected link URL
                
                if let scheme = url.scheme {
                    Logger.d("scheme: \(scheme)")
                    Logger.d("host: \(String(describing: url.host))")
                    
                    if scheme == "tel" {
                        let arr: [String] = url.absoluteString.components(separatedBy: ":")
                        Utility.call(number: arr[1])
                    } else if scheme == "https" {
                        UIApplication.shared.open(url)
                    }
                }
            }
            decisionHandler(.allow)
            return
        case .newCarsSurvey:
            guard let url = navigationAction.request.url else {
                decisionHandler(.cancel)
                return
            }
                    
            // If url changes, cancel current request which has no custom parameters
            // and load a new request with that url with custom parameters
            if url != loadUrl {
                decisionHandler(.cancel)
                loadWebPage(url: url)
                return
             } else {
                decisionHandler(.allow)
                 return
             }
        default:
            if navigationAction.request.url?.scheme == "tel" {
                UIApplication.shared.open(navigationAction.request.url!)
                decisionHandler(.cancel)
                return
            } else {
                if navigationAction.targetFrame == nil {
                    webView.load(navigationAction.request)
                }
                decisionHandler(.allow)
                return
            }
        }
        
        
//        switch navigationAction.navigationType {
//        case .linkActivated:
//            if navigationAction.targetFrame == nil {
//                self.webView?.load(navigationAction.request)// It will load that link in same WKWebView
//            }
//        default:
//            break
//        }
//
        decisionHandler(.allow)
    }
    
    func loadWebPage(url: URL)  {
            var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        components?.query = nil

            // If already has a query then append a param
            // otherwise create a new one
            if let query = url.query {
                // If isFromMobile param already exists jsut reassign the existing query
                if query.contains("AccountId=\(UserManager.shared().currentUser.accountId!)") {
                    components?.query = query
                } else {
                     components?.query = query + "&AccountId=\(UserManager.shared().currentUser.accountId!)"
                }
            } else {
                components?.query = "AccountId=\(UserManager.shared().currentUser.accountId!)"
            }

            let customRequest = URLRequest(url: components!.url!)
            loadUrl = components!.url!
            webView!.load(customRequest)
        }

    
    // MARK: - JS
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {
        
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (action) in
            completionHandler()
        }))
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (Bool) -> Void) {
        
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (action) in
            completionHandler(true)
        }))
        
        alertController.addAction(UIAlertAction(title: "取消", style: .default, handler: { (action) in
            completionHandler(false)
        }))
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (String?) -> Void) {
        
        let alertController = UIAlertController(title: nil, message: prompt, preferredStyle: .actionSheet)
        
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        
        alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "取消", style: .default, handler: { (action) in
            completionHandler(nil)
        }))
        
        present(alertController, animated: true, completion: nil)
    }

}
