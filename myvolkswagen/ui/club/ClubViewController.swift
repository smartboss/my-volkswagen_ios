//
//  ClubViewController.swift
//  myvolkswagen
//
//  Created by CHUNG CHING JIANG on 2019/9/29.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

protocol ClubUpdateDelegate: class {
    func didFinishUpdateClub()
}

class ClubViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource, ClubUpdateDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    enum Section: Int {
        case club = 0, post
        static var allCases: [Section] {
            var values: [Section] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    enum RowClub: Int {
        case photo = 0, name, intro, member, join
        static var allCases: [RowClub] {
            var values: [RowClub] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    
    var club: Club!
    @IBOutlet weak var tableView: UITableView!
    private var posts: [ClubPost] = []
    
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var constraintAddButtonBottom: NSLayoutConstraint!
    
    private var clubUpdated = false
    
    private var shouldClean: Bool = false
    
    var clubUpdateDelegate: ClubUpdateDelegate?
    
    
    
    // card
    @IBOutlet weak var viewShieldCard: UIView!
    @IBOutlet weak var containerCard: UIView!
    var cardMinCenterY: CGFloat = 0
    var cardMaxCenterY: CGFloat = 0
    @IBOutlet weak var viewAdd0: UIView!
    @IBOutlet weak var add0label0: UILabel!
    @IBOutlet weak var add0label1: UILabel!
    @IBOutlet weak var add0label2: UILabel!
    @IBOutlet weak var viewAdd1: UIView!
    @IBOutlet weak var add1tableView: UITableView!
    @IBOutlet weak var add1buttonNext: UIButton!
    @IBOutlet weak var viewAdd2: UIView!
    @IBOutlet weak var add2buttonNext: UIButton!
    @IBOutlet weak var add2label0: UILabel!
    @IBOutlet weak var add2Photo: UIImageView!
    @IBOutlet weak var add2Check: UIImageView!
    @IBOutlet weak var add2ConstraintUserWidth: NSLayoutConstraint!
    @IBOutlet weak var add2ConstraintPhotoWidth: NSLayoutConstraint!
    @IBOutlet weak var add2ConstraintCheckWidth: NSLayoutConstraint!
    @IBOutlet weak var add2ConstraintSpacing: NSLayoutConstraint!
    @IBOutlet weak var add2Name: UILabel!
    @IBOutlet weak var add2label1: UILabel!
    @IBOutlet weak var add2label2: UILabel!

    
    private var members: [ClubRelation] = []
    private var newAdmin: ClubRelation?
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UiUtility.colorBackgroundWhite
        tableView.backgroundColor = UiUtility.colorBackgroundWhite
        
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        if isJoined() {
            naviBar.setRightButton(UIImage(named: "more"), highlighted: UIImage(named: "more"))
        }
        
        constraintAddButtonBottom.constant = UiUtility.adaptiveSize(size: 40)
        buttonAdd.isHidden = !isJoined()
        
        
        
        // card
        setCardVisibility(false, animated: false, isInit: true)
        var panCardGesture = UIPanGestureRecognizer()
        panCardGesture = UIPanGestureRecognizer(target: self, action: #selector(draggedCardView(_:)))
        //        containerCard.isUserInteractionEnabled = true
        containerCard.addGestureRecognizer(panCardGesture)
        add0label0.textColor = UiUtility.colorDarkGrey
        add0label0.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
        add0label0.text = "club_card_text_0".localized
        add0label1.textColor = UiUtility.colorDarkGrey
        add0label1.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
        add0label1.text = "test_club_name".localized
        add0label2.textColor = UiUtility.colorDarkGrey
        add0label2.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
        add0label2.text = "club_card_text_1".localized

        add1buttonNext.setTitle("next_step".localized, for: .normal)
        add1buttonNext.setTitleColor(.black, for: .normal)
        add1buttonNext.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
        
        add2buttonNext.setTitle("leave_club".localized, for: .normal)
        add2buttonNext.setTitleColor(.black, for: .normal)
        add2buttonNext.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
        
        add2label0.textColor = UiUtility.colorDarkGrey
        add2label0.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
        add2label0.text = "you_choose".localized
        
        add2ConstraintUserWidth.constant = UiUtility.adaptiveSize(size: 300)
        add2ConstraintPhotoWidth.constant = UiUtility.adaptiveSize(size: 40)
        add2ConstraintCheckWidth.constant = UiUtility.adaptiveSize(size: 12)
        add2ConstraintSpacing.constant = UiUtility.adaptiveSize(size: 10)
        
        add2Name.textColor = .black
        add2Name.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
        add2Name.text = "username"
        
        add2label1.textColor = UiUtility.colorDarkGrey
        add2label1.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
        add2label1.text = "take_over".localized
        
        add2label2.textColor = .black
        add2label2.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
        add2label2.text = "club_card_text_2".localized
        ////
        
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if clubUpdated {
            clubUpdated = false
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if shouldClean {
            shouldClean = false
            ClubManager.shared().cleanCache()
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        self.back()
    }
    
    override func rightButtonClicked(_ button: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if isManager() {
            alert.addAction(UIAlertAction(title: "edit_club".localized, style: .default, handler: { _ in
                let navi = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateClubNavi") as! UINavigationController
                let vc = navi.viewControllers[0] as! CreateClubViewController
                vc.club = self.club
                vc.mode = .edit
                vc.clubUpdateDelegate = self
                self.present(navi, animated: true, completion: nil)
            }))
            
            alert.addAction(UIAlertAction(title: "remove_member".localized, style: .destructive, handler: { _ in
                self.goClubMember(browse: false)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "leave_club".localized, style: .destructive, handler: { _ in
            let alert = UIAlertController(title: nil, message: "warning_leave_club".localized, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "not_now".localized, style: .default, handler: nil))
            
            alert.addAction(UIAlertAction.init(title: "leave_club_now".localized, style: .destructive, handler:  { _ in
                if self.isManager() {
                    ClubManager.shared().getMembers(clubId: self.club.id, listener: self)
                } else {
                    ClubManager.shared().leave(clubId: self.club.id, newAdminId: nil, listener: self)
                }
            }))
            
            self.present(alert, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction.init(title: "cancel".localized, style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    // MARK: image picker controller delegate
        
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        guard let oriImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            Logger.d("No image found")
            return
        }
        
        let thumb = oriImage.resized(toWidth: 1080.0)!
        // print out the image size as a test
//        Logger.d("image size: \(image.size)")
        
//        UiUtility.goEditPost(from: self, post: Post.init(with: image), contentDelegate: nil)
        if picker.sourceType == .camera {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditPostViewController") as! EditPostViewController
            guard let image = info[.editedImage] as? UIImage else {
                Logger.d("No image found")
                return
            }
            vc.editingPost = Post.init(with: [image])
            vc.contentDelegate = nil
            vc.mode = .club
            vc.clubId = club.id
            vc.clubUpdateDelegate = self
            vc.photoSourceType = .camera
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditPhotosViewController") as! EditPhotosViewController
            vc.editingPost = Post.init(with: [thumb])
            vc.contentDelegate = nil
            vc.mode = .club
            vc.clubId = club.id
            vc.clubUpdateDelegate = self
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
    // MARK: club update delegate
    
    func didFinishUpdateClub() {
        Logger.d("didFinishUpdateClub")
        clubUpdated = true
        shouldClean = true
        clubUpdateDelegate?.didFinishUpdateClub()
    }
    
    
    
    // MARK: user protocol
    
    override func didFinishFollow(success: Bool, accountId: String, set: Bool) {
        dismissHud()
        
        if success {
            for n in posts {
                if let na = n.accountId {
                    if na == accountId {
                        if set {
                            n.isFollow = 1
                        } else {
                            n.isFollow = 0
                        }
                    }
                }
            }
            
            tableView.reloadData()
        }
    }
    
    
    
    // MARK: car protocol
    
    override func didStartGetCar() {
        showHud()
    }
    
    override func didFinishGetCar(success: Bool, cars: [Car]) {
        dismissHud()
        
        if success {
            if cars.count == 0 {
                CarManager.shared().addCarFromMainPage = true
                navigationController?.popViewController(animated: true)
            } else {
                // add new post
                UiUtility.pickImage(from: self, delegate: self, allowsEditing: false)
            }
        }
    }
    
    
    
    // MARK: club protocol
    
    override func didFinishGetClubMembers(success: Bool, relations: [ClubRelation]) {
        super.didFinishGetClubMembers(success: success, relations: relations)
        
        if success {
            members = relations
            var idx = 0
            for member in members {
                if member.isMe() {
                    members.remove(at: idx)
                    break
                }
                idx += 1
            }
            self.addCardDirectly()
            add1tableView.reloadData()
        } else {
            Logger.d("failed to get members")
        }
    }
    
    override func didFinishLeaveClub(success: Bool) {
        super.didFinishLeaveClub(success: success)
        
        if success {
            ClubManager.shared().cleanCache()
            clubUpdateDelegate?.didFinishUpdateClub()
            navigationController?.popViewController(animated: true)
        } else {
            Logger.d("failed to leave club")
        }
    }
    
    override func didFinishMemberAdd(success: Bool) {
        super.didFinishMemberAdd(success: success)
        
        if success {
            ClubManager.shared().cleanCache()

            club.isJoin = 1
            club.memberCount += 1
            naviBar.setRightButton(UIImage(named: "more"), highlighted: UIImage(named: "more"))
            buttonAdd.isHidden = !isJoined()
//            tableView.reloadData()
            
            loadData()
        } else {
            Logger.d("failed to member add")
        }
    }
    
    override func didFinishGetSinglePage(success: Bool, posts: [ClubPost]) {
        super.didFinishGetSinglePage(success: success, posts: posts)
        
        if success {
            self.posts = posts
            tableView.reloadData()
        }
    }
    
    override func didFinishLikeClubPost(success: Bool, post: ClubPost, set: Bool) {
        dismissHud()
        
        if success {
            var index = 0
            for n in posts {
                if n.newsId! == post.newsId! {
                    if set {
                        n.likeStatus = 1
                        n.likeAmount = n.likeAmount + 1
                    } else {
                        n.likeStatus = 0
                        n.likeAmount = n.likeAmount - 1
                    }
                    tableView.reloadData()
                    break
                }
                index += 1
            }
            
            shouldClean = true
            clubUpdateDelegate?.didFinishUpdateClub()
        }
    }
    
    override func didFinishTrackClubPost(success: Bool, post: ClubPost, set: Bool) {
        dismissHud()
        
        if success {
            for n in posts {
                if n.newsId! == post.newsId! {
                    if set {
                        n.bookmarkStatus = 1
                        n.bookmarkAmount = n.bookmarkAmount + 1
                    } else {
                        n.bookmarkStatus = 0
                        n.bookmarkAmount = n.bookmarkAmount - 1
                    }
                    tableView.reloadData()
                    break
                }
            }
            
            shouldClean = true
            clubUpdateDelegate?.didFinishUpdateClub()
        }
    }
    
    override func didFinishSetTop(success: Bool, post: ClubPost, set: Bool) {
        super.didFinishSetTop(success: success, post: post, set: set)
        
        if success {
//            for n in posts {
//                if n.newsId! == post.newsId! {
//                    n.isTop = (set ? 1 : 0)
//                } else if n.clubId == post.clubId {
//                    if set {
//                        n.isTop = 0
//                    }
//
//                }
//            }
//
//            tableView.reloadData()
            
            loadData()
            
            shouldClean = true
            clubUpdateDelegate?.didFinishUpdateClub()
        }
    }
    
    override func didFinishDeleteClubPost(success: Bool, post: ClubPost?) {
        super.didFinishDeleteClubPost(success: success, post: post)
        
        if success {
            if let post = post {
                var index = 0
                for n in posts {
                    if n.newsId! == post.newsId! {
                        posts.remove(at: index)
                        break
                    }
                    index += 1
                }
                
                tableView.reloadData()
            }
            
            shouldClean = true
            clubUpdateDelegate?.didFinishUpdateClub()
        }
        
    }
    
//    override func didFinishEditClubPost(success: Bool, newsId: Int?) {
//        super.didFinishEditClubPost(success: success, newsId: newsId)
//
//        if success {
//
//        }
//    }
    
    
    
    // MARK: table view delegate
        
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == add1tableView {
            return 1
        } else {
            return Section.allCases.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == add1tableView {
            return members.count
        } else {
            switch Section.init(rawValue: section)! {
            case .club:
                if isJoined() {
                    return RowClub.allCases.count - 1
                } else {
                    return RowClub.allCases.count
                }
            case .post:
                return posts.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == add1tableView {
            return UiUtility.adaptiveSize(size: 80)
        } else {
            switch Section.init(rawValue: indexPath.section)! {
            case .club:
                switch RowClub.init(rawValue: indexPath.row)! {
                case .photo:
                    return UiUtility.adaptiveSize(size: 170)
                case .name:
                    return UiUtility.adaptiveSize(size: 12) + club.name!.height(withConstrainedWidth: UiUtility.adaptiveSize(size: 230), font: UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))!)
                case .intro:
                    return UiUtility.adaptiveSize(size: 10) + club.intro!.height(withConstrainedWidth: UiUtility.adaptiveSize(size: 248), font: UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))!)
                case .member:
                    return UiUtility.adaptiveSize(size: 48)
                case .join:
                    return UiUtility.adaptiveSize(size: 66)
                }
            case .post:
                return self.calculateNewsCellHeight(post: posts[indexPath.row])
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            if indexPath.section == Section.post.rawValue {
                if posts.count != 0 {
                    let lastElement = posts.count - 1
                    if indexPath.row == lastElement {
                        if !ClubManager.shared().singlePageListEndReached {
                            // handle your logic here to get more items, add it to dataSource and reload tableview
//                            Logger.d("should try load more")
                            loadData()
                        }
                    }
                }
            }
        }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        if tableView == add1tableView {
            cell = tableView.dequeueReusableCell(withIdentifier: "AdminCell", for: indexPath)
            
            let photo: UIImageView = cell.viewWithTag(1) as! UIImageView
            let name: UILabel = cell.viewWithTag(2) as! UILabel
            let button: UIButton = cell.viewWithTag(3) as! UIButton
            let imageCheck: UIImageView = cell.viewWithTag(4) as! UIImageView
            
            let member = members[indexPath.row]
            
            photo.backgroundColor = UiUtility.colorBackgroundWhite
            photo.layer.masksToBounds = true
            photo.layer.cornerRadius = UiUtility.adaptiveSize(size: 30)
            if let stickerURL = member.stickerURL {
                let url = URL(string: stickerURL)
                photo.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_photo"))
            }
            
            // image check
            imageCheck.isHidden = true
//            if let level = member.level {
//                if level > 2 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_golden_large")
//                } else if level > 1 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_silver_large")
//                } else if level > 0 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_large")
//                }
//            }
            
            name.textColor = UIColor.black
            name.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            name.text = member.nickname
            
            if let newAdmin = newAdmin {
                button.isSelected = (newAdmin.accountId! == member.accountId!)
            } else {
                button.isSelected = false
            }
        } else {
            switch Section.init(rawValue: indexPath.section)! {
            case .club:
                switch RowClub.init(rawValue: indexPath.row)! {
                case .photo:
                    cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCell", for: indexPath)
                    
                    let ivPhoto: UIImageView = cell.viewWithTag(1) as! UIImageView
                    ivPhoto.backgroundColor = UiUtility.colorBackgroundIncomingMessage
                    if let photoUrl = club.picUrl {
                        let url = URL(string: photoUrl)
                        ivPhoto.kf.setImage(with: url)
                    }
                case .name:
                    cell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath)
                    
                    let lbText: UILabel = cell.viewWithTag(1) as! UILabel
                    
                    for constraint in cell.contentView.constraints {
                        if constraint.identifier == "text_top" {
                            constraint.constant = UiUtility.adaptiveSize(size: 12)
                        }
                    }
                    
                    for constraint in lbText.constraints {
                        if constraint.identifier == "text_width" {
                            constraint.constant = UiUtility.adaptiveSize(size: 230)
                        }
                    }
                    
                    lbText.textColor = .black
                    lbText.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
                    lbText.text = club.name
                case .intro:
                    cell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath)
                    
                    let lbText: UILabel = cell.viewWithTag(1) as! UILabel
                    
                    for constraint in cell.contentView.constraints {
                        if constraint.identifier == "text_top" {
                            constraint.constant = UiUtility.adaptiveSize(size: 10)
                        }
                    }
                    
                    for constraint in lbText.constraints {
                        if constraint.identifier == "text_width" {
                            constraint.constant = UiUtility.adaptiveSize(size: 248)
                        }
                    }
                    
                    lbText.textColor = .black
                    lbText.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
                    lbText.text = club.intro
                case .member:
                    cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell", for: indexPath)
                    
                    let ivIcon: UIImageView = cell.viewWithTag(1) as! UIImageView
                    let lbCount: UILabel = cell.viewWithTag(2) as! UILabel
                    let lbText: UILabel = cell.viewWithTag(3) as! UILabel
                    let vContainer: UIView = cell.viewWithTag(4)!
                    let btnAdd: UIButton = cell.viewWithTag(5) as! UIButton
                    
                    for constraint in cell.contentView.constraints {
                        if constraint.identifier == "spacing_2" {
                            constraint.constant = UiUtility.adaptiveSize(size: 8)
                        }
                    }
                    
                    for constraint in ivIcon.constraints {
                        if constraint.identifier == "icon_width" {
                            constraint.constant = UiUtility.adaptiveSize(size: 20)
                        }
                    }
                    
                    for constraint in vContainer.constraints {
                        if constraint.identifier == "spacing_0" {
                            constraint.constant = UiUtility.adaptiveSize(size: 2)
                        } else if constraint.identifier == "spacing_1" {
                            constraint.constant = UiUtility.adaptiveSize(size: 2)
                        }
                    }
                    
                    vContainer.backgroundColor = UiUtility.colorBackgroundWhite
                    
                    lbCount.textColor = UiUtility.colorDarkGrey
                    lbCount.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
                    lbCount.text = UiUtility.numberStringWithComma(club.memberCount)
                    
                    lbText.textColor = UiUtility.colorDarkGrey
                    lbText.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 16))
                    lbText.text = "members".localized
                    
                    btnAdd.isHidden = !isJoined()
                    btnAdd.addTarget(self, action: #selector(addMemberClicked(_:)), for: .touchUpInside)
                case .join:
                    cell = tableView.dequeueReusableCell(withIdentifier: "JoinCell", for: indexPath)
                    
                    let btnJoin: UIButton = cell.viewWithTag(1) as! UIButton
                    btnJoin.addTarget(self, action: #selector(buttonJoinClicked(_:)), for: .touchUpInside)
                }
                
                cell.backgroundColor = UiUtility.colorBackgroundWhite
            case .post:
                let post: ClubPost = posts[indexPath.row]
                
                if post.isTopping() {
                    cell = tableView.dequeueReusableCell(withIdentifier: "TopFeedCell", for: indexPath)
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath)
                }
                
                let clubItem: ClubItemView = cell.viewWithTag(1) as! ClubItemView
                for constraint in cell.contentView.constraints {
                    if constraint.identifier == "marginBottom" {
                        constraint.constant = UiUtility.adaptiveSize(size: 6)
                    }
                }
                
                
                clubItem.setupUi(with: post, isList: true, inPerson: false)
                clubItem.buttonUser.addTarget(self, action: #selector(userClicked(_:)), for: .touchUpInside)
                clubItem.buttonFollow.addTarget(self, action: #selector(followClicked(_:)), for: .touchUpInside)
                clubItem.buttonMore.addTarget(self, action: #selector(moreClicked(_:)), for: .touchUpInside)
                clubItem.buttonLike.addTarget(self, action: #selector(likeClicked(_:)), for: .touchUpInside)
                clubItem.buttonLikeList.addTarget(self, action: #selector(likersClicked(_:)), for: .touchUpInside)
                clubItem.buttonTrack.addTarget(self, action: #selector(trackClicked(_:)), for: .touchUpInside)
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if tableView == add1tableView {
            newAdmin = members[indexPath.row]
            tableView.reloadData()
        } else {
            if indexPath.section == Section.club.rawValue {
                if indexPath.row == RowClub.member.rawValue {
                    goClubMember(browse: true)
                }
            } else if indexPath.section == Section.post.rawValue {
                let post: ClubPost = posts[indexPath.row]
                
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedDetailViewController") as! FeedDetailViewController
                vc.clubPost = post
                vc.clubUpdateDelegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    
    // MARK: private methods
    
    private func isJoined() -> Bool {
        return club.isMember()
    }
    
    private func isManager() -> Bool {
        return club.isManager()
    }
    
    private func calculateNewsCellHeight(post: ClubPost) -> CGFloat {
        let width = UiUtility.adaptiveSize(size: 375 - 20 - 20)
        let font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))!
        let maxContentHeight: CGFloat = "line_4_text".localized.height(withConstrainedWidth: width, font: font)

        var h: CGFloat = 0
        if post.isTopping() {
            h += UiUtility.adaptiveSize(size: 20) // pin
        }
        h += UiUtility.adaptiveSize(size: 70) // height before content
        
        if !Utility.isEmpty(post.content) {
            h += min(post.content!.height(withConstrainedWidth: width, font: font), maxContentHeight)
        }

        h += UiUtility.adaptiveSize(size: 36) // date
        h += UiUtility.adaptiveSize(size: 375) // image
        h += UiUtility.adaptiveSize(size: 50) // footer
        h += UiUtility.adaptiveSize(size: 6) // separator
        
        return h
    }
    
    @objc private func addMemberClicked(_ sender: UIButton) {
        
    }
    
    @objc private func buttonJoinClicked(_ sender: UIButton) {
        ClubManager.shared().memberAdd(clubId: club.id, listener: self)
    }
    
    @IBAction private func addPostClicked(_ sender: UIButton) {
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditPostViewController") as! EditPostViewController
//        vc.editingPost = post
//        vc.contentDelegate = contentDelegate
//        self.navigationController?.pushViewController(vc, animated: true)
        
        CarManager.shared().getCars(remotely: true, listener: self)
    }
    
    func goClubMember(browse: Bool) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ClubMemberViewController") as! ClubMemberViewController
        vc.mode = browse ? .browse : .delete
        vc.club = club
        vc.clubUpdateDelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: card
    
    @objc func draggedCardView(_ sender:UIPanGestureRecognizer) {
        if keyboardShowing {
            return
        }
        //        self.view.bringSubviewToFront(containerCard)
        let translation = sender.translation(in: self.view)
        
        containerCard.center = CGPoint(x: containerCard.center.x/* + translation.x*/, y: min(cardMaxCenterY, max(containerCard.center.y + translation.y, cardMinCenterY)))
        sender.setTranslation(CGPoint.zero, in: self.view)
        
        
        
        //        if sender.state == UIGestureRecognizer.State.began {
        //            Logger.d("began")
        //        } else if sender.state == UIGestureRecognizer.State.changed {
        //            Logger.d("changed")
        //        } else
        if sender.state == UIGestureRecognizer.State.ended {
            //            Logger.d("ended")
            let velocity = sender.velocity(in: view)
            if velocity.y > 0 {
                UIView.animate(withDuration: 0.3, animations: {
                    self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardMaxCenterY)
                }) { (finished) in
                    self.viewShieldCard.isHidden = true
                    self.containerCard.isHidden = true
                    
                    self.afterClosingCard()
                }
            } else if velocity.y < 0 {
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardMinCenterY)
                    self.viewShieldCard.isHidden = false
                    self.containerCard.isHidden = false
                })
            } else {
                if self.containerCard.center.y - self.cardMinCenterY > self.cardMaxCenterY - self.containerCard.center.y {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardMaxCenterY)
                    }) { (finished) in
                        self.viewShieldCard.isHidden = true
                        self.containerCard.isHidden = true
                        
                        self.afterClosingCard()
                    }
                } else {
                    UIView.animate(withDuration: 0.3, animations: { () -> Void in
                        self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardMinCenterY)
                        self.viewShieldCard.isHidden = false
                        self.containerCard.isHidden = false
                    })
                }
            }
        }
    }
    
    private func addCardDirectly() {
        // prepare adding data
//        CarManager.shared().startAddingCar()
        
        if cardMinCenterY == 0 && cardMaxCenterY == 0 {
            cardMinCenterY = containerCard.center.y
            //            Logger.d("card height: \(containerAdd.frame.height)")
            //            Logger.d("self.view height: \(view.frame.height)")
            
            cardMaxCenterY = view.frame.height + (containerCard.frame.height / 2)
            
            //            Logger.d("addMinCenterY: \(addMinCenterY)")
            //            Logger.d("addMaxCenterY: \(addMaxCenterY)")
        }
        setCardVisibility(true, animated: true, isInit: true)
    }
    
    private func setCardVisibility(_ set: Bool, animated: Bool, isInit: Bool) {
        if animated {
            if set {
                self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardMaxCenterY)
                self.containerCard.isHidden = !set
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardMinCenterY)
                    self.viewShieldCard.isHidden = !set
                    
                    if isInit {
                        self.initCard0Ui(isFirstCar: CarManager.shared().isAddingFirstCar())
                    }
                })
            } else {
                self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardMinCenterY)
                
                UIView.animate(withDuration: 0.3, animations: {
                    self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardMaxCenterY)
                }) { (finished) in
                    self.viewShieldCard.isHidden = !set
                    self.containerCard.isHidden = !set
                    
                    self.afterClosingCard()
                }
            }
        } else {
            self.viewShieldCard.isHidden = !set
            self.containerCard.isHidden = !set
        }
    }
    
    private func afterClosingCard() {
//        if !self.viewAdd0.isHidden {
//            CarManager.shared().appendAddingCarInfo(name: self.add0valueOwnerName.textField.text, platePrefix: self.add0valuePlate0.textField.text, plateSuffix: self.add0valuePlate1.textField.text, vinLast5: self.add0valueVin.textField.text)
//        } else if !self.viewAdd2.isHidden {
//
//        }
//
//        if !viewAdd3.isHidden {
//            CarManager.shared().deleteAddingCar()
//            if selectedIndex == 1 {
//                let vc = viewControllers[selectedIndex]
//                if let naviController: UINavigationController = vc as? UINavigationController {
//                    let vcCar: CarViewController = naviController.viewControllers[0] as! CarViewController
//                    CarManager.shared().getCars(remotely: true, listener: vcCar)
//                }
//            }
//
//        } else {
//            showAddingCarWarning()
//        }
    }
    
    private func initCard0Ui(isFirstCar: Bool) {
        viewAdd0.isHidden = false
        viewAdd1.isHidden = true
        viewAdd2.isHidden = true
    }
    
    
    
    // MARK: step 0 of card
    
    @IBAction func card0NextClicked(_ sender: UIButton) {
        viewAdd0.isHidden = true
        viewAdd1.isHidden = false
        viewAdd2.isHidden = true
    }
    
    // MARK: step 1 of card
    
    @IBAction func backToCard0Clicked(_ sender: UIButton) {
        viewAdd0.isHidden = false
        viewAdd1.isHidden = true
        viewAdd2.isHidden = true
    }
    
    @IBAction func nextToCard2Clicked(_ sender: UIButton) {
        if newAdmin == nil {
            return
        }
        
        viewAdd0.isHidden = true
        viewAdd1.isHidden = true
        viewAdd2.isHidden = false
        
        updateCard2Ui()
    }
    
    // MARK: step 2 of card
    
    @IBAction func backToCard1Clicked(_ sender: UIButton) {
        viewAdd0.isHidden = true
        viewAdd1.isHidden = false
        viewAdd2.isHidden = true
    }
    
    @IBAction func finishCard2Clicked(_ sender: UIButton) {
        ClubManager.shared().leave(clubId: club.id, newAdminId: newAdmin!.accountId!, listener: self)
//        setCardVisibility(false, animated: true, isInit: true)
    }
    
    private func updateCard2Ui() {
        
        add2Photo.backgroundColor = UiUtility.colorBackgroundWhite
        add2Photo.layer.masksToBounds = true
        add2Photo.layer.cornerRadius = UiUtility.adaptiveSize(size: 20)
        if let stickerURL = newAdmin!.stickerURL {
            let url = URL(string: stickerURL)
            add2Photo.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_photo"))
        }
        
        // image check
        add2Check.isHidden = true
//        if let level = newAdmin!.level {
//            if level > 2 {
//                add2Check.isHidden = false
//                add2Check.image = UIImage.init(named: "check_golden_large")
//            } else if level > 1 {
//                add2Check.isHidden = false
//                add2Check.image = UIImage.init(named: "check_silver_large")
//            } else if level > 0 {
//                add2Check.isHidden = false
//                add2Check.image = UIImage.init(named: "check_large")
//            }
//        }
        
        add2Name.textColor = UIColor.black
        add2Name.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
        add2Name.text = newAdmin!.nickname
    }

    private func loadData() {
        ClubManager.shared().getSinglePage(clubId: club.id, clubPosts: self.posts, listener: self)
    }
    
    @objc private func followClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            let news: ClubPost = posts[indexPath.row]
            if let accountId = news.accountId {
                UserManager.shared().follow(accountId: accountId, set: true, listener: self)
            }
        }
    }
    
    @objc private func userClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            let news: ClubPost = posts[indexPath.row]
            if let _ = news.accountId {
                UiUtility.goBrowse(profile: Profile.init(with: news), controller: self)
            }
        }
    }
    
    @objc private func moreClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            let post: ClubPost = posts[indexPath.row]
            UiUtility.clubMoreMenu(from: self, post: post, clubProtocol: self, clubUpdateDelegate: self)
        }
    }
    
    @objc private func likeClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            let post: ClubPost = posts[indexPath.row]
            ClubManager.shared().like(news: post, set: (post.likeStatus == 0), listener: self)
        }
    }
    
    @objc private func trackClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            let post: ClubPost = posts[indexPath.row]
            ClubManager.shared().track(news: post, set: (post.bookmarkStatus == 0), listener: self)
        }
    }
    
    @objc private func likersClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            let post: ClubPost = posts[indexPath.row]
            UiUtility.goBrowseClubPostLikers(of: post, controller: self)
        }
    }
}
