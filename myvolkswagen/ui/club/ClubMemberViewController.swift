//
//  ClubMemberViewController.swift
//  myvolkswagen
//
//  Created by CHUNG CHING JIANG on 2019/9/30.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class ClubMemberViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource {
    
    enum Mode: Int {
        case browse = 0, delete
    }
    
    enum Section: Int {
        case content = 0, paddingFooter
        static var allCases: [Section] {
            var values: [Section] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    var mode: Mode = .browse
    var club: Club!
    var clubUpdateDelegate: ClubUpdateDelegate?
    
    
    var members: [ClubRelation] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var footerText0: UILabel!
    @IBOutlet weak var footerText1: UILabel!
    @IBOutlet weak var footerText2: UILabel!
    @IBOutlet weak var constraintFooter0: NSLayoutConstraint!
    @IBOutlet weak var constraintFooter1: NSLayoutConstraint!
    @IBOutlet weak var buttonFooterDone: UIButton!
    
    var removedUser: Set = Set<String>()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UiUtility.colorBackgroundWhite
        tableView.backgroundColor = UiUtility.colorBackgroundWhite
        
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        if mode == .browse {
            naviBar.setTitle("browse_club_member".localized)
        } else if mode == .delete {
            naviBar.setTitle("delete_club_member".localized)
        }
        
//        if isFollower {
//            followers = UserManager.shared().getRelation(of: of, isFollower: self.isFollower, fromRemote: true, listener: self)
//        } else {
//            followings = UserManager.shared().getRelation(of: of, isFollower: self.isFollower, fromRemote: true, listener: self)
//        }
        
        
        viewFooter.isHidden = (mode == .browse)
        if mode == .delete {
            viewFooter.backgroundColor = UiUtility.colorBackgroundWhite
            
            footerText0.textColor = UiUtility.colorDarkGrey
            footerText0.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
            footerText0.text = "total_deleted_member".localized
            
            footerText1.textColor = .black
            footerText1.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            
            footerText2.textColor = UiUtility.colorDarkGrey
            footerText2.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
            footerText2.text = "total_selected_friend_1".localized
            
            constraintFooter0.constant = UiUtility.adaptiveSize(size: 19.5)
            constraintFooter1.constant = UiUtility.adaptiveSize(size: 19.5)
            
            buttonFooterDone.setTitle("remove".localized, for: .normal)
            buttonFooterDone.setTitleColor(UiUtility.colorBackgroundOutgoingMessage, for: .normal)
            buttonFooterDone.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 17))
            
            updateFooterTotalText()
        }
        
        ClubManager.shared().getMembers(clubId: club.id, listener: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateUi()
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        self.back()
    }
    
    
    
    // MARK: club protocol
    
    override func didStartGetClubMembers() {
        super.didStartGetClubMembers()
    }
    
    override func didFinishGetClubMembers(success: Bool, relations: [ClubRelation]) {
        super.didFinishGetClubMembers(success: success, relations: relations)
        
        if success {
            members = relations
            tableView.reloadData()
        } else {
            Logger.d("failed to get members, back")
            navigationController?.popViewController(animated: true)
        }
    }
    
    override func didFinishDiscardMember(success: Bool) {
        super.didFinishDiscardMember(success: success)
        
        if success {
            ClubManager.shared().cleanCache()
            
            clubUpdateDelegate?.didFinishUpdateClub()
            navigationController?.popViewController(animated: true)
        }
    }
    
    
    
    // MARK: user protocol
    
    override func didStartFollow() {
        showHud()
    }
    
    override func didFinishFollow(success: Bool, accountId: String, set: Bool) {
        dismissHud()
        
        if success {
            for member in members {
                if accountId == member.accountId! {
                    member.isFollow = set ? 1 : 0
                    break
                }
            }
            updateUi()
        }
    }
    
    override func didStartGetRelation() {
        showHud()
    }
    
    override func didFinishGetRelation(success: Bool, isFollower: Bool, relations: [Relation]?) {
        dismissHud()
        
        if success {
//            if let relations = relations {
//                if isFollower {
//                    self.followers = relations
//                } else {
//                    self.followings = relations
//                }
//
//                updateUi()
//            }
        }
    }
    
    
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Section.init(rawValue: section)! {
        case .content:
//            if isFollower {
//                return followers.count
//            } else {
//                return followings.count
//            }
            return members.count
        case .paddingFooter:
            if mode == .delete {
                if getOperatingData().count == 0 {
                    return 0
                } else {
                    return 1
                }
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Section.init(rawValue: indexPath.section)! {
        case .content:
            return UiUtility.adaptiveSize(size: 80)
        case .paddingFooter:
            return UiUtility.adaptiveSize(size: 44)
        }
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        switch Section.init(rawValue: indexPath.section)! {
        case .content:
            if mode == .delete {
                cell = tableView.dequeueReusableCell(withIdentifier: "LikerCreateCell", for: indexPath)
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "LikerCell", for: indexPath)
            }
            
            let photo: UIImageView = cell.viewWithTag(1) as! UIImageView
            let name: UILabel = cell.viewWithTag(2) as! UILabel
            let button: UIButton = cell.viewWithTag(3) as! UIButton
            let imageCheck: UIImageView = cell.viewWithTag(4) as! UIImageView
            
            let member = getItem(at: indexPath)
            
            photo.backgroundColor = UiUtility.colorBackgroundWhite
            photo.layer.masksToBounds = true
            photo.layer.cornerRadius = UiUtility.adaptiveSize(size: 30)
            if let stickerURL = member.stickerURL {
                let url = URL(string: stickerURL)
                photo.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_photo"))
            }
            
            // image check
            imageCheck.isHidden = true
//            if let level = member.level {
//                if level > 2 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_golden_large")
//                } else if level > 1 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_silver_large")
//                } else if level > 0 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_large")
//                }
//            }
            
            name.textColor = UIColor.black
            name.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            name.text = member.nickname
            
//
//
            
            if mode == .delete {
                button.isSelected = removedUser.contains(member.accountId!)
            } else {
                button.isHidden = member.isMe() || member.isOfficial()
                button.setImage(member.isFollowedByMe() ? UIImage.init(named: "following_small") : UIImage.init(named: "follow_small"), for: .normal)
//                button.setImage(UIImage.init(named: "follow_small"), for: .normal)
            }
            button.addTarget(self, action: #selector(buttonSelectClicked(_:)), for: .touchUpInside)
        case .paddingFooter:
            cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath)
            cell.backgroundColor = UiUtility.colorBackgroundWhite
            cell.selectionStyle = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == Section.content.rawValue {
            let member = getItem(at: indexPath)
            
            if mode == .delete {
                if member.isMe() {
                    return
                }
                
                if removedUser.contains(member.accountId!) {
                    removedUser.remove(member.accountId!)
                } else {
                    removedUser.insert(member.accountId!)
                }
                
                tableView.reloadData()
                updateFooterTotalText()
            } else {
                UiUtility.goBrowse(profile: Profile.init(with: member), controller: self)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func getItem(at position: IndexPath) -> ClubRelation {
        // no boundry check
        return getOperatingData()[position.row]
    }
    
    private func getOperatingData() -> [ClubRelation] {
        return members
    }
    
    private func updateUi() {
        // table view
        tableView.reloadData()
    }

    
    
    @objc private func buttonSelectClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            let member: ClubRelation = getItem(at: indexPath)
            
            if member.isFollowedByMe() {
                // unfollow
                UserManager.shared().follow(accountId: member.accountId!, set: false, listener: self)
            } else {
                // follow
                UserManager.shared().follow(accountId: member.accountId!, set: true, listener: self)
            }
        }
    }
    
    @IBAction private func footerDoneClicked(_ sender: UIButton) {
        if mode == .delete {
            if removedUser.count == 0 {
                return
            }
            
            let alert = UIAlertController(title: nil, message: "warning_remove_member".localized, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "not_now".localized, style: .default, handler: nil))
            
            alert.addAction(UIAlertAction.init(title: "remove".localized, style: .destructive, handler:  { _ in
                ClubManager.shared().discardMember(clubId: self.club.id, members: Array(self.removedUser), listener: self)
            }))
            
            self.present(alert, animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    private func updateFooterTotalText() {
        footerText1.text = "\(removedUser.count)"
    }
}
