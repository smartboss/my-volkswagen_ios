//
//  ClubListViewController.swift
//  myvolkswagen
//
//  Created by CHUNG CHING JIANG on 2019/10/1.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class ClubListViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource, ClubUpdateDelegate {
    
    enum Mode: Int {
        case joined = 1, managed
    }
    
    enum Sort: Int {
        case mostBrowsed = 1, latestPost, latestJoined, member
    }
    
    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var buttonSort: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var mode: Mode!
    var sort: Sort = .mostBrowsed
    private var clubs: [Club] = []
    
    private var clubUpdated: Bool = false
    
    @IBOutlet weak var viewEmpty: UIView!
    @IBOutlet weak var labelEmpty: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UiUtility.colorBackgroundWhite
        tableView.backgroundColor = UiUtility.colorBackgroundWhite
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        if mode == .joined {
            naviBar.setTitle("club_i_joined".localized)
        } else {
            naviBar.setTitle("club_i_managed".localized)
        }
        
        
        labelHeader.textColor = UiUtility.colorDark4A
        labelHeader.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
        labelHeader.text = "club_list_header".localized
        
        buttonSort.setTitleColor(UiUtility.colorBackgroundOutgoingMessage, for: .normal)
        buttonSort.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
        buttonSort.setTitle("sort".localized, for: .normal)
        
        viewEmpty.isHidden = true
        viewEmpty.backgroundColor = UiUtility.colorBackgroundWhite
        labelEmpty.textColor = UiUtility.colorDark4A
        labelEmpty.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
        labelEmpty.text = "club_list_empty_text".localized
        
        ClubManager.shared().getMyClubList(kind: mode.rawValue, sort: sort.rawValue, listener: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if clubUpdated {
            clubUpdated = false
            ClubManager.shared().getMyClubList(kind: mode.rawValue, sort: sort.rawValue, listener: self)
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        self.back()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    // MARK: club update delegate
    
    func didFinishUpdateClub() {
        clubUpdated = true
    }
    
    
    
    // MARK: club protocol
    
    override func didFinishGetMyClubList(success: Bool, result: [Club]) {
        super.didFinishGetMyClubList(success: success, result: result)
        
        if success {
            clubs = result
            
            if clubs.count == 0 {
                viewEmpty.isHidden = false
            } else {
                viewEmpty.isHidden = true
                tableView.reloadData()
            }
        }
    }
    
    
    
    // MARK: table view delegate

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clubs.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiUtility.adaptiveSize(size: 80)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        cell = tableView.dequeueReusableCell(withIdentifier: "ClubCell", for: indexPath)
        let ivPhoto: UIImageView = cell.viewWithTag(1) as! UIImageView
        let lbTitle: UILabel = cell.viewWithTag(3) as! UILabel
        let lbDate: UILabel = cell.viewWithTag(4) as! UILabel
        
        let club = clubs[indexPath.row]
        
        ivPhoto.backgroundColor = UiUtility.colorBackgroundIncomingMessage
        if let photoUrl = club.picUrl {
            let url = URL(string: photoUrl)
            ivPhoto.kf.setImage(with: url)
        }
        lbTitle.textColor = UiUtility.colorDark4A
        lbTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
        lbTitle.text = club.name
        lbDate.textColor = UiUtility.colorBrownGrey
        lbDate.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
        lbDate.text = club.getDateText()
        
        cell.selectionStyle = .none
        cell.backgroundColor = UiUtility.colorBackgroundWhite
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        goClub(clubs[indexPath.row])
    }
    
    
    
    // MARK: private methods
    
    @IBAction private func sortClicked(_ sender: Any) {
        let alertController = UIAlertController(title: "club_sort_title".localized, message: nil, preferredStyle: .actionSheet)
        
        let actionBrowse = UIAlertAction(title: "club_sort_browse".localized, style: .default) { (action:UIAlertAction) in
            self.sort = .mostBrowsed
            ClubManager.shared().getMyClubList(kind: self.mode.rawValue, sort: self.sort.rawValue, listener: self)
        }
        
        let actionLatestPost = UIAlertAction(title: "club_sort_latest_post".localized, style: .default) { (action:UIAlertAction) in
            self.sort = .latestPost
            ClubManager.shared().getMyClubList(kind: self.mode.rawValue, sort: self.sort.rawValue, listener: self)
        }
        
        let actionLatestJoin = UIAlertAction(title: "club_sort_latest_join".localized, style: .default) { (action:UIAlertAction) in
            self.sort = .latestJoined
            ClubManager.shared().getMyClubList(kind: self.mode.rawValue, sort: self.sort.rawValue, listener: self)
        }
        
        let actionMember = UIAlertAction(title: "club_sort_member".localized, style: .default) { (action:UIAlertAction) in
            self.sort = .member
            ClubManager.shared().getMyClubList(kind: self.mode.rawValue, sort: self.sort.rawValue, listener: self)
        }
        
        let actionCancel = UIAlertAction(title: "cancel".localized, style: .cancel) { (action:UIAlertAction) in
            
        }
        
        alertController.addAction(actionBrowse)
        alertController.addAction(actionLatestPost)
        alertController.addAction(actionLatestJoin)
        
        alertController.addAction(actionMember)
        alertController.addAction(actionCancel)
        
        present(alertController, animated: true, completion: nil)
    }
    
    private func goClub(_ club: Club) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ClubViewController") as! ClubViewController
        vc.club = club
        vc.clubUpdateDelegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction private func createClicked(_ sender: Any) {
        let navi = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateClubNavi") as! UINavigationController
//        (navi.viewControllers[0] as! CreateClubViewController).clubCarModels = clubCarModels
        (navi.viewControllers[0] as! CreateClubViewController).clubUpdateDelegate = self
        self.present(navi, animated: true, completion: nil)
    }

}
