//
//  ExploreClubViewController.swift
//  myvolkswagen
//
//  Created by CHUNG CHING JIANG on 2019/10/7.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class ExploreClubViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {
    
    enum SectionClub: Int {
        case popular = 0, latest, kind, club//, yours, post
        static var allCases: [SectionClub] {
            var values: [SectionClub] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    
    
//    var cellHeightsDictionaryFeed: [IndexPath: CGFloat]!
    
//    private let refreshControl = UIRefreshControl()
    
    
    // club
    @IBOutlet weak var viewClubContainer: UIView!
    @IBOutlet weak var viewClubSearchContainer: UIView!
    @IBOutlet weak var textFieldSearchClub: UITextField!
//    @IBOutlet weak var constraintCreateClubLeading: NSLayoutConstraint!
//    @IBOutlet weak var buttonExploreClub: UIButton!
    @IBOutlet weak var tableViewClub: UITableView!
    weak var collectionViewClubPopular: UICollectionView!
    weak var collectionViewClubLatest: UICollectionView!
    weak var collectionViewClubKind: UICollectionView!
//    weak var collectionViewClubYours: UICollectionView!
    var cellHeightsDictionaryClub: [IndexPath: CGFloat]!
    
    var clubList: [Club] = []
    var clubYours: [Club] = []
    var clubNewsList: [News] = []
    var clubCarModels: [CarModel2] = []
    var clubSort = ClubSort.latestClub
    var clubSearchSort = ClubJoinedSort.mostBrowsed
    
    var clubSearchResult: [Club]?
    
    

    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()
        
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        naviBar.setTitle("explore_club".localized)
        
//        naviBar.setLeftButton(UIImage(named: "vw"), highlighted: UIImage(named: "vw"))
//        naviBar.setRightButton(UIImage(named: "notif_black"), highlighted: UIImage(named: "notif_black_unread"))
//        imageViewNotification = naviBar.imageViewRight
//        naviBar.setArrowVisibility(true)
        
//        emptyView.backgroundColor = UiUtility.colorBackgroundWhite
//        emptyText.textColor = UIColor(rgb: 0x4A4A4A)
//        emptyText.font = UIFont(name: "PingFangTC-Regular", size: 15)
//        emptyText.text = "feed_no_content".localized
        
//        containerKind.backgroundColor = UiUtility.colorBackgroundWhite
//        collectionViewKind.backgroundColor = UiUtility.colorBackgroundWhite
//        collectionViewKind.delaysContentTouches = false
//
//        extendedNavi.backgroundColor = UiUtility.colorBackgroundWhite
//        extendedNaviTitle0.textColor = UIColor.black
//        extendedNaviTitle0.font = UIFont(name: "PingFangTC-Medium", size: 18)
//        extendedNaviTitle1.textColor = UIColor.black
//        extendedNaviTitle1.font = UIFont(name: "PingFangTC-Medium", size: 18)
//
//        tableViewFeed.refreshControl = refreshControl
//        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
//
//        tableViewFeed.backgroundColor = UiUtility.colorBackgroundWhite
//        cellHeightsDictionaryFeed = [:]
//        tableViewFeed.rowHeight = UITableView.automaticDimension
        
        
        
        // club
        viewClubContainer.isHidden = true
        viewClubContainer.backgroundColor = UiUtility.colorBackgroundWhite
        viewClubSearchContainer.layer.cornerRadius = UiUtility.adaptiveSize(size: 14)
        textFieldSearchClub.placeholder = "search_club".localized
        textFieldSearchClub.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 16))
//        textFieldSearchClub.inputAccessoryView = toolbar
        textFieldSearchClub.delegate = self
        tableViewClub.backgroundColor = UiUtility.colorBackgroundWhite
        cellHeightsDictionaryClub = [:]
        tableViewClub.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        NewsManager.shared().getCarModel(remotely: true, listener: self)
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        self.back()
    }
    
    
    
    // MARK: keyboard
    
    @objc override func keyboardWillShow(notification: NSNotification) {
        keyboardShowing = true
    }
    
    @objc override func keyboardWillHide(notification: NSNotification) {
        keyboardShowing = false
    }
    
    
    
    // MARK: text field delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if let text = textField.text {
            let trimmedText = text.trimmingCharacters(in: .whitespaces)
            if trimmedText.count != 0 {
                ClubManager.shared().search(sort: self.clubSort, keyword: trimmedText, listener: self)
            } else {
                clubSearchResult = nil
                ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(), listener: self)
            }
        } else {
            clubSearchResult = nil
            ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(), listener: self)
        }
        
        return true
    }
    
    
    
    // MARK: context label delegate
    
//    func contextLabel(_ sender: ContextLabel, didTouchWithTouchResult touchResult: TouchResult) {
//        switch touchResult.state {
//        case .began:
//            Logger.d("began, touchResult: \(touchResult)")
//        case .ended:
//            Logger.d("ended, touchResult: \(touchResult)")
//        default:
//            break
//        }
//    }
    
    
    
//    // MARK: image picker controller delegate
//
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        picker.dismiss(animated: true)
//
//        guard let image = info[.editedImage] as? UIImage else {
//            Logger.d("No image found")
//            return
//        }
//
//        // print out the image size as a test
////        Logger.d("image size: \(image.size)")
//
//        UiUtility.goEditPost(from: self, post: Post.init(with: image), contentDelegate: nil)
//    }
    
    
    
    // MARK: club sort delegate
    
    override func setSort(type: ClubSort) {
        if clubSort != type {
            clubSort = type
//            ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(), listener: self)
            
            if let text = textFieldSearchClub.text {
                let trimmedText = text.trimmingCharacters(in: .whitespaces)
                if trimmedText.count != 0 {
                    ClubManager.shared().search(sort: self.clubSort, keyword: trimmedText, listener: self)
                } else {
                    clubSearchResult = nil
                    ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(), listener: self)
                }
            } else {
                clubSearchResult = nil
                ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(), listener: self)
            }
        }
    }
    
    
    
    // MARK: club protocol
    
//    override func didStartGetJoinedClubCount() {
//        super.didStartGetJoinedClubCount()
//    }
//
//    override func didFinishGetJoinedClubCount(success: Bool, count: Int?) {
//        super.didFinishGetJoinedClubCount(success: success, count: count)
//
//        if count == nil {
//            Logger.d("failed to get joined club count remotely")
//        } else {
//            switchMode(kind: .club)
//        }
//    }
    
    override func didStartGetGuestData() {
        super.didStartGetGuestData()
    }
    
    override func didFinishGetGuestData(success: Bool, guestClubData: GuestClubData?) {
        super.didFinishGetGuestData(success: success, guestClubData: guestClubData)
        
        if success {
            ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(), listener: self)
            
            Logger.d("reload table view after didFinishGetGuestData")
            tableViewClub.reloadData()
        }
    }
    
    override func didStartGetGuestClubs() {
        super.didStartGetGuestClubs()
    }
    
    override func didFinishGetGuestClubs(success: Bool, guestClubs: [Club]?) {
        super.didFinishGetGuestClubs(success: success, guestClubs: guestClubs)
        
        if success {
            Logger.d("reload table view after didFinishGetGuestClubs")
//            tableViewClub.reloadSections([SectionClub.club.rawValue], with: .automatic)
            tableViewClub.reloadData()
        }
    }
    
//    override func didStartGetClubData() {
//        super.didStartGetClubData()
//    }
//
//    override func didFinishGetClubData(success: Bool, clubData: ClubData?) {
//        super.didFinishGetClubData(success: success, clubData: clubData)
//
//        if success {
//            Logger.d("reload table view after didFinishGetClubData")
//            tableViewClub.reloadData()
//        }
//    }
    
    override func didStartSearchClub() {
        super.didStartSearchClub()
    }
    
    override func didFinishSearchClub(success: Bool, result: [Club]) {
        super.didFinishSearchClub(success: success, result: result)
        
        if success {
            clubSearchResult = result
            tableViewClub.reloadData()
        }
    }
    
    override func didFinishMemberAdd(success: Bool) {
        super.didFinishMemberAdd(success: success)
        
        if success {
            ClubManager.shared().cleanCache()
//            ClubManager.shared().getJoinedClubCount(remotely: true, listener: self)
            if let text = textFieldSearchClub.text {
                let trimmedText = text.trimmingCharacters(in: .whitespaces)
                if trimmedText.count != 0 {
                    ClubManager.shared().search(sort: self.clubSort, keyword: trimmedText, listener: self)
                } else {
                    clubSearchResult = nil
                    ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(), listener: self)
                }
            } else {
                clubSearchResult = nil
                ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(), listener: self)
            }
        } else {
            Logger.d("failed to member add")
        }
    }
    
    
    
//    // MARK: user protocol
//
//    override func didStartFollow() {
//        showHud()
//    }
//
//    override func didFinishFollow(success: Bool, accountId: String, set: Bool) {
//        dismissHud()
//
//        if success {
//            for n in newsList {
//                if let na = n.accountId {
//                    if na == accountId {
//                        if set {
//                            n.isFollow = 1
//                        } else {
//                            n.isFollow = 0
//                        }
//                    }
//                }
//            }
//            updateFeedUi()
//        }
//    }
    
    
    
//    // MARK: car protocol
//
//    override func didStartGetCar() {
//        showHud()
//    }
//
//    override func didFinishGetCar(success: Bool, cars: [Car]) {
//        dismissHud()
//
//        if success {
//            if cars.count == 0 {
//                self.tabControllerDelegate?.addPostClicked()
//            } else {
//                // add new post
//                UiUtility.pickImage(from: self, delegate: self)
//            }
//        }
//    }
    
    
    
    // MARK: news protocol
    
    override func didStartGetNewsCarModel() {
        showHud()
    }
    
    override func didFinishGetNewsCarModel(success: Bool, carModels: [Model]?) {
        dismissHud()
        
        if success {
//            Logger.d("get carModels: \(String(describing: carModels))")
            
            if let carModels = carModels {
                self.clubCarModels = []
                for model in carModels {
                    let clubCarModel = CarModel2.init(model: model)
                    self.clubCarModels.append(clubCarModel)
                }
                
//                self.collectionViewClubKind?.reloadData()
            }
            
            setClubView()
        }
    }
    
//    override func didStartGetNewsList(loadMore: Bool) {
//        if !loadMore {
//            showHud()
//        }
//    }
//
//    override func didFinishGetNewsList(success: Bool, loadMore: Bool, newsList: [News]?, newsKind: NewsKind) {
//        if !loadMore {
//            dismissHud()
//        }
//        if refreshControl.isRefreshing {
//            refreshControl.endRefreshing()
//        }
//
//        if success {
//            contentIsDirtyRemote = false
//            if newsKind == self.newsKind {
//                if let newsList = newsList {
//                    self.newsList = newsList
//                    updateFeedUi()
//                }
//            }
//        }
//    }
//
//    override func didStartLikeNews() {
//        showHud()
//    }
//
//    override func didFinishLikeNews(success: Bool, news: News, set: Bool) {
//        dismissHud()
//
//        if success {
//            var index = 0
//            for n in newsList {
//                if n.newsId! == news.newsId! {
//                    if set {
//                        n.likeStatus = 1
//                        n.likeAmount = n.likeAmount + 1
//                    } else {
//                        n.likeStatus = 0
//                        n.likeAmount = n.likeAmount - 1
//                    }
//                    updateFeedUi()
//                    break
//                }
//                index += 1
//            }
//        }
//    }
//
//    override func didStartTrackNews() {
//        showHud()
//    }
//
//    override func didFinishTrackNews(success: Bool, news: News, set: Bool) {
//        dismissHud()
//
//        if success {
//            for n in newsList {
//                if n.newsId! == news.newsId! {
//                    if set {
//                        n.bookmarkStatus = 1
//                        n.bookmarkAmount = n.bookmarkAmount + 1
//                    } else {
//                        n.bookmarkStatus = 0
//                        n.bookmarkAmount = n.bookmarkAmount - 1
//                    }
//                    updateFeedUi()
//                    break
//                }
//            }
//        }
//    }
//
//    override func didStartReportNews() {
//        showHud()
//    }
//
//    override func didFinishReportNews(success: Bool, news: News, type: Int) {
//        dismissHud()
//
//        if success {
//            self.tabControllerDelegate?.goFinishReport()
//        }
//    }
//
//    override func didStartDeletePost() {
//        showHud()
//    }
//
//    override func didFinishDeletePost(success: Bool, news: News?) {
//        dismissHud()
//
//        if success {
//            // TODO: you can do it better
//            NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: false, listener: self)
//        }
//    }
    
    
    
    
    // MARK: collection view delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewClubPopular {
            if let guestClubData = ClubManager.shared().getGuestData(remotely: false, listener: self) {
                return guestClubData.hotClubs.count
            }
            return 0
        } else if collectionView == collectionViewClubLatest {
            if let guestClubData = ClubManager.shared().getGuestData(remotely: false, listener: self) {
                return guestClubData.newClubs.count
            }
            return 0
        } else if collectionView == collectionViewClubKind {
            return clubCarModels.count
//        } else if collectionView == collectionViewClubYours {
//            if let clubData = ClubManager.shared().getClubData(remotely: false, listener: self) {
//                return clubData.clubs.count
//            }
//            return 0
        } else {
            Logger.d("numberOfItemsInSection should not go here")
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == collectionViewClubPopular {
            let paddingH = UiUtility.adaptiveSize(size: 20)
            return UIEdgeInsets(top: .zero, left: paddingH, bottom: .zero, right: paddingH)
        } else if collectionView == collectionViewClubLatest {
            let paddingH = UiUtility.adaptiveSize(size: 20)
            return UIEdgeInsets(top: .zero, left: paddingH, bottom: .zero, right: paddingH)
        } else if collectionView == collectionViewClubKind {
            let paddingH = UiUtility.adaptiveSize(size: 20)
            return UIEdgeInsets(top: .zero, left: paddingH, bottom: .zero, right: paddingH)
//        } else if collectionView == collectionViewClubYours {
//            let paddingH = UiUtility.adaptiveSize(size: 20)
//            return UIEdgeInsets(top: .zero, left: paddingH, bottom: .zero, right: paddingH)
        } else {
            Logger.d("insetForSectionAt should not go here")
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionViewClubPopular {
            return UiUtility.adaptiveSize(size: 6)
        } else if collectionView == collectionViewClubLatest {
            return UiUtility.adaptiveSize(size: 6)
        } else if collectionView == collectionViewClubKind {
            return UiUtility.adaptiveSize(size: 6)
//        } else if collectionView == collectionViewClubYours {
//            return UiUtility.adaptiveSize(size: 6)
        } else {
            Logger.d("minimumInteritemSpacingForSectionAt should not go here")
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewClubPopular {
            return CGSize.init(width: UiUtility.adaptiveSize(size: 150), height: UiUtility.adaptiveSize(size: 149))
        } else if collectionView == collectionViewClubLatest {
            return CGSize.init(width: UiUtility.adaptiveSize(size: 150), height: UiUtility.adaptiveSize(size: 149))
        } else if collectionView == collectionViewClubKind {
            let height: CGFloat = UiUtility.adaptiveSize(size: 30)
            let modelName = clubCarModels[indexPath.row].model.name!
            let modelNameWidth = modelName.width(withConstrainedHeight: height, font: UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!)
            return CGSize(width: modelNameWidth + UiUtility.adaptiveSize(size: 32), height: height)
//        } else if collectionView == collectionViewClubYours {
//            return CGSize.init(width: UiUtility.adaptiveSize(size: 150), height: UiUtility.adaptiveSize(size: 149))
        } else {
            Logger.d("sizeForItemAt should not go here")
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell!
        
        if collectionView == collectionViewClubPopular {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClubCell", for: indexPath)
            cell.clipsToBounds = false
            
            let ivPhoto: UIImageView = cell.viewWithTag(1) as! UIImageView
            let lbName: UILabel = cell.viewWithTag(2) as! UILabel
            
            ivPhoto.backgroundColor = UiUtility.colorBackgroundIncomingMessage
            ivPhoto.layer.cornerRadius = UiUtility.adaptiveSize(size: 6)
            
            for constraint in lbName.constraints {
                if constraint.identifier == "name_width" {
                    constraint.constant = UiUtility.adaptiveSize(size: 150)
                }
            }
            
            let club = ClubManager.shared().getGuestData(remotely: false, listener: self)!.hotClubs[indexPath.row]
            
            if let photoUrl = club.picUrl {
                let url = URL(string: photoUrl)
                ivPhoto.kf.setImage(with: url)
            }
            
            lbName.font = UIFont(name: "PingFangTC-Medium", size: 12)
            lbName.textColor = UiUtility.colorDarkGrey
            lbName.text = "test_club_name".localized
            lbName.text = club.name
        } else if collectionView == collectionViewClubLatest {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClubCell", for: indexPath)
            cell.clipsToBounds = false

            let ivPhoto: UIImageView = cell.viewWithTag(1) as! UIImageView
            let lbName: UILabel = cell.viewWithTag(2) as! UILabel

            ivPhoto.backgroundColor = UiUtility.colorBackgroundIncomingMessage
            ivPhoto.layer.cornerRadius = UiUtility.adaptiveSize(size: 6)

            for constraint in lbName.constraints {
                if constraint.identifier == "name_width" {
                    constraint.constant = UiUtility.adaptiveSize(size: 150)
                }
            }
            
            let club = ClubManager.shared().getGuestData(remotely: false, listener: self)!.newClubs[indexPath.row]
            
            if let photoUrl = club.picUrl {
                let url = URL(string: photoUrl)
                ivPhoto.kf.setImage(with: url)
            }

            lbName.font = UIFont(name: "PingFangTC-Medium", size: 12)
            lbName.textColor = UiUtility.colorDarkGrey
            lbName.text = club.name
        } else if collectionView == collectionViewClubKind {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KindCell", for: indexPath)
            cell.clipsToBounds = false
//            cell.backgroundColor = .red
            
            let viewContainer: UIView = cell.viewWithTag(2)!
            let lbName: UILabel = cell.viewWithTag(3) as! UILabel

            let carModel = clubCarModels[indexPath.row]

            viewContainer.layer.cornerRadius = UiUtility.adaptiveSize(size: 15)
            viewContainer.layer.shadowColor = UIColor.black.cgColor
            viewContainer.layer.shadowOpacity = 0.3
            viewContainer.layer.shadowOffset = CGSize(width: 0, height: 1)
            viewContainer.layer.shadowRadius = UiUtility.adaptiveSize(size: 1.0)
            
            lbName.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            lbName.text = carModel.model.name

            if carModel.selected {
                lbName.textColor = .white
                viewContainer.backgroundColor = UiUtility.getModelSelectedColor(of: indexPath.row)
            } else {
                lbName.textColor = UiUtility.colorBrownGrey
                viewContainer.backgroundColor = .white
            }
//        } else if collectionView == collectionViewClubYours {
//            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClubCell", for: indexPath)
//            cell.clipsToBounds = false
//
//            let ivPhoto: UIImageView = cell.viewWithTag(1) as! UIImageView
//            let lbName: UILabel = cell.viewWithTag(2) as! UILabel
//
//            ivPhoto.backgroundColor = UiUtility.colorBackgroundIncomingMessage
//            ivPhoto.layer.cornerRadius = UiUtility.adaptiveSize(size: 6)
//
//            for constraint in lbName.constraints {
//                if constraint.identifier == "name_width" {
//                    constraint.constant = UiUtility.adaptiveSize(size: 150)
//                }
//            }
//
//            let club = ClubManager.shared().getClubData(remotely: false, listener: self)!.clubs[indexPath.row]
//
//            if let photoUrl = club.picUrl {
//                let url = URL(string: photoUrl)
//                ivPhoto.kf.setImage(with: url)
//            }
//
//            lbName.font = UIFont(name: "PingFangTC-Medium", size: 12)
//            lbName.textColor = UiUtility.colorDarkGrey
//            lbName.text = club.name
        } else {
            Logger.d("cellForItemAt should not go here")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewClubPopular {
            self.goClub(ClubManager.shared().getGuestData(remotely: false, listener: self)!.hotClubs[indexPath.row])
        } else if collectionView == collectionViewClubLatest {
            self.goClub(ClubManager.shared().getGuestData(remotely: false, listener: self)!.newClubs[indexPath.row])
        } else if collectionView == collectionViewClubKind {
            let carModel: CarModel2 = clubCarModels[indexPath.row]
            carModel.selected = !carModel.selected
            collectionViewClubKind.reloadData()
            
            ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(), listener: self)
//        } else if collectionView == collectionViewClubYours {
//            self.tabControllerDelegate?.goClub(ClubManager.shared().getClubData(remotely: false, listener: self)!.clubs[indexPath.row])
        }
    }




    // MARK: table view delegate

    func numberOfSections(in tableView: UITableView) -> Int {
        return SectionClub.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch SectionClub.init(rawValue: section)! {
        case .popular:
            if clubSearchResult != nil {
                return 0
            }
            
            if let guestClubInfo = ClubManager.shared().getGuestData(remotely: false, listener: self) {
                if guestClubInfo.hotClubs.count == 0 {
                    return 0
                } else {
                    return 1
                }
            } else {
                return 0
            }
        case .latest:
            if clubSearchResult != nil {
                return 0
            }
            
            if let guestClubInfo = ClubManager.shared().getGuestData(remotely: false, listener: self) {
                if guestClubInfo.newClubs.count == 0 {
                    return 0
                } else {
                    return 1
                }
            } else {
                return 0
            }
        case .kind:
            return 1
        case .club:
            if let clubSearchResult = clubSearchResult {
                return clubSearchResult.count
            } else {
                if let guestClubs = ClubManager.shared().getCachedGuestClubs() {
                    return guestClubs.count
                } else {
                    return 0
                }
            }
            
//        case .yours:
//            if hasJoinedClub() {
//                if let clubData = ClubManager.shared().getClubData(remotely: false, listener: self) {
//                    if clubData.clubs.count == 0 {
//                        return 0
//                    } else {
//                        return 1
//                    }
//                } else {
//                    return 0
//                }
//            } else {
//                return 0
//            }
//        case .post:
//            if hasJoinedClub() {
//                if let clubData = ClubManager.shared().getClubData(remotely: false, listener: self) {
//                    return clubData.news.count
//                } else {
//                    return 0
//                }
//            } else {
//                return 0
//            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch SectionClub.init(rawValue: indexPath.section)! {
        case .popular, .latest:
            return UiUtility.adaptiveSize(size: 194)
        case .kind:
            if clubSearchResult != nil {
                return UiUtility.adaptiveSize(size: 28)
            }
            return UiUtility.adaptiveSize(size: 78)
        case .club:
            return UiUtility.adaptiveSize(size: 80)
//        case .yours:
//            return UiUtility.adaptiveSize(size: 214)
//        case .post:
//            if hasJoinedClub() {
//                if let clubData = ClubManager.shared().getClubData(remotely: false, listener: self) {
//                    return self.calculateNewsCellHeight(news: clubData.news[indexPath.row])
//                } else {
//                    return 0
//                }
//            } else {
//                return 0
//            }
//            return hasJoinedClub() ? self.calculateNewsCellHeight(news: newsList[indexPath.row]) : 0
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if newsKind == .club {
//
//        } else {
//            if indexPath.section == SectionFeed.feed.rawValue {
//                if newsList.count != 0 {
//                    let lastElement = newsList.count - 1
//                    if indexPath.row == lastElement {
//                        if !NewsManager.shared().isListEndReached() {
//                            // handle your logic here to get more items, add it to dataSource and reload tableview
////                            Logger.d("should try load more")
//                            NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: true, listener: self)
//                        }
//                    }
//                }
//            }
//        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightsDictionaryClub[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let h: CGFloat = cellHeightsDictionaryClub[indexPath] {
            return h
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        Logger.d("tableView cellForRowAt \(indexPath), newsKind: \(newsKind)")
        var cell: UITableViewCell!
        
        switch SectionClub.init(rawValue: indexPath.section)! {
        case .popular:
            cell = tableView.dequeueReusableCell(withIdentifier: "CollectionCell", for: indexPath)
            let ivIcon: UIImageView = cell.viewWithTag(1) as! UIImageView
            let lbTitle: UILabel = cell.viewWithTag(2) as! UILabel
            let collectionView: UICollectionView = cell.viewWithTag(3) as! UICollectionView
            ivIcon.image = UIImage.init(named: "hotGroup")
            lbTitle.textColor = UiUtility.colorDark4A
            lbTitle.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
            lbTitle.text = "popular_club".localized
            
            collectionViewClubPopular = collectionView
            collectionView.backgroundColor = UiUtility.colorBackgroundWhite
            collectionView.delegate = self
            collectionView.dataSource = self
            
            collectionView.reloadData()
        case .latest:
            cell = tableView.dequeueReusableCell(withIdentifier: "CollectionCell", for: indexPath)
            let ivIcon: UIImageView = cell.viewWithTag(1) as! UIImageView
            let lbTitle: UILabel = cell.viewWithTag(2) as! UILabel
            let collectionView: UICollectionView = cell.viewWithTag(3) as! UICollectionView
            ivIcon.image = UIImage.init(named: "latestGroup")
            lbTitle.textColor = UiUtility.colorDark4A
            lbTitle.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
            lbTitle.text = "new_club".localized
            
            collectionViewClubLatest = collectionView
            collectionView.backgroundColor = UiUtility.colorBackgroundWhite
            collectionView.delegate = self
            collectionView.dataSource = self
            
            collectionView.reloadData()
//        case .yours:
//            cell = tableView.dequeueReusableCell(withIdentifier: "CollectionYoursCell", for: indexPath)
//            let ivIcon: UIImageView = cell.viewWithTag(1) as! UIImageView
//            let lbTitle: UILabel = cell.viewWithTag(2) as! UILabel
//            let collectionView: UICollectionView = cell.viewWithTag(3) as! UICollectionView
//            let btnBrowseAll: UIButton = cell.viewWithTag(4) as! UIButton
//
//            ivIcon.image = UIImage.init(named: "urGroup")
//            lbTitle.textColor = UiUtility.colorDark4A
//            lbTitle.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
//            lbTitle.text = "your_club".localized
//
//            collectionViewClubYours = collectionView
//            collectionView.backgroundColor = UiUtility.colorBackgroundWhite
//            collectionView.delegate = self
//            collectionView.dataSource = self
//
//            collectionView.reloadData()
//
//            btnBrowseAll.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
//            btnBrowseAll.setTitleColor(UiUtility.colorBackgroundOutgoingMessage, for: .normal)
//            btnBrowseAll.setTitle("browse_all_club".localized, for: .normal)
//            btnBrowseAll.addTarget(self, action: #selector(browseAllClubClicked(_:)), for: .touchUpInside)
            
        case .kind:
            if clubSearchResult == nil {
                cell = tableView.dequeueReusableCell(withIdentifier: "KindCell", for: indexPath)
                let lbBrowse: UILabel = cell.viewWithTag(1) as! UILabel
                let btnSort: UIButton = cell.viewWithTag(2) as! UIButton
                let collectionView: UICollectionView = cell.viewWithTag(3) as! UICollectionView
                
                lbBrowse.textColor = UiUtility.colorDark4A
                lbBrowse.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
                lbBrowse.text = "browse_all_club".localized
                
                btnSort.setTitle("sort".localized, for: .normal)
                btnSort.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
                btnSort.setTitleColor(UiUtility.colorBackgroundOutgoingMessage, for: .normal)
                btnSort.addTarget(self, action: #selector(sortClubClicked(_:)), for: .touchUpInside)
                
                collectionViewClubKind = collectionView
                collectionView.backgroundColor = UiUtility.colorBackgroundWhite
                collectionView.delegate = self
                collectionView.dataSource = self
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "SortCell", for: indexPath)
                let lbBrowse: UILabel = cell.viewWithTag(1) as! UILabel
                let btnSort: UIButton = cell.viewWithTag(2) as! UIButton
                
                lbBrowse.textColor = UiUtility.colorDark4A
                lbBrowse.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
                lbBrowse.text = "browse_all_club".localized
                
                btnSort.setTitle("sort".localized, for: .normal)
                btnSort.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
                btnSort.setTitleColor(UiUtility.colorBackgroundOutgoingMessage, for: .normal)
                btnSort.addTarget(self, action: #selector(sortClubClicked(_:)), for: .touchUpInside)
            }
            
            
        case .club:
            cell = tableView.dequeueReusableCell(withIdentifier: "ClubCell", for: indexPath)
            let ivPhoto: UIImageView = cell.viewWithTag(1) as! UIImageView
            let lbTitle: UILabel = cell.viewWithTag(3) as! UILabel
            let lbDate: UILabel = cell.viewWithTag(4) as! UILabel
            let btnJoin: UIButton = cell.viewWithTag(2) as! UIButton
            
            var club: Club!
            if let clubSearchResult = clubSearchResult {
                club = clubSearchResult[indexPath.row]
            } else {
                club = ClubManager.shared().getCachedGuestClubs()![indexPath.row]
            }
            
            ivPhoto.backgroundColor = UiUtility.colorBackgroundIncomingMessage
            if let photoUrl = club.picUrl {
                let url = URL(string: photoUrl)
                ivPhoto.kf.setImage(with: url)
            }
            lbTitle.textColor = UiUtility.colorDark4A
            lbTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
            lbTitle.text = club.name
            lbDate.textColor = UiUtility.colorBrownGrey
            lbDate.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
            lbDate.text = club.getDateText()
            
            btnJoin.isSelected = club.isMember()
            btnJoin.addTarget(self, action: #selector(joinClubClicked(_:)), for: .touchUpInside)
            
//        case .post:
//            cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath)
//
//            let clubItem: ClubItemView = cell.viewWithTag(1) as! ClubItemView
//            for constraint in cell.contentView.constraints {
//                if constraint.identifier == "marginBottom" {
//                    constraint.constant = UiUtility.adaptiveSize(size: 6)
//                }
//            }
//
//            let news: ClubPost = ClubManager.shared().getClubData(remotely: false, listener: self)!.news[indexPath.row]
//            clubItem.setupUi(with: news/*, isList: true, inPerson: false*/)
//            clubItem.buttonUser.addTarget(self, action: #selector(userClicked(_:)), for: .touchUpInside)
//            clubItem.buttonFollow.addTarget(self, action: #selector(followClicked(_:)), for: .touchUpInside)
//            clubItem.buttonMore.addTarget(self, action: #selector(moreClicked(_:)), for: .touchUpInside)
//            clubItem.buttonLike.addTarget(self, action: #selector(likeClicked(_:)), for: .touchUpInside)
//            clubItem.buttonLikeList.addTarget(self, action: #selector(likersClicked(_:)), for: .touchUpInside)
//            clubItem.buttonTrack.addTarget(self, action: #selector(trackClicked(_:)), for: .touchUpInside)
            
        }
        
        cell.selectionStyle = .none
        cell.backgroundColor = UiUtility.colorBackgroundWhite
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var club: Club!
        if let clubSearchResult = clubSearchResult {
            club = clubSearchResult[indexPath.row]
        } else {
            club = ClubManager.shared().getCachedGuestClubs()![indexPath.row]
        }
        self.goClub(club)
    }

    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */


    
    // MARK: private methods
    
    private func goClub(_ club: Club) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ClubViewController") as! ClubViewController
        vc.club = club
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
//    private func calculateNewsCellHeight(news: News) -> CGFloat {
//        let width = UiUtility.adaptiveSize(size: 375 - 20 - 20)
//        let font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))!
//        let maxContentHeight: CGFloat = "line_4_text".localized.height(withConstrainedWidth: width, font: font)
//
//        var h: CGFloat = 0
//        h += UiUtility.adaptiveSize(size: 70) // height before content
//
//        if !Utility.isEmpty(news.content) {
//            h += min(news.content!.height(withConstrainedWidth: width, font: font), maxContentHeight)
//        }
//
//        h += UiUtility.adaptiveSize(size: 36) // date
//        h += UiUtility.adaptiveSize(size: 375) // image
//        h += UiUtility.adaptiveSize(size: 50) // footer
//        h += UiUtility.adaptiveSize(size: 6) // separator
//
//        return h
//    }
    
    private func calculateNewsCellHeight(news: ClubPost) -> CGFloat {
        let width = UiUtility.adaptiveSize(size: 375 - 20 - 20)
        let font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))!
        let maxContentHeight: CGFloat = "line_4_text".localized.height(withConstrainedWidth: width, font: font)

        var h: CGFloat = 0
        h += UiUtility.adaptiveSize(size: 70) // height before content
        
        if !Utility.isEmpty(news.content) {
            h += min(news.content!.height(withConstrainedWidth: width, font: font), maxContentHeight)
        }

        h += UiUtility.adaptiveSize(size: 36) // date
        h += UiUtility.adaptiveSize(size: 375) // image
        h += UiUtility.adaptiveSize(size: 50) // footer
        h += UiUtility.adaptiveSize(size: 6) // separator
        
        return h
    }
    
    
    
//    private func switchMode(kind: NewsKind) {
//        if newsKind != kind {
//            if kind == .club {
//                if ClubManager.shared().getJoinedClubCount(remotely: true, listener: self) == nil {
//                    Logger.d("get no joined club count locally, maybe it's first time")
//                    return
//                }
//            }
//
//            newsKind = kind
//
//            switch newsKind {
//            case .all:
//                setClubView(show: false)
//                resetModels()
//                tableViewFeed.delegate = self
//                tableViewFeed.dataSource = self
//                newsList = NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: false, listener: self)
//                updateFeedUi()
//            case .highlighted:
//                setClubView(show: false)
//                resetModels()
//                tableViewFeed.delegate = self
//                tableViewFeed.dataSource = self
//                newsList = NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: false, listener: self)
//                updateFeedUi()
//            case .club:
//                tableViewFeed.delegate = nil
//                tableViewFeed.dataSource = nil
//
//                setClubView(show: true)
//            }
//        }
//    }
//
//    private func updateFeedUi() {
//        tableViewFeed.reloadData()
//    }
    
//    @objc private func modelClicked(_ sender: UIButton) {
//        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.collectionViewKind)
//        if let indexPath = self.collectionViewKind.indexPathForItem(at: buttonPosition) {
////            Logger.d("click at row \(String(describing: indexPath.row))")
//            let carModel: CarModel2 = carModels[indexPath.row]
//            carModel.selected = !carModel.selected
//            if carModel.selected {
//                let tag: String = "tag_\(carModel.model.name!.lowercased())"
////                Logger.d("tag: \(tag)")
//                Analytics.logEvent("newsfeed_tag", parameters: ["tag_name": tag as NSObject])
//            }
//            collectionViewKind.reloadData()
//            NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: false, listener: self)
//        }
//    }
    
    
    private func getModelArrayText() -> String? {
        var text: String? = nil

        for carModel in clubCarModels {
            if carModel.selected {
                if let t = text {
                    text = "\(t),\(carModel.model.id!)"
                } else {
                    text = "\(carModel.model.id!)"
                }
            }
        }

//        Logger.d("text: \(text)")
        return text
    }
    
//    @objc private func userClicked(_ sender: UIButton) {
//        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewFeed)
//        if let indexPath = self.tableViewFeed.indexPathForRow(at: buttonPosition) {
//            let news: News = newsList[indexPath.row]
//            if let _ = news.accountId {
//                UiUtility.goBrowse(profile: Profile.init(with: news), controller: self)
//            }
//        }
//    }
    
//    @objc private func followClicked(_ sender: UIButton) {
//        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewFeed)
//        if let indexPath = self.tableViewFeed.indexPathForRow(at: buttonPosition) {
////            Logger.d("click follow at row \(String(describing: indexPath.row))")
//            let news: News = newsList[indexPath.row]
//            if let accountId = news.accountId {
//                UserManager.shared().follow(accountId: accountId, set: true, listener: self)
//            }
//        }
//    }
//
//    @objc private func moreClicked(_ sender: UIButton) {
//        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewFeed)
//        if let indexPath = self.tableViewFeed.indexPathForRow(at: buttonPosition) {
//            //            Logger.d("click like at row \(String(describing: indexPath.row))")
//            let news: News = newsList[indexPath.row]
//            if news.offical != nil && news.offical! == Role.official.rawValue {
//                // official
//                UiUtility.moreMenu(from: self, type: 0, news: news, contentDelegate: self)
//            } else if let newsAccoundId = news.accountId {
//                if newsAccoundId == UserManager.shared().currentUser.accountId! {
//                    // mine
//                    UiUtility.moreMenu(from: self, type: 1, news: news, contentDelegate: self)
//                } else {
//                    // others
//                    UiUtility.moreMenu(from: self, type: 2, news: news, contentDelegate: self)
//                }
//            }
//        }
//    }
    
//    @objc private func likeClicked(_ sender: UIButton) {
//        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewFeed)
//        if let indexPath = self.tableViewFeed.indexPathForRow(at: buttonPosition) {
////            Logger.d("click like at row \(String(describing: indexPath.row))")
//            let news: News = newsList[indexPath.row]
//            NewsManager.shared().like(news: news, set: (news.likeStatus == 0), listener: self)
//        }
//    }
//
//    @objc private func trackClicked(_ sender: UIButton) {
//        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewFeed)
//        if let indexPath = self.tableViewFeed.indexPathForRow(at: buttonPosition) {
////            Logger.d("click track at row \(String(describing: indexPath.row))")
//            let news: News = newsList[indexPath.row]
//            NewsManager.shared().track(news: news, set: (news.bookmarkStatus == 0), listener: self)
//        }
//    }
//
//    @objc private func likersClicked(_ sender: UIButton) {
//        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewFeed)
//        if let indexPath = self.tableViewFeed.indexPathForRow(at: buttonPosition) {
//            //            Logger.d("click track at row \(String(describing: indexPath.row))")
//            let news: News = newsList[indexPath.row]
//            self.tabControllerDelegate?.goBrowseLikers(news: news)
//        }
//    }
//
//    @objc private func refresh(_ sender: Any) {
//        NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: false, listener: self)
//    }
    
    
    
    // MARK: club
    
//    private func hasJoinedClub() -> Bool {
//        if let count = ClubManager.shared().getJoinedClubCount(remotely: false, listener: nil) {
//            return count != 0
//        }
//        return false
//    }
    
    private func setClubView() {
        tableViewClub.delegate = self
        tableViewClub.dataSource = self
        
        tableViewClub.reloadData()
        
        viewClubContainer.isHidden = false
    }
    
    @objc private func joinClubClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewClub)
        if let indexPath = self.tableViewClub.indexPathForRow(at: buttonPosition) {
            var club: Club!
            if let clubSearchResult = clubSearchResult {
                club = clubSearchResult[indexPath.row]
            } else {
                club = ClubManager.shared().getCachedGuestClubs()![indexPath.row]
            }
            
            Logger.d("click joinClubClicked at row \(String(describing: indexPath.row))")
            Logger.d("club: \(String(describing: club))")
            
            if !club.isMember() {
                ClubManager.shared().memberAdd(clubId: club.id, listener: self)
            }
        }
    }
    
    @objc private func sortClubClicked(_ sender: UIButton) {
        UiUtility.sortClub(controller: self, hasClub: false)
    }
    
//    @objc private func sortJoinedClubClicked(_ sender: UIButton) {
//        UiUtility.sortClub(controller: self, hasClub: true)
//    }
    
//    @IBAction private func createClubClicked(_ sender: Any) {
//        if clubCarModels.count == 0 {
//            Logger.d("model count is zero")
//        }
//        self.tabControllerDelegate?.goCreateClub(clubCarModels: clubCarModels)
//    }
//
//    @IBAction private func exploreClubClicked(_ sender: Any) {
//
//    }
    
//    @objc private func browseAllClubClicked(_ sender: UIButton) {
//        self.tabControllerDelegate?.goYourClub()
//    }

}
