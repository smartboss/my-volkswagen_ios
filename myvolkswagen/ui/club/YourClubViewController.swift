//
//  YourClubViewController.swift
//  myvolkswagen
//
//  Created by CHUNG CHING JIANG on 2019/10/1.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class YourClubViewController: NaviViewController {
    
    @IBOutlet weak var label0: UILabel!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UiUtility.colorBackgroundWhite
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        naviBar.setTitle("your_club".localized)

        label0.textColor = .black
        label0.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
        label0.text = "club_i_joined".localized
        
        label1.textColor = UiUtility.colorDark4A
        label1.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
        label1.text = "browse_club_i_joined".localized
        
        label2.textColor = .black
        label2.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
        label2.text = "club_i_managed".localized
        
        label3.textColor = UiUtility.colorDark4A
        label3.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
        label3.text = "browse_club_i_managed".localized
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        self.back()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    // MARK: private methods
    
    @IBAction private func item0Clicked(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ClubListViewController") as! ClubListViewController
        vc.mode = .joined
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction private func item1Clicked(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ClubListViewController") as! ClubListViewController
        vc.mode = .managed
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
