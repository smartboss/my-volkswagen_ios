//
//  CreateClubViewController.swift
//  myvolkswagen
//
//  Created by CHUNG CHING JIANG on 2019/9/29.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

protocol MemberDelegate: class {
    func didFinishSelectRelation(invitedUser: Set<ClubRelation>)
}



class CreateClubViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, MemberDelegate {
    
    enum Mode: Int {
        case create = 0, edit
    }
    
    enum Row: Int {
        case photo = 0, spacing, name, intro, category, member
        static var allCases: [Row] {
            var values: [Row] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    
    
    var mode: Mode = .create
    private var profile: Profile?
    var club: Club?
    var clubUpdateDelegate: ClubUpdateDelegate?
    @IBOutlet weak var tableView: UITableView!
    
    private var photo: UIImage?
    private var textFieldName: UITextField!
    private var textFieldIntro: UITextField!
    private var modelSelectedIndex: Int = 0
//    private var model: Model?
    private var invitedUser: Set = Set<ClubRelation>()
    private var isPhotoSelected: Bool = false
    
    @IBOutlet weak var viewPickerContainer: UIView!
    @IBOutlet weak var buttonDone: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    var clubCarModels: [CarModel2]!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UiUtility.colorBackgroundWhite
        if mode == .create {
            naviBar.setTitle("create_club".localized)
            naviBar.setCreateVisibility(true)
        } else {
            naviBar.setTitle("edit_club".localized)
            naviBar.setDoneVisibility(true)
        }
        naviBar.setLeftCancelVisibility(true)
        
        
        tableView.backgroundColor = UiUtility.colorBackgroundWhite
        
        if let myId = UserManager.shared().currentUser.accountId {
            profile = UserManager.shared().getProfile(withId: myId, listener: self)
        }
        
        viewPickerContainer.backgroundColor = UIColor(rgb: 0x000000, a: 0.6)
        pickerView.backgroundColor = UiUtility.colorBackgroundWhite
        buttonDone.setTitle("done".localized, for: .normal)
        buttonDone.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//
//        if clubCarModels == nil || clubCarModels.count == 0 {
//            NewsManager.shared().getCarModel(remotely: true, listener: self)
//        }
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
    }
    
    override func leftCancelButtonClicked(_ button: UIButton) {
        self.view.endEditing(true)
        
        let alert = UIAlertController(title: nil, message: mode == .create ? "warning_cancal_creating_club".localized : "warning_cancal_editing_club".localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "cancel".localized, style: .destructive, handler: { action in
            self.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: mode == .create ? "continue_creating".localized : "continue_editing".localized, style: .default, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func getMemberArrayText() -> [String] {
        var members: [String] = []
        members.append(profile!.accountId!)
        
        for relation in invitedUser {
            members.append(relation.accountId!)
        }
        
        return members
        
//        var text: String? = profile?.accountId
//
//        for relation in invitedUser {
//            if let t = text {
//                text = "\(t),\(relation.accountId!)"
//            } else {
//                text = "\(relation.accountId!)"
//            }
//        }
//
////        Logger.d("text: \(text)")
//        return text
    }
    
    override func createButtonClicked(_ button: UIButton) {
        self.view.endEditing(true)
        
        if let name = textFieldName.text, let intro = textFieldIntro.text, let photo = photo {
            if name.count == 0 || intro.count == 0 || !isPhotoSelected {
                let alert = UIAlertController(title: nil, message: "warning_all_field_neeeded".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "got_it".localized, style: .default, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            if invitedUser.count == 0 {
                let alert = UIAlertController(title: nil, message: "warning_at_least_1_memner".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "got_it".localized, style: .default, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            ClubManager.shared().appendClub(name: name, intro: intro, photo: photo, models: "\(clubCarModels[modelSelectedIndex].model.id!)", members: getMemberArrayText(), listener: self)
        } else {
            let alert = UIAlertController(title: nil, message: "warning_all_field_neeeded".localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "got_it".localized, style: .default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    override func doneButtonClicked(_ button: UIButton) {
        self.view.endEditing(true)
        
        if let name = textFieldName.text, let intro = textFieldIntro.text, let photo = photo {
            if name.count == 0 || intro.count == 0 {
                let alert = UIAlertController(title: nil, message: "warning_all_field_neeeded".localized, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "got_it".localized, style: .default, handler: { action in
                    
                }))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
//            if invitedUser.count == 0 {
//                let alert = UIAlertController(title: nil, message: "warning_at_least_1_memner".localized, preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "got_it".localized, style: .default, handler: { action in
//
//                }))
//                self.present(alert, animated: true, completion: nil)
//                return
//            }
            
            ClubManager.shared().updateClub(clubId: club!.id, name: name, intro: intro, photo: photo, models: "\(clubCarModels[modelSelectedIndex].model.id!)", listener: self)
        } else {
            let alert = UIAlertController(title: nil, message: "warning_all_field_neeeded".localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "got_it".localized, style: .default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    // MARK: news protocol
        
    override func didStartGetNewsCarModel() {
        showHud()
    }
    
    override func didFinishGetNewsCarModel(success: Bool, carModels: [Model]?) {
        dismissHud()
        
        if success {
//            Logger.d("get carModels: \(String(describing: carModels))")
            
            if let carModels = carModels {
                self.clubCarModels = []
                for model in carModels {
                    let clubCarModel = CarModel2.init(model: model)
                    self.clubCarModels.append(clubCarModel)
                }
                self.tableView.reloadData()
                self.pickerView.reloadComponent(0)
            }
        } else {
            Logger.d("failed to get models, back")
            dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    // MARK: member delegate
    
    func didFinishSelectRelation(invitedUser: Set<ClubRelation>) {
        self.invitedUser = invitedUser
        Logger.d("selected \(invitedUser.count) members")
        tableView.reloadData()
    }
    
    
    
    // MARK: profile protocol
        
    override func didStartGetProfile() {
        showHud()
    }
    
    override func didFinishGetProfile(success: Bool, profile: Profile?) {
        dismissHud()
        
        if success {
            if let p = profile {
                self.profile = p
//                Logger.d("profile: \(p)")

                if clubCarModels == nil || clubCarModels.count == 0 {
                    NewsManager.shared().getCarModel(remotely: true, listener: self)
                } else {
                    self.tableView.reloadData()
                }
            }
        } else {
            Logger.d("failed to get profile, back")
            dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    // MARK: club protocol
    
    override func didStartAppendClub() {
        super.didStartAppendClub()
    }
    
    override func didFinishAppendClub(success: Bool) {
        super.didFinishAppendClub(success: success)
        
        if success {
            ClubManager.shared().cleanCache()
            
            clubUpdateDelegate?.didFinishUpdateClub()
            dismiss(animated: true, completion: nil)
        }
    }
    
    override func didStartUpdateClub() {
        super.didStartUpdateProfile()
    }
    
    override func didFinishUpdateClub(success: Bool) {
        super.didFinishUpdateClub(success: success)
        
        if success {
            ClubManager.shared().cleanCache()

            clubUpdateDelegate?.didFinishUpdateClub()
            dismiss(animated: true, completion: nil)
        }
    }
    
    
    
    // MARK: picker view delegate
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if clubCarModels == nil {
            return 0
        }
        return clubCarModels.count
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return clubCarModels[row].model.name
    }
    
    
    
    // MARK: text field delegate
        
    //    func textFieldDidEndEditing(_ textField: UITextField) {
    //        editingContent = textField.text
    //    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if textField == textFieldName {
            textFieldIntro.becomeFirstResponder()
        }
        
        return true
    }
    
    
    
    // MARK: image picker controller delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.editedImage] as? UIImage else {
            Logger.d("No image found")
            return
        }
        
        // print out the image size as a test
//        Logger.d("image size: \(image.size)")
        
        photo = image
        tableView.reloadData()
        isPhotoSelected = true;
    }
    
    
    
    // MARK: table view delegate

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if profile == nil {
            return 0
        } else {
            if mode == .create {
                return Row.allCases.count
            } else {
                return Row.allCases.count - 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Row.init(rawValue: indexPath.row)! {
        case .photo:
            return UiUtility.adaptiveSize(size: 170)
        case .spacing:
            return UiUtility.adaptiveSize(size: 11)
        case .name:
            return UiUtility.adaptiveSize(size: 54)
        case .intro:
            return UiUtility.adaptiveSize(size: 54)
        case .category:
            return UiUtility.adaptiveSize(size: 54)
        case .member:
            let heightUpper = UiUtility.adaptiveSize(size: 14 + 26 + 10)
            
            let avatarWidth = UiUtility.adaptiveSize(size: 40)
            let spacing = UiUtility.adaptiveSize(size: 10)
            
            let containerWidth = UiUtility.getScreenWidth() - UiUtility.adaptiveSize(size: 40)
            var offsetX: CGFloat = 0
            var offsetY: CGFloat = 0
            
            if invitedUser.count == 0 {
                offsetY = offsetY + avatarWidth + spacing
            } else {
                for _ in invitedUser {
                    offsetX = offsetX + avatarWidth + spacing
                    if (offsetX + avatarWidth > containerWidth) {
                        offsetX = 0
                        offsetY = offsetY + avatarWidth + spacing
                    }
                }
            }
            
            if offsetX == 0 {
                return heightUpper + offsetY
            } else {
                return heightUpper + offsetY + avatarWidth + spacing
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        Logger.d("tableView cellForRowAt \(indexPath), newsKind: \(newsKind)")
        var cell: UITableViewCell!
        
        switch Row.init(rawValue: indexPath.row)! {
        case .photo:
            cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCell", for: indexPath)
            let ivPhoto: UIImageView = cell.viewWithTag(1) as! UIImageView
            let btnEdit: UIButton = cell.viewWithTag(2) as! UIButton
            
            ivPhoto.backgroundColor = UiUtility.colorBackgroundWhite
            if let photo = photo {
                ivPhoto.image = photo
            } else {
                if mode == .create {
                    photo = ivPhoto.image
                } else {
                    if let stickerURL = club?.picUrl {
                        let url = URL(string: stickerURL)
                        ivPhoto.kf.setImage(with: url)
                        photo = ivPhoto.image
                    }
                }
            }
        
            btnEdit.addTarget(self, action: #selector(editPhotoClicked(_:)), for: .touchUpInside)
            
        case .spacing:
            cell = tableView.dequeueReusableCell(withIdentifier: "DummyCell", for: indexPath)
            
        case .name:
            cell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath)

            let lbTitle: UILabel = cell.viewWithTag(1) as! UILabel
            let textField: UITextField = cell.viewWithTag(2) as! UITextField

            lbTitle.textColor = .black
            lbTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            lbTitle.text = "club_name".localized

            textField.font = UIFont(name: "PingFangTC-Light", size: UiUtility.adaptiveSize(size: 16))
            textField.placeholder = "club_name_placeholder".localized
            textField.delegate = self
            textField.returnKeyType = .continue
            if mode == .edit && (textField.text == nil || textField.text!.isEmpty) {
                textField.text = club?.name
            }
            textFieldName = textField
        
        case .intro:
            cell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath)
            
            let lbTitle: UILabel = cell.viewWithTag(1) as! UILabel
            let textField: UITextField = cell.viewWithTag(2) as! UITextField

            lbTitle.textColor = .black
            lbTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            lbTitle.text = "club_intro".localized
            
            textField.font = UIFont(name: "PingFangTC-Light", size: UiUtility.adaptiveSize(size: 16))
            textField.placeholder = "club_intro_placeholder".localized
            textField.delegate = self
            textField.returnKeyType = .done
            if mode == .edit && (textField.text == nil || textField.text!.isEmpty) {
                textField.text = club?.intro
            }
            textFieldIntro = textField
            
        case .category:
            cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath)
            
            let lbTitle: UILabel = cell.viewWithTag(1) as! UILabel
            let textField: UITextField = cell.viewWithTag(2) as! UITextField
            let button: UIButton = cell.viewWithTag(3) as! UIButton

            lbTitle.textColor = .black
            lbTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            lbTitle.text = "select_club_category".localized
            
            textField.font = UIFont(name: "PingFangTC-Light", size: UiUtility.adaptiveSize(size: 16))
            textField.textColor = UiUtility.colorDark4A
            if clubCarModels != nil {
                if mode == .edit && (textField.text == nil || textField.text!.isEmpty) && modelSelectedIndex == 0 {
                    if club!.modelList.count != 0 {
                        let model = club!.modelList[0]
                        for carModel in clubCarModels {
                            if carModel.model.id! == model.id! {
                                break
                            }
                            modelSelectedIndex += 1
                        }
                    }
                }
                textField.text = clubCarModels[modelSelectedIndex].model.name
            }
            
            button.addTarget(self, action: #selector(selectCategoryClicked(_:)), for: .touchUpInside)
            
        case .member:
            cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell", for: indexPath)
            
            let lbTitle: UILabel = cell.viewWithTag(1) as! UILabel
            let vMemberContainer: UIView = cell.viewWithTag(2)!
            
            for constraint in cell.contentView.constraints {
                if constraint.identifier == "member_container_top" {
                    constraint.constant = UiUtility.adaptiveSize(size: 10)
                } else if constraint.identifier == "title_top" {
                    constraint.constant = UiUtility.adaptiveSize(size: 14)
                }
            }
            
            lbTitle.textColor = .black
            lbTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            lbTitle.text = "member".localized
            
            for constraint in vMemberContainer.constraints {
                if constraint.identifier == "member_container_height" {
                    constraint.constant = UiUtility.adaptiveSize(size: 128)
                }
            }
            vMemberContainer.backgroundColor = UiUtility.colorBackgroundWhite
            renderMembers(vMemberContainer)
        }
    
        cell.selectionStyle = .none
        cell.backgroundColor = UiUtility.colorBackgroundWhite
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
    // MARK: private methods
    
    @objc private func editPhotoClicked(_ sender: Any) {
        UiUtility.pickImage(from: self, delegate: self, allowsEditing: true)
    }
    
    @objc private func selectCategoryClicked(_ sender: Any) {
        self.view.endEditing(true)
        setPickerVisibility(show: true)
    }
    
    @objc private func addMemberClicked(_ sender: Any) {
        goInviteFriend(of: self.profile!, isFollower: false, controller: self)
    }
    
    private func renderMembers(_ container: UIView) {
        container.subviews.forEach({ $0.removeFromSuperview() })
        
        let avatarWidth = UiUtility.adaptiveSize(size: 40)
        let avatarCornerRadius = avatarWidth / 2
        let checkWidth = UiUtility.adaptiveSize(size: 12)

        let offsetCheck = avatarWidth - checkWidth
        
        let spacing = UiUtility.adaptiveSize(size: 10)
        
        let containerWidth = UiUtility.getScreenWidth() - UiUtility.adaptiveSize(size: 40)
        var offsetX: CGFloat = 0
        var offsetY: CGFloat = 0
        
        for relation in invitedUser {
            let ivPhoto = UIImageView.init(frame: CGRect.init(x: offsetX, y: offsetY, width: avatarWidth, height: avatarWidth))
            ivPhoto.backgroundColor = UiUtility.colorBackgroundWhite
            ivPhoto.layer.masksToBounds = true
            ivPhoto.layer.cornerRadius = avatarCornerRadius
            if let stickerURL = relation.stickerURL {
                let url = URL(string: stickerURL)
                ivPhoto.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_photo"))
            }
            container.addSubview(ivPhoto)
            
            let ivCheck = UIImageView.init(frame: CGRect.init(x: offsetX + offsetCheck, y: offsetY + offsetCheck, width: checkWidth, height: checkWidth))
            ivCheck.isHidden = true
//            if let level = relation.level {
//                if level > 2 {
//                    ivCheck.isHidden = false
//                    ivCheck.image = UIImage.init(named: "check_golden_large")
//                    container.addSubview(ivCheck)
//                } else if level > 1 {
//                    ivCheck.isHidden = false
//                    ivCheck.image = UIImage.init(named: "check_silver_large")
//                    container.addSubview(ivCheck)
//                } else if level > 0 {
//                    ivCheck.isHidden = false
//                    ivCheck.image = UIImage.init(named: "check_large")
//                    container.addSubview(ivCheck)
//                }
//            }
            
            offsetX = offsetX + avatarWidth + spacing
            if (offsetX + avatarWidth > containerWidth) {
                offsetX = 0
                offsetY = offsetY + avatarWidth + spacing
            }
        }
        
        
        let btnAdd = UIButton.init(frame: CGRect.init(x: offsetX, y: offsetY, width: avatarWidth, height: avatarWidth))
        btnAdd.setImage(UIImage.init(named: "add"), for: .normal)
        btnAdd.addTarget(self, action: #selector(addMemberClicked(_:)), for: .touchUpInside)
        container.addSubview(btnAdd)
    }
    
    private func goInviteFriend(of profile: Profile, isFollower: Bool, controller: UIViewController) {
        self.view.endEditing(true)
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InviteFriendViewController") as! InviteFriendViewController
        vc.isFollower = isFollower
        vc.of = profile
        var invitedUsers = Set<String>()
        for relation in invitedUser {
            invitedUsers.insert(relation.accountId!)
        }
        vc.invitedUser = invitedUsers
        vc.memberDelegate = self
        controller.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction private func doneClicked(_ sender: Any) {
        setPickerVisibility(show: false)
        modelSelectedIndex = pickerView.selectedRow(inComponent: 0)
        tableView.reloadRows(at: [IndexPath.init(row: Row.category.rawValue, section: 0)], with: .automatic)
    }
    
    private func setPickerVisibility(show: Bool) {
        viewPickerContainer.isHidden = !show
        if show {
            pickerView.selectRow(modelSelectedIndex, inComponent: 0, animated: true)
        }
    }
    
}
