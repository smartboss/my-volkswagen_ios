//
//  InviteFriendViewController.swift
//  myvolkswagen
//
//  Created by CHUNG CHING JIANG on 2019/9/29.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class InviteFriendViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource {
    
    enum Mode: Int {
        case create = 0, invite
    }
    
    enum Section: Int {
        case paddingTab = 0, selectAll, content, paddingFooter, empty
        static var allCases: [Section] {
            var values: [Section] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    var of: Profile!
    var mode: Mode = .create
    
    // mode follow
    var isFollower: Bool = false
    @IBOutlet weak var containerTab: UIView!
    @IBOutlet weak var imageViewTab: UIImageView!
    var followers: [ClubRelation] = []
    var followings: [ClubRelation] = []
    
    var afterFirstQueryFollowing = false
    var afterFirstQueryFollower = false
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var footerText0: UILabel!
    @IBOutlet weak var footerText1: UILabel!
    @IBOutlet weak var footerText2: UILabel!
    @IBOutlet weak var constraintFooter0: NSLayoutConstraint!
    @IBOutlet weak var constraintFooter1: NSLayoutConstraint!
    @IBOutlet weak var buttonFooterDone: UIButton!
    
//    var invitedUser: Set<ClubRelation>!// = Set<ClubRelation>()
    var invitedUser: Set = Set<String>()
    var memberDelegate: MemberDelegate?
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UiUtility.colorBackgroundWhite
        containerTab.backgroundColor = UiUtility.colorBackgroundWhite
        tableView.backgroundColor = UiUtility.colorBackgroundWhite
        
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        naviBar.setTitle("invite_friend".localized)
        
//        if isFollower {
//            followers = UserManager.shared().getRelation(of: of, isFollower: self.isFollower, fromRemote: true, listener: self)
//        } else {
//            followings = UserManager.shared().getRelation(of: of, isFollower: self.isFollower, fromRemote: true, listener: self)
//        }
        ClubManager.shared().getRelation(kind: .follow, clubId: nil, listener: self)
        
        
        viewFooter.isHidden = (mode == .invite)
        if mode == .create {
            viewFooter.backgroundColor = UiUtility.colorBackgroundWhite
            
            footerText0.textColor = UiUtility.colorDarkGrey
            footerText0.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
            footerText0.text = "total_selected_friend_0".localized
            
            footerText1.textColor = .black
            footerText1.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            
            footerText2.textColor = UiUtility.colorDarkGrey
            footerText2.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
            footerText2.text = "total_selected_friend_1".localized
            
            constraintFooter0.constant = UiUtility.adaptiveSize(size: 19.5)
            constraintFooter1.constant = UiUtility.adaptiveSize(size: 19.5)
            
            buttonFooterDone.setTitle("done".localized, for: .normal)
            buttonFooterDone.setTitleColor(UiUtility.colorBackgroundOutgoingMessage, for: .normal)
            buttonFooterDone.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 17))
            
            updateFooterTotalText()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateUi()
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        self.back()
    }
    
    
    
    // MARK: user protocol
    
//    override func didStartFollow() {
//        showHud()
//    }
//
//    override func didFinishFollow(success: Bool, accountId: String, set: Bool) {
//        dismissHud()
//
//        if success {
//            // TODO: club - users relation
////            if mode == .follow {
////                if isFollower {
////                    self.followers = UserManager.shared().getRelation(of: of, isFollower: true, fromRemote: false, listener: self)
////                } else {
////                    self.followings = UserManager.shared().getRelation(of: of, isFollower: false, fromRemote: false, listener: self)
////                }
////            }
//
//            updateUi()
//        }
//    }
//
//    override func didStartGetRelation() {
//        showHud()
//    }
//
//    override func didFinishGetRelation(success: Bool, isFollower: Bool, relations: [Relation]?) {
//        dismissHud()
//
//        if success {
//            if let relations = relations {
//                if isFollower {
//                    afterFirstQueryFollower = true
//                    self.followers = relations
//                } else {
//                    afterFirstQueryFollowing = true
//                    self.followings = relations
//                }
//
//                updateUi()
//            }
//        }
//    }
    
    
    
    // MARK: club protocol
    
    override func didStartGetClubRelation() {
        super.didStartGetRelation()
    }
    
    override func didFinishGetClubRelation(success: Bool, kind: ClubRelationKind, relations: [ClubRelation]) {
        super.didFinishGetClubRelation(success: success, kind: kind, relations: relations)
        
        if success {
            if kind == .followers {
                afterFirstQueryFollower = true
                self.followers = relations
            } else {
                afterFirstQueryFollowing = true
                self.followings = relations
                
                ClubManager.shared().getRelation(kind: .followers, clubId: nil, listener: self)
            }
            
            updateUi()
        }
    }
    
    
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Section.init(rawValue: section)! {
        case .paddingTab:
            return 1
        case .selectAll:
            if mode == .create {
                return (getOperatingData().count == 0 ? 0 : 1)
            } else {
                return 0
            }
        case .content:
            if isFollower {
                return followers.count
            } else {
                return followings.count
            }
        case .paddingFooter:
            if mode == .create {
                if getOperatingData().count == 0 {
                    return 0
                } else {
                    return 1
                }
            } else {
                return 0
            }
        case .empty:
            if getOperatingData().count == 0 {
                return 1
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Section.init(rawValue: indexPath.section)! {
        case .paddingTab:
            return UiUtility.adaptiveSize(size: 40)
        case .selectAll:
            return UiUtility.adaptiveSize(size: 56)
        case .content:
            return UiUtility.adaptiveSize(size: 80)
        case .paddingFooter:
            return UiUtility.adaptiveSize(size: 44)
        case .empty:
            return UiUtility.adaptiveSize(size: 152)
        }
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        switch Section.init(rawValue: indexPath.section)! {
        case .paddingTab, .paddingFooter:
            cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath)
            cell.backgroundColor = UiUtility.colorBackgroundWhite
            cell.selectionStyle = .none
        case .selectAll:
            cell = tableView.dequeueReusableCell(withIdentifier: "SelectAllCell", for: indexPath)
            cell.selectionStyle = .none
            
            let name: UILabel = cell.viewWithTag(1) as! UILabel
            let button: UIButton = cell.viewWithTag(2) as! UIButton
            
            name.textColor = UIColor.black
            name.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            name.text = "select_all".localized
            
            button.isSelected = isSelectedAll()
            button.addTarget(self, action: #selector(buttonSelectAllClicked(_:)), for: .touchUpInside)
        case .content:
            if mode == .create {
                cell = tableView.dequeueReusableCell(withIdentifier: "LikerCreateCell", for: indexPath)
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "LikerCell", for: indexPath)
            }
            
            let photo: UIImageView = cell.viewWithTag(1) as! UIImageView
            let name: UILabel = cell.viewWithTag(2) as! UILabel
            let button: UIButton = cell.viewWithTag(3) as! UIButton
            let imageCheck: UIImageView = cell.viewWithTag(4) as! UIImageView
            
            let user = getItem(at: indexPath)
            
            photo.backgroundColor = UiUtility.colorBackgroundWhite
            photo.layer.masksToBounds = true
            photo.layer.cornerRadius = UiUtility.adaptiveSize(size: 30)
            if let stickerURL = user.stickerURL {
                let url = URL(string: stickerURL)
                photo.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_photo"))
            }
            
            // image check
            imageCheck.isHidden = true
//            if let level = user.level {
//                if level > 2 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_golden_large")
//                } else if level > 1 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_silver_large")
//                } else if level > 0 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_large")
//                }
//            }
            
            name.textColor = UIColor.black
            name.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            name.text = user.nickname
            
//            button.isHidden = user.isMe() || user.isOfficial()
//            button.setImage(user.isFollowedByMe() ? UIImage.init(named: "following_small") : UIImage.init(named: "follow_small"), for: .normal)
            if mode == .create {
                button.isSelected = invitedUser.contains(user.accountId!)
            } else {
                
            }
            button.addTarget(self, action: #selector(buttonSelectClicked(_:)), for: .touchUpInside)
        case .empty:
            cell = tableView.dequeueReusableCell(withIdentifier: "EmptyMsgCell", for: indexPath)
            cell.selectionStyle = .none
            cell.backgroundColor = UiUtility.colorBackgroundWhite
            
            let msg: UILabel = cell.viewWithTag(1) as! UILabel
            msg.textColor = UIColor(rgb: 0x4A4A4A)
            msg.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
            msg.isHidden = true
            if isFollower {
                if afterFirstQueryFollower {
                    msg.isHidden = false
                    msg.text = "no_follower_yet".localized
                }
            } else {
                if afterFirstQueryFollowing {
                    msg.isHidden = false
                    msg.text = "no_follower_yet".localized
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
//        if indexPath.section == Section.content.rawValue {
//            let relation = getItem(at: indexPath)
//
//            UiUtility.goBrowse(profile: Profile.init(with: relation), controller: self)
//        }
        
        if indexPath.section == Section.selectAll.rawValue {
            for relation in getOperatingData() {
                invitedUser.insert(relation.accountId!)
            }
    //        updateUi()
    //        tableView.reloadSections([Section.selectAll.rawValue, Section.content.rawValue], with: .none)
            tableView.reloadData()
            updateFooterTotalText()
        } else if indexPath.section == Section.content.rawValue {
            let user: ClubRelation = getItem(at: indexPath)
            if invitedUser.contains(user.accountId!) {
                invitedUser.remove(user.accountId!)
            } else {
                invitedUser.insert(user.accountId!)
            }
            
    //            tableView.reloadSections([Section.selectAll.rawValue, Section.content.rawValue], with: .none)
            tableView.reloadData()
            updateFooterTotalText()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func getItem(at position: IndexPath) -> ClubRelation {
        // no boundry check
        return getOperatingData()[position.row]
    }
    
    private func getOperatingData() -> [ClubRelation] {
        if isFollower {
            return followers
        } else {
            return followings
        }
    }
    
    private func updateUi() {
        // navi
        naviBar.setupSearchArea(orig: true)
        
        // tab
        containerTab.isHidden = false
        imageViewTab.isHighlighted = isFollower
        
        // table view
        tableView.reloadData()
    }

    @IBAction private func tab0Clicked(_ sender: UIButton) {
        if isFollower {
            isFollower = false
//            followings = UserManager.shared().getRelation(of: of, isFollower: self.isFollower, fromRemote: false, listener: self)
            updateUi()
        }
    }
    
    @IBAction private func tab1Clicked(_ sender: UIButton) {
        if !isFollower {
            isFollower = true
//            followers = UserManager.shared().getRelation(of: of, isFollower: self.isFollower, fromRemote: false, listener: self)
            updateUi()
        }
    }
    
    @objc private func buttonSelectClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            let user: ClubRelation = getItem(at: indexPath)
            if invitedUser.contains(user.accountId!) {
                invitedUser.remove(user.accountId!)
            } else {
                invitedUser.insert(user.accountId!)
            }
            
//            tableView.reloadSections([Section.selectAll.rawValue, Section.content.rawValue], with: .none)
            tableView.reloadData()
            updateFooterTotalText()
        }
    }
    
    @objc private func buttonSelectAllClicked(_ sender: UIButton) {
        for relation in getOperatingData() {
            invitedUser.insert(relation.accountId!)
        }
//        updateUi()
//        tableView.reloadSections([Section.selectAll.rawValue, Section.content.rawValue], with: .none)
        tableView.reloadData()
        updateFooterTotalText()
    }
    
    @IBAction private func footerDoneClicked(_ sender: UIButton) {
        var invitedUsers = Set<ClubRelation>()
        for relation in invitedUser {
            var added: Bool = false
            
            for f in followers {
                if relation == f.accountId! {
                    added = true
                    invitedUsers.insert(f)
                    break
                }
            }
            
            if !added {
                for f in followings {
                    if relation == f.accountId! {
                        invitedUsers.insert(f)
                        break
                    }
                }
            }
        }
        memberDelegate?.didFinishSelectRelation(invitedUser: invitedUsers)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    private func updateFooterTotalText() {
        footerText1.text = "\(invitedUser.count)"
    }
    
    private func isSelectedAll() -> Bool {
        for relation in getOperatingData() {
            if !invitedUser.contains(relation.accountId!) {
                return false
            }
        }
        return true
    }
}
