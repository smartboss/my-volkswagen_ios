//
//  LoginViewController.swift
//  myvolkswagen
//
//  Created by Apple on 1/29/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import WebKit
import Firebase


class LoginViewController: NaviViewController, WKNavigationDelegate {
    
    
    @IBOutlet weak var container: UIView!
    var webView: WKWebView!
    var state: String!
    var vwLoggedIn: Bool = false
    
    weak var loginDataDelegate: LoginDataDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        naviBar.setTitle("".localized)
        naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if !self.vwLoggedIn {
            Analytics.logEvent("login_back", parameters: nil)
        }
    }
    
    override func viewDidLayoutSubviews() {
        let configuration = WKWebViewConfiguration()
        configuration.setURLSchemeHandler(CustomSchemeHandler(), forURLScheme: Config.redirectScheme)
//        let preferences = WKPreferences()
//        preferences.javaScriptEnabled = true
//        configuration.preferences = preferences
        webView = WKWebView(frame: self.container.bounds, configuration: configuration)
        self.container.addSubview(webView)
        
        
        state = createState()
        
        let str = "\(Config.urlVWServer)?client_id=\(Config.clientId)&scope=openid%20profile&response_type=code%20id_token%20token&nonce=\(Config.nonce)&redirect_uri=\(Config.redirectScheme)://&state=\(state!)"
        //        let str = "https://identity-sandbox.vwgroup.io/oidc/v1/authorize?client_id=35f670e0-f120-44db-99fe-f4082f8470cb@apps_vw-dilab_com&scope=openid%20profile&response_type=code%20id_token%20token&nonce=8jYKt0sTiPS7eeTx9KeRg6Wq12eOppa3&redirect_uri=http://localhost:8080/oauth/callback&state=\(state!)"
        
        webView.navigationDelegate = self
        let url = URL(string: str)!
        //        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        
        let dataStore = WKWebsiteDataStore.default()
        dataStore.fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            //            Logger.d("records: \(records)")
            dataStore.removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), for: records.filter { $0.displayName.contains("vwapps") || $0.displayName.contains("vwgroup") }, completionHandler: {
                self.webView.load(URLRequest(url: url))
            })
        }
        
//        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
//        //        print("[WebCacheCleaner] All cookies deleted")
//        
//        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
//            records.forEach { record in
//                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {
//                    self.webView.load(URLRequest(url: url))
//                })
//                print("[WebCacheCleaner] Record \(record) deleted")
//            }
//        }
    }
    
    
    
    // MARK: WKNavigationDelegate
    
    public func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        if let url = webView.url {
            Logger.d("url: \(url.absoluteString)")
//            Logger.d("url scheme: \(String(describing: url.scheme))")
//            Logger.d("url pathComponents: \(url.pathComponents)")
            
            
            
            if let scheme = url.scheme {
                if scheme == Config.redirectScheme {
                    
                    var pState: String? = nil
                    var pCode: String? = nil
                    var pToken: String? = nil
                    
                    let queries = url.absoluteString.components(separatedBy: "#")
                    if queries.count == 2 {
                        let queryItems = queries[1].components(separatedBy: "&")
                        for item in queryItems {
//                            Logger.d("item: \(item)")
                            let nv = item.components(separatedBy: "=")
                            if nv.count == 2 {
                                let name: String = nv[0]
                                let value: String = nv[1]
//                                Logger.d("name: \(name)")
//                                Logger.d("value: \(value)")
                                
                                if name == "state" {
                                    pState = value
                                } else if name == "code" {
                                    pCode = value
                                } else if name == "id_token" {
                                    pToken = value
                                }
                            } else {
                                Logger.e("nv count is not 2")
                            }
                        }
                        
                        if let pState = pState, let pCode = pCode, let pToken = pToken {
//                            Logger.d("pState: \(pState)")
//                            Logger.d("pCode: \(pCode)")
//                            Logger.d("pToken: \(pToken)")
                            
                            if pState == self.state {
                                Logger.d("successfully login")
                                loginDataDelegate?.setLoginDataWith(code: pCode, token: pToken)
                                self.vwLoggedIn = true
                                navigationController?.popViewController(animated: true)
                            } else {
                                Logger.d("different state")
                                loginDataDelegate?.setLoginDataWith(code: pCode, token: pToken)
                                self.vwLoggedIn = true
                            }
                        }
                    } else {
                        Logger.e("queries count is not 2")
                    }
                    
                }
            }
            
            
        }
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }

    func createState() -> String {
        let randomStr = randomString(length: 8)
//        Logger.d("randomStr: \(randomStr)")
        
        let randomStrBase64 = randomStr.toBase64()!
//        Logger.d("randomStrBase64: \(randomStrBase64)")
        
        let data = Data(randomStrBase64.utf8)
        let hexString = data.map{ String(format:"%02x", $0) }.joined()
        Logger.d("hexString: \(hexString)")
        
        return hexString
    }
}
