//
//  PATViewController.swift
//  myvolkswagen
//
//  Created by TWTPEMAC021 on 2021/9/21.
//  Copyright © 2021 volkswagen. All rights reserved.
//

import UIKit
import Firebase
import SnapKit
import Kingfisher

class PATViewController: NaviViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var cars: [Car]!
    var selectedCar: Car!
    var patData: Pat?
    var patDataArray: [PatData]?
    var uniqueId: String!
    
    // step1
    @IBOutlet weak var currentMileageTextfield: UITextField!
    @IBOutlet weak var currentMileageSlider: UISlider!
    @IBOutlet weak var vinCode: BasicLabel!
    @IBOutlet weak var carNumberTextfield: UITextField!
    let UIPicker: UIPickerView = UIPickerView()
    @IBOutlet weak var step1ScrollView: UIScrollView!
    @IBOutlet weak var step1BtnView: UIView!
    
    // step2
    @IBOutlet weak var step2CollectionView: UICollectionView!
    @IBOutlet weak var step2BtnView: UIView!
    @IBOutlet weak var step2DropDown: UIImageView!
    @IBOutlet var previousStepCons: NSLayoutConstraint!
    @IBOutlet var previousStepWidth: NSLayoutConstraint!
    @IBOutlet weak var reserveBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    // step3
    @IBOutlet weak var step3View: UIView!
    @IBOutlet weak var step3Title: BasicLabel!
    @IBOutlet weak var step3Address: BasicLabel!
    @IBOutlet weak var step3PhoneNum: BasicLabel!
    @IBOutlet weak var step3BtnView: UIView!
    
    
    // notice
    @IBOutlet weak var noticeView: UIView!
    @IBOutlet weak var insCloseBtn: UIButton!
    
    
    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()
        
        currentMileageTextfield.delegate = self
        currentMileageTextfield.inputAccessoryView = toolbar
        
        selectedCar = cars[0]
        refreshCarInfo()
        
        creatUniqueID()
        
        naviBar.setTitle("car_pat".localized)
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        naviBar.setRightButton(UIImage(named: "notif_black"), highlighted: UIImage(named: "notif_black_unread"))
        imageViewNotification = naviBar.imageViewRight
        
        setupSlider()
        currentMileageSlider.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
        
        self.noticeView.isHidden = false
        self.noticeView.alpha = 1
        
        setUpPickerView()
        let layout = step2CollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.estimatedItemSize = CGSize(width: 375, height: 200)
        
        carNumberTextfield.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
        step3PhoneNum.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(phoneCall)))
        step3PhoneNum.isUserInteractionEnabled = true
        
        insCloseBtn.setBackgroundImage(UIImage(named: "delete")?.withRenderingMode(.alwaysTemplate), for: .normal)
        insCloseBtn.tintColor = UIColor.white
        
        if cars.count == 1 {
            step2DropDown.isHidden = true
        }
        
        previousStepCons.isActive = false
        previousStepWidth.isActive = false
        
        let ges = UITapGestureRecognizer(target: self, action: #selector(noticeBtnPressed(_:)))
        noticeView.addGestureRecognizer(ges)
        noticeView.isUserInteractionEnabled = true
        
    }
    
    func creatUniqueID() {
        uniqueId = "\(randomAlphaNumericString(length: 8))-\(randomAlphaNumericString(length: 4))-\(randomAlphaNumericString(length: 4))-\(randomAlphaNumericString(length: 4))-\(randomAlphaNumericString(length: 12))"
    }
    
    func randomAlphaNumericString(length: Int) -> String {
        enum s {
            static let c = Array("abcdefghjklmnpqrstuvwxyz012345789")
            static let k = UInt32(c.count)
        }
        
        var result = [Character](repeating: "-", count: length)
        
        for i in 0..<length {
            let r = Int(arc4random_uniform(s.k))
            result[i] = s.c[r]
        }
        
        return String(result)
    }
    
    override func updateUnreadCount(notification: NSNotification) {
        //        Logger.d("car updateUnreadCount")
        // update notice read/unread status locally
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            self.setNoticeImage(with: readStatus)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //showStep(step: 1)
    }
    
    func setUpPickerView() {
        UIPicker.delegate = self as UIPickerViewDelegate
        UIPicker.dataSource = self as UIPickerViewDataSource
        self.view.addSubview(UIPicker)
        UIPicker.isHidden = true
        UIPicker.snp.makeConstraints { make in
            make.bottom.leading.trailing.equalToSuperview()
        }
        UIPicker.center = self.view.center
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.blue
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        carNumberTextfield.inputView = UIPicker
        carNumberTextfield.inputAccessoryView = toolBar
    }
    
    @objc func phoneCall() {
        if step3PhoneNum.text != nil {
            if let url = URL(string: "telprompt://\(step3PhoneNum.text!)") {
                UIApplication.shared.open(url)
            }
        }
        if let pp = self.patData {
            
        }
    }
    
    func showStep(step: Int) {
        step2BtnView.isHidden = true
        step2CollectionView.isHidden = true
        step1ScrollView.isHidden = true
        step1BtnView.isHidden = true
        step3View.isHidden = true
        step3BtnView.isHidden = true
        
        if step == 1 {
            step1ScrollView.isHidden = false
            step1BtnView.isHidden = false
        } else if step == 2 {
            step2BtnView.isHidden = false
            step2CollectionView.isHidden = false
        } else {
            step3View.isHidden = false
            step3BtnView.isHidden = false
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
        if let pp = self.patData {
            //Utility.sendEventLog(ServiceName: "PAT", UniqueId: self.uniqueId, EventName: "pat_reserve_back_btn", InboxId: String(pp.patId!))
        } else {
            //Utility.sendEventLog(ServiceName: "PAT", UniqueId: self.uniqueId, EventName: "pat_reserve_back_btn", InboxId: "")
        }
    }
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began: break
            case .moved: break
            case .ended:
                if slider == currentMileageSlider {
                    //Utility.sendEventLog(ServiceName: "PAT", UniqueId: self.uniqueId, EventName: "pat_current_miles_slider", InboxId: "")
                }
            default:
                break
            }
        }
    }
    
    // MARK: keyboard
    @objc override func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if let editingTextField = editingTextField {
                //                                let p = self.view.convert(editingTextField.center, to: self.view)
                let p = editingTextField.convert(editingTextField.center, to: self.view)
                //                                Logger.d("p.y: \(p.y)")
                //                                Logger.d("screen height: \(UiUtility.getScreenHeight())")
                //                                Logger.d("view.bounds.height: \(view.bounds.height)")
                
                let d = updateFromKeyboardChangeToFrame(keyboardSize)
                
                //                                Logger.d("view.bounds.height - p.y: \(view.bounds.height - p.y)")
                if view.bounds.height - p.y - 20 < d {
                    if self.view.frame.origin.y == offsetY {
                        //                        Logger.d("keyboardWillShow xx, \(d)")
                        self.view.frame.origin.y -= d
                    }
                }
            }
        }
        
        //        super.keyboardWillShow(notification: notification)
        keyboardShowing = true
    }
    
    @objc override func keyboardWillHide(notification: NSNotification) {
        
        if self.view.frame.origin.y != offsetY {
            self.view.frame.origin.y = offsetY
        }
        
        super.keyboardWillHide(notification: notification)
        keyboardShowing = false
    }
    
    // MARK: - funcs
    @IBAction func noticeBtnPressed(_ sender: Any) {
        //Utility.sendEventLog(ServiceName: "PAT", UniqueId: self.uniqueId, EventName: "pat_first_open_close", InboxId: "")
        UIView.animate(withDuration: 0.3) {
            self.noticeView.alpha = 0
        } completion: { succ in
            self.noticeView.isHidden = true
        }
    }
    
    // step1
    @IBAction func nextBtnPressed(_ sender: Any) {
        //Utility.sendEventLog(ServiceName: "PAT", UniqueId: self.uniqueId, EventName: "pat_next_step_btn", InboxId: "")
        if self.getCurrentMilData() != nil {
            CarManager.shared().getCarPAT(listener: self,
                                          vin: self.selectedCar.vin!,
                                          carNumber: self.selectedCar.licensePlateNumber!,
                                          currentMileage: Int(self.getCurrentMilData()!)!,
                                          annualMileage: nil,
                                          cycleDay: nil)
        } else {
            
        }
    }
    
    // step2
    @IBAction func saveBtnPressed(_ sender: Any) {
        if let pp = self.patData {
            CarManager.shared().saveCarPAT(listener: self, patId: pp.patId!)
            //Utility.sendEventLog(ServiceName: "PAT", UniqueId: self.uniqueId, EventName: "pat_recommend_save_btn", InboxId: String(pp.patId!))
        }
    }
    
    @IBAction func previousPageBtnPressed(_ sender: Any) {
        if let pp = self.patData {
            //Utility.sendEventLog(ServiceName: "PAT", UniqueId: self.uniqueId, EventName: "pat_recommend_prev_btn", InboxId: String(pp.patId!))
        }
        showStep(step: 1)
    }
    
    @IBAction func bookBtnPressed(_ sender: Any) {
        if let pp = self.patData {
            //Utility.sendEventLog(ServiceName: "PAT", UniqueId: self.uniqueId, EventName: "pat_recommend_reserve_btn", InboxId: String(pp.patId!))
        }
        if selectedCar != nil {
            UserManager.shared().getUserMaintenancePlant(accountId: UserManager.shared().currentUser.accountId!, vinCode: selectedCar.vin!, licenseNum: selectedCar.licensePlateNumber!, listener: self)
        }
    }
    
    override func rightButtonClicked(_ button: UIButton) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NotificationViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func step3PreviousBtnPressed(_ sender: Any) {
        if let pp = self.patData {
            //Utility.sendEventLog(ServiceName: "PAT", UniqueId: self.uniqueId, EventName: "pat_reserve_last_step_btn", InboxId: String(pp.patId!))
        }
        showStep(step: 2)
    }
    
    @IBAction func step3OtherBtnPressed(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.nearbyServiceCenter
        
        if let pp = self.patData {
            //Utility.sendEventLog(ServiceName: "PAT", UniqueId: self.uniqueId, EventName: "pat_reserve_other_workshop_btn", InboxId: String(pp.patId!))
            vc.patId = String(pp.patId!)
            vc.uniqueId = self.uniqueId
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Car degegate
    override func didStartSaveCarPAT() {
        showHud()
    }
    
    override func didFinishSaveCarPAT(success: Bool, msg: String?) {
        dismissHud()
        
        if success {
            showGeneralWarning(title: "", message: "專屬保修建議已發送至車主專區")
            if let pp = self.patData {
                //Utility.sendEventLog(ServiceName: "PAT", UniqueId: self.uniqueId, EventName: "pat_recommend_save_complete", InboxId: String(pp.patId!))
            }
        } else {
            showGeneralWarning(title: "", message: msg)
        }
    }
    
    override func didStartGetCarPAT() {
        showHud()
    }
    
    override func didFinishGetCarPAT(success: Bool, pat: Pat?, msg: String?) {
        dismissHud()
        
        if !success {
            if let mm = msg {
                showApiFailureAlert(message: mm)
            } else {
                showApiFailureAlert(message: "資料獲取失敗")
            }
            return
        }
        
        if let pp = pat {
            self.patData = pp
            if pp.patData.count > 0 {
                self.patDataArray = pp.patData
                self.step2CollectionView.reloadData()
                step1BtnView.isHidden = true
                step1ScrollView.isHidden = true
                step2BtnView.isHidden = false
                step2CollectionView.isHidden = false
                step3View.isHidden = true
                step3View.isHidden = true
                previousStepCons.isActive = false
                previousStepWidth.isActive = false
                reserveBtn.isHidden = false
                saveBtn.isHidden = false
                
                if  pp.patData.count == 1 &&
                        pp.patData[0].predictedServiceMileage == "-" {
                    previousStepCons.isActive = true
                    previousStepWidth.isActive = true
                    reserveBtn.isHidden = true
                    saveBtn.isHidden = true
                }
                
            } else {
                showApiFailureAlert(message: "資料獲取失敗")
            }
        }
    }
    
    override func didStartGetUserMaintenancePlant() {
        showHud()
    }
    
    override func didFinishGetUserMaintenancePlant(success: Bool, maintenancePlant: MaintainancePlant?) {
        dismissHud()
        
        if success && maintenancePlant != nil {
            if maintenancePlant!.plantId != 0 {
                showStep(step: 3)
                step3Title.text = maintenancePlant!.plantName
                step3Address.text = maintenancePlant!.Address
                if maintenancePlant!.Phone != nil {
                    let txt = underLineText(text: maintenancePlant!.Phone!)
                    step3PhoneNum.attributedText = txt
                }
                return
            }
        }
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.nearbyServiceCenter
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - silder
    
    @IBAction func currentMileageValueChanged(_ sender: Any) {
        currentMileageTextfield.text = "\(Int(currentMileageSlider.value))"
    }
    
    func getCurrentMilData() -> String? {
        return currentMileageTextfield.text
    }
    
    // MARK: - textfield delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        editingTextField = textField
        if textField == carNumberTextfield {
            UIPicker.isHidden = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == carNumberTextfield {
            UIPicker.isHidden = true
            return
        }
        
        if textField == currentMileageTextfield {
            //Utility.sendEventLog(ServiceName: "PAT", UniqueId: self.uniqueId, EventName: "pat_current_miles_keypad", InboxId: "")
            if getCurrentMilData() != nil {
                let annInt = Int(getCurrentMilData()!)!
                if annInt < Int(currentMileageSlider.minimumValue) {
                    currentMileageTextfield.text = "\(Int(currentMileageSlider.minimumValue))"
                    currentMileageSlider.value = currentMileageSlider.minimumValue
                } else if annInt > Int(currentMileageSlider.maximumValue) {
                    currentMileageTextfield.text = "\(Int(currentMileageSlider.maximumValue))"
                    currentMileageSlider.value = currentMileageSlider.maximumValue
                } else {
                    currentMileageSlider.value = Float(getCurrentMilData()!)!
                }
            }
        }
    }
    
    func setupSlider() {
        if getCurrentMilData() != nil {
            let annInt = Int(getCurrentMilData()!)!
            if annInt < Int(currentMileageSlider.minimumValue) {
                currentMileageTextfield.text = "\(Int(currentMileageSlider.minimumValue))"
                currentMileageSlider.value = currentMileageSlider.minimumValue
            } else if annInt > Int(currentMileageSlider.maximumValue) {
                currentMileageTextfield.text = "\(Int(currentMileageSlider.maximumValue))"
                currentMileageSlider.value = currentMileageSlider.maximumValue
            } else {
                currentMileageSlider.value = Float(getCurrentMilData()!)!
            }
        }
    }
    
    // MARK: - PickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cars!.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let rowdata = cars![row]
        return rowdata.licensePlateNumber
    }
    
    @objc func donePicker() {
        selectedCar = cars[UIPicker.selectedRow(inComponent: 0)]
        refreshCarInfo()
        carNumberTextfield.resignFirstResponder()
    }
    
    @objc func cancelPicker() {
        carNumberTextfield.resignFirstResponder()
    }
    
    func refreshCarInfo() {
        if cars.count <= 1 {
            carNumberTextfield.isUserInteractionEnabled = false
        }
        vinCode.text = selectedCar.vin!
        carNumberTextfield.text = selectedCar.licensePlateNumber!
    }
    
    // MARK: - CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if self.patDataArray != nil {
            return 1
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.patDataArray!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PATResultCell", for: indexPath) as! PATResultCell
        let myPatData = patDataArray![indexPath.item]
        cell.adviceDateLabel.text = myPatData.predictedServiceDate
        cell.adviceMilLabel.text = myPatData.predictedServiceMileage
        var ssString = ""
        for (i, ss) in myPatData.service!.enumerated() {
            if i == 0 {
                ssString.append("\(ss)")
            } else {
                ssString.append("\n\(ss)")
            }
        }
        cell.adviceMustListLabel.text = ssString
        var aaString = ""
        for (i, aa) in myPatData.additional!.enumerated() {
            if i == 0 {
                aaString.append("\(aa)")
            } else {
                aaString.append("\n\(aa)")
            }
        }
        
        if indexPath.item == 0 {
            // first, different title
            cell.patTitleLabel.text = "STEP 2.建議維修保養項目"
        } else {
            cell.patTitleLabel.text = "-----------------------"
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        let size = CGSize(width: width, height: 0)
        // assuming your collection view cell is a nib
        // you may also instantiate an instance of your cell if it doesn't use a Nib
        // let sizingCell = MyCollectionViewCell()
        let sizingCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PATResultCell", for: indexPath) as! PATResultCell
        sizingCell.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        sizingCell.frame.size = size
        //        sizingCell.configure(with: patDataArray[indexPath.item]) // what ever method configures your cell
        return sizingCell.contentView.systemLayoutSizeFitting(size, withHorizontalFittingPriority: UILayoutPriority.required, verticalFittingPriority: UILayoutPriority.defaultLow)
    }
}
