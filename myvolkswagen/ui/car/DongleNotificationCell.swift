//
//  DongleNotificationCell.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/11/8.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

class DongleNotificationCell: UICollectionViewCell {
    @IBOutlet weak var dTitle: UILabel!
    @IBOutlet weak var dDescription: UILabel!
    
}
