//
//  UnderlineInput.swift
//  myvolkswagen
//
//  Created by Apple on 2/23/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class UnderlineInput: UIView {
    
    weak var contentView: UIView!
    @IBOutlet weak var textField: UITextField!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    private func commonInit() {
        contentView = (Bundle.main.loadNibNamed("UnderlineInput", owner: self, options: nil)?.first as! UIView)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        textField.textColor = UIColor.black
        textField.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 16))
        
//        underline.backgroundColor = colorUnderline
    }
    
    func setHint(_ text: String) {
        textField.attributedPlaceholder = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor: UIColor.black.withAlphaComponent(0.5)])

    }
}
