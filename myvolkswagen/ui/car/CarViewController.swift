//
//  CarViewController.swift
//  myvolkswagen
//
//  Created by Apple on 2/1/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase
import SafariServices
import WebKit

class CarViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, WKNavigationDelegate {
    
    enum Tab1Section: Int {
        case paddingTop = 0, maintainance
        static var allCases: [Tab1Section] {
            var values: [Tab1Section] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    enum CardState: Int {
        case collapse = 0, expansion
    }
    
    var targetTab: Int?
    
    var cars: [Car] = []
    var selectedIndex: Int = 0
    var cardState: CardState = .collapse
    var carBtns: [CarBtn]?
    var webview: WKWebView!
    @IBOutlet weak var webviewContainer: UIView!
    
    var addNow: Bool = false
    
    // empty view
    @IBOutlet weak var containerEmpty: UIView!
    @IBOutlet weak var emptyText: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnsCollectionView: UICollectionView!
    
    @IBOutlet weak var indicator: UIImageView!
    @IBOutlet weak var indicatorPageView: UIPageControl!
    
    @IBOutlet weak var buttonShield: UIButton!
    //@IBOutlet weak var containerCard: UIView!
    @IBOutlet weak var constraintContainerCard: NSLayoutConstraint!
    var cardOrigCenterY: CGFloat = 0
    var cardMinCenterY: CGFloat = 0
    
    @IBOutlet weak var tabImage: UIImageView!
    @IBOutlet weak var contentTab0: UIView!
    @IBOutlet weak var contentTab1: UIView!
    
    @IBOutlet weak var tab0_title0: UILabel!
    @IBOutlet weak var tab0_title1: UILabel!
    @IBOutlet weak var tab0_title2: UILabel!
    @IBOutlet weak var tab0_title3: UILabel!
    @IBOutlet weak var tab0_title4: UILabel!
    @IBOutlet weak var tab0_value01: UILabel!
    @IBOutlet weak var tab0_value02: UILabel!
    @IBOutlet weak var tab0_value1: UILabel!
    @IBOutlet weak var tab0_value2: UILabel!
    @IBOutlet weak var tab0_value3: UILabel!
    @IBOutlet weak var tab0_value4: UILabel!
    @IBOutlet weak var tab0_title5: UILabel!
    @IBOutlet weak var tab0_value5: UILabel!
    @IBOutlet weak var tab0_value5_info: UILabel!
    @IBOutlet weak var constraintTab0Title1Top: NSLayoutConstraint!
//    @IBOutlet weak var constraintTab0Value1Top: NSLayoutConstraint!
    @IBOutlet weak var constraintTab0Title2Top: NSLayoutConstraint!
//    @IBOutlet weak var constraintTab0Value2Top: NSLayoutConstraint!
    @IBOutlet weak var constraintTab0Title3Top: NSLayoutConstraint!
//    @IBOutlet weak var constraintTab0Value3Top: NSLayoutConstraint!
    @IBOutlet weak var constraintTab0Title4Top: NSLayoutConstraint!
//    @IBOutlet weak var constraintTab0Value4Top: NSLayoutConstraint!
    
    @IBOutlet weak var tab1_tableView: UITableView!
    
    @IBOutlet var orderDetailView: UIView!
    @IBOutlet weak var orderDetailTitle: UILabel!
    @IBOutlet weak var orderDetailContent: UITextView!
    @IBOutlet weak var orderDetailNotice: UILabel!
    @IBOutlet weak var orderDetailTItle2Cons: NSLayoutConstraint!
    @IBOutlet weak var orderDetailTItle3Cons: NSLayoutConstraint!
    
    @IBOutlet weak var webviewTopConstraint: BasicConstraint!
    

    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()
        
        collectionView.backgroundColor = UiUtility.colorBackgroundWhite
        view.backgroundColor = UiUtility.colorBackgroundWhite
        
//        constraintContainerCard.constant = UiUtility.adaptiveSize(size: 22)
        
        containerEmpty.backgroundColor = UiUtility.colorBackgroundWhite
        emptyText.text = "car_empty_msg".localized
        emptyText.textColor = UIColor(rgb: 0x4A4A4A)
        emptyText.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
        
        updateIndicator()
        
        naviBar.setTitle("car".localized)
        naviBar.setLeftButton(UIImage(named: "vw"), highlighted: UIImage(named: "vw"))
        naviBar.setRightButton(UIImage(named: "notif_black"), highlighted: UIImage(named: "notif_black_unread"))
        imageViewNotification = naviBar.imageViewRight
        //naviBar.setRightButton1(UIImage(named: "addCar"), highlighted: UIImage(named: "addCar"))
        
//        var panGesture = UIPanGestureRecognizer()
//        panGesture = UIPanGestureRecognizer(target: self, action: #selector(draggedView(_:)))
//        containerCard.isUserInteractionEnabled = true
//        containerCard.addGestureRecognizer(panGesture)
        
        let font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        tab0_title0.textColor = UIColor.black
        tab0_title0.font = font
        tab0_title1.textColor = UIColor.black
        tab0_title1.font = font
        tab0_title2.textColor = UIColor.black
        tab0_title2.font = font
        tab0_title3.textColor = UIColor.black
        tab0_title3.font = font
        tab0_title4.textColor = UIColor.black
        tab0_title4.font = font
        tab0_title5.textColor = UIColor.black
        tab0_title5.font = font
        tab0_value01.textColor = UIColor.black
        tab0_value01.font = font
        tab0_value02.textColor = UIColor.black
        tab0_value02.font = font
        tab0_value1.textColor = UIColor.black
        tab0_value1.font = font
        tab0_value2.textColor = UIColor.black
        tab0_value2.font = font
        tab0_value3.textColor = UIColor.black
        tab0_value3.font = font
        tab0_value4.textColor = UIColor.black
        tab0_value4.font = font
        tab0_value5.textColor = UIColor.black
        tab0_value5.font = font
        
        let font2 = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 12))!
        tab0_value5_info.textColor = UiUtility.colorDark4A
        tab0_value5_info.font = font2
        let attStr = underLineText(text: "car_dealer_plant_info2".localized)
        let attributedText = NSMutableAttributedString(string: "car_dealer_plant_info1".localized)
        let attributedText2 = NSMutableAttributedString(string: "car_dealer_plant_info3".localized)
        attributedText.append(attStr)
        attributedText.append(attributedText2)
        tab0_value5_info.attributedText = attributedText
        tab0_value5_info.isUserInteractionEnabled = true
        tab0_value5_info.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goProfileClicked)))
        
        tab0_title0.text = "car_model".localized
        tab0_title1.text = "car_plate".localized
        tab0_title2.text = "car_vin".localized
        tab0_title3.text = "car_plate_created_date".localized
        tab0_title4.text = "car_dealer".localized
        tab0_title5.text = "car_dealer_plant".localized
        
//        NotificationCenter.default.addObserver(self,
//            selector: #selector(applicationDidBecomeActive),
//            name: UIApplication.didBecomeActiveNotification,
//            object: nil)
        
        let configuration = WKWebViewConfiguration()
        configuration.ignoresViewportScaleLimits = true
        configuration.suppressesIncrementalRendering = true
        configuration.allowsInlineMediaPlayback = true
        configuration.allowsAirPlayForMediaPlayback = false
        configuration.allowsPictureInPictureMediaPlayback = true
        configuration.mediaTypesRequiringUserActionForPlayback = .all
        configuration.setURLSchemeHandler(CustomSchemeHandler(), forURLScheme: Config.redirectScheme)
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        configuration.preferences = preferences
        webview = WKWebView(frame: self.webviewContainer.bounds, configuration: configuration)
        self.webviewContainer.addSubview(webview)
        webview.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
        let request = URLRequest(url: URL(string: "\(Config.urlMyCar)?token=\(UserManager.shared().getUserToken()!)&AccountId=\(UserManager.shared().currentUser.accountId!)")!)
        webview.load(request)
        webview.scrollView.bounces = false
        webview.navigationDelegate = self
        
        addFloatingBtnView()
    }
    
//    @objc func applicationDidBecomeActive() {
//        if let tabControllerDelegate = self.tabControllerDelegate {
//            if tabControllerDelegate.getTabbarIndex() == 1 {
//                if cars.count > 0 {
//                    carBtns = CarManager.shared().getCarBtns(vin: cars[selectedIndex].vin ?? "", plateNum: cars[selectedIndex].licensePlateNumber ?? "", listener: self)
//                }
//            }
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        buttonShield.isHidden = true
//        constraintContainerCard.constant = UiUtility.adaptiveSize(size: 22)
        cars = CarManager.shared().getCars(remotely: !addNow, listener: self)
        if !cars.isEmpty {
            if selectedIndex >= cars.count {
                selectedIndex = cars.count - 1
            }
            reloadUi()
        }
        
        // update notice read/unread status locally
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            self.setNoticeImage(with: readStatus)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if offsetY == -1 {
            offsetY = self.view.frame.origin.y
        }
        
        if addNow {
            addNow = false
            addCar()
        } else if let targetTab = targetTab, !cars.isEmpty {
            switch targetTab {
            case 0:
                self.tab0Clicked()
            case 1:
                self.tab1Clicked()
            default:
                break
            }
            
            //expandCard()
            self.targetTab = nil
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        self.tabControllerDelegate?.menuClicked()
    }
    
    override func rightButtonClicked(_ button: UIButton) {
        self.tabControllerDelegate?.notificationClicked()
    }
    
    override func rightButton1Clicked(_ button: UIButton) {
        Analytics.logEvent("add_car", parameters: ["screen_name": "screen_car" as NSObject])
        addCar()
    }
    
    @IBAction func orderDetailCloseBtnClicked(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.orderDetailView.alpha = 0
        }) { (success) in
            self.orderDetailView.isHidden = true
            self.orderDetailView.removeFromSuperview()
        }
    }
    
    override func updateUnreadCount(notification: NSNotification) {
//        Logger.d("car updateUnreadCount")
        // update notice read/unread status locally
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            self.setNoticeImage(with: readStatus)
        }
    }
    
    
    
    
    
    // MARK: car protocol
    
    override func didStartGetCar() {
        //showHud()
    }
    
    override func didFinishGetCar(success: Bool, cars: [Car]) {
        //dismissHud()
        
        self.cars = cars
        reloadUi()
        
//        if cars.count > 0 {
//            carBtns = CarManager.shared().getCarBtns(vin: cars[selectedIndex].vin ?? "", plateNum: cars[selectedIndex].licensePlateNumber ?? "", listener: self)
//        }
    }
    
    override func didStartRemoveCar() {}
    
    override func didFinishRemoveCar(success: Bool, car: Car) {
        cars = CarManager.shared().getCars(remotely: false, listener: nil)
        if selectedIndex >= cars.count {
            selectedIndex = cars.count - 1
        }
        reloadUi()
    }
    
    override func didStartGetCarBtns() {
        //showHud()
    }
    
    override func didFinishGetCarBtns(success: Bool, btns: [CarBtn]?) {
        //dismissHud()
        carBtns = []
        guard let btnss = btns else { return }
        for btn in btnss {
            if btn.Status != 0 {
                carBtns?.append(btn)
            }
        }
        self.btnsCollectionView.reloadData()
    }
    
    override func didStartGetUserMaintenancePlant() {
        showHud()
    }
    
    override func didFinishGetUserMaintenancePlant(success: Bool, maintenancePlant: MaintainancePlant?) {
        dismissHud()
        
        if success && maintenancePlant != nil {
            if maintenancePlant!.Url != nil {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
                vc.loadUrl = URL(string: maintenancePlant!.Url!)
                vc.index = .loadUrl
                self.navigationController?.pushViewController(vc, animated: true)
                return
            }
        }
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.nearbyServiceCenter
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
//    // MARK: text field delegate
//
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        editingTextField = textField
//    }
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == add0valueVin.textField {
//            let maxLength = 5
//            let currentString: NSString = textField.text! as NSString
//            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
//            return newString.length <= maxLength
//        }
//
//        return true
//    }
//
//
//
//    // MARK: keyboard
//
//    @objc override func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
//            if let editingTextField = editingTextField {
////                                let p = self.view.convert(editingTextField.center, to: self.view)
//                let p = editingTextField.convert(editingTextField.center, to: self.view)
////                                Logger.d("p.y: \(p.y)")
////                                Logger.d("screen height: \(UiUtility.getScreenHeight())")
////                                Logger.d("view.bounds.height: \(view.bounds.height)")
//
//                let d = updateFromKeyboardChangeToFrame(keyboardSize)
//
////                                Logger.d("view.bounds.height - p.y: \(view.bounds.height - p.y)")
//                if view.bounds.height - p.y - 20 < d {
//                    if self.view.frame.origin.y == offsetY {
////                        Logger.d("keyboardWillShow xx, \(d)")
//                        self.view.frame.origin.y -= d
//                    }
//                }
//            }
//        }
//
////        super.keyboardWillShow(notification: notification)
//        keyboardShowing = true
//    }
//
//    @objc override func keyboardWillHide(notification: NSNotification) {
//
//        if self.view.frame.origin.y != offsetY {
//            self.view.frame.origin.y = offsetY
//        }
//
//        super.keyboardWillHide(notification: notification)
//        keyboardShowing = false
//    }
    
    
    
    // MARK: collection view delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == btnsCollectionView {
            if let cc = self.carBtns {
                return cc.count
            }
            return 0
        }
        return cars.count
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == btnsCollectionView {
            if self.carBtns != nil {
                let hei = collectionView.frame.height - CGFloat(12).getFitSize()
                return CGSize(width: hei * 2, height: hei)
            }
            return CGSize.zero
        }
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        Logger.d("indexPath: \(indexPath)")
////        self.pageControl.currentPage = indexPath.section
//    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == btnsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarBtnsCollectionViewCell", for: indexPath) as! CarBtnsCollectionViewCell
            let btn = carBtns![indexPath.item]
            cell.imageView.image = UIImage(named: "btn_\(btn.BtnCode!)")
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarCell", for: indexPath)
        
//            button.setImage(UIImage.init(named: "kind_polo_normal"), for: .normal)
//            button.setImage(UIImage.init(named: "kind_polo_selected"), for: .selected)
        
        let labelTitle: UILabel = cell.viewWithTag(1) as! UILabel
        let labelSubtitle: UILabel = cell.viewWithTag(2) as! UILabel
        
        labelTitle.textColor = UiUtility.colorDarkGrey
        labelTitle.font = UIFont(name: "VWHead-Bold", size: UiUtility.adaptiveSize(size: 22))
//        labelTitle.text = "The Tiguan Allspace"
        
        labelSubtitle.textColor = UIColor(rgb: 0x4A4A4A)
//        for family in UIFont.familyNames.sorted() {
//            let names = UIFont.fontNames(forFamilyName: family)
//            print("Family: \(family) Font names: \(names)")
//        }
        
        labelSubtitle.font = UIFont(name: "VWHead", size: UiUtility.adaptiveSize(size: 14))
//        labelSubtitle.text = "380 TSI R-Line Performance"
        
        let photo: UIImageView = cell.viewWithTag(4) as! UIImageView
//        photo.image = UIImage.init(named: "test_car")
        
        let car: Car = cars[indexPath.row]
        labelTitle.text = car.carTypeName
        labelSubtitle.text = car.carModelName
        if let path = car.carModelImage {
            let url = URL(string: path)
            photo.kf.setImage(with: url)
            photo.contentMode = .scaleAspectFit
        }
        
        let buttonService: UIButton = cell.viewWithTag(3) as! UIButton
        buttonService.addTarget(self, action: #selector(nearbyServiceCenterClicked(_:)), for: .touchUpInside)
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == btnsCollectionView {
            let btn = carBtns![indexPath.item]
            if btn.BtnCode == "AddCar" {
                addCar()
            } else if btn.BtnCode == "PAT" {
                self.tabControllerDelegate?.goPatView(cars: self.cars)
            } else if btn.BtnCode == "BodyAndPaint" {
                goBnPWebview()
            } else if btn.BtnCode == "CarMaintenance" {
                UserManager.shared().getUserMaintenancePlant(accountId: UserManager.shared().currentUser.accountId!, vinCode: cars[selectedIndex].vin!, licenseNum: cars[selectedIndex].licensePlateNumber!, listener: self)
            } else if btn.BtnCode == "Recall" {
                if let uu = btn.url {
                    goRecallWebview(url: uu)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == btnsCollectionView {
            return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        }
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == btnsCollectionView {
            return 3
        }
        return 0
    }
    
    
    
    // MARK: scroll view delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        Logger.d("scrollView: \(scrollView)")
        
        if scrollView == self.collectionView/*let _ = scrollView as? UICollectionView*/ {
            selectedIndex = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
//            Logger.d("colection view selectedIndex: \(selectedIndex)")
            updateIndicator()
            //updateCard(with: cars[selectedIndex])
//            carBtns = CarManager.shared().getCarBtns(vin: cars[selectedIndex].vin ?? "", plateNum: cars[selectedIndex].licensePlateNumber ?? "", listener: self)
        }
    }
    
    
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Tab1Section.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Tab1Section.init(rawValue: section)! {
        case .paddingTop:
            return 1
        case .maintainance:
            if cars.count == 0 {
                return 0
            } else {
                let car: Car = cars[selectedIndex]
                return (car.maintainances ?? []).count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Tab1Section.init(rawValue: indexPath.section)! {
        case .paddingTop:
            return UiUtility.adaptiveSize(size: 20)
        case .maintainance:
            let car: Car = cars[selectedIndex]
            let maintainance = car.maintainances[indexPath.row]
            if let tt = maintainance.type {
                if tt.count == 1 {
                    return UiUtility.adaptiveSize(size: 63)
                } else if tt.count == 2 {
                    return UiUtility.adaptiveSize(size: 63) + UiUtility.adaptiveSize(size: 25)
                } else {
                    return UiUtility.adaptiveSize(size: 63) + UiUtility.adaptiveSize(size: 50)
                }
            }
            return UiUtility.adaptiveSize(size: 63)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        switch Tab1Section.init(rawValue: indexPath.section)! {
        case .paddingTop:
            cell = tableView.dequeueReusableCell(withIdentifier: "PaddingTopCell", for: indexPath)
            cell.selectionStyle = .none
        case .maintainance:
            cell = tableView.dequeueReusableCell(withIdentifier: "MaintainanceCell", for: indexPath)
            cell.selectionStyle = .none
            
            let labelDate: UILabel = cell.viewWithTag(1) as! UILabel
            labelDate.font = UIFont(name: "VWHead", size: UiUtility.adaptiveSize(size: 16))
            labelDate.textColor = UIColor.black
            
            let labelMilage: UILabel = cell.viewWithTag(2) as! UILabel
            let labelAmount: UILabel = cell.viewWithTag(3) as! UILabel
            let orderDetailType: UIView = cell.viewWithTag(8)!
            let img: UIImageView = cell.viewWithTag(10) as! UIImageView
            
            img.image = UIImage(named: "ic_arrow")?.withRenderingMode(.alwaysTemplate)
            img.tintColor = UIColor.lightGray
            
            labelMilage.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
            labelMilage.textColor = UiUtility.colorBrownGrey
            labelMilage.text = "car_milage".localized
            
            labelAmount.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
            labelAmount.textColor = UiUtility.colorBrownGrey
            labelAmount.text = "car_order_details".localized
            
            let labelValueMilage: UILabel = cell.viewWithTag(4) as! UILabel
            
            labelValueMilage.font = UIFont(name: "VWHead-Bold", size: UiUtility.adaptiveSize(size: 18))
            labelValueMilage.textColor = UIColor.black
            
            let colorYellow = UIColor(rgb: 0xEBA33B)
            let dot: UIView = cell.viewWithTag(6)!
            dot.backgroundColor = colorYellow
            dot.clipsToBounds = true
            dot.layer.cornerRadius = UiUtility.adaptiveSize(size: 3.5)
            
            let line: UIView = cell.viewWithTag(7)!
            line.backgroundColor = colorYellow
            
            let car: Car = cars[selectedIndex]
            let maintainance = car.maintainances[indexPath.row]
            
            labelDate.text = maintainance.closeDate
            if let mileage = maintainance.mileage {
                labelValueMilage.text = "\(mileage)"
            } else {
                labelValueMilage.text = nil
            }
            //labelValueAmount.text = maintainance.charge
            //                labelDate.text = "2018/12/12"
            //                labelValueMilage.text = "999,999"
            //                labelValueAmount.text = "999,999"
            
            for v in orderDetailType.subviews {
                v.isHidden = true
                v.layer.cornerRadius = UiUtility.adaptiveSize(size: 6)
                v.layer.borderWidth = 1
                v.layer.borderColor = UIColor.black.cgColor
            }
            if let tt = maintainance.type {
                if tt.count > 0 {
                    let lb: BasicLabel = orderDetailType.viewWithTag(11) as! BasicLabel
                    lb.text = tt[0]
                    lb.isHidden = false
                }
                if tt.count > 1 {
                    let lb: BasicLabel = orderDetailType.viewWithTag(12) as! BasicLabel
                    lb.text = tt[1]
                    lb.isHidden = false
                }
                if tt.count > 2 {
                    let lb: BasicLabel = orderDetailType.viewWithTag(13) as! BasicLabel
                    lb.text = tt[2]
                    lb.isHidden = false
                }
            }
            
            if indexPath.row == car.maintainances.count - 1 {
                line.isHidden = true
            } else {
                line.isHidden = false
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let car: Car = cars[selectedIndex]
        let maintainance = car.maintainances[indexPath.row]
        
        orderDetailTitle.text = "car_order_details_title".localized
        orderDetailTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 17))
        orderDetailNotice.text = "car_order_details_notice".localized
        orderDetailNotice.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
        orderDetailNotice.textColor = UiUtility.colorDarkGrey
        
        let lbTitle1: BasicLabel = orderDetailView.viewWithTag(301) as! BasicLabel
        let lbContent1: BasicLabel = orderDetailView.viewWithTag(302) as! BasicLabel
        let lbValueTitle: BasicLabel = orderDetailView.viewWithTag(303) as! BasicLabel
        let lbValue: BasicLabel = orderDetailView.viewWithTag(304) as! BasicLabel
        let lbTitle2: BasicLabel = orderDetailView.viewWithTag(305) as! BasicLabel
        let lbContent2: BasicLabel = orderDetailView.viewWithTag(306) as! BasicLabel
        let lbTitle3: BasicLabel = orderDetailView.viewWithTag(307) as! BasicLabel
        let lbContent3: BasicLabel = orderDetailView.viewWithTag(308) as! BasicLabel
        
        if let moe = maintainance.orderDetailsExternal {
            lbTitle1.text = "car_order_details_external".localized
            lbContent1.text = "\n\(moe)"
            if let price = maintainance.externalCharge {
                lbValueTitle.text = "\n小計"
                lbValue.text = "\n\(price)"
            } else {
                lbValueTitle.text = ""
                lbValue.text = ""
            }
        } else {
            lbTitle1.text = ""
            lbContent1.text = ""
            lbValueTitle.text = ""
            lbValue.text = ""
        }
        
        if let mow = maintainance.orderDetailsWarranty {
            lbContent2.text = "\n\(mow)"
            if maintainance.orderDetailsExternal != nil {
                lbTitle2.text = "\n" + "car_order_details_warranty".localized
                orderDetailTItle2Cons.constant = 10
            } else {
                lbTitle2.text = "car_order_details_warranty".localized
                orderDetailTItle2Cons.constant = 0
            }
        } else {
            lbTitle2.text = ""
            lbContent2.text = ""
            orderDetailTItle2Cons.constant = 0
        }
        
        if let moi = maintainance.orderDetailsInsurance {
            lbContent3.text = "\n\(moi)"
            if maintainance.orderDetailsExternal != nil ||
                maintainance.orderDetailsWarranty != nil {
                lbTitle3.text = "\n" + "car_order_details_insurance".localized
                orderDetailTItle3Cons.constant = 10
            } else {
                lbTitle3.text = "car_order_details_insurance".localized
                orderDetailTItle3Cons.constant = 0
            }
        } else {
            lbTitle3.text = ""
            lbContent3.text = ""
            orderDetailTItle3Cons.constant = 0
        }
        
        
//        orderDetailContent.flashScrollIndicators()
//        orderDetailContent.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        
        if let tabControllerDelegate = self.tabControllerDelegate {
            tabControllerDelegate.showOrderDetail(view: self.orderDetailView)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func shieldClicked() {
//        UIView.animate(withDuration: 0.3, animations: { () -> Void in
//            self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardOrigCenterY)
//            self.cardState = .collapse
//            self.buttonShield.isHidden = true
//            self.tab1_tableView.isUserInteractionEnabled = false
//        })
        
//        UIView.animate(withDuration: 0.3, animations: {
//            self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardOrigCenterY)
//        }) { (finished) in
//            self.constraintContainerCard.constant = UiUtility.adaptiveSize(size: 22)
//            self.cardState = .collapse
//            self.buttonShield.isHidden = true
//            self.tab1_tableView.isUserInteractionEnabled = false
//        }
    }
    
    @IBAction func tab0Clicked() {
        tabImage.isHighlighted = false
        contentTab0.isHidden = false
        contentTab1.isHidden = true
    }
    
    @IBAction func tab1Clicked() {
        let request = URLRequest(url: URL(string: "\(Config.urlMyCar)?tabType=CarMaintain&token=\(UserManager.shared().getUserToken()!)&AccountId=\(UserManager.shared().currentUser.accountId!)")!)
        webview.load(request)
    }
    
    @IBAction func addCarClicked(_ sender: UIButton) {
        Analytics.logEvent("add_car", parameters: ["screen_name": "screen_car" as NSObject])
        addCar()
    }
    
    @objc func goProfileClicked() {
        self.tabControllerDelegate?.goEditProfile()
    }
    
    private func addCar() {
        if cars.count >= Config.maxCarCount {
            let alertController = UIAlertController(title: nil, message: "car_reach_max".localized, preferredStyle: .alert)
            
            let action2 = UIAlertAction(title: "close".localized, style: .cancel) { (action:UIAlertAction) in
                
            }
            
            alertController.addAction(action2)
            present(alertController, animated: true, completion: nil)
        } else {
            self.tabControllerDelegate?.addCar()
        }
    }
    
    @IBAction func addCarBtnPressed(_ sender: Any) {
        addCar()
    }
    
    @IBAction func patBtnPressed(_ sender: Any) {
        //Utility.sendEventLog(ServiceName: "PAT", UniqueId: "", EventName: "pat_entry_point", InboxId: "")
        self.tabControllerDelegate?.goPatView(cars: self.cars)
    }

    private func updateIndicator() {
//        self.indicatorPageView.numberOfPages = cars.count
//        self.indicatorPageView.currentPage = selectedIndex
        
        switch cars.count {
        case 0, 1:
            indicator.isHidden = true
        case 2:
            indicator.isHidden = false
            indicator.image = UIImage.init(named: "indicator_\(selectedIndex+1)_2")
//            if selectedIndex == 0 {
//                indicator.image = UIImage.init(named: "indicator_1_2")
//            } else {
//                indicator.image = UIImage.init(named: "indicator_2_2")
//            }
        case 3:
            indicator.isHidden = false
            indicator.image = UIImage.init(named: "indicator_\(selectedIndex+1)_3")
//            if selectedIndex == 0 {
//                indicator.image = UIImage.init(named: "indicator_1_3")
//            } else if selectedIndex == 1 {
//                indicator.image = UIImage.init(named: "indicator_2_3")
//            } else {
//                indicator.image = UIImage.init(named: "indicator_3_3")
//            }
        case 4:
            indicator.isHidden = false
            indicator.image = UIImage.init(named: "indicator_\(selectedIndex+1)_4")
        case 5:
            indicator.isHidden = false
            indicator.image = UIImage.init(named: "indicator_\(selectedIndex+1)_5")
        default:
            indicator.isHidden = true
        }
    }
    
//    @objc func draggedView(_ sender:UIPanGestureRecognizer) {
////        self.view.bringSubviewToFront(containerCard)
//        let translation = sender.translation(in: self.view)
//        if cardOrigCenterY == 0 {
//            cardOrigCenterY = containerCard.center.y
////            Logger.d("card height: \(containerCard.frame.height)")
////            Logger.d("self.view height: \(view.frame.height)")
//
//            cardMinCenterY = view.frame.height - (containerCard.frame.height / 2)
//
////            Logger.d("cardMinCenterY: \(cardMinCenterY)")
////            Logger.d("cardOrigCenterY: \(cardOrigCenterY)")
//        }
//
//        containerCard.center = CGPoint(x: containerCard.center.x/* + translation.x*/, y: max(cardMinCenterY, min(containerCard.center.y + translation.y, cardOrigCenterY)))
//        sender.setTranslation(CGPoint.zero, in: self.view)
//
//
//
////        if sender.state == UIGestureRecognizer.State.began {
////            Logger.d("began")
////        } else if sender.state == UIGestureRecognizer.State.changed {
////            Logger.d("changed")
////        } else
//
////        buttonShield.isHidden = true
////        constraintContainerCard.constant = UiUtility.adaptiveSize(size: 22)
//        if sender.state == UIGestureRecognizer.State.ended {
//            let velocity = sender.velocity(in: view)
//            if velocity.y > 0 {
////                UIView.animate(withDuration: 0.3, animations: { () -> Void in
////                    self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardOrigCenterY)
////                    self.cardState = .collapse
////                    self.buttonShield.isHidden = true
////                    self.tab1_tableView.isUserInteractionEnabled = false
////                })
//
//                UIView.animate(withDuration: 0.3, animations: {
//                    self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardOrigCenterY)
//                }) { (finished) in
//                    self.constraintContainerCard.constant = UiUtility.adaptiveSize(size: 22)
//                    self.cardState = .collapse
//                    self.buttonShield.isHidden = true
//                    self.tab1_tableView.isUserInteractionEnabled = true
//                }
//            } else if velocity.y < 0 {
////                UIView.animate(withDuration: 0.3, animations: { () -> Void in
////                    self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardMinCenterY)
////                    self.cardState = .expansion
////                    self.buttonShield.isHidden = false
////                    self.tab1_tableView.isUserInteractionEnabled = true
////                })
//
//                UIView.animate(withDuration: 0.3, animations: {
//                    self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardMinCenterY)
//                }) { (finished) in
//                    self.constraintContainerCard.constant = UiUtility.adaptiveSize(size: 22) - (self.cardOrigCenterY - self.cardMinCenterY)
//                    self.cardState = .expansion
//                    self.buttonShield.isHidden = false
//                    self.tab1_tableView.isUserInteractionEnabled = true
//                }
//            } else {
//                if self.containerCard.center.y - self.cardMinCenterY > self.cardOrigCenterY - self.containerCard.center.y {
////                    UIView.animate(withDuration: 0.3, animations: { () -> Void in
////                        self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardOrigCenterY)
////                        self.cardState = .collapse
////                        self.buttonShield.isHidden = true
////                        self.tab1_tableView.isUserInteractionEnabled = false
////                    })
//
//                    UIView.animate(withDuration: 0.3, animations: {
//                        self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardOrigCenterY)
//                    }) { (finished) in
//                        self.constraintContainerCard.constant = UiUtility.adaptiveSize(size: 22)
//                        self.cardState = .collapse
//                        self.buttonShield.isHidden = true
//                        self.tab1_tableView.isUserInteractionEnabled = true
//                    }
//                } else {
////                    UIView.animate(withDuration: 0.3, animations: { () -> Void in
////                        self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardMinCenterY)
////                        self.cardState = .expansion
////                        self.buttonShield.isHidden = false
////                        self.tab1_tableView.isUserInteractionEnabled = true
////                    })
//
//                    UIView.animate(withDuration: 0.3, animations: {
//                        self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardMinCenterY)
//                    }) { (finished) in
//                        self.constraintContainerCard.constant = UiUtility.adaptiveSize(size: 22) - (self.cardOrigCenterY - self.cardMinCenterY)
//                        self.cardState = .expansion
//                        self.buttonShield.isHidden = false
//                        self.tab1_tableView.isUserInteractionEnabled = true
//                    }
//                }
//            }
//        }
//    }
//
//    private func expandCard() {
//        if cardOrigCenterY == 0 {
//            cardOrigCenterY = containerCard.center.y
//            cardMinCenterY = view.frame.height - (containerCard.frame.height / 2)
//        }
//
//        containerCard.center = CGPoint(x: containerCard.center.x, y: containerCard.center.y)
//
//        UIView.animate(withDuration: 0.3, animations: {
//            self.containerCard.center = CGPoint(x: self.containerCard.center.x, y: self.cardMinCenterY)
//        }) { (finished) in
//            self.constraintContainerCard.constant = UiUtility.adaptiveSize(size: 22) - (self.cardOrigCenterY - self.cardMinCenterY)
//            self.cardState = .expansion
//            self.buttonShield.isHidden = false
//            self.tab1_tableView.isUserInteractionEnabled = true
//        }
//    }
//
//    private func updateCard(with car: Car) {
//        containerCard.isHidden = false
//
//        // tab 0
//        tab0_value01.text = car.carTypeName
//        tab0_value02.text = car.carModelName
//        tab0_value1.text = car.licensePlateNumber
//        tab0_value2.text = car.vin
//        tab0_value3.text = car.carDate
//        tab0_value4.text = car.dealer
//        tab0_value5.text = car.dealerPlantName
//
//        if Utility.isEmpty(car.carModelName) {
//            constraintTab0Title1Top.constant = UiUtility.adaptiveSize(size: 62)
//            constraintTab0Title2Top.constant = UiUtility.adaptiveSize(size: 104)
//            constraintTab0Title3Top.constant = UiUtility.adaptiveSize(size: 146)
//            constraintTab0Title4Top.constant = UiUtility.adaptiveSize(size: 188)
//        } else {
//            constraintTab0Title1Top.constant = UiUtility.adaptiveSize(size: 88)
//            constraintTab0Title2Top.constant = UiUtility.adaptiveSize(size: 130)
//            constraintTab0Title3Top.constant = UiUtility.adaptiveSize(size: 172)
//            constraintTab0Title4Top.constant = UiUtility.adaptiveSize(size: 214)
//        }
//
//
//        // tab 1
//        tab1_tableView.reloadData()
//    }
    
    private func reloadUi() {
        if cars.isEmpty {
            containerEmpty.isHidden = false
        } else {
            containerEmpty.isHidden = true

            //updateCard(with: cars[selectedIndex])
            collectionView.reloadData()
            updateIndicator()
        }
    }

    
    
    @objc private func nearbyServiceCenterClicked(_ sender: Any) {
        Analytics.logEvent("car_reservation", parameters: nil)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.nearbyServiceCenter
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goBnPWebview() {
        Analytics.logEvent("pat_entry_point", parameters: nil)
        let accId = UserManager.shared().currentUser.accountId!
        let licNum = cars[selectedIndex].licensePlateNumber!
        let bnpString = "AccountId=\(accId)&PlateNum=\(licNum)"
        let bnpStringEncode = bnpString.toBase64URL()
        let bnpUrlEncode = URL(string: "\(Config.bnpUrl)?\(bnpStringEncode)")!
        if #available(iOS 14.5, *) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.index = .BnP
            vc.loadUrl = bnpUrlEncode
            self.tabControllerDelegate?.goViewController(view: vc)
        } else if #available(iOS 13.0, *) {
            let url = bnpUrlEncode
            let safari = SFSafariViewController(url: url)
            self.navigationController?.present(safari, animated: true, completion: nil)
        } else {
            UIApplication.shared.open(bnpUrlEncode)
        }
    }
    
    func goRecallWebview(url: String) {
        //Analytics.logEvent("pat_entry_point", parameters: nil)
        if let uu = URL(string: url) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.index = .recall
            vc.loadUrl = uu
            self.tabControllerDelegate?.goViewController(view: vc)
        }
    }
    
    @IBAction func resetWebview(_ sender: Any) {
        let request = URLRequest(url: URL(string: "\(Config.urlMyCar)?token=\(UserManager.shared().getUserToken()!)&AccountId=\(UserManager.shared().currentUser.accountId!)")!)
        webview.load(request)
    }
    
    // MARK: - webview
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let urll = navigationAction.request.url!.absoluteString.removingPercentEncoding!
        
        if urll == "about:blank" {
            //decisionHandler(.cancel)
            decisionHandler(.allow)
            return
        }
        
        if urll.contains("myCar.html")  {
            webviewTopConstraint.constant = CGFloat(0).getFitSize()
            showFloatingBtn(show: true)
        } else if navigationAction.request.url!.path.contains("service") {
            webviewTopConstraint.constant = CGFloat(0).getFitSize()
            showFloatingBtn(show: false)
        } else {
            webviewTopConstraint.constant = CGFloat(45).getFitSize()
            showFloatingBtn(show: false)
        }
        
        if urll.contains("#PAT") {
//            self.tabControllerDelegate?.goPatView(cars: self.cars)
//            decisionHandler(.cancel)
//            return
            naviBar.setTitle("car_pat".localized)
        } else if urll.contains("#dccsearch") {
            naviBar.setTitle("car_reservation".localized)
        } else if urll.contains("bodyandpaint") {
            if #available(iOS 14.5, *) {
            } else if #available(iOS 13.0, *) {
                let safari = SFSafariViewController(url: navigationAction.request.url!)
                self.navigationController?.present(safari, animated: true, completion: nil)
                decisionHandler(.cancel)
                return
            } else {
                UIApplication.shared.open(navigationAction.request.url!)
                decisionHandler(.cancel)
                return
            }
            let urllArr = urll.components(separatedBy: "#")
            if urllArr.count > 1 {
                //let one: String = urllArr[0]
                let two: String = urllArr[1]
                naviBar.setTitle(two)
            }
        } else if urll.contains("addCar") {
            naviBar.setTitle("選擇車型".localized)
        } else if navigationAction.request.url!.path.contains("service") {
            naviBar.setTitle("車主服務".localized)
        } else if navigationAction.request.url!.host!.contains("service-cam.volkswagen-service.com") {
            naviBar.setTitle("建議加修報價".localized)
        } else {
            let urllArr = urll.components(separatedBy: "#")
            if urllArr.count > 1 {
                //let one: String = urllArr[0]
                let two: String = urllArr[1]
                naviBar.setTitle(two)
            } else {
                naviBar.setTitle("car".localized)
            }
        }
        
        if navigationAction.request.url?.scheme == "tel" {
            UIApplication.shared.open(navigationAction.request.url!)
            decisionHandler(.cancel)
        } else {
            if navigationAction.targetFrame == nil {
                webView.load(navigationAction.request)
            }
            decisionHandler(.allow)
        }
    }
}
