//
//  MyGarageViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/19.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
import Kingfisher

protocol MyGarageDelegate: class {
    func didSelectCarFromGarage()
}

class MyGarageViewController: NaviViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, GarageCellDelegate, CarSettingSheetDelegate {
    
    var cars: [Car]?
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    weak var delegate: MyGarageDelegate?
    var dongleFinished = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        naviBar.setTitle("我的車庫")
        naviBar.setLeftButton(UIImage(named: "back_btn"), highlighted: UIImage(named: "back_btn"))
        
        collectionView.register(UINib(nibName: "GarageCardCell", bundle: nil), forCellWithReuseIdentifier: "GarageCardCell")
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadCars), name: NSNotification.Name(rawValue: CarManager.nameCarReloadNotification), object: nil)
        
        let gradientVieww = GradientBackgroundView()
        gradientVieww.frame = view.bounds
        gradientView.addSubview(gradientVieww)
        
        if self.cars == nil {
            self.cars = CarManager.shared().getCars(remotely: true, listener: self)
        } else {
            dongleFinished = self.cars!.count
            for cc in self.cars! {
                CarManager.shared().getDongleBindingInfo(listener: self, plateNo: cc.licensePlateNumber!)
            }
        }
        showHud()
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
        Utility.sendEventLog(ServiceName: "pv_vehicle", UniqueId: "", EventName: "上一頁", InboxId: "", EventCategory: "button", PageName: "pv_vehicle", EventLabel: "上一頁", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
    }
    
    @IBAction func addCarBtnPressed(_ sender: Any) {
        let sb = UIStoryboard(name: "Login", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "AddCarNavi") as! UINavigationController
        self.navigationController?.present(vc, animated: true)
        Utility.sendEventLog(ServiceName: "pv_vehicle", UniqueId: "", EventName: "新增愛車", InboxId: "", EventCategory: "button", PageName: "pv_vehicle", EventLabel: "新增愛車", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Utility.sendEventLog(ServiceName: "pv_vehicle", UniqueId: "", EventName: "pageview", InboxId: "", EventCategory: "pageview", PageName: "pv_vehicle", EventLabel: "", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CarManager.nameCarReloadNotification), object: nil)
    }
    
    @objc func reloadCars() {
        self.cars = CarManager.shared().getCars(remotely: true, listener: self)
    }
    
    // MARK: car protocol
    
    override func didStartGetCar() {
    }
    
    override func didFinishGetCar(success: Bool, cars: [Car]) {
        //dismissHud()
        
        print("didFinishGetCar: \(cars)")
        if success {
            self.cars = cars
            self.dongleFinished = cars.count
            //self.collectionView.reloadData()
            
            if let sn = CarManager.shared().getSelectedCarNumber() {
                UserManager.shared().loginCruisys(listener: self, accountId: UserManager.shared().currentUser.accountId!, plateNum: sn)
            }
        }
    }
    
    override func didStartLoginCruisys() {
    }
    
    override func didFinishLoginCruisys(success: Bool) {
        for cc in self.cars! {
            CarManager.shared().getDongleBindingInfo(listener: self, plateNo: cc.licensePlateNumber!)
        }
    }
    
    override func didStartGetDongleBinding() {
    }
    
    override func didFinishGetDongleBinding(state: CarDoungleState?, dongleData: DongleData?) {
        print("didFinishGetDongleBinding state:(\(String(describing: state)), dongleData:\(String(describing: dongleData))")
        for cc in self.cars! {
            if cc.vin == dongleData?.vin {
                cc.dongleData = dongleData
            }
        }
        dongleFinished = dongleFinished - 1
        if dongleFinished == 0 { // finished
            dismissHud()
            self.collectionView.reloadData()
        }
    }
    
    override func didStartRemoveCar() {}
    
    override func didFinishRemoveCar(success: Bool, car: Car) {
        self.cars = CarManager.shared().cars
        if car.licensePlateNumber == CarManager.shared().getSelectedCarNumber() {
            CarManager.shared().clearSelectedCarNumber()
        }
        self.collectionView.reloadData()
        
        guard let mycars = self.cars
        else {
            back()
            return
        }
        if mycars.count == 0 {
            back()
        }
    }
    
    // MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let mycars = self.cars else { return 0 }
        return mycars.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GarageCardCell", for: indexPath) as! GarageCardCell
        guard let mycars = self.cars else { return cell }
        let car = mycars[indexPath.item]
        if let urll = URL(string: car.carModelImage ?? "") {
            cell.carimg.kf.setImage(with: urll, placeholder: nil)
        } else {
            cell.carimg.image = nil
        }
        cell.carTypeLb.text = car.carTypeName
        cell.carNumLb.text = car.licensePlateNumber
        cell.layer.cornerRadius = CGFloat(10).getFitSize()
        cell.delegate = self
        cell.indexpath = indexPath
        if CarManager.shared().getSelectedCarNumber() == car.licensePlateNumber {
            cell.layer.borderColor = UIColor.MyTheme.secondaryColor100.cgColor
            cell.layer.borderWidth = 2
        } else {
            cell.layer.borderWidth = 0
        }
        if car.dongleData?.state == "VIN_MAPPED" {
            cell.assistanceLb.text = "未綁定 Volkswagen 行車助理"
        } else if car.dongleData?.state == "USER_MAPPED" {
            cell.assistanceLb.text = "已綁定 Volkswagen 行車助理"
        } else {
            cell.assistanceLb.text = ""
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width - 48, height: (collectionView.frame.size.width - 48) * 250/345)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 24, left: 24, bottom: 24, right: 24)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let mycars = self.cars else { return }
        let car = mycars[indexPath.item]
        CarManager.shared().saveSelectedCarNumer(with: car.licensePlateNumber!)
        delegate?.didSelectCarFromGarage()
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - GarageCellDelegate
    func garageCarSettingBtnPressed(indexPath: IndexPath) {
        // show setting view
        showBottomSheetButtonTapped(ind: indexPath)
        Utility.sendEventLog(ServiceName: "pv_vehicle", UniqueId: "", EventName: "管理車輛", InboxId: "", EventCategory: "button", PageName: "pv_vehicle", EventLabel: "管理車輛", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
    }
    
    func showBottomSheetButtonTapped(ind: IndexPath) {
        let sb = UIStoryboard(name: "Car", bundle: nil)
        let bottomSheetVC = sb.instantiateViewController(withIdentifier: "CarSettingSheetViewController") as! CarSettingSheetViewController
        bottomSheetVC.delegate = self
        bottomSheetVC.indexPath = ind
        let ccar = self.cars![ind.item]
        if ccar.dongleData?.state == "USER_MAPPED" {
            bottomSheetVC.dongleState = .USER_MAPPED
        } else if ccar.dongleData?.state == "VIN_MAPPED" {
            bottomSheetVC.dongleState = .VIN_MAPPED
        }
    
        let options = SheetOptions(
            // The full height of the pull bar. The presented view controller will treat this area as a safearea inset on the top
            pullBarHeight: 8,

            // The corner radius of the shrunken presenting view controller
            presentingViewCornerRadius: 20,

            // Extends the background behind the pull bar or not
            shouldExtendBackground: true,

            // Attempts to use intrinsic heights on navigation controllers. This does not work well in combination with keyboards without your code handling it.
            setIntrinsicHeightOnNavigationControllers: true,

            // Pulls the view controller behind the safe area top, especially useful when embedding navigation controllers
            useFullScreenMode: true,

            // Shrinks the presenting view controller, similar to the native modal
            shrinkPresentingViewController: true,

            // Determines if using inline mode or not
            useInlineMode: false,

            // Adds a padding on the left and right of the sheet with this amount. Defaults to zero (no padding)
            horizontalPadding: 0,

            // Sets the maximum width allowed for the sheet. This defaults to nil and doesn't limit the width.
            maxWidth: nil
        )

        let sheetController = SheetViewController(
            controller: bottomSheetVC,
            sizes: [.fixed(186 + CGFloat(180).getFitSize())])

        // The corner radius of the sheet
        sheetController.cornerRadius = 20

        // minimum distance above the pull bar, prevents bar from coming right up to the edge of the screen
        sheetController.minimumSpaceAbovePullBar = 0

        // Determine if the rounding should happen on the pullbar or the presented controller only (should only be true when the pull bar's background color is .clear)
        sheetController.treatPullBarAsClear = false

        // Disable the dismiss on background tap functionality
        sheetController.dismissOnOverlayTap = true

        // Disable the ability to pull down to dismiss the modal
        sheetController.dismissOnPull = true

        sheetController.shouldDismiss = { _ in
        // This is called just before the sheet is dismissed. Return false to prevent the build in dismiss events
            return true
        }
        sheetController.didDismiss = { _ in
            // This is called after the sheet is dismissed
        }
        self.present(sheetController, animated: true, completion: nil)
    }
    
    func showRemoveWarning(car: Car) {
        let customAlert = VWCustomAlert()
        customAlert.alertTitle = "確定解除綁定愛車？"
        customAlert.alertMessage = "您即將解除綁定愛車 \(car.licensePlateNumber!)"
        customAlert.alertTag = 1
        customAlert.isCancelButtonHidden = false
        customAlert.okButtonAction = {
            // 先移除行車助理
            if car.dongleData != nil {
                CarManager.shared().removeDongleBindingInGarage(listener: self, plateNo: car.licensePlateNumber!, car: car)
            } else {
                CarManager.shared().remove(car: car, listener: self)
            }
        }
        self.present(customAlert, animated: true, completion: nil)
    }
    
    override func didFinishDeleteDongleBinding(success: Bool, car: Car?) {
        if success {
            // 移除完成再解除綁定愛車
            if car != nil {
                CarManager.shared().remove(car: car!, listener: self)
            }
        }
    }
    
    // CarSettingSheetDelegate
    func bindAssistanceBtnPressed(indexPath: IndexPath) {
        let cars = CarManager.shared().getCars(remotely: true, listener: self)
        let car = cars[indexPath.item]
        if car.dongleData?.state == "USER_MAPPED" {
            let customAlert = VWCustomAlert()
            customAlert.alertTitle = "確定解除綁定行車助理？"
            customAlert.alertMessage = "您即將解除綁定行車助理"
            customAlert.alertTag = 1
            customAlert.isCancelButtonHidden = false
            customAlert.okButtonAction = {
                CarManager.shared().removeDongleBinding(listener: self, plateNo: car.licensePlateNumber!, car: nil)
            }
            self.present(customAlert, animated: true, completion: nil)
            Utility.sendEventLog(ServiceName: "pv_vehicle", UniqueId: "", EventName: "解綁行車助理", InboxId: "", EventCategory: "button", PageName: "pv_vehicle", EventLabel: "解綁行車助理", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        } else {
            let sb = UIStoryboard(name: "Login", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "BindAssistantViewController") as! BindAssistantViewController
            vc.carNumber = car.licensePlateNumber
            vc.type = .profile
            self.navigationController?.present(vc, animated: true)
            Utility.sendEventLog(ServiceName: "pv_vehicle", UniqueId: "", EventName: "綁定行車助理", InboxId: "", EventCategory: "button", PageName: "pv_vehicle", EventLabel: "綁定行車助理", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        }
    }
    
    func unbindCarBtnPressed(indexPath: IndexPath) {
        guard let mycars = self.cars else { return }
        let car = mycars[indexPath.item]
        showRemoveWarning(car: car)
    }
    
    func carPriceBtnPressed(indexPath: IndexPath) {
        guard let mycars = self.cars else { return }
        let ccar = mycars[indexPath.item]
        guard let getvin = ccar.vin else { return }
        guard let licNum = ccar.licensePlateNumber else { return }
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.loadUrl
        vc.mytitle = "cpo_car".localized
        
        let userToken = UserManager.shared().getUserToken()!
        let accId = UserManager.shared().currentUser.accountId!
        let paramString = "token=\(userToken)&Vin=\(getvin)&AccountId=\(accId)&LicensePlateNumber=\(licNum)"
        
        vc.loadUrl = URL(string: "\(Config.urlCpoCar)?\(paramString)")!
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
