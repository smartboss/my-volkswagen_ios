//
//  MyDealerInfoViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/5.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

class MyDealerInfoViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func goSetupDealer(_ sender: Any) {
        self.dismiss(animated: true)
#if DEVELOPMENT
            if Deeplinker.handleDeeplink(url: URL(string: "myvolkswagendev://member/profile")!, ref: nil) {
                Deeplinker.checkDeepLink()
            }
#else
            if Deeplinker.handleDeeplink(url: URL(string: "myvolkswagen://member/profile")!, ref: nil) {
                Deeplinker.checkDeepLink()
            }
#endif
    }
}
