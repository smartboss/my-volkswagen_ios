//
//  CarSettingSheetViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/1.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

protocol CarSettingSheetDelegate: class {
    func bindAssistanceBtnPressed(indexPath: IndexPath)
    func unbindCarBtnPressed(indexPath: IndexPath)
    func carPriceBtnPressed(indexPath: IndexPath)
}

class CarSettingSheetViewController: BaseViewController {
    
    weak var delegate: CarSettingSheetDelegate?
    var indexPath: IndexPath!
    var dongleState: CarDoungleState!
    @IBOutlet weak var dongleBtn: VWButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if dongleState == .USER_MAPPED {
            dongleBtn.setTitle("解除綁定行車助理", for: .normal)
        } else if dongleState == .VIN_MAPPED {
            dongleBtn.setTitle("綁定行車助理", for: .normal)
        } else {
            dongleBtn.removeFromSuperview()
        }
    }
    
    @IBAction func bindAssistanceBtnPressed(_ sender: Any) {
        if dongleState == .USER_MAPPED {
//            Utility.sendEventLog(ServiceName: "pv_vehicle", UniqueId: "", EventName: "解綁行車助理", InboxId: "", EventCategory: "button", PageName: "pv_vehicle", EventLabel: "解綁行車助理", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        } else if dongleState == .VIN_MAPPED {
//            Utility.sendEventLog(ServiceName: "pv_vehicle", UniqueId: "", EventName: "綁定行車助理", InboxId: "", EventCategory: "button", PageName: "pv_vehicle", EventLabel: "綁定行車助理", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        }
        self.dismiss(animated: true) {
            self.delegate?.bindAssistanceBtnPressed(indexPath: self.indexPath)
        }
    }
    
    @IBAction func unbindCarBtnPressed(_ sender: Any) {
        Utility.sendEventLog(ServiceName: "pv_vehicle", UniqueId: "", EventName: "解綁愛車", InboxId: "", EventCategory: "button", PageName: "pv_vehicle", EventLabel: "解綁愛車", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        self.dismiss(animated: true) {
            self.delegate?.unbindCarBtnPressed(indexPath: self.indexPath)
        }
    }
    
    @IBAction func carPriceBtnPressed(_ sender: Any) {
        Utility.sendEventLog(ServiceName: "pv_vehicle", UniqueId: "", EventName: "愛車鑑價", InboxId: "", EventCategory: "button", PageName: "pv_vehicle", EventLabel: "愛車鑑價", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        self.dismiss(animated: true) {
            self.delegate?.carPriceBtnPressed(indexPath: self.indexPath)
        }
    }
}
