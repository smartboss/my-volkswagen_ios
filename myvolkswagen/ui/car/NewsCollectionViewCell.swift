//
//  NewsCollectionViewCell.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/20.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var titleLb: BasicLabel!
    @IBOutlet weak var dateLb: BasicLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
