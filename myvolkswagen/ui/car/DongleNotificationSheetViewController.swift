//
//  DongleNotificationSheetViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/11/8.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
protocol DongleNotificationSheetDelegate: NSObject {
    func didPressDongleNotificationSheetBottomBtn(ind: Int)
}

class DongleNotificationSheetViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var naviGrayBar: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottomBtn: VWButton!
    lazy var pageControll: VWPageControl = {
        let pc                   = VWPageControl(numberOfPages: self.dongleNoti.count, currentPage: 0, isCircular: true)
        pc.currentIndicatorColor = .black
        return pc
    }()
    @IBOutlet weak var pageControlView: UIView!
    weak var delegate: DongleNotificationSheetDelegate?
    var dongleNoti: [Notificationn]!
    var currentIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    func setupUI() {
        let long: CGFloat = 16
        let short: CGFloat = (CGFloat((self.dongleNoti.count - 1)) * CGFloat(8).getFitSize())
        let spacing: CGFloat = CGFloat((self.dongleNoti.count - 1)) * 4
        let mywidth = long + short + spacing + 10
        // 長 + 短 + spacing
        pageControll = VWPageControl(numberOfPages: self.dongleNoti.count, currentPage: 0, isCircular: true)
        pageControll.backgroundColor = UIColor.clear

        // 添加到视图
        pageControlView.addSubview(pageControll)
        pageControll.snp.makeConstraints { make in
            make.width.equalTo(mywidth)
            make.height.equalTo(CGFloat(4).getFitSize())
            make.centerX.centerY.equalToSuperview()
        }
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func bottomBtnPressed(_ sender: Any) {
        dismiss(animated: true) {
            self.delegate?.didPressDongleNotificationSheetBottomBtn(ind: self.pageControll.currentpage)
        }
    }
    
    //MARK: ScrollView Delegate
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        guard let visible = collectionView.visibleCells.first else { return }
        guard let index = collectionView.indexPath(for: visible)?.row else { return }
        pageControll.currentpage = index

    }
    
    // MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dongleNoti.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DongleNotificationCell", for: indexPath) as! DongleNotificationCell
        let dd = dongleNoti[indexPath.item]
        cell.dTitle.text = dd.descriptionn
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
