//
//  GarageCardCell.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/19.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
protocol GarageCellDelegate: class {
    func garageCarSettingBtnPressed(indexPath: IndexPath)
}

class GarageCardCell: UICollectionViewCell {
    
    @IBOutlet weak var carimg: UIImageView!
    @IBOutlet weak var carTypeLb: UILabel!
    @IBOutlet weak var carNumLb: UILabel!
    @IBOutlet weak var assistanceLb: UILabel!
    @IBOutlet weak var bgview: UIView!
    var indexpath: IndexPath!
    weak var delegate: GarageCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = CGFloat(10).getFitSize()
        //        contentView.layer.masksToBounds = false
        //        layer.masksToBounds = false
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowRadius = CGFloat(10).getFitSize()
        self.contentView.layer.cornerRadius = CGFloat(10).getFitSize()
        self.contentView.layer.masksToBounds = true
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.clipsToBounds = false
    }
    
    @IBAction func settingBtnPressed(_ sender: Any) {
        delegate?.garageCarSettingBtnPressed(indexPath: self.indexpath)
    }
}

extension UIColor {
    convenience init?(hex: String) {
        var hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")
        
        var rgb: UInt32 = 0
        
        Scanner(string: hexSanitized).scanHexInt32(&rgb)
        
        let r = (rgb & 0xFF0000) >> 16
        let g = (rgb & 0x00FF00) >> 8
        let b = rgb & 0x0000FF
        
        self.init(red: CGFloat(r) / 0xFF, green: CGFloat(g) / 0xFF, blue: CGFloat(b) / 0xFF, alpha: 1.0)
    }
}
