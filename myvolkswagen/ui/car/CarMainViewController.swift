//
//  CarMainViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/19.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
import MapKit
import Kingfisher
import SnapKit

class CarMainViewController: NaviViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MyGarageDelegate, DongleNotificationSheetDelegate {
    
    var imageNews: [ImageNews] = []
    var cars: [Car] = []
    var selectedCar: Car?
    var profile: Profile?
    var dpLinkIndex: Int?
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var contentView: UIView!
    // top info
    @IBOutlet weak var topInfoView: UIView!
    @IBOutlet weak var topUserName: UILabel!
    @IBOutlet weak var carNumberPicker: UIView!
    @IBOutlet weak var carNumberLb: UILabel!
    // welcome card
    @IBOutlet var welcomeCard: UIView!
    @IBOutlet var welcomeAddCarBtn: BasicSizingButton!
    // user view
    @IBOutlet var level0Tag: UIImageView!
    @IBOutlet var userView: UIView!
    @IBOutlet var userViewUserName: UILabel!
    @IBOutlet var userLevelImg: UIImageView!
    @IBOutlet var userLevelCons: NSLayoutConstraint!
    @IBOutlet var userViewBG: UIImageView!
    // car card
    var timer: Timer?
    var textIndex = 0
    var notiList: [Notificationn] = []
    var notiDialogList: [Notificationn] = []
    var dongleSummary: DongleSummary?
    @IBOutlet var carCard: UIView!
    @IBOutlet var carImg: UIImageView!
    @IBOutlet var carCardHeightCons: BasicConstraint!
    @IBOutlet var assisCons: NSLayoutConstraint!
    @IBOutlet var assistanceView: UIView!
    @IBOutlet var assistantInfoView: UIView!
    @IBOutlet var assistRemindView: UIView!
    @IBOutlet var assistLoadingImg: UIImageView!
    @IBOutlet var totalMiles: BasicLabel!
    @IBOutlet var gasPumpLb: BasicLabel!
    @IBOutlet var batteryLb: BasicLabel!
    @IBOutlet var carStateView: UIView!
    @IBOutlet var carStateLoadingImg: UIImageView!
    @IBOutlet var carStateLb: UILabel!
    @IBOutlet var carStateUpdateTime: UILabel!
    @IBOutlet var carStateRoundView: UIView!
    // maintainence
    lazy var pageControll: VWPageControl = {
        let pc                   = VWPageControl(numberOfPages: self.notiList.count, currentPage: 0, isCircular: true)
        pc.currentIndicatorColor = .black
        return pc
    }()
    var maintainenceInfo: [MaintainanceInfo] = []
    @IBOutlet var maintainenceView: UIView!
    @IBOutlet var maintenanceLoadingImg: UIImageView!
    @IBOutlet var maintainenceCollectionView: UICollectionView!
    @IBOutlet var maintainencePageControlCons: NSLayoutConstraint!
    @IBOutlet var pagecontrolHeight: NSLayoutConstraint!
    @IBOutlet var pageControlView: UIView!
    // news
    @IBOutlet var newsView: UIView!
    @IBOutlet var newsCollectionVIew: UICollectionView!
    // car info
    @IBOutlet var carInfoView: UIView!
    @IBOutlet var carInfoWhiteBG: UIView!
    @IBOutlet var carTypeLb: UILabel!
    @IBOutlet var carModelLb: UILabel!
    @IBOutlet var carNumLb: UILabel!
    @IBOutlet var carDateLb: UILabel!
    @IBOutlet var carInsureLb: UILabel!
    @IBOutlet var carInsureLoadingImg: UIImageView!
    @IBOutlet var carDealerLb: UILabel!
    // contact
    @IBOutlet weak var contactUsBtn: VWButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        naviBar.setTitle("".localized)
        naviBar.setRightNotiButton()
        
        let gradientVieww = GradientBackgroundView()
        gradientVieww.frame = view.bounds
        gradientView.addSubview(gradientVieww)
        
        let ges = UITapGestureRecognizer(target: self, action: #selector(tapCarImg))
        self.carImg.addGestureRecognizer(ges)
        self.carImg.isUserInteractionEnabled = true
        
        newsCollectionVIew.register(UINib(nibName: "NewsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "newscell")
        maintainenceCollectionView.register(UINib(nibName: "CarMaintanceCell", bundle: nil), forCellWithReuseIdentifier: "CarMaintanceCell")
        
        //setupMapView()
        setupUI()
        
        // Data
        NoticeManager.shared().getNotice(kind: 3, historical: false, fromRemote: true, listener: self)
        imageNews = NewsManager.shared().getAllNews(listener: self)
        self.cars = CarManager.shared().getCars(remotely: true, listener: self)
        
        if let myId = UserManager.shared().currentUser.accountId {
            profile = UserManager.shared().getProfile(withId: myId, listener: self)
            self.topUserName.text = "Hi, \(profile?.nickname ?? "")"
            self.userViewUserName.text = "Hi, \(profile?.nickname ?? "")"
            self.setupUserLevel()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadCars), name: NSNotification.Name(rawValue: CarManager.nameCarReloadNotification), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("getUnreadCount")
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: false, listener: nil) {
            if readStatus == .read {
                print("getUnreadCount: read")
                naviBar.btnNoti.setImage(UIImage(named: "noti_btn_read"), for: .normal)
            } else {
                print("getUnreadCount: unread")
                naviBar.btnNoti.setImage(UIImage(named: "noti_btn_unread"), for: .normal)
            }
        }
        
        if self.cars.count != CarManager.shared().cars.count {
            reloadCars()
        }
        else if selectedCar != nil {
            if selectedCar?.licensePlateNumber != CarManager.shared().getSelectedCarNumber() {
                reloadCars()
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(updateUnreadCount), name: NSNotification.Name(rawValue: NoticeManager.nameUnreadCountNotification), object: nil)
        
        Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "pageview", InboxId: "", EventCategory: "pageview", PageName: "pv_home", EventLabel: "", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NoticeManager.nameUnreadCountNotification), object: nil)
    }
    
    override func updateUnreadCount(notification: NSNotification) {
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            if readStatus == .read {
                naviBar.btnNoti.setImage(UIImage(named: "noti_btn_read"), for: .normal)
            } else {
                naviBar.btnNoti.setImage(UIImage(named: "noti_btn_unread"), for: .normal)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if dpLinkIndex == 1 { // addcar
            showAddCar()
        }
        dpLinkIndex = nil
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func startTextRotation() {
        self.textIndex = 0 // reset
        
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { [weak self] _ in
            self?.textIndex += 1
            if self?.textIndex == self?.notiList.count {
                self?.textIndex = 0
            }
            self?.updateLabelWithText()
            self?.pagingMaintainenceView()
        }
    }
    
    deinit {
        timer?.invalidate()
        timer = nil
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CarManager.nameCarReloadNotification), object: nil)
    }
    
    func pagingMaintainenceView() {
        if self.maintainenceInfo.count > 1 {
            let visibleIndexPaths = maintainenceCollectionView.indexPathsForVisibleItems
            let currentIndexPath = visibleIndexPaths.first
            var nextIndexPath: IndexPath
            if currentIndexPath != nil {
                if currentIndexPath!.item < self.maintainenceInfo.count - 1 {
                    nextIndexPath = IndexPath(item: currentIndexPath!.item + 1, section: 0)
                } else {
                    nextIndexPath = IndexPath(item: 0, section: 0)
                }
                
                maintainenceCollectionView.scrollToItem(at: nextIndexPath, at: .centeredHorizontally, animated: true)
            }
        }
    }
    
    func updateLabelWithText() {
        if notiList.count > 0 {
            let timeDifference = Utility.timeDifferenceString(from: self.notiList[self.textIndex].eventTime ?? "")
            
            UIView.transition(with: carStateLb, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.carStateLb.text = self.notiList[self.textIndex].descriptionn
                if timeDifference != nil {
                    self.carStateUpdateTime.text = timeDifference!
                } else {
                    self.carStateUpdateTime.text = ""
                }
            }, completion: nil)
        }
    }
    
    func setupUI() {
        carInfoView.layer.cornerRadius = CGFloat(8).getFitSize()
        carStateRoundView.layer.cornerRadius = CGFloat(8).getFitSize()
        carInfoWhiteBG.layer.cornerRadius = CGFloat(8).getFitSize()
        welcomeAddCarBtn.layer.cornerRadius = CGFloat(5).getFitSize()
        carNumberPicker.layer.cornerRadius = CGFloat(8).getFitSize()
        carNumberPicker.layer.borderColor = UIColor.MyTheme.greyColor300.cgColor
        carNumberPicker.layer.borderWidth = 1
        contactUsBtn.setImage(UIImage(named: "service")?.withRenderingMode(.alwaysTemplate), for: .normal)
        contactUsBtn.imageView?.tintColor = UIColor.white
        assistLoadingImg.loadGif(name: "VW-APP_Loading")
        assistLoadingImg.isHidden = true
        maintenanceLoadingImg.loadGif(name: "VW-APP_Loading")
        maintenanceLoadingImg.isHidden = true
        carStateLoadingImg.loadGif(name: "VW-APP_Loading")
        carStateLoadingImg.isHidden = true
        carInsureLoadingImg.loadGif(name: "VW-APP_Loading")
        carInsureLoadingImg.isHidden = true
        
        removeViews()
    }
    
    func setupUserLevel() {
        self.userLevelImg.isHidden = false
        userLevelCons.isActive = false
        if UserManager.shared().currentUser.level == "1" {
            self.userLevelImg.image = UIImage(named: "user_Blue")
            self.userViewBG.image = UIImage(named: "userview_blue")
        } else if UserManager.shared().currentUser.level == "2" {
            self.userLevelImg.image = UIImage(named: "user_Silver")
            self.userViewBG.image = UIImage(named: "userview_silver")
        } else if UserManager.shared().currentUser.level == "3" {
            self.userLevelImg.image = UIImage(named: "user_Golden")
            self.userViewBG.image = UIImage(named: "userview_gold")
        } else if UserManager.shared().currentUser.level == "4" {
            self.userLevelImg.image = UIImage(named: "user_Platinum")
            self.userViewBG.image = UIImage(named: "userview_platinum")
        } else {
            self.userLevelImg.isHidden = true
            userLevelCons.isActive = true
        }
    }
    
    override func rightButtonNotiClicked(_ button: UIButton) {
        let sb = UIStoryboard(name: "Car", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "NotificationViewControllerV2") as! NotificationViewControllerV2
        vc.imageNewsList = imageNews
        vc.tabSelectedIndex = 0
        tabControllerDelegateV2?.goViewController(view: vc)
        Utility.sendEventLog(ServiceName: "header_menu", UniqueId: "", EventName: "首頁小鈴鐺", InboxId: "", EventCategory: "button", PageName: "header_menu", EventLabel: "首頁小鈴鐺", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
    }
    
    func removeViews() {
        topInfoView.isHidden = true
        level0Tag.isHidden = true
        level0Tag.removeFromSuperview()
        welcomeCard.isHidden = true
        welcomeCard.removeFromSuperview()
        userView.removeFromSuperview()
        carCard.removeFromSuperview()
        assistanceView.isHidden = true
        carStateView.isHidden = true
        maintainenceView.removeFromSuperview()
        newsView.removeFromSuperview()
        carInfoView.removeFromSuperview()
    }
    
    @objc func reloadCars() {
        self.cars = CarManager.shared().getCars(remotely: true, listener: self)
    }
    
    func setupSelectedCar() {
        if let ci = CarManager.shared().getSelectedCarNumber() {
            for car in self.cars {
                if car.licensePlateNumber == ci {
                    self.selectedCar = car
                    // 選過車
                }
            }
            if self.selectedCar == nil {
                if self.cars.count > 0 { // 選過的車不見了 設定第一台車為預設
                    self.selectedCar = CarManager.shared().cars[0]
                    CarManager.shared().saveSelectedCarNumer(with: CarManager.shared().cars[0].licensePlateNumber!)
                }
            }
            if let urll = URL(string: self.selectedCar?.carModelImage ?? "") {
                self.carImg.kf.setImage(with: urll, placeholder: nil)
            }
            self.carNumberLb.text = self.selectedCar?.licensePlateNumber
            self.carTypeLb.text = self.selectedCar?.carTypeName
            self.carModelLb.text = self.selectedCar?.carModelName
            self.carNumLb.text = self.selectedCar?.vin
            self.carDateLb.text = self.selectedCar?.ownerDate == "" ? " " : self.selectedCar?.ownerDate
            self.carDealerLb.text = self.selectedCar?.dealerPlantName
        } else {
            // 沒選過車
            if self.cars.count > 0 {
                self.selectedCar = self.cars[0]
                CarManager.shared().saveSelectedCarNumer(with: self.cars[0].licensePlateNumber!)
                
                if let urll = URL(string: self.selectedCar?.carModelImage ?? "") {
                    self.carImg.kf.setImage(with: urll, placeholder: nil)
                }
                self.carNumberLb.text = self.selectedCar?.licensePlateNumber
                self.carTypeLb.text = self.selectedCar?.carTypeName
                self.carModelLb.text = self.selectedCar?.carModelName
                self.carNumLb.text = self.selectedCar?.vin
                self.carDateLb.text = self.selectedCar?.ownerDate == "" ? " " : self.selectedCar?.ownerDate
                self.carDealerLb.text = self.selectedCar?.dealerPlantName
            } else {
                // 沒車
            }
        }
    }
    func updateUI(dongleState: CarDoungleState?) {
        removeViews()
        
        if self.cars.count == 0 {
            topInfoView.isHidden = false
            topUserName.isHidden = false
            carNumberPicker.isHidden = true
            
            
            if UserManager.shared().currentUser.level == "4" {
                level0Tag.isHidden = false
                self.contentView.addSubview(level0Tag)
                level0Tag.snp.makeConstraints { make in
                    make.leading.equalToSuperview().offset(24)
                    make.top.equalToSuperview()
                }
                
                welcomeCard.isHidden = false
                self.contentView.addSubview(welcomeCard)
                welcomeCard.snp.makeConstraints { make in
                    make.top.equalTo(level0Tag.snp.bottom).offset(16)
                    make.leading.equalToSuperview().offset(24)
                    make.trailing.equalToSuperview().offset(-24)
                }
                self.level0Tag.image = UIImage(named: "user_Platinum")
            } else {
                welcomeCard.isHidden = false
                self.contentView.addSubview(welcomeCard)
                welcomeCard.snp.makeConstraints { make in
                    make.leading.top.equalToSuperview().offset(24)
                    make.trailing.equalToSuperview().offset(-24)
                }
            }
            
            self.contentView.addSubview(newsView)
            newsView.snp.makeConstraints { make in
                make.leading.trailing.equalToSuperview()
                make.top.equalTo(self.welcomeCard.snp.bottom).offset(24)
            }
            contactUsBtn.snp.makeConstraints { make in
                make.top.equalTo(self.newsView.snp.bottom).offset(24)
            }
            
            //                CarManager.shared().addCarFromMainPage = true
            //                navigationController?.popViewController(animated: true)
        } else {
            if dongleState == .USER_MAPPED { // 已綁定,有行車助理
                UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                    self.topInfoView.isHidden = false
                    self.topUserName.isHidden = true
                    self.carNumberPicker.isHidden = false
                    self.contentView.addSubview(self.userView)
                    self.userView.snp.makeConstraints { make in
                        make.top.equalToSuperview().offset(8)
                        make.leading.equalToSuperview().offset(24)
                        make.trailing.equalToSuperview().offset(-24)
                    }
                    self.contentView.addSubview(self.carCard)
                    self.carCard.snp.makeConstraints { make in
                        make.leading.equalToSuperview().offset(24)
                        make.trailing.equalToSuperview().offset(-24)
                        make.top.equalTo(self.userView.snp.bottom).offset(16)
                    }
                    // dongle summary
//                    if self.dongleSummary != nil {
                        self.assistanceView.isHidden = false
                        self.assistantInfoView.isHidden = false
                        self.assistRemindView.isHidden = true
                        self.carCardHeightCons.constant = CGFloat(216)
//                    } else {
//                        self.assistanceView.isHidden = true
//                        self.carCardHeightCons.constant = CGFloat(182)
//                    }
                    // dongle notification
//                    if self.notiList.count > 0 {
                        self.carStateView.isHidden = false
                        self.assisCons.isActive = true
//                    } else {
//                        self.carStateView.isHidden = true
//                        self.assisCons.isActive = false
//                    }
                    // 保養維修資訊
//                    if self.maintainenceInfo.count > 0 {
                        self.contentView.addSubview(self.maintainenceView)
                        self.maintainenceView.snp.makeConstraints { make in
                            make.leading.equalToSuperview()
                            make.trailing.equalToSuperview()
                            make.top.equalTo(self.carCard.snp.bottom).offset(16)
                        }
//                    }
                    // 最新消息
                    self.contentView.addSubview(self.newsView)
                    self.newsView.snp.makeConstraints { make in
                        make.leading.trailing.equalToSuperview()
                        make.top.equalTo(self.maintainenceView.snp.bottom).offset(24)
                    }
                    // 車輛資訊
                    self.contentView.addSubview(self.carInfoView)
                    self.carInfoView.snp.makeConstraints { make in
                        make.leading.trailing.equalToSuperview()
                        make.top.equalTo(self.newsView.snp.bottom).offset(24)
                    }
                    self.contactUsBtn.snp.makeConstraints { make in
                        make.top.equalTo(self.carInfoView.snp.bottom).offset(24)
                    }
                }, completion: nil)
            } else {
                if dongleState == .VIN_MAPPED { // 未綁定,顯示可綁定的資訊
                    if UserManager.shared().getUserCarAssistantReminder() == true {
                        self.assistanceView.isHidden = true
                        self.carCardHeightCons.constant = CGFloat(182).getFitSize()
                    } else {
                        self.assistanceView.isHidden = false
                        self.assistantInfoView.isHidden = true
                        self.assistRemindView.isHidden = false
                        self.carCardHeightCons.constant = CGFloat(216).getFitSize()
                    }
                } else { // 未安裝行車助理
                    self.assistanceView.isHidden = true
                    self.carCardHeightCons.constant = CGFloat(182).getFitSize()
                }
                topInfoView.isHidden = false
                topUserName.isHidden = true
                carNumberPicker.isHidden = false
                self.carStateView.isHidden = true
                self.assisCons.isActive = false
                self.contentView.addSubview(userView)
                userView.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(8)
                    make.leading.equalToSuperview().offset(24)
                    make.trailing.equalToSuperview().offset(-24)
                }
                self.contentView.addSubview(carCard)
                carCard.snp.makeConstraints { make in
                    make.leading.equalToSuperview().offset(24)
                    make.trailing.equalToSuperview().offset(-24)
                    make.top.equalTo(self.userView.snp.bottom).offset(16)
                }
                // 保養維修資訊
                self.contentView.addSubview(maintainenceView)
                maintainenceView.snp.makeConstraints { make in
                    make.leading.equalToSuperview()
                    make.trailing.equalToSuperview()
                    make.top.equalTo(self.carCard.snp.bottom).offset(16)
                }
                // 最新消息
                self.contentView.addSubview(newsView)
                newsView.snp.makeConstraints { make in
                    make.leading.trailing.equalToSuperview()
                    make.top.equalTo(self.maintainenceView.snp.bottom).offset(24)
                }
                self.contentView.addSubview(carInfoView)
                carInfoView.snp.makeConstraints { make in
                    make.leading.trailing.equalToSuperview()
                    make.top.equalTo(self.newsView.snp.bottom).offset(24)
                }
                contactUsBtn.snp.makeConstraints { make in
                    make.top.equalTo(self.carInfoView.snp.bottom).offset(24)
                }
            }
        }
        
        if let myId = UserManager.shared().currentUser.accountId {
            profile = UserManager.shared().getProfile(withId: myId, listener: self)
            self.topUserName.text = "Hi, \(profile?.nickname ?? "")"
            self.userViewUserName.text = "Hi, \(profile?.nickname ?? "")"
            self.setupUserLevel()
        }
    }
    
    func showDongleNotiSheet() {
        let sb = UIStoryboard(name: "Car", bundle: nil)
        let bottomSheetVC = sb.instantiateViewController(withIdentifier: "DongleNotificationSheetViewController") as! DongleNotificationSheetViewController
        bottomSheetVC.delegate = self
        bottomSheetVC.dongleNoti = self.notiDialogList
        
        let options = SheetOptions(
            pullBarHeight: 8,
            presentingViewCornerRadius: 20,
            shouldExtendBackground: true,
            setIntrinsicHeightOnNavigationControllers: true,
            useFullScreenMode: true,
            shrinkPresentingViewController: true,
            useInlineMode: false,
            horizontalPadding: 0,
            maxWidth: nil
        )
        
        let sheetController = SheetViewController(
            controller: bottomSheetVC,
            sizes: [.fixed(440)])
        
        sheetController.cornerRadius = 20
        sheetController.minimumSpaceAbovePullBar = 0
        sheetController.treatPullBarAsClear = false
        sheetController.dismissOnOverlayTap = true
        sheetController.dismissOnPull = true
        
        sheetController.shouldDismiss = { _ in
            return true
        }
        sheetController.didDismiss = { _ in
        }
        self.present(sheetController, animated: true, completion: nil)
    }
    
    override func didStartGetNotice(historical: Bool) {
        //printCurrentTime(str: "didStartGetNotice")
    }
    
    override func didFinishGetNotice(success: Bool, historical: Bool, notices: [Notice]?) {
        //printCurrentTime(str: "didFinishGetNotice")
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            if readStatus == .read {
                naviBar.btnNoti.setImage(UIImage(named: "noti_btn_read"), for: .normal)
            } else {
                naviBar.btnNoti.setImage(UIImage(named: "noti_btn_unread"), for: .normal)
            }
        }
    }
    
    func showAddCar() {
        let sb = UIStoryboard(name: "Login", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "AddCarNavi") as! UINavigationController
        self.navigationController?.present(vc, animated: true)
    }
    
    // MARK: ScrollView Delegate
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView == maintainenceCollectionView {
            guard let visible = self.maintainenceCollectionView.visibleCells.first else { return }
            guard let index = self.maintainenceCollectionView.indexPath(for: visible)?.row else { return }
            pageControll.currentpage = index
        }

    }
    
    // MARK: Btns
    
    @IBAction func welcomeAddBtnPressed(_ sender: Any) {
        //self.tabControllerDelegateV2?.addCar()
        showAddCar()
    }
    
    @IBAction func showMoreNewsBtnPressed(_ sender: Any) {
        // 最新消息查看更多
        Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "最新消息查看更多", InboxId: "", EventCategory: "button", PageName: "pv_home", EventLabel: "最新消息查看更多", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        let sb = UIStoryboard(name: "Car", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "NotificationViewControllerV2") as! NotificationViewControllerV2
        vc.imageNewsList = imageNews
        vc.tabSelectedIndex = 1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func carInsureBtnPressed(_ sender: Any) {
        Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "愛車保固", InboxId: "", EventCategory: "button", PageName: "pv_home", EventLabel: "愛車保固", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
#if DEVELOPMENT
        let urll = URL(string: "myvolkswagendev://maintenance/warranty")!
        if Deeplinker.handleDeeplink(url: urll, ref: nil) {
            Deeplinker.checkDeepLink()
        }
#else
        let urll = URL(string: "myvolkswagen://maintenance/warranty")!
        if Deeplinker.handleDeeplink(url: urll, ref: nil) {
            Deeplinker.checkDeepLink()
        }
#endif
    }
    
    @IBAction func carPriceBtnPressed(_ sender: Any) {
        Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "立即鑑價", InboxId: "", EventCategory: "button", PageName: "pv_home", EventLabel: "立即鑑價", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.loadUrl
        vc.mytitle = "cpo_car".localized
        vc.loadUrl = URL(string: "\(Config.urlCpoCar)?\(getDeeplinkParams())")!
        //self.navigationController?.pushViewController(vc, animated: true)
        tabControllerDelegateV2?.goViewController(view: vc)
    }
    
    @IBAction func carDealerBtnPressed(_ sender: Any) {
        Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "鄰近服務中心", InboxId: "", EventCategory: "button", PageName: "pv_home", EventLabel: "鄰近服務中心", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        vc.index = WebsiteIndex.nearbyServiceCenter
        //self.navigationController?.pushViewController(vc, animated: true)
        tabControllerDelegateV2?.goViewController(view: vc)
    }
    
    @IBAction func carDealerInfoBtnPressed(_ sender: Any) {
        Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "我的服務中心介紹", InboxId: "", EventCategory: "button", PageName: "pv_home", EventLabel: "我的服務中心介紹", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        let sb = UIStoryboard(name: "Car", bundle: nil)
        let bottomSheetVC = sb.instantiateViewController(withIdentifier: "MyDealerInfoViewController") as! MyDealerInfoViewController
        
        let options = SheetOptions(
            pullBarHeight: 8,
            presentingViewCornerRadius: 20,
            shouldExtendBackground: true,
            setIntrinsicHeightOnNavigationControllers: true,
            useFullScreenMode: true,
            shrinkPresentingViewController: true,
            useInlineMode: false,
            horizontalPadding: 0,
            maxWidth: nil
        )
        
        let lbHeight = "「我的服務中心」讓您能夠選擇服務中心，掌握第一手服務內容與優惠訊息！\n \n・若您為曾經回服務中心保養過的福斯車主，則此欄位會自動設定為您最近一次造訪的服務中心。您也可以自行選取偏好的服務中心，此欄位將以您自行選擇的服務中心為主。\n \n您為尚未回服務中心過的車主或新車主，您可以先選取偏好的服務中心，若您未選取福斯系統會在您首次回服務中心保養後，自動設定為您最近一次造訪的服務中心。\n \n您還不是福斯車主，則可以忽略此欄位。\n \n・系統於每日早上 8 點更新回服務中心紀錄，此欄位可能非最即時的資料。 若您持有兩台以上的福斯車輛，則會以您第一台綁定的車輛資料為主。".getLabelStringHeightFrom(labelWidth: UiUtility.getScreenWidth() - 48, font: UIFont(name: "VWHead", size: CGFloat(15))!)
        
        let sheetController = SheetViewController(
            controller: bottomSheetVC,
            sizes: [.fixed(48+24+40+40+40 + lbHeight + CGFloat(60).getFitSize())])
        
        sheetController.cornerRadius = 20
        sheetController.minimumSpaceAbovePullBar = 0
        sheetController.treatPullBarAsClear = false
        sheetController.dismissOnOverlayTap = true
        sheetController.dismissOnPull = true
        
        sheetController.shouldDismiss = { _ in
            return true
        }
        sheetController.didDismiss = { _ in
        }
        self.present(sheetController, animated: true, completion: nil)
    }
    
    @IBAction func carPickerBtnPressed(_ sender: Any) {
        Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "我的車庫", InboxId: "", EventCategory: "button", PageName: "pv_home", EventLabel: "我的車庫", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        let vc = UIStoryboard(name: "Car", bundle: nil).instantiateViewController(withIdentifier: "MyGarageViewController") as! MyGarageViewController
        vc.cars = cars
        vc.delegate = self
        tabControllerDelegateV2?.goViewController(view: vc)
    }
    
    @IBAction func assistantNomoreRemindBtnPressed(_ sender: Any) {
        assistRemindView.isHidden = true
        self.assistanceView.isHidden = true
        self.carCardHeightCons.constant = CGFloat(182)
        UserManager.shared().saveUserCarAssistantReminder()
    }
    
    @IBAction func contactUsBtnPressed(_ sender: Any) {
        Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "聯絡我們", InboxId: "", EventCategory: "button", PageName: "pv_home", EventLabel: "聯絡我們", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
        let sb = UIStoryboard(name: "Car", bundle: nil)
        let bottomSheetVC = sb.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        
        let options = SheetOptions(
            pullBarHeight: 8,
            presentingViewCornerRadius: 20,
            shouldExtendBackground: true,
            setIntrinsicHeightOnNavigationControllers: true,
            useFullScreenMode: true,
            shrinkPresentingViewController: true,
            useInlineMode: false,
            horizontalPadding: 0,
            maxWidth: nil
        )
        
        let sheetController = SheetViewController(
            controller: bottomSheetVC,
            sizes: [.fixed(210 + CGFloat(180+50.5).getFitSize())])
        
        sheetController.cornerRadius = 20
        sheetController.minimumSpaceAbovePullBar = 0
        sheetController.treatPullBarAsClear = false
        sheetController.dismissOnOverlayTap = true
        sheetController.dismissOnPull = true
        
        sheetController.shouldDismiss = { _ in
            return true
        }
        sheetController.didDismiss = { _ in
        }
        self.present(sheetController, animated: true, completion: nil)
    }
    
    @objc func tapCarImg() {
        DeeplinkNavigator.shared.proceedToDeeplink(.DrivingAssistant, param: nil)
        Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "查看車圖", InboxId: "", EventCategory: "button", PageName: "pv_home", EventLabel: "查看車圖", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
    }
    
    // MARK: MyGarageDelegate
    func didSelectCarFromGarage() {
        self.cars = CarManager.shared().cars
        setupSelectedCar()
        UserManager.shared().loginCruisys(listener: self, accountId: UserManager.shared().currentUser.accountId!, plateNum: CarManager.shared().getSelectedCarNumber()!)
    }
    
    // MARK: DongleNotificationSheetDelegate
    func didPressDongleNotificationSheetBottomBtn(ind: Int) {
        let noti = self.notiDialogList[ind]
//        if let nb = noti.btnUrl {
//            let urll = URL(string: nb)!
//            if Deeplinker.handleDeeplink(url: urll, ref: nil) {
//                Deeplinker.checkDeepLink()
//            }
//        }
        DeeplinkNavigator.shared.proceedToDeeplink(.DrivingAssistant, param: nil)
    }
    
    // MARK: CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == newsCollectionVIew {
            if imageNews.count > 6 {
                return 6
            }
            return imageNews.count
        } else {
            return maintainenceInfo.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == newsCollectionVIew {
            let news = imageNews[indexPath.item]
            let cell = newsCollectionVIew.dequeueReusableCell(withReuseIdentifier: "newscell", for: indexPath) as! NewsCollectionViewCell
            if let urll = URL(string: news.thumbCoverImage ?? "") {
                cell.imageview.kf.setImage(with: urll, placeholder: nil)
            } else {
                cell.imageview.image = nil
            }
            let originalDateString = news.publishedAt ?? ""
            if let formattedDateString = formatDateString(originalDateString) {
                cell.dateLb.text = formattedDateString
            } else {
                cell.dateLb.text = news.publishedAt ?? ""
            }
            cell.titleLb.text = news.title ?? ""
            cell.layer.cornerRadius = CGFloat(8).getFitSize()
            return cell
        } else {
            let maint = maintainenceInfo[indexPath.item]
            let cell = maintainenceCollectionView.dequeueReusableCell(withReuseIdentifier: "CarMaintanceCell", for: indexPath) as! CarMaintanceCell
            cell.desLb.text = maint.descriptionn
            cell.moreInfoBtn.setTitle(maint.btnName, for: .normal)
            if maint.type == "notification" {
                cell.setWarningBorder(warning: false)
            } else {
                cell.setWarningBorder(warning: true)
            }
            return cell
        }
    }
    
    func formatDateString(_ dateString: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = "yyyy/MM/dd"
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == newsCollectionVIew {
            return CGSize(width: 240/215 * newsCollectionVIew.frame.size.height, height: newsCollectionVIew.frame.size.height)
        } else {
            return CGSize(width: UiUtility.getScreenWidth(), height: maintainenceCollectionView.frame.size.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == newsCollectionVIew {
            return UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
        } else {
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == newsCollectionVIew {
            let news = imageNews[indexPath.item]
            // 最新消息詳細
            if news.id != nil {
                Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "最新消息詳細_\(news.id!)", InboxId: "", EventCategory: "button", PageName: "pv_home", EventLabel: "最新消息詳細_\(news.id!)", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
            }
            if news.isLink != nil && news.isLink == true {
                if news.link != nil {
                    if let urll = URL(string: news.link!) {
                        UIApplication.shared.open(urll)
                    }
                }
            } else {
                let sb = UIStoryboard(name: "Car", bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "NotificationDetailViewController") as! NotificationDetailViewController
                vc.imageNews = news
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            let maint = maintainenceInfo[indexPath.item]
            if maint.serviceName != nil && maint.serviceName != "" && maint.eventName != nil {
                if maint.eventJson != nil {
                    Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "\(maint.eventName!)", InboxId: "", EventCategory: "button", PageName: "pv_home", EventLabel: "\(maint.eventName!)", eventJson: maint.eventJson!, utmSource: nil, utmMedium: nil, utmCampaign: nil)
                } else {
                    Utility.sendEventLog(ServiceName: "pv_home", UniqueId: "", EventName: "\(maint.eventName!)", InboxId: "", EventCategory: "button", PageName: "pv_home", EventLabel: "\(maint.eventName!)", eventJson: nil, utmSource: nil, utmMedium: nil, utmCampaign: nil)
                }
            }
            if let btnurl = maint.btnUrl {
                if let urll = URL(string: btnurl) {
                    if Deeplinker.handleDeeplink(url: urll, ref: nil) {
                        print("didSelectItemAt start deeplink: \(urll.absoluteString)")
                        Deeplinker.checkDeepLink()
                    } else {
                        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
                        vc.index = .loadUrl
                        vc.loadUrl = urll
                        self.tabControllerDelegateV2?.goViewController(view: vc)
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == newsCollectionVIew {
            return 10
        }
        return 0
    }
    
    override func didStartGetImageNews() {
        //printCurrentTime(str: "didStartGetNews")
    }
    
    override func didFinishGetImageNews(success: Bool, news: [ImageNews]?) {
        //printCurrentTime(str: "didFinishGetNews")
        imageNews = news ?? []
        newsCollectionVIew.reloadData()
    }
    
    // MARK: user protocol
    override func didStartLoginCruisys() {
        //printCurrentTime(str: "didStartGetCruisysToken")
        showHud()
    }
    
    override func didFinishLoginCruisys(success: Bool) {
        //printCurrentTime(str: "didFinishGetCruisysToken")
        if success {
            CarManager.shared().getDongleBindingInfo(listener: self, plateNo: CarManager.shared().getSelectedCarNumber()!)
        }
        else {
            showApiFailureAlert(message: "發生錯誤，重試中")
        }
    }
    
    // MARK: car protocol
    
    func printCurrentTime(str: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss.SSS"

        // 獲取現在的日期和時間，包含毫秒
        let currentDate = Date()

        // 使用 DateFormatter 格式化日期和時間，包含毫秒
        let formattedDate = dateFormatter.string(from: currentDate)

        // 印出格式化後的日期和時間，包含毫秒
        print("\(str) - Current Time：\(formattedDate)")
    }
    override func didStartGetDongleBinding() {
        //printCurrentTime(str: "didStartGetDongleBinding")
    }
    
    override func didFinishGetDongleBinding(state: CarDoungleState?, dongleData: DongleData?) {
        //printCurrentTime(str: "didFinishGetDongleBinding")
        if dongleData?.redirectUrl != nil && dongleData?.redirectUrl != "" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CarManager.nameCarAssistantReloadNotification), object: nil, userInfo: nil)
            CarManager.shared().assistantRedirectUrl = dongleData?.redirectUrl!
        }
        
        guard let selectedCarNum = CarManager.shared().getSelectedCarNumber() else {
            return
        }
        
        if state == .USER_MAPPED {
            CarManager.shared().getCarMaintanceInfo(listener: self, plateNo: selectedCarNum)
            CarManager.shared().getDongleSummary(listener: self, plateNo: selectedCarNum)
            CarManager.shared().getDongleNotification(listener: self, plateNo: selectedCarNum)
            CarManager.shared().getCarWarranty(listener: self, plateNo: selectedCarNum)
        } else {
            CarManager.shared().getDongleSummary(listener: self, plateNo: selectedCarNum)
            CarManager.shared().getDongleNotification(listener: self, plateNo: selectedCarNum)
            CarManager.shared().getCarMaintanceInfo(listener: self, plateNo: selectedCarNum)
            CarManager.shared().getCarWarranty(listener: self, plateNo: selectedCarNum)
        }
        
        canStartDplk = true
        DeeplinkNavigator.shared.canStartDeeplink()
        
        self.updateUI(dongleState: state)
        self.dismissHud()
    }
    
    override func didStartGetCar() {
        showHud()
        //printCurrentTime(str: "didStartGetCar")
    }
    
    override func didFinishGetCar(success: Bool, cars: [Car]) {
        //printCurrentTime(str: "didFinishGetCar")
        dismissHud()
        
        if success {
            self.cars = cars
            if self.cars.count == 0 {
                CarManager.shared().clearSelectedCarNumber()
                updateUI(dongleState: nil)
                canStartDplk = true
                DeeplinkNavigator.shared.canStartDeeplink()
            } else {
                setupSelectedCar()
                UserManager.shared().loginCruisys(listener: self, accountId: UserManager.shared().currentUser.accountId!, plateNum: CarManager.shared().getSelectedCarNumber()!)
            }
        }
    }
    
    override func didStartGetDongleSummary() {
        assistLoadingImg.isHidden = false
        //printCurrentTime(str: "didStartGetDongleSummary")
    }
    
    override func didFinishGetDongleSummary(data: DongleSummary?) {
        assistLoadingImg.isHidden = true
        //printCurrentTime(str: "didFinishGetDongleSummary")
        self.dongleSummary = data
        let intValue = data?.odometer?.value ?? 0
        let formattedString = formatNumberWithThousandsSeparator(intValue)
        totalMiles.text = "總里程 \(formattedString)"
        gasPumpLb.text = "\(data?.fuelLevel?.value ?? 0)"
        batteryLb.text = "\(data?.controlModuleVoltage?.value ?? 0)"
    }
    
    func formatNumberWithThousandsSeparator(_ number: Int) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter.string(from: NSNumber(value: number)) ?? ""
    }
    
    override func didStartGetDongleNotification() {
        //printCurrentTime(str: "didStartGetDongleNotification")
        carStateLoadingImg.isHidden = false
    }
    
    override func didFinishGetDongleNotification(success: Bool, notificationList: [Notificationn]?, notificationDialogList: [Notificationn]?) {
        carStateLoadingImg.isHidden = true
        //printCurrentTime(str: "didFinishGetDongleNotification")
        if success {
            if notificationList != nil {
                self.notiList = notificationList!
            }
            
            if self.notiList.count > 0 {
                self.carStateView.isHidden = false
                self.assisCons.isActive = true
            } else {
                self.carStateView.isHidden = true
                self.assisCons.isActive = false
            }
            
            if notificationDialogList != nil && notificationDialogList!.count > 0 {
                self.notiDialogList = notificationDialogList!
                
                if let savedTime = CarManager.shared().getDongleNotiTimeEvent() {
                    if let dialogEventTime = notificationDialogList![0].eventTime {
                        if let result = Utility.compareDates(dateStringA: dialogEventTime, dateStringB: savedTime) {
                            if result == .orderedDescending {
                                // 日期A > 日期B ---> 有新通知，跳通知
                                showDongleNotiSheet()
                                CarManager.shared().saveDongleNotiTimeEvent(with: dialogEventTime)
                            } else if result == .orderedAscending {
                                // 日期A < 日期B
                            } else {
                                // 日期A = 日期B
                            }
                        } else {
                            // 日期格式无效
                        }
                    }
                } else {
                    // 沒顯示過
                    showDongleNotiSheet()
                    if notificationDialogList![0].eventTime != nil {
                        CarManager.shared().saveDongleNotiTimeEvent(with: notificationDialogList![0].eventTime!)
                    }
                }
            }
            startTextRotation()
        }
    }
    
    override func didStartGetCarWarranty() {
        carInsureLoadingImg.isHidden = false
        //printCurrentTime(str: "didStartGetCarWarranty")
    }
    
    override func didFinishGetCarWarranty(success: Bool, date: String?) {
        carInsureLoadingImg.isHidden = true
        //printCurrentTime(str: "didFinishGetCarWarranty")
        if success {
            carInsureLb.text = date
        } else {
            carInsureLb.text = ""
        }
    }
    
    override func didStartGetCarMaintanceInfo() {
        maintenanceLoadingImg.isHidden = false
        //printCurrentTime(str: "didStartGetCarMaintanceInfo")
    }
    
    override func didFinishGetCarMaintanceInfo(success: Bool, info: [MaintainanceInfo]?) {
        maintenanceLoadingImg.isHidden = true
        //printCurrentTime(str: "didFinishGetCarMaintanceInfo")
        if success {
            self.maintainenceInfo = info ?? []
        } else {
            self.maintainenceInfo = []
        }
        self.maintainenceCollectionView.reloadData()
        
        if self.maintainenceInfo.count > 1 {
            pagecontrolHeight.constant = 16
            for vv in self.pageControlView.subviews {
                vv.removeFromSuperview()
            }
            let long: CGFloat = 16
            let short: CGFloat = (CGFloat((self.maintainenceInfo.count - 1)) * CGFloat(8).getFitSize())
            let spacing: CGFloat = CGFloat((self.maintainenceInfo.count - 1)) * 4
            let mywidth = long + short + spacing + 10
            // 長 + 短 + spacing
            pageControll = VWPageControl(numberOfPages: self.maintainenceInfo.count, currentPage: 0, isCircular: true)
            pageControll.backgroundColor = UIColor.clear
            
            // 添加到视图
            pageControlView.addSubview(pageControll)
            pageControll.snp.makeConstraints { make in
                make.width.equalTo(mywidth)
                make.height.equalTo(CGFloat(4).getFitSize())
                make.centerX.centerY.equalToSuperview()
            }
        } else {
            pagecontrolHeight.constant = 0
            pageControll.removeFromSuperview()
        }
        startTextRotation()
    }
    
    // MARK: profile protocol
    
    override func didStartGetProfile() {
        //printCurrentTime(str: "didStartGetProfile")
    }
    
    override func didFinishGetProfile(success: Bool, profile: Profile?) {
        //printCurrentTime(str: "didFinishGetProfile")
        if success {
            if let p = profile {
                self.profile = p
                self.topUserName.text = "Hi, \(p.nickname ?? "")"
                self.userViewUserName.text = "Hi, \(p.nickname ?? "")"
                if self.cars.count > 0 {
                    self.setupUserLevel()
                }
            }
        }
    }
}
