//
//  CarMaintanceCell.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/13.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

class CarMaintanceCell: UICollectionViewCell {

    @IBOutlet weak var borderBG: UIView!
    @IBOutlet weak var desLb: UILabel!
    @IBOutlet weak var moreInfoBtn: UIButton!
    @IBOutlet var warningCons: NSLayoutConstraint!
    @IBOutlet weak var warningImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.borderBG.layer.cornerRadius = CGFloat(8).getFitSize()
        self.borderBG.clipsToBounds = true
        
        setWarningBorder(warning: true)
    }

    func setWarningBorder(warning: Bool) {
        if warning {
            self.warningCons.isActive = true
            self.warningImg.isHidden = false
            self.borderBG.layer.borderColor = UIColor.MyTheme.errorColor000.withAlphaComponent(0.16).cgColor
            self.borderBG.layer.borderWidth = 1
        } else {
            self.warningCons.isActive = false
            self.warningImg.isHidden = true
            self.borderBG.layer.borderWidth = 0
        }
    }
}
