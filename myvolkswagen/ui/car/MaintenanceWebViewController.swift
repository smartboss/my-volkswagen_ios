//
//  MaintenanceWebViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/30.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
import WebKit
import SafariServices
import SwiftyJSON
import Alamofire

class MaintenanceWebViewController: NaviViewController, WKNavigationDelegate, WKUIDelegate, UIScrollViewDelegate {

    @IBOutlet weak var topInfoView: UIView!
    @IBOutlet weak var topUserName: UILabel!
    @IBOutlet weak var carNumberPicker: UIView!
    @IBOutlet weak var carNumberLb: UILabel!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var gradientView: UIView!
    var loadUrl: URL?
    var dpLinkIndex: Int?
    var cars: [Car]?
    var selectedCar: Car?
    var profile: Profile?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        naviBar.setTitle("".localized)
        naviBar.setRightNotiButton()
        
        carNumberPicker.layer.cornerRadius = CGFloat(8).getFitSize()
        carNumberPicker.layer.borderColor = UIColor.MyTheme.greyColor300.cgColor
        carNumberPicker.layer.borderWidth = 1
        
        if let selec = CarManager.shared().getSelectedCarNumber() {
            self.carNumberLb.text = selec
        }
        self.cars = CarManager.shared().getCars(remotely: false, listener: self)
        if self.cars != nil && self.cars!.count > 0 {
            setupSelectedCar()
            self.topUserName.isHidden = true
        } else {
            self.carNumberPicker.isHidden = true
            self.topUserName.isHidden = false
        }
        
        if let myId = UserManager.shared().currentUser.accountId {
            profile = UserManager.shared().getProfile(withId: myId, listener: self)
            self.topUserName.text = "Hi, \(profile?.nickname ?? "")"
        }
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.scrollView.maximumZoomScale = 1.0
        webView.scrollView.minimumZoomScale = 1.0
        webView.scrollView.bouncesZoom = false
        webView.scrollView.pinchGestureRecognizer?.isEnabled = false
        webView.scrollView.delegate = self
        
        let gradientVieww = GradientBackgroundView()
        gradientVieww.frame = view.bounds
        gradientView.addSubview(gradientVieww)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            if readStatus == .read {
                naviBar.btnNoti.setImage(UIImage(named: "noti_btn_read"), for: .normal)
            } else {
                naviBar.btnNoti.setImage(UIImage(named: "noti_btn_unread"), for: .normal)
            }
        }
        
        if self.cars != nil && (self.cars!.count != CarManager.shared().cars.count) {
            reloadCars()
        }
        else if selectedCar != nil {
            if selectedCar?.licensePlateNumber != CarManager.shared().getSelectedCarNumber() {
                reloadCars()
            }
        }
        
        if self.cars != nil && self.cars!.count > 0 {
            self.topUserName.isHidden = true
        } else {
            self.carNumberPicker.isHidden = true
            self.topUserName.isHidden = false
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateUnreadCount), name: NSNotification.Name(rawValue: NoticeManager.nameUnreadCountNotification), object: nil)
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
            scrollView.pinchGestureRecognizer?.isEnabled = false
        }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NoticeManager.nameUnreadCountNotification), object: nil)
    }
    
    override func updateUnreadCount(notification: NSNotification) {
//        Logger.d("car updateUnreadCount")
        // update notice read/unread status locally
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            if readStatus == .read {
                naviBar.btnNoti.setImage(UIImage(named: "noti_btn_read"), for: .normal)
            } else {
                naviBar.btnNoti.setImage(UIImage(named: "noti_btn_unread"), for: .normal)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadCars), name: NSNotification.Name(rawValue: CarManager.nameCarReloadNotification), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: CarManager.nameCarReloadNotification), object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        var getvin = ""
        for cc in CarManager.shared().cars {
            if cc.licensePlateNumber == CarManager.shared().getSelectedCarNumber()! {
                getvin = cc.vin ?? ""
            }
        }
        
        if CarManager.shared().getSelectedCarNumber() == nil {
            let url = URL(string: "\(Config.urlMaintenance)?AccountId=\(UserManager.shared().currentUser.accountId!)&token=\(UserManager.shared().getUserToken()!)")!
            self.webView.load(URLRequest(url: url))
        } else {
            let url = URL(string: "\(Config.urlMaintenance)?VIN=\(getvin)&LicensePlateNumber=\(CarManager.shared().getSelectedCarNumber()!)&AccountId=\(UserManager.shared().currentUser.accountId!)&token=\(UserManager.shared().getUserToken()!)")!
            self.webView.load(URLRequest(url: url))
        }
    }
    
    @objc func reloadCars() {
        self.cars = CarManager.shared().getCars(remotely: true, listener: self)
    }
    
    override func didStartGetCar() {
        showHud()
    }
    
    override func didFinishGetCar(success: Bool, cars: [Car]) {
        dismissHud()
        
        if success {
            self.cars = cars
            if self.cars!.count == 0 {
                CarManager.shared().clearSelectedCarNumber()
                
            } else {
                setupSelectedCar()
                UserManager.shared().loginCruisys(listener: self, accountId: UserManager.shared().currentUser.accountId!, plateNum: CarManager.shared().getSelectedCarNumber()!)
            }
        }
    }
    
    func setupSelectedCar() {
        if let ci = CarManager.shared().getSelectedCarNumber() {
            self.carNumberPicker.isHidden = false
            self.topUserName.isHidden = true
            for car in self.cars! {
                if car.licensePlateNumber == ci {
                    self.selectedCar = car
                    // 選過車
                }
            }
            if self.selectedCar == nil {
                if self.cars!.count > 0 { // 選過的車不見了 設定第一台車為預設
                    self.selectedCar = CarManager.shared().cars[0]
                    CarManager.shared().saveSelectedCarNumer(with: CarManager.shared().cars[0].licensePlateNumber!)
                }
            }
            self.carNumberLb.text = self.selectedCar?.licensePlateNumber
            if self.cars!.count == 0 {
                self.carNumberPicker.isHidden = true
                self.topUserName.isHidden = false
            }
        } else {
            // 沒選過車
            if self.cars!.count > 0 {
                self.carNumberPicker.isHidden = false
                self.topUserName.isHidden = true
                self.selectedCar = self.cars![0]
                CarManager.shared().saveSelectedCarNumer(with: self.cars![0].licensePlateNumber!)
                
                self.carNumberLb.text = self.selectedCar?.licensePlateNumber
            } else {
                // 沒車
                self.carNumberPicker.isHidden = true
                self.topUserName.isHidden = false
            }
        }
    }
    
    @IBAction func carPickerBtnPressed(_ sender: Any) {
        // 車庫
        let vc = UIStoryboard(name: "Car", bundle: nil).instantiateViewController(withIdentifier: "MyGarageViewController") as! MyGarageViewController
        vc.cars = cars
        tabControllerDelegateV2?.goViewController(view: vc)
    }
    
    override func rightButtonNotiClicked(_ button: UIButton) {
        let sb = UIStoryboard(name: "Car", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "NotificationViewControllerV2") as! NotificationViewControllerV2
        vc.tabSelectedIndex = 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    }
    
    func webView(_ webView: WKWebView,
                 didFail navigation: WKNavigation!,
                 withError error: Error) {
        Logger.d("didFail")
    }
    
    func webView(_ webView: WKWebView,
                 didFailProvisionalNavigation navigation: WKNavigation!,
                 withError error: Error) {
        Logger.d("didFailProvisionalNavigation, error: \(error)")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Logger.d("didFinish")
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: ((WKNavigationActionPolicy) -> Void)) {
        
        let urll = navigationAction.request.url!
        if Deeplinker.handleDeeplink(url: urll, ref: nil) {
            print("start deeplink: \(urll)")
            Deeplinker.checkDeepLink()
        }
        
        decisionHandler(.allow)
    }

    // MARK: - JS
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {
        
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (action) in
            completionHandler()
        }))
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (Bool) -> Void) {
        
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (action) in
            completionHandler(true)
        }))
        
        alertController.addAction(UIAlertAction(title: "取消", style: .default, handler: { (action) in
            completionHandler(false)
        }))
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (String?) -> Void) {
        
        let alertController = UIAlertController(title: nil, message: prompt, preferredStyle: .actionSheet)
        
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        
        alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "取消", style: .default, handler: { (action) in
            completionHandler(nil)
        }))
        
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: profile protocol
    
    override func didStartGetProfile() {
    }
    
    override func didFinishGetProfile(success: Bool, profile: Profile?) {
        if success {
            if let p = profile {
                self.profile = p
                self.topUserName.text = "Hi, \(p.nickname ?? "")"
            }
        }
    }
}
