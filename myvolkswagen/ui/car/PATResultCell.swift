//
//  PATResultCell.swift
//  myvolkswagen
//
//  Created by Shelley on 2021/11/7.
//  Copyright © 2021 volkswagen. All rights reserved.
//

import UIKit

class PATResultCell: UICollectionViewCell {
    @IBOutlet weak var patTitleLabel: BasicLabel!
    @IBOutlet weak var adviceDateLabel: BasicLabel!
    @IBOutlet weak var adviceMilLabel: BasicLabel!
    @IBOutlet weak var adviceMustListLabel: BasicLabel!
    @IBOutlet weak var adviceExtraListLabel: BasicLabel!
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let autoLayoutAttributes = super.preferredLayoutAttributesFitting(layoutAttributes)
        let targetSize = CGSize(width: layoutAttributes.frame.width, height: 0)
        let autoLayoutSize = contentView.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: UILayoutPriority.required, verticalFittingPriority: UILayoutPriority.defaultLow)
        let autoLayoutFrame = CGRect(origin: autoLayoutAttributes.frame.origin, size: autoLayoutSize)
        autoLayoutAttributes.frame = autoLayoutFrame
        return autoLayoutAttributes
    }
}
