//
//  CarNumberTextField.swift
//  myvolkswagen
//
//  Created by Shelley on 2021/11/11.
//  Copyright © 2021 volkswagen. All rights reserved.
//

import UIKit

class CarNumberTextField: UITextField {
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        return CGRect.zero
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        updateFontSize()
    }
    
    func updateFontSize() {
        self.font = self.font?.withSize(self.font!.pointSize.getFitSize())
    }
    
}

class BasicSizingTextField: UITextField {

    override func awakeFromNib() {
        super.awakeFromNib()
        updateFontSize()
    }
    
    func updateFontSize() {
        self.font = self.font?.withSize(self.font!.pointSize.getFitSize())
    }
    
}

