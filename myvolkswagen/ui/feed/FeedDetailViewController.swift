//
//  FeedDetailViewController.swift
//  myvolkswagen
//
//  Created by Apple on 2/20/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class FeedDetailViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, ClubUpdateDelegate {
    
    
    
    enum Section: Int {
        case header = 0, comment, input
        static var allCases: [Section] {
            var values: [Section] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    var news: News!
    var clubPost: ClubPost!
    var inboxId: Int!
    var clubUpdateDelegate: ClubUpdateDelegate?
    var isFull: Bool = false
    var isInboxContent: Bool = false
    var noticeKind: NoticeKind?
    
    @IBOutlet weak var tableView: UITableView!
    var cellHeightsDictionary: [IndexPath: CGFloat]!
    
    var editingContent: String?
    var shouldScrollToBottom: Bool = false
    
    // footer
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet weak var input: UITextField!
    @IBOutlet weak var submit: UIButton!

    
    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()
        
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        naviBar.setTitle("post".localized)
        
        tableView.backgroundColor = UiUtility.colorBackgroundWhite
        cellHeightsDictionary = [:]
        tableView.rowHeight = UITableView.automaticDimension
        
        // footer
        viewFooter.backgroundColor = UiUtility.colorBackgroundWhite
        
        input.textColor = UIColor.black.withAlphaComponent(0.5)
        input.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 16))!
        input.placeholder = "hint_input".localized
        input.layer.borderColor = UiUtility.colorBrownGrey.cgColor
        input.layer.borderWidth = 1
        input.layer.cornerRadius = 15
        input.backgroundColor = UIColor.white
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 30))
        input.leftView = paddingView
        input.leftViewMode = .always
        input.inputAccessoryView = toolbar
        input.delegate = self
        input.text = editingContent
        
        submit.setTitleColor(UIColor.black, for: .normal)
        submit.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!
        submit.setTitle("leave_message".localized, for: .normal)
        submit.addTarget(self, action: #selector(leaveMessage(_:)), for: .touchUpInside)
        
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        self.tableView.addGestureRecognizer(longPressRecognizer)
        
        if !isFull {
            if news == nil {
                ClubManager.shared().getPost(post: clubPost, listener: self)
            } else {
                NewsManager.shared().getNews(news: news, noticeKind: noticeKind, listener: self)
            }
        }
        
        if isInboxContent {
            NoticeManager.shared().getInboxContent(inBoxId: inboxId, listener: self)
            viewFooter.isHidden = true
            naviBar.setTitle("news".localized)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if contentIsDirtyRemote {
            if news != nil {
                NewsManager.shared().getNews(news: news, noticeKind: noticeKind, listener: self)
            }
            
            if clubPost != nil {
                ClubManager.shared().getPost(post: clubPost, listener: self)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if offsetY == -1 {
            offsetY = self.view.frame.origin.y
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    // MARK: notice protocol
    override func didStartGetInboxContent() {
        showHud()
    }
    
    override func didFinishGetInboxContent(success: Bool, inboxContent: News?) {
        dismissHud()
        if success {
//            Logger.d("get news: \(news)")
            if inboxContent?.newsId == nil {
                let alertController = UIAlertController(title: nil, message: "post_has_deleted".localized, preferredStyle: .alert)
                let action1 = UIAlertAction(title: "got_it".localized, style: .default) { (action:UIAlertAction) in
                    Logger.d("do nothing")
                }
                
                alertController.addAction(action1)
                present(alertController, animated: true, completion: nil)
            } else {
                self.news = inboxContent
                let n = Notice(with: "", idd: self.news.noticeId!)
                NoticeManager.shared().setRead(notice: n, listener: self)
                self.tableView.reloadData()
            }
        } else {
            Logger.d("failed to get news")
        }
    }
    
    // MARK: club update delegate
    
    func didFinishUpdateClub() {
        contentIsDirtyRemote = true
        
        if clubUpdateDelegate == nil {
            ClubManager.shared().cleanCache()
        } else {
            clubUpdateDelegate?.didFinishUpdateClub()
        }
    }
    
    
    
    // MARK: club protocol
    
    override func didFinishGetClubPost(success: Bool, post: ClubPost?) {
        super.didFinishGetClubPost(success: success, post: post)
        
        if success {
            contentIsDirtyRemote = false
            contentIsDirtyLocal = false
            if let post = post {
                self.clubPost = post
                tableView.reloadData()
                
                if shouldScrollToBottom {
                    shouldScrollToBottom = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.tableView.scrollToRow(at: IndexPath.init(row: 0, section: Section.input.rawValue), at: .bottom, animated: true)
                    }
                }
            }
        }
    }
    
    override func didFinishCommentClubPost(success: Bool, post: ClubPost) {
        super.didFinishCommentClubPost(success: success, post: post)
        
        if success {
            editingContent = nil
            input.text = editingContent
            shouldScrollToBottom = true
//            NewsManager.shared().listIsDirty = true
            ClubManager.shared().getPost(post: clubPost, listener: self)
        }
    }
    
    override func didFinishDeleteClubPostComment(success: Bool, comment: Comment?) {
        super.didFinishDeleteClubPostComment(success: success, comment: comment)
        
        if success {
            var comments: [Comment] = []
            var hasMine: Bool = false
            for c in clubPost.comments {
                if c.commentId != comment?.commentId {
                    if c.isMine() {
                        hasMine = true
                    }
                    comments.append(c)
                }
            }
            
            clubPost.comments = comments
            clubPost.commentStatus = hasMine ? 1 : 0
            clubPost.commentAmount -= 1
            
//            NewsManager.shared().listIsDirty = true
            tableView.reloadData()
        }
    }
    
    override func didFinishLikeClubPost(success: Bool, post: ClubPost, set: Bool) {
        dismissHud()
        
        if success {
            if set {
                clubPost.likeStatus = 1
                clubPost.likeAmount = clubPost.likeAmount + 1
            } else {
                clubPost.likeStatus = 0
                clubPost.likeAmount = clubPost.likeAmount - 1
            }
            
            tableView.reloadData()
            if clubUpdateDelegate == nil {
                ClubManager.shared().cleanCache()
            } else {
                clubUpdateDelegate?.didFinishUpdateClub()
            }
        }
    }
    
    override func didFinishTrackClubPost(success: Bool, post: ClubPost, set: Bool) {
        dismissHud()
        
        if success {
            if set {
                clubPost.bookmarkStatus = 1
                clubPost.bookmarkAmount = clubPost.bookmarkAmount + 1
            } else {
                clubPost.bookmarkStatus = 0
                clubPost.bookmarkAmount = clubPost.bookmarkAmount - 1
            }
            tableView.reloadData()
            if clubUpdateDelegate == nil {
                ClubManager.shared().cleanCache()
            } else {
                clubUpdateDelegate?.didFinishUpdateClub()
            }
        }
    }
    
    override func didFinishSetTop(success: Bool, post: ClubPost, set: Bool) {
        super.didFinishSetTop(success: success, post: post, set: set)
        
        if success {
            clubPost.isTop = (set ? 1 : 0)
            if clubUpdateDelegate == nil {
                ClubManager.shared().cleanCache()
            } else {
                clubUpdateDelegate?.didFinishUpdateClub()
            }
        }
    }
    
    override func didFinishDeleteClubPost(success: Bool, post: ClubPost?) {
        super.didFinishDeleteClubPost(success: success, post: post)
        
        if clubUpdateDelegate == nil {
            ClubManager.shared().cleanCache()
        } else {
            clubUpdateDelegate?.didFinishUpdateClub()
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    
    
    // MARK: news protocol
    
    override func didStartCommentNews() {
        showHud()
    }
    
    override func didFinishCommentNews(success: Bool, news: News) {
        dismissHud()
        if success {
            editingContent = nil
            input.text = editingContent
            shouldScrollToBottom = true
            NewsManager.shared().listIsDirty = true
            NewsManager.shared().getNews(news: news, noticeKind: noticeKind, listener: self)
        }
    }
    
    override func didStartGetNews() {
        showHud()
    }
    
    override func didFinishGetNews(success: Bool, news: News?, noticeKind: NoticeKind?) {
        dismissHud()
        if success {
            self.noticeKind = noticeKind
            contentIsDirtyRemote = false
            contentIsDirtyLocal = false
            if let n = news {
                self.news = n
                tableView.reloadData()
                
                if shouldScrollToBottom {
                    shouldScrollToBottom = false
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.tableView.scrollToRow(at: IndexPath.init(row: 0, section: Section.input.rawValue), at: .bottom, animated: true)
                    }
                }
            }
        }
    }
    
    override func didStartLikeNews() {
        showHud()
    }
    
    override func didFinishLikeNews(success: Bool, news: News, set: Bool) {
        dismissHud()
        
        if success {
            if set {
                self.news.likeStatus = 1
                self.news.likeAmount = self.news.likeAmount + 1
            } else {
                self.news.likeStatus = 0
                self.news.likeAmount = self.news.likeAmount - 1
            }
            
            NewsManager.shared().listIsDirty = true
            tableView.reloadData()
        }
    }
    
    override func didStartTrackNews() {
        showHud()
    }
    
    override func didFinishTrackNews(success: Bool, news: News, set: Bool) {
        dismissHud()
        
        if success {
            if set {
                self.news.bookmarkStatus = 1
                self.news.bookmarkAmount = self.news.bookmarkAmount + 1
            } else {
                self.news.bookmarkStatus = 0
                self.news.bookmarkAmount = self.news.bookmarkAmount - 1
            }
            
            NewsManager.shared().listIsDirty = true
            tableView.reloadData()
        }
    }
    
    override func didStartReportNews() {
        showHud()
    }
    
    override func didFinishReportNews(success: Bool, news: News, type: Int) {
        dismissHud()
        
        if success {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReportViewController")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func didStartDeletePost() {
        showHud()
    }
    
    override func didFinishDeletePost(success: Bool, news: News?) {
        dismissHud()
        
        if success {
            // TODO: you can do it better
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func didStartDeleteComment() {
        showHud()
    }
    
    override func didFinishDeleteComment(success: Bool, comment: Comment?) {
        dismissHud()
        
        if success {
            var comments: [Comment] = []
            var hasMine: Bool = false
            for c in news.comments {
                if c.commentId != comment?.commentId {
                    if c.isMine() {
                        hasMine = true
                    }
                    comments.append(c)
                }
            }
            
            news.comments = comments
            news.commentStatus = hasMine ? 1 : 0
            news.commentAmount -= 1
            
            NewsManager.shared().listIsDirty = true
            tableView.reloadData()
        }
    }
    
    
    
    // MARK: text field delegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        editingContent = textField.text
    }
    
    
    
    // MARK: keyboard
    
    @objc override func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let d = updateFromKeyboardChangeToFrame(keyboardSize)
            self.view.frame.origin.y -= d
        }
        
        keyboardShowing = true
    }
    
    @objc override func keyboardWillHide(notification: NSNotification) {
        
        if self.view.frame.origin.y != offsetY {
            self.view.frame.origin.y = offsetY
        }
        
        keyboardShowing = false
    }
    
    
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isInboxContent && news == nil {
            return 0
        }
        return Section.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Section.init(rawValue: section)! {
        case .header:
            return 1
        case .comment:
            if isInboxContent {
                return 0
            }
            if news != nil {
                return news.comments.count
            } else {
                return clubPost.comments.count
            }
        case .input:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Section.init(rawValue: indexPath.section)! {
        case .header:
            let width = UiUtility.adaptiveSize(size: 375 - 20 - 20)
            let font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))!
            
            var h: CGFloat = 0
            h += UiUtility.adaptiveSize(size: 70) // height before content
            
            if news != nil {
                if !Utility.isEmpty(news.content) {
                    h += news.content!.height(withConstrainedWidth: width, font: font)
                }
            } else if clubPost != nil {
                if !Utility.isEmpty(clubPost.content) {
                    h += clubPost.content!.height(withConstrainedWidth: width, font: font)
                }
            }
            
            h += UiUtility.adaptiveSize(size: 36) // date
            h += UiUtility.adaptiveSize(size: 375) // image
            h += UiUtility.adaptiveSize(size: 50) // footer
            h += UiUtility.adaptiveSize(size: 6) // separator
            
            return h
        case .comment:
            let comment: Comment = (news == nil ? clubPost.comments[indexPath.row] : news.comments[indexPath.row])
            let width = UiUtility.adaptiveSize(size: 375 - 70 - 39)
            let font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))!
            
            var h: CGFloat = 0
            h += UiUtility.adaptiveSize(size: 60) // height before content
            
            if !Utility.isEmpty(comment.content) {
                h += comment.content!.height(withConstrainedWidth: width, font: font)
            }
            
            h += UiUtility.adaptiveSize(size: 36) // date
            return h
        case .input:
            return 48
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightsDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let h:CGFloat = cellHeightsDictionary[indexPath] {
            return h
        } else {
            return UITableView.automaticDimension
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        switch Section.init(rawValue: indexPath.section)! {
        case .header:
            if news != nil {
                cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath)
                cell.selectionStyle = .none
                
                let feedItem: FeedItemView = cell.viewWithTag(1) as! FeedItemView
                for constraint in cell.contentView.constraints {
                    if constraint.identifier == "marginBottom" {
                        constraint.constant = UiUtility.adaptiveSize(size: 6)
                    }
                }
                
                feedItem.setupUi(with: news, isList: false, inPerson: false)
                feedItem.content.numberOfLines = 0
                feedItem.buttonUser.addTarget(self, action: #selector(userClicked(_:)), for: .touchUpInside)
                feedItem.buttonMore.addTarget(self, action: #selector(moreClicked(_:)), for: .touchUpInside)
                feedItem.buttonLike.addTarget(self, action: #selector(likeClicked(_:)), for: .touchUpInside)
                feedItem.buttonLikeList.addTarget(self, action: #selector(likersClicked(_:)), for: .touchUpInside)
                feedItem.buttonTrack.addTarget(self, action: #selector(trackClicked(_:)), for: .touchUpInside)
                
                if isInboxContent {
                    feedItem.toolBarView.isHidden = true
                }
                
                feedItem.content.foregroundColor = { (linkResult) in
                    switch linkResult.detectionType {
                    case .url:
                        return UIColor(rgb: 0x007AFF)
                    default:
                        return UiUtility.colorDarkGrey
                    }
                }
                
                feedItem.content.underlineStyle = { (linkResult) in
                    switch linkResult.detectionType {
                    default:
                        return NSUnderlineStyle.single
                    }
                }
                
                feedItem.content.didTouch = { (touchResult) in
                    switch touchResult.state {
                    case .began:
                        Logger.d("began")
                    case .ended:
                        Logger.d("ended")
                        if let linkResult = touchResult.linkResult {
                            if linkResult.detectionType == .url {
                                Logger.d("text: \(linkResult.text)")
                                
                                var text = linkResult.text
                                if !text.starts(with: "http://") && !text.starts(with: "https://") {
                                    text = "http://\(text)"
                                }
                                if let link = URL(string: text) {
                                    if let nk = self.noticeKind {
                                        if nk == .normalSurvey {
                                            let ttext = "\(text)?aka_account_id=\(UserManager.shared().currentUser.accountId!)"
                                            UiUtility.goBrowseWebview(url: URL(string: ttext)!, notiKind: .normalSurvey, controller: self)
                                        } else if nk == .newCarsSurvey {
                                            let ttext = "\(text)?AccountId=\(UserManager.shared().currentUser.accountId!)"
                                            UiUtility.goBrowseWebview(url: URL(string: ttext)!, notiKind: .newCarsSurvey, controller: self)
                                        } else if nk == .bodyAndPaint {
                                            UiUtility.goBrowseWebview(url: link, notiKind: .BnP, controller: self)
                                        }
                                    } else {
                                        if link.pathComponents.contains("memberCouponList.html") {
                                            let ttext = "\(text)?token=\(UserManager.shared().getUserToken()!)&AccountId=\(UserManager.shared().currentUser.accountId!)"
                                            UiUtility.goBrowseWebview(url: URL(string: ttext)!, notiKind: .memberPointList, controller: self)
                                        } else if link.host!.contains("service-cam.volkswagen-service.com") {
                                            UiUtility.goBrowseWebview(url: link, notiKind: .serviceCam, controller: self)
                                        } else {
                                            UIApplication.shared.open(link)
                                        }
                                    }
                                }
                            }
                            if linkResult.detectionType == .phoneNumber {
                                if let url = URL(string: "telprompt://\(linkResult.text)") {
                                    UIApplication.shared.open(url)
                                }
                            }
                        }
                    //                    Logger.d("ended, \(touchResult)")
                    default:
                        break
                    }
                }
            } else if clubPost != nil {
                cell = tableView.dequeueReusableCell(withIdentifier: "ClubCell", for: indexPath)
                cell.selectionStyle = .none
                
                let feedItem: ClubItemView = cell.viewWithTag(1) as! ClubItemView
                for constraint in cell.contentView.constraints {
                    if constraint.identifier == "marginBottom" {
                        constraint.constant = UiUtility.adaptiveSize(size: 6)
                    }
                }
                
                feedItem.setupUi(with: clubPost, isList: false, inPerson: false)
                feedItem.content.numberOfLines = 0
                feedItem.buttonUser.addTarget(self, action: #selector(userClicked(_:)), for: .touchUpInside)
                feedItem.buttonMore.addTarget(self, action: #selector(moreClicked(_:)), for: .touchUpInside)
                feedItem.buttonLike.addTarget(self, action: #selector(likeClicked(_:)), for: .touchUpInside)
                feedItem.buttonLikeList.addTarget(self, action: #selector(likersClicked(_:)), for: .touchUpInside)
                feedItem.buttonTrack.addTarget(self, action: #selector(trackClicked(_:)), for: .touchUpInside)
                
                feedItem.content.foregroundColor = { (linkResult) in
                    switch linkResult.detectionType {
                    case .url:
                        return UIColor(rgb: 0x007AFF)
                    default:
                        return UiUtility.colorDarkGrey
                    }
                }
                
                feedItem.content.underlineStyle = { (linkResult) in
                    switch linkResult.detectionType {
                    default:
                        return NSUnderlineStyle.single
                    }
                }
                
                feedItem.content.didTouch = { (touchResult) in
                    switch touchResult.state {
                    case .began:
                        Logger.d("began")
                    case .ended:
                        Logger.d("ended")
                        if let linkResult = touchResult.linkResult {
                            if linkResult.detectionType == .url {
                                Logger.d("text: \(linkResult.text)")
                                
                                var text = linkResult.text
                                if !text.starts(with: "http://") && !text.starts(with: "https://") {
                                    text = "http://\(text)"
                                }
                                if let link = URL(string: text) {
                                    UIApplication.shared.open(link)
                                }
                            }
                            if linkResult.detectionType == .phoneNumber {
                                if let url = URL(string: "telprompt://\(linkResult.text)") {
                                    UIApplication.shared.open(url)
                                }
                            }
                        }
                    //                    Logger.d("ended, \(touchResult)")
                    default:
                        break
                    }
                }
            }
            
            
            
        case .comment:
            cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath)
            cell.selectionStyle = .none
            
            let imageUser: UIImageView = cell.viewWithTag(1) as! UIImageView
            let nameUser: UILabel = cell.viewWithTag(2) as! UILabel
            let content: UILabel = cell.viewWithTag(3) as! UILabel
            let date: UILabel = cell.viewWithTag(4) as! UILabel
            let buttonUser: UIButton = cell.viewWithTag(5) as! UIButton
            let imageCheck: UIImageView = cell.viewWithTag(6) as! UIImageView
            
            for constraint in cell.contentView.constraints {
                if constraint.identifier == "imageTop" {
                    constraint.constant = UiUtility.adaptiveSize(size: 10)
                }
            }
            
            for constraint in date.constraints {
                if constraint.identifier == "dateHeight" {
                    constraint.constant = UiUtility.adaptiveSize(size: 36)
                }
            }
            
            imageUser.backgroundColor = UiUtility.colorBackgroundWhite
            imageUser.layer.masksToBounds = true
            imageUser.layer.cornerRadius = UiUtility.adaptiveSize(size: 20)
            
            buttonUser.addTarget(self, action: #selector(commentUserClicked(_:)), for: .touchUpInside)
            
            // user name
            nameUser.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            nameUser.textColor = UIColor.black
            
            // content
            content.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
            content.textColor = UiUtility.colorDarkGrey
            for constraint in cell.contentView.constraints {
                if constraint.identifier == "marginTopContent" {
                    constraint.constant = UiUtility.adaptiveSize(size: 60)
                }
            }
            
            // date
            date.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
            date.textColor = UiUtility.colorBrownGrey
            
            let comment: Comment = (news == nil ? clubPost.comments[indexPath.row] : news.comments[indexPath.row])
            Logger.d("comment: \(comment)")
            if let stickerURL = comment.stickerURL {
                let url = URL(string: stickerURL)
                imageUser.kf.setImage(with: url)
            }
            
            // username
            nameUser.text = comment.nickname
            
            // content
            content.text = comment.content
            
            // date
            date.text = comment.getDateText()
            
            // image check
            imageCheck.isHidden = true
//            if let level = comment.level {
//                if level > 2 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_golden_large")
//                } else if level > 1 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_silver_large")
//                } else if level > 0 {
//                    imageCheck.isHidden = false
//                    imageCheck.image = UIImage.init(named: "check_large")
//                }
//            }
        case .input:
            cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath)
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    
    
    @objc private func leaveMessage(_ sender: UIButton) {
        self.view.endEditing(true)
        if let content = editingContent {
//            Logger.d("sending content: \(content)")
            if news == nil {
                ClubManager.shared().comment(post: clubPost, content: content, listener: self)
            } else {
                NewsManager.shared().comment(news: news, content: content, listener: self)
            }
        }
    }
    
    @objc private func userClicked(_ sender: UIButton) {
        if news == nil {
            if let _ = clubPost.accountId {
                UiUtility.goBrowse(profile: Profile.init(with: clubPost), controller: self)
            }
        } else {
            if let _ = news.accountId {
                UiUtility.goBrowse(profile: Profile.init(with: news), controller: self)
            }
        }
    }
    
    @objc private func commentUserClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            if news == nil {
                let comment: Comment = clubPost.comments[indexPath.row]
                UiUtility.goBrowse(profile: Profile.init(with: comment), controller: self)
            } else {
                let comment: Comment = news.comments[indexPath.row]
                UiUtility.goBrowse(profile: Profile.init(with: comment), controller: self)
            }
        }
    }
    
    @objc private func moreClicked(_ sender: UIButton) {
        if news == nil {
            UiUtility.clubMoreMenu(from: self, post: clubPost, clubProtocol: self, clubUpdateDelegate: self)
        } else {
            if news.offical != nil && news.offical! == Role.official.rawValue {
                // official
                UiUtility.moreMenu(from: self, type: 0, news: news, contentDelegate: self)
            } else if let newsAccoundId = news.accountId {
                if newsAccoundId == UserManager.shared().currentUser.accountId! {
                    // mine
                    UiUtility.moreMenu(from: self, type: 1, news: news, contentDelegate: self)
                } else {
                    // others
                    UiUtility.moreMenu(from: self, type: 2, news: news, contentDelegate: self)
                }
            }
        }
    }
    
    @objc private func likeClicked(_ sender: UIButton) {
        if news == nil {
            ClubManager.shared().like(news: clubPost, set: (clubPost.likeStatus == 0), listener: self)
        } else {
            NewsManager.shared().like(news: news, set: (news.likeStatus == 0), listener: self)
        }
    }
    
    @objc private func likersClicked(_ sender: UIButton) {
        if news == nil {
            UiUtility.goBrowseClubPostLikers(of: clubPost, controller: self)
        } else {
            UiUtility.goBrowseLikers(of: news, controller: self)
        }
    }
    
    @objc private func trackClicked(_ sender: UIButton) {
        if news == nil {
            ClubManager.shared().track(news: clubPost, set: (clubPost.bookmarkStatus == 0), listener: self)
        } else {
            NewsManager.shared().track(news: news, set: (news.bookmarkStatus == 0), listener: self)
        }
    }
    
    @objc private func handleLongPress(_ longPressGestureRecognizer: UILongPressGestureRecognizer) {
        
        if longPressGestureRecognizer.state == UIGestureRecognizer.State.began {
            
            let touchPoint = longPressGestureRecognizer.location(in: self.tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint) {
                switch Section.init(rawValue: indexPath.section)! {
                case .comment:
                    let comment: Comment = (news == nil ? clubPost.comments[indexPath.row] : news.comments[indexPath.row])
                    if comment.isMine() {
                        if let _ = comment.commentId {
                            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                            
                            alert.addAction(UIAlertAction(title: "delete_comment".localized, style: .destructive, handler: { _ in
                                if self.news == nil {
                                    ClubManager.shared().delete(comment: comment, listener: self)
                                } else {
                                    NewsManager.shared().delete(comment: comment, listener: self)
                                }
                            }))
                            
                            alert.addAction(UIAlertAction.init(title: "cancel".localized, style: .cancel, handler: nil))
                            present(alert, animated: true, completion: nil)
                        }
                    }
                default:
                    break
                }
            }
        }
    }
}

