//
//  LikeViewController.swift
//  myvolkswagen
//
//  Created by Apple on 3/6/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class LikeViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource {
    
    var news: News!
    var post: ClubPost!
    
    @IBOutlet weak var tableView: UITableView!
    var likers: [Relation] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UiUtility.colorBackgroundWhite
        tableView.backgroundColor = UiUtility.colorBackgroundWhite
        
        naviBar.setTitle("liker".localized)
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if news != nil {
            NewsManager.shared().getMembersOfBehavior(news: news, behavior: .like, listener: self)
        }
        
        if post != nil {
            ClubManager.shared().getMembersOfBehavior(news: post, behavior: .like, listener: self)
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    
    
    // MARK: user protocol
    
    override func didStartFollow() {
        showHud()
    }
    
    override func didFinishFollow(success: Bool, accountId: String, set: Bool) {
        dismissHud()
        
        if success {
            var index = 0
            for liker in likers {
                if let na = liker.accountId {
                    if na == accountId {
                        if set {
                            liker.isFollow = 1
                        } else {
                            liker.isFollow = 0
                        }
                        
                        NewsManager.shared().listIsDirtyRemote = true
                        tableView.reloadData()
                        break
                    }
                }
                
                index += 1
            }
        }
    }
    
    
    
    // MARK: club protocol
    
    override func didFinishGetClubMembersOfBehavior(success: Bool, members: [Relation]?) {
        super.didFinishGetClubMembersOfBehavior(success: success, members: members)
        
        if success {
            if let members = members {
                self.likers = members
                tableView.reloadData()
            }
        }
    }
    
    
    
    // MARK: news protocol
    
    override func didStartGetMembersOfBehavior() {
        showHud()
    }
    
    override func didFinishGetMembersOfBehavior(success: Bool, members: [Relation]?) {
        dismissHud()
        
        if success {
            if let members = members {
                self.likers = members
                tableView.reloadData()
            }
        }
    }
    
    
    
    // MARK: table view delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return likers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiUtility.adaptiveSize(size: 80)
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        cell = tableView.dequeueReusableCell(withIdentifier: "LikerCell", for: indexPath)
        
        let photo: UIImageView = cell.viewWithTag(1) as! UIImageView
        let name: UILabel = cell.viewWithTag(2) as! UILabel
        let button: UIButton = cell.viewWithTag(3) as! UIButton
        let imageCheck: UIImageView = cell.viewWithTag(4) as! UIImageView
        
        let liker = likers[indexPath.row]
        
        photo.backgroundColor = UiUtility.colorBackgroundWhite
        photo.layer.masksToBounds = true
        photo.layer.cornerRadius = UiUtility.adaptiveSize(size: 30)
        if let stickerURL = liker.stickerURL {
            let url = URL(string: stickerURL)
            photo.kf.setImage(with: url, placeholder: UIImage(named: "default_profile_photo"))
        }
        
        name.textColor = UIColor.black
        name.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
        name.text = liker.nickname
        
        button.isHidden = liker.isMe() || liker.isOfficial()
        button.setImage(liker.isFollowedByMe() ? UIImage.init(named: "following_small") : UIImage.init(named: "follow_small"), for: .normal)
        button.addTarget(self, action: #selector(buttonClicked(_:)), for: .touchUpInside)
        
        // image check
        imageCheck.isHidden = true
//        if let level = liker.level {
//            if level > 2 {
//                imageCheck.isHidden = false
//                imageCheck.image = UIImage.init(named: "check_golden_large")
//            } else if level > 1 {
//                imageCheck.isHidden = false
//                imageCheck.image = UIImage.init(named: "check_silver_large")
//            } else if level > 0 {
//                imageCheck.isHidden = false
//                imageCheck.image = UIImage.init(named: "check_large")
//            }
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        Logger.d("You tapped cell number \(indexPath.row).")
        tableView.deselectRow(at: indexPath, animated: true)
        let liker = likers[indexPath.row]
        UiUtility.goBrowse(profile: Profile.init(with: liker), controller: self)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @objc private func buttonClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            //            Logger.d("click like at row \(String(describing: indexPath.row))")
            let liker: Relation = likers[indexPath.row]
            if let aid = liker.accountId {
                UserManager.shared().follow(accountId: aid, set: !liker.isFollowedByMe(), listener: self)
            }

        }
    }
}
