//
//  FavoritePostViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2022/9/22.
//  Copyright © 2022 volkswagen. All rights reserved.
//

import UIKit

class FavoritePostViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    enum Section: Int {
        case feed = 0, empty
        static var allCases: [Section] {
            var values: [Section] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    var contentKind: ContentKind = .collection
    var profile: Profile! = nil
    var isMyId = false
    var relationFollowing: Relation?
    
    var newsList: [News] = []
    
    var couldShowPostEmpty: Bool = false
    var couldShowCollectionEmpty: Bool = false
    var couldShowOfficialAllEmpty: Bool = false
    var couldShowOfficialMessageEmpty: Bool = false
    var couldShowOfficialActivityEmpty: Bool = false
    var cellHeightsDictionary: [IndexPath: CGFloat]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UiUtility.colorBackgroundWhite
        
        naviBar.setBackgroundColor(UIColor.clear)
        
        cellHeightsDictionary = [:]
        tableView.rowHeight = UITableView.automaticDimension
        
        NewsManager.shared().getMemberContent(memberProfile: self.profile, contentKind: contentKind, listener: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateProfileAccountId), name: Notification.Name(rawValue: "updateProfileAccountId"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func refreshContent() {
        NewsManager.shared().getMemberContent(memberProfile: self.profile, contentKind: contentKind, listener: self)
    }
    
    @objc func updateProfileAccountId() {
        if isMyId {
            profile.accountId = UserManager.shared().currentUser.accountId
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        
        // get mine
        isMyId = true
        if let myId = UserManager.shared().currentUser.accountId {
            profile = UserManager.shared().getProfile(withId: myId, listener: self)
            
            
            updateUi()
        }
    }
    
    func updateUi() {
        self.tableView.reloadData()
    }
    
    // MARK: user protocol
    
    override func didStartFollow() {
        showHud()
    }
    
    override func didFinishFollow(success: Bool, accountId: String, set: Bool) {
        dismissHud()
        
        if success {
            for n in newsList {
                if let na = n.accountId {
                    if na == accountId {
                        if set {
                            n.isFollow = 1
                        } else {
                            n.isFollow = 0
                        }
                    }
                }
            }
            
            NewsManager.shared().listIsDirtyRemote = true
            self.relationFollowing = UserManager.shared().getFollowingRelation(to: self.profile, listener: self)
            updateUi()
        }
    }
    
    override func didStartGetRelation() {}
    override func didFinishGetRelation(success: Bool, isFollower: Bool, relations: [Relation]?) {
        // we now only care about following relation to specific user
        if success && !isFollower {
            if let relations = relations {
                if relations.count != 0 {
                    if let profile = self.profile {
                        self.relationFollowing = UserManager.shared().getFollowingRelation(to: profile, listener: nil)
                        updateUi()
                    }
                }
            }
        }
    }
    
    
    
    // MARK: news protocol
    
    override func didStartGetMemberContent() {
        //showHud()
    }
    
    override func didFinishGetMemberContent(success: Bool, newsList: [News]?) {
        //dismissHud()
        
        if success {
            contentIsDirtyRemote = false
            if let newsList = newsList {
                if contentKind == .post {
                    couldShowPostEmpty = true
                } else if contentKind == .collection {
                    couldShowCollectionEmpty = true
                } else if contentKind == .all {
                    couldShowOfficialAllEmpty = true
                } else if contentKind == .new {
                    couldShowOfficialMessageEmpty = true
                } else if contentKind == .activity {
                    couldShowOfficialActivityEmpty = true
                }
                self.newsList = newsList
                updateUi()
            }
        }
    }
    
    override func didStartLikeNews() {
        showHud()
    }
    
    override func didFinishLikeNews(success: Bool, news: News, set: Bool) {
        dismissHud()
        
        if success {
            var index = 0
            for n in newsList {
                if n.newsId! == news.newsId! {
                    if set {
                        n.likeStatus = 1
                        n.likeAmount = n.likeAmount + 1
                    } else {
                        n.likeStatus = 0
                        n.likeAmount = n.likeAmount - 1
                    }
                    updateUi()
                    break
                }
                index += 1
            }
        }
    }
    
    override func didStartTrackNews() {
        showHud()
    }
    
    override func didFinishTrackNews(success: Bool, news: News, set: Bool) {
        dismissHud()
        
        if success {
            if self.profile.isMine() && contentKind == .collection {
                var newArray: [News] = []
                for n in newsList {
                    if n.newsId! != news.newsId! {
                        newArray.append(n)
                    }
                }
                newsList = newArray
                updateUi()
            } else {
                for n in newsList {
                    if n.newsId! == news.newsId! {
                        if set {
                            n.bookmarkStatus = 1
                            n.bookmarkAmount = n.bookmarkAmount + 1
                        } else {
                            n.bookmarkStatus = 0
                            n.bookmarkAmount = n.bookmarkAmount - 1
                        }
                        updateUi()
                        break
                    }
                }
            }
        }
    }
    
    override func didStartReportNews() {
        showHud()
    }
    
    override func didFinishReportNews(success: Bool, news: News, type: Int) {
        dismissHud()
        
        if success {
            self.tabControllerDelegate?.goFinishReport()
        }
    }
    
    override func didStartDeletePost() {
        showHud()
    }
    
    override func didFinishDeletePost(success: Bool, news: News?) {
        dismissHud()
        
        if success {
            // TODO: you can do it better
            NewsManager.shared().getMemberContent(memberProfile: self.profile, contentKind: contentKind, listener: self)
        }
    }
    
    
    
    // MARK: profile protocol
    
    override func didStartGetProfile() {
        showHud()
    }
    
    override func didFinishGetProfile(success: Bool, profile: Profile?) {
        dismissHud()
        
        if success {
            if let p = profile {
                self.profile = p
                //                Logger.d("profile: \(p)")
                //                updateUi()
                
                // get content from remote
                NewsManager.shared().getMemberContent(memberProfile: self.profile, contentKind: contentKind, listener: self)
                
                if !p.isMine() {
                    if p.offical == nil || p.offical! != 2 {
                        // get relation (from remote maybe)
                        self.relationFollowing = UserManager.shared().getFollowingRelation(to: p, listener: self)
                    }
                }
                
                updateUi()
            }
        }
    }
    
    
    // MARK: table view delegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if profile == nil {
            return 0
        }
        
        switch Section.init(rawValue: section)! {
        case .feed:
            return newsList.count
        case .empty:
            switch contentKind {
            case .post:
                return newsList.count == 0 && couldShowPostEmpty ? 1 : 0
            case .collection:
                return newsList.count == 0 && couldShowCollectionEmpty ? 1 : 0
            case .all:
                return newsList.count == 0 && couldShowOfficialAllEmpty ? 1 : 0
            case .new:
                return newsList.count == 0 && couldShowOfficialMessageEmpty ? 1 : 0
            case .activity:
                return newsList.count == 0 && couldShowOfficialActivityEmpty ? 1 : 0
                //            default:
                //                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch Section.init(rawValue: indexPath.section)! {
        case .feed:
            let width = UiUtility.adaptiveSize(size: 375 - 20 - 20)
            let font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))!
            let maxContentHeight: CGFloat = "line_4_text".localized.height(withConstrainedWidth: width, font: font)
            
            var h: CGFloat = 0
            h += UiUtility.adaptiveSize(size: 70) // height before content
            
            let news = newsList[indexPath.row]
            if !Utility.isEmpty(news.content) {
                h += min(news.content!.height(withConstrainedWidth: width, font: font), maxContentHeight)
            }
            
            h += UiUtility.adaptiveSize(size: 36) // date
            h += UiUtility.adaptiveSize(size: 375) // image
            h += UiUtility.adaptiveSize(size: 50) // footer
            h += UiUtility.adaptiveSize(size: 6) // separator
            return h
        case .empty:
            return UiUtility.adaptiveSize(size: 186)
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeightsDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let h:CGFloat = cellHeightsDictionary[indexPath] {
            return h
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell!
        
        switch Section.init(rawValue: indexPath.section)! {
        case .feed:
            cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath)
            cell.backgroundColor = UiUtility.colorBackgroundWhite
            cell.selectionStyle = .none
            
            let feedItem: FeedItemView = cell.viewWithTag(1) as! FeedItemView
            for constraint in cell.contentView.constraints {
                if constraint.identifier == "marginBottom" {
                    constraint.constant = UiUtility.adaptiveSize(size: 6)
                }
            }
            
            let news = newsList[indexPath.row]
            feedItem.setupUi(with: news, isList: false, inPerson: true)
            
            feedItem.buttonUser.addTarget(self, action: #selector(userClicked(_:)), for: .touchUpInside)
            feedItem.buttonFollow.addTarget(self, action: #selector(followClicked(_:)), for: .touchUpInside)
            feedItem.buttonMore.addTarget(self, action: #selector(moreClicked(_:)), for: .touchUpInside)
            feedItem.buttonLike.addTarget(self, action: #selector(likeClicked(_:)), for: .touchUpInside)
            feedItem.buttonLikeList.addTarget(self, action: #selector(likersClicked(_:)), for: .touchUpInside)
            feedItem.buttonTrack.addTarget(self, action: #selector(trackClicked(_:)), for: .touchUpInside)
            
            //            feedItem.content.didTouch = { (touchResult) in
            //                switch touchResult.state {
            //                case .began:
            //                    Logger.d("began")
            //                //                    Logger.d("began, \(touchResult)")
            //                case .ended:
            //                    Logger.d("ended")
            //                    if let linkResult = touchResult.linkResult {
            //                        if linkResult.detectionType == .url {
            //                            Logger.d("text: \(linkResult.text)")
            //                        }
            //                    }
            //                //                    Logger.d("ended, \(touchResult)")
            //                default:
            //                    break
            //                }
            //            }
        case .empty:
            cell = tableView.dequeueReusableCell(withIdentifier: "EmptyMsgCell", for: indexPath)
            cell.backgroundColor = UiUtility.colorBackgroundWhite
            cell.selectionStyle = .none
            
            let msg: UILabel = cell.viewWithTag(1) as! UILabel
            msg.textColor = UIColor(rgb: 0x4A4A4A)
            msg.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
            switch contentKind {
            case .post:
                msg.text = "no_post_yet".localized
            case .collection:
                msg.text = "no_collection_yet".localized
            case .all:
                msg.text = "no_post_yet".localized
            case .new:
                msg.text = "no_new_message".localized
            case .activity:
                msg.text = "no_new_activity".localized
                //            default:
                //                msg.text = nil
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        Logger.d("Person You tapped cell number \(indexPath.row).")
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch Section.init(rawValue: indexPath.section)! {
        case .feed:
            if let tabControllerDelegate = self.tabControllerDelegate {
                tabControllerDelegate.goNewsDetail(news: newsList[indexPath.row])
            } else {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedDetailViewController") as! FeedDetailViewController
                vc.news = newsList[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }
        default:
            break
        }
    }
    
    @objc private func userClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            let news: News = newsList[indexPath.row]
            if let nAccountId = news.accountId, let pAccountId = profile.accountId {
                if nAccountId == pAccountId {
                    //                    Logger.d("same profile, skip")
                    return
                }
                UiUtility.goBrowse(profile: Profile.init(with: news), controller: self)
            }
        }
    }
    
    @objc private func followClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            //            Logger.d("click follow at row \(String(describing: indexPath.row))")
            let news: News = newsList[indexPath.row]
            if let accountId = news.accountId {
                UserManager.shared().follow(accountId: accountId, set: true, listener: self)
            }
        }
    }
    
    @objc private func moreClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            //            Logger.d("click like at row \(String(describing: indexPath.row))")
            let news: News = newsList[indexPath.row]
            if news.offical != nil && news.offical! == Role.official.rawValue {
                // official
                UiUtility.moreMenu(from: self, type: 0, news: news, contentDelegate: self)
            } else if let newsAccoundId = news.accountId {
                if newsAccoundId == UserManager.shared().currentUser.accountId! {
                    // mine
                    UiUtility.moreMenu(from: self, type: 1, news: news, contentDelegate: self)
                } else {
                    // others
                    UiUtility.moreMenu(from: self, type: 2, news: news, contentDelegate: self)
                }
            }
        }
    }
    
    @objc private func likeClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            //            Logger.d("click like at row \(String(describing: indexPath.row))")
            let news: News = newsList[indexPath.row]
            NewsManager.shared().like(news: news, set: (news.likeStatus == 0), listener: self)
        }
    }
    
    @objc private func likersClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            //            Logger.d("click track at row \(String(describing: indexPath.row))")
            let news: News = newsList[indexPath.row]
            self.tabControllerDelegate?.goBrowseLikers(news: news)
        }
    }
    
    @objc private func trackClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableView)
        if let indexPath = self.tableView.indexPathForRow(at: buttonPosition) {
            //            Logger.d("click track at row \(String(describing: indexPath.row))")
            let news: News = newsList[indexPath.row]
            NewsManager.shared().track(news: news, set: (news.bookmarkStatus == 0), listener: self)
        }
    }
}
