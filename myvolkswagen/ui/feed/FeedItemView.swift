//
//  FeedItemView.swift
//  myvolkswagen
//
//  Created by Apple on 2/13/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Kingfisher

class FeedItemView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    weak var contentView: UIView!
    
    @IBOutlet weak var imgCollectionView: UICollectionView!
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var imageCheck: UIImageView!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var buttonUser: UIButton!
    @IBOutlet weak var buttonFollow: UIButton!
    @IBOutlet weak var buttonMore: UIButton!
    
    @IBOutlet weak var content: ContextLabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var footer: UIView!
    @IBOutlet weak var imageLike: UIImageView!
    @IBOutlet weak var countLike: UILabel!
    @IBOutlet weak var buttonLike: UIButton!
    @IBOutlet weak var buttonLikeList: UIButton!
    @IBOutlet weak var imageComment: UIImageView!
    @IBOutlet weak var countComment: UILabel!
    @IBOutlet weak var imageTrack: UIImageView!
    @IBOutlet weak var buttonTrack: UIButton!
    @IBOutlet weak var pageControl: CustomPageControl!
    @IBOutlet weak var pageControlWidth: NSLayoutConstraint!
    @IBOutlet weak var toolBarView: UIView!
    
    var images: [String] = []
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    private func commonInit() {
        contentView = (Bundle.main.loadNibNamed("FeedItemView", owner: self, options: nil)?.first as! UIView)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        // user image
        imageUser.backgroundColor = UiUtility.colorBackgroundWhite
        imageUser.layer.masksToBounds = true
        imageUser.layer.cornerRadius = UiUtility.adaptiveSize(size: 20)
        for constraint in contentView.constraints {
            if constraint.identifier == "marginTopUserImage" {
                constraint.constant = UiUtility.adaptiveSize(size: 20)
            }
        }
        
        // user name
        nameUser.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
        nameUser.textColor = UIColor.black
        
        // content
        content.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
        content.textColor = UiUtility.colorDarkGrey
        for constraint in contentView.constraints {
            if constraint.identifier == "marginTopContent" {
                constraint.constant = UiUtility.adaptiveSize(size: 70)
            }
        }
        content.foregroundColor = { (linkResult) in
            switch linkResult.detectionType {
            case .url:
                return UIColor(rgb: 0x007AFF)
            default:
                return UiUtility.colorDarkGrey
            }
        }
        
        content.underlineStyle = { (linkResult) in
            switch linkResult.detectionType {
            default:
                return NSUnderlineStyle.single
            }
        }
        
        content.didTouch = { (touchResult) in
            switch touchResult.state {
            case .began:
                break
            case .ended:
                if let linkResult = touchResult.linkResult {
                    if linkResult.detectionType == .phoneNumber {
                        if let url = URL(string: "telprompt://\(linkResult.text)") {
                            UIApplication.shared.open(url)
                        }
                    }
                }
            default:
                break
            }
        }
        
        content.modifiedAttributedString = { (attributedString) in

            if attributedString.string.hasSuffix("more".localized) {
                let ellipsisLength = "more".localized.count
                let _attributedString = NSMutableAttributedString(attributedString: attributedString)

                let text = attributedString.string

                let fromIdx = text.unicodeScalars.index(text.unicodeScalars.endIndex, offsetBy: -ellipsisLength)
                let toIdx = text.unicodeScalars.index(fromIdx, offsetBy: ellipsisLength)
                let nsRange = NSRange(fromIdx..<toIdx, in: text)

                _attributedString.addAttribute(.foregroundColor, value: UIColor.black, range: nsRange)
                _attributedString.addAttribute(.font, value: UIFont(name: "PingFangTC-Semibold", size: UiUtility.adaptiveSize(size: 15))!, range: nsRange)

                return _attributedString
            }

            return attributedString
        }
        
        // date
        for constraint in date.constraints {
            if constraint.identifier == "heightDate" {
                constraint.constant = UiUtility.adaptiveSize(size: 36)
            }
        }
        date.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
        date.textColor = UiUtility.colorBrownGrey
        
        // iamge
        image.backgroundColor = UiUtility.colorBackgroundWhite
        
        // footer
        for constraint in footer.constraints {
            if constraint.identifier == "heightFooter" {
                constraint.constant = UiUtility.adaptiveSize(size: 50)
            }
        }
        
        let footerTextColor: UIColor = UIColor(rgb: 0x4A4A4A)
        let footerTextFont: UIFont = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))!
        
        // like count
        countLike.textColor = footerTextColor
        countLike.font = footerTextFont
        
        // comment count
        countComment.textColor = footerTextColor
        countComment.font = footerTextFont
        
        imgCollectionView.register(UINib(nibName: "FeedImgCell", bundle: nil), forCellWithReuseIdentifier: "FeedImgCell")
        imgCollectionView.delegate = self
        imgCollectionView.dataSource = self
    }

    func setupUi(with news: News, isList: Bool, inPerson: Bool) {
        // image user
        if let stickerURL = news.stickerURL {
            let url = URL(string: stickerURL)
            imageUser.kf.setImage(with: url)
        }
        
        // image check
        imageCheck.isHidden = true
//        if let level = news.level {
//            if level > 2 {
//                imageCheck.isHidden = false
//                imageCheck.image = UIImage.init(named: "check_golden_large")
//            } else if level > 1 {
//                imageCheck.isHidden = false
//                imageCheck.image = UIImage.init(named: "check_silver_large")
//            } else if level > 0 {
//                imageCheck.isHidden = false
//                imageCheck.image = UIImage.init(named: "check_large")
//            }
//        }
        
        // username
        nameUser.text = news.nickname
        
        // more and follow buttons
        if inPerson {
            buttonMore.isHidden = false
            buttonFollow.isHidden = true
        } else if isList {
            if news.isMine() {
                buttonMore.isHidden = false
                buttonFollow.isHidden = true
            } else {
                buttonMore.isHidden = (news.isFollow == 0)
                buttonFollow.isHidden = !(news.isFollow == 0)
            }
        } else {
            // in detail
            buttonMore.isHidden = false
            buttonFollow.isHidden = true
        }
        
        // content
        self.content.text = news.content
        
        if !Utility.isEmpty(news.content) && (isList || inPerson) {
            if numberOfLinesNeeded(str: news.content!) > 4 {
                var lines: [String] = getLinesArrayOfString(in: self.content)
                let ellipsis: String = "more".localized
                if let lastChar: Character = lines[3].last {
                    if lastChar == "\n" {
                        lines[3].removeLast()
                    }
                }
                
                // add a custom ellipsis
                var newLine3: String = appendEllipsis(on: lines[3], ellpsis: ellipsis)

                // make lines[3] in one line
                while numberOfLinesNeeded(str: newLine3) > 1 {
                    lines[3].removeLast()
                    newLine3 = appendEllipsis(on: lines[3], ellpsis: ellipsis)
                }
                
                let all: String = "\(lines[0])\(lines[1])\(lines[2])\(newLine3)"
                self.content.text = all
            }
        }
        
        
        // date
        date.text = news.getDateText()
        
        // image
        if let photoList = news.photoList, photoList.count > 0 {
//            let url = URL(string: photoUrl)
//            image.kf.setImage(with: url)
            images = photoList
            pageControl.numberOfPages = images.count
            pageControl.isHidden = images.count == 1
            pageControlWidth.constant = CGFloat(20 * images.count)
            pageControl.currentPage = 0
            imgCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
            imgCollectionView.reloadData()
        }
        
        // like status
        imageLike.isHighlighted = (news.likeStatus == 1)
        // like count
        countLike.text = "\(news.likeAmount)"
        
        // comment status
        imageComment.isHighlighted = (news.commentStatus == 1)
        // comment count
        countComment.text = "\(news.commentAmount)"
        
        // track status
        imageTrack.isHighlighted = (news.bookmarkStatus == 1)
    }
    
    private func appendEllipsis(on text: String, ellpsis: String) -> String {
        if text.count == 0 {
            return ellpsis
        } else {
            if let lastChar: Character = text.last {
                if lastChar == " " {
                    return "\(text)\(ellpsis)"
                }
            }
        }
        return "\(text) \(ellpsis)"
    }
    
    private func numberOfLinesNeeded(str: String) -> Int {
        let width = UiUtility.adaptiveSize(size: 375 - 20 - 20)
        let oneLineHeight: CGFloat = "A".height(withConstrainedWidth: width, font: content.font!)
        let maxContentHeight: CGFloat = str.height(withConstrainedWidth: width, font: content.font!)
        return Int(nearbyint(maxContentHeight / oneLineHeight))
    }
    
    private func getLinesArrayOfString(in label: UILabel) -> [String] {
        /// An empty string's array
        var linesArray = [String]()
        
        guard let text = label.text, let font = label.font else {return linesArray}
        
        let rect = label.frame
        
        let myFont: CTFont = CTFontCreateWithName(font.fontName as CFString, font.pointSize, nil)
        let attStr = NSMutableAttributedString(string: text)
        attStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: myFont, range: NSRange(location: 0, length: attStr.length))
        
        let frameSetter: CTFramesetter = CTFramesetterCreateWithAttributedString(attStr as CFAttributedString)
        let path: CGMutablePath = CGMutablePath()
        path.addRect(CGRect(x: 0, y: 0, width: rect.size.width, height: 100000), transform: .identity)
        
        let frame: CTFrame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, nil)
        guard let lines = CTFrameGetLines(frame) as? [Any] else {return linesArray}
        
        for line in lines {
            let lineRef = line as! CTLine
            let lineRange: CFRange = CTLineGetStringRange(lineRef)
            let range = NSRange(location: lineRange.location, length: lineRange.length)
            let lineString: String = (text as NSString).substring(with: range)
            linesArray.append(lineString)
        }
        return linesArray
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedImgCell", for: indexPath) as! FeedImgCell
        cell.img.kf.setImage(with: URL(string: images[indexPath.item]))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collectionView.bounds.size.width
        return CGSize(width: w, height: w)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if pageControl.currentPage == indexPath.item {
            guard let visible = collectionView.visibleCells.first else { return }
            guard let index = collectionView.indexPath(for: visible)?.row else { return }
            pageControl.currentPage = index
        }
    }
}
