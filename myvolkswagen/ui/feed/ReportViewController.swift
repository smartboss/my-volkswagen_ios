//
//  ReportViewController.swift
//  myvolkswagen
//
//  Created by Apple on 2/28/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class ReportViewController: NaviViewController {
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelValue: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        naviBar.setTitle("report_post".localized)
        
        container.backgroundColor = UiUtility.colorBackgroundWhite
        
        labelTitle.textColor = UIColor.black
        labelTitle.font = UIFont(name: "PingFangTC-Medium", size: 14)
        labelTitle.text = "report_title".localized
        
        labelValue.textColor = UIColor(rgb: 0x4A4A4A)
        labelValue.font = UIFont(name: "PingFangTC-Regular", size: 14)
        labelValue.text = "report_msg".localized
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
