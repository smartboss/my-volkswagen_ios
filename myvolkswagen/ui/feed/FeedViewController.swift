//
//  FeedViewController.swift
//  myvolkswagen
//
//  Created by Apple on 2/1/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Firebase
import WebKit

enum Kind: Int {
    case polo = 0, golf, touran, tiguan, passat, sharan, beetle, others
    static var allCases: [Kind] {
        var values: [Kind] = []
        var index = 0
        while let element = self.init(rawValue: index) {
            values.append(element)
            index += 1
        }
        return values
    }

    func getText() -> String {
        switch self {
        case .polo:
            return "Polo"
        case .golf:
            return "Golf"
        case .touran:
            return "Touran"
        case .tiguan:
            return "Tiguan"
        case .passat:
            return "Passat"
        case .sharan:
            return "Sharan"
        case .beetle:
            return "Beetle"
        case .others:
            return "Others"
        }
    }
}

class CarModel {
    var kind: Kind
    var selected: Bool = false

    init(kind: Kind) {
        self.kind = kind
    }
}

class CarModel2 {
    var model: Model
    var selected: Bool = false
    
    init(model: Model) {
        self.model = model
    }
}

class FeedViewController: NaviViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate,  WKNavigationDelegate, WKUIDelegate {
    
    
    
    enum SectionFeed: Int {
        case paddingKind = 0, feed
        static var allCases: [SectionFeed] {
            var values: [SectionFeed] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    enum SectionClub: Int {
        case popular = 0, latest, kind, club, yours, post
        static var allCases: [SectionClub] {
            var values: [SectionClub] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    
    
    var newsKind: NewsKind = .all
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyText: UILabel!
    
    @IBOutlet weak var tableViewFeed: UITableView!
    @IBOutlet weak var FavPostContainer: UIView!
    @IBOutlet weak var MyPostContainer: UIView!
    @IBOutlet weak var followContainer: UIView!
    var cellHeightsDictionaryFeed: [IndexPath: CGFloat]!
    @IBOutlet weak var buttonAdd: UIButton!
    
    @IBOutlet weak var containerKind: UIView!
    @IBOutlet weak var collectionViewKind: UICollectionView!
    
    @IBOutlet weak var buttonShield: UIButton!
    @IBOutlet weak var extendedNavi: UIView!
    @IBOutlet weak var extendedNaviTitle0: UILabel!
    @IBOutlet weak var extendedNaviTitle1: UILabel!
    @IBOutlet weak var extendedNaviTitle2: UILabel!
    @IBOutlet weak var extendedNaviTitle3: UILabel!
    @IBOutlet weak var extendedNaviTitle4: UILabel!
    @IBOutlet weak var buttonAddCons: BasicConstraint!
    
    var carModels: [CarModel2] = []
    
    var newsList: [News] = []
    
    private let refreshControl = UIRefreshControl()
    
    // club
    @IBOutlet weak var viewClubContainer: UIView!
    @IBOutlet weak var viewClubSearchContainer: UIView!
    @IBOutlet weak var textFieldSearchClub: UITextField!
    @IBOutlet weak var constraintCreateClubLeading: NSLayoutConstraint!
    @IBOutlet weak var buttonExploreClub: UIButton!
    @IBOutlet weak var tableViewClub: UITableView!
    weak var collectionViewClubPopular: UICollectionView!
    weak var collectionViewClubLatest: UICollectionView!
    weak var collectionViewClubKind: UICollectionView!
    weak var collectionViewClubYours: UICollectionView!
    var cellHeightsDictionaryClub: [IndexPath: CGFloat]!
    
    var controllerfollow: FollowViewController!
    var controllerfavPosts: FavoritePostViewController!
    var controllerMyPosts: FavoritePostViewController!
    private let refreshControlClub = UIRefreshControl()
    
    var clubList: [Club] = []
    var clubYours: [Club] = []
    var clubNewsList: [News] = []
    var clubCarModels: [CarModel2] = []
    var clubSort = ClubSort.latestClub
    
    var clubSearchResult: [Club]?
    
    

    override func viewDidLoad() {
        hasTextField = true
        super.viewDidLoad()
        
        setNaviTitle()
        naviBar.setLeftButton(UIImage(named: "vw"), highlighted: UIImage(named: "vw"))
        naviBar.setRightButton(UIImage(named: "notif_black"), highlighted: UIImage(named: "notif_black_unread"))
        imageViewNotification = naviBar.imageViewRight
        naviBar.setArrowVisibility(true)
        
        emptyView.backgroundColor = UiUtility.colorBackgroundWhite
        emptyText.textColor = UIColor(rgb: 0x4A4A4A)
        emptyText.font = UIFont(name: "PingFangTC-Regular", size: 15)
        emptyText.text = "feed_no_content".localized
        
        containerKind.backgroundColor = UiUtility.colorBackgroundWhite
        collectionViewKind.backgroundColor = UiUtility.colorBackgroundWhite
        collectionViewKind.delaysContentTouches = false
        
        extendedNavi.backgroundColor = UiUtility.colorBackgroundWhite
        extendedNaviTitle0.textColor = UIColor.black
        extendedNaviTitle0.font = UIFont(name: "PingFangTC-Medium", size: 18)
        extendedNaviTitle1.textColor = UIColor.black
        extendedNaviTitle1.font = UIFont(name: "PingFangTC-Medium", size: 18)
        extendedNaviTitle2.textColor = UIColor.black
        extendedNaviTitle2.font = UIFont(name: "PingFangTC-Medium", size: 18)
        extendedNaviTitle3.textColor = UIColor.black
        extendedNaviTitle3.font = UIFont(name: "PingFangTC-Medium", size: 18)
        extendedNaviTitle4.textColor = UIColor.black
        extendedNaviTitle4.font = UIFont(name: "PingFangTC-Medium", size: 18)
        
        tableViewFeed.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        tableViewFeed.backgroundColor = UiUtility.colorBackgroundWhite
        cellHeightsDictionaryFeed = [:]
        tableViewFeed.rowHeight = UITableView.automaticDimension
        
        Analytics.logEvent("view_newsfeed_all", parameters: nil)
        
        // club
        viewClubContainer.isHidden = true
        viewClubContainer.backgroundColor = UiUtility.colorBackgroundWhite
        viewClubSearchContainer.layer.cornerRadius = UiUtility.adaptiveSize(size: 14)
        textFieldSearchClub.placeholder = "search_club".localized
        textFieldSearchClub.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 16))
//        textFieldSearchClub.inputAccessoryView = toolbar
        textFieldSearchClub.delegate = self
        tableViewClub.backgroundColor = UiUtility.colorBackgroundWhite
        cellHeightsDictionaryClub = [:]
        tableViewClub.rowHeight = UITableView.automaticDimension
        tableViewClub.refreshControl = refreshControlClub
        refreshControlClub.addTarget(self, action: #selector(refreshClub(_:)), for: .valueChanged)
        
        // collection posts
        // follow
        if let myId = UserManager.shared().currentUser.accountId {
            _ = UserManager.shared().getProfile(withId: myId, listener: self)
        }
        
        if UserManager.shared().showNoticeBoard == nil {
            UserManager.shared().getRecallCheckCampaign(listener: self)
        }
        
        NotificationCenter.default.addObserver(self,
            selector: #selector(applicationDidBecomeActive),
            name: UIApplication.didBecomeActiveNotification,
            object: nil)
        
        addFloatingBtnView()
        CarManager.shared().getCars(remotely: true, listener: nil)
    }
    
    override func didFinishGetProfile(success: Bool, profile: Profile?) {
        if success {
            if let _ = UserManager.shared().currentUser.accountId {
                // collection posts
                controllerfavPosts = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FavoritePostViewController") as! FavoritePostViewController
                controllerfavPosts.contentKind = .collection
                controllerfavPosts.profile = profile
                addChild(controllerfavPosts)
                controllerfavPosts.view.translatesAutoresizingMaskIntoConstraints = false
                FavPostContainer.addSubview(controllerfavPosts.view)

                NSLayoutConstraint.activate([
                    controllerfavPosts.view.leadingAnchor.constraint(equalTo: FavPostContainer.leadingAnchor),
                    controllerfavPosts.view.trailingAnchor.constraint(equalTo: FavPostContainer.trailingAnchor),
                    controllerfavPosts.view.topAnchor.constraint(equalTo: FavPostContainer.topAnchor),
                    controllerfavPosts.view.bottomAnchor.constraint(equalTo: FavPostContainer.bottomAnchor)
                ])

                controllerfavPosts.didMove(toParent: self)
                // follow
                controllerfollow = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FollowViewController") as! FollowViewController
                controllerfollow.hideLeftBtn = true
                controllerfollow.isFollower = false
                controllerfollow.of = profile
                addChild(controllerfollow)
                controllerfollow.view.translatesAutoresizingMaskIntoConstraints = false
                followContainer.addSubview(controllerfollow.view)

                NSLayoutConstraint.activate([
                    controllerfollow.view.leadingAnchor.constraint(equalTo: followContainer.leadingAnchor),
                    controllerfollow.view.trailingAnchor.constraint(equalTo: followContainer.trailingAnchor),
                    controllerfollow.view.topAnchor.constraint(equalTo: followContainer.topAnchor),
                    controllerfollow.view.bottomAnchor.constraint(equalTo: followContainer.bottomAnchor)
                ])

                controllerfollow.didMove(toParent: self)
                // my posts
                controllerMyPosts = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FavoritePostViewController") as! FavoritePostViewController
                controllerMyPosts.contentKind = .post
                controllerMyPosts.profile = profile
                addChild(controllerMyPosts)
                controllerMyPosts.view.translatesAutoresizingMaskIntoConstraints = false
                MyPostContainer.addSubview(controllerMyPosts.view)

                NSLayoutConstraint.activate([
                    controllerMyPosts.view.leadingAnchor.constraint(equalTo: MyPostContainer.leadingAnchor),
                    controllerMyPosts.view.trailingAnchor.constraint(equalTo: MyPostContainer.trailingAnchor),
                    controllerMyPosts.view.topAnchor.constraint(equalTo: MyPostContainer.topAnchor),
                    controllerMyPosts.view.bottomAnchor.constraint(equalTo: MyPostContainer.bottomAnchor)
                ])

                controllerMyPosts.didMove(toParent: self)
            }
        }
    }
    
    @objc func applicationDidBecomeActive() {
        if !didEnterBackground {
            return
        }
        didEnterBackground = false
        
        if let tabControllerDelegate = self.tabControllerDelegate {
            if tabControllerDelegate.getTabbarIndex() == 0 {
                if UserManager.shared().showNoticeBoard == nil {
                    UserManager.shared().getRecallCheckCampaign(listener: self)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if newsKind == .club {
            ClubManager.shared().getJoinedClubCount(remotely: false, listener: self)
        }
        
        if carModels.count == 0 {
            NewsManager.shared().getCarModel(remotely: true, listener: self)
        }
        
        // update notice read/unread status locally
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            self.setNoticeImage(with: readStatus)
        }
        
        if CarManager.shared().addCarFromMainPage {
            CarManager.shared().addCarFromMainPage = false
            self.tabControllerDelegate?.addPostClicked()
        }
        
        if NewsManager.shared().listIsDirtyRemote {
            NewsManager.shared().listIsDirtyRemote = false
            
            
            
            if newsKind == .club {
                if ClubManager.shared().getJoinedClubCount(remotely: false, listener: nil) != 0 {
                    ClubManager.shared().getClubData(remotely: true, listener: self)
                }
            } else if newsKind == .myPost {
                controllerMyPosts.refreshContent()
            } else if newsKind == .favoritePost {
                controllerfavPosts.refreshContent()
            } else if newsKind == .follow {
                controllerfollow.switchToTab0()
            } else {
                newsList = NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: false, listener: self)
                
                if ClubManager.shared().getJoinedClubCount(remotely: false, listener: nil) != 0 {
                    ClubManager.shared().getClubData(remotely: true, listener: nil)
                }
            }
        }
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        self.tabControllerDelegate?.menuClicked()
    }
    
    override func rightButtonClicked(_ button: UIButton) {
        self.tabControllerDelegate?.notificationClicked()
    }
    
    override func centerButtonClicked() {
        buttonShield.isHidden = false
        extendedNavi.isHidden = false
    }
    
    
    
    // MARK: keyboard
    
    @objc override func keyboardWillShow(notification: NSNotification) {
        keyboardShowing = true
    }
    
    @objc override func keyboardWillHide(notification: NSNotification) {
        keyboardShowing = false
    }
    
    
    
    // MARK: text field delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if let text = textField.text {
            let trimmedText = text.trimmingCharacters(in: .whitespaces)
            if trimmedText.count != 0 {
                ClubManager.shared().search(sort: self.clubSort, keyword: trimmedText, listener: self)
            } else {
                clubSearchResult = nil
                ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(), listener: self)
            }
        } else {
            clubSearchResult = nil
            ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(), listener: self)
        }
        
        return true
    }
    
    
    
    
    override func updateUnreadCount(notification: NSNotification) {
//        Logger.d("feed updateUnreadCount")
        // update notice read/unread status locally
        if let readStatus = NoticeManager.shared().getUnreadCount(fromLocal: true, listener: nil) {
            self.setNoticeImage(with: readStatus)
        }
    }
    
    
    
    // MARK: context label delegate
    
//    func contextLabel(_ sender: ContextLabel, didTouchWithTouchResult touchResult: TouchResult) {
//        switch touchResult.state {
//        case .began:
//            Logger.d("began, touchResult: \(touchResult)")
//        case .ended:
//            Logger.d("ended, touchResult: \(touchResult)")
//        default:
//            break
//        }
//    }
    
    
    
    // MARK: image picker controller delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            Logger.d("No image found")
            return
        }
        
        let thumb = image.resized(toWidth: 1080.0)!
        // print out the image size as a test
//        Logger.d("image size: \(image.size)")
        
        if picker.sourceType == .camera {
            UiUtility.goEditPost(from: self, post: Post.init(with: [info[UIImagePickerController.InfoKey.editedImage] as! UIImage]), contentDelegate: nil, photoSourceType: .camera)
        } else {
            UiUtility.goEditPhotos(from: self, post: Post.init(with: [thumb]), contentDelegate: nil)
        }
    }
    
    
    
    // MARK: club sort delegate
    
    override func setSort(type: ClubSort) {
        if clubSort != type {
            clubSort = type
//            ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(forFeed: false), listener: self)
            
            if let text = textFieldSearchClub.text {
                let trimmedText = text.trimmingCharacters(in: .whitespaces)
                if trimmedText.count != 0 {
                    ClubManager.shared().search(sort: self.clubSort, keyword: trimmedText, listener: self)
                } else {
                    clubSearchResult = nil
                    ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(), listener: self)
                }
            } else {
                clubSearchResult = nil
                ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(), listener: self)
            }
        }
    }
    
    
    
    // MARK: club protocol
    
    override func didStartGetJoinedClubCount() {
        super.didStartGetJoinedClubCount()
    }
    
    override func didFinishGetJoinedClubCount(success: Bool, count: Int?) {
        super.didFinishGetJoinedClubCount(success: success, count: count)
        
        if count == nil {
            Logger.d("failed to get joined club count remotely")
        } else {
            if refreshControlClub.isRefreshing {
                refreshControlClub.endRefreshing()
            }
            
            if newsKind == .club {
                setClubView(show: true)
            } else {
                switchMode(kind: .club)
            }
        }
    }
    
    override func didStartGetGuestData() {
        super.didStartGetGuestData()
    }
    
    override func didFinishGetGuestData(success: Bool, guestClubData: GuestClubData?) {
        super.didFinishGetGuestData(success: success, guestClubData: guestClubData)
        
        if success {
            ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(forFeed: false), listener: self)
            
            Logger.d("reload table view after didFinishGetGuestData")
            tableViewClub.reloadData()
        }
    }
    
    override func didStartGetGuestClubs() {
        super.didStartGetGuestClubs()
    }
    
    override func didFinishGetGuestClubs(success: Bool, guestClubs: [Club]?) {
        super.didFinishGetGuestClubs(success: success, guestClubs: guestClubs)
        
        if success {
            Logger.d("reload table view after didFinishGetGuestClubs")
//            tableViewClub.reloadSections([SectionClub.club.rawValue], with: .automatic)
            tableViewClub.reloadData()
        }
    }
    
    override func didStartGetClubData() {
        super.didStartGetClubData()
    }
    
    override func didFinishGetClubData(success: Bool, clubData: ClubData?) {
        super.didFinishGetClubData(success: success, clubData: clubData)
        
        if success {
//            Logger.d("reload table view after didFinishGetClubData, clubData: \(clubData)")
            tableViewClub.reloadData()
        }
    }
    
    override func didStartSearchClub() {
        super.didStartSearchClub()
    }
    
    override func didFinishSearchClub(success: Bool, result: [Club]) {
        super.didFinishSearchClub(success: success, result: result)
        
        if success {
            clubSearchResult = result
            tableViewClub.reloadData()
        }
    }
    
    override func didFinishMemberAdd(success: Bool) {
        super.didFinishMemberAdd(success: success)
        
        if success {
            ClubManager.shared().cleanCache()
            ClubManager.shared().getJoinedClubCount(remotely: true, listener: self)
        } else {
            Logger.d("failed to member add")
        }
    }
    
    override func didFinishLikeClubPost(success: Bool, post: ClubPost, set: Bool) {
        dismissHud()
        
        if success {
            if let posts = ClubManager.shared().getClubData(remotely: false, listener: nil)?.news {
                var index = 0
                for n in posts {
                    if n.newsId! == post.newsId! {
                        if set {
                            n.likeStatus = 1
                            n.likeAmount = n.likeAmount + 1
                        } else {
                            n.likeStatus = 0
                            n.likeAmount = n.likeAmount - 1
                        }
                        updateClubUi()
                        break
                    }
                    index += 1
                }
            }
        }
    }
    
    override func didFinishTrackClubPost(success: Bool, post: ClubPost, set: Bool) {
        dismissHud()
        
        if success {
            if let posts = ClubManager.shared().getClubData(remotely: false, listener: nil)?.news {
                for n in posts {
                    if n.newsId! == post.newsId! {
                        if set {
                            n.bookmarkStatus = 1
                            n.bookmarkAmount = n.bookmarkAmount + 1
                        } else {
                            n.bookmarkStatus = 0
                            n.bookmarkAmount = n.bookmarkAmount - 1
                        }
                        updateClubUi()
                        break
                    }
                }
            }
        }
    }
    
    override func didFinishSetTop(success: Bool, post: ClubPost, set: Bool) {
        super.didFinishSetTop(success: success, post: post, set: set)
        
        if success {
            if let posts = ClubManager.shared().getClubData(remotely: false, listener: nil)?.news {
                for n in posts {
                    if n.newsId! == post.newsId! {
                        n.isTop = (set ? 1 : 0)
                    } else if n.clubId == post.clubId {
                        if set {
                            n.isTop = 0
                        }
                        
                    }
                }
                
                updateClubUi()
            }
        }
    }
    
    override func didFinishDeleteClubPost(success: Bool, post: ClubPost?) {
        super.didFinishDeleteClubPost(success: success, post: post)
        
        ClubManager.shared().getJoinedClubCount(remotely: true, listener: self)
    }
    
//    override func didFinishEditClubPost(success: Bool, newsId: Int?) {
//        super.didFinishEditClubPost(success: success, newsId: newsId)
//
//        if success {
//            ClubManager.shared().getJoinedClubCount(remotely: true, listener: self)
//        }
//    }
    
    
    
    // MARK: user protocol
    
    override func didStartFollow() {
        showHud()
    }
    
    override func didFinishFollow(success: Bool, accountId: String, set: Bool) {
        dismissHud()
        
        if success {
            for n in newsList {
                if let na = n.accountId {
                    if na == accountId {
                        if set {
                            n.isFollow = 1
                        } else {
                            n.isFollow = 0
                        }
                    }
                }
            }
            
            
            if newsKind == .club {
                if ClubManager.shared().getJoinedClubCount(remotely: false, listener: self) != 0 {
                    if let clubData = ClubManager.shared().getClubData(remotely: false, listener: self) {
                        for post in clubData.news {
                            if let na = post.accountId {
                                if na == accountId {
                                    if set {
                                        post.isFollow = 1
                                    } else {
                                        post.isFollow = 0
                                    }
                                }
                            }
                        }
                        
                        updateClubUi()
                    }
                }
            } else if newsKind == .follow {
                controllerfollow.switchToTab0()
            } else if newsKind == .myPost {
                controllerMyPosts.refreshContent()
            } else if newsKind == .favoritePost {
                controllerfavPosts.refreshContent()
            } else if newsKind == .myPost {
                // test
            } else {
                updateFeedUi()
            }
        }
    }
    
    override func didStartRecallCheck() {
    }
    
    override func didFinishRecallCheck(success: Bool, showNoticeBoard: Bool, recellUrl: String?) {
        if success && showNoticeBoard {
            if recellUrl != nil {
                showRecallWebview(url: recellUrl)
            }
        }
    }
    
    func showRecallWebview(url: String?) {
        let mybgview = UIView(frame: CGRect(x: 0, y: 0, width: UiUtility.getScreenWidth(), height: UiUtility.getScreenHeight()))
        mybgview.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        mybgview.tag = 80
        let myview = UIView(frame: CGRect(x: 0, y: 0, width: UiUtility.getScreenWidth(), height: UiUtility.getScreenHeight()))
        myview.backgroundColor = UIColor.white
        let wkw = WKWebView(frame: CGRect(x: 0, y: 0, width: UiUtility.getScreenWidth(), height: UiUtility.getScreenHeight()))
        
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        btn.setBackgroundImage(UIImage(named: "clear"), for: .normal)
        btn.addTarget(self, action: #selector(removeWebview), for: .touchUpInside)
        
        guard let myurl = URL(string: url!) else { return }
        UIApplication.shared.keyWindow?.rootViewController?.view.addSubview(mybgview)
        mybgview.addSubview(myview)
        myview.addSubview(wkw)
        myview.addSubview(btn)
        
        mybgview.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
        
        myview.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(CGFloat(100).getFitSize())
            make.bottom.equalToSuperview().offset(CGFloat(-100).getFitSize())
        }
        
        wkw.load(URLRequest(url: myurl))
        wkw.navigationDelegate = self
        wkw.uiDelegate = self
        wkw.snp.makeConstraints { make in
            make.trailing.top.leading.bottom.equalToSuperview()
        }
        
        btn.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(CGFloat(8).getFitSize())
            make.width.height.equalTo(CGFloat(45).getFitSize())
        }
    }
    
    @objc func removeWebview() {
        UserManager.shared().readRecallCampaign() // 已讀蓋板訊息
        
        if let myrootview = UIApplication.shared.keyWindow?.rootViewController?.view! {
            for vv in myrootview.subviews {
                if vv.tag == 80 {
                    vv.removeFromSuperview()
                }
            }
        }
    }
    
    // MARK: car protocol
    
    override func didStartGetCar() {
        showHud()
    }
    
    override func didFinishGetCar(success: Bool, cars: [Car]) {
        dismissHud()
        
        if success {
            if cars.count == 0 {
                self.tabControllerDelegate?.addPostClicked()
            } else {
                // add new post
                UiUtility.pickImage(from: self, delegate: self, allowsEditing: false)
            }
        }
    }
    
    
    
    // MARK: news protocol
    
    override func didStartGetNewsCarModel() {
        showHud()
    }
    
    override func didFinishGetNewsCarModel(success: Bool, carModels: [Model]?) {
        dismissHud()
        
        if success {
//            Logger.d("get carModels: \(String(describing: carModels))")
            
            if let carModels = carModels {
                self.carModels = []
                self.clubCarModels = []
                for model in carModels {
                    let carModel = CarModel2.init(model: model)
                    self.carModels.append(carModel)
                    
                    let clubCarModel = CarModel2.init(model: model)
                    self.clubCarModels.append(clubCarModel)
                }
                
                self.collectionViewKind.reloadData()
                if self.newsList.count == 0 || NewsManager.shared().listIsDirtyRemote {
                    NewsManager.shared().listIsDirtyRemote = false
                    newsList = NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: false, listener: self)
                } else if NewsManager.shared().listIsDirty {
                    NewsManager.shared().listIsDirty = false
                    updateFeedUi()
                }
                
                self.collectionViewClubKind?.reloadData()
            }
        }
    }
    
    override func didStartGetNewsList(loadMore: Bool) {
        if !loadMore {
            showHud()
        }
    }
    
    override func didFinishGetNewsList(success: Bool, loadMore: Bool, newsList: [News]?, newsKind: NewsKind) {
        if !loadMore {
            dismissHud()
        }
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
        
        if success {
            contentIsDirtyRemote = false
            if newsKind == self.newsKind {
                if let newsList = newsList {
                    self.newsList = newsList
                    updateFeedUi()
                }
            }
        }
    }
    
    override func didStartLikeNews() {
        showHud()
    }
    
    override func didFinishLikeNews(success: Bool, news: News, set: Bool) {
        dismissHud()
        
        if success {
            var index = 0
            for n in newsList {
                if n.newsId! == news.newsId! {
                    if set {
                        n.likeStatus = 1
                        n.likeAmount = n.likeAmount + 1
                    } else {
                        n.likeStatus = 0
                        n.likeAmount = n.likeAmount - 1
                    }
                    updateFeedUi()
                    break
                }
                index += 1
            }
        }
    }
    
    override func didStartTrackNews() {
        showHud()
    }
    
    override func didFinishTrackNews(success: Bool, news: News, set: Bool) {
        dismissHud()
        
        if success {
            for n in newsList {
                if n.newsId! == news.newsId! {
                    if set {
                        n.bookmarkStatus = 1
                        n.bookmarkAmount = n.bookmarkAmount + 1
                    } else {
                        n.bookmarkStatus = 0
                        n.bookmarkAmount = n.bookmarkAmount - 1
                    }
                    updateFeedUi()
                    break
                }
            }
        }
    }
    
    override func didStartReportNews() {
        showHud()
    }
    
    override func didFinishReportNews(success: Bool, news: News, type: Int) {
        dismissHud()
        
        if success {
            self.tabControllerDelegate?.goFinishReport()
        }
    }
    
    override func didStartDeletePost() {
        showHud()
    }
    
    override func didFinishDeletePost(success: Bool, news: News?) {
        dismissHud()
        
        if success {
            // TODO: you can do it better
            NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: false, listener: self)
        }
    }
    
    
    
    
    // MARK: collection view delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewKind {
            return carModels.count
        } else if collectionView == collectionViewClubPopular {
            if let guestClubData = ClubManager.shared().getGuestData(remotely: false, listener: self) {
                return guestClubData.hotClubs.count
            }
            return 0
        } else if collectionView == collectionViewClubLatest {
            if let guestClubData = ClubManager.shared().getGuestData(remotely: false, listener: self) {
                return guestClubData.newClubs.count
            }
            return 0
        } else if collectionView == collectionViewClubKind {
            return clubCarModels.count
        } else if collectionView == collectionViewClubYours {
            if let clubData = ClubManager.shared().getClubData(remotely: false, listener: self) {
                return clubData.clubs.count
            }
            return 0
        } else {
            Logger.d("numberOfItemsInSection should not go here")
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == collectionViewKind {
            return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        } else if collectionView == collectionViewClubPopular {
            let paddingH = UiUtility.adaptiveSize(size: 20)
            return UIEdgeInsets(top: .zero, left: paddingH, bottom: .zero, right: paddingH)
        } else if collectionView == collectionViewClubLatest {
            let paddingH = UiUtility.adaptiveSize(size: 20)
            return UIEdgeInsets(top: .zero, left: paddingH, bottom: .zero, right: paddingH)
        } else if collectionView == collectionViewClubKind {
            let paddingH = UiUtility.adaptiveSize(size: 20)
            return UIEdgeInsets(top: .zero, left: paddingH, bottom: .zero, right: paddingH)
        } else if collectionView == collectionViewClubYours {
            let paddingH = UiUtility.adaptiveSize(size: 20)
            return UIEdgeInsets(top: .zero, left: paddingH, bottom: .zero, right: paddingH)
        } else {
            Logger.d("insetForSectionAt should not go here")
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == collectionViewKind {
            return 6
        } else if collectionView == collectionViewClubPopular {
            return UiUtility.adaptiveSize(size: 6)
        } else if collectionView == collectionViewClubLatest {
            return UiUtility.adaptiveSize(size: 6)
        } else if collectionView == collectionViewClubKind {
            return UiUtility.adaptiveSize(size: 6)
        } else if collectionView == collectionViewClubYours {
            return UiUtility.adaptiveSize(size: 6)
        } else {
            Logger.d("minimumInteritemSpacingForSectionAt should not go here")
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionViewKind {
            let height: CGFloat = 34
            return CGSize(width: carModels[indexPath.row].model.name!.width(withConstrainedHeight: height, font: UIFont(name: "PingFangTC-Medium", size: 16)!) + 32, height: height)
        } else if collectionView == collectionViewClubPopular {
            return CGSize.init(width: UiUtility.adaptiveSize(size: 150), height: UiUtility.adaptiveSize(size: 149))
        } else if collectionView == collectionViewClubLatest {
            return CGSize.init(width: UiUtility.adaptiveSize(size: 150), height: UiUtility.adaptiveSize(size: 149))
        } else if collectionView == collectionViewClubKind {
            let height: CGFloat = UiUtility.adaptiveSize(size: 30)
            let modelName = clubCarModels[indexPath.row].model.name!
            let modelNameWidth = modelName.width(withConstrainedHeight: height, font: UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))!)
            return CGSize(width: modelNameWidth + UiUtility.adaptiveSize(size: 32), height: height)
        } else if collectionView == collectionViewClubYours {
            return CGSize.init(width: UiUtility.adaptiveSize(size: 150), height: UiUtility.adaptiveSize(size: 149))
        } else {
            Logger.d("sizeForItemAt should not go here")
            return .zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell!
        
        if collectionView == collectionViewKind {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KindCell", for: indexPath)
            cell.clipsToBounds = false
            
            let viewContainer: UIView = cell.viewWithTag(2)!
            let lbName: UILabel = cell.viewWithTag(3) as! UILabel
            
            let carModel = carModels[indexPath.row]
            
            viewContainer.layer.cornerRadius = 17
            viewContainer.layer.shadowColor = UIColor.black.cgColor
            viewContainer.layer.shadowOpacity = 0.3
            viewContainer.layer.shadowOffset = CGSize(width: 0, height: 1)
            viewContainer.layer.shadowRadius = UiUtility.adaptiveSize(size: 1.0)
            
            lbName.font = UIFont(name: "PingFangTC-Medium", size: 16)
            lbName.text = carModel.model.name
            
            if carModel.selected {
                lbName.textColor = .white
                viewContainer.backgroundColor = UiUtility.getModelSelectedColor(of: indexPath.row)
            } else {
                lbName.textColor = UiUtility.colorBrownGrey
                viewContainer.backgroundColor = .white
            }
        } else if collectionView == collectionViewClubPopular {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClubCell", for: indexPath)
            cell.clipsToBounds = false
            
            let ivPhoto: UIImageView = cell.viewWithTag(1) as! UIImageView
            let lbName: UILabel = cell.viewWithTag(2) as! UILabel
            
            ivPhoto.backgroundColor = UiUtility.colorBackgroundIncomingMessage
            ivPhoto.layer.cornerRadius = UiUtility.adaptiveSize(size: 6)
            
            for constraint in lbName.constraints {
                if constraint.identifier == "name_width" {
                    constraint.constant = UiUtility.adaptiveSize(size: 150)
                }
            }
            
            let club = ClubManager.shared().getGuestData(remotely: false, listener: self)!.hotClubs[indexPath.row]
            
            if let photoUrl = club.picUrl {
                let url = URL(string: photoUrl)
                ivPhoto.kf.setImage(with: url)
            }
            
            lbName.font = UIFont(name: "PingFangTC-Medium", size: 12)
            lbName.textColor = UiUtility.colorDarkGrey
            lbName.text = "test_club_name".localized
            lbName.text = club.name
        } else if collectionView == collectionViewClubLatest {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClubCell", for: indexPath)
            cell.clipsToBounds = false

            let ivPhoto: UIImageView = cell.viewWithTag(1) as! UIImageView
            let lbName: UILabel = cell.viewWithTag(2) as! UILabel

            ivPhoto.backgroundColor = UiUtility.colorBackgroundIncomingMessage
            ivPhoto.layer.cornerRadius = UiUtility.adaptiveSize(size: 6)

            for constraint in lbName.constraints {
                if constraint.identifier == "name_width" {
                    constraint.constant = UiUtility.adaptiveSize(size: 150)
                }
            }
            
            let club = ClubManager.shared().getGuestData(remotely: false, listener: self)!.newClubs[indexPath.row]
            
            if let photoUrl = club.picUrl {
                let url = URL(string: photoUrl)
                ivPhoto.kf.setImage(with: url)
            }

            lbName.font = UIFont(name: "PingFangTC-Medium", size: 12)
            lbName.textColor = UiUtility.colorDarkGrey
            lbName.text = club.name
        } else if collectionView == collectionViewClubKind {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KindCell", for: indexPath)
            cell.clipsToBounds = false
//            cell.backgroundColor = .red
            
            let viewContainer: UIView = cell.viewWithTag(2)!
            let lbName: UILabel = cell.viewWithTag(3) as! UILabel

            let carModel = clubCarModels[indexPath.row]

            viewContainer.layer.cornerRadius = UiUtility.adaptiveSize(size: 15)
            viewContainer.layer.shadowColor = UIColor.black.cgColor
            viewContainer.layer.shadowOpacity = 0.3
            viewContainer.layer.shadowOffset = CGSize(width: 0, height: 1)
            viewContainer.layer.shadowRadius = UiUtility.adaptiveSize(size: 1.0)
            
            lbName.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
            lbName.text = carModel.model.name

            if carModel.selected {
                lbName.textColor = .white
                viewContainer.backgroundColor = UiUtility.getModelSelectedColor(of: indexPath.row)
            } else {
                lbName.textColor = UiUtility.colorBrownGrey
                viewContainer.backgroundColor = .white
            }
        } else if collectionView == collectionViewClubYours {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClubCell", for: indexPath)
            cell.clipsToBounds = false
            
            let ivPhoto: UIImageView = cell.viewWithTag(1) as! UIImageView
            let lbName: UILabel = cell.viewWithTag(2) as! UILabel
            
            ivPhoto.backgroundColor = UiUtility.colorBackgroundIncomingMessage
            ivPhoto.layer.cornerRadius = UiUtility.adaptiveSize(size: 6)
            
            for constraint in lbName.constraints {
                if constraint.identifier == "name_width" {
                    constraint.constant = UiUtility.adaptiveSize(size: 150)
                }
            }
            
            let club = ClubManager.shared().getClubData(remotely: false, listener: self)!.clubs[indexPath.row]
            
            if let photoUrl = club.picUrl {
                let url = URL(string: photoUrl)
                ivPhoto.kf.setImage(with: url)
            }
            
            lbName.font = UIFont(name: "PingFangTC-Medium", size: 12)
            lbName.textColor = UiUtility.colorDarkGrey
            lbName.text = club.name
        } else {
            Logger.d("cellForItemAt should not go here")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewKind {
            let carModel: CarModel2 = carModels[indexPath.row]
            carModel.selected = !carModel.selected
            if carModel.selected {
                let tag: String = "tag_\(carModel.model.name!.lowercased())"
                Analytics.logEvent("newsfeed_tag", parameters: ["tag_name": tag as NSObject])
            }
            collectionViewKind.reloadData()
            NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: false, listener: self)
        } else if collectionView == collectionViewClubPopular {
            self.tabControllerDelegate?.goClub(ClubManager.shared().getGuestData(remotely: false, listener: self)!.hotClubs[indexPath.row])
        } else if collectionView == collectionViewClubLatest {
            self.tabControllerDelegate?.goClub(ClubManager.shared().getGuestData(remotely: false, listener: self)!.newClubs[indexPath.row])
        } else if collectionView == collectionViewClubKind {
            let carModel: CarModel2 = clubCarModels[indexPath.row]
            carModel.selected = !carModel.selected
            collectionViewClubKind.reloadData()
            
            ClubManager.shared().getGuestClubs(sort: clubSort, models: getModelArrayText(forFeed: false), listener: self)
        } else if collectionView == collectionViewClubYours {
            self.tabControllerDelegate?.goClub(ClubManager.shared().getClubData(remotely: false, listener: self)!.clubs[indexPath.row])
        }
    }




    // MARK: table view delegate

    func numberOfSections(in tableView: UITableView) -> Int {
        if newsKind == .club {
            return SectionClub.allCases.count
        } else {
            return SectionFeed.allCases.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if newsKind == .club {
            switch SectionClub.init(rawValue: section)! {
            case .popular:
                if hasJoinedClub() {
                    return 0
                } else {
                    if clubSearchResult != nil {
                        return 0
                    }
                    
                    if let guestClubInfo = ClubManager.shared().getGuestData(remotely: false, listener: self) {
                        if guestClubInfo.hotClubs.count == 0 {
                            return 0
                        } else {
                            return 1
                        }
                    } else {
                        return 0
                    }
                }
            case .latest:
                if hasJoinedClub() {
                    return 0
                } else {
                    if clubSearchResult != nil {
                        return 0
                    }
                    
                    if let guestClubInfo = ClubManager.shared().getGuestData(remotely: false, listener: self) {
                        if guestClubInfo.newClubs.count == 0 {
                            return 0
                        } else {
                            return 1
                        }
                    } else {
                        return 0
                    }
                }
            case .kind:
                return hasJoinedClub() ? 0 : 1
            case .club:
                if hasJoinedClub() {
                    return 0
                } else {
                    if let clubSearchResult = clubSearchResult {
                        return clubSearchResult.count
                    } else {
                        if let guestClubs = ClubManager.shared().getCachedGuestClubs() {
                            return guestClubs.count
                        } else {
                            return 0
                        }
                    }
                }
            case .yours:
                if hasJoinedClub() {
                    if let clubData = ClubManager.shared().getClubData(remotely: false, listener: self) {
                        if clubData.clubs.count == 0 {
                            return 0
                        } else {
                            return 1
                        }
                    } else {
                        return 0
                    }
                } else {
                    return 0
                }
            case .post:
                if hasJoinedClub() {
                    if let clubData = ClubManager.shared().getClubData(remotely: false, listener: self) {
                        return clubData.news.count
                    } else {
                        return 0
                    }
                } else {
                    return 0
                }
            }
        } else {
            switch SectionFeed.init(rawValue: section)! {
            case .paddingKind:
                return 1
            case .feed:
                if newsList.count != 0 {
                    emptyView.isHidden = true
                    return newsList.count
                } else {
                    emptyView.isHidden = false
                    return 0
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if newsKind == .club {
            switch SectionClub.init(rawValue: indexPath.section)! {
            case .popular, .latest:
                return UiUtility.adaptiveSize(size: 194)
            case .kind:
                if clubSearchResult != nil {
                    return UiUtility.adaptiveSize(size: 28)
                }
                return UiUtility.adaptiveSize(size: 78)
            case .club:
                return UiUtility.adaptiveSize(size: 80)
            case .yours:
                return UiUtility.adaptiveSize(size: 214)
            case .post:
                if hasJoinedClub() {
                    if let clubData = ClubManager.shared().getClubData(remotely: false, listener: self) {
                        return self.calculateNewsCellHeight(news: clubData.news[indexPath.row])
                    } else {
                        return 0
                    }
                } else {
                    return 0
                }
                return hasJoinedClub() ? self.calculateNewsCellHeight(news: newsList[indexPath.row]) : 0
            }
        } else {
            switch SectionFeed.init(rawValue: indexPath.section)! {
            case .paddingKind:
                return 44
            case .feed:
                return self.calculateNewsCellHeight(news: newsList[indexPath.row])
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if newsKind == .club {
            
        } else {
            if indexPath.section == SectionFeed.feed.rawValue {
                if newsList.count != 0 {
                    let lastElement = newsList.count - 1
                    if indexPath.row == lastElement {
                        if !NewsManager.shared().isListEndReached() {
                            // handle your logic here to get more items, add it to dataSource and reload tableview
//                            Logger.d("should try load more")
                            NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: true, listener: self)
                        }
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if newsKind == .club {
            cellHeightsDictionaryClub[indexPath] = cell.frame.size.height
        } else {
            cellHeightsDictionaryFeed[indexPath] = cell.frame.size.height
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if newsKind == .club {
            if let h: CGFloat = cellHeightsDictionaryClub[indexPath] {
                return h
            } else {
                return UITableView.automaticDimension
            }
        } else {
            if let h:CGFloat = cellHeightsDictionaryFeed[indexPath] {
                return h
            } else {
                return UITableView.automaticDimension
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        Logger.d("tableView cellForRowAt \(indexPath), newsKind: \(newsKind)")
        var cell: UITableViewCell!
        
        if newsKind == .club {
            switch SectionClub.init(rawValue: indexPath.section)! {
            case .popular:
                cell = tableView.dequeueReusableCell(withIdentifier: "CollectionCell", for: indexPath)
                let ivIcon: UIImageView = cell.viewWithTag(1) as! UIImageView
                let lbTitle: UILabel = cell.viewWithTag(2) as! UILabel
                let collectionView: UICollectionView = cell.viewWithTag(3) as! UICollectionView
                ivIcon.image = UIImage.init(named: "hotGroup")
                lbTitle.textColor = UiUtility.colorDark4A
                lbTitle.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
                lbTitle.text = "popular_club".localized
                
                collectionViewClubPopular = collectionView
                collectionView.backgroundColor = UiUtility.colorBackgroundWhite
                collectionView.delegate = self
                collectionView.dataSource = self
                
                collectionView.reloadData()
            case .latest:
                cell = tableView.dequeueReusableCell(withIdentifier: "CollectionCell", for: indexPath)
                let ivIcon: UIImageView = cell.viewWithTag(1) as! UIImageView
                let lbTitle: UILabel = cell.viewWithTag(2) as! UILabel
                let collectionView: UICollectionView = cell.viewWithTag(3) as! UICollectionView
                ivIcon.image = UIImage.init(named: "latestGroup")
                lbTitle.textColor = UiUtility.colorDark4A
                lbTitle.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
                lbTitle.text = "new_club".localized
                
                collectionViewClubLatest = collectionView
                collectionView.backgroundColor = UiUtility.colorBackgroundWhite
                collectionView.delegate = self
                collectionView.dataSource = self
                
                collectionView.reloadData()
            case .yours:
                cell = tableView.dequeueReusableCell(withIdentifier: "CollectionYoursCell", for: indexPath)
                let ivIcon: UIImageView = cell.viewWithTag(1) as! UIImageView
                let lbTitle: UILabel = cell.viewWithTag(2) as! UILabel
                let collectionView: UICollectionView = cell.viewWithTag(3) as! UICollectionView
                let btnBrowseAll: UIButton = cell.viewWithTag(4) as! UIButton
                
                ivIcon.image = UIImage.init(named: "urGroup")
                lbTitle.textColor = UiUtility.colorDark4A
                lbTitle.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
                lbTitle.text = "your_club".localized
                
                collectionViewClubYours = collectionView
                collectionView.backgroundColor = UiUtility.colorBackgroundWhite
                collectionView.delegate = self
                collectionView.dataSource = self
                
                collectionView.reloadData()
                
                btnBrowseAll.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
                btnBrowseAll.setTitleColor(UiUtility.colorBackgroundOutgoingMessage, for: .normal)
                btnBrowseAll.setTitle("browse_all_club".localized, for: .normal)
                btnBrowseAll.addTarget(self, action: #selector(browseAllClubClicked(_:)), for: .touchUpInside)
                
            case .kind:
                if clubSearchResult == nil {
                    cell = tableView.dequeueReusableCell(withIdentifier: "KindCell", for: indexPath)
                    let lbBrowse: UILabel = cell.viewWithTag(1) as! UILabel
                    let btnSort: UIButton = cell.viewWithTag(2) as! UIButton
                    let collectionView: UICollectionView = cell.viewWithTag(3) as! UICollectionView
                    
                    lbBrowse.textColor = UiUtility.colorDark4A
                    lbBrowse.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
                    lbBrowse.text = "browse_all_club".localized
                    
                    btnSort.setTitle("sort".localized, for: .normal)
                    btnSort.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
                    btnSort.setTitleColor(UiUtility.colorBackgroundOutgoingMessage, for: .normal)
                    btnSort.addTarget(self, action: #selector(sortClubClicked(_:)), for: .touchUpInside)
                    
                    collectionViewClubKind = collectionView
                    collectionView.backgroundColor = UiUtility.colorBackgroundWhite
                    collectionView.delegate = self
                    collectionView.dataSource = self
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "SortCell", for: indexPath)
                    let lbBrowse: UILabel = cell.viewWithTag(1) as! UILabel
                    let btnSort: UIButton = cell.viewWithTag(2) as! UIButton
                    
                    lbBrowse.textColor = UiUtility.colorDark4A
                    lbBrowse.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
                    lbBrowse.text = "browse_all_club".localized
                    
                    btnSort.setTitle("sort".localized, for: .normal)
                    btnSort.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
                    btnSort.setTitleColor(UiUtility.colorBackgroundOutgoingMessage, for: .normal)
                    btnSort.addTarget(self, action: #selector(sortClubClicked(_:)), for: .touchUpInside)
                }
                
            case .club:
                cell = tableView.dequeueReusableCell(withIdentifier: "ClubCell", for: indexPath)
                let ivPhoto: UIImageView = cell.viewWithTag(1) as! UIImageView
                let lbTitle: UILabel = cell.viewWithTag(3) as! UILabel
                let lbDate: UILabel = cell.viewWithTag(4) as! UILabel
                let btnJoin: UIButton = cell.viewWithTag(2) as! UIButton
                
//                let club = ClubManager.shared().getCachedGuestClubs()![indexPath.row]
                var club: Club!
                if let clubSearchResult = clubSearchResult {
                    club = clubSearchResult[indexPath.row]
                } else {
                    club = ClubManager.shared().getCachedGuestClubs()![indexPath.row]
                }
                
                ivPhoto.backgroundColor = UiUtility.colorBackgroundIncomingMessage
                if let photoUrl = club.picUrl {
                    let url = URL(string: photoUrl)
                    ivPhoto.kf.setImage(with: url)
                }
                lbTitle.textColor = UiUtility.colorDark4A
                lbTitle.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 14))
                lbTitle.text = club.name
                lbDate.textColor = UiUtility.colorBrownGrey
                lbDate.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
                lbDate.text = club.getDateText()
                
                btnJoin.isSelected = club.isMember()
                btnJoin.addTarget(self, action: #selector(joinClubClicked(_:)), for: .touchUpInside)
                
            case .post:
                cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath)
                
                let clubItem: ClubItemView = cell.viewWithTag(1) as! ClubItemView
                for constraint in cell.contentView.constraints {
                    if constraint.identifier == "marginBottom" {
                        constraint.constant = UiUtility.adaptiveSize(size: 6)
                    }
                }
                
                let news: ClubPost = ClubManager.shared().getClubData(remotely: false, listener: self)!.news[indexPath.row]
                clubItem.setupUi(with: news, isList: true, inPerson: false)
                clubItem.buttonUser.addTarget(self, action: #selector(user2Clicked(_:)), for: .touchUpInside)
                clubItem.buttonFollow.addTarget(self, action: #selector(follow2Clicked(_:)), for: .touchUpInside)
                clubItem.buttonMore.addTarget(self, action: #selector(more2Clicked(_:)), for: .touchUpInside)
                clubItem.buttonLike.addTarget(self, action: #selector(like2Clicked(_:)), for: .touchUpInside)
                clubItem.buttonLikeList.addTarget(self, action: #selector(likers2Clicked(_:)), for: .touchUpInside)
                clubItem.buttonTrack.addTarget(self, action: #selector(track2Clicked(_:)), for: .touchUpInside)
                
            }
        } else {
            switch SectionFeed.init(rawValue: indexPath.section)! {
            case .paddingKind:
                cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath)
            case .feed:
                let news = newsList[indexPath.row]
                if news.isTopping() {
                    cell = tableView.dequeueReusableCell(withIdentifier: "TopFeedCell", for: indexPath)
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell", for: indexPath)
                }
                
                let feedItem: FeedItemView = cell.viewWithTag(1) as! FeedItemView
                for constraint in cell.contentView.constraints {
                    if constraint.identifier == "marginBottom" {
                        constraint.constant = UiUtility.adaptiveSize(size: 6)
                    }
                }
                
                feedItem.setupUi(with: news, isList: true, inPerson: false)
                feedItem.buttonUser.addTarget(self, action: #selector(userClicked(_:)), for: .touchUpInside)
                feedItem.buttonFollow.addTarget(self, action: #selector(followClicked(_:)), for: .touchUpInside)
                feedItem.buttonMore.addTarget(self, action: #selector(moreClicked(_:)), for: .touchUpInside)
                feedItem.buttonLike.addTarget(self, action: #selector(likeClicked(_:)), for: .touchUpInside)
                feedItem.buttonLikeList.addTarget(self, action: #selector(likersClicked(_:)), for: .touchUpInside)
                feedItem.buttonTrack.addTarget(self, action: #selector(trackClicked(_:)), for: .touchUpInside)
            }
        }
        
        cell.selectionStyle = .none
        cell.backgroundColor = UiUtility.colorBackgroundWhite
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if newsKind == .club {
            if indexPath.section == SectionClub.club.rawValue {
                var club: Club!
                if let clubSearchResult = clubSearchResult {
                    club = clubSearchResult[indexPath.row]
                } else {
                    club = ClubManager.shared().getCachedGuestClubs()![indexPath.row]
                }
                self.tabControllerDelegate?.goClub(club)
            } else if indexPath.section == SectionClub.post.rawValue {
                let post: ClubPost = ClubManager.shared().getClubData(remotely: false, listener: self)!.news[indexPath.row]
                self.tabControllerDelegate?.goClubPostDetail(post: post, clubUpdateDelegate: nil)
            }
        } else {
            switch SectionFeed.init(rawValue: indexPath.section)! {
            case .feed:
                self.tabControllerDelegate?.goNewsDetail(news: newsList[indexPath.row])
            default:
                break
            }
        }
    }

    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */


    
    // MARK: private methods
    
    private func calculateNewsCellHeight(news: News) -> CGFloat {
        let width = UiUtility.adaptiveSize(size: 375 - 20 - 20)
        let font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))!
        let maxContentHeight: CGFloat = "line_4_text".localized.height(withConstrainedWidth: width, font: font)

        var h: CGFloat = 0
        h += UiUtility.adaptiveSize(size: 70) // height before content
        
        if !Utility.isEmpty(news.content) {
            h += min(news.content!.height(withConstrainedWidth: width, font: font), maxContentHeight)
        }

        h += UiUtility.adaptiveSize(size: 36) // date
        h += UiUtility.adaptiveSize(size: 375) // image
        h += UiUtility.adaptiveSize(size: 50) // footer
        h += UiUtility.adaptiveSize(size: 6) // separator
        
        return h
    }
    
    private func calculateNewsCellHeight(news: ClubPost) -> CGFloat {
        let width = UiUtility.adaptiveSize(size: 375 - 20 - 20)
        let font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))!
        let maxContentHeight: CGFloat = "line_4_text".localized.height(withConstrainedWidth: width, font: font)

        var h: CGFloat = 0
        h += UiUtility.adaptiveSize(size: 70) // height before content
        
        if !Utility.isEmpty(news.content) {
            h += min(news.content!.height(withConstrainedWidth: width, font: font), maxContentHeight)
        }

        h += UiUtility.adaptiveSize(size: 36) // date
        h += UiUtility.adaptiveSize(size: 375) // image
        h += UiUtility.adaptiveSize(size: 50) // footer
        h += UiUtility.adaptiveSize(size: 6) // separator
        
        return h
    }
    
    @IBAction func shieldClicked() {
        buttonShield.isHidden = true
        extendedNavi.isHidden = true
    }
    
    @IBAction func extendedNavi0Clicked() {
        buttonShield.isHidden = true
        extendedNavi.isHidden = true
        
        switchMode(kind: getModeFromExtendedNavi(index: 0))
    }
    
    @IBAction func extendedNavi1Clicked() {
        buttonShield.isHidden = true
        extendedNavi.isHidden = true
        
        switchMode(kind: getModeFromExtendedNavi(index: 1))
    }
    
    @IBAction func extendedNavi2Clicked() {
        buttonShield.isHidden = true
        extendedNavi.isHidden = true
        
        switchMode(kind: getModeFromExtendedNavi(index: 2))
    }
    
    @IBAction func extendedNavi3Clicked() {
        buttonShield.isHidden = true
        extendedNavi.isHidden = true
        
        switchMode(kind: getModeFromExtendedNavi(index: 3))
    }
    
    @IBAction func extendedNavi4Clicked(_ sender: Any) {
        buttonShield.isHidden = true
        extendedNavi.isHidden = true
        
        switchMode(kind: getModeFromExtendedNavi(index: 4))
    }
    
    @IBAction func AddClicked() {
        switch newsKind {
        case .all:
            Analytics.logEvent("add_post", parameters: ["screen_name": "screen_newsfeed_all" as NSObject])
        case .highlighted:
            Analytics.logEvent("add_post", parameters: ["screen_name": "screen_newsfeed_following" as NSObject])
        case .club, .follow, .favoritePost, .myPost:
            break;
        }
        
        CarManager.shared().getCars(remotely: true, listener: self)
    }
    
    private func setNaviTitle() {
        switch newsKind {
        case .all:
            buttonAddCons.constant = UiUtility.adaptiveSize(size: 102)
            showFloatingBtn(show: true)
            naviBar.setTitle("feed_all".localized)
            extendedNaviTitle0.text = "my_post".localized
            extendedNaviTitle1.text = "feed_highlight".localized
            extendedNaviTitle2.text = "club".localized
            extendedNaviTitle3.text = "collection_posts".localized
            extendedNaviTitle4.text = "follow_page".localized
        case .highlighted:
            buttonAddCons.constant = UiUtility.adaptiveSize(size: 38)
            showFloatingBtn(show: false)
            naviBar.setTitle("feed_highlight".localized)
            extendedNaviTitle0.text = "my_post".localized
            extendedNaviTitle1.text = "club".localized
            extendedNaviTitle2.text = "feed_all".localized
            extendedNaviTitle3.text = "collection_posts".localized
            extendedNaviTitle4.text = "follow_page".localized
        case .club:
            buttonAddCons.constant = UiUtility.adaptiveSize(size: 38)
            showFloatingBtn(show: false)
            naviBar.setTitle("club".localized)
            extendedNaviTitle0.text = "my_post".localized
            extendedNaviTitle1.text = "feed_all".localized
            extendedNaviTitle2.text = "feed_highlight".localized
            extendedNaviTitle3.text = "collection_posts".localized
            extendedNaviTitle4.text = "follow_page".localized
        case .favoritePost:
            buttonAddCons.constant = UiUtility.adaptiveSize(size: 38)
            showFloatingBtn(show: false)
            naviBar.setTitle("collection_posts".localized)
            extendedNaviTitle0.text = "my_post".localized
            extendedNaviTitle1.text = "feed_all".localized
            extendedNaviTitle2.text = "feed_highlight".localized
            extendedNaviTitle3.text = "club".localized
            extendedNaviTitle4.text = "follow_page".localized
        case .follow:
            buttonAddCons.constant = UiUtility.adaptiveSize(size: 38)
            showFloatingBtn(show: false)
            naviBar.setTitle("follow_page".localized)
            extendedNaviTitle0.text = "my_post".localized
            extendedNaviTitle1.text = "feed_all".localized
            extendedNaviTitle2.text = "feed_highlight".localized
            extendedNaviTitle3.text = "club".localized
            extendedNaviTitle4.text = "collection_posts".localized
        case .myPost:
            buttonAddCons.constant = UiUtility.adaptiveSize(size: 38)
            showFloatingBtn(show: false)
            naviBar.setTitle("my_post".localized)
            extendedNaviTitle0.text = "feed_all".localized
            extendedNaviTitle1.text = "feed_highlight".localized
            extendedNaviTitle2.text = "club".localized
            extendedNaviTitle3.text = "collection_posts".localized
            extendedNaviTitle4.text = "follow_page".localized
        }
    }
    
    private func getModeFromExtendedNavi(index: Int) -> NewsKind {
        switch newsKind {
        case .all:
            if index == 0 {
                return .myPost
            } else if index == 1 {
                return .highlighted
            } else if index == 2 {
                return .club
            } else if index == 3 {
                return .favoritePost
            } else {
                return .follow
            }
        case .highlighted:
            if index == 0 {
                return .myPost
            } else if index == 1 {
                return .club
            } else if index == 2 {
                return .all
            } else if index == 3 {
                return .favoritePost
            } else {
                return .follow
            }
        case .club:
            if index == 0 {
                return .myPost
            } else if index == 1 {
                return .all
            } else if index == 2 {
                return .highlighted
            } else if index == 3 {
                return .favoritePost
            } else {
                return .follow
            }
        case .favoritePost:
            if index == 0 {
                return .myPost
            } else if index == 1 {
                return .all
            } else if index == 2 {
                return .highlighted
            } else if index == 3 {
                return .club
            } else {
                return .follow
            }
        case .follow:
            if index == 0 {
                return .myPost
            } else if index == 1 {
                return .all
            } else if index == 2 {
                return .highlighted
            } else if index == 3 {
                return .club
            } else {
                return .favoritePost
            }
        case .myPost:
            if index == 0 {
                return .all
            } else if index == 1 {
                return .highlighted
            } else if index == 2 {
                return .club
            } else if index == 3 {
                return .favoritePost
            } else {
                return .follow
            }
        }
    }
    
    private func switchMode(kind: NewsKind) {
        if newsKind != kind {
            if kind == .club {
                if ClubManager.shared().getJoinedClubCount(remotely: true, listener: self) == nil {
                    Logger.d("get no joined club count locally, maybe it's first time")
                    return
                }
            }
            
            newsKind = kind
            
            setNaviTitle()
            
            switch newsKind {
            case .all:
                Analytics.logEvent("view_newsfeed_all", parameters: nil)
                setClubView(show: false)
                setCollectionPostView(show: false)
                setFollowView(show: false)
                setMyPostView(show: false)
                resetModels()
                tableViewFeed.delegate = self
                tableViewFeed.dataSource = self
                newsList = NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: false, listener: self)
                updateFeedUi()
            case .highlighted:
                Analytics.logEvent("view_newsfeed_following", parameters: nil)
                setClubView(show: false)
                setCollectionPostView(show: false)
                setFollowView(show: false)
                setMyPostView(show: false)
                resetModels()
                tableViewFeed.delegate = self
                tableViewFeed.dataSource = self
                newsList = NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: false, listener: self)
                updateFeedUi()
            case .club:
                tableViewFeed.delegate = nil
                tableViewFeed.dataSource = nil
                
                setClubView(show: true)
                setCollectionPostView(show: false)
                setFollowView(show: false)
                setMyPostView(show: false)
            case .favoritePost:
                setCollectionPostView(show: true)
                setClubView(show: false)
                setFollowView(show: false)
                setMyPostView(show: false)
                controllerfavPosts.refreshContent()
            case .follow:
                setFollowView(show: true)
                setCollectionPostView(show: false)
                setClubView(show: false)
                setMyPostView(show: false)
                controllerfollow.switchToTab0()
            case .myPost:
                setMyPostView(show: true)
                setFollowView(show: false)
                setCollectionPostView(show: false)
                setClubView(show: false)
                controllerMyPosts.refreshContent()
            }
        }
    }
    
    private func updateFeedUi() {
        tableViewFeed.reloadData()
    }
    
    private func updateClubUi() {
        tableViewClub.reloadData()
    }
    
//    @objc private func modelClicked(_ sender: UIButton) {
//        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.collectionViewKind)
//        if let indexPath = self.collectionViewKind.indexPathForItem(at: buttonPosition) {
////            Logger.d("click at row \(String(describing: indexPath.row))")
//            let carModel: CarModel2 = carModels[indexPath.row]
//            carModel.selected = !carModel.selected
//            if carModel.selected {
//                let tag: String = "tag_\(carModel.model.name!.lowercased())"
////                Logger.d("tag: \(tag)")
//                Analytics.logEvent("newsfeed_tag", parameters: ["tag_name": tag as NSObject])
//            }
//            collectionViewKind.reloadData()
//            NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: false, listener: self)
//        }
//    }
    
    private func resetModels() {
        for carModel in carModels {
            if carModel.selected {
                carModel.selected = false
            }
        }
        
        collectionViewKind.reloadData()
    }
    
    private func getModelArrayText(forFeed: Bool = true) -> String? {
        var text: String? = nil
        
        if forFeed {
            for carModel in carModels {
                if carModel.selected {
                    if let t = text {
                        text = "\(t),\(carModel.model.id!)"
                    } else {
                        text = "\(carModel.model.id!)"
                    }
                }
            }
        } else {
            for carModel in clubCarModels {
                if carModel.selected {
                    if let t = text {
                        text = "\(t),\(carModel.model.id!)"
                    } else {
                        text = "\(carModel.model.id!)"
                    }
                }
            }
        }
        
//        Logger.d("text: \(text)")
        return text
    }
    
    @objc private func userClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewFeed)
        if let indexPath = self.tableViewFeed.indexPathForRow(at: buttonPosition) {
            let news: News = newsList[indexPath.row]
            if let _ = news.accountId {
                UiUtility.goBrowse(profile: Profile.init(with: news), controller: self)
            }
        }
    }
    
    @objc private func user2Clicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewClub)
        if let indexPath = self.tableViewClub.indexPathForRow(at: buttonPosition) {
            let news: ClubPost = ClubManager.shared().getClubData(remotely: false, listener: self)!.news[indexPath.row]
            if let _ = news.accountId {
                UiUtility.goBrowse(profile: Profile.init(with: news), controller: self)
            }
        }
    }
    
    @objc private func followClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewFeed)
        if let indexPath = self.tableViewFeed.indexPathForRow(at: buttonPosition) {
            let news: News = newsList[indexPath.row]
            if let accountId = news.accountId {
                UserManager.shared().follow(accountId: accountId, set: true, listener: self)
            }
        }
    }
    
    @objc private func follow2Clicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewClub)
        if let indexPath = self.tableViewClub.indexPathForRow(at: buttonPosition) {
            let news: ClubPost = ClubManager.shared().getClubData(remotely: false, listener: self)!.news[indexPath.row]
            if let accountId = news.accountId {
                UserManager.shared().follow(accountId: accountId, set: true, listener: self)
            }
        }
    }
    
    @objc private func moreClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewFeed)
        if let indexPath = self.tableViewFeed.indexPathForRow(at: buttonPosition) {
            let news: News = newsList[indexPath.row]
            if news.offical != nil && news.offical! == Role.official.rawValue {
                // official
                UiUtility.moreMenu(from: self, type: 0, news: news, contentDelegate: self)
            } else if let newsAccoundId = news.accountId {
                if newsAccoundId == UserManager.shared().currentUser.accountId! {
                    // mine
                    UiUtility.moreMenu(from: self, type: 1, news: news, contentDelegate: self)
                } else {
                    // others
                    UiUtility.moreMenu(from: self, type: 2, news: news, contentDelegate: self)
                }
            }
        }
    }
    
    @objc private func more2Clicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewClub)
        if let indexPath = self.tableViewClub.indexPathForRow(at: buttonPosition) {
            let post: ClubPost = ClubManager.shared().getClubData(remotely: false, listener: self)!.news[indexPath.row]
            UiUtility.clubMoreMenu(from: self, post: post, clubProtocol: self, clubUpdateDelegate: nil)
        }
    }
    
    @objc private func likeClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewFeed)
        if let indexPath = self.tableViewFeed.indexPathForRow(at: buttonPosition) {
            let news: News = newsList[indexPath.row]
            NewsManager.shared().like(news: news, set: (news.likeStatus == 0), listener: self)
        }
    }
    
    @objc private func like2Clicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewClub)
        if let indexPath = self.tableViewClub.indexPathForRow(at: buttonPosition) {
            let news: ClubPost = ClubManager.shared().getClubData(remotely: false, listener: self)!.news[indexPath.row]
            ClubManager.shared().like(news: news, set: (news.likeStatus == 0), listener: self)
        }
    }
    
    @objc private func trackClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewFeed)
        if let indexPath = self.tableViewFeed.indexPathForRow(at: buttonPosition) {
            let news: News = newsList[indexPath.row]
            NewsManager.shared().track(news: news, set: (news.bookmarkStatus == 0), listener: self)
        }
    }
    
    @objc private func track2Clicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewClub)
        if let indexPath = self.tableViewClub.indexPathForRow(at: buttonPosition) {
            let news: ClubPost = ClubManager.shared().getClubData(remotely: false, listener: self)!.news[indexPath.row]
            ClubManager.shared().track(news: news, set: (news.bookmarkStatus == 0), listener: self)
        }
    }
    
    @objc private func likersClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewFeed)
        if let indexPath = self.tableViewFeed.indexPathForRow(at: buttonPosition) {
            let news: News = newsList[indexPath.row]
            self.tabControllerDelegate?.goBrowseLikers(news: news)
        }
    }
    
    @objc private func likers2Clicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewClub)
        if let indexPath = self.tableViewClub.indexPathForRow(at: buttonPosition) {
            let news: ClubPost = ClubManager.shared().getClubData(remotely: false, listener: self)!.news[indexPath.row]
            self.tabControllerDelegate?.goBrowseClubPostLikers(post: news)
        }
    }
    
    @objc private func refresh(_ sender: Any) {
        NewsManager.shared().getNewsList(remotely: true, kind: newsKind, models: getModelArrayText(), loadMore: false, listener: self)
    }
    
    
    
    // MARK: club
    
    private func hasJoinedClub() -> Bool {
        if let count = ClubManager.shared().getJoinedClubCount(remotely: false, listener: nil) {
            return count != 0
        }
        return false
    }
    
    private func setClubView(show: Bool) {
        tableViewClub.delegate = show ? self : nil
        tableViewClub.dataSource = show ? self : nil
        
        if show {
            let hasJoined = hasJoinedClub()
            
            viewClubSearchContainer.isHidden = hasJoined
            buttonExploreClub.isHidden = !hasJoined
            if hasJoined {
                // has joined clubs
                constraintCreateClubLeading.constant = UiUtility.adaptiveSize(size: 20)
            } else {
                // no joined clubs
                constraintCreateClubLeading.constant = UiUtility.adaptiveSize(size: 275)
            }
            
            updateClubUi()
        }
        
        viewClubContainer.isHidden = !show
    }
    
    private func setCollectionPostView(show: Bool) {
        FavPostContainer.isHidden = !show
    }
    
    private func setMyPostView(show: Bool) {
        MyPostContainer.isHidden = !show
    }
    
    private func setFollowView(show: Bool) {
        followContainer.isHidden = !show
    }
    
    @objc private func joinClubClicked(_ sender: UIButton) {
        let buttonPosition: CGPoint = sender.convert(CGPoint.zero, to: self.tableViewClub)
        if let indexPath = self.tableViewClub.indexPathForRow(at: buttonPosition) {
            var club: Club!
            if let clubSearchResult = clubSearchResult {
                club = clubSearchResult[indexPath.row]
            } else {
                club = ClubManager.shared().getCachedGuestClubs()![indexPath.row]
            }
            
            Logger.d("click joinClubClicked at row \(String(describing: indexPath.row))")
            Logger.d("club: \(String(describing: club))")
            
            if !club.isMember() {
                ClubManager.shared().memberAdd(clubId: club.id, listener: self)
            }
        }
    }
    
    @objc private func sortClubClicked(_ sender: UIButton) {
        self.tabControllerDelegate?.sortClub()
    }
    
    @IBAction private func createClubClicked(_ sender: Any) {
        if clubCarModels.count == 0 {
            Logger.d("model count is zero")
        }
        self.tabControllerDelegate?.goCreateClub(clubCarModels: clubCarModels)
    }
    
    @IBAction private func exploreClubClicked(_ sender: Any) {
        self.tabControllerDelegate?.goExploreClub()
    }
    
    @objc private func browseAllClubClicked(_ sender: UIButton) {
        self.tabControllerDelegate?.goYourClub()
    }
    
    @objc private func refreshClub(_ sender: Any) {
        ClubManager.shared().cleanCache()
        ClubManager.shared().getJoinedClubCount(remotely: true, listener: self)
    }
    
    // MARK: WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
//        Logger.d("didCommit")
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//        Logger.d("didStartProvisionalNavigation")
//        showHud()
    }
    
    func webView(_ webView: WKWebView,
                 didFail navigation: WKNavigation!,
                 withError error: Error) {
        Logger.d("didFail")
    }
    
    func webView(_ webView: WKWebView,
                 didFailProvisionalNavigation navigation: WKNavigation!,
                 withError error: Error) {
        Logger.d("didFailProvisionalNavigation, error: \(error)")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Logger.d("didFinish")

    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if navigationAction.request.url == nil {
            return
        }
        
        if navigationAction.request.url?.scheme == "tel" {
            UIApplication.shared.open(navigationAction.request.url!)
            decisionHandler(.cancel)
        }
        
        if navigationAction.targetFrame == nil {
            let decodedResponse = navigationAction.request.url!.absoluteString.removingPercentEncoding!
            let encodeURL = decodedResponse.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            if let strTourl = URL(string: encodeURL) {
                print("load url: \(strTourl)")
                let request = URLRequest(url: strTourl)
                webView.load(request)
            }
        }
        
        decisionHandler(.allow)
    }
    
    // MARK: - JS
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {
        
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (action) in
            completionHandler()
        }))
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (Bool) -> Void) {
        
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (action) in
            completionHandler(true)
        }))
        
        alertController.addAction(UIAlertAction(title: "取消", style: .default, handler: { (action) in
            completionHandler(false)
        }))
        
        present(alertController, animated: true, completion: nil)
    }
    
    
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping (String?) -> Void) {
        
        let alertController = UIAlertController(title: nil, message: prompt, preferredStyle: .actionSheet)
        
        alertController.addTextField { (textField) in
            textField.text = defaultText
        }
        
        alertController.addAction(UIAlertAction(title: "確定", style: .default, handler: { (action) in
            if let text = alertController.textFields?.first?.text {
                completionHandler(text)
            } else {
                completionHandler(defaultText)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: "取消", style: .default, handler: { (action) in
            completionHandler(nil)
        }))
        
        present(alertController, animated: true, completion: nil)
    }
}
