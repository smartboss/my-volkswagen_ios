//
//  VWButton.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/7.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import Foundation
import UIKit

class VWButton: UIButton {
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    // MARK: - Private Methods
    
    private func setupButton() {
        setTitleColor(.white, for: .normal)
        setTitleColor(UIColor(hex: 0xCBD5E0), for: .disabled)
        backgroundColor = UIColor(hex: 0x43BEF2)
        layer.cornerRadius = CGFloat(8.0).getFitSize()
        layer.masksToBounds = true
        updateFontSize()
    }
    
    func updateFontSize() {
        self.titleLabel?.font = self.titleLabel?.font.withSize(self.titleLabel!.font!.pointSize.getFitSize())
    }
    
    override var isEnabled: Bool {
        didSet {
            if isEnabled {
                backgroundColor = UIColor(hex: 0x43BEF2)
                setTitleColor(.white, for: .normal)
            } else {
                backgroundColor = UIColor(hex: 0xE2E8F0)
                setTitleColor(UIColor(hex: 0xCBD5E0), for: .normal)
            }
        }
    }
}

// UIColor扩展以接受16进制颜色值
extension UIColor {
    convenience init(hex: Int) {
        let red = CGFloat((hex >> 16) & 0xFF) / 255.0
        let green = CGFloat((hex >> 8) & 0xFF) / 255.0
        let blue = CGFloat(hex & 0xFF) / 255.0
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
}

class VWButtonWhite: UIButton {
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    // MARK: - Private Methods
    
    private func setupButton() {
        setTitleColor(UIColor(hex: 0x43BEF2), for: .normal)
        backgroundColor = .white
        layer.cornerRadius = CGFloat(8.0).getFitSize()
        layer.masksToBounds = true
        layer.borderColor = UIColor(hex: 0x43BEF2).cgColor
        layer.borderWidth = 1
        updateFontSize()
    }
    
    func updateFontSize() {
        self.titleLabel?.font = self.titleLabel?.font.withSize(self.titleLabel!.font!.pointSize.getFitSize())
    }
    
    //    override var isEnabled: Bool {
    //        didSet {
    //            if isEnabled {
    //                backgroundColor = UIColor(hex: 0x43BEF2)
    //                setTitleColor(.white, for: .normal)
    //            } else {
    //                backgroundColor = UIColor(hex: 0xE2E8F0)
    //                setTitleColor(UIColor(hex: 0xCBD5E0), for: .normal)
    //            }
    //        }
    //    }
}
