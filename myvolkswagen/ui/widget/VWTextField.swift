//
//  VWTextField.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/13.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit

class VWTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.layer.cornerRadius = CGFloat(6).getFitSize()
        setBorderColor(UIColor.MyTheme.greyColor400)
        layer.borderWidth = 1.0 // 設置邊框寬度
        
        // 監聽編輯事件
        addTarget(self, action: #selector(textFieldDidBeginEditing), for: .editingDidBegin)
        addTarget(self, action: #selector(textFieldDidEndEditing), for: .editingDidEnd)

    }
    
    // MARK: - UITextFieldDelegate methods
    
    // 編輯開始時改變外框顏色
    @objc private func textFieldDidBeginEditing() {
        // 正在輸入時，更改光標和邊框顏色
        setBorderColor(UIColor.MyTheme.secondaryColor100)
        tintColor = UIColor.MyTheme.secondaryColor400 // 更改光標顏色
    }

    // 編輯結束時改變外框顏色
    @objc private func textFieldDidEndEditing() {
        // 輸入結束時，恢復原來的光標和邊框顏色
        setBorderColor(UIColor.MyTheme.greyColor400)
        tintColor = UIColor.MyTheme.greyColor400 // 恢復光標顏色
    }
    
    // 設置邊框顏色的方法
    private func setBorderColor(_ color: UIColor) {
        layer.borderColor = color.cgColor
    }
    
    // 當輸入錯誤時調用該方法，設置邊框為紅色
    func setErrorBorder(error: Bool) {
        if error {
            setBorderColor(.red)
        } else {
            setBorderColor(UIColor.MyTheme.greyColor400)
        }
    }
}
