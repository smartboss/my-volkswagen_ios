//
//  VWCustomAlert.swift
//  VWCustomAlert
//
//  Created by Shelley on 19/10/20.
//

import UIKit

class VWCustomAlert: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var alertView: UIView!
    
    var alertTitle = ""
    var alertMessage = ""
    var okButtonTitle = "確定"
    var cancelButtonTitle = "取消"
    var alertTag = 0
    var isCancelButtonHidden = false
    var blueOkBtn = false
    
    var okButtonAction: (() -> Void)?
    var cancelButtonAction: (() -> Void)?

    init() {
        super.init(nibName: "VWCustomAlert", bundle: Bundle(for: VWCustomAlert.self))
        self.modalPresentationStyle = .overFullScreen
        self.modalTransitionStyle = .crossDissolve
        
    }
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAlert()
        if blueOkBtn {
            setupBlueBtn()
        }
    }
    
    func show() {
        if #available(iOS 13, *) {
            UIApplication.shared.windows.first?.rootViewController?.present(self, animated: true, completion: nil)
        } else {
            UIApplication.shared.keyWindow?.rootViewController!.present(self, animated: true, completion: nil)
        }
    }
    
    func setupAlert() {
        titleLabel.text = alertTitle
        messageLabel.text = alertMessage
        okButton.setTitle(okButtonTitle, for: .normal)
        cancelButton.setTitle(cancelButtonTitle, for: .normal)
        if isCancelButtonHidden {
            cancelButton.removeFromSuperview()
        } else {
            cancelButton.layer.cornerRadius = CGFloat(8).getFitSize()
            cancelButton.layer.borderWidth = 1
            cancelButton.backgroundColor = UIColor.MyTheme.secondaryColor400
        }
        //cancelButton.isHidden = isCancelButtonHidden
        alertView.layer.cornerRadius = CGFloat(10).getFitSize()
        okButton.layer.borderColor = UIColor.MyTheme.secondaryColor400.cgColor
        okButton.setTitleColor(UIColor.MyTheme.secondaryColor400, for: .normal)
    }
    
    func setupBlueBtn() {
        okButton.layer.cornerRadius = CGFloat(8).getFitSize()
        okButton.layer.borderWidth = 1
        okButton.backgroundColor = UIColor.MyTheme.secondaryColor400
        okButton.setTitleColor(.white, for: .normal)
    }
    
    @IBAction func actionOnOkButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        okButtonAction?()
    }
    @IBAction func actionOnCancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        cancelButtonAction?()
    }
   
}
