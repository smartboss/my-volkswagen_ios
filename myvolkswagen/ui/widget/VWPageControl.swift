//
//  VWPageControl.swift
//  myvolkswagen
//
//  Created by Shelley on 2024/1/17.
//  Copyright © 2024 volkswagen. All rights reserved.
//

import UIKit

class VWPageControl: UIView {
    
    var numberOfPage: Int
    var currentpage : Int                  = 0{didSet{reloadView()}}
    var currentIndicatorColor: UIColor     = UIColor(red: 0.796, green: 0.835, blue: 0.878, alpha: 1)
    var indicatorColor: UIColor            = UIColor(red: 0.886, green: 0.91, blue: 0.941, alpha: 0.7)
    var circleIndicator: Bool              = false
    private var dotView                    = [UIView]()
    private let spacing: CGFloat           = 4
    private lazy var  extraWidth: CGFloat  = circleIndicator ? 6 : 4
    let myheight: CGFloat = CGFloat(4).getFitSize()
    
    init(numberOfPages: Int,currentPage: Int,isCircular: Bool){
        self.numberOfPage    = numberOfPages
        self.currentpage     = currentPage
        self.circleIndicator = isCircular
        super.init(frame: .zero)
        configView()
    }
    required init?(coder: NSCoder) {fatalError("not implemented")}
    
    private func configView(){
        backgroundColor = .clear
        (0..<numberOfPage).forEach { _ in
            let view = UIView()
            addSubview(view)
            dotView.append(view)
        }
    }
    
    private func reloadView(){
        if dotView.count > 0 {
            dotView.forEach{$0.backgroundColor = indicatorColor}
            dotView[currentpage].backgroundColor = currentIndicatorColor
            UIView.animate(withDuration: 0.2) {
                self.dotView[self.currentpage].frame.origin.x   = self.dotView[self.currentpage].frame.origin.x - self.extraWidth
                self.dotView[self.currentpage].frame.size.width = self.dotView[self.currentpage].frame.size.width + (self.extraWidth * 2)
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //let normalWidth = (self.bounds.width - CGFloat(numberOfPage - 1) * spacing) / CGFloat(numberOfPage)
        let normalWidth = CGFloat(8).getFitSize()
        let currentPageWidth = normalWidth + extraWidth
        var xPosition: CGFloat = 0
        
        for (index, view) in dotView.enumerated() {
            let isCurrentPage = index == currentpage
            view.backgroundColor = isCurrentPage ? currentIndicatorColor : indicatorColor
            let width = isCurrentPage ? currentPageWidth : normalWidth
            view.frame = CGRect(x: xPosition, y: 0, width: width, height: self.myheight)
            view.layer.cornerRadius = self.myheight / 2
            view.clipsToBounds = true
            xPosition += width + spacing
        }
    }
    
}
