//
//  GradientBackgroundView.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/12.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import Foundation
import UIKit

class GradientBackgroundView: UIView {
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    init() {
        super.init(frame: CGRect.zero)
        
        // 設置漸變顏色
        let gradientLayer = self.layer as! CAGradientLayer
        gradientLayer.colors = [
            UIColor(red: 0.97, green: 0.98, blue: 0.99, alpha: 1.00).cgColor,
            UIColor(red: 0.89, green: 0.96, blue: 0.99, alpha: 1.00).cgColor
        ]
        
        // 設置漸變方向（從上到下）
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
