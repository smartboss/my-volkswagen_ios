//
//  UImageViewHelper.swift
//  myvolkswagen
//
//  Created by Shelley on 2024/1/9.
//  Copyright © 2024 volkswagen. All rights reserved.
//

import Foundation
import UIKit
import ImageIO

extension UIImageView {

    func loadGif(name: String) {
        DispatchQueue.global().async {
            guard let path = Bundle.main.path(forResource: name, ofType: "gif") else {
                print("Gif does not exist at that path")
                return
            }
            let url = URL(fileURLWithPath: path)
            guard let gifData = try? Data(contentsOf: url),
                  let source = CGImageSourceCreateWithData(gifData as CFData, nil) else {
                print("Could not create image source with data")
                return
            }
            var images = [UIImage]()
            let count = CGImageSourceGetCount(source)
            for i in 0..<count {
                if let cgImage = CGImageSourceCreateImageAtIndex(source, i, nil) {
                    images.append(UIImage(cgImage: cgImage))
                }
            }
            DispatchQueue.main.async {
                self.animationImages = images
                self.animationDuration = Double(images.count) / 9.0
                self.animationRepeatCount = 0
                self.startAnimating()
            }
        }
    }
}
