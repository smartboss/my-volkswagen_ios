//
//  VWNaviBar.swift
//  myvolswagen
//
//  Created by Apple on 2/1/19.
//  Copyright © 2018 volkswagen. All rights reserved.
//

import UIKit



protocol VWNaviBarDelegate {
    func leftButtonClicked(_ button: UIButton)
    func rightButtonClicked(_ button: UIButton)
    func rightButton1Clicked(_ button: UIButton)
    func rightButton2Clicked(_ button: UIButton)
    func rightButtonNotiClicked(_ button: UIButton)
    func doneButtonClicked(_ button: UIButton)
    func editButtonClicked(_ button: UIButton)
    func publishButtonClicked(_ button: UIButton)
    func cancelButtonClicked(_ button: UIButton)
    func centerButtonClicked()
    
    func leftCancelButtonClicked(_ button: UIButton)
    func createButtonClicked(_ button: UIButton)
    func segmentDidChangeIndex(index: Int)
}



class VWNaviBar: UIView {
    
    var delegate: VWNaviBarDelegate?
    
    weak var contentView: UIView!
    
    @IBOutlet weak var imageViewLeft: UIImageView!
    @IBOutlet weak var buttonLeft: UIButton!
    
    @IBOutlet weak var imageViewRight: UIImageView!
    @IBOutlet weak var buttonRight: UIButton!
    
    @IBOutlet weak var imageViewRight1: UIImageView!
    @IBOutlet weak var buttonRight1: UIButton!
    
    @IBOutlet weak var imageViewRight2: UIImageView!
    @IBOutlet weak var buttonRight2: UIButton!
    @IBOutlet weak var btnNoti: UIButton!
    
    @IBOutlet weak var buttonDone: UIButton!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var buttonPublish: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!
    
    @IBOutlet weak var buttonLeftCancel: UIButton!
    @IBOutlet weak var buttonCreate: UIButton!
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var arrow: UIImageView!
    
    @IBOutlet weak var containerSearch: UIView!
    @IBOutlet weak var constraintSearchLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintSearchTrailing: NSLayoutConstraint!
    let leadingSearchOrig: CGFloat = UiUtility.adaptiveSize(size: 57)
    let trailingSearchOrig: CGFloat = UiUtility.adaptiveSize(size: 38)
    let leadingSearching: CGFloat = UiUtility.adaptiveSize(size: 20)
    let trailingSearching: CGFloat = UiUtility.adaptiveSize(size: 55)
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var segmentContainerView: UIView!

    private enum SegmentConstants {
        static let segmentedControlHeight: CGFloat = 40
        static let underlineViewColor: UIColor = UiUtility.colorPinkyRed
        static let underlineViewHeight: CGFloat = 2
    }
    
    private lazy var segmentedControlContainerView: UIView = {
        let containerView = UIView()
        containerView.backgroundColor = .clear
        containerView.translatesAutoresizingMaskIntoConstraints = false
        return containerView
    }()
    
    lazy var segmentedControl: UISegmentedControl = {
        let segmentedControl = PlainSegmentedControl(items:
            ["redeem_title_member_level".localized,
             "redeem_title_reward".localized,
             "redeem_title_point".localized])
        segmentedControl.tintColor = .gray
        segmentedControl.selectedSegmentIndex = 0
        
        segmentedControl.addTarget(self, action: #selector(segmentedControlValueChanged), for: .valueChanged)
        
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        return segmentedControl
    }()
    
    private lazy var bottomUnderlineView: UIView = {
        let underlineView = UIView()
        underlineView.backgroundColor = SegmentConstants.underlineViewColor
        underlineView.translatesAutoresizingMaskIntoConstraints = false
        return underlineView
    }()
    
    private lazy var leadingDistanceConstraint: NSLayoutConstraint = {
        return bottomUnderlineView.leftAnchor.constraint(equalTo: segmentedControl.leftAnchor)
    }()
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    private func commonInit() {
        contentView = (Bundle.main.loadNibNamed("VWNaviBar", owner: self, options: nil)?.first as! UIView)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        contentView.backgroundColor = .clear
        
        title.textColor = UIColor.black
        title.font = UIFont(name: "PingFangTC-Medium", size: 18)
        
        buttonDone.setTitle("done".localized, for: .normal)
        buttonDone.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: 18)
        
        buttonEdit.setTitle("edit".localized, for: .normal)
        buttonEdit.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: 18)
        
        buttonPublish.setTitle("publish".localized, for: .normal)
        buttonPublish.titleLabel?.font = UIFont(name: "PingFangTC-Regular", size: 17)
        buttonPublish.setTitleColor(UIColor(rgb: 0x007AFF), for: .normal)
        
        buttonCancel.setTitle("cancel".localized, for: .normal)
        buttonCancel.titleLabel?.font = UIFont(name: "PingFangTC-Regular", size: 17)
        buttonCancel.setTitleColor(UIColor(rgb: 0x007AFF), for: .normal)
        
        buttonLeftCancel.setTitle("cancel".localized, for: .normal)
        buttonLeftCancel.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: 18)
        buttonLeftCancel.setTitleColor(UiUtility.colorBrownGrey, for: .normal)
        
        buttonCreate.setTitle("create".localized, for: .normal)
        buttonCreate.titleLabel?.font = UIFont(name: "PingFangTC-Medium", size: 18)
        buttonCreate.setTitleColor(UiUtility.colorBackgroundOutgoingMessage, for: .normal)
        
//        buttonCancel.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
        
        containerSearch.layer.cornerRadius = 14
        containerSearch.backgroundColor = UIColor(rgb: 0x8E8E93).withAlphaComponent(0.12)
//        containerSearch.clipsToBounds = true
        setupSearchArea(orig: true)
        
        textFieldSearch.placeholder = "hint_search".localized
        textFieldSearch.returnKeyType = .search
        textFieldSearch.font = UIFont(name: "PingFangTC-Regular", size: 16)
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 29, height: 28))
        textFieldSearch.leftView = paddingView
        textFieldSearch.leftViewMode = .always
    }
    
    func setBackgroundColor(_ color: UIColor?) {
        contentView.backgroundColor = color
    }
    
    func setTitle(_ text: String) {
        title.text = text//.uppercased()
        title.isHidden = false
    }
    
    func setLeftVisibility(_ show: Bool) {
        imageViewLeft.isHidden = !show
        buttonLeft.isHidden = !show
    }
    
    func setDoneVisibility(_ show: Bool) {
        buttonDone.isHidden = !show
    }
    
    func setEditVisibility(_ show: Bool) {
        buttonEdit.isHidden = !show
    }
    
    func setPublishVisibility(_ show: Bool) {
        buttonPublish.isHidden = !show
    }
    
    func setLeftCancelVisibility(_ show: Bool) {
        buttonLeftCancel.isHidden = !show
    }
    
    func setCreateVisibility(_ show: Bool) {
        buttonCreate.isHidden = !show
    }
    
    func setLeftButton(_ image: UIImage?, highlighted: UIImage?) {
        if let image = image {
            imageViewLeft.image = image
            
            if let highlighted = highlighted {
                imageViewLeft.highlightedImage = highlighted
            }
            
            buttonLeft.isHidden = false
            imageViewLeft.isHidden = false
        } else {
            buttonLeft.isHidden = true
            imageViewLeft.isHidden = true
        }
    }
    
    func setRightButton(_ image: UIImage?, highlighted: UIImage?) {
        if let image = image {
            imageViewRight.image = image
            
            if let highlighted = highlighted {
                imageViewRight.highlightedImage = highlighted
            }
            
            buttonRight.isHidden = false
            imageViewRight.isHidden = false
        }
    }
    
    func setRightButton1(_ image: UIImage?, highlighted: UIImage?) {
        if let image = image {
            imageViewRight1.image = image
            
            if let highlighted = highlighted {
                imageViewRight1.highlightedImage = highlighted
            }
            
            buttonRight1.isHidden = false
            imageViewRight1.isHidden = false
        }
    }
    
    func setRightNotiButton() {
        btnNoti.isHidden = false
    }
    
    func setRightButton2(_ image: UIImage?, highlighted: UIImage?) {
        if let image = image {
            imageViewRight2.image = image
            
            if let highlighted = highlighted {
                imageViewRight2.highlightedImage = highlighted
            }
            
            buttonRight2.isHidden = false
            imageViewRight2.isHidden = false
        }
    }
    
    func setArrowVisibility(_ set: Bool) {
        arrow.isHidden = !set
    }
    
    @IBAction func leftButtonClicked(_ button: UIButton) {
        delegate?.leftButtonClicked(button)
    }
    
    @IBAction func rightButtonClicked(_ button: UIButton) {
        delegate?.rightButtonClicked(button)
    }
    
    @IBAction func rightButton1Clicked(_ button: UIButton) {
        delegate?.rightButton1Clicked(button)
    }
    
    @IBAction func rightButton2Clicked(_ button: UIButton) {
        delegate?.rightButton2Clicked(button)
    }
    
    @IBAction func rightNotiButtonClicked(_ button: UIButton) {
        delegate?.rightButtonNotiClicked(button)
    }
    
    @IBAction func doneButtonClicked(_ button: UIButton) {
        delegate?.doneButtonClicked(button)
    }
    
    @IBAction func editButtonClicked(_ button: UIButton) {
        delegate?.editButtonClicked(button)
    }
    
    @IBAction func publishButtonClicked(_ button: UIButton) {
        delegate?.publishButtonClicked(button)
    }
    
    @IBAction func cancelButtonClicked(_ button: UIButton) {
        delegate?.cancelButtonClicked(button)
    }
    
    @IBAction func centerButtonClicked(_ button: UIButton) {
        delegate?.centerButtonClicked()
    }
    
    @IBAction func leftCancelButtonClicked(_ button: UIButton) {
        delegate?.leftCancelButtonClicked(button)
    }
    
    @IBAction func createButtonClicked(_ button: UIButton) {
        delegate?.createButtonClicked(button)
    }

    func setupSearchArea(orig: Bool) {
        if orig {
            setLeftVisibility(true)
            constraintSearchLeading.constant = self.leadingSearchOrig
            constraintSearchTrailing.constant = self.trailingSearchOrig
            buttonCancel.isHidden = true
        } else {
            setLeftVisibility(false)
            constraintSearchLeading.constant = self.leadingSearching
            constraintSearchTrailing.constant = self.trailingSearching
            buttonCancel.isHidden = false
        }
    }
    
    func setUpSegmentControl(defaultIndex: Int) {
        segmentContainerView.addSubview(segmentedControlContainerView)
        segmentedControlContainerView.addSubview(segmentedControl)
        segmentedControlContainerView.addSubview(bottomUnderlineView)
        
        let safeLayoutGuide = self.segmentContainerView.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            segmentedControlContainerView.topAnchor.constraint(equalTo: safeLayoutGuide.topAnchor),
            segmentedControlContainerView.leadingAnchor.constraint(equalTo: safeLayoutGuide.leadingAnchor),
            segmentedControlContainerView.widthAnchor.constraint(equalTo: safeLayoutGuide.widthAnchor),
            segmentedControlContainerView.heightAnchor.constraint(equalToConstant: SegmentConstants.segmentedControlHeight)
        ])
        
        NSLayoutConstraint.activate([
            segmentedControl.topAnchor.constraint(equalTo: segmentedControlContainerView.topAnchor),
            segmentedControl.leadingAnchor.constraint(equalTo: segmentedControlContainerView.leadingAnchor),
            segmentedControl.centerXAnchor.constraint(equalTo: segmentedControlContainerView.centerXAnchor),
            segmentedControl.centerYAnchor.constraint(equalTo: segmentedControlContainerView.centerYAnchor)
        ])
        
        NSLayoutConstraint.activate([
            bottomUnderlineView.bottomAnchor.constraint(equalTo: segmentedControl.bottomAnchor),
            bottomUnderlineView.heightAnchor.constraint(equalToConstant: SegmentConstants.underlineViewHeight),
            leadingDistanceConstraint,
            bottomUnderlineView.widthAnchor.constraint(equalTo: segmentedControl.widthAnchor, multiplier: 1 / CGFloat(segmentedControl.numberOfSegments))
        ])
        
        segmentedControl.selectedSegmentIndex = defaultIndex
        changeSegmentedControlLinePosition()
        segmentContainerView.isHidden = false
    }
    
    private func changeSegmentedControlLinePosition() {
        let segmentIndex = CGFloat(segmentedControl.selectedSegmentIndex)
        let segmentWidth = segmentContainerView.frame.size.width / CGFloat(segmentedControl.numberOfSegments)
        let leadingDistance = segmentWidth * segmentIndex
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.leadingDistanceConstraint.constant = leadingDistance
            self?.layoutIfNeeded()
        })
    }
    
    @objc func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        changeSegmentedControlLinePosition()
        delegate?.segmentDidChangeIndex(index: sender.selectedSegmentIndex)
    }
}
