//
//  CustomCollectionView.swift
//  myvolkswagen
//
//  Created by Apple on 2/14/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class CustomCollectionView: UICollectionView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func touchesShouldCancel(in view: UIView) -> Bool {
        if view is UIControl {
            return true
        }
        return super.touchesShouldCancel(in: view)
    }
}
