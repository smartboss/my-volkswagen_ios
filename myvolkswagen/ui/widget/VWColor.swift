//
//  VWColor.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/9/12.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    struct MyTheme {
        // Primary Color
        static var primaryColor900: UIColor { return UIColor(red: 0, green: 0.12, blue: 0.31, alpha: 1) }
        static var primaryColor800: UIColor { return UIColor(red: 0.04, green: 0.18, blue: 0.4, alpha: 1) }
        static var primaryColor700: UIColor { return UIColor(red: 0.07, green: 0.21, blue: 0.45, alpha: 1) }
        static var primaryColor600: UIColor { return UIColor(red: 0.11, green: 0.25, blue: 0.49, alpha: 1) }
        static var primaryColor500: UIColor { return UIColor(red: 0.14, green: 0.27, blue: 0.53, alpha: 1) }
        static var primaryColor400: UIColor { return UIColor(red: 0.28, green: 0.38, blue: 0.59, alpha: 1) }
        static var primaryColor300: UIColor { return UIColor(red: 0.41, green: 0.48, blue: 0.65, alpha: 1) }
        static var primaryColor200: UIColor { return UIColor(red: 0.57, green: 0.62, blue: 0.75, alpha: 1) }
        static var primaryColor100: UIColor { return UIColor(red: 0.74, green: 0.77, blue: 0.85, alpha: 1) }
        static var primaryColor000: UIColor { return UIColor(red: 0.9, green: 0.91, blue: 0.94, alpha: 1) }
        static var primaryGreen: UIColor { return UIColor(red: 0.33, green: 0.75, blue: 0.71, alpha: 1.00) }
        static var primaryLightYellow: UIColor { return UIColor(red: 1.00, green: 0.78, blue: 0.20, alpha: 1.00) }
        
        // Secondary Color
        static var secondaryColor900: UIColor { return UIColor(red: 0, green: 0.32, blue: 0.59, alpha: 1) }
        static var secondaryColor800: UIColor { return UIColor(red: 0, green: 0.44, blue: 0.72, alpha: 1) }
        static var secondaryColor700: UIColor { return UIColor(red: 0, green: 0.51, blue: 0.8, alpha: 1) }
        static var secondaryColor600: UIColor { return UIColor(red: 0, green: 0.58, blue: 0.87, alpha: 1) }
        static var secondaryColor500: UIColor { return UIColor(red: 0, green: 0.64, blue: 0.93, alpha: 1) }
        static var secondaryColor400: UIColor { return UIColor(red: 0.26, green: 0.75, blue: 0.95, alpha: 1) }
        static var secondaryColor300: UIColor { return UIColor(red: 0.26, green: 0.75, blue: 0.95, alpha: 1) }
        static var secondaryColor200: UIColor { return UIColor(red: 0.48, green: 0.82, blue: 0.96, alpha: 1) }
        static var secondaryColor100: UIColor { return UIColor(red: 0.69, green: 0.89, blue: 0.98, alpha: 1) }
        static var secondaryColor000: UIColor { return UIColor(red: 0.88, green: 0.96, blue: 0.99, alpha: 1) }
        
        // Grey
        static var greyColor800: UIColor { return UIColor(red: 0.18, green: 0.22, blue: 0.28, alpha: 1) }
        static var greyColor700: UIColor { return UIColor(red: 0.29, green: 0.33, blue: 0.41, alpha: 1) }
        static var greyColor600: UIColor { return UIColor(red: 0.44, green: 0.5, blue: 0.59, alpha: 1) }
        static var greyColor500: UIColor { return UIColor(red: 0.63, green: 0.68, blue: 0.75, alpha: 1) }
        static var greyColor400: UIColor { return UIColor(red: 0.8, green: 0.84, blue: 0.88, alpha: 1) }
        static var greyColor300: UIColor { return UIColor(red: 0.89, green: 0.91, blue: 0.94, alpha: 1) }
        static var greyColor200: UIColor { return UIColor(red: 0.93, green: 0.95, blue: 0.97, alpha: 1) }
        static var greyColor100: UIColor { return UIColor(red: 0.97, green: 0.98, blue: 0.99, alpha: 1) }
        
        // Error Color
        static var errorColor100: UIColor { return UIColor(red: 0.93, green: 0.12, blue: 0.37, alpha: 1) }
        static var errorColor000: UIColor { return UIColor(red: 0.98, green: 0.44, blue: 0.51, alpha: 1) }
    }
    
    convenience init(hexString : String)
    {
        var hs = hexString
        if hs.hasPrefix("#") {
            hs = hs.replacingOccurrences(of: "#", with: "")
        }
        if let rgbValue = UInt(hs, radix: 16) {
            let red   =  CGFloat((rgbValue >> 16) & 0xff) / 255
            let green =  CGFloat((rgbValue >>  8) & 0xff) / 255
            let blue  =  CGFloat((rgbValue      ) & 0xff) / 255
            self.init(red: red, green: green, blue: blue, alpha: 1.0)
        } else {
            self.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        }
    }
}
