//
//  DeepLinkManager.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/2/7.
//  Copyright © 2023 myvolkswagen. All rights reserved.
//


import Foundation

/*
 Prod:
 註冊流程第四步Line綁定頁面-綁定失敗導回 myvolkswagen://register/line/fail
 註冊流程第四步Line綁定頁面-綁定成功導回popup myvolkswagen://register/line/success
 首頁 myvolkswagen://main
 我的車庫 myvolkswagen://car/mygarage
 半屏聯繫我們頁面 myvolkswagen://contact
 半屏我的服務中心 myvolkswagen://servicecenter
 保養維修首頁(tab) myvolkswagen://maintenance
 保養維修tab/召回活動 myvolkswagen://maintenance/recalls
 保養維修tab/專屬保修建議 myvolkswagen://maintenance/pat
 保養維修tab/鈑噴估價  myvolkswagen://maintenance/bodyandpaint
 保養維修tab/加修報價 myvolkswagen://maintenance/additionalquotation
 保養維修tab/預約保養 myvolkswagen://maintenance/reservation
 保養維修tab/紀錄頁面 myvolkswagen://maintenance/records
 保養維修tab/愛車保固 myvolkswagen://maintenance/warranty
 福斯人tab myvolkswagen://member
 福斯人tab/設定頁 myvolkswagen://member/setting
 福斯人tab/會員資料頁(設定服務中心) myvolkswagen://member/profile
 公版webview myvolkswagen://webview?url=
 愛車鑑價 myvolkswagen://cpo
 
 Dev:
 myvolkswagendev
*/

enum DeeplinkType {
    case LineBindingFail
    case LineBindingSuccess
    case Main
    case MyGarage
    case AddCar
    case Contact
    case ServiceCenter
    case Maintenance
    case MaintenanceRecalls
    case MaintenancePat
    case MaintenanceBodyAndPaint
    case MaintenanceAdditionalQuotation
    case MaintenanceReservation
    case MaintenanceRecords
    case MaintenanceWarranty
    case Member
    case MemberSetting
    case MemberProfile
    case MemberBenefits
    case MemberPrivacy
    case MemberTerms
    case MemberCoupon
    case Notification
    case NotificationCarOwner
    case NotificationNews
    case Webview
    case Cpo
    case DrivingAssistant
    case OpenHub
}

let Deeplinker = DeepLinkManager()
class DeepLinkManager {
    fileprivate init() {}
    private var deeplinkType: DeeplinkType?
    private var param: String?
    
    // check existing deepling and perform action
    func checkDeepLink() {
        guard let deeplinkType = deeplinkType else {
            return
        }
        
        DeeplinkNavigator.shared.proceedToDeeplink(deeplinkType, param: param)
        // reset deeplink after handling
        self.deeplinkType = nil // (1)
        self.param = nil
    }
    
    func handleDeeplink(url: URL, ref: String?) -> Bool {
        deeplinkType = DeeplinkParser.shared.parseDeepLink(url, ref: ref)
        param = url.query
        return deeplinkType != nil
    }
}
