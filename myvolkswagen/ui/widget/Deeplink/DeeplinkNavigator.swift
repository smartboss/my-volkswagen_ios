//
//  DeeplinkNavigator.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/2/7.
//  Copyright © 2023 myvolkswagen. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

var canStartDplk = false

class DeeplinkNavigator {
    static let shared = DeeplinkNavigator()
    var tmpType: DeeplinkType?
    var tmpParam: String?
    private init() {
        NotificationCenter.default.addObserver(self, selector: #selector(canStartDeeplink), name: Notification.Name.canStartDeeplink, object: nil)
    }
    
    @objc func canStartDeeplink() {
        print("canStartDeeplink")
        if let tt = tmpType {
            proceedToDeeplink(tt, param: tmpParam)
        }
    }
    
    func proceedToDeeplink(_ type: DeeplinkType, param: String?) {
        print("proceedToDeeplink")
        
        if !canStartDplk {
            print("proceedToDeeplink: can not StartDplk")
            tmpType = type
            tmpParam = param
            return
        }
        
        tmpType = nil
        tmpParam = nil
        
        print("proceedToDeeplink: switch type")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        switch type {
        case .LineBindingFail:
            // 註冊流程第四步Line綁定頁面-綁定失敗導回
            print("proceedToDeeplink: type LineBindingFail")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let bindLine: BindLineIDViewController = mController.viewControllers.last as? BindLineIDViewController {
                    //do nothing
                    print("proceedToDeeplink: type LineBindingFail, do nothing")
                } else if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    let sb = UIStoryboard(name: "Login", bundle: nil)
                    let vc = sb.instantiateViewController(withIdentifier: "BindLineIDViewController") as! BindLineIDViewController
                    mainVC.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                goInitialPage()
            }
        case .LineBindingSuccess:
            // 註冊流程第四步Line綁定頁面-綁定成功導回popup
            print("proceedToDeeplink: type LineBindingSuccess")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let bindLine: BindLineIDViewController = mController.viewControllers.last as? BindLineIDViewController {
                    bindLine.showSuccessMsg()
                } else if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    let sb = UIStoryboard(name: "Login", bundle: nil)
                    let vc = sb.instantiateViewController(withIdentifier: "BindLineIDViewController") as! BindLineIDViewController
                    vc.showSuccMsg = true
                    mainVC.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                goInitialPage()
            }
        case .Main:
            // 首頁
            print("proceedToDeeplink: type Main")
            goInitialPage()
        case .MyGarage:
            // 我的車庫
            print("proceedToDeeplink: type MyGarage")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    let vc = UIStoryboard(name: "Car", bundle: nil).instantiateViewController(withIdentifier: "MyGarageViewController") as! MyGarageViewController
                    mainVC.navigationController?.pushViewController(vc, animated: true)
                }
            }
        case .AddCar:
            // 新增車輛
            print("proceedToDeeplink: type AddCar")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    mainVC.goDeeplink(deeplink: type, param: param)
                }
            }
        case .Contact:
            // 聯絡我們
            print("proceedToDeeplink: type Contact")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    let sb = UIStoryboard(name: "Car", bundle: nil)
                    let bottomSheetVC = sb.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
                    
                    let options = SheetOptions(
                        pullBarHeight: 8,
                        presentingViewCornerRadius: 20,
                        shouldExtendBackground: true,
                        setIntrinsicHeightOnNavigationControllers: true,
                        useFullScreenMode: true,
                        shrinkPresentingViewController: true,
                        useInlineMode: false,
                        horizontalPadding: 0,
                        maxWidth: nil
                    )
                    
                    let sheetController = SheetViewController(
                        controller: bottomSheetVC,
                        sizes: [.fixed(210 + CGFloat(180+50.5).getFitSize())])
                    
                    sheetController.cornerRadius = 20
                    sheetController.minimumSpaceAbovePullBar = 0
                    sheetController.treatPullBarAsClear = false
                    sheetController.dismissOnOverlayTap = true
                    sheetController.dismissOnPull = true
                    
                    sheetController.shouldDismiss = { _ in
                        return true
                    }
                    sheetController.didDismiss = { _ in
                    }
                    mainVC.present(sheetController, animated: true, completion: nil)
                }
            }
        case .ServiceCenter:
            // 我的服務中心
            print("proceedToDeeplink: type ServiceCenter")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    let sb = UIStoryboard(name: "Car", bundle: nil)
                    let bottomSheetVC = sb.instantiateViewController(withIdentifier: "MyDealerInfoViewController") as! MyDealerInfoViewController
                    
                    let options = SheetOptions(
                        pullBarHeight: 8,
                        presentingViewCornerRadius: 20,
                        shouldExtendBackground: true,
                        setIntrinsicHeightOnNavigationControllers: true,
                        useFullScreenMode: true,
                        shrinkPresentingViewController: true,
                        useInlineMode: false,
                        horizontalPadding: 0,
                        maxWidth: nil
                    )
                    
                    let lbHeight = "「我的服務中心」讓您能夠選擇服務中心，掌握第一手服務內容與優惠訊息！\n \n・若您為曾經回服務中心保養過的福斯車主，則此欄位會自動設定為您最近一次造訪的服務中心。您也可以自行選取偏好的服務中心，此欄位將以您自行選擇的服務中心為主。\n \n您為尚未回服務中心過的車主或新車主，您可以先選取偏好的服務中心，若您未選取福斯系統會在您首次回服務中心保養後，自動設定為您最近一次造訪的服務中心。\n \n您還不是福斯車主，則可以忽略此欄位。\n \n・系統於每日早上 8 點更新回服務中心紀錄，此欄位可能非最即時的資料。 若您持有兩台以上的福斯車輛，則會以您第一台綁定的車輛資料為主。".getLabelStringHeightFrom(labelWidth: UiUtility.getScreenWidth() - 48, font: UIFont(name: "VWHead", size: CGFloat(15))!)
                    
                    let sheetController = SheetViewController(
                        controller: bottomSheetVC,
                        sizes: [.fixed(48+24+40+40+40 + lbHeight + CGFloat(60).getFitSize())])
                    
                    sheetController.cornerRadius = 20
                    sheetController.minimumSpaceAbovePullBar = 0
                    sheetController.treatPullBarAsClear = false
                    sheetController.dismissOnOverlayTap = true
                    sheetController.dismissOnPull = true
                    
                    sheetController.shouldDismiss = { _ in
                        return true
                    }
                    sheetController.didDismiss = { _ in
                    }
                    mainVC.present(sheetController, animated: true, completion: nil)
                }
            }
        case .Maintenance:
            // 保養維修首頁
            print("proceedToDeeplink: type Maintenance")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    if let presentedVC = mainVC.presentedViewController {
                        presentedVC.dismiss(animated: false) {
                        }
                    }
                    
                    if mController.children.count > 0 {
                        mController.popToRootViewController(animated: false)
                    }
                    
                        mainVC.goDeeplink(deeplink: type, param: param)
                    
                }
            }
        case .MaintenanceRecalls:
            // 保養維修tab/召回活動
            print("proceedToDeeplink: type MaintenanceRecalls")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    mainVC.goDeeplink(deeplink: type, param: param)
                }
            }
        case .MaintenancePat:
            // 保養維修tab/專屬保修建議
            print("proceedToDeeplink: type MaintenancePat")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    mainVC.goDeeplink(deeplink: type, param: param)
                }
            }
        case .MaintenanceBodyAndPaint:
            // 保養維修tab/鈑噴估價
            print("proceedToDeeplink: type MaintenanceBodyAndPaint")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    mainVC.goDeeplink(deeplink: type, param: param)
                }
            }
        case .MaintenanceAdditionalQuotation:
            // 保養維修tab/加修報價
            print("proceedToDeeplink: type MaintenanceAdditionalQuotation")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    mainVC.goDeeplink(deeplink: type, param: param)
                }
            }
        case .MaintenanceReservation:
            // 保養維修tab/預約保養
            print("proceedToDeeplink: type MaintenanceReservation")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    mainVC.goDeeplink(deeplink: type, param: param)
                }
            }
        case .MaintenanceRecords:
            // 保養維修tab/紀錄頁面
            print("proceedToDeeplink: type MaintenanceRecords")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    mainVC.goDeeplink(deeplink: type, param: param)
                }
            }
        case .MaintenanceWarranty:
            // 保養維修tab/愛車保固
            print("proceedToDeeplink: type MaintenanceWarranty")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    mainVC.goDeeplink(deeplink: type, param: param)
                }
            }
        case .DrivingAssistant:
            // 行車助理tab
            print("proceedToDeeplink: type DrivingAssistant")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    if let presentedVC = mainVC.presentedViewController {
                        presentedVC.dismiss(animated: false) {
                        }
                    }
                    
                    if mController.children.count > 0 {
                        mController.popToRootViewController(animated: false)
                    }
                    
                        mainVC.goDeeplink(deeplink: type, param: param)
                    
                }
            }
        case .Member:
            // 福斯人tab
            print("proceedToDeeplink: type Member")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    mainVC.goDeeplink(deeplink: type, param: param)
                }
            }
        case .MemberSetting:
            // 福斯人tab/設定頁
            print("proceedToDeeplink: type MemberSetting")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    mainVC.goDeeplink(deeplink: type, param: param)
                }
            }
        case .MemberProfile:
            // 福斯人tab/會員資料頁(設定服務中心)
            print("proceedToDeeplink: type MemberProfile")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    mainVC.goDeeplink(deeplink: type, param: param)
                }
            }
        case .MemberBenefits, .MemberPrivacy, .MemberTerms, .MemberCoupon:
            print("proceedToDeeplink: type Member subtabes")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    mainVC.goDeeplink(deeplink: type, param: param)
                }
            }
        case .Notification, .NotificationCarOwner:
            print("proceedToDeeplink: type NotificationCarOwner")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    let sb = UIStoryboard(name: "Car", bundle: nil)
                    let vc = sb.instantiateViewController(withIdentifier: "NotificationViewControllerV2") as! NotificationViewControllerV2
                    vc.tabSelectedIndex = 0
                    mainVC.navigationController?.pushViewController(vc, animated: true)
                }
            }
        case .NotificationNews:
            print("proceedToDeeplink: type NotificationNews")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    let sb = UIStoryboard(name: "Car", bundle: nil)
                    let vc = sb.instantiateViewController(withIdentifier: "NotificationViewControllerV2") as! NotificationViewControllerV2
                    vc.tabSelectedIndex = 1
                    mainVC.navigationController?.pushViewController(vc, animated: true)
                }
            }
        case .Webview:
            print("proceedToDeeplink: type Webview")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
                    vc.index = WebsiteIndex.loadUrl
                    
                    if param != nil {
                        if let components = URLComponents(string: "myvolkswagen://webview?\(param!)") {
                            var url: String?
                            var header: String?
                            var remainingParams = [URLQueryItem]()
                            
                            for item in components.queryItems ?? [] {
                                switch item.name {
                                case "url":
                                    url = item.value?.removingPercentEncoding
                                case "header":
                                    header = item.value?.removingPercentEncoding
                                default:
                                    remainingParams.append(item)
                                }
                            }
                            
                            let remainingString = remainingParams.map { "\($0.name)=\($0.value ?? "")" }.joined(separator: "&")
                            
                            print("url: \(url ?? "")")
                            print("header: \(header ?? "")")
                            vc.mytitle = header
                            print("string: \(remainingString)")
                            
                            if let urlString = url {
                                if let encodedString = (urlString + (remainingString.isEmpty ? "" : "?\(remainingString)")).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                                   let url = URL(string: encodedString) {
                                    vc.loadUrl = url
                                } else {
                                    print("無法創建URL")
                                }
                                
                                mainVC.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    }
                }
            }
        case .Cpo:
            print("proceedToDeeplink: type cpo")
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
                    vc.index = WebsiteIndex.loadUrl
                    vc.mytitle = "cpo_car".localized
                    if let paramm = param {
                        vc.loadUrl = URL(string: "\(Config.urlCpoCar)?\(paramm)")!
                    } else {
                        vc.loadUrl = URL(string: "\(Config.urlCpoCar)?\(getDeeplinkParams())")!
                    }
                    mainVC.navigationController?.pushViewController(vc, animated: true)
                }
            }
        case .OpenHub:
            print("proceedToDeeplink: type OpenHub")
            if CarManager.shared().openhubUrl != nil {
                if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                    if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                        mainVC.navigationController?.popToRootViewController(animated: false)
                        if CarManager.shared().openhubUrl != "" {
                            var uurl = "\(CarManager.shared().openhubUrl!)"
                            if CarManager.shared().openhubUrl!.contains("?") {
                                if let param = param {
                                    uurl = "\(uurl)&\(param)&\(getDeeplinkParams())"
                                }
                                print("proceedToDeeplink: type OpenHub url: \(uurl)")
                            } else {
                                if let param = param {
                                    uurl = "\(uurl)?\(param)&\(getDeeplinkParams())"
                                }
                                print("proceedToDeeplink: type OpenHub url: \(uurl)")
                                
                            }
                            mainVC.showOpenhubWebview(url: uurl)
                        }
                    }
                }
            } else {
                if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                    if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                        mainVC.showBGHud()
                    }
                }
                UserManager.shared().getProfileToCheckCellphone(param: param)
            }
        default:
            print("no event")
            break
        }
    }
    
    func getDeeplinkParams() -> String {
        let userToken = UserManager.shared().getUserToken()!
        var getvin = ""
        for cc in CarManager.shared().cars {
            if cc.licensePlateNumber == CarManager.shared().getSelectedCarNumber()! {
                getvin = cc.vin ?? ""
            }
        }
        let accId = UserManager.shared().currentUser.accountId!
        let licNum = CarManager.shared().getSelectedCarNumber()!
        let paramString = "token=\(userToken)&Vin=\(getvin)&AccountId=\(accId)&LicensePlateNumber=\(licNum)"
        return paramString
    }
    
    //    func navigateToVC(vc: UIViewController) {
    //        if let tabVC = UIApplication.topViewController() as? ESTabBarController {
    //            if let navvc = tabVC.viewControllers?[tabVC.selectedIndex] as? UINavigationController {
    //                navvc.pushViewController(vc, animated: true)
    //            }
    //        } else if let someVC = UIApplication.topViewController() {
    //            let nav = UINavigationController(rootViewController: vc)
    //            nav.modalPresentationStyle = .fullScreen
    //            someVC.present(nav, animated: true, completion: nil)
    //        }
    //    }
    
    func goViewController(vc: UIViewController) {
        if let topvc = getTopViewController() {
            if let nav = topvc as? UINavigationController {
                nav.show(vc, sender: self)
            } else {
                // 測試
                topvc.navigationController?.show(vc, sender: self)
            }
        }
    }
    
    func getTopViewController() -> UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController
    }
    
    func goInitialPage() {
        if UserManager.shared().currentUser.showProfile ?? false {
            // show profile editor
            let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewControllerV2") as! EditProfileViewControllerV2
            vc.type = .login
            getTopViewController()?.navigationController?.pushViewController(vc, animated: true)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.updateRootVC(showAlert: false)
        }
    }
    
    func goInitialPageWithDeeplink(deeplink: URL) {
        if UserManager.shared().currentUser.showProfile ?? false {
            // show profile editor
            let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewControllerV2") as! EditProfileViewControllerV2
            vc.type = .login
            getTopViewController()?.navigationController?.pushViewController(vc, animated: true)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.updateRootVC(showAlert: false)
            Deeplinker.checkDeepLink()
        }
    }
}
