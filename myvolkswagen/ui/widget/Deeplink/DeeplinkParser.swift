//
//  DeeplinkParser.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/2/7.
//  Copyright © 2023 myvolkswagen. All rights reserved.
//

import Foundation

class DeeplinkParser {
    static let shared = DeeplinkParser()
    private init() { }
    
    func parseDeepLink(_ url: URL, ref: String?) -> DeeplinkType? {
        guard let components = URLComponents(url: url, resolvingAgainstBaseURL: true),
            let _ = components.host else {
                return nil
        }
        
        if let sch = components.scheme {
#if DEVELOPMENT
            if sch != "myvolkswagendev" {
                return nil
            }
#else
            if sch != "myvolkswagen" {
                return nil
            }
#endif
        }
        
        switch components.host {
        case let x where x!.contains("register"):
            let fileArray = url.path.components(separatedBy: "/")
            let finalFileName = fileArray.last
            if finalFileName == "fail" {
                return .LineBindingFail
            } else {
                return .LineBindingSuccess
            }
        case let x where x!.contains("car"):
            let fileArray = url.path.components(separatedBy: "/")
            let finalFileName = fileArray.last
            if finalFileName == "mygarage" {
                return .MyGarage
            } else if finalFileName == "addcar" {
                return .AddCar
            }
        case let x where x!.contains("contact"):
            return .Contact
        case let x where x!.contains("servicecenter"):
            return .ServiceCenter
        case let x where x!.contains("maintenance"):
            let fileArray = url.path.components(separatedBy: "/")
            if fileArray.count == 1 {
                return .Maintenance
            }
            let finalFileName = fileArray.last
            if finalFileName == "recalls" {
                return .MaintenanceRecalls
            } else if finalFileName == "pat" {
                return .MaintenancePat
            } else if finalFileName == "bodyandpaint" {
                return .MaintenanceBodyAndPaint
            } else if finalFileName == "additionalquotation" {
                return .MaintenanceAdditionalQuotation
            } else if finalFileName == "reservation" {
                return .MaintenanceReservation
            } else if finalFileName == "records" {
                return .MaintenanceRecords
            } else if finalFileName == "warranty" {
                return .MaintenanceWarranty
            }
        case let x where x!.contains("drivingassistant"):
            let fileArray = url.path.components(separatedBy: "/")
            if fileArray.count == 1 {
                return .DrivingAssistant
            }
            return .DrivingAssistant
        case let x where x!.contains("member"):
            let fileArray = url.path.components(separatedBy: "/")
            if fileArray.count == 1 {
                return .Member
            }
            let finalFileName = fileArray.last
            if finalFileName == "profile" {
                return .MemberProfile
            } else if finalFileName == "setting" {
                return .MemberSetting
            } else if finalFileName == "coupon" {
                return .MemberCoupon
            } else if finalFileName == "benefits" {
                return .MemberBenefits
            } else if finalFileName == "privacy" {
                return .MemberPrivacy
            } else if finalFileName == "terms" {
                return .MemberTerms
            }
        case let x where x!.contains("notification"):
            let fileArray = url.path.components(separatedBy: "/")
            if fileArray.count == 1 {
                return .Notification
            }
            let finalFileName = fileArray.last
            if finalFileName == "carowner" {
                return .NotificationCarOwner
            } else if finalFileName == "news" {
                return .NotificationNews
            }
        case let x where x!.contains("main"):
            return .Main
        case let x where x!.contains("webview"):
            return .Webview
        case let x where x!.contains("cpo"):
            return .Cpo
        case let x where x!.contains("openhub"):
            return .OpenHub
        default:
            break
        }
        
        return nil
    }
}
