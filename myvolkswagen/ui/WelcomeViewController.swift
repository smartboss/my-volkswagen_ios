//
//  WelcomeViewController.swift
//  myvolkswagen
//
//  Created by Apple on 1/24/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Firebase


protocol LoginDataDelegate: class {
    func setLoginDataWith(code: String, token: String)
}

class WelcomeViewController: BaseViewController, LoginDataDelegate {
    
    
    
    // login data
    var code: String? = nil
    var token: String? = nil
    
    @IBOutlet weak var appName: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        appName.text = "app_name".localized
        appName.textColor = UiUtility.colorDarkGrey
        appName.font = UIFont(name: "VWHead-Bold", size: UiUtility.adaptiveSize(size: 22))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let code = self.code, let token = self.token {
            Logger.d("code: \(code), token: \(token)")
            
            if let code = self.code, let token = self.token {
                Logger.d("code: \(code), token: \(token)")
                
                Installations.installations().installationID { (installationID, error) in
                    if let error = error {
                        Logger.d("Error fetching remote instance ID: \(error)")
                        return
                    }
                    
                    guard let installationID = installationID else {
                        print("设备的安装 ID 为空")
                        return
                    }
                    
                    print("设备的安装 ID：\(installationID)")
                    Logger.d("Remote instance ID token: \(installationID)")
                    UserManager.shared().signIn(token: token, code: code, deviceId: installationID, listener: self)
                }
            }
            
//            InstanceID.instanceID().instanceID { (result, error) in
//                if let error = error {
//                    Logger.d("Error fetching remote instance ID: \(error)")
//                } else if let result = result {
//                    Logger.d("Remote instance ID token: \(result.token)")
//                    Logger.d("Remote instance ID instanceID: \(result.instanceID)")
//                    UserManager.shared().signIn(token: token, code: code, deviceId: result.instanceID, listener: self)
//                    //                    self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
//                }
//            }
        }
    }
    
    
    
    // MARK: LoginDataDelegate
    func setLoginDataWith(code: String, token: String) {
        self.code = code
        self.token = token
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    
    @IBAction private func loginClicked(_ sender: Any) {
        Analytics.logEvent("login", parameters: nil)
        
        if let vc: LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            vc.loginDataDelegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
