//
//  RedeemInfoViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2020/1/4.
//  Copyright © 2020 volkswagen. All rights reserved.
//

import UIKit

protocol RedeemInfoDismissDelegate: class {
    func didDismiss()
    func infoPageDidFinishRedeem()
}

class RedeemInfoViewController: BaseViewController {
    
    var viewTranslation = CGPoint(x: 0, y: 0)
    var redeem: Redeem?
    weak var delegate: RedeemInfoDismissDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var missionNameLabel: UILabel!
    @IBOutlet weak var confirmLabel: UILabel!
    @IBOutlet weak var noticeLabel: UILabel!
    @IBOutlet weak var redeemBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
        redeemBtn.setImage(UIImage(named: "redeem_pressed_long"), for: .highlighted)
        
        if let red = redeem {
            missionNameLabel.text = red.name
        }
        titleLabel.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
        subtitleLabel.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 16))
        missionNameLabel.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
        confirmLabel.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 16))
        noticeLabel.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
    }
    
    @objc func handleDismiss(sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .changed:
            viewTranslation = sender.translation(in: view)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.view.transform = CGAffineTransform(translationX: 0, y: self.viewTranslation.y)
            })
        case .ended:
            if viewTranslation.y < 200 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.view.transform = .identity
                })
            } else {
                delegate?.didDismiss()
                dismiss(animated: true, completion: nil)
            }
        default:
            break
        }
    }
    
    @IBAction func redeemBtnPressed(_ sender: Any) {
        if let rid = redeem?.redeemId {
            UserManager.shared().redeem(redeemId: String(rid), listener: self)
        }
    }
    
    // MARK: user protocol
    
    override func didStartRedeem() {
        showHud()
    }
    
    override func didFinishRedeem(success: Bool) {
        dismissHud()
        
        if success {
            dismiss(animated: true, completion: nil)
            delegate?.infoPageDidFinishRedeem()
        }
    }
}
