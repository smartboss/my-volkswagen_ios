//
//  ViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2020/1/3.
//  Copyright © 2020 volkswagen. All rights reserved.
//

import UIKit

class RedeemViewController: NaviViewController, UITableViewDataSource, UITableViewDelegate, RedeemInfoDismissDelegate, RedeemGetDismissDelegate {
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var darkBGView: UIView!
    @IBOutlet weak var memberContainer: UIView!
    
    var redeemRecord: [Redeem] = []
    var redeemList: [Redeem] = []
    var totalPoints: Int = 0
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        naviBar.setLeftButton(UIImage(named: "btn_back"), highlighted: UIImage(named: "btn_back"))
        naviBar.setUpSegmentControl(defaultIndex: selectedIndex)
        segmentDidChangeIndex(index: selectedIndex)
        
//        UserManager.shared().getRedeemRecord(listener: self)
//        UserManager.shared().getRedeemList(listener: self)
    }
    
    override func leftButtonClicked(_ button: UIButton) {
        back()
    }
    
    override func segmentDidChangeIndex(index: Int) {
        selectedIndex = index
        memberContainer.isHidden = !(index == 0)
        //tbView.reloadData()
        if selectedIndex == 0 {
        } else if selectedIndex == 1 {
            UserManager.shared().getRedeemRecord(listener: self)
        } else {
            UserManager.shared().getRedeemList(listener: self)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedIndex == 1 {
            return redeemRecord.count
        } else {
            return redeemList.count + 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if selectedIndex == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RedeemCell", for: indexPath)
            
            let lbContent: UILabel = cell.viewWithTag(1) as! UILabel
            let lbDate: UILabel = cell.viewWithTag(2) as! UILabel
            let lbDeadline: UILabel = cell.viewWithTag(3) as! UILabel
            let lbPoints: UILabel = cell.viewWithTag(4) as! UILabel
            let imgPoins: UIImageView = cell.viewWithTag(5) as! UIImageView
            let btnRedeem: UIButton = cell.viewWithTag(6) as! UIButton
            
            let reco = redeemRecord[indexPath.row]
            lbContent.text = reco.name
            lbDate.text = reco.getTime
            lbDeadline.text = "兌換期限: \(String(describing: reco.redeemExpired == nil ? "" : reco.redeemExpired!))"
            lbContent.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
            lbDate.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
            lbDeadline.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
            
            btnRedeem.isHidden = false
            lbPoints.isHidden = true
            imgPoins.isHidden = true
            if reco.status == 1 {
                // 未兌換
                lbContent.alpha = 1
                lbDate.alpha = 1
                lbDeadline.alpha = 1
                btnRedeem.setImage(UIImage.init(named: "redeem_normal"), for: .normal)
                btnRedeem.setImage(UIImage.init(named: "redeem_pressed"), for: .highlighted)
                btnRedeem.isUserInteractionEnabled = true
            } else if reco.status == 2 {
                // 已兌換
                lbContent.alpha = 0.3
                lbDate.alpha = 0.5
                lbDeadline.alpha = 0.5
                btnRedeem.setImage(UIImage.init(named: "redeem_disable"), for: .normal)
                btnRedeem.isUserInteractionEnabled = false
            } else {
                // 已逾期
                lbContent.alpha = 0.3
                lbDate.alpha = 0.5
                lbDeadline.alpha = 0.5
                btnRedeem.setImage(UIImage.init(named: "redeem_disable"), for: .normal)
                btnRedeem.isUserInteractionEnabled = false
            }
            return cell
        } else {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "RedeemHeaderCell", for: indexPath)
                let lbPoints: UILabel = cell.viewWithTag(1) as! UILabel
                let lbPointsTitle: UILabel = cell.viewWithTag(2) as! UILabel
                lbPoints.font = UIFont(name: "VWHead-Bold", size: UiUtility.adaptiveSize(size: 46))
                lbPointsTitle.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
                lbPoints.text = String(totalPoints)
                return cell
            } else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "RedeemInfoCell", for: indexPath)
                let lbInfoTitle: UILabel = cell.viewWithTag(1) as! UILabel
                lbInfoTitle.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "RedeemCell", for: indexPath)
                
                let lbContent: UILabel = cell.viewWithTag(1) as! UILabel
                let lbDate: UILabel = cell.viewWithTag(2) as! UILabel
                let lbDeadline: UILabel = cell.viewWithTag(3) as! UILabel
                let lbPoints: UILabel = cell.viewWithTag(4) as! UILabel
                let imgPoins: UIImageView = cell.viewWithTag(5) as! UIImageView
                let btnRedeem: UIButton = cell.viewWithTag(6) as! UIButton
                
                let reli = redeemList[indexPath.row - 2]
                lbContent.text = reli.name
                lbDate.text = reli.getTime
                lbDeadline.text = "領取期限: \(String(describing: reli.receiveExpired == nil ? "" : reli.receiveExpired!))"
                lbPoints.text = reli.point
                lbContent.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
                lbDate.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
                lbDeadline.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 10))
                lbPoints.font = UIFont(name: "VWHead-Bold", size: UiUtility.adaptiveSize(size: 18))
                
                btnRedeem.isHidden = true
                lbPoints.isHidden = false
                imgPoins.isHidden = false
                if reli.status == 1 {
                    // 未領取
                    lbContent.alpha = 1
                    lbDate.alpha = 1
                    lbDeadline.alpha = 1
                    lbPoints.alpha = 1
                    imgPoins.image = UIImage(named: "linepoint")
                } else if reli.status == 2 {
                    // 已領取
                    lbContent.alpha = 0.3
                    lbPoints.alpha = 0.3
                    lbDate.alpha = 0.5
                    lbDeadline.alpha = 0.5
                    imgPoins.image = UIImage(named: "linepointEd")
                } else {
                    // 已逾期
                    lbContent.alpha = 0.3
                    lbPoints.alpha = 0.3
                    lbDate.alpha = 0.5
                    lbDeadline.alpha = 0.5
                    imgPoins.image = UIImage(named: "linepointEd")
                }
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndex == 1 {
            let reco = redeemRecord[indexPath.row]
            if reco.status != 1 {
                return
            }
            
            let sb = UIStoryboard(name: "Redeem", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "RedeemInfoVC") as! RedeemInfoViewController
            vc.redeem = reco
            vc.delegate = self
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true) {
                self.darkBGView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.darkBGView.alpha = 1
                }) { (success) in
                }
            }
        } else {
            if indexPath.row == 1 {
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
                vc.index = WebsiteIndex.linePointsInfo
                self.navigationController?.pushViewController(vc, animated: true)
            } else if indexPath.row > 1 {
                let reli = redeemList[indexPath.row - 2]
//                if reli.status != 1 {
//                    return
//                }
                
                if let myid = reli.redeemId {
                    UserManager.shared().redeemReceive(redeemId: String(myid), redeem: reli, listener: self)
                }
            }
        }
    }
    
    @IBAction func didPressRedeemBtn(_ sender:AnyObject) {
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to: self.tbView)
        let indexPath = self.tbView.indexPathForRow(at: buttonPosition)
        if let idp = indexPath {
            tbView.selectRow(at: idp, animated: true, scrollPosition: .none)
            tbView.delegate?.tableView!(tbView, didSelectRowAt: idp)
        }
    }
    
    // MARK: info page delegate
    
    func didDismiss() {
        hideBGView()
    }
    
    func infoPageDidFinishRedeem() {
        hideBGView()
        //UserManager.shared().getRedeemRecord(listener: self)
        naviBar.segmentedControl.selectedSegmentIndex = 2
        naviBar.segmentedControlValueChanged(naviBar.segmentedControl)
    }
    
    // MARK: get page delegate
    
    func redeemGetdidDismiss() {
        hideBGView()
        UserManager.shared().getRedeemList(listener: self)
    }
    
    func hideBGView() {
        UIView.animate(withDuration: 0.3, animations: {
            self.darkBGView.alpha = 0
        }) { (success) in
            self.darkBGView.isHidden = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MemberLevelViewController,
            segue.identifier == "memberSeg" {
            vc.hideNavBar = true
        }
    }
    
    // MARK: user protocol
    
    override func didStartGetRedeemRecord() {
        showHud()
    }
    
    override func didFinishGetRedeemRecord(success: Bool, record: [Redeem]?) {
        dismissHud()
        if success {
            guard let re = record else { return }
            self.redeemRecord = re
        }
        self.tbView.reloadData()
    }
    
    override func didStartGetRedeemList() {
        showHud()
    }
    
    override func didFinishGetRedeemList(success: Bool, list: [Redeem]?, totalPoints: Int?) {
        dismissHud()
        if success {
            guard let li = list else { return }
            self.redeemList = li
            self.totalPoints = totalPoints == nil ? 0 : totalPoints!
        }
        self.tbView.reloadData()
    }
    
    override func didStartRedeemReceive() {
        showHud()
    }
    
    override func didFinishRedeemReceive(success: Bool, code: String?, redeem: Redeem?) {
        dismissHud()
        
        if let co = code {
            let decrypted = co.aesDecrypt(key: Config.redeemKey, iv: Config.redeemIv)
//            let decrypted = "FYAecqxqpy57Bt6i/sh4Dh5YNrc1to4c85Kl1VUmU6I=".aesDecrypt(key: "TzDma3z9MwYAsLjaPqxsMmE3G8ENpFvx", iv: "em8DRutVJ6yFJSYa")
            
            let sb = UIStoryboard(name: "Redeem", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "RedeemGetVC") as! RedeemGetViewController
            vc.redeem = redeem
            vc.code = decrypted
            vc.delegate = self
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true) {
                self.darkBGView.isHidden = false
                UIView.animate(withDuration: 0.3, animations: {
                    self.darkBGView.alpha = 1
                }) { (success) in
                }
            }
        }
    }
}
