//
//  RedeemGetViewController.swift
//  myvolkswagen
//
//  Created by Shelley on 2020/1/7.
//  Copyright © 2020 volkswagen. All rights reserved.
//

import UIKit
//import CryptoSwift
import IDZSwiftCommonCrypto


protocol RedeemGetDismissDelegate: class {
    func redeemGetdidDismiss()
}

class RedeemGetViewController: BaseViewController {
    
    var viewTranslation = CGPoint(x: 0, y: 0)
    var redeem: Redeem?
    var code: String?
    weak var delegate: RedeemGetDismissDelegate?
    @IBOutlet weak var copyBtn: UIButton!
    @IBOutlet weak var lineBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var missionNameLabel: UILabel!
    @IBOutlet weak var getPointLabel: UILabel!
    @IBOutlet weak var linePointsLabel: UILabel!
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var linePointCodeTitleLabel: UILabel!
    @IBOutlet weak var linePointCodeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleDismiss)))
        copyBtn.setImage(UIImage(named: "copyCode_default"), for: .normal)
        copyBtn.setImage(UIImage(named: "copyCode_pressed"), for: .highlighted)
        lineBtn.setImage(UIImage(named: "redeem_open_line_disable"), for: .disabled)
        lineBtn.isEnabled = false
        
        if let red = redeem {
            missionNameLabel.text = red.name
            linePointsLabel.text = red.point
        }
        
        if let co = code {
            linePointCodeLabel.text = co
        }
        
        titleLabel.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 16))
        subtitleLabel.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 16))
        missionNameLabel.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
        getPointLabel.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 16))
        linePointsLabel.font = UIFont(name: "VWHead-Bold", size: UiUtility.adaptiveSize(size: 22))
        instructionLabel.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 14))
        linePointCodeTitleLabel.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 12))
        linePointCodeLabel.font = UIFont(name: "PingFangTC-Regular", size: UiUtility.adaptiveSize(size: 15))
    }
    
    @objc func handleDismiss(sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .changed:
            viewTranslation = sender.translation(in: view)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.view.transform = CGAffineTransform(translationX: 0, y: self.viewTranslation.y)
            })
        case .ended:
            if viewTranslation.y < 200 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.view.transform = .identity
                })
            } else {
                delegate?.redeemGetdidDismiss()
                dismiss(animated: true, completion: nil)
            }
        default:
            break
        }
    }
    
    @IBAction func copyBtnPressed(_ sender: Any) {
        lineBtn.isEnabled = true
        UIPasteboard.general.string = linePointCodeLabel.text
        copyBtn.setImage(UIImage(named: "copyCode_normal"), for: .normal)
        if  let red = redeem,
            let rid = red.redeemId {
            UserManager.shared().redeemCopy(redeemId: String(rid), redeem: red, listener: self)
        }
        
        let alertController = UIAlertController(title: nil, message: "序號已複製", preferredStyle: .alert)
        present(alertController, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                alertController.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func lineBtnPressed(_ sender: Any) {
        if let url = URL(string: Config.urlLinePointsRedeem){
            UIApplication.shared.open(url)
        }
    }
}

extension String {
    /// AES解密
    ///
    /// - Parameters:
    ///   - key: key
    ///   - iv: iv
    /// - Returns: String
    func aesDecrypt(key: String, iv: String) -> String? {
//        var result: String?
//        do {
//            // 使用Base64的解碼方式將字串解碼後再轉换Data
//            let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0))!
//
//            // 用AES方式將Data解密
//            // test
//            let aesDec = try AES(key: key, iv: iv)
//            let dec = try aesDec.decrypt(data.bytes)
//
//            // 用UTF8的編碼方式將解完密的Data轉回字串
//            let desData: Data = Data(bytes: dec, count: dec.count)
//            result = String(data: desData, encoding: .utf8)!
//        } catch {
//            print("\(error.localizedDescription)")
//        }
//
//        return result
        
        let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0))!
        let algorithm = Cryptor.Algorithm.aes
        let plainText = data

        let cryptor = Cryptor(operation:.decrypt, algorithm:algorithm, options:.PKCS7Padding, key:key, iv:iv)
        let decryptedPlainText = cryptor.update(plainText)?.final()
        let decryptedString = String(bytes: decryptedPlainText!, encoding: .utf8)
        return decryptedString
    }
}
