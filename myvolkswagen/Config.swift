//
//  Config.swift
//  badminton
//
//  Created by Apple on 9/28/18.
//  Copyright © 2018 sofasogood. All rights reserved.
//

import UIKit

class Config: NSObject {
    
#if DEVELOPMENT
    static let redirectScheme: String = "myvolkswagen-dev"
    static let urlVWServer: String = "https://identity-sandbox.vwgroup.io/oidc/v1/authorize"
    static let clientId: String = "35f670e0-f120-44db-99fe-f4082f8470cb@apps_vw-dilab_com"
    static let nonce: String = "8jYKt0sTiPS7eeTx9KeRg6Wq12eOppa3"
    
    static let urlChangePassword: String = "https://id-sandbox.vwgroup.io/landing-page"
    
    static let urlMemberServer: String = "https://vwapp-demo.volkswagentaiwan.com.tw/api/"
    static let urlCommunityServer: String = "https://vwapp-demo.volkswagentaiwan.com.tw/api/"
    
    static let urlRoadLevelInfoV2: String = "https://cdn.vwapp-demo.volkswagentaiwan.com.tw/tisvFreeway/v2/road_section.json"
    static let urlRoadLevelValueV2: String = "https://cdn.vwapp-demo.volkswagentaiwan.com.tw/tisvFreeway/v2/road_live.json"
    static let urlCCTVInfoV2: String = "https://cdn.vwapp-demo.volkswagentaiwan.com.tw/tisvFreeway/v2/road_cctv.json"
    static let urlRoadLocationV2: String = "https://cdn.vwapp-demo.volkswagentaiwan.com.tw/tisvFreeway/v2/roadlevel_location.json"
    
    static let urlMemberExplanation: String = "https://vwapp-demo.volkswagentaiwan.com.tw/member_terms/level"
    static let urlMemberFaq: String = "https://vwapp-demo.volkswagentaiwan.com.tw/member_terms/faq"
    static let urlLinePointsInfo: String = "https://vwapp-demo.volkswagentaiwan.com.tw/member_terms/redeem"
    
    //static let urlChat: String = "http://61.56.209.157/api/"
    static let urlChat: String = "https://volkswagen-web-dev3.azurewebsites.net/api/"
    
    static let redeemKey: String = "TzDma3z9MwYAsLjaPqxsMmE3G8ENpFvx"
    static let redeemIv: String = "em8DRutVJ6yFJSYa"
    static let bnpUrl: String = "https://vjinc.cloud/vw/"
    //static let bnpUrl: String = "https://vjinc.cloud/bankeetest/vw/"
    static let recallUrl: String = "https://vjinc.cloud/vw/rp/"
    static let recallUrlSpecificCar: String = "https://vjinc.cloud/vw/r/"
    static let urlMyCar: String = "https://vwapp-demo.volkswagentaiwan.com.tw/webview/myCar.html"
    static let urlMemberCenter: String = "https://vwapp-demo.volkswagentaiwan.com.tw/webview/member.html"
    static let urlMemberCenterCouponList: String = "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/coupon"
    static let urlMemberCenterLevel: String = "https://vwapp-demo.volkswagentaiwan.com.tw/webview/memberLevel.html"
    static let urlAst: String = "https://sit-ast.vw-bodyandpaint.com/ast/"
    static let urlCruisys: String = "https://sit-api.vw-bodyandpaint.com:25443/api/volkswagen/"
    // Maintenance
    static let urlMaintenance: String = "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/maintenance"
    static let urlMaintenanceRecord: String = "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/maintenance/record"
    static let urlMaintenancePat: String = "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/pat"
    static let urlMaintenanceBodyAndPaint: String = "https://sit.vw-bodyandpaint.com/vw/"
    static let urlMaintenanceRecall: String = "http://sit-recall.vw-bodyandpaint.com/vw/r"
    static let urlMaintenanceAdditional: String = "https://sit-service-cam.volkswagen-service.com/"
    static let urlMaintenanceWarranty: String = "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/warranty"
    static let urlMaintenanceReservation: String = "https://sit-ast.volkswagen-service.com/ast/"
    // ---------
    static let urlMemberCenterV2: String = "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/member/user"
    static let urlLineLiff: String = "https://liff.line.me/1655984739-AabB5xZq?token="
    static let urlCpoCar: String = "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/connectToCPO.html"
    static let urlMemberBenefit: String = "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/member/member-benefits"
    static let urlMemberPrivacy: String = "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/privacy-Statement"
    static let urlMemberTerms: String = "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/terms"
    static let urlMemberUserEdit: String = "https://vwapp-demo.volkswagentaiwan.com.tw/webview/new/member/user/edit"
    static let urlOnlineService: String = "https://consulting-service-pre.volkswagentaiwan.com.tw/join?category_id=3"
#else
    static let redirectScheme: String = "myvolkswagen"
    static let urlVWServer: String = "https://identity.vwgroup.io/oidc/v1/authorize"
    static let clientId: String = "e3ed3ac1-453e-46cd-b1ef-49bde85350ff@apps_vw-dilab_com"
    static let nonce: String = "joza7h9au82wlcfKzqeE1O2J1Ki3p86Y"
    
    static let urlChangePassword: String = "https://vwid.vwgroup.io/landing-page"
    
    static let urlMemberServer: String = "https://vwapp.volkswagentaiwan.com.tw/api/"
    static let urlCommunityServer: String = "https://vwapp.volkswagentaiwan.com.tw/api/"
    
    static let urlRoadLevelInfoV2: String = "https://cdn.vwapp.volkswagentaiwan.com.tw/tisvFreeway/v2/road_section.json"
    static let urlRoadLevelValueV2: String = "https://cdn.vwapp.volkswagentaiwan.com.tw/tisvFreeway/v2/road_live.json"
    static let urlCCTVInfoV2: String = "https://cdn.vwapp.volkswagentaiwan.com.tw/tisvFreeway/v2/road_cctv.json"
    static let urlRoadLocationV2: String = "https://cdn.vwapp.volkswagentaiwan.com.tw/tisvFreeway/v2/roadlevel_location.json"
    
    static let urlMemberExplanation: String = "https://vwapp.volkswagentaiwan.com.tw/member_terms/level"
    static let urlMemberFaq: String = "https://vwapp.volkswagentaiwan.com.tw/member_terms/faq"
    static let urlLinePointsInfo: String = "https://vwapp.volkswagentaiwan.com.tw/member_terms/redeem"
    
    static let urlChat: String = "https://volkswagen-web.azurewebsites.net/api/"
    
    static let redeemKey: String = "pROqtn1AeuHL3Z8k8ZsN5ndyQedO39Ea"
    static let redeemIv: String = "kTwGp9aLh8Qozf5U"
    static let bnpUrl: String = "https://vw-bodyandpaint.com/vw/"
    static let recallUrl: String = "https://vw-bodyandpaint.com/vw/rp/"
    static let recallUrlSpecificCar: String = "https://vw-bodyandpaint.com/vw/r/"
    static let urlMyCar: String = "https://vwapp.volkswagentaiwan.com.tw/webview/myCar.html"
    static let urlMemberCenter: String = "https://vwapp.volkswagentaiwan.com.tw/webview/member.html"
    static let urlMemberCenterCouponList: String = "https://vwapp.volkswagentaiwan.com.tw/webview/new/coupon"
    static let urlMemberCenterLevel: String = "https://vwapp.volkswagentaiwan.com.tw/webview/memberLevel.html"
    static let urlAst: String = "https://ast.volkswagen-service.com/ast/"
    static let urlCruisys: String = "https://api.vw-bodyandpaint.com/api/volkswagen/"
    // Maintenance
    static let urlMaintenance: String = "https://vwapp.volkswagentaiwan.com.tw/webview/new/maintenance"
    static let urlMaintenanceRecord: String = "https://vwapp.volkswagentaiwan.com.tw/webview/new/maintenance/record"
    static let urlMaintenancePat: String = "https://vwapp.volkswagentaiwan.com.tw/webview/new/pat"
    static let urlMaintenanceBodyAndPaint: String = "https://vw-bodyandpaint.com/vw/"
    static let urlMaintenanceRecall: String = "http://recall.vw-bodyandpaint.com/vw/r"
    static let urlMaintenanceAdditional: String = "https://service-cam.volkswagen-service.com"
    static let urlMaintenanceWarranty: String = "https://vwapp.volkswagentaiwan.com.tw/webview/new/warranty"
    static let urlMaintenanceReservation: String = "https://ast.volkswagen-service.com/ast/"
    // ---------
    static let urlMemberCenterV2: String = "https://vwapp.volkswagentaiwan.com.tw/webview/new/member/user"
    static let urlLineLiff: String = "https://liff.line.me/1655984733-vm1KoyD4?token="
    static let urlCpoCar: String = "https://vwapp.volkswagentaiwan.com.tw/webview/new/connectToCPO.html"
    static let urlMemberBenefit: String = "https://vwapp.volkswagentaiwan.com.tw/webview/new/member/member-benefits"
    static let urlMemberPrivacy: String = "https://vwapp.volkswagentaiwan.com.tw/webview/new/privacy-Statement"
    static let urlMemberTerms: String = "https://vwapp.volkswagentaiwan.com.tw/webview/new/terms"
    static let urlMemberUserEdit: String = "https://vwapp.volkswagentaiwan.com.tw/webview/new/member/user/edit"
    static let urlOnlineService: String = "https://consulting-service.volkswagentaiwan.com.tw/join?category_id=3"
#endif
    
    
    
    static let maxCarCount: Int = 3
    
    // webs
    static let urlReservation: String = "https://www.volkswagen.com.tw/app/testdrive/default.aspx"
//    static let urlVisualizer360: String = "http://demo.volkswagentaiwan.com.tw/volkswagen_360visualizer/index.html"
    static let urlVisualizer360: String = "https://www.volkswagentaiwan.com.tw/volkswagen_visualizer/"
    
    static let urlDealer: String = "https://www.volkswagen.com.tw/zh/find-a-dealer.html?lat-app=23.77479&lng-app=120.9428&zoom-app=7"
    static let urlOfficial: String = "https://www.volkswagen.com.tw/zh.html"
    
    static let urlNearbyServiceCenter: String = "https://www.volkswagen.com.tw/zh/find-a-dealer.html?lat-app=23.77479&lng-app=120.9428&zoom-app=7"
//    static let urlETag: String = "https://www.fetc.net.tw/UX/?GnOunE=k7JecfpvAKxaCqdj23e80gAobXzAVDtazmFf0E.5bSIi7QszVI5ZpmA6iDTrthWD8aAvmD_tfns_cX3bG6F3Tat4qQ1IwydvhkKDhyDz8Cmc9zpCvlp0rlAR8dMSMnwbcZuCdkRc7Yn5wfcl"
//    static let urlETag: String = "https://www.fetc.net.tw/UX/?GnOunE=k7iweWADTU28J36Ktbq9ExThL5lG29PYveEzoGDf3JkuHV9pLB9V8YlLJVwTCRhcLHo7OeVYXqZM5_pE8KoUXMyJw3fqkTJALGEcyYZ0BMydFLasNyDLa3xWWBR.UddTmEAfGqSzBbFHfsqS"
    static let urlETag: String = "https://www.fetc.net.tw/"
    static let urlSupervisionStation: String = "https://www.mvdis.gov.tw/m3-emv-vil/vil/penaltyQueryPay"
    
    static let urlFacebook: String = "https://www.facebook.com/VolkswagenTaiwan"
    static let urlInstagram: String = "https://www.instagram.com/volkswagentaiwan/"
    static let urlYoutube: String = "https://www.youtube.com/channel/UCvAJOkPViSm0VKMmgOjXzqw/featured"
    static let urlLinePointsRedeem: String = "https://points.line.me/pointcode/#/pointcode/form"
    static let urlEShop: String = "https://www.volkswagen-eshop.com/"
    
    // phone
    static let numberRescue: String = "0800828818"
    static let numberPolice: String = "110"
    
    // chatbot grading count down
    static let gradingSeconds: TimeInterval = 25
    static let urlContactUs: String = "https://www.volkswagen.com.tw/app/contactus/index.aspx?utm_source=VWAPP&utm_medium=member&utm_campaign=contactus"
}
