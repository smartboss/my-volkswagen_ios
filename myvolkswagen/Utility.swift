//
//  Utility.swift
//  badminton
//
//  Created by Apple on 10/12/18.
//  Copyright © 2018 sofasogood. All rights reserved.
//

import UIKit
import Firebase
import Alamofire
import SwiftyJSON


class Utility: NSObject {
    
    static let secondsInMinute: Double = 60
    static let secondsInHour: Double = secondsInMinute * 60
    static let secondsInDay: Double = secondsInHour * 24
    static let secondsInWeek: Double = secondsInDay * 7

//    static func getFullPath(path: String) -> String {
//        return Config.URL_API_SERVER + path
//    }
    
    static func isEmpty(_ str: String?) -> Bool {
        return (str ?? "").isEmpty
    }
    
//    static func dateString(from date: Date) -> String {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//
//        let str = formatter.string(from: date)
//        return str
//    }
//
//    static func date(from str: String) -> Date? {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
//
//        // convert your string to date
//        let date = formatter.date(from: str)
//
//        return date
//    }
    
    static func versionText() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
//        let build = dictionary["CFBundleVersion"] as! String
        
//        return "\(version) (\(build))"
        return "\(version)"
    }
    
    static func versionFloat() -> Float {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        return (version as NSString).floatValue
    }
    
    // MARK: phone
    static func call(number: String) {
        guard let number = URL(string: "telprompt://" + number) else { return }
        UIApplication.shared.open(number)
    }
    
    
    // MARK: format
    
    static func isValidEmail(testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static func isValidPhoneNumber(testStr: String) -> Bool {
        if testStr.count != 10 {
            return false
        }
        
        return testStr.starts(with: "09")
    }
    
    
    
    // MARK: image
    static func getBase64Image(image: UIImage) -> String {
        let imageData: Data = image.jpegData(compressionQuality: 0.3)!
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        //        Logger.d("strBase64: \(strBase64)")
        
        return "data:image/jpeg;base64,\(strBase64)"
    }
    
    
    
    // MARK: share
    
    static func shareAll(text: String?, image: UIImage, controller: UIViewController) {
        Analytics.logEvent("post_share", parameters: nil)
        
//        let myWebsite = NSURL(string:"https://stackoverflow.com/users/4600136/mr-javed-multani?tab=profile")
        
        var items: [Any] = []
        if let text = text {
            items = [text, image/*, myWebsite*/]
        } else {
            items = [image/*, myWebsite*/]
        }
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = controller.view
        controller.present(activityViewController, animated: true, completion: nil)
    }
    
    
    
    @discardableResult static func checkTokenValidation(of result: Result) -> Bool {
        if let errorCode = result.errorCode {
            if errorCode == 1001 {
                Logger.d("1001!")
                UserManager.shared().signOutLocal(showAlert: false)
                return false
            }
        }
        return true
    }
    
    static func sendEventLog(ServiceName: String, UniqueId: String, EventName: String, InboxId: String, EventCategory: String, PageName: String, EventLabel: String, eventJson: [String: Any]?, utmSource: String?, utmMedium: String?, utmCampaign: String?) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateStr = formatter.string(from: Date())
        var eventj = eventJson
        let innerJsonObject: [String: Any] = [
            "event_category": EventCategory,
            "pageName": PageName,
            "eventLabel": EventLabel,
            "utm_source": utmSource ?? "",
            "utm_medium": utmMedium ?? "",
            "utm_campaign": utmCampaign ?? ""
        ]
        if eventj == nil {
            eventj = innerJsonObject
        }
        let parameters: Parameters = [
            "Level": UserManager.shared().currentUser.level ?? "",
            "AccountId": UserManager.shared().currentUser.accountId ?? "",
            "ServiceName": ServiceName,
            "UniqueId": UniqueId,
            "EventName": EventName,
            "Timestamp": dateStr,
            "InboxId": InboxId,
            "event_category": EventCategory,
            "pageName": PageName,
            "eventLabel": EventLabel,
            "EventJSON": eventj!
        ]
        Alamofire.request(Config.urlMemberServer + "APP_Add_App_Event_Log?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters, encoding: JSONEncoding.default).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                })
                
        }
    }

    static func timeDifferenceString(from dateString: String) -> String? {
        // 创建日期格式化器以解析输入日期字符串
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm"

        // 将输入日期字符串解析为 Date 对象
        if let date = dateFormatter.date(from: dateString) {
            // 获取当前时间
            let currentDate = Date()
            
            // 计算日期差异
            let calendar = Calendar.current
            let components = calendar.dateComponents([.minute, .hour, .day, .month, .year], from: date, to: currentDate)

            // 根据差异生成字符串
            if let yearDiff = components.year, yearDiff > 0 {
                return "\(yearDiff)年前"
            } else if let monthDiff = components.month, monthDiff > 0 {
                return "\(monthDiff)月前"
            } else if let dayDiff = components.day, dayDiff > 0 {
                return "\(dayDiff)天前"
            } else if let hourDiff = components.hour, hourDiff > 0 {
                return "\(hourDiff)小時前"
            } else if let minuteDiff = components.minute, minuteDiff > 0 {
                return "\(minuteDiff)分鐘前"
            } else {
                return "剛剛"
            }
        }

        return nil
    }
    
    static func timeDifferenceStringV2(from dateString: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"

        if let date = dateFormatter.date(from: dateString) {
            let currentDate = Date()
            
            let calendar = Calendar.current
            let components = calendar.dateComponents([.minute, .hour, .day, .month, .year], from: date, to: currentDate)

            if let yearDiff = components.year, yearDiff > 0 {
                return "\(yearDiff)年前"
            } else if let monthDiff = components.month, monthDiff > 0 {
                return "\(monthDiff)月前"
            } else if let dayDiff = components.day, dayDiff > 0 {
                return "\(dayDiff)天前"
            } else if let hourDiff = components.hour, hourDiff > 0 {
                return "\(hourDiff)小時前"
            } else if let minuteDiff = components.minute, minuteDiff > 0 {
                return "\(minuteDiff)分鐘前"
            } else {
                return "剛剛"
            }
        }

        return nil
    }
    
    static func compareDates(dateStringA: String, dateStringB: String) -> ComparisonResult? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm"
        
        if let dateA = dateFormatter.date(from: dateStringA), let dateB = dateFormatter.date(from: dateStringB) {
            return dateA.compare(dateB)
        } else {
            // 日期格式无效
            return nil
        }
    }
}

extension String {
    func extractTime() -> String? {
        let components = self.components(separatedBy: " ")
        if components.count >= 2 {
            return components[1]
        }
        return nil
    }
    
    func truncateString(inputString: String, maxLength: Int) -> String {
        if inputString.count > maxLength {
            let index = inputString.index(inputString.startIndex, offsetBy: maxLength)
            return String(inputString[..<index])
        } else {
            return inputString
        }
    }
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0)) else {
            return nil
        }
        
        return String(data: data as Data, encoding: String.Encoding.utf8)
    }
    
    func toBase64() -> String? {
        guard let data = self.data(using: String.Encoding.utf8) else {
            return nil
        }
        
        return data.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }
}

extension Int {
    func metersToKilometers() -> String {
        let kilometers = Double(self) / 1000.0 // 將公尺轉換為公里
        let formattedKilometers = String(format: "%.2f", kilometers) // 格式化公里數（保留兩位小數）
        return formattedKilometers
    }
}

extension NSAttributedString {
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.width)
    }
}

extension String {
    /// 根据字符串 获取 Label 控件的高度
    func getLabelStringHeightFrom(labelWidth: CGFloat, font: UIFont) -> CGFloat {
        let topOffset = CGFloat(0)
        let bottomOffset = CGFloat(0)
        let textContentWidth = labelWidth
        let normalText: NSString = self as NSString
        let size = CGSize(width: textContentWidth, height: 1000)
        let attributes = [NSAttributedString.Key.font: font]
        let stringSize = normalText.boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context:nil).size
        return  CGFloat(ceilf(Float(stringSize.height)))+topOffset+bottomOffset
    }
}
