//
//  UserManager.swift
//  badminton
//
//  Created by Apple on 9/24/18.
//  Copyright © 2018 sofasogood. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import UserNotifications
import Firebase


protocol UserProtocol: class {
    func didStartLogin()
    func didFinishLogin(success: Bool)
    
    func didStartPhoneLogin()
    func didFinishPhoneLogin(success: Bool, code: Int?, msg: String?)
    
    func didStartLogout()
    func didFinishLogout(success: Bool)
    
    func didStartBindFb()
    func didFinishBindFb(success: Bool, isBind: Bool)
    
    func didStartBindLine()
    func didFinishBindLine(success: Bool, isBind: Bool)
    
    func didStartUnbindLine()
    func didFinishUnbindLine(success: Bool, isBind: Bool)
    
    func didStartBindVWId()
    func didFinishBindVWId(success: Bool, isBind: Bool)
    
    func didStartGetQrcode()
    func didFinishGetQrcode(success: Bool, qrcodeImage: UIImage?)
    
    func didStartFollow()
    func didFinishFollow(success: Bool, accountId: String, set: Bool)
    
    func didStartGetRelation()
    func didFinishGetRelation(success: Bool, isFollower: Bool, relations: [Relation]?)
    
    func didStartUpdatePushToken()
    func didFinishUpdatePushToken(success: Bool)
    
    func didStartSearch()
    func didFinishSearch(success: Bool, relations: [Relation]?)
    
    func didStartGetRecentlyBrosedMember()
    func didFinishGetRecentlyBrosedMember(success: Bool, relations: [Relation]?)
    
    func didStartGetMemberLevelInfo()
    func didFinishGetMemberLevelInfo(success: Bool, level: Int?, levelName: String?, levelIntro: String?)
    
    func didStartGetMemberLevelInfoWebview()
    func didFinishGetMemberLevelInfoWebview(success: Bool, level: Int?, levelName: String?)
    
    func didStartGetRedeemState()
    func didFinishGetRedeemState(success: Bool, taskCount: Int?, totalPoints: Int?)
    
    func didStartGetRedeemRecord()
    func didFinishGetRedeemRecord(success: Bool, record: [Redeem]?)
    
    func didStartRedeem()
    func didFinishRedeem(success: Bool)
    
    func didStartGetRedeemList()
    func didFinishGetRedeemList(success: Bool, list: [Redeem]?, totalPoints: Int?)
    
    func didStartRedeemReceive()
    func didFinishRedeemReceive(success: Bool, code: String?, redeem: Redeem?)
    
    func didStartRedeemCopy()
    func didFinishRedeemCopy(success: Bool, redeem: Redeem?)
    
    func didStartGetUserMaintenancePlant()
    func didFinishGetUserMaintenancePlant(success: Bool, maintenancePlant: MaintainancePlant?)
    
    func didStartRecallCheck()
    func didFinishRecallCheck(success: Bool, showNoticeBoard: Bool, recellUrl: String?)
    
    func didStartLoginCruisys()
    func didFinishLoginCruisys(success: Bool)
}

protocol ProfileProtocol: class {
    func didStartGetProfile()
    func didFinishGetProfile(success: Bool, profile: Profile?)
    
    func didStartUpdateProfile()
    func didFinishUpdateProfile(success: Bool, profile: Profile?, msg: String?)
}

class UserManager: NSObject {
    
    let keyToken = "key_user_token"
    let keyVWToken = "key_vwid_token"
    let keyCruisysToken = "key_cruisys_user_token"
    let keyPushToken = "key_push_token"
    let keyUserInstruction = "key_user_instruction"
    let keyUserCarAssistantReminder = "key_user_car_assistant_reminder"
    
    // MARK: - Properties
    
    private static var sharedUserManager: UserManager = {
        let userManager = UserManager()
        
        // Configuration
        // ...
        
        return userManager
    }()
    
    // MARK: -
    
    var currentUser: User
    private var profile: Profile?
    var showNoticeBoard: Bool?
    var recallUrl: String?
    var recallIsLoading = false
    
    var relationFollowing: [Relation] = []
    var relationFollower: [Relation] = []
    
    // Initialization
    override private init() {
        self.currentUser = User()
        
        super.init()
    }
    
    // MARK: - Accessors
    
    class func shared() -> UserManager {
        return sharedUserManager
    }
    
    func getOTPFlag() -> Int {
        if let pro = profile {
            return pro.OTPFlag
        }
        return 0
    }
    
    func setOTPFlag(flag: Int) {
        if let pro = profile {
            pro.OTPFlag = flag
        }
    }
    
    func isSignedIn() -> Bool {
        if let token = getUserToken() {
            if !Utility.isEmpty(token) {
                return true
            }
        }
        
        return false
    }
    
    
    
    func autoLogin() {
        //        Logger.d("getUserToken(): \(getUserToken()!)")
        Alamofire.request(Config.urlMemberServer + "APP_AutoLogin?token=\(getUserToken()!)", method: .post, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //            Logger.d("Request: \(String(describing: response.request))")   // original url request
                //            Logger.d("Response: \(String(describing: response.response))") // http url response
                //            Logger.d("Result: \(response.result)")                         // response serialization result
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        if let token: String = responseJson["UserToken"].string {
                            //                            Logger.d("token: \(token)")
                            self.saveUserToken(with: token)
                            
                            self.currentUser = User(with: responseJson["UserData"])
                            self.currentUser.save()
                            //                            Logger.d("load user from server response: \(self.currentUser.description)")
                            
                            // user had logged in already so app doesn't need to update ui
                            //                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            //                            appDelegate.updateRootVC()
                        } else {
                            Logger.e("no token found")
                            self.signOut(listener: nil, showAlert: false)
                        }
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        if !Utility.checkTokenValidation(of: result) {
                            self.signOut(listener: nil, showAlert: false)
                        }
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    self.signOut(listener: nil, showAlert: false)
                })
                
            }
    }
    
    func loginCruisys(listener: UserProtocol?, accountId: String, plateNum: String) {
        listener?.didStartLoginCruisys()
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        let stringWithoutDash = plateNum.replacingOccurrences(of: "-", with: "")

        var parameters: Parameters = [
            "account_id": accountId,
            "plate_no": stringWithoutDash
        ]
        //        Logger.d("getUserToken(): \(getUserToken()!)")
        Alamofire.request(Config.urlCruisys + "service_go/ext_id_auth", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    if let token: String = responseJson["data"]["access_token"].string {
                        //                            Logger.d("token: \(token)")
                        self.saveCruisysUserToken(with: token)
                        print("saveCruisysUserToken token: \(token)")
                        listener?.didFinishLoginCruisys(success: true)
                    } else {
                        Logger.e("no token found")
                        self.clearCruisysUserToken()
                        listener?.didFinishLoginCruisys(success: false)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    self.clearCruisysUserToken()
                    listener?.didFinishLoginCruisys(success: false)
                })
                
            }
    }
    
    func signIn(token: String, code: String, deviceId: String, listener: UserProtocol?) {
        let parameters: Parameters = [
            "id_token": token,
            "auth_code": code,
            "device_id": deviceId
        ]
        
        //        Logger.d("signIn parameters: \(parameters)")
        
        listener?.didStartLogin()
        
        Alamofire.request(Config.urlMemberServer + "APP_VWID_Login", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //            Logger.d("Request: \(String(describing: response.request))")   // original url request
                //            Logger.d("Response: \(String(describing: response.response))") // http url response
                //            Logger.d("Result: \(response.result)")                         // response serialization result
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        if let tokenn: String = responseJson["UserToken"].string {
                            //                            Logger.d("token: \(token)")
                            self.saveUserToken(with: tokenn)
                            
                            self.currentUser = User(with: responseJson["UserData"])
                            self.currentUser.save()
                            //                            Logger.d("load user from server response: \(self.currentUser.description)")
                            
                            //                        self.registerForPushNotifications()
                            
                            //                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            //                        appDelegate.updateRootVC()
                            
                            // start to get notice unread count
                            NoticeManager.shared().startTimerUnreadCount()
                            listener?.didFinishLogin(success: true)
                            if let tt = self.getPushToken() {
                                self.updatePushToken(token: tt, listener: nil)
                            }
                        } else {
                            Logger.e("no token found")
                            listener?.didFinishLogin(success: false)
                        }
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishLogin(success: false)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishLogin(success: false)
                })
                
            }
    }
    
    func signOut(listener: UserProtocol?, showAlert: Bool) {
        if let token = getUserToken() {
            listener?.didStartLogout()
            Alamofire.request(Config.urlMemberServer + "APP_Logout?token=\(token)", method: .post, parameters: nil).validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"]).responseJSON { response in
                    //            Logger.d("Request: \(String(describing: response.request))")   // original url request
                    //            Logger.d("Response: \(String(describing: response.response))") // http url response
                    //            Logger.d("Result: \(response.result)")                         // response serialization result
                    
                    response.result.ifSuccess({
                        //                        Logger.d(response.result.value as Any)
                        let responseJson = JSON.init(rawValue: response.result.value as Any)!
                        
                        let result: Result = Result(with: responseJson)
                        //                        Logger.d("result: \(result)")
                        if result.isSuccess() {
                            
                            listener?.didFinishLogout(success: true)
                            self.signOutLocal(showAlert: showAlert)
                        } else {
                            Logger.d("errorCode: \(String(describing: result.errorCode))")
                            Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                            listener?.didFinishLogout(success: false)
                        }
                    })
                    
                    response.result.ifFailure({
                        Logger.d("error: \(response.error as Any)")
                        listener?.didFinishLogout(success: false)
                    })
                    
                }
        } else {
            Logger.e("try to logout without user token")
        }
    }
    
    func phoneSignIn(code: String, deviceId: String, mobile: String, mobilePrefix: String, listener: UserProtocol?) {
        let parameters: Parameters = [
            "mobile_prefix": mobilePrefix,
            "mobile": mobile,
            "code": code,
            "device_id": deviceId
        ]
        //        Logger.d("signIn parameters: \(parameters)")
        
        listener?.didStartLogin()
        
        Alamofire.request(Config.urlMemberServer + "APP_Phone_Login", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //            Logger.d("Request: \(String(describing: response.request))")   // original url request
                //            Logger.d("Response: \(String(describing: response.response))") // http url response
                //            Logger.d("Result: \(response.result)")                         // response serialization result
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        if let token: String = responseJson["UserToken"].string {
                            //                            Logger.d("token: \(token)")
                            self.saveUserToken(with: token)
                            
                            self.currentUser = User(with: responseJson["UserData"])
                            self.currentUser.save()
                            //                            Logger.d("load user from server response: \(self.currentUser.description)")
                            
                            //                        self.registerForPushNotifications()
                            
                            //                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            //                        appDelegate.updateRootVC()
                            
                            // start to get notice unread count
                            NoticeManager.shared().startTimerUnreadCount()
                            
                            listener?.didFinishPhoneLogin(success: true, code: nil, msg: nil)
                        } else {
                            Logger.e("no token found")
                            listener?.didFinishPhoneLogin(success: false, code: nil, msg: nil)
                        }
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishPhoneLogin(success: false, code: result.errorCode, msg: result.errorMsg)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishPhoneLogin(success: false, code: nil, msg: nil)
                })
                
            }
    }
    
    func bindVWId(token: String, code: String, deviceId: String, listener: UserProtocol?) {
        let parameters: Parameters = [
            "token": self.getUserToken()!,
            "id_token": token,
            "auth_code": code,
            "device_id": deviceId
        ]
        
        //        Logger.d("signIn parameters: \(parameters)")
        
        listener?.didStartBindVWId()
        
        Alamofire.request(Config.urlMemberServer + "APP_Bind_VWID", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //            Logger.d("Request: \(String(describing: response.request))")   // original url request
                //            Logger.d("Response: \(String(describing: response.response))") // http url response
                //            Logger.d("Result: \(response.result)")                         // response serialization result
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        if let tokenn: String = responseJson["UserToken"].string {
                            //                            Logger.d("token: \(token)")
                            self.saveUserToken(with: tokenn)
                            
                            self.currentUser = User(with: responseJson["UserData"])
                            self.currentUser.save()
                            //                            Logger.d("load user from server response: \(self.currentUser.description)")
                            
                            //                        self.registerForPushNotifications()
                            
                            //                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            //                        appDelegate.updateRootVC()
                            
                            // start to get notice unread count
                            NoticeManager.shared().startTimerUnreadCount()
                            DispatchQueue.main.async {
                                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "updateProfileAccountId"), object: nil))
                            }
                            listener?.didFinishBindVWId(success: true, isBind: true)
                        } else {
                            Logger.e("no token found")
                            listener?.didFinishBindVWId(success: false, isBind: false)
                        }
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishBindVWId(success: false, isBind: false)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishBindVWId(success: false, isBind: false)
                })
                
            }
    }
    
    func signOutLocal(showAlert: Bool) {
        //        self.unregisterForPushNotification()
        
        // clear user model data
        self.currentUser.clear()
        self.clearUserToken()
        self.clearCruisysUserToken()
        CarManager.shared().cars.removeAll()
        
        self.recallUrl = nil
        self.showNoticeBoard = nil
        
        // stop notice unread count timer
        NoticeManager.shared().stopTimerUnreadCount()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.updateRootVC(showAlert: showAlert)
    }
    
    func bindFb(set: Bool, fbId: String, listener: UserProtocol?) {
        let parameters: Parameters = [
            "FBID": fbId,
            "Type": set ? "B" : "R"
        ]
        
        if let token = getUserToken() {
            listener?.didStartBindFb()
            Alamofire.request(Config.urlMemberServer + "APP_FBID?token=\(token)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"]).responseJSON { response in
                    //            Logger.d("Request: \(String(describing: response.request))")   // original url request
                    //            Logger.d("Response: \(String(describing: response.response))") // http url response
                    //            Logger.d("Result: \(response.result)")                         // response serialization result
                    
                    if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                        let arr: [String] = authorization.components(separatedBy: " ")
                        
                        // "Bearer xxxxx"
                        if arr.count == 2 {
                            UserManager.shared().saveUserToken(with: arr[1])
                        }
                    }
                    
                    response.result.ifSuccess({
                        //                        Logger.d(response.result.value as Any)
                        let responseJson = JSON.init(rawValue: response.result.value as Any)!
                        
                        let result: Result = Result(with: responseJson)
                        //                        Logger.d("result: \(result)")
                        if result.isSuccess() {
                            if set {
                                self.currentUser.fbId = fbId
                            } else {
                                self.currentUser.fbId = nil
                            }
                            self.currentUser.save()
                            
                            listener?.didFinishBindFb(success: true, isBind: set)
                        } else {
                            Logger.d("errorCode: \(String(describing: result.errorCode))")
                            Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                            listener?.didFinishBindFb(success: false, isBind: set)
                            Utility.checkTokenValidation(of: result)
                        }
                    })
                    
                    response.result.ifFailure({
                        Logger.d("error: \(response.error as Any)")
                        listener?.didFinishBindFb(success: false, isBind: set)
                    })
                    
                }
        } else {
            Logger.e("try to bind facebook without user token")
        }
    }
    
    func getProfileToCheckCellphone(param: String?) {
        let profile = self.getProfileToCheckCellphone(withId: self.currentUser.accountId!, listener: nil, param: param)
    }
    
    func checkCellphone(param: String?){
        if profile?.cellphone != nil {
            var getvin = ""
            for cc in CarManager.shared().cars {
                if cc.licensePlateNumber == CarManager.shared().getSelectedCarNumber()! {
                    getvin = cc.vin ?? ""
                }
            }
            if let cvin = CarManager.shared().getSelectedCarNumber() {
                CarManager.shared().getExtUrl(listener: nil, type: "openhub", plateNo: cvin, vin: getvin, param: param)
            } else {
                CarManager.shared().getExtUrl(listener: nil, type: "openhub", plateNo: nil, vin: nil, param: param)
            }
            
        } else {
            //showalert
            let customAlert = VWCustomAlert()
            customAlert.alertTitle = "請填寫手機號碼"
            customAlert.alertMessage = "您需於個人資料填寫手機號碼\n才能使用此功能"
            customAlert.alertTag = 1
            customAlert.isCancelButtonHidden = true
            customAlert.okButtonAction = {
                DeeplinkNavigator.shared.proceedToDeeplink(.MemberProfile, param: nil)
            }
            customAlert.blueOkBtn = true
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            if let mController = appDelegate.window?.rootViewController as? UINavigationController {
                if let mainVC: MainViewControllerV2 = mController.viewControllers.first as? MainViewControllerV2 {
                    mainVC.present(customAlert, animated: true, completion: nil)
                    mainVC.dismissHud()
                }
            }
        }
    }
    
    func bindLine(set: Bool, lineUid: String, listener: UserProtocol?) {
        if set == false {
            if let token = getUserToken() {
                listener?.didStartUnbindLine()
                Alamofire.request(Config.urlMemberServer + "APP_Unbind_LineUID?token=\(token)", method: .post, parameters: nil).validate(statusCode: 200..<300)
                    .validate(contentType: ["application/json"]).responseJSON { response in
                        //            Logger.d("Request: \(String(describing: response.request))")   // original url request
                        //            Logger.d("Response: \(String(describing: response.response))") // http url response
                        //            Logger.d("Result: \(response.result)")                         // response serialization result
                        
                        if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                            let arr: [String] = authorization.components(separatedBy: " ")
                            
                            // "Bearer xxxxx"
                            if arr.count == 2 {
                                UserManager.shared().saveUserToken(with: arr[1])
                            }
                        }
                        
                        response.result.ifSuccess({
                            //                        Logger.d(response.result.value as Any)
                            let responseJson = JSON.init(rawValue: response.result.value as Any)!
                            
                            let result: Result = Result(with: responseJson)
                            //                        Logger.d("result: \(result)")
                            if result.isSuccess() {
                                if set {
                                    self.currentUser.lineUid = lineUid
                                } else {
                                    self.currentUser.lineUid = nil
                                }
                                self.currentUser.save()
                                
                                listener?.didFinishUnbindLine(success: true, isBind: set)
                            } else {
                                Logger.d("errorCode: \(String(describing: result.errorCode))")
                                Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                                listener?.didFinishUnbindLine(success: false, isBind: set)
                                Utility.checkTokenValidation(of: result)
                            }
                        })
                        
                        response.result.ifFailure({
                            Logger.d("error: \(response.error as Any)")
                            listener?.didFinishUnbindLine(success: false, isBind: set)
                        })
                        
                    }
            } else {
                Logger.e("try to bind facebook without user token")
            }
            return
        }
        
        if lineUid != "" {
            let parameters: Parameters = [
                "LineUID": lineUid,
            ]
            
            if let token = getUserToken() {
                listener?.didStartBindLine()
                Alamofire.request(Config.urlMemberServer + "APP_Bind_LineUID?token=\(token)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
                    .validate(contentType: ["application/json"]).responseJSON { response in
                        //            Logger.d("Request: \(String(describing: response.request))")   // original url request
                        //            Logger.d("Response: \(String(describing: response.response))") // http url response
                        //            Logger.d("Result: \(response.result)")                         // response serialization result
                        
                        if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                            let arr: [String] = authorization.components(separatedBy: " ")
                            
                            // "Bearer xxxxx"
                            if arr.count == 2 {
                                UserManager.shared().saveUserToken(with: arr[1])
                            }
                        }
                        
                        response.result.ifSuccess({
                            //                        Logger.d(response.result.value as Any)
                            let responseJson = JSON.init(rawValue: response.result.value as Any)!
                            
                            let result: Result = Result(with: responseJson)
                            //                        Logger.d("result: \(result)")
                            if result.isSuccess() {
                                if set {
                                    self.currentUser.lineUid = lineUid
                                } else {
                                    self.currentUser.lineUid = nil
                                }
                                self.currentUser.save()
                                
                                listener?.didFinishBindLine(success: true, isBind: set)
                            } else {
                                Logger.d("errorCode: \(String(describing: result.errorCode))")
                                Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                                listener?.didFinishBindLine(success: false, isBind: set)
                                Utility.checkTokenValidation(of: result)
                            }
                        })
                        
                        response.result.ifFailure({
                            Logger.d("error: \(response.error as Any)")
                            listener?.didFinishBindLine(success: false, isBind: set)
                        })
                        
                    }
            } else {
                Logger.e("try to bind facebook without user token")
            }
        }
    }
    
    func getQrcode(listener: UserProtocol?) {
        listener?.didStartGetQrcode()
        Alamofire.request(Config.urlMemberServer + "APP_Get_Member_Qrcode?token=\(getUserToken()!)", method: .get, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //            Logger.d("Request: \(String(describing: response.request))")   // original url request
                //            Logger.d("Response: \(String(describing: response.response))") // http url response
                //            Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        let text: String = responseJson["QRcode"].stringValue
                        //                        Logger.d("text: \(text)")
                        listener?.didFinishGetQrcode(success: true, qrcodeImage: self.base64Convert(base64String: text))
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetQrcode(success: false, qrcodeImage: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetQrcode(success: false, qrcodeImage: nil)
                })
                
            }
    }
    
    private func base64Convert(base64String: String?) -> UIImage {
        if (base64String?.isEmpty)! {
            return #imageLiteral(resourceName: "no_image_found")
        }else {
            // !!! Separation part is optional, depends on your Base64String !!!
            let temp = base64String?.components(separatedBy: ",")
            let dataDecoded : Data = Data(base64Encoded: temp![1], options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            return decodedimage!
        }
    }
    
    func getProfile(withId accountId: String, listener: ProfileProtocol?) -> Profile? {
        var isMyId = false
        if let myId: String = self.currentUser.accountId {
            if myId == accountId {
                isMyId = true
            }
        }
        let parameters: Parameters = [
            "AccountID": accountId
        ]
        //        Logger.d("getProfile parameters: \(parameters)")
        listener?.didStartGetProfile()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Get_Member_Profile?token=\(getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //            Logger.d("Request: \(String(describing: response.request))")   // original url request
                //            Logger.d("Response: \(String(describing: response.response))") // http url response
                //            Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        let profile: Profile = Profile.init(with: responseJson)
                        if isMyId {
                            self.profile = profile
                            self.currentUser.level = profile.level ?? "0"
                            self.currentUser.levelName = profile.levelName!
                            self.currentUser.save()
                        }
                        
                        //                        Logger.d("load profile from server: \(String(describing: profile))")
                        listener?.didFinishGetProfile(success: true, profile: profile)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetProfile(success: false, profile: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetProfile(success: false, profile: nil)
                })
                
            }
        
        if isMyId {
            return profile
        } else {
            return nil
        }
    }
    
    func getProfileToCheckCellphone(withId accountId: String, listener: ProfileProtocol?, param: String?) -> Profile? {
        var isMyId = false
        if let myId: String = self.currentUser.accountId {
            if myId == accountId {
                isMyId = true
            }
        }
        let parameters: Parameters = [
            "AccountID": accountId
        ]
        //        Logger.d("getProfile parameters: \(parameters)")
        listener?.didStartGetProfile()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Get_Member_Profile?token=\(getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //            Logger.d("Request: \(String(describing: response.request))")   // original url request
                //            Logger.d("Response: \(String(describing: response.response))") // http url response
                //            Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        let profile: Profile = Profile.init(with: responseJson)
                        if isMyId {
                            self.profile = profile
                            self.currentUser.level = profile.level ?? "0"
                            self.currentUser.levelName = profile.levelName!
                            self.currentUser.save()
                            self.checkCellphone(param: param)
                        }
                        
                        //                        Logger.d("load profile from server: \(String(describing: profile))")
                        listener?.didFinishGetProfile(success: true, profile: profile)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetProfile(success: false, profile: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetProfile(success: false, profile: nil)
                })
                
            }
        
        if isMyId {
            return profile
        } else {
            return nil
        }
    }
    
    func updateProfile(profile: Profile, listener: ProfileProtocol?) {
        var parameters: Parameters = [
            "Nickname": profile.nickname!
        ]
        
        if let brief = profile.brief {
            parameters["Brief"] = brief
        }
        if let sex = profile.sex {
            parameters["Sex"] = sex
        }
        if let email = profile.email {
            parameters["EMail"] = email
        }
        if let cellphone = profile.cellphone {
            parameters["Cellphone"] = cellphone
        }
        if let birthday = profile.birthday {
            parameters["Birthday"] = birthday
        }
        if let stickerStream = profile.stickerStream {
            parameters["StickerStream"] = stickerStream
        }
        if let coverStream = profile.coverStream {
            parameters["CoverStream"] = coverStream
        }
        if let dealerAreaId = profile.dealerAreaId {
            if dealerAreaId != "" {
                parameters["AreaId"] = dealerAreaId
            }
        }
        if let dealerPlantId = profile.dealerPlantId {
            if dealerPlantId != "" {
                parameters["PlantId"] = dealerPlantId
            }
        }
        
        //        Logger.d("updateProfile parameters: \(parameters)")
        listener?.didStartUpdateProfile()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Update_Member_Profile?token=\(getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //            Logger.d("Request: \(String(describing: response.request))")   // original url request
                //            Logger.d("Response: \(String(describing: response.response))") // http url response
                //            Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        self.profile = profile
                        //                        Logger.d("load profile from server: \(String(describing: self.profile))")
                        listener?.didFinishUpdateProfile(success: true, profile: profile, msg: nil)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishUpdateProfile(success: false, profile: nil, msg: result.errorMsg)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishUpdateProfile(success: false, profile: nil, msg: nil)
                })
                
            }
    }
    
    func follow(accountId: String, set: Bool, listener: UserProtocol?) {
        let parameters: Parameters = [
            "FollowAccountID": "\(accountId)",
            "Action": set ? "1" : "0"
        ]
        
        //        Logger.d("follow parameters: \(parameters)")
        
        if set {
            Analytics.logEvent("follow", parameters: nil)
        } else {
            Analytics.logEvent("unfollow", parameters: nil)
        }
        
        listener?.didStartFollow()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Member_Follow?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //                Logger.d("Request: \(String(describing: response.request))")   // original url request
                //                Logger.d("Response: \(String(describing: response.response))") // http url response
                //                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        if set {
                            self.relationFollowing = [] // clear following list to trigger next fetch of following relations
                            
                            for relation in self.relationFollower {
                                if relation.accountId == accountId {
                                    relation.isFollow = 1
                                    break
                                }
                            }
                        } else {
                            var r: [Relation] = []
                            for relation in self.relationFollowing {
                                if relation.accountId != accountId {
                                    r.append(relation)
                                }
                            }
                            self.relationFollowing = r
                            
                            for relation in self.relationFollower {
                                if relation.accountId == accountId {
                                    relation.isFollow = 0
                                    break
                                }
                            }
                        }
                        
                        NewsManager.shared().listIsDirtyRemote = true
                        listener?.didFinishFollow(success: true, accountId: accountId, set: set)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishFollow(success: false, accountId: accountId, set: set)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishFollow(success: false, accountId: accountId, set: set)
                })
                
            }
    }
    
    // who 1 for self, 2 for others
    // kind 1 for following, 2 for follower
    func getRelation(of profile: Profile, isFollower: Bool, fromRemote: Bool, listener: UserProtocol?) -> [Relation] {
        var fromRemoteInternal: Bool = fromRemote
        
        if !fromRemoteInternal {
            if !profile.isMine() {
                fromRemoteInternal = true
            } else if isFollower {
                if relationFollower.count == 0 {
                    fromRemoteInternal = true
                }
            } else {
                if relationFollowing.count == 0 {
                    fromRemoteInternal = true
                }
            }
        }
        
        if fromRemoteInternal {
            let parameters: Parameters = [
                "AccountID": profile.accountId!,
                "Who": profile.isMine() ? "1" : "2",
                "Kind": isFollower ? "2" : "1"
            ]
            
            //            Logger.d("getRelation parameters: \(parameters)")
            
            listener?.didStartGetRelation()
            Alamofire.request(Config.urlCommunityServer + "APP_CU_Get_MemberRelation?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"]).responseJSON { response in
                    //                Logger.d("Request: \(String(describing: response.request))")   // original url request
                    //                Logger.d("Response: \(String(describing: response.response))") // http url response
                    //                Logger.d("Result: \(response.result)")                         // response serialization result
                    
                    if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                        let arr: [String] = authorization.components(separatedBy: " ")
                        
                        // "Bearer xxxxx"
                        if arr.count == 2 {
                            UserManager.shared().saveUserToken(with: arr[1])
                        }
                    }
                    
                    response.result.ifSuccess({
                        //                        Logger.d(response.result.value as Any)
                        let responseJson = JSON.init(rawValue: response.result.value as Any)!
                        
                        let result: Result = Result(with: responseJson)
                        //                    Logger.d("result: \(result)")
                        if result.isSuccess() {
                            var relations: [Relation] = []
                            if let data = responseJson["MemberRelationList"].array {
                                for datum in data {
                                    let relation: Relation = Relation(with: datum)
                                    //                                    Logger.d("load relation from server response: \(relation)")
                                    relations.append(relation)
                                }
                            }
                            
                            //                            Logger.d("now we have \(relations.count) relation")
                            
                            if isFollower {
                                self.relationFollower = relations
                            } else {
                                self.relationFollowing = relations
                            }
                            
                            listener?.didFinishGetRelation(success: true, isFollower: isFollower, relations: relations)
                        } else {
                            Logger.d("errorCode: \(String(describing: result.errorCode))")
                            Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                            listener?.didFinishGetRelation(success: false, isFollower: isFollower, relations: nil)
                            Utility.checkTokenValidation(of: result)
                        }
                    })
                    
                    response.result.ifFailure({
                        Logger.d("error: \(response.error as Any)")
                        listener?.didFinishGetRelation(success: false, isFollower: isFollower, relations: nil)
                    })
                    
                }
        }
        
        if profile.isMine() {
            if isFollower {
                return relationFollower
            } else {
                return relationFollowing
            }
        } else {
            return []
        }
    }
    
    // listener not nil: maybe get from remote
    func getFollowingRelation(to memberProfile: Profile, listener: UserProtocol?) -> Relation? {
        if relationFollowing.count == 0 {
            if listener != nil {
                // get following relation from remote
                //                Logger.d("get following relation from remote")
                getRelation(of: Profile.init(with: currentUser.accountId!), isFollower: false, fromRemote: true, listener: listener)
            }
        } else {
            //            Logger.d("find following relation from local, count: \(relationFollowing.count)")
            for relation in relationFollowing {
                if relation.accountId == memberProfile.accountId {
                    //                    Logger.d("following relation found")
                    return relation
                }
            }
        }
        
        return nil
    }
    
    func updatePushToken(token: String, listener: UserProtocol?) {
        let parameters: Parameters = [
            "Device": "ios",
            "PushToken": token
        ]
        
        //        Logger.d("updatePushToken parameters: \(parameters)")
        
        listener?.didStartUpdatePushToken()
        let userToken = getUserToken()!
        //        Logger.d("userToken: \(userToken)")
        Alamofire.request(Config.urlMemberServer + "APP_Push_Token?token=\(userToken)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //                Logger.d("Request: \(String(describing: response.request))")   // original url request
                //                Logger.d("Response: \(String(describing: response.response))") // http url response
                //                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        //                        Logger.d("update fcm token successfully")
                        listener?.didFinishUpdatePushToken(success: true)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishUpdatePushToken(success: false)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishUpdatePushToken(success: false)
                })
                
            }
    }
    
    func search(keyword: String, listener: UserProtocol?) {
        let parameters: Parameters = [
            "Keyword": keyword
        ]
        
        //        Logger.d("search parameters: \(parameters)")
        
        listener?.didStartSearch()
        Analytics.logEvent("search", parameters: nil)
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Search_MemberRelation?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //                Logger.d("Request: \(String(describing: response.request))")   // original url request
                //                Logger.d("Response: \(String(describing: response.response))") // http url response
                //                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                Analytics.logEvent("view_search_results", parameters: nil)
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var relations: [Relation] = []
                        if let data = responseJson["MemberRelationList"].array {
                            for datum in data {
                                let relation: Relation = Relation(with: datum)
                                //                                    Logger.d("load news from server response: \(news)")
                                relations.append(relation)
                            }
                        }
                        
                        //                        Logger.d("now we have \(relations.count) search results")
                        
                        listener?.didFinishSearch(success: true, relations: relations)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishSearch(success: false, relations: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishSearch(success: false, relations: nil)
                })
                
            }
    }
    
    func getRecentlyBrowsed(memberIds: [String], listener: UserProtocol?) {
        if memberIds.count == 0 {
            Logger.d("no member id")
            return
        }
        
        var parameters: Parameters = [:]
        
        var index: Int = 0
        for mi in memberIds {
            parameters["RecentAccountIDList[\(index)]"] = mi
            index += 1
        }
        // RecentAccountIDList[0]
        
        //        Logger.d("search parameters: \(parameters)")
        
        listener?.didStartGetRecentlyBrosedMember()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_MemberRelation_Recently?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //                Logger.d("Request: \(String(describing: response.request))")   // original url request
                //                Logger.d("Response: \(String(describing: response.response))") // http url response
                //                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var relations: [Relation] = []
                        if let data = responseJson["MemberRelationList"].array {
                            for datum in data {
                                let relation: Relation = Relation(with: datum)
                                //                                    Logger.d("load news from server response: \(news)")
                                relations.append(relation)
                            }
                        }
                        
                        //                        Logger.d("now we have \(relations.count) recent browsed")
                        
                        listener?.didFinishGetRecentlyBrosedMember(success: true, relations: relations)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetRecentlyBrosedMember(success: false, relations: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetRecentlyBrosedMember(success: false, relations: nil)
                })
                
            }
    }
    
    func getMemberLevelInfo(listener: UserProtocol?) {
        listener?.didStartGetMemberLevelInfo()
        Alamofire.request(Config.urlMemberServer + "APP_Get_MemberLevel_Info?token=\(getUserToken()!)", method: .get, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //                Logger.d("Request: \(String(describing: response.request))")   // original url request
                //                Logger.d("Response: \(String(describing: response.response))") // http url response
                //                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        let level: Int? = responseJson["Level"].int
                        let levelName: String? = responseJson["LevelName"].string
                        let levelIntro: String? = responseJson["LevelIntro"].string
                        //                        Logger.d("level: \(String(describing: level))")
                        //                        Logger.d("levelName: \(String(describing: levelName))")
                        //                        Logger.d("levelIntro: \(String(describing: levelIntro))")
                        listener?.didFinishGetMemberLevelInfo(success: true, level: level, levelName: levelName, levelIntro: levelIntro)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetMemberLevelInfo(success: false, level: nil, levelName: nil, levelIntro: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetMemberLevelInfo(success: false, level: nil, levelName: nil, levelIntro: nil)
                })
                
            }
    }
    
    func getMemberLevelInfoWebview(listener: UserProtocol?) {
        listener?.didStartGetMemberLevelInfoWebview()
        Alamofire.request(Config.urlMemberServer + "APP_Get_MemberLevel_Info_Webview?token=\(getUserToken()!)", method: .get, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //                Logger.d("Request: \(String(describing: response.request))")   // original url request
                //                Logger.d("Response: \(String(describing: response.response))") // http url response
                //                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        let level: Int? = responseJson["Data"]["LevelInfo"]["Level"].int
                        let levelName: String? = responseJson["Data"]["LevelInfo"]["LevelName"].string
                        listener?.didFinishGetMemberLevelInfoWebview(success: true, level: level, levelName: levelName)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetMemberLevelInfoWebview(success: false, level: nil, levelName: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetMemberLevelInfoWebview(success: false, level: nil, levelName: nil)
                })
                
            }
    }
    
    func getRedeemState(listener: UserProtocol?) {
        listener?.didStartGetRedeemState()
        Alamofire.request(Config.urlMemberServer + "APP_Redeem_Get_Stat?token=\(getUserToken()!)", method: .get, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //                Logger.d("Request: \(String(describing: response.request))")   // original url request
                //                Logger.d("Response: \(String(describing: response.response))") // http url response
                //                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        if  let taskCount = responseJson["RedeemTaskCount"].int,
                            let totalPoints = responseJson["RedeemTotalPoints"].int {
                            listener?.didFinishGetRedeemState(success: true, taskCount: taskCount, totalPoints: totalPoints)
                        }
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetRedeemState(success: false, taskCount: nil, totalPoints: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetRedeemState(success: false, taskCount: nil, totalPoints: nil)
                })
                
            }
    }
    
    func getRedeemRecord(listener: UserProtocol?) {
        listener?.didStartGetRedeemRecord()
        Alamofire.request(Config.urlMemberServer + "APP_Redeem_Get_Records?token=\(getUserToken()!)", method: .get, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //                Logger.d("Request: \(String(describing: response.request))")   // original url request
                //                Logger.d("Response: \(String(describing: response.response))") // http url response
                //                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var records: [Redeem] = []
                        if let data = responseJson["DataList"].array {
                            for datum in data {
                                let redRecord: Redeem = Redeem(with: datum)
                                records.append(redRecord)
                            }
                        }
                        listener?.didFinishGetRedeemRecord(success: true, record: records)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetRedeemRecord(success: false, record: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetRedeemRecord(success: false, record: nil)
                })
                
            }
    }
    
    func redeem(redeemId: String, listener: UserProtocol?) {
        let parameters: Parameters = [
            "RedeemID": redeemId
        ]
        
        listener?.didStartRedeem()
        Alamofire.request(Config.urlCommunityServer + "APP_Redeem_Redeem?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //                Logger.d("Request: \(String(describing: response.request))")   // original url request
                //                Logger.d("Response: \(String(describing: response.response))") // http url response
                //                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishRedeem(success: true)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishRedeem(success: false)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishRedeem(success: false)
                })
                
            }
    }
    
    func getRedeemList(listener: UserProtocol?) {
        listener?.didStartGetRedeemList()
        Alamofire.request(Config.urlMemberServer + "APP_Redeem_Get_List?token=\(getUserToken()!)", method: .get, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //                Logger.d("Request: \(String(describing: response.request))")   // original url request
                //                Logger.d("Response: \(String(describing: response.response))") // http url response
                //                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var totalP: Int = 0
                        var reList: [Redeem] = []
                        if let data = responseJson["DataList"].array {
                            for datum in data {
                                let redRecord: Redeem = Redeem(with: datum)
                                reList.append(redRecord)
                            }
                        }
                        if let tp = responseJson["TotalPoints"].int {
                            totalP = tp
                        }
                        listener?.didFinishGetRedeemList(success: true, list: reList, totalPoints: totalP)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetRedeemList(success: false, list: nil, totalPoints: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetRedeemList(success: false, list: nil, totalPoints: nil)
                })
                
            }
    }
    
    func redeemReceive(redeemId: String, redeem: Redeem, listener: UserProtocol?) {
        let parameters: Parameters = [
            "RedeemID": redeemId
        ]
        
        listener?.didStartRedeemReceive()
        Alamofire.request(Config.urlCommunityServer + "APP_Redeem_Receive?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //                Logger.d("Request: \(String(describing: response.request))")   // original url request
                //                Logger.d("Response: \(String(describing: response.response))") // http url response
                //                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        let mycode = responseJson["Code"].string
                        listener?.didFinishRedeemReceive(success: true, code: mycode, redeem: redeem)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishRedeemReceive(success: false, code: nil, redeem: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishRedeemReceive(success: false, code: nil, redeem: nil)
                })
                
            }
    }
    
    func redeemCopy(redeemId: String, redeem: Redeem, listener: UserProtocol?) {
        let parameters: Parameters = [
            "RedeemID": redeemId
        ]
        
        listener?.didStartRedeemReceive()
        Alamofire.request(Config.urlCommunityServer + "APP_Redeem_Copied?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    if result.isSuccess() {
                        listener?.didFinishRedeemCopy(success: true, redeem: redeem)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishRedeemCopy(success: false, redeem: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishRedeemCopy(success: false, redeem: nil)
                })
                
            }
    }
    
    func getUserMaintenancePlant(accountId: String, vinCode: String, licenseNum: String, listener: UserProtocol?) {
        let parameters: Parameters = [
            "AccountId": accountId,
            "VIN": vinCode,
            "LicensePlateNumber": licenseNum
        ]
        
        listener?.didStartGetUserMaintenancePlant()
        Alamofire.request(Config.urlCommunityServer + "APP_Get_MemberCar_Maintenance_Plant?token=\(self.getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        self.saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        let data = responseJson["Data"]["MaintenancePlant"]
                        let mpp: MaintainancePlant = MaintainancePlant(with: data)
                        listener?.didFinishGetUserMaintenancePlant(success: true, maintenancePlant: mpp)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetUserMaintenancePlant(success: false, maintenancePlant: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetUserMaintenancePlant(success: false, maintenancePlant: nil)
                })
                
            }
    }
    
    func getRecallCheckCampaign(listener: UserProtocol?) {
        if recallIsLoading == true {
            return
        }
        recallIsLoading = true
        
        print("getRecallCheckCampaign")
        listener?.didStartRecallCheck()
        Alamofire.request(Config.urlMemberServer + "APP_Recall_Check_Campaign?token=\(self.getUserToken()!)", method: .get, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                
                self.recallIsLoading = false
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        self.saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        let showNoticeBoardd: Bool = responseJson["Data"]["ShowNoticeBoard"].stringValue == "Y"
                        let recallUrll = responseJson["Data"]["RecallUrl"].stringValue
                        self.recallUrl = recallUrll
                        self.showNoticeBoard = showNoticeBoardd
                        print("didFinishRecallCheck success show:\(showNoticeBoardd), url:\(recallUrll)")
                        listener?.didFinishRecallCheck(success: true, showNoticeBoard: showNoticeBoardd, recellUrl: recallUrll)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishRecallCheck(success: false, showNoticeBoard: false, recellUrl: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishRecallCheck(success: false, showNoticeBoard: false, recellUrl: nil)
                })
                
            }
    }
    
    func readRecallCampaign() {
        Alamofire.request(Config.urlMemberServer + "APP_Recall_Read_NoticeBoard?token=\(self.getUserToken()!)", method: .post, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        self.saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    if result.isSuccess() {
                        NoticeManager.shared().getNoticeEventCount() // 已讀後，重新獲取未讀數
                    } else {
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                })
                
            }
    }
    
    func getUserToken() -> String? {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: keyToken)
    }
    
    private func clearUserToken() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: keyToken)
    }
    
    func saveUserToken(with token: String) {
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: self.keyToken)
    }
    
    func getPushToken() -> String? {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: keyPushToken)
    }
    
    private func clearPushToken() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: keyPushToken)
    }
    
    func savePushToken(with token: String) {
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: self.keyPushToken)
    }
    
    func getCruisysUserToken() -> String? {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: keyCruisysToken)
    }
    
    private func clearCruisysUserToken() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: keyCruisysToken)
    }
    
    func saveCruisysUserToken(with token: String) {
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: self.keyCruisysToken)
    }
    
    private func clearUserInstruction() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: keyUserInstruction)
    }
    
    func saveUserInstruction() {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: keyUserInstruction)
    }
    
    func getUserInstruction() -> Bool? {
        let defaults = UserDefaults.standard
        return defaults.bool(forKey: keyUserInstruction)
    }
    
    private func clearUserCarAssistantReminder() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: keyUserCarAssistantReminder)
    }
    
    func saveUserCarAssistantReminder() {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: keyUserCarAssistantReminder)
    }
    
    func getUserCarAssistantReminder() -> Bool? {
        let defaults = UserDefaults.standard
        return defaults.bool(forKey: keyUserCarAssistantReminder)
    }
    
    // MARK: APN
    
    //    func registerForPushNotifications() {
    //        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { [weak self] granted, error in
    //            Logger.d("Permission granted: \(granted)")
    //            guard granted else { return }
    //            self?.getNotificationSettings()
    //        }
    //    }
    
    //    private func getNotificationSettings() {
    //        UNUserNotificationCenter.current().getNotificationSettings { settings in
    //            Logger.d("Notification settings: \(settings)")
    //            guard settings.authorizationStatus == .authorized else { return }
    //            DispatchQueue.main.async {
    //                UIApplication.shared.registerForRemoteNotifications()
    //            }
    //        }
    //    }
    //
    //    private func unregisterForPushNotification() {
    //        Logger.d("unregisterForPushNotification")
    //        UIApplication.shared.unregisterForRemoteNotifications()
    //    }
}
