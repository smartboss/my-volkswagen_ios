//
//  ClubManager.swift
//  myvolkswagen
//
//  Created by Apple on 10/3/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol ClubProtocol: class {
    func didStartGetJoinedClubCount()
    func didFinishGetJoinedClubCount(success: Bool, count: Int?)

    func didStartGetGuestData()
    func didFinishGetGuestData(success: Bool, guestClubData: GuestClubData?)
    
    func didStartGetGuestClubs()
    func didFinishGetGuestClubs(success: Bool, guestClubs: [Club]?)

    func didStartGetClubRelation()
    func didFinishGetClubRelation(success: Bool, kind: ClubRelationKind, relations: [ClubRelation])

    func didStartAppendClub()
    func didFinishAppendClub(success: Bool)

    func didStartGetClubData()
    func didFinishGetClubData(success: Bool, clubData: ClubData?)
    
    func didStartSearchClub()
    func didFinishSearchClub(success: Bool, result: [Club])
    
    func didStartUpdateClub()
    func didFinishUpdateClub(success: Bool)
    
    func didStartGetClubMembers()
    func didFinishGetClubMembers(success: Bool, relations: [ClubRelation])
    
    func didStartDiscardMember()
    func didFinishDiscardMember(success: Bool)
    
    func didStartLeaveClub()
    func didFinishLeaveClub(success: Bool)
    
    func didStartMemberAdd()
    func didFinishMemberAdd(success: Bool)
    
    func didStartGetMyClubList()
    func didFinishGetMyClubList(success: Bool, result: [Club])
    
    func didStartGetSinglePage()
    func didFinishGetSinglePage(success: Bool, posts: [ClubPost])
    
    func didStartAppendNews()
    func didFinishAppendNews(success: Bool)
    
    func didStartLikeClubPost()
    func didFinishLikeClubPost(success: Bool, post: ClubPost, set: Bool)
    
    func didStartTrackClubPost()
    func didFinishTrackClubPost(success: Bool, post: ClubPost, set: Bool)
    
    func didStartGetClubMembersOfBehavior()
    func didFinishGetClubMembersOfBehavior(success: Bool, members: [Relation]?)
    
    func didStartSetTop()
    func didFinishSetTop(success: Bool, post: ClubPost, set: Bool)
    
    func didStartDeleteClubPost()
    func didFinishDeleteClubPost(success: Bool, post: ClubPost?)
    
    func didStartEditClubPost()
    func didFinishEditClubPost(success: Bool, newsId: Int?)
    
    func didStartGetClubPost()
    func didFinishGetClubPost(success: Bool, post: ClubPost?)
    
    func didStartCommentClubPost()
    func didFinishCommentClubPost(success: Bool, post: ClubPost)
    
    func didStartDeleteClubPostComment()
    func didFinishDeleteClubPostComment(success: Bool, comment: Comment?)
}

enum ClubSort: Int {
    case latestClub = 1, latestPost, member
}

enum ClubJoinedSort: Int {
    case mostBrowsed = 1, latestPost, latestJoined, member
}

enum ClubRelationKind: Int {
    case follow = 1, followers
}



class ClubManager: NSObject {
    
    // MARK: - Properties

    private var joinedClubCount: Int?
    private var queryintJoinedClubCount = false
    
    private var guestClubData: GuestClubData?
    private var queryintGuestClubData = false
    
    private var guestClubs: [Club]?
    private var queryintGuestClubs = false
    
    private var clubData: ClubData?
    private var queryintClubData = false
    
    
    
    
    private static var sharedManager: ClubManager = {
        let manager = ClubManager()
        
        // Configuration
        // ...
        
        return manager
    }()
    
    
    // Initialization
    override private init() {
//        cars = []
        
        super.init()
    }
    
    // MARK: - Accessors
    
    class func shared() -> ClubManager {
        return sharedManager
    }
    
    
    
    // MARK: public methods
    
    func getCachedGuestClubs() -> [Club]? {
        return guestClubs
    }
    
    func cleanCache() {
        joinedClubCount = nil
        guestClubData = nil
        guestClubs = nil
        clubData = nil
    }
    
    
    
    // MARK: API methods
    
    // cu_api 33
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_My_JoinCount
    func getJoinedClubCount(remotely: Bool, listener: ClubProtocol?) -> Int? {
        if !queryintJoinedClubCount && (remotely || (joinedClubCount == nil)) {
            queryintJoinedClubCount = true
            listener?.didStartGetJoinedClubCount()
            Alamofire.request(Config.urlMemberServer + "APP_CU_Club_My_JoinCount?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: nil).validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                    
                    if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                        let arr: [String] = authorization.components(separatedBy: " ")
                        
                        // "Bearer xxxxx"
                        if arr.count == 2 {
                            UserManager.shared().saveUserToken(with: arr[1])
                        }
                    }
                    
                    response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                        let responseJson = JSON.init(rawValue: response.result.value as Any)!
                        
                        let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                        if result.isSuccess() {
                            self.joinedClubCount = responseJson["Count"].int
                            
                            listener?.didFinishGetJoinedClubCount(success: true, count: self.joinedClubCount)
                        } else {
                            Logger.d("errorCode: \(String(describing: result.errorCode))")
                            Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                            listener?.didFinishGetJoinedClubCount(success: false, count: self.joinedClubCount)
                            Utility.checkTokenValidation(of: result)
                        }
                        
                        self.queryintJoinedClubCount = false
                    })
                    
                    response.result.ifFailure({
                        Logger.d("error: \(response.error as Any)")
                        listener?.didFinishGetJoinedClubCount(success: false, count: self.joinedClubCount)
                        self.queryintJoinedClubCount = false
                    })
                    
            }
        }
        
        return joinedClubCount
    }
    
    // cu_api 34
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Get_Guest_Page
    func getGuestData(remotely: Bool, listener: ClubProtocol?) -> GuestClubData? {
        if !queryintGuestClubData && (remotely || guestClubData == nil || guestClubData!.hasEmptyClub()) {
            queryintGuestClubData = true
            listener?.didStartGetGuestData()
            Alamofire.request(Config.urlMemberServer + "APP_CU_Club_Get_Guest_Page?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: nil).validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                    
                    if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                        let arr: [String] = authorization.components(separatedBy: " ")
                        
                        // "Bearer xxxxx"
                        if arr.count == 2 {
                            UserManager.shared().saveUserToken(with: arr[1])
                        }
                    }
                    
                    response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                        let responseJson = JSON.init(rawValue: response.result.value as Any)!
                        
                        let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                        if result.isSuccess() {
                            self.guestClubData = GuestClubData(with: responseJson)
//                            Logger.d("load self.guestClubData: \(String(describing: self.guestClubData))")
                            
                            listener?.didFinishGetGuestData(success: true, guestClubData: self.guestClubData)
                        } else {
                            Logger.d("errorCode: \(String(describing: result.errorCode))")
                            Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                            listener?.didFinishGetGuestData(success: false, guestClubData: self.guestClubData)
                            Utility.checkTokenValidation(of: result)
                        }
                        
                        self.queryintGuestClubData = false
                    })
                    
                    response.result.ifFailure({
                        Logger.d("error: \(response.error as Any)")
                        listener?.didFinishGetGuestData(success: false, guestClubData: self.guestClubData)
                        
                        self.queryintGuestClubData = false
                    })
                    
            }
        }
        
        return guestClubData
    }
    
    // cu_api 35
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Get_Guest_All_List
    func getGuestClubs(sort: ClubSort, models: String?, listener: ClubProtocol?) {
        if !queryintGuestClubs/* && (guestClubs == nil || guestClubs!.isEmpty)*/ {
            queryintGuestClubs = true
            listener?.didStartGetGuestClubs()
            
            var parameters: Parameters = [
                "Sort": "\(sort.rawValue)"
            ]
            
            if let models = models {
                parameters["ModelList"] = models
            }
            
//            Logger.d("getGuestClubs parameters: \(parameters)")
            
            Alamofire.request(Config.urlMemberServer + "APP_CU_Club_Get_Guest_All_List?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                    
                    if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                        let arr: [String] = authorization.components(separatedBy: " ")
                        
                        // "Bearer xxxxx"
                        if arr.count == 2 {
                            UserManager.shared().saveUserToken(with: arr[1])
                        }
                    }
                    
                    response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                        let responseJson = JSON.init(rawValue: response.result.value as Any)!
                        
                        let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                        if result.isSuccess() {
                            self.guestClubs = []
                            if let data = responseJson["DataList"].array {
                                for datum in data {
                                    let club: Club = Club(with: datum)
//                                    Logger.d("load club from server response: \(club)")
                                    self.guestClubs!.append(club)
                                }
                            }
//                            Logger.d("load guest clubs from server response: \(String(describing: self.guestClubs))")
                            
                            listener?.didFinishGetGuestClubs(success: true, guestClubs: self.guestClubs)
                        } else {
                            Logger.d("errorCode: \(String(describing: result.errorCode))")
                            Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                            listener?.didFinishGetGuestClubs(success: false, guestClubs: self.guestClubs)
                            Utility.checkTokenValidation(of: result)
                        }
                        
                        self.queryintGuestClubs = false
                    })
                    
                    response.result.ifFailure({
                        Logger.d("error: \(response.error as Any)")
                        listener?.didFinishGetGuestClubs(success: false, guestClubs: self.guestClubs)
                        
                        self.queryintGuestClubs = false
                    })
                    
            }
        }
    }
    
    // cu_api 38
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Get_Relation
    func getRelation(kind: ClubRelationKind, clubId: Int?, listener: ClubProtocol?) {
        listener?.didStartGetClubRelation()
                    
        var parameters: Parameters = [
            "AccountID": UserManager.shared().currentUser.accountId!,
            "Who": "1",
            "Kind": "\(kind.rawValue)"
        ]
        
        if let clubId = clubId {
            parameters["ClubID"] = "\(clubId)"
        }
        
//        Logger.d("getRelation parameters: \(parameters)")
        
        Alamofire.request(Config.urlMemberServer + "APP_CU_Club_Get_Relation?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var relations: [ClubRelation] = []
                        if let data = responseJson["MemberRelationList"].array {
                            for datum in data {
                                let relation: ClubRelation = ClubRelation(with: datum)
//                                    Logger.d("load club from server response: \(club)")
                                relations.append(relation)
                            }
                        }
//                            Logger.d("load guest clubs from server response: \(String(describing: self.guestClubs))")
                        
                        listener?.didFinishGetClubRelation(success: true, kind: kind, relations: relations)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetClubRelation(success: false, kind: kind, relations: [])
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetClubRelation(success: false, kind: kind, relations: [])
                })
                
        }
    }
    
    // cu_api 27
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Append
    func appendClub(name: String, intro: String, photo: UIImage, models: String, members: [String], listener: ClubProtocol?) {
        listener?.didStartAppendClub()
                    
        var parameters: Parameters = [
            "ClubName": name,
            "ClubIntro": intro,
            "ModelList": models,
            "AccountList": members
        ]
        
//        Logger.d("appendClub parameters: \(parameters)")
        parameters["ClubPhotoStream"] = Utility.getBase64Image(image: photo)
        
        Alamofire.request(Config.urlMemberServer + "APP_CU_Club_Append?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishAppendClub(success: true)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishAppendClub(success: false)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishAppendClub(success: false)
                })
                
        }
    }
    
    // cu_api 36
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Get_My_Page
    func getClubData(remotely: Bool, listener: ClubProtocol?) -> ClubData? {
        if !queryintClubData && (remotely || clubData == nil) {
            queryintClubData = true
            listener?.didStartGetClubData()
            Alamofire.request(Config.urlMemberServer + "APP_CU_Club_Get_My_Page?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: nil).validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                    
                    if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                        let arr: [String] = authorization.components(separatedBy: " ")
                        
                        // "Bearer xxxxx"
                        if arr.count == 2 {
                            UserManager.shared().saveUserToken(with: arr[1])
                        }
                    }
                    
                    response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                        let responseJson = JSON.init(rawValue: response.result.value as Any)!
                        
                        let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                        if result.isSuccess() {
                            self.clubData = ClubData(with: responseJson)
//                            Logger.d("load self.guestClubData: \(String(describing: self.guestClubData))")
                            
                            listener?.didFinishGetClubData(success: true, clubData: self.clubData)
                        } else {
                            Logger.d("errorCode: \(String(describing: result.errorCode))")
                            Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                            listener?.didFinishGetClubData(success: false, clubData: self.clubData)
                            Utility.checkTokenValidation(of: result)
                        }
                        
                        self.queryintClubData = false
                    })
                    
                    response.result.ifFailure({
                        Logger.d("error: \(response.error as Any)")
                        listener?.didFinishGetClubData(success: false, clubData: self.clubData)
                        
                        self.queryintClubData = false
                    })
                    
            }
        }
        
        return clubData
    }
    
    // cu_api 30
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Search
    func search(sort: ClubSort, keyword: String, listener: ClubProtocol?) {
        listener?.didStartSearchClub()
                    
        let parameters: Parameters = [
            "Sort": "\(sort.rawValue)",
            "Keyword": keyword
        ]
        
        Logger.d("search parameters: \(parameters)")
        
        Alamofire.request(Config.urlMemberServer + "APP_CU_Club_Search?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var searchResult: [Club] = []
                        if let data = responseJson["DataList"].array {
                            for datum in data {
                                let club: Club = Club(with: datum)
//                                    Logger.d("load club from server response: \(club)")
                                searchResult.append(club)
                            }
                        }
//                            Logger.d("load guest clubs from server response: \(String(describing: self.guestClubs))")
                        
                        listener?.didFinishSearchClub(success: true, result: searchResult)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishSearchClub(success: false, result: [])
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishSearchClub(success: false, result: [])
                })
                
        }
    }
    func search(sort: ClubJoinedSort, keyword: String, listener: ClubProtocol?) {
        listener?.didStartSearchClub()
                    
        let parameters: Parameters = [
            "Sort": "\(sort.rawValue)",
            "Keyword": keyword
        ]
        
        Logger.d("search parameters: \(parameters)")
        
        Alamofire.request(Config.urlMemberServer + "APP_CU_Club_Search?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var searchResult: [Club] = []
                        if let data = responseJson["DataList"].array {
                            for datum in data {
                                let club: Club = Club(with: datum)
//                                    Logger.d("load club from server response: \(club)")
                                searchResult.append(club)
                            }
                        }
//                            Logger.d("load guest clubs from server response: \(String(describing: self.guestClubs))")
                        
                        listener?.didFinishSearchClub(success: true, result: searchResult)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishSearchClub(success: false, result: [])
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishSearchClub(success: false, result: [])
                })
                
        }
    }
    
    // cu_api 28
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Update
    func updateClub(clubId: Int, name: String, intro: String, photo: UIImage, models: String, listener: ClubProtocol?) {
        listener?.didStartUpdateClub()
                    
        var parameters: Parameters = [
            "ClubID": "\(clubId)",
            "ClubName": name,
            "ClubIntro": intro,
            "ModelList": models
        ]
        
        Logger.d("updateClub parameters: \(parameters)")
        parameters["ClubPhotoStream"] = Utility.getBase64Image(image: photo)
        
        Alamofire.request(Config.urlMemberServer + "APP_CU_Club_Update?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishUpdateClub(success: true)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishUpdateClub(success: false)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishUpdateClub(success: false)
                })
                
        }
    }
    
    // cu_api 42
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Get_Member_List
    func getMembers(clubId: Int, listener: ClubProtocol?) {
        listener?.didStartGetClubMembers()
                    
        let parameters: Parameters = [
            "ClubID": "\(clubId)"
        ]
        
//        Logger.d("getMembers parameters: \(parameters)")
        
        Alamofire.request(Config.urlMemberServer + "APP_CU_Club_Get_Member_List?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var relations: [ClubRelation] = []
                        if let data = responseJson["DataList"].array {
                            for datum in data {
                                let relation: ClubRelation = ClubRelation(with: datum)
//                                    Logger.d("load club from server response: \(club)")
                                relations.append(relation)
                            }
                        }
//                        Logger.d("load members from server response: \(String(describing: relations))")
                        
                        listener?.didFinishGetClubMembers(success: true, relations: relations)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetClubMembers(success: false, relations: [])
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetClubMembers(success: false, relations: [])
                })
                
        }
    }
    
    // cu_api 32
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Member_Discard
    func discardMember(clubId: Int, members: [String], listener: ClubProtocol?) {
        listener?.didStartDiscardMember()
                    
        let parameters: Parameters = [
            "ClubID": "\(clubId)",
            "AccountIDList": members
        ]
        
//        Logger.d("discardMember parameters: \(parameters)")
        
        Alamofire.request(Config.urlMemberServer + "APP_CU_Club_Member_Discard?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishDiscardMember(success: true)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishDiscardMember(success: false)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishDiscardMember(success: false)
                })
                
        }
    }
    
    // cu_api 29
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Leave
    func leave(clubId: Int, newAdminId: String?, listener: ClubProtocol?) {
        listener?.didStartLeaveClub()
                    
        var parameters: Parameters = [
            "ClubID": "\(clubId)"
        ]
        
        if let newAdminId = newAdminId {
            parameters["AdminAccountID"] = newAdminId
        }
        
//        Logger.d("leave parameters: \(parameters)")
        
        Alamofire.request(Config.urlMemberServer + "APP_CU_Club_Leave?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishLeaveClub(success: true)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishLeaveClub(success: false)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishLeaveClub(success: false)
                })
                
        }
    }
    
    // cu_api 31
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Member_Add
    func memberAdd(clubId: Int, listener: ClubProtocol?) {
        listener?.didStartMemberAdd()
                    
        let parameters: Parameters = [
            "ClubID": "\(clubId)"
        ]
        
//        Logger.d("memberAdd parameters: \(parameters)")
        
        Alamofire.request(Config.urlMemberServer + "APP_CU_Club_Member_Add?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishMemberAdd(success: true)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishMemberAdd(success: false)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishMemberAdd(success: false)
                })
                
        }
    }
    
    // cu_api 40
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Get_My_ClubList
    func getMyClubList(kind: Int, sort: Int, listener: ClubProtocol?) {
        listener?.didStartGetMyClubList()
                    
        let parameters: Parameters = [
            "Sort": "\(sort)",
            "Kind": "\(kind)"
        ]
        
//        Logger.d("getMyClubList parameters: \(parameters)")
        
        Alamofire.request(Config.urlMemberServer + "APP_CU_Club_Get_My_ClubList?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var clubs: [Club] = []
                        if let data = responseJson["DataList"].array {
                            for datum in data {
                                let club: Club = Club(with: datum)
//                                    Logger.d("load club from server response: \(club)")
                                clubs.append(club)
                            }
                        }
//                            Logger.d("load guest clubs from server response: \(String(describing: self.guestClubs))")
                        
                        listener?.didFinishGetMyClubList(success: true, result: clubs)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetMyClubList(success: false, result: [])
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetMyClubList(success: false, result: [])
                })
                
        }
    }
    
    // cu_api 41
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Get_Single_Page
    private var singlePagePageIndex: Int = 1 // starts from 1
    var singlePageListEndReached: Bool = false
//    private var clubPosts: [News] = []
    func getSinglePage(clubId: Int, clubPosts: [ClubPost], listener: ClubProtocol?) {
        listener?.didStartGetSinglePage()
        
        let loadMore: Bool = (clubPosts.count != 0)
        
        if !loadMore {
            singlePagePageIndex = 1
            singlePageListEndReached = false
        }
        
        let parameters: Parameters = [
            "ClubID": "\(clubId)",
            "Page": singlePagePageIndex
        ]
        
//        Logger.d("parameters: \(parameters)")
//        Logger.d("UserManager.shared().getUserToken()!: \(UserManager.shared().getUserToken()!)")
        
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Club_Get_Single_Page?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        self.singlePageListEndReached = true
                        
                        var posts: [ClubPost] = []
                        if let data = responseJson["NewsList"].array {
                            if loadMore {
                                posts.append(contentsOf: clubPosts)
                            }
                            
                            for datum in data {
                                let post: ClubPost = ClubPost(with: datum)
//                                    Logger.d("load club from server response: \(club)")
                                posts.append(post)
                                self.singlePageListEndReached = false
                            }
                        }
//                        Logger.d("load posts from server response: \(String(describing: posts))")
                        
                        if !self.singlePageListEndReached {
                            self.singlePagePageIndex += 1
                        }
                        
                        listener?.didFinishGetSinglePage(success: true, posts: posts)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetSinglePage(success: false, posts: [])
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetSinglePage(success: false, posts: [])
                })
        }
    }
    
    // cu_api 45
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Append_News
    func appendNews(clubId: Int, content: String?, photos: [UIImage], listener: ClubProtocol?) {
        listener?.didStartAppendNews()
                    
        var parameters: Parameters = [
            "ClubID": "\(clubId)"
        ]
        
        if let content = content {
            parameters["Content"] = content
        }
        
        Logger.d("appendNews parameters: \(parameters)")
        
        var base64Imgs: [String] = []
        for img in photos {
            base64Imgs.append(Utility.getBase64Image(image: img))
        }
        parameters["PhotoStreamArray"] = base64Imgs
        
        Alamofire.request(Config.urlMemberServer + "APP_CU_Club_Append_News?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result

                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")

                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }

                response.result.ifSuccess({
                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!

                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishAppendNews(success: true)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishAppendNews(success: false)
                        Utility.checkTokenValidation(of: result)
                    }
                })

                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishAppendNews(success: false)
                })

        }
    }
    
    // cu_api 48
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_News_Like
    func like(news: ClubPost, set: Bool, listener: ClubProtocol?) {
        let parameters: Parameters = [
            "NewsID": "\(news.newsId!)",
            "Action": set ? "1" : "0"
        ]
        
//        Logger.d("like parameters: \(parameters)")
        
        listener?.didStartLikeClubPost()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Club_News_Like?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishLikeClubPost(success: true, post: news, set: set)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishLikeClubPost(success: false, post: news, set: set)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishLikeClubPost(success: false, post: news, set: set)
                })
                
        }
    }
    
    // cu_api 51
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_News_Bookmark
    func track(news: ClubPost, set: Bool, listener: ClubProtocol?) {
        let parameters: Parameters = [
            "NewsID": "\(news.newsId!)",
            "Action": set ? "1" : "0"
        ]
        
//        Logger.d("track parameters: \(parameters)")
        
        listener?.didStartTrackClubPost()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Club_News_Bookmark?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishTrackClubPost(success: true, post: news, set: set)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishTrackClubPost(success: false, post: news, set: set)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishTrackClubPost(success: false, post: news, set: set)
                })
                
        }
    }
    
    
    // cu_api 52
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_MemberList_In_Behavior
    func getMembersOfBehavior(news: ClubPost, behavior: Behavior, listener: ClubProtocol?) {
        let parameters: Parameters = [
            "NewsID": "\(news.newsId!)",
            "Behavior": "\(behavior.rawValue)"
        ]
        
//        Logger.d("getMembersOfBehavior parameters: \(parameters)")
        
        listener?.didStartGetClubMembersOfBehavior()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Club_MemberList_In_Behavior?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var relations: [Relation] = []
                        if let data = responseJson["MemberRelationList"].array {
                            for datum in data {
                                let relation: Relation = Relation(with: datum)
//                                    Logger.d("load news from server response: \(news)")
                                relations.append(relation)
                            }
                        }
                        
//                        Logger.d("now we have \(relations.count) members")
                        
                        listener?.didFinishGetClubMembersOfBehavior(success: true, members: relations)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetClubMembersOfBehavior(success: false, members: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetClubMembersOfBehavior(success: false, members: nil)
                })
                
        }
    }
    
    // cu_api 44
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Set_TopNews
    func setTop(post: ClubPost, set: Bool, listener: ClubProtocol?) {
        let parameters: Parameters = [
            "ClubID": "\(post.clubId)",
            "NewsID": "\(post.newsId!)",
            "Action": set ? "1" : "0"
        ]
        
//        Logger.d("getMembersOfBehavior parameters: \(parameters)")
        
        listener?.didStartSetTop()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Club_Set_TopNews?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishSetTop(success: true, post: post, set: set)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishSetTop(success: false, post: post, set: set)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishSetTop(success: false, post: post, set: set)
                })
                
        }
    }
    
    
    // 47
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Delete_News
    func delete(post: ClubPost, listener: ClubProtocol?) {
            
        let parameters: Parameters = [
            "NewsID": "\(post.newsId!)"
        ]
        
//        Logger.d("delete parameters: \(parameters)")
        
        listener?.didStartDeleteClubPost()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Club_Delete_News?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        self.cleanCache()
                        listener?.didFinishDeleteClubPost(success: true, post: post)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishDeleteClubPost(success: false, post: post)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishDeleteClubPost(success: false, post: post)
                })
        }
    }
    
    // 46
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Update_News
    func edit(newsId: Int, content: String?, listener: ClubProtocol?) {
        var parameters: Parameters = [
            "NewsID": "\(newsId)"
        ]
        
        if let content = content {
            parameters["Content"] = content
        }
        
        listener?.didStartEditClubPost()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Club_Update_News?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        // set remote dirty directly
//                        self.listIsDirtyRemote = true
                        
                        // set local dirty
//                        for news in self.newsListAll {
//                            if news.newsId == newsId {
//                                news.content = content
//                                news.modelList = modelList
//                                break
//                            }
//                        }
//                        for news in self.newsListHighlighted {
//                            if news.newsId == newsId {
//                                news.content = content
//                                news.modelList = modelList
//                                break
//                            }
//                        }
//                        self.listIsDirty = true
                        
                        listener?.didFinishEditClubPost(success: true, newsId: newsId)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishEditClubPost(success: false, newsId: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishEditClubPost(success: false, newsId: nil)
                })
        }
    }
    
    // 43
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Get_News
    func getPost(post: ClubPost, listener: ClubProtocol?) {
            
        let parameters: Parameters = [
            "NewsID": "\(post.newsId!)"
        ]
        
//        Logger.d("getNews parameters: \(parameters)")
        
        listener?.didStartGetClubPost()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Club_Get_News?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        post.update(with: responseJson)
//                        Logger.d("updated news: \(news)")
                        listener?.didFinishGetClubPost(success: true, post: post)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetClubPost(success: false, post: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetClubPost(success: false, post: nil)
                })
        }
    }
    
    // 49
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Append_News_Comment
    func comment(post: ClubPost, content: String, listener: ClubProtocol?) {
        if content.count == 0 {
            Logger.d("empty coment")
            return
        }
        
        let parameters: Parameters = [
            "NewsID": "\(post.newsId!)",
            "Content": "\(content)"
        ]
        
//        Logger.d("comment parameters: \(parameters)")
        
        listener?.didStartCommentClubPost()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Club_Append_News_Comment?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishCommentClubPost(success: true, post: post)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishCommentClubPost(success: false, post: post)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishCommentClubPost(success: false, post: post)
                })
        }
    }
    
    // 50
    // https://vwapp-demo.volkswagentaiwan.com.tw/api/APP_CU_Club_Comment_Delete
    func delete(comment: Comment, listener: ClubProtocol?) {

        let parameters: Parameters = [
            "CommentID": "\(comment.commentId!)"
        ]

//        Logger.d("delete parameters: \(parameters)")

        listener?.didStartDeleteClubPostComment()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Club_Comment_Delete?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result

                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")

                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }

                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!

                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishDeleteClubPostComment(success: true, comment: comment)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishDeleteClubPostComment(success: false, comment: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })

                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishDeleteClubPostComment(success: false, comment: nil)
                })
        }
    }
    
    
    
    // MARK: private methods

}
