//
//  RoadManager.swift
//  myvolkswagen
//
//  Created by Apple on 7/4/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol RoadProtocol: class {
    func didStartSyncRoadInfo()
    func didFinishSyncRoadInfo(success: Bool)
}

class RoadManager: NSObject {
    
    // MARK: - Properties
    private var listener: RoadProtocol?
    
    private var roadStaticInfoDownloaded: Bool = false
    private var timestampRoadStaticInfo: Date?
    private var intervalUpdateRoadStaticInfo: Int?
    private var roadStaticInfo: [RoadStaticInfo]?
    
    private var roadDynamicInfoDownloaded: Bool = false
    private var timestampRoadDynamicInfo: Date?
    private var intervalUpdateRoadDynamicInfo: Int?
    private var roadDynamicInfo: [RoadDynamicInfo]?
    
    private var cCTVInfoDownloaded: Bool = false
    private var timestampCCTVInfo: Date?
    private var intervalUpdateCCTVInfo: Int?
    private var cCTVInfo: [CCTVInfo]?
    
    private var timestampLastUpdated: Date!
    
    private var pathsDownloaded: Bool = false
    private var paths: [Path]?
    
    
    private var pointToKM: [String: String]!
    private var pointToPath: [String: Path]!
    private var startEndToRouteId: [String: String]!
    private var routeIdToRoute: [String: RoadDynamicInfo]!
    private var startEndToCCTVIds: [String: [String]]!
    private var cCTVIdToCCTV: [String: CCTVDynamicInfo]!
    
    
    
    
    private static var sharedRoadManager: RoadManager = {
        let roadManager = RoadManager()
        
        // Configuration
        // ...
        
        return roadManager
    }()
    
    
    // Initialization
    override private init() {
        super.init()
    }
    
    // MARK: - Accessors
    
    class func shared() -> RoadManager {
        return sharedRoadManager
    }
    
    
    
    // MARK: listener
    func registerListener(listener: RoadProtocol?) {
        self.listener = listener
    }
    
    func unregisterListener(listener: RoadProtocol?) {
        if (self.listener === listener) {
            self.listener = nil
        }
    }
    
    
    
    func collectDataIfNeededV2(init set: Bool) {
        Logger.d("collectDataIfNeededV2")
        
        if set {
            roadStaticInfoDownloaded = false
            roadDynamicInfoDownloaded = false
            cCTVInfoDownloaded = false
            pathsDownloaded = false
        }
        
        timestampLastUpdated = Date()
        
        if (timestampRoadStaticInfo == nil) {
            getRoadLevelStaticInfoV2()
            return
        } else if (intervalUpdateRoadStaticInfo != nil && timestampRoadStaticInfo!.timeIntervalSinceNow < -Double(intervalUpdateRoadStaticInfo!)) {
            Logger.d("timestampRoadStaticInfo since now: \(String(describing: timestampRoadStaticInfo?.timeIntervalSinceNow))")
            if !roadStaticInfoDownloaded {
                getRoadLevelStaticInfoV2()
                return
            }
        } else {
            roadStaticInfoDownloaded = true
        }

        if (timestampRoadDynamicInfo == nil) {
            getRoadLevelDynamicInfoV2()
            return
        } else if (intervalUpdateRoadDynamicInfo != nil && timestampRoadDynamicInfo!.timeIntervalSinceNow < -Double(intervalUpdateRoadDynamicInfo!)) {
            Logger.d("timestampRoadDynamicInfo since now: \(String(describing: timestampRoadDynamicInfo?.timeIntervalSinceNow))")
            if !roadDynamicInfoDownloaded {
                getRoadLevelDynamicInfoV2()
                return
            }
        } else {
            roadDynamicInfoDownloaded = true
        }
        
        if (timestampCCTVInfo == nil) {
            getCCTVInfo()
            return
        } else if (intervalUpdateCCTVInfo != nil && timestampCCTVInfo!.timeIntervalSinceNow < -Double(intervalUpdateCCTVInfo!)) {
            Logger.d("timestampCCTVInfo since now: \(String(describing: timestampCCTVInfo?.timeIntervalSinceNow))")
            if !cCTVInfoDownloaded {
                getCCTVInfo()
                return
            }
        } else {
            cCTVInfoDownloaded = true
        }

        if (paths == nil || !pathsDownloaded) {
            getRoadLocationV2()
            return
        }

        // build data structures

        listener?.didStartSyncRoadInfo()

        pointToPath = [:]
        for path in paths! {
            for point in path.points {
                pointToPath[point.id] = path
            }
        }

        if let roadStaticInfo = roadStaticInfo {
            pointToKM = [:]
            startEndToRouteId = [:]
            for info in roadStaticInfo {
                pointToKM[info.startlocationpoint] = info.fromkm
                pointToKM[info.endlocationpoint] = info.tokm

                startEndToRouteId[getKeyOf(startPointId: info.startlocationpoint, endPointId: info.endlocationpoint)] = info.routeid
            }
//            Logger.d("startEndToRouteId: \(String(describing: startEndToRouteId))")
        }

        if let roadDynamicInfo = roadDynamicInfo {
            routeIdToRoute = [:]
            for info in roadDynamicInfo {
                routeIdToRoute[info.routeid] = info
            }
        }
        
        if let cCTVInfo = cCTVInfo {
            startEndToCCTVIds = [:]
            cCTVIdToCCTV = [:]
            
            for info in cCTVInfo {
                let key: String = getKeyOf(startPointId: info.startlocationpoint, endPointId: info.endlocationpoint)
                var ids = startEndToCCTVIds[key]
                if ids == nil {
                    startEndToCCTVIds[key] = [info.cctvid]
                } else {
                    ids!.append(info.cctvid)
                }
                
                cCTVIdToCCTV[info.cctvid] = CCTVDynamicInfo(with: info)
            }
            
//            Logger.d("startEndToCCTVIds: \(String(describing: startEndToCCTVIds))")
        }

        listener?.didFinishSyncRoadInfo(success: true)
    }
    
    private func getRoadLevelStaticInfoV2() {
        Logger.d("getRoadLevelStaticInfoV2")
        listener?.didStartSyncRoadInfo()
        Alamofire.request(Config.urlRoadLevelInfoV2, method: .get, parameters: nil, encoding: URLEncoding.default).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseString { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    guard let data = response.data else {
                        Logger.d("no road level data")
                        self.listener?.didFinishSyncRoadInfo(success: false)
                        return
                    }
                    let utf8Text = String(data: data, encoding: .utf8) ?? String(decoding: data, as: UTF8.self)
//                    Logger.d("utf8Text: \(utf8Text)")
                    
                    if let json = self.stringToJsonV2(utf8Text) {
                        let responseJson = JSON.init(rawValue: json as Any)!
                        
                        let updatetime = responseJson["UpdateTime"].stringValue
                        Logger.d("updatetime: \(updatetime)")
                        if let date: Date = self.stringToDate(updatetime) {
                            self.timestampRoadStaticInfo = date
                            Logger.d("self.timestampRoadStaticInfo: \(self.timestampRoadStaticInfo!)")
                        }

                        let interval = responseJson["UpdateInterval"].stringValue
                        self.intervalUpdateRoadStaticInfo = Int(interval)
                        Logger.d("self.intervalUpdateRoadStaticInfo: \(String(describing: self.intervalUpdateRoadStaticInfo))")

                        if let jsonArray = responseJson["Sections"].array {
                            self.roadStaticInfo = []
                            for datum in jsonArray {
//                                Logger.d("load datum from server response: \(datum)")
                                let info: RoadStaticInfo = RoadStaticInfo(with: datum)
//                                Logger.d("road static info: \(info)")
                                self.roadStaticInfo!.append(info)
                            }
                            Logger.d("get \(self.roadStaticInfo!.count) road static info")
                            self.listener?.didFinishSyncRoadInfo(success: true)
                            
                            self.roadStaticInfoDownloaded = true
                            self.collectDataIfNeededV2(init: false)
                        } else {
                            Logger.d("data is not a json array")
                            self.listener?.didFinishSyncRoadInfo(success: false)
                        }
                    } else {
                        Logger.d("failed to parse json string")
                        self.listener?.didFinishSyncRoadInfo(success: false)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    self.listener?.didFinishSyncRoadInfo(success: false)
                })
                
        }
    }
    
    private func getRoadLevelDynamicInfoV2() {
        Logger.d("getRoadLevelDynamicInfoV2")
        listener?.didStartSyncRoadInfo()
        Alamofire.request(Config.urlRoadLevelValueV2, method: .get, parameters: nil, encoding: URLEncoding.default).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseString { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result

                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    guard let data = response.data else {
                        Logger.d("no road dynamic data")
                        self.listener?.didFinishSyncRoadInfo(success: false)
                        return
                    }
                    let utf8Text = String(data: data, encoding: .utf8) ?? String(decoding: data, as: UTF8.self)
//                    Logger.d("utf8Text: \(utf8Text)")
                    
                    if let json = self.stringToJsonV2(utf8Text) {
                        let responseJson = JSON.init(rawValue: json as Any)!
                        
                        let updatetime = responseJson["UpdateTime"].stringValue
                        Logger.d("updatetime: \(updatetime)")
                        if let date: Date = self.stringToDate(updatetime) {
                            self.timestampRoadDynamicInfo = date
                            Logger.d("self.timestampRoadDynamicInfo: \(self.timestampRoadDynamicInfo!)")
                        }

                        let interval = responseJson["UpdateInterval"].stringValue
                        self.intervalUpdateRoadDynamicInfo = Int(interval)
                        Logger.d("self.intervalUpdateRoadDynamicInfo: \(String(describing: self.intervalUpdateRoadDynamicInfo))")

                        if let jsonArray = responseJson["LiveTraffics"].array {
                            self.roadDynamicInfo = []
                            for datum in jsonArray {
//                                Logger.d("load datum from server response: \(datum)")
                                
                                let info: RoadDynamicInfo = RoadDynamicInfo(with: datum)
//                                Logger.d("road dynamic info: \(info)")
                                self.roadDynamicInfo!.append(info)
                            }
                            Logger.d("get \(self.roadDynamicInfo!.count) road dynamic info")
                            self.listener?.didFinishSyncRoadInfo(success: true)
                            
                            self.roadDynamicInfoDownloaded = true
                            self.collectDataIfNeededV2(init: false)
                        } else {
                            Logger.d("data is not a json array")
                            self.listener?.didFinishSyncRoadInfo(success: false)
                        }
                    } else {
                        Logger.d("failed to parse json string")
                        self.listener?.didFinishSyncRoadInfo(success: false)
                    }
                })

                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    self.listener?.didFinishSyncRoadInfo(success: false)
                })

        }
    }
    
    private func getCCTVInfo() {
        Logger.d("getCCTVInfo")
        listener?.didStartSyncRoadInfo()
        Alamofire.request(Config.urlCCTVInfoV2, method: .get, parameters: nil, encoding: URLEncoding.default).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseString { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result

                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    guard let data = response.data else {
                        Logger.d("no cctv data")
                        self.listener?.didFinishSyncRoadInfo(success: false)
                        return
                    }
                    let utf8Text = String(data: data, encoding: .utf8) ?? String(decoding: data, as: UTF8.self)
//                    Logger.d("utf8Text: \(utf8Text)")
                    
                    if let json = self.stringToJsonV2(utf8Text) {
                        let responseJson = JSON.init(rawValue: json as Any)!
                        
                        let updatetime = responseJson["UpdateTime"].stringValue
                        Logger.d("updatetime: \(updatetime)")
                        if let date: Date = self.stringToDate(updatetime) {
                            self.timestampCCTVInfo = date
                            Logger.d("self.timestampCCTVInfo: \(self.timestampCCTVInfo!)")
                        }

                        let interval = responseJson["UpdateInterval"].stringValue
                        self.intervalUpdateCCTVInfo = Int(interval)
                        Logger.d("self.intervalUpdateCCTVInfo: \(String(describing: self.intervalUpdateCCTVInfo))")

                        if let jsonArray = responseJson["CCTVs"].array {
                            self.cCTVInfo = []
                            for datum in jsonArray {
//                                Logger.d("load datum from server response: \(datum)")
                                
                                let info: CCTVInfo = CCTVInfo(with: datum)
//                                Logger.d("cctv info: \(info)")
                                self.cCTVInfo!.append(info)
                            }
                            Logger.d("get \(self.cCTVInfo!.count) cctv info")
                            self.listener?.didFinishSyncRoadInfo(success: true)
                            
                            self.cCTVInfoDownloaded = true
                            self.collectDataIfNeededV2(init: false)
                        } else {
                            Logger.d("data is not a json array")
                            self.listener?.didFinishSyncRoadInfo(success: false)
                        }
                    } else {
                        Logger.d("failed to parse cctv infos")
                        self.listener?.didFinishSyncRoadInfo(success: false)
                    }
                })

                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    self.listener?.didFinishSyncRoadInfo(success: false)
                })

        }
    }
    
    private func getRoadLocationV2() {
        Logger.d("getRoadLocationV2")
        listener?.didStartSyncRoadInfo()
        Alamofire.request(Config.urlRoadLocationV2, method: .get, parameters: nil, encoding: URLEncoding.default).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseString { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    guard let data = response.data else {
                        Logger.d("no road level data")
                        self.listener?.didFinishSyncRoadInfo(success: false)
                        return
                    }
                    let utf8Text = String(data: data, encoding: .utf8) ?? String(decoding: data, as: UTF8.self)
//                    Logger.d("utf8Text: \(utf8Text)")
                    if let json = self.stringToJson(utf8Text) {
                        let responseJson = JSON.init(rawValue: json as Any)!
                        
                        if let jsonArray = responseJson.array {
                            self.paths = []
                            for datum in jsonArray {
                                let path: Path = Path(with: datum)
//                                Logger.d("load path from server response: \(path)")
                                self.paths!.append(path)
                            }
                            Logger.d("get \(self.paths!.count) paths")
                            self.listener?.didFinishSyncRoadInfo(success: true)
                            
                            self.pathsDownloaded = true
                            self.collectDataIfNeededV2(init: false)
                        } else {
                            Logger.d("data is not a json array")
                            self.listener?.didFinishSyncRoadInfo(success: false)
                        }
                        
                    } else {
                        Logger.d("failed to parse json string")
                        self.listener?.didFinishSyncRoadInfo(success: false)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    self.listener?.didFinishSyncRoadInfo(success: false)
                })
                
        }
    }

    
    
    
    private func stringToDateV1(_ dateAsString: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        return dateFormatter.date(from: dateAsString)
    }
    
    private func stringToDate(_ dateAsString: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: dateAsString)
    }
    
    private func stringToJson(_ jsonAsString: String) -> Any? {
        let data = jsonAsString.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>] {
//                Logger.d(jsonArray)
                return jsonArray
            } else {
                Logger.d("bad json")
                return nil
            }
        } catch let error as NSError {
            Logger.d("error: \(error)")
            return nil
        }
    }
    
    private func stringToJsonV2(_ jsonAsString: String) -> Any? {
        let data = jsonAsString.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:AnyObject] {
//                Logger.d(jsonArray)
                return jsonArray
            } else {
                Logger.d("bad json")
                return nil
            }
        } catch let error as NSError {
            Logger.d("error: \(error)")
            return nil
        }
    }
    
    func dataReady() -> Bool {
        if !roadStaticInfoDownloaded || !roadDynamicInfoDownloaded || !cCTVInfoDownloaded || !pathsDownloaded {
            return false
        }
        
        if paths == nil || paths!.count == 0 {
            return false
        }
        
        if pointToPath == nil || pointToPath!.count == 0 {
            return false
        }
        
        if pointToKM == nil || pointToKM!.count == 0 {
            return false
        }
        
        if startEndToRouteId == nil || startEndToRouteId!.count == 0 {
            return false
        }
        
        if routeIdToRoute == nil || routeIdToRoute!.count == 0 {
            return false
        }
        
        if startEndToCCTVIds == nil || startEndToCCTVIds!.count == 0 {
            return false
        }
        
        if cCTVIdToCCTV == nil || cCTVIdToCCTV!.count == 0 {
            return false
        }
        
        return true
    }
    
    private func getKeyOf(startPointId: String, endPointId: String) -> String {
        let startEnd: String = "\(startPointId)-\(endPointId)"
        return startEnd
    }
    
    func getRouteBy(startPointId: String, endPointId: String) -> RoadDynamicInfo? {
        let key = getKeyOf(startPointId: startPointId, endPointId: endPointId)
        let routeId: String? = startEndToRouteId[key]
//        Logger.d("key: \(key)")
        if let routeId = routeId {
            return routeIdToRoute[routeId]
        } else {
            Logger.d("route id not found")
            return nil
        }
    }
    
    func getPaths() -> [Path] {
        if let paths = paths {
            return paths
        } else {
            return []
        }
    }
    
    func getPath(of pointId: String) -> Path? {
        return pointToPath[pointId]
    }
    
    func getKMInfo(from pointId: String) -> String? {
        if let str = pointToKM[pointId] {
            let substring1 = str.dropLast(2)
            let result = String(substring1)
            let replaced = result.replacingOccurrences(of: "K+", with: ".")
            return "\(replaced)K"
        } else {
            return nil
        }
    }
    
    func getLastUpdate() -> Date {
        return timestampLastUpdated
    }
    
    func getCCTVsBy(startPointId: String, endPointId: String) -> [CCTVDynamicInfo]? {
        let key = getKeyOf(startPointId: startPointId, endPointId: endPointId)
//        Logger.d("key: \(key)")
        if let cCTVIds: [String] = startEndToCCTVIds[key] {
            var cCTVs: [CCTVDynamicInfo] = []
            for cCTVId in cCTVIds {
                if let cCTV = cCTVIdToCCTV[cCTVId] {
                    cCTVs.append(cCTV)
                }
            }
            return cCTVs
        } else {
//            Logger.d("cctv ids not found")
            return nil
        }
    }
}




class RoadStaticInfo: NSObject {
    let routeid: String
    let sourceid: String
//    let locationpath: Int
    let startlocationpoint: String
    let endlocationpoint: String
    let roadtype: Int
    let fromkm: String
    let tokm: String

    public override var description: String {
        return "\n{\n routeid: \(self.routeid),\n"
            + " sourceid: \(self.sourceid),\n"
            + " startlocationpoint: \(self.startlocationpoint),\n"
            + " endlocationpoint: \(self.endlocationpoint),\n"
            + " roadtype: \(self.roadtype),\n"
            + " fromkm: \(self.fromkm),\n"
            + " tokm: \(self.tokm)\n}"
    }

    init(with json:JSON) {
        routeid = json["SectionID"].stringValue
        sourceid = json["RoadID"].stringValue
        startlocationpoint = "\(sourceid)-\(json["RoadSection"]["Start"].stringValue)"
        endlocationpoint = "\(sourceid)-\(json["RoadSection"]["End"].stringValue)"
        roadtype = json["RoadClass"].intValue
        fromkm = json["SectionMile"]["StartKM"].stringValue
        tokm = json["SectionMile"]["EndKM"].stringValue

        super.init()
    }
}

class RoadDynamicInfo: NSObject {
    let routeid: String
    let level: Int
    let value: Int
    let traveltime: Int
    
    public override var description: String {
        return "\n{\n routeid: \(self.routeid),\n"
            + " level: \(self.level),\n"
            + " value: \(self.value),\n"
            + " traveltime: \(self.traveltime)\n}"
    }

    init(with json:JSON) {
        routeid = json["SectionID"].stringValue
        level = json["CongestionLevel"].intValue
        value = json["TravelSpeed"].intValue
        traveltime = json["TravelTime"].intValue

        super.init()
    }
}

class CCTVDynamicInfo: NSObject {
    let cctvid: String
    let url: String
    let status: Int
    
    init(with info:CCTVInfo) {
        cctvid = info.cctvid
        url = info.videoStreamURL
        status = 0

        super.init()
    }
}

class CCTVInfo: NSObject {
    let cctvid: String
    let videoStreamURL: String
    let locationType: Int
    let positionLon: Double
    let positionLat: Double
    let roadID: String
    let roadClass: Int
    let startlocationpoint: String
    let endlocationpoint: String
    
    
    public override var description: String {
        return "\n{\n cctvid: \(self.cctvid),\n"
            + " videoStreamURL: \(self.videoStreamURL),\n"
            + " locationType: \(self.locationType),\n"
            + " positionLon: \(self.positionLon),\n"
            + " positionLat: \(self.positionLat),\n"
            + " roadID: \(self.roadID),\n"
            + " roadClass: \(self.roadClass),\n"
            + " startlocationpoint: \(self.startlocationpoint),\n"
            + " endlocationpoint: \(self.endlocationpoint)\n}"
    }

    init(with json:JSON) {
        cctvid = json["CCTVID"].stringValue
        videoStreamURL = json["VideoStreamURL"].stringValue
        locationType = json["LocationType"].intValue
        positionLon = json["PositionLon"].doubleValue
        positionLat = json["PositionLat"].doubleValue
        roadID = json["RoadID"].stringValue
        roadClass = json["RoadClass"].intValue
        startlocationpoint = "\(roadID)-\(json["RoadSection"]["Start"].stringValue)"
        endlocationpoint = "\(roadID)-\(json["RoadSection"]["End"].stringValue)"

        super.init()
    }
}
