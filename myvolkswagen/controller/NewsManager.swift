//
//  NewsManager.swift
//  myvolkswagen
//
//  Created by Apple on 2/27/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Firebase

protocol NewsProtocol: class {
    func didStartGetNewsCarModel()
    func didFinishGetNewsCarModel(success: Bool, carModels: [Model]?)
    
    func didStartGetNewsList(loadMore: Bool)
    func didFinishGetNewsList(success: Bool, loadMore: Bool, newsList: [News]?, newsKind: NewsKind)
    
    func didStartLikeNews()
    func didFinishLikeNews(success: Bool, news: News, set: Bool)
    
    func didStartTrackNews()
    func didFinishTrackNews(success: Bool, news: News, set: Bool)
    
    func didStartReportNews()
    func didFinishReportNews(success: Bool, news: News, type: Int)
    
    func didStartCommentNews()
    func didFinishCommentNews(success: Bool, news: News)
    
    func didStartGetNews()
    func didFinishGetNews(success: Bool, news: News?, noticeKind: NoticeKind?)
    
    func didStartGetImageNews()
    func didFinishGetImageNews(success: Bool, news: [ImageNews]?)
    
    func didStartAddPost()
    func didFinishAddPost(success: Bool, newsId: String?)
    
    func didStartEditPost()
    func didFinishEditPost(success: Bool, newsId: Int?)
    
    func didStartDeletePost()
    func didFinishDeletePost(success: Bool, news: News?)
    
    func didStartDeleteComment()
    func didFinishDeleteComment(success: Bool, comment: Comment?)
    
    func didStartGetMemberContent()
    func didFinishGetMemberContent(success: Bool, newsList: [News]?)
    
    func didStartGetMembersOfBehavior()
    func didFinishGetMembersOfBehavior(success: Bool, members: [Relation]?)
}

enum PhotoSourceType: Int {
    case album = 0, camera, none
}

enum NewsKind: Int {
    case all = 1, highlighted, club, favoritePost, follow, myPost
}

enum Behavior: Int {
    case like = 1
}

class NewsManager: NSObject {

    // MARK: - Properties
    private var carModels: [Model]
    private var newsListAll: [News]
    private var newsListHighlighted: [News]
    var listIsDirtyRemote: Bool = false
    var listIsDirty: Bool = false
    private var newsListPageIndex: Int = 1 // starts from 1
    private var listEndReached: Bool = false
    private var imageNewsListAll: [ImageNews]
    
    private static var sharedNewsManager: NewsManager = {
        let newsManager = NewsManager()
        
        // Configuration
        // ...
        
        return newsManager
    }()
    
    
    // Initialization
    override private init() {
        carModels = []
        newsListAll = []
        newsListHighlighted = []
        imageNewsListAll = []
        
        super.init()
        
        carModels = getCarModel(remotely: true, listener: nil)
    }
    
    // MARK: - Accessors
    
    class func shared() -> NewsManager {
        return sharedNewsManager
    }
    
    
    
    func getModel(by id: Int) -> Model? {
        for model in carModels {
            if id == model.id {
                return model
            }
        }
        return nil
    }
    
    func getCarModel(remotely: Bool, listener: NewsProtocol?) -> [Model] {
        if remotely {
            listener?.didStartGetNewsCarModel()
            Alamofire.request(Config.urlCommunityServer + "APP_CU_Get_Car_Model?token=\(UserManager.shared().getUserToken()!)", method: .get, parameters: nil).validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                    
                    if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                        let arr: [String] = authorization.components(separatedBy: " ")
                        
                        // "Bearer xxxxx"
                        if arr.count == 2 {
                            UserManager.shared().saveUserToken(with: arr[1])
                        }
                    }
                    
                    response.result.ifSuccess({
//                        Logger.d("getCarModel response: \(String(describing: response.result.value))")
                        let responseJson = JSON.init(rawValue: response.result.value as Any)!
                        
                        let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                        if result.isSuccess() {
                            if let data = responseJson["ModelList"].array {
                                self.carModels = []
                                for datum in data {
                                    let model: Model = Model.init(with: datum)
                                    self.carModels.append(model)
                                }
//                                Logger.d("get \(self.carModels.count) car models")
                            }
//                            Logger.d("self.carModels \(self.carModels)")
                            
//                            self.listIsDirtyRemote = false
                            listener?.didFinishGetNewsCarModel(success: true, carModels: self.carModels)
                        } else {
                            Logger.d("errorCode: \(String(describing: result.errorCode))")
                            Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                            listener?.didFinishGetNewsCarModel(success: false, carModels: nil)
                            Utility.checkTokenValidation(of: result)
                        }
                    })
                    
                    response.result.ifFailure({
                        Logger.d("error: \(response.error as Any)")
                        listener?.didFinishGetNewsCarModel(success: false, carModels: nil)
                    })
                    
            }
        }
        
        return carModels
    }
    
    func isListEndReached() -> Bool {
        return listEndReached
    }
    
    func getNewsList(remotely: Bool, kind: NewsKind, models: String?, loadMore: Bool, listener: NewsProtocol?) -> [News] {
        var operatingList: [News] = newsListAll
        if kind == .highlighted {
            operatingList = newsListHighlighted
        }
        
//        Logger.d("operatingList size: \(operatingList.count)")
        
        if remotely || operatingList.count == 0 {
            if !loadMore {
                newsListPageIndex = 1
                listEndReached = false
            }
            
            var parameters: Parameters = [
                "Kind": "\(kind.rawValue)",
                "Page": newsListPageIndex
            ]
            
            if let models = models {
                parameters["ModelList"] = models
            }
            
//            Logger.d("parameters: \(parameters)")
//            Logger.d("UserManager.shared().getUserToken()!: \(UserManager.shared().getUserToken()!)")
            
            listener?.didStartGetNewsList(loadMore: loadMore)
            Alamofire.request(Config.urlCommunityServer + "APP_CU_Get_News_List?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                    
                    if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                        let arr: [String] = authorization.components(separatedBy: " ")
                        
                        // "Bearer xxxxx"
                        if arr.count == 2 {
                            UserManager.shared().saveUserToken(with: arr[1])
                        }
                    }
                    
                    response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                        let responseJson = JSON.init(rawValue: response.result.value as Any)!
                        
                        let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                        if result.isSuccess() {
                            self.listEndReached = true
                            if let data = responseJson["NewsList"].array {
                                if !loadMore {
                                    operatingList = []
                                }
                                for datum in data {
                                    let news: News = News(with: datum)
//                                    Logger.d("load news from server response: \(news)")
                                    operatingList.append(news)
                                    self.listEndReached = false
                                }
                            }
                            
//                            Logger.d("now we have \(operatingList.count) news")
                            
                            if kind == .highlighted {
                                self.newsListHighlighted = operatingList
                            } else {
                                self.newsListAll = operatingList
                            }
                            
                            if !self.listEndReached {
                                self.newsListPageIndex += 1
                            }
                            
                            listener?.didFinishGetNewsList(success: true, loadMore: loadMore, newsList: operatingList, newsKind: kind)
                        } else {
                            Logger.d("errorCode: \(String(describing: result.errorCode))")
                            Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                            listener?.didFinishGetNewsList(success: false, loadMore: loadMore, newsList: nil, newsKind: kind)
                            Utility.checkTokenValidation(of: result)
                        }
                    })
                    
                    response.result.ifFailure({
                        Logger.d("error: \(response.error as Any)")
                        listener?.didFinishGetNewsList(success: false, loadMore: loadMore, newsList: nil, newsKind: kind)
                    })
            }
        }
        
        return operatingList
    }
    
    func like(news: News, set: Bool, listener: NewsProtocol?) {
        let parameters: Parameters = [
            "NewsID": "\(news.newsId!)",
            "Action": set ? "1" : "0"
        ]
        
//        Logger.d("like parameters: \(parameters)")
        
        listener?.didStartLikeNews()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_News_Like?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        Analytics.logEvent("post_like", parameters: nil)
                        listener?.didFinishLikeNews(success: true, news: news, set: set)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishLikeNews(success: false, news: news, set: set)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishLikeNews(success: false, news: news, set: set)
                })
                
        }
    }
    
    func track(news: News, set: Bool, listener: NewsProtocol?) {
        let parameters: Parameters = [
            "NewsID": "\(news.newsId!)",
            "Action": set ? "1" : "0"
        ]
        
//        Logger.d("track parameters: \(parameters)")
        
        listener?.didStartTrackNews()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_News_Bookmark?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        if set {
                            Analytics.logEvent("post_save", parameters: nil)
                        }
                        listener?.didFinishTrackNews(success: true, news: news, set: set)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishTrackNews(success: false, news: news, set: set)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishTrackNews(success: false, news: news, set: set)
                })
                
        }
    }
    
    // type 1: garbage
    // type 2: unproper
    func report(news: News, type: Int, listener: NewsProtocol?) {
        let parameters: Parameters = [
            "NewsID": "\(news.newsId!)",
            "ReportItem": "\(type)"
        ]
        
//        Logger.d("report parameters: \(parameters)")
        
        listener?.didStartReportNews()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_News_Report?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishReportNews(success: true, news: news, type: type)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishReportNews(success: false, news: news, type: type)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishReportNews(success: false, news: news, type: type)
                })
                
        }
    }
    
    func comment(news: News, content: String, listener: NewsProtocol?) {
        if content.count == 0 {
            Logger.d("empty coment")
            return
        }
        
        let parameters: Parameters = [
            "NewsID": "\(news.newsId!)",
            "Content": "\(content)"
        ]
        
//        Logger.d("comment parameters: \(parameters)")
        
        listener?.didStartCommentNews()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Append_News_Comment?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        Analytics.logEvent("post_comment", parameters: nil)
                        listener?.didFinishCommentNews(success: true, news: news)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishCommentNews(success: false, news: news)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishCommentNews(success: false, news: news)
                })
        }
    }
    
    func getNews(news: News, noticeKind: NoticeKind?, listener: NewsProtocol?) {
        
        let parameters: Parameters = [
            "NewsID": "\(news.newsId!)"
        ]
        
//        Logger.d("getNews parameters: \(parameters)")
        
        listener?.didStartGetNews()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Get_News?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        news.update(with: responseJson)
//                        Logger.d("updated news: \(news)")
                        listener?.didFinishGetNews(success: true, news: news, noticeKind: noticeKind)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetNews(success: false, news: nil, noticeKind: noticeKind)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetNews(success: false, news: nil, noticeKind: noticeKind)
                })
        }
    }
    
    func add(imageStreams: [String], content: String?, models: String, listener: NewsProtocol?) {
        
        var parameters: Parameters = [
            "PhotoStreamArray": imageStreams,
            "ModelList": models
        ]
        
        if let content = content {
            parameters["Content"] = content
        }
        
//        Logger.d("add parameter models: \(models)")
//        Logger.d("add parameter content: \(String(describing: content))")
        
        listener?.didStartAddPost()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Append_News?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        self.listIsDirtyRemote = true
                        if let newsId: String = responseJson["NewsID"].string {
                            Logger.d("added newsId: \(newsId)")
                            listener?.didFinishAddPost(success: true, newsId: newsId)
                        } else {
                            listener?.didFinishAddPost(success: true, newsId: nil)
                        }
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishAddPost(success: false, newsId: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishAddPost(success: false, newsId: nil)
                })
        }
    }
    
    func edit(newsId: Int, content: String?, models: String, modelList: [Model], listener: NewsProtocol?) {
        
        var parameters: Parameters = [
            "NewsID": "\(newsId)",
            "ModelList": models
        ]
        
        if let content = content {
            parameters["Content"] = content
        }
        
        Logger.d("edit parameter models: \(models)")
//        Logger.d("edit parameter content: \(String(describing: content))")
        
        listener?.didStartEditPost()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Update_News?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        // set remote dirty directly
//                        self.listIsDirtyRemote = true
                        
                        // set local dirty
                        for news in self.newsListAll {
                            if news.newsId == newsId {
                                news.content = content
                                news.modelList = modelList
                                break
                            }
                        }
                        for news in self.newsListHighlighted {
                            if news.newsId == newsId {
                                news.content = content
                                news.modelList = modelList
                                break
                            }
                        }
                        self.listIsDirty = true
                        
                        listener?.didFinishEditPost(success: true, newsId: newsId)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishEditPost(success: false, newsId: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishEditPost(success: false, newsId: nil)
                })
        }
    }
    
    func delete(news: News, listener: NewsProtocol?) {
        
        let parameters: Parameters = [
            "NewsID": "\(news.newsId!)"
        ]
        
//        Logger.d("delete parameters: \(parameters)")
        
        listener?.didStartDeletePost()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_News_Delete?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        self.listIsDirtyRemote = true
                        listener?.didFinishDeletePost(success: true, news: news)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishDeletePost(success: false, news: news)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishDeletePost(success: false, news: news)
                })
        }
    }
    
    func delete(comment: Comment, listener: NewsProtocol?) {
        
        let parameters: Parameters = [
            "CommentID": "\(comment.commentId!)"
        ]
        
//        Logger.d("delete parameters: \(parameters)")
        
        listener?.didStartDeleteComment()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Comment_Delete?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishDeleteComment(success: true, comment: comment)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishDeleteComment(success: false, comment: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishDeleteComment(success: false, comment: nil)
                })
        }
    }
    
    func getMemberContent(memberProfile: Profile, contentKind: ContentKind, listener: NewsProtocol?) {
        if memberProfile.accountId == nil {
            Logger.d("no account id, stop fetching member content")
            return
        }
        
        var parameters: Parameters = [
            "AccountID": memberProfile.accountId!
        ]
        
        // 1 for me, 2 for others, 3 for official
        var who: Int = 1
        if memberProfile.offical == Role.official.rawValue {
            who = 3
        } else if !memberProfile.isMine() {
            who = 2
        }
        parameters["Who"] = "\(who)"
        
        // me: 1 for posts, 2 for collection
        // others: 1 for posts
        // official: 1 for activity, 2 for new, 3 for all
        switch contentKind {
        case .post:
            parameters["Kind"] = "1"
        case .collection:
            parameters["Kind"] = "2"
        case .all:
            parameters["Kind"] = "3"
        case .new:
            parameters["Kind"] = "2"
        case .activity:
            parameters["Kind"] = "1"
        }
        
//        Logger.d("getMemberContent parameters: \(parameters)")
        
        listener?.didStartGetMemberContent()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Member_Main_Content?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var newsList: [News] = []
                        if let data = responseJson["NewsList"].array {
                            for datum in data {
                                let news: News = News(with: datum)
//                                Logger.d("load news from server response: \(news)")
                                newsList.append(news)
                            }
                        }
                        
//                        Logger.d("now we have \(newsList.count) news")
                        
                        listener?.didFinishGetMemberContent(success: true, newsList: newsList)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetMemberContent(success: false, newsList: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetMemberContent(success: false, newsList: nil)
                })
                
        }
    }
    
    func getMembersOfBehavior(news: News, behavior: Behavior, listener: NewsProtocol?) {
        let parameters: Parameters = [
            "NewsID": "\(news.newsId!)",
            "Behavior": "\(behavior.rawValue)"
        ]
        
//        Logger.d("getMembersOfBehavior parameters: \(parameters)")
        
        listener?.didStartGetMembersOfBehavior()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_MemberList_In_Behavior?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var relations: [Relation] = []
                        if let data = responseJson["MemberRelationList"].array {
                            for datum in data {
                                let relation: Relation = Relation(with: datum)
//                                    Logger.d("load news from server response: \(news)")
                                relations.append(relation)
                            }
                        }
                        
//                        Logger.d("now we have \(relations.count) members")
                        
                        listener?.didFinishGetMembersOfBehavior(success: true, members: relations)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetMembersOfBehavior(success: false, members: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetMembersOfBehavior(success: false, members: nil)
                })
                
        }
    }
    
    func getAllNews(listener: NewsProtocol?) -> [ImageNews] {
        if imageNewsListAll.count == 0 {
            listener?.didStartGetImageNews()
            Alamofire.request(Config.urlCommunityServer + "app/news", method: .get, parameters: nil).validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"]).responseJSON { response in
                    //                Logger.d("Request: \(String(describing: response.request))")   // original url request
                    //                Logger.d("Response: \(String(describing: response.response))") // http url response
                    //                Logger.d("Result: \(response.result)")                         // response serialization result
                    
                    if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                        let arr: [String] = authorization.components(separatedBy: " ")
                        
                        // "Bearer xxxxx"
                        if arr.count == 2 {
                            UserManager.shared().saveUserToken(with: arr[1])
                        }
                    }
                    
                    response.result.ifSuccess({
                        //                    Logger.d(response.result.value as Any)
                        let responseJson = JSON.init(rawValue: response.result.value as Any)!
                        
                        let result: Result = Result(with: responseJson)
                        //                    Logger.d("result: \(result)")
                        if result.isSuccess() {
                            self.imageNewsListAll = responseJson["Data"].arrayValue.map { (jsonn) -> ImageNews in
                                ImageNews(with: jsonn)
                            }
                            //                        Logger.d("updated news: \(news)")
                            listener?.didFinishGetImageNews(success: true, news: self.imageNewsListAll)
                        } else {
                            Logger.d("errorCode: \(String(describing: result.errorCode))")
                            Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                            listener?.didFinishGetImageNews(success: false, news: self.imageNewsListAll)
                        }
                    })
                    
                    response.result.ifFailure({
                        Logger.d("error: \(response.error as Any)")
                        listener?.didFinishGetImageNews(success: false, news: nil)
                    })
                }
        }
        return imageNewsListAll
    }
}
