//
//  CarManager.swift
//  myvolkswagen
//
//  Created by Apple on 2/24/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Firebase

protocol CarProtocol: class {
    func didStartGetCar()
    func didFinishGetCar(success: Bool, cars: [Car])
    
    func didStartGetCarPAT()
    func didFinishGetCarPAT(success: Bool, pat: Pat?, msg: String?)
    
    func didStartSaveCarPAT()
    func didFinishSaveCarPAT(success: Bool, msg: String?)
    
    func didStartRemoveCar()
    func didFinishRemoveCar(success: Bool, car: Car)
    
    func didStartCheckCarExist()
    func didFinishCheckCarExist(result: Result?)
    
    func didStartGetCarModel()
    func didFinishGetCarModel(success: Bool, models: [Model]?)
    
    func didStartAddCar()
    func didFinishAddCar(success: Bool)
    
    func didStartGetCarMaintenancePlant()
    func didFinishGetCarMaintenancePlant(success: Bool, maintenancePlants: [MaintainancePlant]?)
    
    func didStartGetCarBtns()
    func didFinishGetCarBtns(success: Bool, btns: [CarBtn]?)
    
    func didStartGetDongleBinding()
    func didFinishGetDongleBinding(state: CarDoungleState?, dongleData: DongleData?)
    
    func didStartPostDongleBinding()
    func didFinishPostDongleBinding(state: CarDoungleState?, dongleData: DongleData?, msg: String?)
    
    func didStartDeleteDongleBinding()
    func didFinishDeleteDongleBinding(success: Bool, car: Car?)
    
    func didStartGetCarMaintanceInfo()
    func didFinishGetCarMaintanceInfo(success: Bool, info: [MaintainanceInfo]?)
    
    func didStartGetDongleSummary()
    func didFinishGetDongleSummary(data: DongleSummary?)
    
    func didStartGetDongleNotification()
    func didFinishGetDongleNotification(success: Bool, notificationList: [Notificationn]?, notificationDialogList: [Notificationn]?)
    
    func didStartGetCarWarranty()
    func didFinishGetCarWarranty(success: Bool, date: String?)
    
    func didStartGetCarTrips()
    func didFinishGetCarTrips(success: Bool, trips: [CarTrip]?)
    
    func didStartGetExtUrl()
    func didFinishGetExtUrl(success: Bool, type: String, url: String?)
}

enum CarDoungleState: Int {
//    ACTIVATED, DISCARDED, EXPIRED,
//    USER_MAPPED, VIN_MAPPED
    case VIN_MAPPED = 1, // 未綁定
         USER_MAPPED, // 已綁定
         DEVICE_NOT_FOUND, // 未安裝行車助理
         ACTIVATED,
         DISCARDED,
         EXPIRED
}

class CarManager: NSObject {
    
    // MARK: - Properties
    var cars: [Car]
    //private var cars: [Car]
    private var addingCar: AddingCar?
    private var maintenancePlant: [MaintainancePlant]?
    var assistantRedirectUrl: String?
    var addCarFromMainPage: Bool = false
    var carBtns: [CarBtn]?
    let keyCarNumber = "key_car_number"
    let keyDongleNotiTime = "key_dongle_time"
    static let nameCarReloadNotification: String = "nameCarReloadNotification"
    static let nameCarGarageReloadNotification: String = "nameCarGarageReloadNotification"
    static let nameCarAssistantReloadNotification: String = "nameCarAssistantReloadNotification"
    var openhubUrl: String?
    
    private static var sharedCarManager: CarManager = {
        let carManager = CarManager()
        
        // Configuration
        // ...
        
        return carManager
    }()
    
    
    // Initialization
    override private init() {
        cars = []
        
        super.init()
    }
    
    func printCurrentTime(str: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss.SSS"

        // 獲取現在的日期和時間，包含毫秒
        let currentDate = Date()

        // 使用 DateFormatter 格式化日期和時間，包含毫秒
        let formattedDate = dateFormatter.string(from: currentDate)

        // 印出格式化後的日期和時間，包含毫秒
        print("\(str) - Current Time：\(formattedDate)")
    }
    
    // MARK: - Accessors
    
    class func shared() -> CarManager {
        return sharedCarManager
    }

    func getSelectedCarNumber() -> String? {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: keyCarNumber)
    }
    
    func clearSelectedCarNumber() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: keyCarNumber)
    }
    
    func saveSelectedCarNumer(with carNumber: String) {
        let defaults = UserDefaults.standard
        defaults.set(carNumber, forKey: self.keyCarNumber)
    }
    
    func getDongleNotiTimeEvent() -> String? {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: keyDongleNotiTime)
    }
    
    func clearDongleNotiTimeEvent() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: keyDongleNotiTime)
    }
    
    func saveDongleNotiTimeEvent(with time: String) {
        let defaults = UserDefaults.standard
        defaults.set(time, forKey: self.keyDongleNotiTime)
    }
    
    func getCars(remotely: Bool, listener: CarProtocol?) -> [Car] {
        if remotely {
            listener?.didStartGetCar()
            print("user token: \(UserManager.shared().getUserToken()!)")
            Alamofire.request(Config.urlMemberServer + "APP_Get_MyCar_Data?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: nil).validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                    
                    if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                        let arr: [String] = authorization.components(separatedBy: " ")
                        
                        // "Bearer xxxxx"
                        if arr.count == 2 {
                            UserManager.shared().saveUserToken(with: arr[1])
                        }
                    }
                    
                    response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                        let responseJson = JSON.init(rawValue: response.result.value as Any)!
                        
                        let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                        if result.isSuccess() {
                            self.cars = []
                            if let data = responseJson["datas"].array {
                                // test 0930 --------------------------- 假資料
//                                let car1: Car = Car.init()
//                                car1.name = "蔡介偉"
//                                car1.licensePlateNumber = "AVQ-5988"
//                                car1.carTypeName = "The T-Roc R"
//                                car1.carModelName = "280 TSI Comfortline"
//                                car1.ownerDate = "2023/09/30"
//                                self.cars.append(car1)
//                                let car2: Car = Car.init()
//                                car2.name = "蔡介偉"
//                                car2.licensePlateNumber = "ABC-1234"
//                                car2.carTypeName = "Golf"
//                                car2.carModelName = "280 TSI Comfortline"
//                                car2.ownerDate = "2021/01/30"
//                                self.cars.append(car2)
//                                let car3: Car = Car.init()
//                                car3.name = "蔡介偉"
//                                car3.licensePlateNumber = "ABB-6032"
//                                car3.carTypeName = "Golf"
//                                car3.carModelName = "330 TSI R-Line"
//                                car3.ownerDate = "2022/03/20"
//                                self.cars.append(car3)
//                                let car4: Car = Car.init()
//                                car4.name = "蔡介偉"
//                                car4.licensePlateNumber = "ABR-3392"
//                                car4.carTypeName = "Tiguan"
//                                car4.carModelName = "330 TSI R-Line"
//                                car4.ownerDate = "2020/05/07"
//                                self.cars.append(car4)
                                // test 0930 ---------------------------
                                for datum in data {
                                    let car: Car = Car(with: datum)
//                                    Logger.d("load car from server response: \(car)")
                                    self.cars.append(car)
                                }
//                                Logger.d("get \(self.cars.count) cars")
                            }
                            
                            listener?.didFinishGetCar(success: true, cars: self.cars)
                        } else {
                            Logger.d("errorCode: \(String(describing: result.errorCode))")
                            Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                            listener?.didFinishGetCar(success: false, cars: self.cars)
                            Utility.checkTokenValidation(of: result)
                        }
                    })
                    
                    response.result.ifFailure({
                        Logger.d("error: \(response.error as Any)")
                        listener?.didFinishGetCar(success: false, cars: self.cars)
                    })
                    
            }
        }
        
        return cars
    }
    
    func remove(car: Car, listener: CarProtocol?) {
        var parameters: Parameters = [
            "VIN": car.vin!,
            "LicensePlateNumber": car.licensePlateNumber!
        ]
        
        if let name: String = car.name {
            parameters["Name"] = name
        }
        
        listener?.didStartRemoveCar()
        Alamofire.request(Config.urlMemberServer + "APP_Del_Car_Data?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var cs: [Car] = []
                        for c in self.cars {
                            if c.vin! == car.vin! && c.licensePlateNumber! == car.licensePlateNumber! {
                                
                            } else {
                                cs.append(c)
                            }
                        }
                        self.cars = cs
                        
                        listener?.didFinishRemoveCar(success: true, car: car)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: CarManager.nameCarReloadNotification), object: nil, userInfo: nil)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishRemoveCar(success: false, car: car)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishRemoveCar(success: false, car: car)
                })
                
        }
    }
    
    func checkExist(listener: CarProtocol?) {
        let parameters: Parameters = [
            "Name": addingCar!.name!,
            "VIN": addingCar!.vinLast5!,
            "LicensePlateNumber": addingCar!.getFullPlateNumber()
        ]
//        Logger.d("checkExist parameters: \(parameters)")
        listener?.didStartCheckCarExist()
        Alamofire.request(Config.urlMemberServer + "APP_Get_CarExist?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        let typeId = responseJson["CarTypeID"].intValue
                        let typeName = responseJson["CarTypeName"].stringValue
//                        Logger.d("typeId: \(typeId), typeName: \(typeName)")
                        
                        self.appendAddingCarType(typeId: typeId, typeName: typeName)
                        
                        listener?.didFinishCheckCarExist(result: result)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishCheckCarExist(result: result)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishCheckCarExist(result: nil)
                })
                
        }
    }
    
    func getModel(listener: CarProtocol?) {
        let parameters: Parameters = [
            "CarTypeID": addingCar!.typeId!
        ]
//        Logger.d("getModel parameters: \(parameters)")
        listener?.didStartGetCarModel()
        Alamofire.request(Config.urlMemberServer + "APP_Get_CarModel_Data?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var models: [Model] = []
                        
                        if let data = responseJson["CarModel"].array {
                            for datum in data {
                                let model: Model = Model(with: datum)
//                                Logger.d("load model from server response: \(model)")
                                models.append(model)
                            }
//                            Logger.d("get \(models.count) models")
                        }
                        
                        if models.isEmpty {
//                            Logger.d("strange, no models from api")
                            listener?.didFinishGetCarModel(success: false, models: nil)
                        } else {
                            self.appendAddingCarModels(models: models)
                            listener?.didFinishGetCarModel(success: true, models: models)
                        }
                        
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetCarModel(success: false, models: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetCarModel(success: false, models: nil)
                })
                
        }
    }
    
    func getMaintenancePlant(listener: CarProtocol?) -> [MaintainancePlant]? {
        if self.maintenancePlant != nil {
            return self.maintenancePlant
        }
        
        listener?.didStartGetCarMaintenancePlant()
        Alamofire.request(Config.urlMemberServer + "APP_Get_Maintenance_Plant?token=\(UserManager.shared().getUserToken()!)", method: .get, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        var mp: [MaintainancePlant] = []
                        
                        if let data = responseJson["Data"].array {
                            for datum in data {
                                let mpp: MaintainancePlant = MaintainancePlant(with: datum)
//                                Logger.d("load model from server response: \(model)")
                                mp.append(mpp)
                            }
//                            Logger.d("get \(models.count) models")
                        }
                        
                        if mp.isEmpty {
//                            Logger.d("strange, no models from api")
                            listener?.didFinishGetCarMaintenancePlant(success: false, maintenancePlants: nil)
                        } else {
                            self.maintenancePlant = mp
                            listener?.didFinishGetCarMaintenancePlant(success: true, maintenancePlants: mp)
                        }
                        
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetCarMaintenancePlant(success: false, maintenancePlants: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetCarMaintenancePlant(success: false, maintenancePlants: nil)
                })
        }
        return self.maintenancePlant
    }
    
    func getCarBtns(vin: String, plateNum: String, listener: CarProtocol?) -> [CarBtn]? {
        listener?.didStartGetCarBtns()
        Alamofire.request(Config.urlMemberServer + "APP_Get_MyCar_Buttons?VIN=\(vin)&LicensePlateNumber=\(plateNum)&token=\(UserManager.shared().getUserToken()!)", method: .get, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    if result.isSuccess() {
                        var mp: [CarBtn] = []
                        
                        if let data = responseJson["Data"]["Buttons"].array {
                            for datum in data {
                                let mpp: CarBtn = CarBtn(with: datum)
                                mp.append(mpp)
                            }
                        }
                        
                        if mp.isEmpty {
                            listener?.didFinishGetCarBtns(success: false, btns: nil)
                        } else {
                            self.carBtns = mp
                            listener?.didFinishGetCarBtns(success: true, btns: self.carBtns)
                        }
                        
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetCarBtns(success: false, btns: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetCarBtns(success: false, btns: nil)
                })
        }
        return self.carBtns
    }
    
    func addCar(listener: CarProtocol?) {
        let parameters: Parameters = [
            "Name": addingCar!.name!,
            "VIN": addingCar!.vinLast5!,
            "LicensePlateNumber": addingCar!.getFullPlateNumber(),
            "CarTypeID": "\(addingCar!.typeId!)",
            "CarModelID": "\(addingCar!.models![addingCar!.selectedModelIndex].id!)"
        ]
//        Logger.d("addCar parameters: \(parameters)")
        listener?.didStartAddCar()
        Alamofire.request(Config.urlMemberServer + "APP_Add_Car_Data?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        Logger.d("existing car count: \(self.cars.count)")
                        if self.cars.count == 0 {
                            Analytics.logEvent("add_car_first", parameters: nil)
                        }
                        Analytics.logEvent("add_car_complete", parameters: nil)
                        print("add_car_completeadd_car_completeadd_car_completeadd_car_complete")
                        listener?.didFinishAddCar(success: true)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: CarManager.nameCarReloadNotification), object: nil, userInfo: nil)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishAddCar(success: false)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishAddCar(success: false)
                })
                
        }
    }
    
    func getCarPAT(listener: CarProtocol?, vin: String, carNumber: String, currentMileage: Int, annualMileage: Int?, cycleDay: Int?) {
        let parameters: Parameters = [
            "VIN": vin,
            "CarNumber": carNumber,
            "CurrentMileage": currentMileage
        ]
        
        listener?.didStartGetCarPAT()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_PAT_Get_Content?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                    if result.isSuccess() {
                        let pat: Pat = Pat(with: responseJson["PAT"])
                        listener?.didFinishGetCarPAT(success: true, pat: pat, msg: nil)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetCarPAT(success: false, pat: nil, msg: "\(String(describing: result.errorMsg))")
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetCarPAT(success: false, pat: nil, msg: "")
                })
                
        }
    }
    
    func saveCarPAT(listener: CarProtocol?, patId: Int) {
        let parameters: Parameters = [
            "PAT_ID": patId
        ]
        
        listener?.didStartSaveCarPAT()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_PAT_Send?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                        Logger.d("result: \(result)")
                    if result.isSuccess() {
                        let pat: Pat = Pat(with: responseJson["PAT"])
                        listener?.didFinishSaveCarPAT(success: true, msg: nil)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishSaveCarPAT(success: false, msg: "\(String(describing: result.errorMsg))")
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishSaveCarPAT(success: false, msg: "")
                })
                
        }
    }
    
    func getAddingCar() -> AddingCar? {
        return addingCar
    }
    
    func isAddingFirstCar() -> Bool {
        return cars.isEmpty
    }
    
    func deleteAddingCar() {
        addingCar = nil
    }
    
    func startAddingCar() {
        if addingCar == nil {
            addingCar = AddingCar.init()
        }
        
        if !isAddingFirstCar() {
            addingCar?.name = cars[0].name
        }
    }
    
    func appendAddingCarInfo(name: String?, platePrefix: String?, plateSuffix: String?, vinLast5: String?) {
        if isAddingFirstCar() {
            addingCar?.name = name
        }
        addingCar?.platePrefix = platePrefix
        addingCar?.plateSuffix = plateSuffix
        addingCar?.vinLast5 = vinLast5
    }
    
    private func appendAddingCarType(typeId: Int?, typeName: String?) {
        addingCar?.typeId = typeId
        addingCar?.typeName = typeName
    }
    
    private func appendAddingCarModels(models: [Model]) {
        addingCar?.models = models
    }
    
    func appendAddingCarModel(index: Int) {
        addingCar?.selectedModelIndex = index
    }
    
    func getExtUrl(listener: CarProtocol?, type: String, plateNo: String?, vin: String?, param: String?) {
        self.openhubUrl = ""
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        let parameters: Parameters = [
            "type": type,
            "token": UserManager.shared().getUserToken()!,
            "vin": vin ?? "",
            "plate_no": plateNo ?? "",
            "account_id": UserManager.shared().currentUser.accountId!
        ]
        
        listener?.didStartGetExtUrl()
        Alamofire.request("\(Config.urlCruisys)service_go/ext_redirect_url", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        //UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    let url = responseJson["data"]["redirect_url"].stringValue
                    self.openhubUrl = url
                    print("getExtUrl, type: \(type), url: \(url)")
                    DeeplinkNavigator.shared.proceedToDeeplink(.OpenHub, param: param)
                    //listener?.didFinishGetExtUrl(success: true, type: type, url: url)
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    self.openhubUrl = ""
                    //listener?.didFinishGetExtUrl(success: false, type: type, url: nil)
                })
                
        }
    }
    
    func getCarMaintanceInfo(listener: CarProtocol?, plateNo: String) {
        //{base_url}/service_go/cars/{plate_no}/service_reminder/{user_token}
        let stringWithoutDash = plateNo.replacingOccurrences(of: "-", with: "")
        
        guard let token = UserManager.shared().getCruisysUserToken() else { return }
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json"
        ]
        
        listener?.didStartGetCarMaintanceInfo()
        Alamofire.request("\(Config.urlCruisys)service_go/cars/\(stringWithoutDash)/service_reminder/\(UserManager.shared().getUserToken()!)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        //UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    let maint = responseJson["data"].arrayValue.map { (jsonn) -> MaintainanceInfo in
                        MaintainanceInfo(with: jsonn)
                    }
                    listener?.didFinishGetCarMaintanceInfo(success: true, info: maint)
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetCarMaintanceInfo(success: false, info: nil)
                })
                
        }
    }
    
    func getDongleBindingInfo(listener: CarProtocol?, plateNo: String) {
        self.printCurrentTime(str: "getDongleBindingInfo: \(plateNo)")
        guard let token = UserManager.shared().getCruisysUserToken() else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.updateRootVC(showAlert: false)
            return
        }
        
        let stringWithoutDash = plateNo.replacingOccurrences(of: "-", with: "")
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json"
        ]
        
        listener?.didStartGetDongleBinding()
        Alamofire.request("\(Config.urlCruisys)service_go/cars/\(stringWithoutDash)/dongle_binding", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        //UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let data = responseJson["data"].arrayValue
                    if data.count > 0 {
                        let data1 = data[0]
                        let dongledata = DongleData(with: data1)
                        let str = data1["state"].stringValue
                        print("getDongleBindingInfo \(plateNo) str: \(str)")
                        if str == "USER_MAPPED" {
                            listener?.didFinishGetDongleBinding(state: .USER_MAPPED, dongleData: dongledata)
                        } else if str == "VIN_MAPPED" {
                            listener?.didFinishGetDongleBinding(state: .VIN_MAPPED, dongleData: dongledata)
                        } else if str == "502 Device not Found" {
                            listener?.didFinishGetDongleBinding(state: .DEVICE_NOT_FOUND, dongleData: dongledata)
                        } else {
                            listener?.didFinishGetDongleBinding(state: nil, dongleData: nil)
                        }
                    }
                })
                
                response.result.ifFailure({
                    print("getDongleBindingInfo fail")
                    if let error = response.error {
                        print("getDongleBindingInfo fail: \(error.localizedDescription)")
                    }
                    listener?.didFinishGetDongleBinding(state: nil, dongleData: nil)
                })
        }
    }
    
    func postDongleBinding(listener: CarProtocol?, dongleData: DongleData, plateNo: String) {
        guard let token = UserManager.shared().getCruisysUserToken() else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.updateRootVC(showAlert: false)
            return
        }
        
        let stringWithoutDash = plateNo.replacingOccurrences(of: "-", with: "")
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json"
        ]
        
        let parameters: Parameters = [
            "first_name": dongleData.firstName ?? "",
            "last_name": dongleData.lastName ?? "",
            "phone": dongleData.phone ?? "",
            "email": dongleData.email ?? "",
            "imei": dongleData.imei ?? "",
            "brand": "Volkswagen Personal",
            "model": dongleData.model ?? "",
            "power_type": "fuel"
        ]
        
        listener?.didStartPostDongleBinding()
        Alamofire.request("\(Config.urlCruisys)service_go/cars/\(stringWithoutDash)/dongle_binding", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        //UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let data = responseJson["data"].arrayValue
                    if data.count > 0 {
                        let data1 = data[0]
                        let dongledata = DongleData(with: data1)
                        //self.assistantRedirectUrl = dongledata.redirectUrl
                        let str = data1["state"].stringValue
                        if str == "USER_MAPPED" {
                            listener?.didFinishPostDongleBinding(state: .USER_MAPPED, dongleData: dongleData, msg: nil)
                        } else if str == "VIN_MAPPED" {
                            listener?.didFinishPostDongleBinding(state: .VIN_MAPPED, dongleData: dongleData, msg: nil)
                        } else if str == "502 Device not Found" {
                            listener?.didFinishPostDongleBinding(state: .DEVICE_NOT_FOUND, dongleData: dongleData, msg: nil)
                        } else if str == "ACTIVATED" {
                            listener?.didFinishPostDongleBinding(state: .ACTIVATED, dongleData: dongleData, msg: nil)
                        } else if str == "DISCARDED" {
                            listener?.didFinishPostDongleBinding(state: .DISCARDED, dongleData: dongleData, msg: nil)
                        } else if str == "EXPIRED" {
                            listener?.didFinishPostDongleBinding(state: .EXPIRED, dongleData: dongleData, msg: nil)
                        } else {
                            listener?.didFinishPostDongleBinding(state: nil, dongleData: nil, msg: nil)
                        }
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: CarManager.nameCarReloadNotification), object: nil, userInfo: nil)
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    if let responseJson = JSON.init(rawValue: response.result.value as Any) {
                        listener?.didFinishPostDongleBinding(state: nil, dongleData: nil, msg: responseJson["return"].stringValue)
                    } else {
                        listener?.didFinishPostDongleBinding(state: nil, dongleData: nil, msg: "綁定失敗")
                    }
                })
                
        }
    }
    
    func removeDongleBinding(listener: CarProtocol?, plateNo: String, car: Car?) {
        guard let token = UserManager.shared().getCruisysUserToken() else {
            return
        }
        
        let stringWithoutDash = plateNo.replacingOccurrences(of: "-", with: "")
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json"
        ]
        
        listener?.didStartDeleteDongleBinding()
        Alamofire.request("\(Config.urlCruisys)service_go/cars/\(stringWithoutDash)/dongle_binding", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        //UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    listener?.didFinishDeleteDongleBinding(success: true, car: nil)
                    self.assistantRedirectUrl = nil
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: CarManager.nameCarReloadNotification), object: nil, userInfo: nil)
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishDeleteDongleBinding(success: false, car: nil)
                })
                
        }
    }
    
    func removeDongleBindingInGarage(listener: CarProtocol?, plateNo: String, car: Car) {
        guard let token = UserManager.shared().getCruisysUserToken() else {
            return
        }
        
        let stringWithoutDash = plateNo.replacingOccurrences(of: "-", with: "")
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json"
        ]
        
        listener?.didStartDeleteDongleBinding()
        Alamofire.request("\(Config.urlCruisys)service_go/cars/\(stringWithoutDash)/dongle_binding", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        //UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    listener?.didFinishDeleteDongleBinding(success: true, car: car)
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishDeleteDongleBinding(success: false, car: car)
                })
                
        }
    }
    
    func getDongleSummary(listener: CarProtocol?, plateNo: String) {
        let stringWithoutDash = plateNo.replacingOccurrences(of: "-", with: "")
        
        guard let token = UserManager.shared().getCruisysUserToken() else { return }
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json"
        ]
        
        listener?.didStartGetDongleSummary()
        Alamofire.request("\(Config.urlCruisys)service_go/cars/\(stringWithoutDash)/summary", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        //UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let data = responseJson["data"].arrayValue
                    if data.count > 0 {
                        let data1 = data[0]
                        let summary = DongleSummary(with: data1)
                        listener?.didFinishGetDongleSummary(data: summary)
                    } else {
                        listener?.didFinishGetDongleSummary(data: nil)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetDongleSummary(data: nil)
                })
                
        }
    }
    
    func getDongleNotification(listener: CarProtocol?, plateNo: String) {
        let stringWithoutDash = plateNo.replacingOccurrences(of: "-", with: "")
        
        guard let token = UserManager.shared().getCruisysUserToken() else { return }
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json"
        ]
        
        listener?.didStartGetDongleNotification()
        Alamofire.request("\(Config.urlCruisys)service_go/cars/\(stringWithoutDash)/dongle_notification", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        //UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let data = responseJson["data"]
                    let notificationList = data["notification_list"].arrayValue.map { (jsonn) -> Notificationn in
                        Notificationn(with: jsonn)
                    }
                    let notificationDialogList = data["notification_dialog_list"].arrayValue.map { (jsonn) -> Notificationn in
                        Notificationn(with: jsonn)
                    }
                    // test fake data
//                    let nn00 = Notificationn()
//                    nn00.eventTime = "2023/11/7 21:12"
//                    nn00.descriptionn = "您的愛車目前油量低於15%啪啪"
//                    nn00.btnUrl = "myvolkswagendev://notification/carowner"
//                    let nn0 = Notificationn()
//                    nn0.eventTime = "2023/11/7 20:12"
//                    nn0.descriptionn = "您的愛車目前油量低於15%啪啪"
//                    nn0.btnUrl = "myvolkswagendev://notification/carowner"
//                    let nn1 = Notificationn()
//                    nn1.eventTime = "2023/11/7 10:12"
//                    nn1.descriptionn = "您的愛車目前油量低於15%"
//                    nn1.btnUrl = "myvolkswagendev://notification/carowner"
//                    let nn2 = Notificationn()
//                    nn2.eventTime = "2023/11/7 18:12"
//                    nn2.descriptionn = "您的愛車目前被拖吊中"
//                    nn2.btnUrl = "myvolkswagendev://notification/carowner"
//                    let nn3 = Notificationn()
//                    nn3.eventTime = "2023/11/6 18:12"
//                    nn3.descriptionn = "您的愛車"
//                    nn3.btnUrl = "myvolkswagendev://car/mygarage"
//                    notificationDialogList.append(nn00)
//                    notificationDialogList.append(nn0)
//                    notificationDialogList.append(nn1)
//                    notificationDialogList.append(nn2)
//                    notificationDialogList.append(nn3)
//                    listener?.didFinishGetDongleNotification(success: true, notificationList: notificationDialogList, notificationDialogList: notificationDialogList)
                    // ----------------
                    listener?.didFinishGetDongleNotification(success: true, notificationList: notificationList, notificationDialogList: notificationDialogList)
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetDongleNotification(success: false, notificationList: nil, notificationDialogList: nil)
                })
                
        }
    }
    
    func getCarWarranty(listener: CarProtocol?, plateNo: String) {
        let stringWithoutDash = plateNo.replacingOccurrences(of: "-", with: "")
        
        guard let token = UserManager.shared().getCruisysUserToken() else { return }
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json"
        ]
        
        listener?.didStartGetCarWarranty()
        Alamofire.request("\(Config.urlCruisys)service_go/cars/\(stringWithoutDash)/warranty", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        //UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    let data = responseJson["data"]
                    if data.count > 0 {
                        let date = data["home_expiration_date"].string
                        listener?.didFinishGetCarWarranty(success: true, date: date)
                    } else {
                        listener?.didFinishGetCarWarranty(success: false, date: nil)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetCarWarranty(success: false, date: nil)
                })
                
        }
    }
    
    func getCarTrip(listener: CarProtocol?, plateNo: String, startDate: String?, endDate: String?, page: String, perPage: String) {
        let stringWithoutDash = plateNo.replacingOccurrences(of: "-", with: "")
        
        guard let token = UserManager.shared().getCruisysUserToken() else { return }
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json"
        ]
        
        var myurl = "\(Config.urlCruisys)service_go/cars/\(stringWithoutDash)/trips?p=\(page)&pp=\(perPage)"
        if startDate != nil && endDate != nil {
            myurl = "\(Config.urlCruisys)service_go/cars/\(stringWithoutDash)/trips?from_date=\(startDate!)&to_date=\(endDate!)"
        }
        listener?.didStartGetCarTrips()
        Alamofire.request(myurl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                    Logger.d("Request: \(String(describing: response.request))")   // original url request
//                    Logger.d("Response: \(String(describing: response.response))") // http url response
//                    Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        //UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    var carTrips: [CarTrip] = []
                    let data = responseJson["data"].arrayValue
                    for trip in data {
                        carTrips.append(CarTrip(with: trip))
                    }
                    if carTrips.count > 0 {
                        listener?.didFinishGetCarTrips(success: true, trips: carTrips)
                    } else {
                        listener?.didFinishGetCarTrips(success: false, trips: nil)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetCarTrips(success: false, trips: nil)
                })
                
        }
    }
}
