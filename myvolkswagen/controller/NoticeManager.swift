//
//  NoticeManager.swift
//  myvolkswagen
//
//  Created by Apple on 3/3/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol NoticeProtocol: class {
    func didStartGetNotice(historical: Bool)
    func didFinishGetNotice(success: Bool, historical: Bool, notices: [Notice]?)
    
    func didStartSetRead()
    func didFinishSetRead(success: Bool, notice: Notice)
    
    func didStartGetUnreadCount()
    func didFinishGetUnreadCount(success: Bool, unreadCount: Int?)
    
    func didStartGetInboxContent()
    func didFinishGetInboxContent(success: Bool, inboxContent: News?)
    
    func didStartDeleteNotice()
    func didFinishDeleteNotice(success: Bool)
}

class NoticeManager: NSObject {

    // MARK: - Properties
    private var notices: [Notice]
    private var histories: [Notice]
    var uiUpdateNeeded: Bool = false
    
    static let nameUnreadCountNotification: String = "nameUnreadCountNotification"
    var unreadCount: Int = 0
    private var timerUnreadCount: Timer?
    
    private static var sharedNoticeManager: NoticeManager = {
        let noticeManager = NoticeManager()
        
        // Configuration
        // ...
        
        return noticeManager
    }()
    
    
    // Initialization
    override private init() {
        notices = []
        histories = []
        
        super.init()
    }
    
    // MARK: - Accessors
    
    class func shared() -> NoticeManager {
        return sharedNoticeManager
    }
    
    
    
    func getNotice(kind: Int, historical: Bool, fromRemote: Bool, listener: NoticeProtocol?) -> [Notice] {
        let oNotices: [Notice] = historical ? self.histories : self.notices
        
        var fromRemoteInternal: Bool = fromRemote
        if !fromRemoteInternal {
            if oNotices.count == 0 {
                fromRemoteInternal = true
            }
        }
        
        if fromRemoteInternal {
            let parameters = [
                "Kind": "\(kind)"
            ]
            
            let method: String = historical ? "APP_CU_Get_Notice_History" : "APP_CU_Get_Notice_List"
            listener?.didStartGetNotice(historical: historical)
            Alamofire.request(Config.urlCommunityServer + "\(method)?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                    
                    if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                        let arr: [String] = authorization.components(separatedBy: " ")
                        
                        // "Bearer xxxxx"
                        if arr.count == 2 {
                            UserManager.shared().saveUserToken(with: arr[1])
                        }
                    }
                    
                    response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                        let responseJson = JSON.init(rawValue: response.result.value as Any)!
                        
                        let result: Result = Result(with: responseJson)
                        Logger.d("result: \(result)")
                        if result.isSuccess() {
                            var array: [Notice] = []
                            if let data = responseJson["NoticeList"].array {
                                for datum in data {
                                    let notice: Notice = Notice(with: datum)
                                    Logger.d("load notice from server response: \(notice)")
                                    array.append(notice)
                                }
                            }
                            
                            Logger.d("now we have \(array.count) notices")
                            
                            if historical {
                                self.histories = array
                            } else {
                                self.notices = array
                            }
                            
                            listener?.didFinishGetNotice(success: true, historical: historical, notices: array)
                        } else {
                            Logger.d("errorCode: \(String(describing: result.errorCode))")
                            Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                            listener?.didFinishGetNotice(success: false, historical: historical, notices: nil)
                            Utility.checkTokenValidation(of: result)
                        }
                    })
                    
                    response.result.ifFailure({
                        Logger.d("error: \(response.error as Any)")
                        listener?.didFinishGetNotice(success: false, historical: historical, notices: nil)
                    })
                    
            }
        }
        
        return oNotices
    }
    
    func setRead(notice: Notice, listener: NoticeProtocol?) {
        let parameters = [
            "NoticeID": "\(notice.noticeID)"
        ]
        
        listener?.didStartSetRead()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Notice_Read?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        // update notice locally
                        for n in self.notices {
                            if n.noticeID == notice.noticeID {
                                n.read = ReadStatus.read.rawValue
                            }
                        }
                        for n in self.histories {
                            if n.noticeID == notice.noticeID {
                                n.read = ReadStatus.read.rawValue
                            }
                        }
                        self.uiUpdateNeeded = true
                        listener?.didFinishSetRead(success: true, notice: notice)
                        
                        self.getNoticeEventCount()
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishSetRead(success: false, notice: notice)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishSetRead(success: false, notice: notice)
                })
                
        }
    }
    
    func deleteNotice(notiId: Int, listener: NoticeProtocol?) {
        let token = UserManager.shared().getUserToken()!
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json"
        ]
        
        listener?.didStartDeleteNotice()
        Alamofire.request(Config.urlMemberServer + "app/notice/\(notiId)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                
//                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
//                    let arr: [String] = authorization.components(separatedBy: " ")
//
//                    // "Bearer xxxxx"
//                    if arr.count == 2 {
//                        UserManager.shared().saveUserToken(with: arr[1])
//                    }
//                }
                
                response.result.ifSuccess({
                    //                        Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishDeleteNotice(success: true)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishDeleteNotice(success: false)
                        //Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishDeleteNotice(success: false)
                })
            }
    }
    
    func getNoticeEventCount() {
        let parameters = [
            "AccountList[0]": "\(UserManager.shared().currentUser.accountId!)"
        ]
        
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Get_Notice_Event_Count?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        let badgeNumber = responseJson["AccountList"]["\(UserManager.shared().currentUser.accountId!)"].intValue
//                        Logger.d("badgeNumber: \(badgeNumber)")
                        UIApplication.shared.applicationIconBadgeNumber = badgeNumber
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                })
                
        }
    }
    
    func getNoticeRingClose() {
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Get_Notice_Ring_Close?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("getNoticeRingClose result: \(result)")
                    if result.isSuccess() {
                        
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                })
                
        }
    }
    
    func getInboxContent(inBoxId: Int, listener: NoticeProtocol?) {
            
        let parameters: Parameters = [
            "NewsID": "\(inBoxId)"
        ]
        
//        Logger.d("getNews parameters: \(parameters)")
        
        listener?.didStartGetInboxContent()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Get_Inbox_Content?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: parameters).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        listener?.didFinishGetInboxContent(success: true, inboxContent: News(with: responseJson["News"]))
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetInboxContent(success: false, inboxContent: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetInboxContent(success: false, inboxContent: nil)
                })
        }
    }
    
    // return unread status if we check locally
    @discardableResult func getUnreadCount(fromLocal: Bool, listener: NoticeProtocol?) -> ReadStatus? {
        if fromLocal {
            for notice in notices {
                if !notice.isRead() {
                    return .unread
                }
            }
            
            for notice in histories {
                if !notice.isRead() {
                    return .unread
                }
            }
            
            if self.unreadCount == 0 {
                return .read
            } else {
                return .unread
            }
        }
        
        listener?.didStartGetUnreadCount()
        Alamofire.request(Config.urlCommunityServer + "APP_CU_Get_Notice_Count?token=\(UserManager.shared().getUserToken()!)", method: .post, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
//                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        let c: Int? = responseJson["Count"].int
                        
//                        Logger.d("unread notice count: \(String(describing: c))")
                        if let c = c {
                            self.unreadCount = c
                        }
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NoticeManager.nameUnreadCountNotification), object: nil, userInfo: nil)
                        listener?.didFinishGetUnreadCount(success: true, unreadCount: c)
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        listener?.didFinishGetUnreadCount(success: false, unreadCount: nil)
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetUnreadCount(success: false, unreadCount: nil)
                })
                
        }
        
        return nil
    }
    
    func startTimerUnreadCount() {
        if UserManager.shared().isSignedIn() {
            if let timer = self.timerUnreadCount {
                if timer.isValid {
//                    Logger.d("unread count timer is valid, skip")
                    return
                }
            }
            
//            Logger.d("startTimerUnreadCount for real")
            getUnreadCount(fromLocal: false, listener: nil)
            timerUnreadCount = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
        }
    }
    
    func stopTimerUnreadCount() {
        if let timer = self.timerUnreadCount {
//            Logger.d("stopTimerUnreadCount for real")
            timer.invalidate()
        }
    }
    
    @objc private func fireTimer() {
        getUnreadCount(fromLocal: false, listener: nil)
    }
}
