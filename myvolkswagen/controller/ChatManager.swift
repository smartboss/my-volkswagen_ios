//
//  ChatManager.swift
//  myvolkswagen
//
//  Created by Apple on 8/29/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol ChatProtocol: class {
    func didStartGetGreeting()
    func didFinishGetGreeting(success: Bool, greeting: String?)
    
    func didStartGetMenuButtons()
    func didFinishGetMenuButtons(success: Bool, content: String?, buttons: [ChatButton]?)
    
    func didStartGetButtons()
    func didFinishGetButtons(success: Bool, buttons: [ChatButton]?)
    
    func didStartGetGrading()
    func didFinishGetGrading(success: Bool, chatGrade: ChatGrade?)
    
    func didStartGenerateAnswer()
    func didFinishGenerateAnswer(success: Bool, answers: [ChatAnswer]?, buttons: [ChatButton]?, exception: String?, question: String?)
    
    func didStartWriteTalkRecords()
    func didFinishWriteTalkRecords(success: Bool)
    
    func didStartGetTalkHistroy()
    func didFinishGetTalkHistroy(success: Bool, history: [ChatHistory]?, exception: String?)
}

class ChatManager: NSObject {
    
    // MARK: - Properties
    private var kbId: String!
    private var auth: String!
    
    private static var sharedChatManager: ChatManager = {
        let chatManager = ChatManager()
        
        // Configuration
        // ...
        
        return chatManager
    }()
    
    
    // Initialization
    override private init() {
        kbId = "b0c46d01-637e-4348-a923-8f98c4ce8e97" // chatbot doc v1.2.5
//        kbId = "b6c4518b-bad9-4891-a03a-bdf238325e0e" // chatbot doc v1.2.4
        auth = "pnxYQjT+6bxrpUC/9q0ef9AtKAbuuSh4tFi3hTSaO+U="
        
        super.init()
    }
    
    // MARK: - Accessors
    
    class func shared() -> ChatManager {
        return sharedChatManager
    }

    
    
    
    func getGreeting(listener: ChatProtocol?) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        let parameters: Parameters = [
            "kbId": kbId
        ]
        
        listener?.didStartGetGreeting()
        Alamofire.request(Config.urlChat + "accuhit/knowledgebases/getOpeningRemarks", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300).validate(contentType: ["application/json"]).responseJSON { response in
//                Logger.d("Request: \(String(describing: response.request))")   // original url request
//                Logger.d("Response: \(String(describing: response.response))") // http url response
//                Logger.d("Result: \(response.result)")                         // response serialization result
            
                response.result.ifSuccess({
//                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    if let status: Int = responseJson["status"].int, status == 200 {
                        listener?.didFinishGetGreeting(success: true, greeting: responseJson["message"].string)
                    } else {
                        listener?.didFinishGetGreeting(success: false, greeting: nil)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                    listener?.didFinishGetGreeting(success: false, greeting: nil)
                })
        }
    }
    
    func getMenuButtons(listener: ChatProtocol?) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        let parameters: Parameters = [
            "kbId": kbId
        ]
        
        listener?.didStartGetMenuButtons()
        Alamofire.request(Config.urlChat + "accuhit/getMenuButtons", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300).validate(contentType: ["application/json"]).responseJSON { response in
//            Logger.d("Request: \(String(describing: response.request))")   // original url request
//            Logger.d("Response: \(String(describing: response.response))") // http url response
//            Logger.d("Result: \(response.result)")                         // response serialization result
            
            response.result.ifSuccess({
//                Logger.d(response.result.value as Any)
                let responseJson = JSON.init(rawValue: response.result.value as Any)!

                if let status: Int = responseJson["status"].int, status == 200 {
//                    Logger.d("content: \(String(describing: responseJson["content"].string))")
                    
                    var array: [ChatButton] = []
                    if let data = responseJson["buttons"].array {
                        for datum in data {
                            let chatButton: ChatButton = ChatButton(with: datum)
//                            Logger.d("load chatButton from server response: \(chatButton)")
                            array.append(chatButton)
                        }
                    }
                    listener?.didFinishGetMenuButtons(success: true, content: responseJson["content"].string, buttons: array)
                } else {
                    listener?.didFinishGetMenuButtons(success: false, content: nil, buttons: nil)
                }
            })
            
            response.result.ifFailure({
                Logger.d("error: \(response.error as Any)")
                listener?.didFinishGetMenuButtons(success: false, content: nil, buttons: nil)
            })
        }
    }
    
    func getButtons(with question: String, listener: ChatProtocol?) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]

        let parameters: Parameters = [
            "question": question,
            "kbId": kbId
        ]

        listener?.didStartGetButtons()
        Alamofire.request(Config.urlChat + "accuhit/knowledgebases/getButtons", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300).validate(contentType: ["application/json"]).responseJSON { response in
//            Logger.d("Request: \(String(describing: response.request))")   // original url request
//            Logger.d("Response: \(String(describing: response.response))") // http url response
//            Logger.d("Result: \(response.result)")                         // response serialization result

            response.result.ifSuccess({
                Logger.d(response.result.value as Any)
                let responseJson = JSON.init(rawValue: response.result.value as Any)!

                if let status: Int = responseJson["status"].int, status == 200 {
                    var array: [ChatButton] = []
                    if let data = responseJson["buttons"].array {
                        for datum in data {
                            let chatButton: ChatButton = ChatButton(with: datum)
                            Logger.d("load chatButton from server response: \(chatButton)")
                            array.append(chatButton)
                        }
                    }
                    listener?.didFinishGetButtons(success: true, buttons: array)
                } else {
                    listener?.didFinishGetButtons(success: false, buttons: nil)
                }
            })

            response.result.ifFailure({
                Logger.d("error: \(response.error as Any)")
                listener?.didFinishGetButtons(success: false, buttons: nil)
            })
        }
    }
    
    func getGrading(listener: ChatProtocol?) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        let parameters: Parameters = [
            "kbId": kbId
        ]
        
        listener?.didStartGetGrading()
        Alamofire.request(Config.urlChat + "accuhit/getGrading", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300).validate(contentType: ["application/json"]).responseJSON { response in
//            Logger.d("Request: \(String(describing: response.request))")   // original url request
//            Logger.d("Response: \(String(describing: response.response))") // http url response
//            Logger.d("Result: \(response.result)")                         // response serialization result
            
            response.result.ifSuccess({
//                Logger.d(response.result.value as Any)
                let responseJson = JSON.init(rawValue: response.result.value as Any)!
                
                if let status: Int = responseJson["status"].int, status == 200 {
//                    Logger.d("title: \(String(describing: responseJson["title"].string))")
//                    Logger.d("content: \(String(describing: responseJson["content"].string))")
//
//                    var array: [ChatButton] = []
//                    if let data = responseJson["buttons"].array {
//                        for datum in data {
//                            let chatButton: ChatButton = ChatButton(with: datum)
//                            Logger.d("load chatButton from server response: \(chatButton)")
//                            array.append(chatButton)
//                        }
//                    }
                    
                    let chatGrade: ChatGrade = ChatGrade(with: responseJson)
                    listener?.didFinishGetGrading(success: true, chatGrade: chatGrade)
                } else {
                    listener?.didFinishGetGrading(success: false, chatGrade: nil)
                }
            })
            
            response.result.ifFailure({
                Logger.d("error: \(response.error as Any)")
                listener?.didFinishGetGrading(success: false, chatGrade: nil)
            })
        }
    }
    
    func generateAnswer(with question: String, listener: ChatProtocol?) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": auth
        ]
        
        let parameters: Parameters = [
            "uid": UserManager.shared().currentUser.accountId!,
            "question": question,
            "top": "1",
            "kbId": kbId
        ]
        
        listener?.didStartGenerateAnswer()
        Alamofire.request(Config.urlChat + "qnamaker/generateAnswer", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300).validate(contentType: ["application/json"]).responseJSON { response in
//            Logger.d("Request: \(String(describing: response.request))")   // original url request
//            Logger.d("Response: \(String(describing: response.response))") // http url response
//            Logger.d("Result: \(response.result)")                         // response serialization result
            
            response.result.ifSuccess({
//                Logger.d(response.result.value as Any)
                let responseJson = JSON.init(rawValue: response.result.value as Any)!
                
                if let status: Int = responseJson["status"].int, status == 200 {
//                    Logger.d("exception: \(String(describing: responseJson["exception"].string))")
//                    Logger.d("question: \(String(describing: responseJson["question"].string))")

                    var arrayAnswers: [ChatAnswer] = []
                    if let data = responseJson["answers"].array {
                        for datum in data {
                            let chatAnswer: ChatAnswer = ChatAnswer(with: datum)
//                            Logger.d("load chatAnswer from server response: \(chatAnswer)")
                            arrayAnswers.append(chatAnswer)
                        }
                    }
                    
                    var arrayButtons: [ChatButton] = []
                    if let data = responseJson["buttons"].array {
                        for datum in data {
                            let chatButton: ChatButton = ChatButton(with: datum)
//                            Logger.d("load chatButton from server response: \(chatButton)")
                            arrayButtons.append(chatButton)
                        }
                    }
                    
                    listener?.didFinishGenerateAnswer(success: true, answers: arrayAnswers, buttons: arrayButtons, exception: responseJson["exception"].string, question: responseJson["question"].string)
                } else {
                    listener?.didFinishGenerateAnswer(success: false, answers: nil, buttons: nil, exception: nil, question: nil)
                }
            })
            
            response.result.ifFailure({
                Logger.d("error: \(response.error as Any)")
                listener?.didFinishGenerateAnswer(success: false, answers: nil, buttons: nil, exception: nil, question: nil)
            })
            
        }
        
    }
    
    func writeTalkRecord(with record: ChatRecord, listener: ChatProtocol?) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        let records: Parameters = [
            "records": [record.toDictionary()]
        ]
        
        let parameters: Parameters = [
            "kbId": kbId,
            "uid": UserManager.shared().currentUser.accountId!,
            "talkRecords": records
        ]
        
        Logger.d("parameters: \(parameters)")
        
        listener?.didStartWriteTalkRecords()
        Alamofire.request(Config.urlChat + "accuhit/knowledgebases/writeTalkRecords", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300).validate(contentType: ["application/json"]).responseJSON { response in
//            Logger.d("Request: \(String(describing: response.request))")   // original url request
//            Logger.d("Response: \(String(describing: response.response))") // http url response
//            Logger.d("Result: \(response.result)")                         // response serialization result
            
            response.result.ifSuccess({
//                Logger.d(response.result.value as Any)
                let responseJson = JSON.init(rawValue: response.result.value as Any)!
                
                if let status: Int = responseJson["status"].int, status == 200 {
                    listener?.didFinishWriteTalkRecords(success: true)
                } else {
                    listener?.didFinishWriteTalkRecords(success: false)
                }
            })
            
            response.result.ifFailure({
                Logger.d("error: \(response.error as Any)")
                listener?.didFinishWriteTalkRecords(success: false)
            })
            
        }
        
    }
    
    func getTalkHistory(start: Int, offset: Int, listener: ChatProtocol?) {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        let parameters: Parameters = [
            "kbId": kbId,
            "uid": UserManager.shared().currentUser.accountId!,
            "start": start,
            "offset": offset
        ]
        
//        Logger.d("parameters: \(parameters)")
        
        listener?.didStartGetTalkHistroy()
        Alamofire.request(Config.urlChat + "accuhit/getTalkHistory", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).validate(statusCode: 200..<300).validate(contentType: ["application/json"]).responseJSON { response in
//            Logger.d("Request: \(String(describing: response.request))")   // original url request
//            Logger.d("Response: \(String(describing: response.response))") // http url response
//            Logger.d("Result: \(response.result)")                         // response serialization result
            
            response.result.ifSuccess({
//                Logger.d(response.result.value as Any)
                let responseJson = JSON.init(rawValue: response.result.value as Any)!
                
                if let status: Int = responseJson["status"].int, status == 200 {
                    let hist = responseJson["records"].arrayValue.map { (jsonn) -> ChatHistory in
                        ChatHistory(with: jsonn)
                    }
                    listener?.didFinishGetTalkHistroy(success: true, history: hist, exception: nil)
                } else {
                    listener?.didFinishGetTalkHistroy(success: false, history: nil, exception: nil)
                }
            })
            
            response.result.ifFailure({
                Logger.d("error: \(response.error as Any)")
                listener?.didFinishGetTalkHistroy(success: false, history: nil, exception: nil)
            })
            
        }
        
    }
}
