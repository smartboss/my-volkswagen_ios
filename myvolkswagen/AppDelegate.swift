//
//  AppDelegate.swift
//  myvolkswagen
//
//  Created by Apple on 1/24/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import UserNotifications
import Firebase
import FirebaseMessaging
import FirebaseDynamicLinks
import AppTrackingTransparency

var didEnterBackground = true
extension Notification.Name {
    static let canStartDeeplink = Notification.Name("canStartDeeplink")
    static let appDidBecomeActive = Notification.Name("AppDidBecomeActive")
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var secondWindow: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // init firebase
        var filePath:String!
#if DEVELOPMENT
        filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist", inDirectory: "dev")
#else
        filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist", inDirectory: "pro")
#endif
        
        let options = FirebaseOptions.init(contentsOfFile: filePath)!
        FirebaseApp.configure(options: options)

        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        
        updateRootVC(showAlert: false)
        
        if UserManager.shared().isSignedIn() {
            UserManager.shared().autoLogin()
        }
        
        registerForPushNotifications()
        
        Messaging.messaging().delegate = self
        
        return true
    }
    
//    @objc func handler(gesture: UIPanGestureRecognizer){
//        print(gesture.location(in: self.window?.rootViewController?.view))
//        let location = gesture.location(in: self.window?.rootViewController?.view)
//        let draggedView = gesture.view
//        draggedView?.center = location
//
//        if gesture.state == .ended {
//            if self.secondWindow!.frame.midX >= self.window!.rootViewController!.view.layer.frame.width / 2 {
//                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
//                    self.secondWindow!.center.x = self.window!.rootViewController!.view.layer.frame.width - 40
//                }, completion: nil)
//            }else{
//                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
//                    self.secondWindow!.center.x = 40
//                }, completion: nil)
//            }
//        }
//    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        NoticeManager.shared().stopTimerUnreadCount()
        didEnterBackground = true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { (status) in }
        }
        
        NotificationCenter.default.post(name: .appDidBecomeActive, object: nil)
        
        Alamofire.request(Config.urlMemberServer + "APP_Version?Device=ios", method: .get, parameters: nil).validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"]).responseJSON { response in
                //                Logger.d("Request: \(String(describing: response.request))")   // original url request
                //                Logger.d("Response: \(String(describing: response.response))") // http url response
                //                Logger.d("Result: \(response.result)")                         // response serialization result
                
                if let authorization = response.response?.allHeaderFields["Authorization"] as? String {
                    let arr: [String] = authorization.components(separatedBy: " ")
                    
                    // "Bearer xxxxx"
                    if arr.count == 2 {
                        UserManager.shared().saveUserToken(with: arr[1])
                    }
                }
                
                response.result.ifSuccess({
                    //                    Logger.d(response.result.value as Any)
                    let responseJson = JSON.init(rawValue: response.result.value as Any)!
                    
                    let result: Result = Result(with: responseJson)
                    //                    Logger.d("result: \(result)")
                    if result.isSuccess() {
                        let version: Float = responseJson["Version"].floatValue
                        let isUpdate: Bool = responseJson["IsUpdate"].stringValue == "Y"
                        let message: String = responseJson["Message"].stringValue
                        let url: String = responseJson["URL"].stringValue
                        
                        Logger.d("version: \(version)")
                        Logger.d("isUpdate: \(isUpdate)")
                        Logger.d("message: \(message)")
                        Logger.d("url: \(url)")
                        
                        let currentVersion: Float = Utility.versionFloat()
                        Logger.d("currentVersion: \(currentVersion)")
                        
                        if version > currentVersion {
                            if let drawerController = self.window?.rootViewController as? DrawerController {
                                let naviViewController: UINavigationController = drawerController.rootViewController as UINavigationController
                                let viewController: BaseViewController = naviViewController.viewControllers.last as! BaseViewController
                                viewController.showVersionUpdate(message: message, force: isUpdate, url: url)
                            } else if let naviViewController = self.window?.rootViewController as? UINavigationController {
                                let viewController: BaseViewController = naviViewController.viewControllers.last as! BaseViewController
                                viewController.showVersionUpdate(message: message, force: isUpdate, url: url)
                            } else {
                                Logger.e("strange ui state")
                            }
                        }
                    } else {
                        Logger.d("errorCode: \(String(describing: result.errorCode))")
                        Logger.d("errorMsg: \(String(describing: result.errorMsg))")
                        
                        Utility.checkTokenValidation(of: result)
                    }
                })
                
                response.result.ifFailure({
                    Logger.d("error: \(response.error as Any)")
                })
                
            }
        
        UserManager.shared().showNoticeBoard = nil
        
        NoticeManager.shared().startTimerUnreadCount()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            // Handle the deep link. For example, show the deep-linked content or
            // apply a promotional offer to the user's account.
            // ...
            Logger.d("DynamicLink received: \(dynamicLink)")
            return true
        } else if Deeplinker.handleDeeplink(url: url, ref: nil) {
            print("sourceApplication start deeplink(1): \(url.absoluteString)")
            if url.absoluteString.contains("register/line") {
                canStartDplk = true
            }
            Deeplinker.checkDeepLink()
            return true
        } else {
            return true
        }
    }
    
    
    
    
    func updateRootVC(showAlert: Bool) {
        var rootVC : UIViewController?
        
        if UserManager.shared().isSignedIn() {
            if UserManager.shared().currentUser.showProfile ?? false {
                // show profile editor
                rootVC = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "EditProfileNaviV2") as! UINavigationController
            } else {
                rootVC = UIStoryboard(name: "Car", bundle: nil).instantiateViewController(withIdentifier: "CarMainNavi") as! UINavigationController
            }
        } else {
            // login flow
            rootVC = UIStoryboard(name: "Car", bundle: nil).instantiateViewController(withIdentifier: "mainVideoNavi") as! UINavigationController
        }
        
        self.window?.rootViewController = rootVC
        if showAlert {
            let alertController = UIAlertController(title: nil, message: "success_delete_account".localized, preferredStyle: .alert)
            
            let action2 = UIAlertAction(title: "i_see".localized, style: .cancel) { (action:UIAlertAction) in
            }
            
            alertController.addAction(action2)
            rootVC!.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    var orientationLock = UIInterfaceOrientationMask.portrait
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamicLink, error) in
            // ...
            Logger.d("DynamicLink received: \(dynamicLink.debugDescription)")
        }
        
        return handled
    }
    
    // MARK: remote notification
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            [weak self] granted, error in
            
            Logger.d("Permission granted: \(granted)")
            guard granted else { return }
            self?.getNotificationSettings()
        }
        
#if DEVELOPMENT
        Messaging.messaging().subscribe(toTopic: "dev") { error in
          print("Subscribed to dev topic")
        }
#endif
        
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            Logger.d("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        //Logger.d("Device Token: \(token)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        Logger.d("Failed to register: \(error)")
    }
    
    
    
    // MARK: firebase
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        Logger.d("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        if UserManager.shared().isSignedIn() {
            UserManager.shared().updatePushToken(token: fcmToken ?? "", listener: nil)
            UserManager.shared().savePushToken(with: fcmToken ?? "")
        } else {
            Logger.d("not signed in, do not update fcm token")
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            Logger.d("Message ID: \(messageID)")
        //        }
        
        // Print full message.
        Logger.d("userInfo1: \(userInfo)")
        handleFcmNotification(userInfo: userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            Logger.d("Message ID: \(messageID)")
        //        }
        
        // Print full message.
        Logger.d("userInfo2: \(userInfo)")
        handleFcmNotification(userInfo: userInfo)
        
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    private func handleFcmNotification(userInfo: [AnyHashable: Any]) {
        if let kindId = userInfo["kind"] as? String {
            if kindId == NoticeKind.recall.rawValue {
                // 回首頁重新 call 召回 campaign
                UserManager.shared().showNoticeBoard = nil
                updateRootVC(showAlert: false)
                return
            }
        }
        if let newsId = userInfo["news_id"] {
            Logger.d("newsId: \(newsId)")
            
            if UserManager.shared().isSignedIn() {
                Logger.d("window?.rootViewController: \(String(describing: window?.rootViewController))")
                if let drawerController = window?.rootViewController as? DrawerController {
                    Logger.d("root view is a drawer controller")
                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedDetailViewController") as! FeedDetailViewController
                    if let i = newsId as? Int {
                        vc.news = News.init(with: i)
                    } else if let str = newsId as? String {
                        if let i = Int(str) {
                            vc.news = News.init(with: i)
                        }
                    }
                    
                    drawerController.rootViewController.pushViewController(vc, animated: true)
                } else {
                    Logger.d("root view is NOT a drawer controller")
                }
                //                let homeTabBarController: HomeTabBarController = window?.rootViewController as! HomeTabBarController
                //                if notificationIndex == NotificationIndex.follow.rawValue {
                //                    homeTabBarController.selectedIndex = 2
                //                } else if notificationIndex == NotificationIndex.activity_created.rawValue || notificationIndex == NotificationIndex.activity_canceled.rawValue || notificationIndex == NotificationIndex.activity_decided.rawValue {
                //                    homeTabBarController.selectedIndex = 0
                //                    let activityNaviController: BaseNavigationController = homeTabBarController.viewControllers![0] as! BaseNavigationController
                //                    let activityController: ActivityViewController = activityNaviController.viewControllers[0] as! ActivityViewController
                //                    activityController.showDetail(with: userInfo["activity_id"] as! String)
                //                }
            }
        } else if let inboxId = userInfo["inbox_id"] {
            //Logger.d("inbox_id: \(inboxId)")
            
            if UserManager.shared().isSignedIn() {
                if let drawerController = window?.rootViewController as? UINavigationController {
                    let sb = UIStoryboard(name: "Car", bundle: nil)
                    let vc = sb.instantiateViewController(withIdentifier: "NotificationDetailViewController") as! NotificationDetailViewController
                    if let i = inboxId as? Int {
                        vc.inboxId = i
                    }  else if let str = inboxId as? String {
                        vc.inboxId = Int(str)
                    }
                    if let k = userInfo["kind"] as? String {
                        if k == NoticeKind.newCarsSurvey.rawValue {
                            vc.noticeKind = .newCarsSurvey
                        } else if k == NoticeKind.normalSurvey.rawValue {
                            vc.noticeKind = .normalSurvey
                        } else if k == NoticeKind.bodyAndPaint.rawValue {
                            vc.noticeKind = .bodyAndPaint
                        } else if k == NoticeKind.redeem.rawValue {
                            vc.noticeKind = .redeem
                        } else if k == NoticeKind.drivingAssistant.rawValue {
                            DeeplinkNavigator.shared.proceedToDeeplink(.DrivingAssistant, param: nil)
                            return
                        }
                    }
                    drawerController.pushViewController(vc, animated: true)
                }
//                Logger.d("window?.rootViewController: \(String(describing: window?.rootViewController))")
//                if let drawerController = window?.rootViewController as? DrawerController {
//                    Logger.d("root view is a drawer controller")
//                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedDetailViewController") as! FeedDetailViewController
//                    if let i = inboxId as? Int {
//                        vc.inboxId = i
//                        vc.isFull = true
//                        vc.isInboxContent = true
//                    }  else if let str = inboxId as? String {
//                        vc.inboxId = Int(str)
//                        vc.isFull = true
//                        vc.isInboxContent = true
//                    }
//                    if let k = userInfo["kind"] as? String {
//                        if k == NoticeKind.newCarsSurvey.rawValue {
//                            vc.noticeKind = .newCarsSurvey
//                        } else if k == NoticeKind.normalSurvey.rawValue {
//                            vc.noticeKind = .normalSurvey
//                        } else if k == NoticeKind.bodyAndPaint.rawValue {
//                            vc.noticeKind = .bodyAndPaint
//                        }
//                    }
//                    drawerController.rootViewController.pushViewController(vc, animated: true)
//                } else {
//                    Logger.d("root view is NOT a drawer controller")
//                }
            }
        }
    }
}

struct AppUtility {
    
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }
    
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        
        self.lockOrientation(orientation)
        
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }
    
}

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        
        // Print full message.
        Logger.d("userInfo 3: \(userInfo)")
        
        // Change this to your preferred presentation option
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        
        // Print full message.
        Logger.d("tap on on forground app, userInfo: \(userInfo)")
        handleFcmNotification(userInfo: userInfo)
        
        completionHandler()
    }
}
