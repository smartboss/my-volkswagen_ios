//
//  MenuViewController.swift
//  DrawerMenuExample
//
//  Created by Florian Marcu on 1/17/18.
//  Copyright © 2018 iOS App Templates. All rights reserved.
//

import UIKit
import Firebase

class MenuViewController: UIViewController {
    
    weak var rootNavi: DrawerNavigationController?

    @IBOutlet weak var img01: UIImageView!
    @IBOutlet weak var label0: UILabel!
    @IBOutlet weak var label01: UILabel!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        label0.textColor = UIColor.black
        label0.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
        label0.text = "trial_dirve".localized
        let originalImage = UIImage(named: "steeringWheel")
        let tintedImage = originalImage?.withRenderingMode(.alwaysTemplate)
        img01.image = tintedImage
        img01.tintColor = UIColor(red: 0.00, green: 0.69, blue: 0.94, alpha: 1.00)
        
        label01.textColor = UIColor.black
        label01.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
        label01.text = "cpo_car".localized
        
        label1.textColor = UIColor.black
        label1.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
        label1.text = "online_preview".localized
        
        label2.textColor = UIColor.black
        label2.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
        label2.text = "customer_service".localized
        
        label3.textColor = UIColor.black
        label3.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
        label3.text = "sale".localized
        
        label4.textColor = UIColor.black
        label4.font = UIFont(name: "PingFangTC-Medium", size: UiUtility.adaptiveSize(size: 18))
        label4.text = "member_explanation".localized
    }
    
    @IBAction private func item0Clicked(_ sender: Any) {
        if let rootNavi = rootNavi {
            Analytics.logEvent("vw_test_drive", parameters: nil)
            rootNavi.handleMenuButton()
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.index = WebsiteIndex.reservation
            rootNavi.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction private func item01Clicked(_ sender: Any) {
        if let rootNavi = rootNavi {
            Analytics.logEvent("vw_test_drive", parameters: nil)
            rootNavi.handleMenuButton()
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.index = WebsiteIndex.cpoCar
            rootNavi.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction private func item1Clicked(_ sender: Any) {
        if let rootNavi = rootNavi {
            Analytics.logEvent("vw_360", parameters: nil)
            rootNavi.handleMenuButton()
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.index = WebsiteIndex.visualizer360
            rootNavi.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction private func item2Clicked(_ sender: Any) {
        Analytics.logEvent("vw_help_call", parameters: nil)
        UiUtility.callService(controller: self)
    }
    
    @IBAction private func item3Clicked(_ sender: Any) {
        if let rootNavi = rootNavi {
            Analytics.logEvent("vw_center", parameters: nil)
            rootNavi.handleMenuButton()
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.index = WebsiteIndex.reseller
            rootNavi.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction private func item4Clicked(_ sender: Any) {
        if let rootNavi = rootNavi {
            rootNavi.handleMenuButton()
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.index = WebsiteIndex.memberExplanation
            rootNavi.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction private func officialWebsiteClicked(_ sender: Any) {
        if let rootNavi = rootNavi {
            Analytics.logEvent("vw_official_site", parameters: nil)
            rootNavi.handleMenuButton()
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            vc.index = WebsiteIndex.official
            rootNavi.pushViewController(vc, animated: true)
        }
    }
}
