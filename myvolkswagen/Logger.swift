//
//  Logger.swift
//  badminton
//
//  Created by Apple on 9/24/18.
//  Copyright © 2018 sofasogood. All rights reserved.
//

import UIKit

class Logger: NSObject {

    class func d(_ message: Any) {
#if DEVELOPMENT
        print(message)
#endif
    }
    
//    class func d(_ message: Any) {
//        print(message)
//    }
    
    class func e(_ message: Any) {
        print(message)
    }
    
}
