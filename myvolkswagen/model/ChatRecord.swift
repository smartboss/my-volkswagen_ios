//
//  ChatRecord.swift
//  myvolkswagen
//
//  Created by Apple on 9/4/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChatRecord: NSObject {
    
    static let categoryQna: String = "qna"
    static let categoryButton: String = "button"
    static let categoryGrading: String = "grading"

    var category: String
    var id: Int
    var buttonName: String?
    var title: String?
    
    var score: Double?
    var question: String?
    var answer: String?
    var data: ChatButton?
    
    init(with category: String, id: Int, buttonName: String?, title: String?, button: ChatButton) {
        self.category = category
        self.id = id
        
        super.init()
        
        self.buttonName = buttonName
        self.title = title
        self.data = button
    }
    
    init(with question: String?, answer: String?, score: Double?) {
        self.category = ChatRecord.categoryQna
        self.id = -1
        
        super.init()
        
        self.question = question
        self.answer = answer
        self.score = score
    }
    
    func toDictionary() -> [String: Any] {
        var dictionary: [String: Any] = [
            "category": category,
            "id": id
        ]
        
        if let buttonName = buttonName {
            dictionary["buttonName"] = buttonName
        }
        
        if let title = title {
            dictionary["title"] = title
        } else {
            dictionary["title"] = ""
        }
        
        if let question = question {
            dictionary["question"] = question
        }
        
        if let answer = answer {
            dictionary["answer"] = answer
        }
        
        if let score = score {
            dictionary["score"] = "\(score)"
        }
        
        if let data = data {
            dictionary["data"] = data.toDictionary()
        }
        
        return dictionary
    }
}

class ChatRecordData: NSObject {
    var category: String
    var id: Int
    var buttonName: String?
    var title: String?
    
    var score: Double?
    var question: String?
    var answer: String?
    
    init(with category: String, id: Int, buttonName: String?, title: String?) {
        self.category = category
        self.id = id
        
        super.init()
        
        self.buttonName = buttonName
        self.title = title
    }
    
    init(with question: String?, answer: String?, score: Double?) {
        self.category = ChatRecord.categoryQna
        self.id = -1
        
        super.init()
        
        self.question = question
        self.answer = answer
        self.score = score
    }
    
    func DataToDictionary() -> [String: Any] {
        var dictionary: [String: Any] = [
            "category": category,
            "id": id
        ]
        
        if let buttonName = buttonName {
            dictionary["buttonName"] = buttonName
        }
        
        if let title = title {
            dictionary["title"] = title
        } else {
            dictionary["title"] = ""
        }
        
        if let question = question {
            dictionary["question"] = question
        }
        
        if let answer = answer {
            dictionary["answer"] = answer
        }
        
        if let score = score {
            dictionary["score"] = "\(score)"
        }
        
        return dictionary
    }
}

enum ChatHistoryType: String {
    case Button = "button"
    case QnA = "QnA"
}

class ChatHistory: NSObject {
    var category: String?
    var id: Int?
    var buttonName: String?
    var title: String?
    
    var score: Double?
    var question: String?
    var answer: String?
    var data: ChatButton?
    
    init(with json: JSON) {        
        super.init()
        
        self.category = json["context"]["category"].stringValue
        if self.category == "qna" {
            self.id = -1
            self.question = json["context"]["question"].stringValue
            self.answer = json["context"]["answer"].stringValue
            self.score = json["context"]["score"].doubleValue
        } else {
            self.id = json["context"]["id"].intValue
            self.buttonName = json["context"]["buttonName"].stringValue
            self.title = json["context"]["title"].stringValue
            self.data = ChatButton(with: json["context"]["data"])
        }
    }
}

enum ChatHistoryMsgType: String {
    case Question = "question"
    case Answer = "answer"
}

class ChatHistoryMsg: NSObject {
    var text: String = ""
    var type: ChatHistoryMsgType?
    
//    init(with json: JSON) {
//        super.init()
//
//        text = json["text"].string
//        type = ChatHistoryMsgType(rawValue: json["type"].stringValue)
//    }
    
    init(with text: String?, type: ChatHistoryMsgType?) {
        super.init()
        
        self.text = text ?? ""
        self.type = type
    }
}

