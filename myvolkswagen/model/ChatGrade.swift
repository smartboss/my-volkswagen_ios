//
//  ChatGrade.swift
//  myvolkswagen
//
//  Created by Apple on 8/29/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChatGrade: NSObject {

    var title: String?
    var content: String?
    var buttons: [ChatButton]?
    
    
    public override var description: String {
        return "\n{\n title: \(String(describing: self.title)),\n"
            + " content: \(String(describing: self.content)),\n"
            + " buttons: \(String(describing: self.buttons))\n}"
    }
    
    init(with json:JSON) {
        super.init()
        
        title = json["title"].string
        content = json["content"].string
        buttons = []
        if let items = json["buttons"].array {
            for item in items {
//                Logger.d("item: \(item)")
                let chatButton: ChatButton = ChatButton(with: item)
//                Logger.d("load chat button from server response: \(chatButton)")
                buttons?.append(chatButton)
            }
        }
    }
}
