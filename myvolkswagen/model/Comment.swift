//
//  Comment.swift
//  myvolkswagen
//
//  Created by Apple on 2/27/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class Comment: NSObject {
    
    var commentId: Int?
    var content: String?
    var pDate: String?
    var accountId: String?
    var level: String?
    var nickname: String?
    var stickerURL: String?
    var offical: Int = 1
    var isFollower: Int = 0
    var isFollow: Int = 0
    
    
    
    public override var description: String {
        return "\n{\n commentId: \(String(describing: self.commentId)),\n"
            + " content: \(String(describing: self.content)),\n"
            + " pDate: \(String(describing: self.pDate)),\n"
            + " accountId: \(String(describing: self.accountId)),\n"
            + " level: \(String(describing: self.level)),\n"
            + " nickname: \(String(describing: self.nickname)),\n"
            + " stickerURL: \(String(describing: self.stickerURL)),\n"
            + " offical: \(String(describing: self.offical)),\n"
            + " isFollower: \(String(describing: self.isFollower)),\n"
            + " isFollow: \(String(describing: self.isFollow))\n}"
    }
    
    override init() {
        super.init()
    }
    
    init(with json:JSON) {
        super.init()
        
        commentId = json["CommentID"].int
        content = json["Content"].string
        pDate = json["PDate"].string
        accountId = json["AccountID"].string
        level = json["Level"].string
        nickname = json["Nickname"].string
        stickerURL = json["StickerURL"].string
        offical = json["Offical"].intValue
        isFollower = json["IsFollower"].intValue
        isFollow = json["IsFollow"].intValue
    }

    func getDateText() -> String? {
        return UiUtility.formatDateText(from: pDate)
    }
    
    func isMine() -> Bool {
        if let aid = accountId {
            if let myId = UserManager.shared().currentUser.accountId {
                return aid == myId
            } else {
                return false
            }
        } else {
            return false
        }
    }
}
