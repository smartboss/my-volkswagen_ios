//
//  DongleSummary.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/13.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import Foundation
import SwiftyJSON

class DongleSummary: NSObject {
    
    var odometer: DongleSummaryObject?
    var coolantTemperature: DongleSummaryObject?
    var intakeAirTemperature: DongleSummaryObject?
    var controlModuleVoltage: DongleSummaryObject?
    var fuelLevel: DongleSummaryObject?
    var engine: String?
    var speed: DongleSummaryObject?
    var state: DongleSummaryState?

    init(with id: Int) {
        super.init()
    }
    
    init(with json:JSON) {
        super.init()
        
        odometer = DongleSummaryObject(with: json["odometer"])
        coolantTemperature = DongleSummaryObject(with: json["coolant_temperature"])
        intakeAirTemperature = DongleSummaryObject(with: json["intake_air_temperature"])
        controlModuleVoltage = DongleSummaryObject(with: json["control_module_voltage"])
        fuelLevel = DongleSummaryObject(with: json["fuel_level"])
        engine = json["engine"].string
        speed = DongleSummaryObject(with: json["speed"])
        state = DongleSummaryState(with: json["state"])
    }
}

class DongleSummaryObject: NSObject {
    
    var value: Int = 0
    var unit: String?

    init(with id: Int) {
        super.init()
    }
    
    init(with json:JSON) {
        super.init()
        
        value = json["value"].intValue
        unit = json["unit"].string
    }
}

class DongleSummaryState: NSObject {
    
    var code: String?
    var name: String?

    init(with id: Int) {
        super.init()
    }
    
    init(with json:JSON) {
        super.init()
        
        code = json["code"].string
        name = json["name"].string
    }
}

class DongleData: NSObject {
//    "car_id":"string",
//    "vin":"string",
//    "imei":"string",
//    "model":"string",
//    "state":"ACTIVATED",
//    "email":"string",
//    "first_name":"string",
//    "id":"string",
//    "last_name":"string",
//    "phone":"string",
//    "user_mapped_time":"2023/09/15 06:09"
    
    var carId: String?
    var vin: String?
    var imei: String?
    var model: String?
    var state: String?
    var email: String?
    var firstName: String?
    var userId: String?
    var lastName: String?
    var phone: String?
    var userMappedTime: String?
    var redirectUrl: String?

    init(with id: Int) {
        super.init()
    }
    
    init(with json:JSON) {
        super.init()
        
        carId = json["car_id"].string
        vin = json["vin"].string
        imei = json["imei"].string
        model = json["model"].string
        state = json["state"].string
        email = json["email"].string
        firstName = json["first_name"].string
        userId = json["id"].string
        lastName = json["last_name"].string
        phone = json["phone"].string
        userMappedTime = json["user_mapped_time"].string
        redirectUrl = json["redirect_url"].string
    }
}
