//
//  ClubRelation.swift
//  myvolkswagen
//
//  Created by CHUNG CHING JIANG on 2019/10/7.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class ClubRelation: NSObject {

    var accountId: String?
    var official: Int = Role.normal.rawValue
    var level: String?
    var ownModel: [String]?
    var nickname: String?
    var stickerURL: String?
    var isFollower: Int = 0
    var isFollow: Int = 0
    var isJoin: Int = 0
    var isAdmin: Int = 0
    
    
    
    
    public override var description: String {
        return "\n{\n accountId: \(String(describing: self.accountId)),\n"
            + " official: \(String(describing: self.official)),\n"
            + " level: \(String(describing: self.level)),\n"
            + " ownModel: \(String(describing: self.ownModel)),\n"
            + " nickname: \(String(describing: self.nickname)),\n"
            + " stickerURL: \(String(describing: self.stickerURL)),\n"
            + " isFollower: \(String(describing: self.isFollower)),\n"
            + " isFollow: \(String(describing: self.isFollow)),\n"
            + " isJoin: \(String(describing: self.isJoin)),\n"
            + " isAdmin: \(String(describing: self.isAdmin))\n}"
    }
    
//    override init() {
//        super.init()
//    }
    
    init(with json:JSON) {
        accountId = json["AccountID"].string
        official = json["Offical"].intValue
        level = json["Level"].string
        ownModel = []
        if let data = json["OwnModel"].array {
            for datum in data {
                ownModel?.append(datum.stringValue)
            }
        }
        nickname = json["Nickname"].string
        stickerURL = json["StickerURL"].string
        isFollower = json["IsFollower"].intValue
        isFollow = json["IsFollow"].intValue
        
        isJoin = json["IsJoin"].intValue
        isAdmin = json["IsAdmin"].intValue
        
        super.init()
    }
    
    func isMe() -> Bool {
        if let aid = accountId {
            if let myId = UserManager.shared().currentUser.accountId {
                return aid == myId
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    func isFollowedByMe() -> Bool {
        return isFollow == 1
    }
    
    func isFollowingMe() -> Bool {
        return isFollower == 1
    }
    
    func isOfficial() -> Bool {
        return official == Role.official.rawValue
    }
    
    func isJoined() -> Bool {
        return isJoin == 1
    }
    
    func isManager() -> Bool {
        return isAdmin == 1
    }
    
//    override func isEqual(_ object: Any?) -> Bool {
//        if let object = object as? ClubRelation {
//            return accountId! == object.accountId!
//        } else {
//            return false
//        }
//    }
}
