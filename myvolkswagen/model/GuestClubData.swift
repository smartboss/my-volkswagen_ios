//
//  GuestClubData.swift
//  myvolkswagen
//
//  Created by Apple on 10/3/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class GuestClubData: NSObject {

    var hotClubs: [Club] = []
    var newClubs: [Club] = []
    
    
    
    public override var description: String {
        return "\n{\n hotClubs: \(String(describing: self.hotClubs)),\n"
            + " newClubs: \(String(describing: self.newClubs))\n}"
    }
    
    
    
    init(with json:JSON) {
        super.init()
        
        hotClubs = []
        if let data = json["HotList"].array {
            for datum in data {
                let club: Club = Club(with: datum)
//                Logger.d("load hot club from server response: \(club)")
                hotClubs.append(club)
            }
        }
        
        newClubs = []
        if let data = json["NewList"].array {
            for datum in data {
                let club: Club = Club(with: datum)
//                Logger.d("load new club from server response: \(club)")
                newClubs.append(club)
            }
        }
    }
    
    func hasEmptyClub() -> Bool {
        if hotClubs.count == 0 || newClubs.count == 0 {
            return true
        }
        
        return false
    }
}
