//
//  ClubData.swift
//  myvolkswagen
//
//  Created by CHUNG CHING JIANG on 2019/10/7.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class ClubData: NSObject {

    var clubs: [Club] = []
    var news: [ClubPost] = []
    
    
    
    public override var description: String {
        return "\n{\n clubs: \(String(describing: self.clubs)),\n"
            + " news: \(String(describing: self.news))\n}"
    }
    
    
    
    init(with json:JSON) {
        super.init()
        
        clubs = []
        if let data = json["ClubList"].array {
            for datum in data {
                let club: Club = Club(with: datum)
//                Logger.d("load hot club from server response: \(club)")
                clubs.append(club)
            }
        }
        
        news = []
        if let data = json["NewsList"].array {
            for datum in data {
                let post: ClubPost = ClubPost(with: datum)
//                Logger.d("load new club from server response: \(club)")
                news.append(post)
            }
        }
    }
    
}
