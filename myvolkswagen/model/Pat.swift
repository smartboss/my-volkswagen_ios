//
//  Pat.swift
//  myvolkswagen
//
//  Created by TWTPEMAC021 on 2021/9/27.
//  Copyright © 2021 volkswagen. All rights reserved.
//

import Foundation
import SwiftyJSON

class Pat: NSObject {
    
    var patId: Int?
    var vin: String?
    var carNumber: String?
    var currentMileage: String?
    var annualMileage: String?
    var cycleDay: String?
    
    var patData: [PatData]!
    
    override init() {
        super.init()
    }
    
    init(with json:JSON) {
        super.init()
        
        patId = json["PAT_ID"].intValue
        vin = json["VIN"].string
        carNumber = json["CarNumber"].string
        currentMileage = json["CurrentMileage"].string
        annualMileage = json["AnnualMileage"].string
        cycleDay = json["CycleDay"].string
        
        patData = []
        if let items = json["Data"].array {
            if items.count > 0 {
                for itemm in items {
                    let patdd: PatData = PatData(with: itemm)
                    patData?.append(patdd)
                }
            }
        }
    }
}

class PatData: NSObject {
    
    var service: [String]?
    var additional: [String]?
    var predictedServiceDate: String?
    var predictedServiceMileage: String?
    
    override init() {
        super.init()
    }
    
    init(with json:JSON) {
        super.init()
        
        service = json["Service"].arrayValue.map { $0.stringValue}
        additional = json["Additional"].arrayValue.map { $0.stringValue}
        predictedServiceDate = json["PredictedServiceDate"].stringValue
        predictedServiceMileage = json["PredictedServiceMileage"].stringValue
    }
}
