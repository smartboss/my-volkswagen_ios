//
//  Point.swift
//  myvolkswagen
//
//  Created by Apple on 7/10/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class Point: NSObject {
    var id: String
    var name: String
    var intersections: [String]?
    var lat: Double
    var lon: Double
    
    
    
    
    public override var description: String {
        return "\n{\n id: \(self.id),\n"
            + " name: \(self.name),\n"
            + " intersections: \(String(describing: self.intersections)),\n"
            + " lat: \(self.lat),\n"
            + " lon: \(self.lon)\n}"
    }
    
    init(with json:JSON) {
        id = json["id"].stringValue
        name = json["name"].stringValue
        intersections = json["intersections"].arrayValue.map { $0.stringValue }
        lat = json["lat"].doubleValue
        lon = json["lon"].doubleValue
        
        super.init()
    }

}
