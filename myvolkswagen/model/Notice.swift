//
//  Notice.swift
//  myvolkswagen
//
//  Created by Apple on 3/3/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

enum NoticeKind: String {
    case follow = "11", like = "21", officialActivity = "31", officialNews = "32", inBox = "33", normalSurvey = "34", newCarsSurvey = "35", bodyAndPaint = "36", recall = "37", newPost = "41", comment = "51", clubComment = "52", suggestedFriend = "61", clubPostComment = "81", postComment = "82", newClubPost = "91", redeem = "99", drivingAssistant = "101"
}

enum ReadStatus: Int {
    case unread = 0, read
}

class Notice: NSObject {

    var kind: String
    var noticeID: Int
    var content: String?
    var stickerURL: String?
    var read: Int
    var accountId: String?
    var level: String?
    var nickname: String?
    var newsID: Int?
    var commentContent: String?
    var pDate: String?
    var offical: Int = 1
    var isFollower: Int = 0
    var isFollow: Int = 0
    var SVURL: String?
    var imagesList: [String]?
    var title: String?
    var des: String?
    
    
    
    public override var description: String {
        return "\n{\n kind: \(String(describing: self.kind)),\n"
            + " noticeID: \(String(describing: self.noticeID)),\n"
            + " content: \(String(describing: self.content)),\n"
            + " stickerURL: \(String(describing: self.stickerURL)),\n"
            + " read: \(String(describing: self.read)),\n"
            + " accountId: \(String(describing: self.accountId)),\n"
            + " level: \(String(describing: self.level)),\n"
            + " nickname: \(String(describing: self.nickname)),\n"
            + " newsID: \(String(describing: self.newsID)),\n"
            + " commentContent: \(String(describing: self.commentContent)),\n"
            + " pDate: \(String(describing: self.pDate)),\n"
            + " offical: \(String(describing: self.offical)),\n"
            + " isFollower: \(String(describing: self.isFollower)),\n"
            + " isFollow: \(String(describing: self.isFollow))\n}"
    }
    
//    override init() {
//        super.init()
//    }
    
    init(with json:JSON) {
        kind = json["Kind"].stringValue
        noticeID = json["NoticeID"].intValue
        content = json["Content"].string
        stickerURL = json["StickerURL"].string
        read = json["Read"].intValue
        accountId = json["AccountID"].string
        level = json["Level"].string
        nickname = json["Nickname"].string
        newsID = json["NewsID"].int
        commentContent = json["CommentContent"].string
        pDate = json["PDate"].string
        offical = json["Offical"].intValue
        isFollower = json["IsFollower"].intValue
        isFollow = json["IsFollow"].intValue
        SVURL = json["SVURL"].string
        imagesList = json["PhotoList"].arrayValue.map { $0.stringValue}
        title = json["title"].string
        des = json["description"].string
        
        super.init()
    }
    
    init(with kindd: String, idd: Int) {
        kind = kindd
        noticeID = idd
        content = ""
        stickerURL = ""
        read = 0
        accountId = ""
        level = ""
        nickname = ""
        newsID = 0
        commentContent = ""
        pDate = ""
        offical = 0
        isFollower = 0
        isFollow = 0
        SVURL = ""
        imagesList = []
        title = ""
        des = ""
        
        super.init()
    }
    
    func getDateText() -> String? {
        return UiUtility.formatDateText(from: pDate)
    }
    
    func isRead() -> Bool {
        return read == ReadStatus.read.rawValue
    }
    
    func setReadStatus(_ status: ReadStatus) {
        self.read = status.rawValue
    }
}
