//
//  Post.swift
//  myvolkswagen
//
//  Created by Apple on 3/1/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class Post: NSObject {

    // edit
    var newsId: Int?
    var photoList: [String]?
    
    // add
    var images: [UIImage]?
    
    var content: String?
    var models: [Model]
    
    
    
    public override var description: String {
        return "\n{\n newsId: \(String(describing: self.newsId)),\n"
            + " photoList: \(String(describing: self.photoList)),\n"
            + " content: \(String(describing: self.content)),\n"
            + " models: \(String(describing: self.models))\n}"
    }
    
    override init() {
        models = []

        super.init()
    }
    
    init(with news: News) {
        self.newsId = news.newsId
        self.photoList = news.photoList
        self.content = news.content
        
        models = []
        for m in news.modelList {
            let model: Model = Model.init(with: m)
            models.append(model)
        }
        super.init()
    }
    
    init(with clubPost: ClubPost) {
        self.newsId = clubPost.newsId
        self.photoList = clubPost.photoList
        self.content = clubPost.content
        
        models = []
        super.init()
    }
    
    init(with images: [UIImage]) {
        self.images = images
        models = []
        super.init()
    }
    
    func getImageStream() -> String {
        return ""
    }
}
