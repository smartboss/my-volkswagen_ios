//
//  Result.swift
//  myvolkswagen
//
//  Created by Apple on 1/31/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class Result: NSObject {
    
    var status: Bool?
    var errorCode: Int?
    var errorMsg: String?
    
    
    
    public override var description: String {
        return "\n{\n status: \(String(describing: self.status)),\n"
            + " errorCode: \(String(describing: self.errorCode)),\n"
            + " errorMsg: \(String(describing: self.errorMsg))\n}"
    }
    
    override init() {
        super.init()
    }
    
    init(with json:JSON) {
        let sp = json["Result"].string
        status = sp == "Y"
        errorCode = json["ErrorCode"].int
        errorMsg = json["ErrorMsg"].string
        
        
        super.init()
    }
    
    func isSuccess() -> Bool {
        return status ?? false
    }
}
