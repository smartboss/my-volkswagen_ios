//
//  Notification.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/13.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import Foundation
import SwiftyJSON

class Notificationn: NSObject {

    var eventTime: String?
    var descriptionn: String?
    var btnUrl: String?
    
    init(with json:JSON) {
        eventTime = json["event_time"].stringValue
        descriptionn = json["description"].stringValue
        btnUrl = json["btn_url"].stringValue
        
        super.init()
    }
    
    override init() {
        super.init()
    }
}
