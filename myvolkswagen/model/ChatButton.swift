//
//  ChatButton.swift
//  myvolkswagen
//
//  Created by Apple on 8/29/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChatButton: NSObject {
    
    enum ButtonType: String {
        case text = "text", url = "url", image = "image", deeplink = "deeplink", generic = "generic"
    }
    
    var id: Int?
    var type: String?
    var buttonName: String?
    var content: String?
    var genericMenu: ChatGenericMenu?
    
    
    public override var description: String {
        return "\n{\n id: \(String(describing: self.id)),\n"
            + " type: \(String(describing: self.type)),\n"
            + " buttonName: \(String(describing: self.buttonName)),\n"
            + " content: \(String(describing: self.content)),\n"
            + " genericMenu: \(String(describing: self.genericMenu))\n}"
    }
    
    func toDictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        
        if let tt = type {
            dictionary["type"] = tt
        }
        
        if let idd = id {
            dictionary["id"] = idd
        }
        
        if let buttonNamee = buttonName {
            dictionary["buttonName"] = buttonNamee
        }
        
        if let contentt = content {
            dictionary["content"] = contentt
        }
        
        if let genericMenuu = genericMenu {
            dictionary["genericMenu"] = genericMenuu.toDictionary()
        }
        
        return dictionary
    }
    
    init(with json:JSON) {
        super.init()
        
        id = json["id"].int
        type = json["type"].string
        buttonName = json["buttonName"].string
        content = json["content"].string
        
        genericMenu = ChatGenericMenu.init(with: json["genericMenu"])
    }

}
