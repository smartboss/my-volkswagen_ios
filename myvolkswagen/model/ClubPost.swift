//
//  ClubPost.swift
//  myvolkswagen
//
//  Created by CHUNG CHING JIANG on 2019/10/7.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class ClubPost: NSObject {

    var clubId: Int
    var clubName: String
    var accountId: String?
    var level: String?
    var nickname: String?
    var stickerURL: String?
    var newsId: Int?
    var pDate: String?
    var content: String?
    var photoList: [String]?
    var offical: Int?
    var likeAmount: Int = 0
    var likeStatus: Int = 0
    var bookmarkAmount: Int = 0
    var bookmarkStatus: Int = 0
//    var reportStatus: Int = 0
    var commentAmount: Int = 0
    var commentStatus: Int = 0
    var isFollower: Int = 0
    var isFollow: Int = 0
    var isTop: Int = 0
//    var modelList: [Model] = []
//
    var comments: [Comment] = []

    
    
    public override var description: String {
        return "\n{\n clubId: \(String(describing: self.clubId)),\n"
            + " clubName: \(String(describing: self.clubName)),\n"
            + " accountId: \(String(describing: self.accountId)),\n"
            + " level: \(String(describing: self.level)),\n"
            + " nickname: \(String(describing: self.nickname)),\n"
            + " stickerURL: \(String(describing: self.stickerURL)),\n"
            + " newsId: \(String(describing: self.newsId)),\n"
            + " pDate: \(String(describing: self.pDate)),\n"
            + " content: \(String(describing: self.content)),\n"
            + " photoList: \(String(describing: self.photoList)),\n"
            + " offical: \(String(describing: self.offical)),\n"
            + " likeAmount: \(String(describing: self.likeAmount)),\n"
            + " likeStatus: \(String(describing: self.likeStatus)),\n"
            + " bookmarkAmount: \(String(describing: self.bookmarkAmount)),\n"
            + " bookmarkStatus: \(String(describing: self.bookmarkStatus)),\n"
            + " commentAmount: \(String(describing: self.commentAmount)),\n"
            + " commentStatus: \(String(describing: self.commentStatus)),\n"
            + " isFollower: \(String(describing: self.isFollower)),\n"
            + " isFollow: \(String(describing: self.isFollow)),\n"
            + " isTop: \(String(describing: self.isTop))\n}"
            + " comments: \(String(describing: self.comments))\n}"
    }
    
//    override init() {
//        clubId = ""
//        clubName = ""
//    }
    
    init(with newsId: Int) {
        clubId = 0
        clubName = ""
        
        super.init()
        
        self.newsId = newsId
    }
    
//    init(with notice: Notice) {
//        clubId = ""
//        clubName = ""
//        
//        super.init()
//        
//        accountId = notice.accountId
//        level = notice.level
//        nickname = notice.nickname
//        stickerURL = notice.stickerURL
//        newsId = notice.newsID
////        pDate = json["PDate"].string
//        
////        offical = json["Offical"].int
//        isFollower = notice.isFollower
//        isFollow = notice.isFollow
//    }

    init(with json: JSON) {
        clubId = json["ClubID"].intValue
        clubName = json["ClubName"].stringValue
        
        super.init()

        accountId = json["AccountID"].string
        level = json["Level"].string
        nickname = json["Nickname"].string
        stickerURL = json["StickerURL"].string
        newsId = json["NewsID"].int
        pDate = json["PDate"].string
        content = json["Content"].string
        photoList = json["PhotoList"].arrayValue.map { $0.stringValue}
        
        offical = json["Offical"].int
        likeAmount = json["LikeAmount"].intValue
        likeStatus = json["LikeStatus"].intValue
        bookmarkAmount = json["BookmarkAmount"].intValue
        bookmarkStatus = json["BookmarkStatus"].intValue
//        reportStatus = json["ReportStatus"].intValue
        commentAmount = json["CommentAmount"].intValue
        commentStatus = json["CommentStatus"].intValue
        isFollower = json["IsFollower"].intValue
        isFollow = json["IsFollow"].intValue
        isTop = json["IsTop"].intValue
    }
    
//    func edited(by post: Post) {
//        content = post.content
//        modelList = []
//        for model in post.models {
//            modelList.append(model)
//        }
//    }
    
    func update(with json: JSON) {
        let newsObject = json["News"]
    
        clubId = newsObject["ClubID"].intValue
        clubName = newsObject["ClubName"].stringValue
        accountId = newsObject["AccountID"].string
        level = newsObject["Level"].string
        nickname = newsObject["Nickname"].string
        stickerURL = newsObject["StickerURL"].string
        newsId = newsObject["NewsID"].int
        pDate = newsObject["PDate"].string
        content = newsObject["Content"].string
        photoList = newsObject["PhotoList"].arrayValue.map { $0.stringValue}
        
        offical = newsObject["Offical"].int
        likeAmount = newsObject["LikeAmount"].intValue
        likeStatus = newsObject["LikeStatus"].intValue
        bookmarkAmount = newsObject["BookmarkAmount"].intValue
        bookmarkStatus = newsObject["BookmarkStatus"].intValue
//        reportStatus = newsObject["ReportStatus"].intValue
        commentAmount = newsObject["CommentAmount"].intValue
        commentStatus = newsObject["CommentStatus"].intValue
        isFollower = newsObject["IsFollower"].intValue
        isFollow = newsObject["IsFollow"].intValue
        
//        modelList = []
//        if let data = newsObject["ModelList"].array {
//            for datum in data {
//                let newsId: Int = datum.intValue
//                if let m = NewsManager.shared().getModel(by: newsId) {
//                    modelList.append(m)
//                }
//            }
//        }
        
        comments = []
        if let items = json["CommentLists"].array {
            for item in items {
                let comment: Comment = Comment(with: item)
                comments.append(comment)
            }
        }
    }
    
    func getDateText() -> String? {
        return UiUtility.formatDateText(from: pDate)
    }
    
    func isMine() -> Bool {
        if let aid = accountId {
            if let myId = UserManager.shared().currentUser.accountId {
                return aid == myId
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    func isTopping() -> Bool {
        return isTop == 1
    }
}
