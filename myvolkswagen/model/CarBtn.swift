//
//  CarBtn.swift
//  myvolkswagen
//
//  Created by Shelley on 2021/11/7.
//  Copyright © 2021 volkswagen. All rights reserved.
//

import Foundation
import SwiftyJSON

class CarBtn: NSObject {
    
    var BtnCode: String?
    var BtnName: String?
    var Order: Int?
    var Status: Int = 0
    var url: String?
    
    override init() {
        super.init()
    }
    
    init(with json:JSON) {
        super.init()
    
        BtnCode = json["BtnCode"].string
        BtnName = json["BtnName"].string
        Order = json["Order"].intValue
        Status = json["Status"].intValue
        url    = json["Url"].string
    }
}
