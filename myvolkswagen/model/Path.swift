//
//  Path.swift
//  myvolkswagen
//
//  Created by Apple on 7/10/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class Path: NSObject {

    var id: Int
    var name: String
    var icon: String
    var direction: Int
    var points: [Point]!
    
    
    
    
    public override var description: String {
        return "\n{\n id: \(self.id),\n"
            + " name: \(self.name),\n"
            + " icon: \(self.icon),\n"
            + " direction: \(self.direction),\n"
            + " points: \(String(describing: self.points))\n}"
    }
    
    init(with json:JSON) {
        id = json["roadid"].intValue
        name = json["name"].stringValue
        icon = json["icon"].stringValue
        direction = json["direction"].intValue
        
        points = []
        if let items = json["points"].array {
            for item in items {
//                Logger.d("item: \(item)")
                let point: Point = Point(with: item)
//                Logger.d("load point from server response: \(point)")
                points?.append(point)
            }
        }
        
        super.init()
    }
}
