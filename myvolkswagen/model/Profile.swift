//
//  Profile.swift
//  myvolkswagen
//
//  Created by Apple on 2/26/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

enum Gender: String {
    case male = "M", female = "F", notSpecified = "N"
    
    func displayText() -> String {
        switch self {
        case .male:
            return "male".localized
        case .female:
            return "female".localized
        case.notSpecified:
            return "not_specified".localized
        }
    }
}

enum Role: Int {
    case normal = 1, official
}

class Profile: NSObject {
    
    enum AccountType: Int {
        case normal = 1, official
        
        static var allCases: [AccountType] {
            var values: [AccountType] = []
            var index = 0
            while let element = self.init(rawValue: index) {
                values.append(element)
                index += 1
            }
            return values
        }
    }
    
    

    var accountId: String?
    var level: String?
    var levelName: String?
    var ownModel: [String]?
    var name: String?
    var nickname: String?
    var sex: String?
    var brief: String?
    var email: String?
    var cellphone: String?
    var birthday: String?
    var address: String?
    var stickerURL: String?
    var coverURL: String?
    var followAmount: Int = 0
    var followerAmount: Int = 0
    var offical: Int?
    var dealerAreaId: String?
    var dealerAreaName: String?
    var dealerPlantId: String?
    var dealerPlantName: String?
    var OTPFlag: Int = 0
    
    var stickerStream: String?
    var coverStream: String?
    
    
    
    public override var description: String {
        return "\n{\n accountId: \(String(describing: self.accountId)),\n"
            + " level: \(String(describing: self.level)),\n"
            + " levelName: \(String(describing: self.levelName)),\n"
            + " ownModel: \(String(describing: self.ownModel)),\n"
            + " nickname: \(String(describing: self.nickname)),\n"
            + " sex: \(String(describing: self.sex)),\n"
            + " brief: \(String(describing: self.brief)),\n"
            + " email: \(String(describing: self.email)),\n"
            + " cellphone: \(String(describing: self.cellphone)),\n"
            + " birthday: \(String(describing: self.birthday)),\n"
            + " stickerURL: \(String(describing: self.stickerURL)),\n"
            + " coverURL: \(String(describing: self.coverURL)),\n"
            + " followAmount: \(String(describing: self.followAmount)),\n"
            + " followerAmount: \(String(describing: self.followerAmount)),\n"
            + " offical: \(String(describing: self.offical)),\n"
            + " has stickerStream: \(String(describing: self.stickerStream != nil)),\n"
            + " has coverStream: \(String(describing: self.coverStream != nil)),\n"
            + " dealerAreaId: \(String(describing: self.dealerAreaId)),\n"
            + " dealerAreaName: \(String(describing: self.dealerAreaName)),\n"
            + " dealerPlantId: \(String(describing: self.dealerPlantId)),\n"
            + " dealerPlantName: \(String(describing: self.dealerPlantName))\n}"
    }
    
    override init() {
        super.init()
    }
    
    init(with accountId: String) {
        super.init()
        
        self.accountId = accountId
    }
    
    init(with relation: Relation) {
        super.init()
        
        accountId = relation.accountId
        level = relation.level
        ownModel = relation.ownModel
        nickname = relation.nickname
        stickerURL = relation.stickerURL
        offical = relation.official
    }
    
    init(with relation: ClubRelation) {
        super.init()
        
        accountId = relation.accountId
        level = relation.level
        ownModel = relation.ownModel
        nickname = relation.nickname
        stickerURL = relation.stickerURL
        offical = relation.official
    }
    
    init(with comment: Comment) {
        super.init()
        
        accountId = comment.accountId
        level = comment.level
        nickname = comment.nickname
        stickerURL = comment.stickerURL
        offical = comment.offical
    }
    
    init(with notice: Notice) {
        super.init()
        
        accountId = notice.accountId
        level = notice.level
        nickname = notice.nickname
        stickerURL = notice.stickerURL
        offical = notice.offical
    }
    
    init(with news: News) {
        super.init()
        
        accountId = news.accountId
        level = news.level
        nickname = news.nickname
        stickerURL = news.stickerURL
        offical = news.offical
    }
    
    init(with clubPost: ClubPost) {
        super.init()
        
        accountId = clubPost.accountId
        level = clubPost.level
        nickname = clubPost.nickname
        stickerURL = clubPost.stickerURL
        offical = clubPost.offical
    }
    
    init(with profile: Profile) {
        super.init()
        
        accountId = profile.accountId
        level = profile.level
        levelName = profile.levelName
        ownModel = profile.ownModel
        nickname = profile.nickname
        sex = profile.sex
        brief = profile.brief
        email = profile.email
        cellphone = profile.cellphone
        birthday = profile.birthday
        stickerURL = profile.stickerURL
        coverURL = profile.coverURL
        followAmount = profile.followAmount
        followerAmount = profile.followerAmount
        offical = profile.offical
        dealerAreaId = profile.dealerAreaId
        dealerAreaName = profile.dealerAreaName
        dealerPlantId = profile.dealerPlantId
        dealerPlantName = profile.dealerPlantName
        OTPFlag = profile.OTPFlag
    }
    
    init(with json:JSON) {
        accountId = json["AccountID"].string
        if json["Level"].string == nil {
            level = String(json["Level"].int ?? 0)
        } else {
            level = json["Level"].string
        }
        levelName = json["LevelName"].string
        ownModel = []
        if let data = json["OwnModel"].array {
            for datum in data {
                ownModel?.append(datum.stringValue)
            }
        }
        nickname = json["Nickname"].string
        sex = json["Sex"].string
        brief = json["Brief"].string
        email = json["EMail"].string
        cellphone = json["Cellphone"].string
        birthday = json["Birthday"].string
        stickerURL = json["StickerURL"].string
        coverURL = json["CoverURL"].string
        followAmount = json["FollowAmount"].intValue
        followerAmount = json["FollowerAmount"].intValue
        offical = json["Offical"].intValue
        dealerAreaId = json["DealerAreaId"].stringValue
        dealerAreaName = json["DealerAreaName"].string
        dealerPlantId = json["DealerPlantId"].stringValue
        dealerPlantName = json["DealerPlantName"].string
        OTPFlag = json["OTPFlag"].intValue
        
        super.init()
    }
    
    func setBirthday(to date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        birthday = dateFormatter.string(from: date)
    }
    
    func getBirthDate() -> Date? {
        if let b = birthday {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            guard let date = dateFormatter.date(from: b) else {
//                fatalError("ERROR: Date conversion failed due to mismatched format.")
                Logger.e("can't convert birthday string to object")
                return nil
            }
            
            return date
        } else {
            return nil
        }
    }
    
    func getBirthdayText() -> String {
        if let d = getBirthDate() {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy/MM/dd"
            return dateFormatter.string(from: d)
        } else {
            return ""
        }
    }
    
    func getGenderText() -> String {
        if let g = sex {
            if let gender: Gender = Gender.init(rawValue: g) {
                return gender.displayText()
            }
        }
        
        return Gender.notSpecified.displayText()
    }
    
    func setGender(to gender: Gender) {
        sex = gender.rawValue
    }
    
    func isEdited(from profile: Profile) -> Bool {
        return nickname != profile.nickname || sex != profile.sex || brief != profile.brief || email != profile.email || cellphone != profile.cellphone || birthday != profile.birthday || stickerStream != nil || coverStream != nil
    }
    
    func isMine() -> Bool {
        if let aid = accountId {
            if let myId = UserManager.shared().currentUser.accountId {
                return aid == myId
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    
    
    // update profile maybe
//    var nickname: String?
//    var sex: String?
//    var brief: String?
//    var email: String?
//    var cellphone: String?
//    var birthday: String?
//    var stickerStream: String?
//    var coverStream: String?
//
//
//
//    public override var description: String {
//        return "\n{\n nickname: \(String(describing: self.nickname)),\n"
//            + " sex: \(String(describing: self.sex)),\n"
//            + " brief: \(String(describing: self.brief)),\n"
//            + " email: \(String(describing: self.email)),\n"
//            + " cellphone: \(String(describing: self.cellphone)),\n"
//            + " birthday: \(String(describing: self.birthday)),\n"
//            + " stickerStream: \(String(describing: self.stickerStream)),\n"
//            + " coverStream: \(String(describing: self.coverStream))\n}"
//    }
//
//    override init() {
//        super.init()
//    }
//
//    init(with json:JSON) {
//        nickname = json["Nickname"].string
//        sex = json["Sex"].string
//        brief = json["Brief"].string
//        email = json["EMail"].string
//        cellphone = json["Cellphone"].string
//        birthday = json["Birthday"].string
//        stickerStream = json["StickerStream"].string
//        coverStream = json["CoverStream"].string
//
//        super.init()
//    }
}
