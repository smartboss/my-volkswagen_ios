//
//  Redeem.swift
//  myvolkswagen
//
//  Created by Shelley on 2020/1/22.
//  Copyright © 2020 volkswagen. All rights reserved.
//

import Foundation
import SwiftyJSON

class Redeem: NSObject {
    var redeemId: Int?
    var name: String?
    var getTime: String?
    var redeemExpired: String?
    var status: Int?
    var point: String?
    var receiveExpired: String?
    
    public override var description: String {
        return "\n{\n RedeemId: \(String(describing: self.redeemId)),\n"
            + " Name: \(String(describing: self.name)),\n"
            + " GetTime: \(String(describing: self.getTime)),\n"
            + " RedeemExpired: \(String(describing: self.redeemExpired)),\n"
            + " Status: \(String(describing: self.status)),\n"
            + " Point: \(String(describing: self.point)),\n"
            + " ReceiveExpired: \(String(describing: self.receiveExpired))\n}"
    }
    
    init(with json:JSON) {
        super.init()
        
        redeemId = json["RedeemID"].int
        name = json["Name"].string
        getTime = json["GetTime"].string
        redeemExpired = json["RedeemExpired"].string
        status = json["Status"].int
        point = json["Point"].string
        receiveExpired = json["ReceiveExpired"].string
    }
}
