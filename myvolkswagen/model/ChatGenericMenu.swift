//
//  ChatGenericMenu.swift
//  myvolkswagen
//
//  Created by Apple on 9/2/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChatGenericMenu: NSObject {

    var elements: [ChatElement]!
    
    public override var description: String {
            return "{elements: \(String(describing: self.elements))"
    }
    
    init(with json:JSON) {
        super.init()
        
        elements = []
        if let items = json["elements"].array {
            for item in items {
//                Logger.d("item: \(item)")
                let chatElement: ChatElement = ChatElement(with: item)
//                Logger.d("load chat element from server response: \(chatElement)")
                elements?.append(chatElement)
            }
        }
    }
    
    func toDictionary() -> [String: Any] {
        var elem: [[String: Any]] = []
        if let items = self.elements {
            for item in items {
                elem.append(item.toDictionary())
            }
        }
        return ["elements": elem]
    }
}
