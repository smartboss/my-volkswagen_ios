//
//  Relation.swift
//  myvolkswagen
//
//  Created by Apple on 3/2/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class Relation: NSObject {

    var accountId: String?
    var official: Int = Role.normal.rawValue
    var level: String?
    var ownModel: [String]?
    var nickname: String?
    var stickerURL: String?
    var isFollower: Int = 0
    var isFollow: Int = 0
    
    
    
    
    public override var description: String {
        return "\n{\n accountId: \(String(describing: self.accountId)),\n"
            + " official: \(String(describing: self.official)),\n"
            + " level: \(String(describing: self.level)),\n"
            + " ownModel: \(String(describing: self.ownModel)),\n"
            + " nickname: \(String(describing: self.nickname)),\n"
            + " stickerURL: \(String(describing: self.stickerURL)),\n"
            + " isFollower: \(String(describing: self.isFollower)),\n"
            + " isFollow: \(String(describing: self.isFollow))\n}"
    }
    
    override init() {
        super.init()
    }
    
//    init(with news:News) {
//        super.init()
//
//        accountId = news.accountId
//        level = news.level
//        //        ownModel = profile.ownModel
//        nickname = news.nickname
//        //        sex = profile.sex
//        //        brief = profile.brief
//        //        email = profile.email
//        //        cellphone = profile.cellphone
//        //        birthday = profile.birthday
//        stickerURL = news.stickerURL
//        //        coverURL = profile.coverURL
//        //        followAmount = profile.followAmount
//        //        followerAmount = profile.followerAmount
//        offical = news.offical
//    }
//
//    init(with profile:Profile) {
//        super.init()
//
//        accountId = profile.accountId
//        level = profile.level
//        ownModel = profile.ownModel
//        nickname = profile.nickname
//        sex = profile.sex
//        brief = profile.brief
//        email = profile.email
//        cellphone = profile.cellphone
//        birthday = profile.birthday
//        stickerURL = profile.stickerURL
//        coverURL = profile.coverURL
//        followAmount = profile.followAmount
//        followerAmount = profile.followerAmount
//        offical = profile.offical
//    }
    
    init(with json:JSON) {
        accountId = json["AccountID"].string
        official = json["Offical"].intValue
        level = json["Level"].string
        ownModel = []
        if let data = json["OwnModel"].array {
            for datum in data {
                ownModel?.append(datum.stringValue)
            }
        }
        nickname = json["Nickname"].string
        stickerURL = json["StickerURL"].string
        isFollower = json["IsFollower"].intValue
        isFollow = json["IsFollow"].intValue
        
        super.init()
    }
    
    func isMe() -> Bool {
        if let aid = accountId {
            if let myId = UserManager.shared().currentUser.accountId {
                return aid == myId
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    func isFollowedByMe() -> Bool {
        return isFollow == 1
    }
    
    func isFollowingMe() -> Bool {
        return isFollower == 1
    }
    
    func isOfficial() -> Bool {
        return official == Role.official.rawValue
    }
}
