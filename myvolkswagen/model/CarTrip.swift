//
//  CarTrip.swift
//  myvolkswagen
//
//  Created by Shelley on 2023/10/19.
//  Copyright © 2023 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON
//import GoogleMaps

class CarTrip: NSObject {
    var startTime: String?
    var endTime: String?
    var distance: Int?
    var averageFuelConsumption: Float?
    var drivingTime: Int?
    var maxSpeed: Int?
    var startLocation: CarLocation?
    var endLocation: CarLocation?
    var startAddress: String?
    var endAddress: String?
    
    init(with json:JSON) {
        super.init()
        
        startTime = json["start_time"].string
        endTime = json["end_time"].string
        distance = json["distance"].int
        averageFuelConsumption = json["average_fuel_consumption"].float
        drivingTime = json["driving_time"].int
        maxSpeed = json["max_speed"].int
        startLocation = CarLocation(with: json["start_location"])
        endLocation = CarLocation(with: json["end_location"])
        
//        let geocoder = GMSGeocoder()
//        let coordinate = CLLocationCoordinate2D(latitude: startLocation!.latitude!, longitude: startLocation!.longitude!)
//        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
//            if let address = response?.firstResult() {
//                let lines = address.lines
//                if let addressString = lines?.joined(separator: "\n") {
//                    print("反地理編碼結果：\(addressString)")
//                    self.startAddress = addressString
//                }
//            } else {
//                print("反地理編碼失敗，錯誤：\(error?.localizedDescription ?? "未知錯誤")")
//            }
//        }
//
//        let geocoder2 = GMSGeocoder()
//        let coordinate2 = CLLocationCoordinate2D(latitude: endLocation!.latitude!, longitude: endLocation!.longitude!)
//        geocoder2.reverseGeocodeCoordinate(coordinate2) { response, error in
//            if let address = response?.firstResult() {
//                let lines = address.lines
//                if let addressString = lines?.joined(separator: "\n") {
//                    print("反地理編碼結果：\(addressString)")
//                    self.endAddress = addressString
//                }
//            } else {
//                print("反地理編碼失敗，錯誤：\(error?.localizedDescription ?? "未知錯誤")")
//            }
//        }
    }
}

class CarLocation: NSObject {
    var latitude: Double?
    var longitude: Double?
    var elevation: Int?
    var heading: Int?
    var speed: Int?
    var eventTime: String?
    
    init(with json:JSON) {
        super.init()
        
        latitude = json["latitude"].double
        longitude = json["longitude"].double
        elevation = json["elevation"].int
        heading = json["heading"].int
        speed = json["speed"].int
        eventTime = json["event_time"].string
    }
}
