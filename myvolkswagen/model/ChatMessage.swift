//
//  ChatMessage.swift
//  myvolkswagen
//
//  Created by Apple on 8/29/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class ChatMessage: NSObject {
    
    enum Direction: Int {
        case incoming = 0, outgoing, grade, keep
    }

    var direction: Direction
    var text: String?
    var button: ChatButton?
    
    
    
    init(with direction: Direction) {
        self.direction = direction
    }
    
    init(with direction: Direction, text: String?) {
        self.direction = direction
        self.text = text
    }
    
    init(with direction: Direction, text: String?, button: ChatButton?) {
        self.direction = direction
        self.text = text
        self.button = button
    }
}
