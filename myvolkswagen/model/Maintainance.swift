//
//  Maintainance.swift
//  myvolkswagen
//
//  Created by Apple on 2/24/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class Maintainance: NSObject {

    var vin: String?
    var licensePlateNumber: String?
    var dealer: String?
    var charge: String?
    var mileage: String?
    var externalCharge: String?
    var type: [String]?
    var closeDate: String?
    var orderDetailsExternal: String?
    var orderDetailsWarranty: String?
    var orderDetailsInsurance: String?
    var dealerAreaId: String?
    var dealerAreaName: String?
    var dealerPlantId: String?
    var dealerPlantName: String?
    
    
    public override var description: String {
        return "\n{\n vin: \(String(describing: self.vin)),\n"
            + " licensePlateNumber: \(String(describing: self.licensePlateNumber)),\n"
            + " dealer: \(String(describing: self.dealer)),\n"
            + " charge: \(String(describing: self.charge)),\n"
            + " mileage: \(String(describing: self.mileage)),\n"
            + " externalCharge: \(String(describing: self.externalCharge)),\n"
            + " type: \(String(describing: self.type)),\n"
            + " closeDate: \(String(describing: self.closeDate)),\n"
            + " orderDetailsExternal: \(String(describing: self.orderDetailsExternal)),\n"
            + " orderDetailsWarranty: \(String(describing: self.orderDetailsWarranty)),\n"
            + " orderDetailsInsurance: \(String(describing: self.orderDetailsInsurance)),\n"
            + " dealerAreaId: \(String(describing: self.dealerAreaId)),\n"
            + " dealerAreaName: \(String(describing: self.dealerAreaName)),\n"
            + " dealerPlantId: \(String(describing: self.dealerPlantId)),\n"
            + " dealerPlantName: \(String(describing: self.dealerPlantName))\n}"
    }
    
    override init() {
        super.init()
    }
    
    init(with json:JSON) {
        super.init()
        
        vin = json["VIN"].string
        licensePlateNumber = json["LicensePlateNumber"].string
        dealer = json["Dealer"].string
        charge = json["Charge"].string
        mileage = json["Mileage"].string
        externalCharge = json["ExternalCharge"].string
        closeDate = json["CloseDate"].string
        type = json["Type"].arrayValue.map { $0.stringValue}
        orderDetailsExternal = json["OrderDetails"]["EXTERNAL"].string
        orderDetailsWarranty = json["OrderDetails"]["WARRANTY"].string
        orderDetailsInsurance = json["OrderDetails"]["INSURANCE"].string
        dealerAreaId = json["DealerAreaId"].string
        dealerAreaName = json["DealerAreaName"].string
        dealerPlantId = json["DealerPlantId"].string
        dealerPlantName = json["DealerPlantName"].string
    }
}

class MaintainancePlant: NSObject {

    var areaId: Int?
    var areaName: String?
    var plants: [Plant]?
    var plantId: Int?
    var plantName: String?
    var Address: String?
    var Phone: String?
    var Url: String?
    
    public override var description: String {
        return "\n{\n areaId: \(String(describing: self.areaId)),\n"
            + " areaName: \(String(describing: self.areaName)),\n"
            + " plants: \(String(describing: self.plants))\n}"
    }
    
    override init() {
        super.init()
    }
    
    init(with json:JSON) {
        super.init()
        
        areaId = json["AreaId"].intValue
        areaName = json["AreaName"].string
        plants = json["Plants"].arrayValue.map { (jsonn) -> Plant in
            Plant(with: jsonn)
        }
        plantId = json["PlantId"].intValue
        plantName = json["PlantsName"].string
        Address = json["Address"].string
        Phone = json["Phone"].string
        Url = json["Url"].string
    }
}

class Plant: NSObject {

    var plantId: Int?
    var plantName: String?
    
    
    public override var description: String {
        return "\n{\n plantId: \(String(describing: self.plantId)),\n"
            + " plantName: \(String(describing: self.plantName))\n}"
    }
    
    override init() {
        super.init()
    }
    
    init(with json:JSON) {
        super.init()
        
        plantId = json["PlantId"].intValue
        plantName = json["PlantName"].string
    }
}

class MaintainanceInfo: NSObject {

    var btnName: String?
    var btnUrl: String?
    var descriptionn: String?
    var infoId: String?
    var type: String?
    var eventName: String?
    var serviceName: String?
    var eventJson: [String: Any]?
    
    override init() {
        super.init()
    }
    
    init(with json:JSON) {
        super.init()
        
        btnName = json["btn_name"].string
        btnUrl = json["btn_url"].string
        descriptionn = json["description"].string
        infoId = json["id"].string
        type = json["type"].string
        eventName = json["event_name"].stringValue
        serviceName = json["service_name"].stringValue
        
        if let dictionary = convertJSONToDictionary(json: json["event_json"]) {
            eventJson = dictionary
        } else {
            print("event_json fail")
        }
    }
    
    func convertJSONToDictionary(json: JSON) -> [String: Any]? {
        guard let dictionary = json.dictionaryObject else {
            return nil
        }
        return dictionary
    }
}
