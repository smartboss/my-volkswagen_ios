//
//  User.swift
//  badminton
//
//  Created by Apple on 9/24/18.
//  Copyright © 2018 sofasogood. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {
    
    let keyFbId = "key_user_fb_id"
    let keyLineId = "key_user_line_uid"
    let keyAccountId = "key_user_account_id"
    let keyVwEmail = "key_user_vw_email"
    let keyVwMobile = "key_user_vw_mobile"
    let keyLevel = "key_user_level"
    let keyLevelName = "key_user_level_name"
    let keyShowProfile = "key_user_show_profile"
    
    var fbId: String?
    var lineUid: String?
    var accountId: String?
    var vwEmail: String?
    var vwMobie: String?
    var level: String?
    var levelName: String
    var showProfile: Bool?
    
    
    
    public override var description: String {
        return "\n{\n fbId: \(String(describing: self.fbId)),\n"
            + " accountId: \(String(describing: self.accountId)),\n"
            + " vwEmail: \(String(describing: self.vwEmail)),\n"
            + " vwMobile: \(String(describing: self.vwMobie)),\n"
            + " level: \(String(describing: self.level)),\n"
            + " levelName: \(String(describing: self.levelName)),\n"
            + " showProfile: \(String(describing: self.showProfile))\n}"
    }
    
    override init() {
        
        self.vwEmail = ""
        self.vwMobie = ""
        self.level = "0"
        self.levelName = ""
        
        super.init()
        
        load()
    }
    
    init(with json:JSON) {
        fbId = json["fb_id"].string
        lineUid = json["line_uid"].string
        accountId = json["account_id"].string
        vwEmail = json["vw_email"].stringValue
        let m = json["vw_mobile"].stringValue
        vwMobie = m
        level = json["level"].stringValue
        levelName = json["level_name"].stringValue
        let sp = json["show_profile"].string
        showProfile = sp == "Y"
        
        super.init()
    }
    
    private func load() {
        let defaults = UserDefaults.standard
        
        self.fbId = defaults.string(forKey: keyFbId)
        self.lineUid = defaults.string(forKey: keyLineId)
        self.accountId = defaults.string(forKey: keyAccountId)
        if let ve = defaults.string(forKey: keyVwEmail) {
            self.vwEmail = ve
        } else {
            self.vwEmail = ""
        }
        if let mo = defaults.string(forKey: keyVwMobile) {
            self.vwMobie = mo
        } else {
            self.vwMobie = ""
        }
        self.level = defaults.string(forKey: keyLevel)
        if let ln = defaults.string(forKey: keyLevelName) {
            self.levelName = ln
        } else {
            self.levelName = ""
        }
        self.showProfile = defaults.bool(forKey: keyShowProfile)
        
//        Logger.d("load user from local data: \(self.description)")
    }
    
    func save() {
        let defaults = UserDefaults.standard
        
        defaults.set(self.fbId, forKey: keyFbId)
        defaults.set(self.lineUid, forKey: keyLineId)
        defaults.set(self.accountId, forKey: keyAccountId)
        defaults.set(self.vwEmail, forKey: keyVwEmail)
        defaults.set(self.vwMobie, forKey: keyVwMobile)
        defaults.set(self.level, forKey: keyLevel)
        defaults.set(self.levelName, forKey: keyLevelName)
        defaults.set(self.showProfile, forKey: keyShowProfile)
    }
    
    func clear() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: keyFbId)
        defaults.removeObject(forKey: keyLineId)
        defaults.removeObject(forKey: keyAccountId)
        defaults.removeObject(forKey: keyVwEmail)
        defaults.removeObject(forKey: keyVwMobile)
        defaults.removeObject(forKey: keyLevel)
        defaults.removeObject(forKey: keyLevelName)
        defaults.removeObject(forKey: keyShowProfile)
        
        self.fbId = nil
        self.lineUid = nil
        
        self.accountId = nil
        self.vwEmail = ""
        self.vwMobie = ""
        self.level = "0"
        self.levelName = ""
        self.showProfile = nil
    }
    
}

