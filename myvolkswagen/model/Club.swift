//
//  Club.swift
//  myvolkswagen
//
//  Created by CHUNG CHING JIANG on 2019/9/27.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class Club: NSObject {
    
    var id: Int = 0
    var intro: String?
    var name: String?
    var pDate: String?
    var picUrl: String?
    var isAdmin: Int = 0
    var isJoin: Int = 0
    var memberCount: Int = 0
    var modelList: [Model] = []
    
    
    
    public override var description: String {
        return "\n{\n id: \(String(describing: self.id)),\n"
            + " intro: \(String(describing: self.intro)),\n"
            + " name: \(String(describing: self.name)),\n"
            + " pDate: \(String(describing: self.pDate)),\n"
            + " picUrl: \(String(describing: self.picUrl)),\n"
            + " isAdmin: \(String(describing: self.isAdmin)),\n"
            + " isJoin: \(String(describing: self.isJoin)),\n"
            + " memberCount: \(String(describing: self.memberCount)),\n"
            + " modelList: \(String(describing: self.modelList))\n}"
    }
    
    

    init(with id: Int) {
        super.init()
    }
    
    init(with json:JSON) {
        super.init()
        
        id = json["ClubID"].intValue
        intro = json["ClubIntro"].string
        name = json["ClubName"].string
        pDate = json["ClubPDate"].string
        picUrl = json["ClubPicURL"].string
        isAdmin = json["IsAdmin"].intValue
        isJoin = json["IsJoin"].intValue
        memberCount = json["MemberCount"].intValue
        
        if let data = json["ModelList"].array {
            for datum in data {
                let newsId: Int = datum.intValue
                if let m = NewsManager.shared().getModel(by: newsId) {
                    modelList.append(m)
                }
            }
        }
    }
    
    func isMember() -> Bool {
        return isJoin != 0
    }
    
    func isManager() -> Bool {
        return isAdmin != 0
    }
    
    func getDateText() -> String? {
        return UiUtility.formatDateText(from: pDate)
    }
}
