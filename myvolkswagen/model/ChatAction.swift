//
//  ChatAction.swift
//  myvolkswagen
//
//  Created by Apple on 9/2/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChatAction: NSObject {
    var data: String?
    var title: String?
    var type: String?
    
    public override var description: String {
        return "\n{\n data: \(String(describing: self.data)),\n"
            + " title: \(String(describing: self.title)),\n"
            + " type: \(String(describing: self.type))\n}"
    }
    
    init(with json:JSON) {
        super.init()
        
        data = json["data"].string
        title = json["title"].string
        type = json["type"].string
    }
    
    func toDictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        
        if let data = data {
            dictionary["data"] = data
        }
        
        if let title = title {
            dictionary["title"] = title
        }
        
        if let type = type {
            dictionary["type"] = type
        }

        return dictionary
    }
}
