//
//  Car.swift
//  myvolkswagen
//
//  Created by Apple on 2/23/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class Car: NSObject {
    
    var name: String?
    var vin: String?
    var licensePlateNumber: String?
    var carTypeID: String?
    var carTypeName: String?
    var carModelID: String?
    var carModelName: String?
    var carModelImage: String?
    var carDate: String?
    var ownerDate: String?
    var mileage: Float?
    var dealer: String?
    var dealerAreaId: Int?
    var dealerAreaName: String?
    var dealerPlantId: Int?
    var dealerPlantName: String?
    var dongleData: DongleData?
    
    var maintainances: [Maintainance]!
    
    
    
    public override var description: String {
        return "\n{\n name: \(String(describing: self.name)),\n"
            + " vin: \(String(describing: self.vin)),\n"
            + " licensePlateNumber: \(String(describing: self.licensePlateNumber)),\n"
            + " carTypeID: \(String(describing: self.carTypeID)),\n"
            + " carTypeName: \(String(describing: self.carTypeName)),\n"
            + " carModelID: \(String(describing: self.carModelID)),\n"
            + " carModelName: \(String(describing: self.carModelName)),\n"
            + " carModelImage: \(String(describing: self.carModelImage)),\n"
            + " carDate: \(String(describing: self.carDate)),\n"
            + " ownerDate: \(String(describing: self.ownerDate)),\n"
            + " mileage: \(String(describing: self.mileage)),\n"
            + " dealer: \(String(describing: self.dealer)),\n"
            + " dealerAreaId: \(String(describing: self.dealerAreaId)),\n"
            + " dealerAreaName: \(String(describing: self.dealerAreaName)),\n"
            + " dealerPlantId: \(String(describing: self.dealerPlantId)),\n"
            + " dealerPlantName: \(String(describing: self.dealerPlantName)),\n"
            + " maintainances: \(String(describing: self.maintainances))\n}"
    }
    
    override init() {
        
        super.init()
        
        load()
    }
    
    init(with json:JSON) {
        super.init()
        
        let carData = json["CarData"][0]
        
        name = carData["Name"].string
        vin = carData["VIN"].string
        licensePlateNumber = carData["LicensePlateNumber"].string
        carTypeID = carData["CarTypeID"].string
        carTypeName = carData["CarTypeName"].string
        carModelID = carData["CarModelID"].string
        carModelName = carData["CarModelName"].string
        //carModelImage = carData["CarModelImage"].string
        let urlString = carData["CarModelImage"].string
        let encodedUrlString = urlString?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        carModelImage = encodedUrlString
        carDate = carData["CarDate"].string
        ownerDate = carData["OwnerDate"].string
        mileage = carData["Mileage"].float
        dealer = carData["Dealer"].string
        dealerAreaId = carData["DealerAreaId"].int
        dealerAreaName = carData["DealerAreaName"].string
        dealerPlantId = carData["DealerPlantId"].int
        dealerPlantName = carData["DealerPlantName"].string
        
        
        maintainances = []
        if let items = json["CarMaintainData"].array {
            if items.count > 0 {
                if let itemss = items[0].array {
                    for itemm in itemss {
        //                Logger.d("item: \(item)")
                        let maintainance: Maintainance = Maintainance(with: itemm)
        //                Logger.d("load maintainance from server response: \(maintainance)")
                        maintainances?.append(maintainance)
                    }
                }
            }
        }
//        if let items = json["CarMaintainData"].array {
//            for item in items {
////                Logger.d("item: \(item)")
//                let maintainance: Maintainance = Maintainance(with: item)
////                Logger.d("load maintainance from server response: \(maintainance)")
//                maintainances?.append(maintainance)
//            }
//        }
    }
    
    private func load() {
//        let defaults = UserDefaults.standard
//        
//        self.fbId = defaults.string(forKey: keyFbId)
//        self.accountId = defaults.string(forKey: keyAccountId)
//        self.showProfile = defaults.bool(forKey: keyShowProfile)
        
        //        Logger.d("load user from local data: \(self.description)")
    }
    
//    func save() {
//        let defaults = UserDefaults.standard
//        
//        defaults.set(self.fbId, forKey: keyFbId)
//        defaults.set(self.accountId, forKey: keyAccountId)
//        defaults.set(self.showProfile, forKey: keyShowProfile)
//    }
//    
//    func clear() {
//        let defaults = UserDefaults.standard
//        defaults.removeObject(forKey: keyFbId)
//        defaults.removeObject(forKey: keyAccountId)
//        defaults.removeObject(forKey: keyShowProfile)
//        
//        self.fbId = nil
//        self.accountId = nil
//        self.showProfile = nil
//    }
}
