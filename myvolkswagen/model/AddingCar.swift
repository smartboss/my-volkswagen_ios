//
//  AddingCar.swift
//  myvolkswagen
//
//  Created by Apple on 2/25/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit

class AddingCar: NSObject {

    var name: String?
    var platePrefix: String?
    var plateSuffix: String?
    var vinLast5: String?
    
    var typeId: Int?
    var typeName: String?
    
    var models: [Model]?
    var selectedModelIndex: Int = 0
    
    func getFullPlateNumber() -> String {
        return "\(platePrefix!)-\(plateSuffix!)"
    }
}
