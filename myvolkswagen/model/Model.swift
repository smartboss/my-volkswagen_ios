//
//  Model.swift
//  myvolkswagen
//
//  Created by Apple on 2/25/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class Model: NSObject {

    var id: Int?
    var name: String?
    var image: String?
    
    
    public override var description: String {
        return "\n{\n id: \(String(describing: self.id)),\n"
            + " name: \(String(describing: self.name))\n}"
    }
    
    override init() {
        super.init()
    }
    
    init(with json:JSON) {
        id = json["id"].int
        name = json["name"].string
        image = json["image"].string
        
        super.init()
    }
    
//    init(with name:String) {
//        self.name = name
//
//        super.init()
//    }
    
    init(with model:Model) {
        self.id = model.id
        self.name = model.name
        self.image = model.image
        
        super.init()
    }
}
