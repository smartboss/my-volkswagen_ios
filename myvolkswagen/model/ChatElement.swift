//
//  ChatElement.swift
//  myvolkswagen
//
//  Created by Apple on 9/2/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChatElement: NSObject {

    var image_url: String
    var title: String
    var subtitle: String?
    var actions: [ChatAction]?
    
    public override var description: String {
        return "\n{\n image_url: \(String(describing: self.image_url)),\n"
            + " title: \(String(describing: self.title)),\n"
            + " subtitle: \(String(describing: self.subtitle)),\n"
            + " actions: \(String(describing: self.actions))\n}"
    }
    
    init(with json:JSON) {
        image_url = json["image_url"].stringValue
        title = json["title"].stringValue
        
        super.init()
        
        subtitle = json["subtitle"].string
        actions = []
        if let items = json["action"].array {
            for item in items {
//                Logger.d("item: \(item)")
                let chatAction: ChatAction = ChatAction(with: item)
//                Logger.d("load chat action from server response: \(chatAction)")
                actions?.append(chatAction)
            }
        }
    }
    
    func toDictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        
        dictionary["image_url"] = image_url
        dictionary["title"] = title
        
        if let subtitle = subtitle {
            dictionary["subtitle"] = subtitle
        }
        
        var elem: [[String: Any]] = []
        if let items = self.actions {
            for item in items {
                elem.append(item.toDictionary())
            }
        }
        if elem.count > 0 {
            dictionary["action"] = elem
        }
        
        return dictionary
    }
}
