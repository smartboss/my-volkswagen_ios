//
//  ChatAnswer.swift
//  myvolkswagen
//
//  Created by Apple on 9/2/19.
//  Copyright © 2019 volkswagen. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChatAnswer: NSObject {
    
    var answer: String?
    var questions: [String]?
    var score: Double?
    
    public override var description: String {
        return "\n{\n answer: \(String(describing: self.answer)),\n"
            + " questions: \(String(describing: self.questions)),\n"
            + " score: \(String(describing: self.score))\n}"
    }
    
    init(with json:JSON) {
        super.init()
        
        answer = json["answer"].string
        questions = []
        if let items = json["questions"].array {
            for item in items {
//                Logger.d("item.string: \(String(describing: item.string))")
                if let q = item.string {
                    questions?.append(q)
                }
            }
        }
        score = json["score"].double
    }
}
